﻿// -----------------------------------------------------------------------
// <copyright file="Extension.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using System.Xml;
using System.Xml.Linq;
using Itenso.TimePeriod;

namespace Nouvem.Shared
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Media;
    using Neodynamic.SDK.Printing;
    using Nouvem.Shared.Properties;

    /// <summary>
    /// Class of shared extension methods.
    /// </summary>
    public static class Extension
    {
        #region bool

        /// <summary>
        /// Method that compares strings, ignoring case, and trimming whitespace.
        /// </summary>
        /// <param name="a">The first string to compare.</param>
        /// <param name="b">The second string to compare.</param>
        /// <returns>A flag, indicating a match or not.</returns>
        public static bool CompareIgnoringCase(this string a, string b)
        {
            if (a == null || b == null)
            {
                return false;
            }

            return a.ToLower().Trim().Equals(b.ToLower().Trim());
        }

        /// <summary>
        /// Method that compares strings, ignoring case, and trimming whitespace.
        /// </summary>
        /// <param name="a">The first string to compare.</param>
        /// <param name="b">The second string to compare.</param>
        /// <returns>A flag, indicating a match or not.</returns>
        public static bool CompareIgnoringCaseAndWhitespace(this string a, string b)
        {
            if (a == null || b == null)
            {
                return false;
            }

            return a.ToLower().Replace(" ", "").Trim().Equals(b.ToLower().Replace(" ", "").Trim());
        }

        /// <summary>
        /// Method that compares strings, ignoring case, and trimming whitespace.
        /// </summary>
        /// <param name="a">The first string to compare.</param>
        /// <param name="b">The second string to search the beginning of a for.</param>
        /// <returns>A flag, indicating a match or not.</returns>
        public static bool StartsWithIgnoringCase(this string a, string b)
        {
            if (a == null)
            {
                a = string.Empty;
            }

            if (b == null)
            {
                b = string.Empty;
            }

            return a.ToLower().Trim().StartsWith(b.ToLower().Trim());
        }

        /// <summary>
        /// Method that compares strings, ignoring case, and trimming whitespace.
        /// </summary>
        /// <param name="a">The first string to compare.</param>
        /// <param name="b">The second string to search the beginning of a for.</param>
        /// <returns>A flag, indicating a match or not.</returns>
        public static bool EndsWithIgnoringCase(this string a, string b)
        {
            if (a == null)
            {
                a = string.Empty;
            }

            if (b == null)
            {
                b = string.Empty;
            }

            return a.ToLower().Trim().EndsWith(b.ToLower());
        }

        /// <summary>
        /// Method that compares strings, ignoring case, and trimming whitespace.
        /// </summary>
        /// <param name="a">The first string to compare.</param>
        /// <param name="b">The second string to search the beginning of a for.</param>
        /// <returns>A flag, indicating a match or not.</returns>
        public static bool ContainsIgnoringCase(this string a, string b)
        {
            if (a == null)
            {
                a = string.Empty;
            }

            if (b == null)
            {
                b = string.Empty;
            }

            return a.ToLower().Trim().Contains(b.ToLower().Trim());
        }

        /// <summary>
        /// Validates a time.
        /// </summary>
        /// <param name="time">The time to checkt.</param>
        /// <returns>The time validation result.</returns>
        public static bool IsValidTime(this string time)
        {
            DateTime outTime;
            string[] formats =
                {
                    "h.mm",
                    "H.mm",
                    "h:mm",
                    "H:mm",
                    "hh.mm",
                    "HH.mm",
                    "hh:mm", 
                    "HH:mm",
                    "HH",
                    "hh",
                    "H",
                    "h"
                };

            return DateTime.TryParseExact(time, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out outTime);
        }

        /// <summary>
        /// Validates an email address.
        /// </summary>
        /// <param name="s">The email to validate.</param>
        /// <returns>Flag, as to a valid email address or not.</returns>
        public static bool IsValidEmail(this string s)
        {
            return Regex.IsMatch(s, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        /// <summary>
        /// Convert a nullable Boolean to a Boolean.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>The boolean conversion.</returns>
        public static bool ToBool(this bool? b)
        {
            return b.HasValue && b == true;
        }

        /// <summary>
        /// Convert a string true / false to a Boolean.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>The boolean conversion.</returns>
        public static bool StringTrueOrFalseToBool(this string s)
        {
            return s.CompareIgnoringCase("True");
        }

        /// <summary>
        /// Method to convert Y or N to Boolean.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>Boolean conversion of Y or N.</returns>
        public static bool YesNoShortToBool(this string s)
        {
            return s.ToUpper() == Constant.YesShort.ToUpper();
        }

        /// <summary>
        /// Method to convert string true or false to bool.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>Boolean conversion.</returns>
        public static bool ToBool(this string s)
        {
            return s.CompareIgnoringCase(Constant.True);
        }

        /// <summary>
        /// Convert an to a Boolean.
        /// </summary>
        /// <param name="b">The object to convert.</param>
        /// <returns>The boolean conversion.</returns>
        public static bool ToBool(this object b)
        {
            if (b is bool)
            {
                return (bool)b;
            }

            return false;
        }

        /// <summary>
        /// Convert an to a Boolean.
        /// </summary>
        /// <param name="b">The object to convert.</param>
        /// <returns>The boolean conversion.</returns>
        public static bool ToBool(this DateTime? b)
        {
            return b.HasValue;
        }

        /// <summary>
        /// Convert an to a Boolean.
        /// </summary>
        /// <param name="b">The object to convert.</param>
        /// <returns>The boolean conversion.</returns>
        public static int ToInt(this object b)
        {
            if (b is int)
            {
                return (int)b;
            }

            return 0;
        }

        /// <summary>
        /// Converts a windows state to boolean.
        /// </summary>
        /// <param name="w">The window state to convert.</param>
        /// <returns>A converted windows state.</returns>
        public static bool ToBool(this WindowState w)
        {
            return w == WindowState.Maximized;
        }

        /// <summary>
        /// Determines if a nullable boolean is true.
        /// </summary>
        /// <param name="b">The input parameter.</param>
        /// <returns>Flag, as to true.</returns>
        public static bool IsTrue(this bool? b)
        {
            return b == true;
        }
        
        /// <summary>
        /// Determines if a nullable boolean is false.
        /// </summary>
        /// <param name="b">The input parameter.</param>
        /// <returns>Flag, as to false.</returns>
        public static bool IsFalse(this bool? b)
        {
            return b == false;
        }

      /// <summary>
      /// Determines if a nullable boolean is null or false.
      /// </summary>
      /// <param name="b">The input parameter.</param>
      /// <returns>Flag, as to null or false.</returns>
        public static bool IsNullOrFalse(this bool? b)
       {
           return !b.HasValue || b == false;
       }

        /// <summary>
        /// Determines if an object is null.
        /// </summary>
        /// <param name="o">The input parameter.</param>
        /// <returns>Flag, as to null or not.</returns>
        public static bool IsNull<T>(this T o)
        {
            return o == null;
        }

        /// <summary>
        /// Determines if an object is null.
        /// </summary>
        /// <param name="o">The input parameter.</param>
        /// <returns>Flag, as to null or not.</returns>
        public static bool IsNull(this object o)
        {
            return o == null;
        }

        /// <summary>
        /// Determines if an object is null.
        /// </summary>
        /// <param name="o">The input parameter.</param>
        /// <returns>Flag, as to null or not.</returns>
        public static bool IsNotNull<T>(this T o)
        {
            return o != null;
        }

        /// <summary>
        /// Determines if an object is null.
        /// </summary>
        /// <param name="o">The input parameter.</param>
        /// <returns>Flag, as to null or not.</returns>
        public static bool IsNotNull(this object o)
        {
            return o != null;
        }

        /// <summary>
        /// Determines if a nullable boolean is null or true.
        /// </summary>
        /// <param name="b">The input parameter.</param>
        /// <returns>Flag, as to null or true.</returns>
        public static bool IsNullOrTrue(this bool? b)
        {
            return !b.HasValue || b == true;
        }

        /// <summary>
        /// Determines if a collection is null or empty.
        /// </summary>
        /// <typeparam name="T">The generic collection type.</typeparam>
        /// <param name="col">The collection.</param>
        /// <returns>Flag, as to a null or empty collection.</returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> col)
        {
            return col == null || !col.Any();
        }

        /// <summary>
        /// Convert a nullable Boolean to a Boolean.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>The boolean conversion.</returns>
        public static bool AuthorisationToBool(this string s)
        {
            return s.CompareIgnoringCaseAndWhitespace("Full Access");
        }

        /// <summary>
        /// Method that determines if the input string can be converted
        /// to a numeric value.
        /// </summary>
        /// <param name="stringToCheck">the string to check</param>
        /// <returns>a flag indicating if the string is convertivble or not</returns>
        public static bool IsNumeric(this string stringToCheck)
        {
            decimal result;
            if (decimal.TryParse(stringToCheck, out result))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Checks that the file path is valid on the file system.
        /// </summary>
        /// <param name="path">The path to verify.</param>
        /// <returns>Flag to indicate if the path is valid.</returns>
        public static bool VerifyFilePath(this string path)
        {
            return File.Exists(path);
        }

        /// <summary>
        /// Checks that a nullable integer is either null or zero.
        /// </summary>
        /// <param name="i">the integer to check</param>
        /// <returns>flag to indicate if it's null.</returns>
        public static bool IsNull(this int? i)
        {
            return i == null;
        }

        /// <summary>
        /// Converts a string to a nullable decimal.
        /// </summary>
        /// <param name="d">The decimal to check.</param>
        /// <returns>flag to indicate if it's null.</returns>
        public static bool IsNull(this decimal? d)
        {
            return !d.HasValue;
        }

        /// <summary>
        /// Converts a string to a nullable decimal.
        /// </summary>
        /// <param name="d">The double to check.</param>
        /// <returns>flag to indicate if it's null.</returns>
        public static bool IsNull(this double? d)
        {
            return !d.HasValue;
        }

        /// <summary>
        /// Checks that a nullable integer is either null or zero.
        /// </summary>
        /// <param name="i">the integer to check</param>
        /// <returns>flag to indicate if it's null or zero.</returns>
        public static bool IsNullOrZero(this int? i)
        {
            return i == null || i == 0;
        }

        /// <summary>
        /// Converts a string to a nullable decimal.
        /// </summary>
        /// <param name="d">The decimal to check.</param>
        /// <returns>flag to indicate if it's null or zero.</returns>
        public static bool IsNullOrZero(this decimal? d)
        {
            return !d.HasValue || d == 0;
        }

        /// <summary>
        /// Converts a string to a nullable decimal.
        /// </summary>
        /// <param name="d">The double to check.</param>
        /// <returns>flag to indicate if it's null or zero.</returns>
        public static bool IsNullOrZero(this double? d)
        {
            return !d.HasValue || d == 0;
        }

        /// <summary>
        /// Checks that a nullable integer is either null or zero.
        /// </summary>
        /// <param name="i">the integer to check</param>
        /// <returns>flag to indicate if it's null or zero.</returns>
        public static bool IsNullOrZeroOrOne(this int? i)
        {
            return i == null || i == 0 || i == 1;
        }

        /// <summary>
        /// Check if a string is a time in 24 hour format (HH:MM).
        /// </summary>
        /// <param name="s">The string to validate.</param>
        /// <returns>A flag to indicate if the string is valid.</returns>
        public static bool IsValidHoursAndMins(this string s)
        {
            return !string.IsNullOrEmpty(s) && s.Length <= 5 && Regex.IsMatch(s, @"\b\d{2}:\d{2}");
        }

        /// <summary>
        /// Check if a string contains only characters.
        /// </summary>
        /// <param name="s">The string to validate.</param>
        /// <returns>A flag to indicate if the string is valid.</returns>
        public static bool IsCharacterSequence(this string s)
        {
            return !string.IsNullOrEmpty(s) && Regex.IsMatch(s, @"^[a-z A-Z]+");
        }

        /// <summary>
        /// Check if a string contains only numbers.
        /// </summary>
        /// <param name="s">The string to validate.</param>
        /// <returns>A flag to indicate if the string is valid.</returns>
        public static bool IsNumericSequence(this string s)
        {
            return !string.IsNullOrEmpty(s) && Regex.IsMatch(s, @"^[0-9]*$");
        }

        /// <summary>
        /// Check if a string contains only decimal values.
        /// </summary>
        /// <param name="s">The string to validate.</param>
        /// <returns>A flag to indicate if the string is valid.</returns>
        public static bool IsDecimalSequence(this string s)
        {
            return !string.IsNullOrEmpty(s) && Regex.IsMatch(s, @"^[0-9.]*$");
        }

        /// <summary>
        /// Check if a string is a time in 24 hour format (HH:MM).
        /// </summary>
        /// <param name="s">The string to validate.</param>
        /// <returns>A flag to indicate if the string is valid.</returns>
        public static bool Is24HourTime(this string s)
        {
            return !string.IsNullOrEmpty(s) && s.Length <=5 && Regex.IsMatch(s, @"([0-1]\d|2[0-3]):([0-5]\d)");
        }

        /// <summary>
        /// Method to convert Yes or No to Boolean.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>Boolean conversion of Yes or No.</returns>
        public static bool YesNoToNonNullableBool(this string s)
        {
            return s == Constant.Yes;
        }

        #endregion

        #region bool?

        /// <summary>
        /// Method to convert Y or N to Boolean.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>Boolean conversion of Y or N.</returns>
        public static bool? YesNoShortToNullableBool(this string s)
        {
            return s != null && s.ToUpper() == Constant.YesShort.ToUpper();
        }

        /// <summary>
        /// Method to convert Yes or No to Boolean.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>Boolean conversion of Yes or No.</returns>
        public static bool? YesNoToBool(this string s)
        {
            return s.CompareIgnoringCase(Constant.Yes);
        }

        /// <summary>
        /// Checks that the current day falls within a specified date range.
        /// </summary>
        /// <param name="from">The start date.</param>
        /// <param name="to">The end date.</param>
        /// <returns>A falg, indicating whether the current day falls within the specified date range.</returns>
        public static bool IsValidDate(this DateTime from, DateTime to)
        {
            return from <= DateTime.Today && to >= DateTime.Today;
        }

        /// <summary>
        /// Checks that the current day falls within a specified date range.
        /// </summary>
        /// <param name="from">The start date.</param>
        /// <param name="to">The end date.</param>
        /// <returns>A falg, indicating whether the current day falls within the specified date range.</returns>
        public static bool IsValidDate(this DateTime? from, DateTime? to)
        {
            if (!from.HasValue || !to.HasValue)
            {
                // default to pass, if a value is missing.
                return true;
            }

            return from <= DateTime.Today && to >= DateTime.Today;
        }

        #endregion

        #region byte[]

        /// <summary>
        /// Converts a uri to stream.
        /// </summary>
        /// <param name="uri">The uri to convert.</param>
        /// <returns>A converted uri byte array.</returns>
        public static byte[] UriToBytes(this Uri uri)
        {
            try
            {
                var stream = Application.GetResourceStream(uri);

                if (stream != null)
                {
                    var memoryStream = new MemoryStream();
                    stream.Stream.CopyTo(memoryStream);

                    return memoryStream.ToArray();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }

        /// <summary>
        /// Converts a string to stream.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>A converted string byte array.</returns>
        public static byte[] ToBytes(this string s)
        {
            try
            {
                return Encoding.ASCII.GetBytes(s);
            }
            catch 
            {
                return null;
            }
        }

        #endregion

        #region Char

        /// <summary>
        /// Convert a one letter string to a character.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>The character conversion.</returns>
        public static char ToChar(this string s)
        {
            return !string.IsNullOrEmpty(s) && s.Length == 1 ? s[0] : '\0';
        }

        /// <summary>
        /// Convert an int to character.
        /// </summary>
        /// <param name="i">The interger to convert.</param>
        /// <returns>The interger conversion.</returns>
        public static char ToChar(this int i)
        {
            try
            {
                return (char)i;
            }
            catch 
            {
                return '\0';
            }
        }

        #endregion

        #region colour

        /// <summary>
        /// Converts a brush to color.
        /// </summary>
        /// <param name="brush">The brush to convert.</param>
        /// <returns>A converted color.</returns>
        public static System.Windows.Media.Color BrushToColor(this Brush brush)
        {
            var color = new System.Windows.Media.Color();
            if (brush is SolidColorBrush)
            {
                color = (brush as SolidColorBrush).Color;
            }

            return color;
        }

        /// <summary>
        /// Converts a color to brush.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        /// <returns>A converted brush.</returns>
        public static Brush ColorToBrush(this System.Windows.Media.Color color)
        {
            return new SolidColorBrush(color);
        }

        /// <summary>
        /// Converts a printer color to brush.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        /// <returns>A converted brush.</returns>
        public static Brush PrinterColorToBrush(this Neodynamic.SDK.Printing.Color color)
        {
            if (color == Neodynamic.SDK.Printing.Color.Black)
            {
                return Brushes.Black;
            }

            return Brushes.White;
        }

        /// <summary>
        /// Converts a printer color to color.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        /// <returns>A converted printer color.</returns>
        public static System.Windows.Media.Color PrinterColorToColor(this Neodynamic.SDK.Printing.Color color)
        {
            if (color == Neodynamic.SDK.Printing.Color.Black)
            {
                return Colors.Black;
            }

            return Colors.White;
        }

        /// <summary>
        /// Converts a brush to printer color.
        /// </summary>
        /// <param name="brush">The brush to convert.</param>
        /// <returns>A converted printer color.</returns>
        public static Neodynamic.SDK.Printing.Color BrushToPrinterColor(this Brush brush)
        {
            if (brush.Equals(Brushes.White))
            {
                return Neodynamic.SDK.Printing.Color.White;
            }

            return Neodynamic.SDK.Printing.Color.Black;
        }

        /// <summary>
        /// Converts a color to printer color.
        /// </summary>
        /// <param name="color">The color to convert.</param>
        /// <returns>A converted printer color.</returns>
        public static Neodynamic.SDK.Printing.Color ColorToPrinterColor(this System.Windows.Media.Color color)
        {
            if (color.Equals(Colors.White))
            {
                return Neodynamic.SDK.Printing.Color.White;
            }

            return Neodynamic.SDK.Printing.Color.Black;
        }

        #endregion

        #region DateTime

        /// <summary>
        /// Method that converts a nullable datetime to datetime.
        /// </summary>
        /// <param name="source">The nullable datetime to convert.</param>
        /// <returns>A datetime.</returns>
        public static DateTime ToDate(this DateTime? source)
        {
            if (source.HasValue)
            {
                return (DateTime)source;
            }

            return DateTime.Today;
        }

        /// <summary>
        /// Convert string DateTime to DateTime.
        /// </summary>
        /// <param name="source">The string date to convert.</param>
        /// <returns>The date time conversion.</returns>
        public static DateTime ToDate(this string source)
        {
            DateTime date;

            string[] formats =
                {
                    "dd/MM/yyyy",
                    "dd/M/yyyy", 
                    "d/M/yyyy", 
                    "d/MM/yyyy", 
                    "dd/MM/yy", 
                    "dd/M/yy", 
                    "d/M/yy", 
                    "d/MM/yy", 
                    "yyyy-MM-dd", 
                    "M/d/yyyy h:mm:ss tt", 
                    "dd/MM/yyyy h:mm:ss", 
                    "d/M/yyyy hh:mm:ss", 
                    "dd/MM/yyyy hh:mm:ss", 
                    "dd/MM/yyyy HH:mm:ss", 
                    "yyyyMMdd",
                    "ddMMyyyy",
                    "yyyyMMdd"
                };

            if (DateTime.TryParseExact(source, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                return date;
            }

            return date;
        }

        /// <summary>
        /// Convert string DateTime to DateTime.
        /// </summary>
        /// <param name="s">The string date to convert.</param>
        /// <returns>The date time conversion.</returns>
        public static DateTime? ToDateNullable(this string s)
        {
            DateTime? date;
            DateTime dateNotNullable;

            string[] formats =
                {
                    "dd/MM/yyyy",
                    "dd/M/yyyy", 
                    "d/M/yyyy", 
                    "d/MM/yyyy", 
                    "dd/MM/yy", 
                    "dd/M/yy", 
                    "d/M/yy", 
                    "d/MM/yy", 
                    "yyyy-MM-dd", 
                    "M/d/yyyy h:mm:ss tt", 
                    "dd/MM/yyyy h:mm:ss", 
                    "d/M/yyyy hh:mm:ss", 
                    "dd/MM/yyyy hh:mm:ss", 
                    "dd/MM/yyyy HH:mm:ss", 
                    "yyyyMMdd"
                };

            if (DateTime.TryParseExact(s, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateNotNullable))
            {
                return (DateTime?)dateNotNullable;
            }

            return null;
        }

        /// <summary>
        /// Method that calculates the date of the Monday of the current week falls on.
        /// </summary>
        /// <param name="year">The given year</param>
        /// <returns>The date of curent weeks monday.</returns>
        public static DateTime GetMondaysDate(this int year)
        {
            // get the week number (Note- we are using the non iso8601 standard for our calculations)
            var dfi = DateTimeFormatInfo.CurrentInfo;
            var cal = dfi.Calendar;
            var weekNumber = cal.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);

            // get the first day of the current year.
            var firstDayOfYear = new DateTime(year, 1, 1);

            // get the start day of the current week.
            var currentWeek = firstDayOfYear.AddDays((weekNumber * 7) - 6);

            // now iterate until monday is reached.
            while (currentWeek.DayOfWeek != DayOfWeek.Monday)
            {
                currentWeek = currentWeek.AddDays(1);
            }

            var mondaysDate = currentWeek;

            return mondaysDate;
        }

        /// <summary>
        /// Gets the current Iso8601 week.
        /// </summary>
        /// <param name="date">The date(year) to get the current week for.</param>
        /// <returns>The current Iso8601 week.</returns>
        public static int GetIso8601WeekOfYear(this DateTime date)
        {
            // Iso8601 stipulates that the first week of the year is the week containing the first thursday of the year.
            var day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(date);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                date = date.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        /// <summary>
        /// Convert a string date to a string date of the format YYYY-MM-DD.
        /// </summary>
        /// <param name="source">The date to convert and format.</param>
        /// <returns>The string conversion of the date in the format.</returns>
        public static string ToStringYYYYMMDD(this DateTime source)
        {
            return source.ToString("yyyy-MM-dd");
        }

        public static DateTime AddBusinessDays(this DateTime current, int days)
        {
            var sign = Math.Sign(days);
            var unsignedDays = Math.Abs(days);
            for (var i = 0; i < unsignedDays; i++)
            {
                do
                {
                    current = current.AddDays(sign);
                } while (current.DayOfWeek == DayOfWeek.Saturday ||
                         current.DayOfWeek == DayOfWeek.Sunday);
            }

            return current;
        }

        #endregion

        #region DateTime?

        /// <summary>
        /// Convert string DateTime to DateTime.
        /// </summary>
        /// <param name="source">The string date to convert.</param>
        /// <returns>The date time conversion.</returns>
        public static DateTime? ToNullableDate(this string source)
        {
            DateTime date;

            string[] formats =
                {
                    "dd/MM/yyyy", 
                    "dd/M/yyyy", 
                    "d/M/yyyy", 
                    "d/MM/yyyy", 
                    "dd/MM/yy", 
                    "dd/M/yy", 
                    "d/M/yy",
                    "d/MM/yy", 
                    "yyyy-MM-dd",
                    "M/d/yyyy h:mm:ss tt", 
                    "dd/MM/yyyy h:mm:ss", 
                    "d/M/yyyy hh:mm:ss", 
                    "dd/MM/yyyy hh:mm:ss", 
                    "dd/MM/yyyy HH:mm:ss",
                    "ddMMyyyy"
                };

            if (DateTime.TryParseExact(source, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                return date;
            }

            return null;
        }

        /// <summary>
        /// Convert a nullable date to a string date.
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>The string conversion of the date in the format.</returns>
        public static string ToStringYYYYMMDD(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToString("yyyy-MM-dd") : string.Empty;
        }

        #endregion

        #region decimal

        /// <summary>
        /// Gets the percentage amount of a in relation to b.
        /// </summary>
        /// <param name="a">The a double.</param>
        /// <param name="b">The b double.</param>
        /// <returns>A percentage amount of a in relation to b.</returns>
        public static decimal ToDecimalGetPercentage(this decimal a, decimal b)
        {
            if (a <= 0)
            {
                return 0;
            }

            return Math.Round(((a / b) * 100), 2);
        }

        /// <summary>
        /// Applys a percentage amount to double d.
        /// </summary>
        /// <param name="d">The d double.</param>
        /// <param name="percentage">The percentage to apply.</param>
        /// <returns>An applied percentage amount.</returns>
        public static decimal ToDecimalApplyPercentage(this decimal d, decimal percentage)
        {
            return Math.Round((d / 100) * percentage, 2);
        }

        /// <summary>
        /// Gets the difference between 2 numbers.
        /// </summary>
        /// <param name="d">The first number to compare.</param>
        /// <param name="d2">The second number to compare.</param>
        /// <returns>The difference between the input numbers.</returns>
        public static decimal GetDifference(this decimal d, decimal d2)
        {
            return Math.Abs(d - d2);
        }

        /// <summary>
        /// Method that removes any trailing 0's from a decimal
        /// </summary>
        /// <param name="d">the decimal to truncate</param>
        /// <returns>a truncated decimal</returns>
        public static decimal TruncateTrailingZeros(this decimal d)
        {
            return d / 1.000000000000000000000000000000000m;
        }

        /// <summary>
        /// Gets the difference between 2 numbers.
        /// </summary>
        /// <param name="d">The first number to compare.</param>
        /// <param name="d2">The second number to compare.</param>
        /// <returns>The difference between the input numbers.</returns>
        public static decimal Subtract(this decimal d, decimal d2)
        {
            return d - d2;
        }

        /// <summary>
        /// Convert a nullable decimal to a decimal.
        /// </summary>
        /// <param name="i">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static decimal ToDecimal(this decimal? i)
        {
            if (i.HasValue)
            {
                return (decimal)i;
            }

            return 0;
        }

        /// <summary>
        /// Convert a double to a decimal.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Converted decimal.</returns>
        public static decimal ToDecimal(this double d)
        {
            return (decimal)d;
        }

        /// <summary>
        /// Convert a nullable double to a decimal.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Converted decimal.</returns>
        public static decimal ToDecimal(this double? d)
        {
            if (d.HasValue)
            {
                return (decimal)d;
            }

            return 0;
        }

        /// <summary>
        /// Converts a string to decimal.
        /// </summary>
        /// <param name="value">The string to convert.</param>
        /// <returns>The converted decimal.</returns>
        public static decimal ToDecimal(this string value)
        {
            decimal conversion = 0;

            if (decimal.TryParse(value, out conversion))
            {
                return conversion;
            }

            return 0;
        }

        /// <summary>
        /// Converts a nullable int to decimal.
        /// </summary>
        /// <param name="i">The nullable int to convert.</param>
        /// <returns>The converted decimal.</returns>
        public static decimal ToDecimal(this int? i)
        {
            if (i.HasValue)
            {
                return (decimal)i;
            }

            return 0M;
        }

        /// <summary>
        /// Gets the percentage amount of a in relation to b.
        /// </summary>
        /// <param name="a">The a double.</param>
        /// <param name="b">The b double.</param>
        /// <returns>A percentage amount of a in relation to b.</returns>
        public static decimal ZeroIfNegative(this decimal a)
        {
            if (a < 0)
            {
                return 0;
            }

            return a;
        }

        /// <summary>
        /// Converts a nullable int to decimal.
        /// </summary>
        /// <param name="i">The nullable int to convert.</param>
        /// <returns>The converted decimal.</returns>
        public static decimal ToNearestDecimalMultiple(this decimal d, int multiple)
        {
            var rem = d % multiple;
            return rem >= 5 ? (d - rem + multiple) : (d - rem);
        }

        #endregion

        #region decimal?

        /// <summary>
        /// Converts a string to a nullable decimal.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>A nullable decimal.</returns>
        public static decimal? ToNullableDecimal(this string s)
        {
            decimal i;

            if (Decimal.TryParse(s, out i))
            {
                return i;
            }

            return null;
        }

        /// <summary>
        /// Converts an object to a nullable decimal.
        /// </summary>
        /// <param name="s">The object to convert.</param>
        /// <returns>A nullable decimal.</returns>
        public static decimal ToDecimal(this object o)
        {
            try
            {
                return Convert.ToDecimal(o);
            }
            catch (Exception)
            {
                return 0M;
            }
        }

        /// <summary>
        /// Formats a nullable decimal;
        /// </summary>
        /// <param name="d">The decimal to format.</param>
        /// <param name="dp">The rounding value.</param>
        /// <returns>A formatted, nullable decimal.</returns>
        public static decimal? ToNullableDecimalFormatted(this decimal? d, int dp)
        {
            if (!d.HasValue)
            {
                return d;
            }

            if (dp == 0)
            {
                return Convert.ToDecimal(string.Format("{0:F0}", d));
            }

            if (dp == 1)
            {
                return Convert.ToDecimal(string.Format("{0:F1}", d));
            }

            if (dp == 2)
            {
                return Convert.ToDecimal(string.Format("{0:F2}", d));
            }

            if (dp == 3)
            {
                return Convert.ToDecimal(string.Format("{0:F3}", d));
            }

            if (dp == 4)
            {
                return Convert.ToDecimal(string.Format("{0:F4}", d));
            }

            return Convert.ToDecimal(string.Format("{0:F5}", d));
        }


        #endregion

        #region double

        /// <summary>
        /// Gets the difference between 2 numbers.
        /// </summary>
        /// <param name="d">The first number to compare.</param>
        /// <param name="d2">The second number to compare.</param>
        /// <returns>The difference between the input numbers.</returns>
        public static double GetDifference(this double d, double d2)
        {
            return Math.Abs(d - d2);
        }

        /// <summary>
        /// Convert a nullable int to a double.
        /// </summary>
        /// <param name="i">The value to convert.</param>
        /// <returns>Double conversion of the value.</returns>
        public static double ToDouble(this int? i)
        {
            if (i.HasValue)
            {
                return (double)i;
            }

            return 0;
        }

        /// <summary>
        /// Convert a decimal to a double.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Decimal conversion of the value.</returns>
        public static double ToDouble(this decimal d)
        {
            return (double)d;
        }

        /// <summary>
        /// Convert a nullable long to a double.
        /// </summary>
        /// <param name="l">The value to convert.</param>
        /// <returns>Double conversion of the value.</returns>
        public static double ToDouble(this long? l)
        {
            if (l.HasValue)
            {
                return (double)l;
            }

            return 0;
        }

        /// <summary>
        /// Converts a string to double.
        /// </summary>
        /// <param name="value">The string to convert.</param>
        /// <returns>The converted double.</returns>
        public static double ToDouble(this string value)
        {
            double conversion = 0;

            if (double.TryParse(value, out conversion))
            {
                return conversion;
            }

            return 0;
        }

        /// <summary>
        /// Convert a nullable decimal to a double.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Decimal conversion of the value.</returns>
        public static double ToDouble(this decimal? d)
        {
            if (d.HasValue)
            {
                return (double)d;
            }

            return 0;
        }

        /// <summary>
        /// Convert an integer to a double.
        /// </summary>
        /// <param name="i">The value to convert.</param>
        /// <returns>Decimal conversion of the value.</returns>
        public static double ToDouble(this int i)
        {
            return i;
        }

        /// <summary>
        /// Converts a nullable double to double.
        /// </summary>
        /// <param name="d">The double to convert.</param>
        /// <returns>The converted double.</returns>
        public static double ToDouble(this double? d)
        {
            double conversion = 0;

            if (d.HasValue)
            {
                return (double)d;
            }

            return 0;
        }

        /// <summary>
        /// Gets the percentage amount of a in relation to b.
        /// </summary>
        /// <param name="a">The a double.</param>
        /// <param name="b">The b double.</param>
        /// <returns>A percentage amount of a in relation to b.</returns>
        public static double ZeroIfNegative(this double a)
        {
            if (a < 0)
            {
                return 0;
            }

            return a;
        }

        /// <summary>
        /// Gets the percentage amount of a in relation to b.
        /// </summary>
        /// <param name="a">The a double.</param>
        /// <param name="b">The b double.</param>
        /// <returns>A percentage amount of a in relation to b.</returns>
        public static double ToDoubleGetPercentage(this double a, double b)
        {
            if (a <= 0)
            {
                return 0;
            }

            return Math.Round(((a / b) * 100), 2);
        }

        /// <summary>
        /// Applys a percentage amount to double d.
        /// </summary>
        /// <param name="d">The d double.</param>
        /// <param name="percentage">The percentage to apply.</param>
        /// <returns>An applied percentage amount.</returns>
        public static double ToDoubleApplyPercentage(this double d, double percentage)
        {
            return Math.Round((d / 100) * percentage, 2);
        }
        
        /// <summary>
        /// Gets the julian date of the input date.
        /// </summary>
        /// <param name="date">The input date to convert.</param>
        /// <returns>A converted input date.</returns>
        public static double ToJulianDate(this DateTime date)
        {
            return date.ToOADate();
        } 

        /// <summary>
        /// Converts a string to a font compatible double.
        /// </summary>
        /// <param name="value">The string to convert.</param>
        /// <returns>The converted double.</returns>
        public static double ToFontDouble(this string value)
        {
            double conversion;

            if (double.TryParse(value, out conversion))
            {
                return conversion <= 0 ? 10 : conversion;
            }

            return 10;
        }

        #endregion

        #region double?

        /// <summary>
        /// Convert a decimal to a double.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Decimal conversion of the value.</returns>
        public static double? ToDoubleNullable(this decimal d)
        {
            return (double)d;
        }

        /// <summary>
        /// Convert a decimal to a nullable double.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Decimal conversion of the value.</returns>
        public static double? ToDoubleNullable(this decimal? d)
        {
            if (d.HasValue)
            {
                return (double)d;
            }

            return null;
        }

        #endregion

        #region float

        /// <summary>
        /// Convert a double to a double.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Decimal conversion of the value.</returns>
        public static float ToFloat(this double? d)
        {
            if (d.HasValue)
            {
                return (float)d;
            }

            return 0;
        }

        /// <summary>
        /// Convert a decimal to a double.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Decimal conversion of the value.</returns>
        public static float ToFloat(this decimal d)
        {
            return (float)d;
        }

        /// <summary>
        /// Convert a decimal to a double.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Decimal conversion of the value.</returns>
        public static float ToFloat(this decimal? d)
        {
            if (d.HasValue)
            {
                return (float)d;
            }

            return 0;
        }

        #endregion

        #region generic

        /// <summary>
        /// Creates a deep copy of the given object.
        /// </summary>
        /// <param name="objSource">The object source.</param>
        /// <returns>A deep copy of the input object.</returns>
        public static object CloneObject(this object objSource)
        {
            //Get the type of source object and create a new instance of that type
            Type typeSource = objSource.GetType();
            object objTarget = Activator.CreateInstance(typeSource);

            //Get all the properties of source object type
            PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Assign all source property to target object's properties
            foreach (PropertyInfo property in propertyInfo)
            {
                //Check whether property can be written to
                if (property.CanWrite)
                {
                    //check whether property type is value type, enum or string type
                    if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType.Equals(typeof(System.String)))
                    {
                        property.SetValue(objTarget, property.GetValue(objSource, null), null);
                    }
                    //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                    else
                    {
                        object objPropertyValue = property.GetValue(objSource, null);
                        if (objPropertyValue == null)
                        {
                            property.SetValue(objTarget, null, null);
                        }
                        else
                        {
                            property.SetValue(objTarget, objPropertyValue.CloneObject(), null);
                        }
                    }
                }
            }
            return objTarget;
        }

        #endregion

        #region IEnumerable

        /// <summary>
        /// Method which allow a LINQ distinct extension method call to query by the property of an object.
        /// </summary>
        /// <typeparam name="TSource">The type of the collection being returned.</typeparam>
        /// <typeparam name="TKey">The key on which the function passed acts.</typeparam>
        /// <param name="source">The collection being queried.</param>
        /// <param name="keySelector">The value property of the key on which the function acts.</param>
        /// <returns>The collection based on the distinct property passed.</returns>
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        #endregion

        #region IList

        /// <summary>
        /// Join two lists together.
        /// </summary>
        /// <param name="firstCollection">The first collection.</param>
        /// <param name="secondCollection">The second collection.</param>
        /// <typeparam name="T">The type of the object in the collection.</typeparam>
        /// <returns>The two collections joined.</returns>
        public static IList<T> AddToEnumeration<T>(this IList<T> firstCollection, IList<T> secondCollection)
        {
            var tempList = new List<T>();

            firstCollection.ToList().ForEach(x => tempList.Add(x));
            secondCollection.ToList().ForEach(x => tempList.Add(x));

            return tempList;
        }

        #endregion

        #region int

        /// <summary>
        /// Gets the difference between 2 numbers.
        /// </summary>
        /// <param name="i">The first number to compare.</param>
        /// <param name="i2">The second number to compare.</param>
        /// <returns>The difference between the input numbers.</returns>
        public static int GetDifference(this int i, int i2)
        {
            return Math.Abs(i - i2);
        }

        /// <summary>
        /// Convert a nullable integer to an int.
        /// </summary>
        /// <param name="i">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToInt(this int? i)
        {
            if (i.HasValue)
            {
                return (int)i;
            }

            return 0;
        }

        /// <summary>
        /// Convert a nullable byte to an int.
        /// </summary>
        /// <param name="b">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToInt(this byte? b)
        {
            if (b.HasValue)
            {
                return (int)b;
            }

            return 0;
        }

        /// <summary>
        /// Convert a nullable long to an int.
        /// </summary>
        /// <param name="b">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToInt(this long? b)
        {
            if (b.HasValue)
            {
                return (int)b;
            }

            return 0;
        }

        /// <summary>
        /// Convert a double to an int.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToInt(this double d)
        {
            return (int)d;
        }

        /// <summary>
        /// Convert a nullable double to an int.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToInt(this double? d)
        {
            if (d.HasValue)
            {
                return (int)d;
            }

            return 0;
        }

        /// <summary>
        /// Converts a string to int.
        /// </summary>
        /// <param name="value">The string to convert.</param>
        /// <returns>The converted integer.</returns>
        public static int ToInt(this string value)
        {
            int conversion = 0;

            if (int.TryParse(value, out conversion))
            {
                return conversion;
            }

            return 0;
        }

        /// <summary>
        /// Converts a decimal to int, truncating anything after the decimal point.
        /// </summary>
        /// <param name="value">The decimal to convert.</param>
        /// <returns>The converted integer.</returns>
        public static int ToInt(this decimal value)
        {
            try
            {
                return (int)value;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Converts a  nullable decimal to int, truncating anything after the decimal point.
        /// </summary>
        /// <param name="value">The nullable decimal to convert.</param>
        /// <returns>The converted integer.</returns>
        public static int ToInt(this decimal? value)
        {
            if (value.HasValue)
            {
                try
                {
                    return (int)value;
                }
                catch (Exception)
                {
                    return 0;
                }
            }

            return 0;
        }

        /// <summary>
        /// Convert a double to an int ceiling.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToIntCeiling(this double d)
        {
            return Math.Ceiling(d).ToInt();
        }

        /// <summary>
        /// Convert a double to an int ceiling.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToIntCeiling(this decimal d)
        {
            return Math.Ceiling(d).ToInt();
        }

        /// <summary>
        /// Convert a double to an int ceiling.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToMixCount(this double a, double b)
        {
            if (a <= 0 || b <= 0)
            {
                return 1;
            }

            return (a / b).ToIntCeiling();
        }

        /// <summary>
        /// Convert a double to an int ceiling.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToMixCount(this decimal a, decimal b)
        {
            if (a <= 0 || b <= 0)
            {
                return 1;
            }

            return (a / b).ToIntCeiling();
        }

        /// <summary>
        /// Convert a double to an int ceiling.
        /// </summary>
        /// <param name="d">The value to convert.</param>
        /// <returns>Integer conversion of the value.</returns>
        public static int ToIntFloor(this double d)
        {
            return Math.Floor(d).ToInt();
        }

        /// <summary>
        /// Gets the decimal precision of the input decimal.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <returns>The decimal precision value.</returns>
        public static int DecimalPrecision(this decimal value)
        {
            var stringValue = value.ToString();
            var decimalPointPos = stringValue.IndexOf('.');
            if (decimalPointPos == -1)
            {
                return 0;
            }

            return stringValue.Substring(decimalPointPos + 1).Length;
        }

        /// <summary>
        /// Gets the percentage amount of a in relation to b.
        /// </summary>
        /// <param name="a">The a double.</param>
        /// <param name="b">The b double.</param>
        /// <returns>A percentage amount of a in relation to b.</returns>
        public static int ZeroIfNegative(this int a)
        {
            if (a < 0)
            {
                return 0;
            }

            return a;
        }

        /// <summary>
        /// Gets the difference in months between 2 dates.
        /// </summary>
        /// <param name="dateFrom">Start date.</param>
        /// <param name="dateTo">End date.</param>
        /// <returns>The difference in months between 2 dates.</returns>
        public static int DateDifferenceInMonths(this DateTime dateFrom, DateTime dateTo)
        {
            var dateDiff = new DateDiff(dateFrom, dateTo);
            return dateDiff.ElapsedMonths + (dateDiff.ElapsedYears * 12);
        }

        /// <summary>
        /// Gets the difference in days between 2 dates.
        /// </summary>
        /// <param name="dateFrom">Start date.</param>
        /// <param name="dateTo">End date.</param>
        /// <returns>The difference in days between 2 dates.</returns>
        public static int DateDifferenceInCurrentMonthDays(this DateTime dateFrom, DateTime dateTo)
        {
            var dateDiff = new DateDiff(dateFrom, dateTo);
            return dateDiff.ElapsedDays;
        }

        /// <summary>
        /// Gets the difference in days between 2 dates.
        /// </summary>
        /// <param name="dateFrom">Start date.</param>
        /// <param name="dateTo">End date.</param>
        /// <returns>The difference in days between 2 dates.</returns>
        public static int DateDifferenceInDays(this DateTime dateFrom, DateTime dateTo)
        {
            var dateDiff = new DateDiff(dateFrom, dateTo);
            return dateDiff.Days;
        }

        #endregion

        #region int?

        /// <summary>
        /// Converts a string to a nullable integer.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <returns>A nullable int.</returns>
        public static int? ToNullableInt(this string s)
        {
            int i;

            if (int.TryParse(s, out i))
            {
                return i;
            }

            return null;
        }

        /// <summary>
        /// Converts an object to a nullable integer.
        /// </summary>
        /// <param name="o">The object to convert.</param>
        /// <returns>A nullable int.</returns>
        public static int? ToNullableInt(this object o)
        {
            try
            {
                return Convert.ToInt32(o);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Converts an int to a nullable integer.
        /// </summary>
        /// <param name="i">The int to convert.</param>
        /// <returns>A nullable int.</returns>
        public static int? ToNullableInt(this int i)
        {
            return i;
        }

        #endregion

        #region long

        /// <summary>
        /// Converts a string to long.
        /// </summary>
        /// <param name="value">The string to convert.</param>
        /// <returns>The converted long.</returns>
        public static long ToLong(this string value)
        {
            long conversion = 0;

            if (long.TryParse(value, out conversion))
            {
                return conversion;
            }

            return 0;
        }

        /// <summary>
        /// Converts a double to long.
        /// </summary>
        /// <param name="value">The double to convert.</param>
        /// <returns>The converted long.</returns>
        public static long ToLong(this double value)
        {
            long conversion = 0;

            if (long.TryParse(value.ToString(), out conversion))
            {
                return conversion;
            }

            return 0;
        }

        #endregion

        #region short

        /// <summary>
        /// Converts a string to short.
        /// </summary>
        /// <param name="value">The string to convert.</param>
        /// <returns>The converted short.</returns>
        public static short ToShort(this int value)
        {
            try
            {
                return (short) value;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Converts a string to short.
        /// </summary>
        /// <param name="value">The string to convert.</param>
        /// <returns>The converted short.</returns>
        public static short ToShort(this string value)
        {
            short conversion = 0;

            if (short.TryParse(value, out conversion))
            {
                return conversion;
            }

            return 0;
        }

        #endregion

        #region string

        /// <summary>
        /// Method to convert a decimal to a string keeping the input precision.
        /// </summary>
        /// <param name="d">The decimal to convert.</param>
        /// <param name="precision">The precision value.</param>
        /// <returns>String conversion of the decimal input.</returns>
        public static string ToDisplayPrecision(this decimal d, int precision)
        {
            return d.ToString(string.Format("F{0}", precision));
        }

        /// <summary>
        /// Method to convert a decimal to a string keeping the input precision.
        /// </summary>
        /// <param name="d">The decimal to convert.</param>
        /// <param name="precision">The precision value.</param>
        /// <returns>String conversion of the decimal input.</returns>
        public static string ToDisplayPrecision(this double d, int precision)
        {
            return d.ToString(string.Format("F{0}", precision));
        }

        /// <summary>
        /// Returns a string comprised on integers only, up until a non integer is found.
        /// </summary>
        /// <param name="s">The string to parse.</param>
        /// <returns>An integer only string.</returns>
        public static string ReturnIntegersAtStartOfString(this string s)
        {
            var offset = s.TakeWhile(c => c.ToString().IsNumeric()).Count();
            return s.Substring(0, offset);
        }

        /// <summary>
        /// Returns a string comprised of the original string with the end character removed if matching input character.
        /// </summary>
        /// <param name="s">The string to parse.</param>
        /// <returns>An integer only string.</returns>
        public static string RemoveEndCharacterIfMatch(this string s, string character)
        {
            if (!s.IsNullOrEmpty())
            {
                if (s.ElementAt(s.Length - 1).ToString().CompareIgnoringCase(character))
                {
                    return s.Substring(0, s.Length - 1);
                }
            }

            return s;
        }

        /// <summary>
        /// Method to convert Boolean to On or Off.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>String conversion of the Boolean input.</returns>
        public static string BoolToOnOff(this bool b)
        {
            return b ? Constant.On : Constant.Off;
        }

        /// <summary>
        /// Method to convert Boolean to Yes or No.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>String conversion of the Boolean input.</returns>
        public static string BoolToYesNo(this bool b)
        {
            return b ? Constant.Yes : Constant.No;
        }

        /// <summary>
        /// Method to convert Boolean to Yes or No.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>String conversion of the Boolean input.</returns>
        public static string BoolToYesNo(this bool? b)
        {
            return b == true ? Constant.Yes : Constant.No;
        }

        /// <summary>
        /// Method to convert Boolean to Yes or No.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>String conversion of the Boolean input.</returns>
        public static string BoolToPlusMinus(this bool b)
        {
            return b ? "+" : "-";
        }

        /// <summary>
        /// Method to convert Boolean to Y or N.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>String conversion of the Boolean input.</returns>
        public static string BoolToYesNoShort(this bool? b)
        {
            return b == true ? Constant.YesShort : Constant.NoShort;
        }

        /// <summary>
        /// Converts a low level key press to it's string equivalent.
        /// </summary>
        /// <param name="k">The key press.</param>
        /// <returns>A low level key press string.</returns>
        public static string ToScannerString(this System.Windows.Input.Key k)
        {
            if (k == Key.D1)
            {
                return "1";
            }

            if (k == Key.D2)
            {
                return "2";
            }

            if (k == Key.D3)
            {
                return "3";
            }

            if (k == Key.D4)
            {
                return "4";
            }

            if (k == Key.D5)
            {
                return "5";
            }

            if (k == Key.D6)
            {
                return "6";
            }
           
            if (k == Key.D7)
            {
                return "7";
            }

            if (k == Key.D8)
            {
                return "8";
            }

            if (k == Key.D9)
            {
                return "9";
            }

            if (k == Key.D0)
            {
                return "0";
            }

            return string.Empty;
        }

        /// <summary>
        /// Method to convert Boolean to Y or N.
        /// </summary>
        /// <param name="b">The Boolean to convert.</param>
        /// <returns>String conversion of the Boolean input.</returns>
        public static string BoolToYesNoShort(this bool b)
        {
            return b ? Constant.YesShort : Constant.NoShort;
        }

        /// <summary>
        /// Convert a string date to a string date of the format YYYY-MM-DD.
        /// </summary>
        /// <param name="source">The string date to convert.</param>
        /// <returns>The string conversion of the date in the format.</returns>
        public static string ToStringYYYYMMDD(this string source)
        {
            var dtSource = source.ToNullableDate();

            return dtSource.HasValue ? dtSource.Value.ToString("yyyy-MM-dd") : string.Empty;
        }

        /// <summary>
        /// Helper method to read the contents from a file.
        /// </summary>
        /// <param name="fileName">The path of the file.</param>
        /// <returns>The contents of the file as a string.</returns>
        public static string ReadFile(this string fileName)
        {
            if (!string.IsNullOrEmpty(fileName) && File.Exists(fileName))
            {
                return File.ReadAllText(fileName);
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts a nullable decimal to a string.
        /// </summary>
        /// <param name="d">The decimal to convert.</param>
        /// <returns>A string.</returns>
        public static string ToNullableString(this decimal? d)
        {
            if (d.HasValue)
            {
                return d.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts a stream to string.
        /// </summary>
        /// <param name="b">The stream to convert.</param>
        /// <returns>A converted stream to string.</returns>
        public static string ToStringFromStream(this byte[] b)
        {
            try
            {
                return Encoding.ASCII.GetString(b);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Method that pads a nullable integer to the left with c
        /// </summary>
        /// <param name="i">the nullable integer to pad</param>
        /// <param name="length">the length of the string to pad</param>
        /// <param name="c">the character to pad with</param>
        /// <returns>a padded string</returns>
        public static string PadLeft(this int? i, int length, char c)
        {
            if (i.HasValue)
            {
                return i.ToString().PadLeft(length, c);
            }

            return string.Empty;
        }

        /// <summary>
        /// Method that pads a nullable integer to the right with c
        /// </summary>
        /// <param name="i">the nullable integer to pad</param>
        /// <param name="length">the length of the string to pad</param>
        /// <param name="c">the character to pad with</param>
        /// <returns>a padded string</returns>
        public static string PadRight(this int? i, int length, char c)
        {
            if (i.HasValue)
            {
                return i.ToString().PadRight(length, c);
            }

            return string.Empty;
        }

        /// <summary>
        /// Pad an integer to the left with sequence of characters.
        /// </summary>
        /// <param name="i">The integer to pad.</param>
        /// <param name="padElement">The character to pad with.</param>
        /// <param name="length">The number of characters to pad to.</param>
        /// <returns>A string version of the padded integer.</returns>
        public static string PadLeft(this int i, char padElement, int length)
        {
            if (i.ToString().Length < length)
            {
                return i.ToString().PadLeft(length, padElement);
            }

            return i.ToString();
        }

        /// <summary>
        /// Pad an integer to the right with sequence of characters.
        /// </summary>
        /// <param name="i">The integer to pad.</param>
        /// <param name="padElement">The character to pad with.</param>
        /// <param name="length">The number of characters to pad to.</param>
        /// <returns>A string version of the padded integer.</returns>
        public static string PadRight(this int i, char padElement, int length)
        {
            if (i.ToString().Length < length)
            {
                return i.ToString().PadRight(length, padElement);
            }

            return i.ToString();
        }

        /// <summary>
        /// Find a particular element within an xml element.
        /// </summary>
        /// <param name="element">The element to traverse.</param>
        /// <param name="itemToFind">The item to find within the element.</param>
        /// <returns>The value associated with the item searched for.</returns>
        public static string FindInElement(this XElement element, string itemToFind)
        {
            if (element != null)
            {
                if (element.Element(itemToFind) != null)
                {
                    return element.Element(itemToFind).Value;
                }

                if (element.Attribute(itemToFind) != null)
                {
                    return element.Attribute(itemToFind).Value;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Pad an integer to the right with sequence of characters.
        /// </summary>
        /// <param name="i">The integer to pad.</param>
        /// <param name="padElement">The character to pad with.</param>
        /// <param name="length">The number of characters to pad to.</param>
        /// <returns>A string version of the padded integer.</returns>
        public static string PadDecimalRight(this string s, char padElement, int length)
        {
            var decimalPos = s.IndexOf('.');

            if (decimalPos == -1)
            {
                return s;
            }

            var wholeNumber = s.Substring(0, s.Length - (decimalPos));
            var fractions = s.Substring(decimalPos).PadRight(length, padElement);

            return string.Format("{0}{1}", wholeNumber, fractions);
        }

        /// <summary>
        /// Return the first numeric sequence in a string.
        /// </summary>
        /// <param name="s">The string from which the sequence should be extracted.</param>
        /// <returns>The sequence of numeric characters in string.</returns>
        public static string FindNumericSequence(this string s)
        {
            var match = Regex.Match(s, @"[0-9]+", RegexOptions.IgnoreCase);

            return match.Success ? match.Groups[0].Value : string.Empty;
        }

        /// <summary>
        /// Return the first character in a string.
        /// </summary>
        /// <param name="s">The string from which the sequence should be extracted.</param>
        /// <returns>The first character in a string.</returns>
        public static string FindFirstCharacter(this string s)
        {
            var match = Regex.Match(s, @"[a-z]", RegexOptions.IgnoreCase);

            return match.Success ? match.Groups[0].Value : string.Empty;
        }

        /// <summary>
        /// Return the first character in a string.
        /// </summary>
        /// <param name="s">The string from which the sequence should be extracted.</param>
        /// <returns>The first character in a string.</returns>
        public static string ToBinary(this string s)
        {
            var strBinary = string.Empty;
            try
            {
                strBinary = string.Join(string.Empty,
                    s.Select(
                        c => Convert.ToString(Convert.ToInt64(c.ToString(), 16), 2).PadLeft(4, '0')
                    )
                );
            }
            catch (Exception e)
            {
                return string.Empty;
            }

            return strBinary;
        }

        /// <summary>
        /// Removes any non integers from a string.
        /// </summary>
        /// <param name="s">The string from which the non integers are to be removed.</param>
        /// <returns>A string stripped of non integers.</returns>
        public static string RemoveNonIntegers(this string s)
        {
            return Regex.Replace(s, "[^0-9]", "");
        }

        /// <summary>
        /// Removes any non decimals from a string.
        /// </summary>
        /// <param name="s">The string from which the non decimals are to be removed.</param>
        /// <returns>A string stripped of non decimals.</returns>
        public static string RemoveNonDecimals(this string s)
        {
            var result = Regex.Replace(s, "[^0-9.-]", "");
            if (result.Contains("-") && result.ElementAt(0) != '-')
            {
                result = result.Replace("-", "");
            }

            return result;
        }

        /// <summary>
        /// Checks that the folder path is valid on the file system.
        /// </summary>
        /// <param name="path">The path to validate.</param>
        /// <returns>The valid folder path.</returns>
        public static string VerifyDirectoryPath(this string path)
        {
            var fullPath = path.EndsWith("\\") ? path : path + "\\";

            return Directory.Exists(fullPath) ? fullPath : string.Empty;
        }

        /// <summary>
        /// Method that truncates a date in string format, removing the time.
        /// </summary>
        /// <param name="s">the date string</param>
        /// <returns>a truncated date string</returns>
        public static string ToDateOnly(this string s)
        {
            return s.Length >= 10 ? s.Substring(0, 10) : string.Empty;
        }

        #endregion

        #region WindowsState

        /// <summary>
        /// Converts a boolean to a windows state.
        /// </summary>
        /// <param name="b">The boolean to convert.</param>
        /// <returns>A converted windows state.</returns>
        public static WindowState ToWindowsState(this bool b)
        {
            return b ? WindowState.Maximized : WindowState.Normal;
        }

        #endregion

        #region xml

        /// <summary>
        /// Method to convert an XDocument to a XmlDocument.
        /// </summary>
        /// <param name="document">The document to convert.</param>
        /// <returns>An XmlDocument conversion of the XDocument passed.</returns>
        public static XmlDocument ToXmlDocument(this XDocument document)
        {
            var xmlDocument = new XmlDocument();
            using (var xmlReader = document.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }

            return xmlDocument;
        }

        /// <summary>
        /// Method to convert an XmlDocument to a XDocument.
        /// </summary>
        /// <param name="xmlDocument">The document to convert.</param>
        /// <returns>An XDocument conversion of the XmlDocument passed.</returns>
        public static XDocument ToXDocument(this XmlDocument xmlDocument)
        {
            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
        }

        #endregion

        //#region Window

        ///// <summary>
        ///// Returns the current application active window.
        ///// </summary>
        ///// <param name="currentApp">The current application.</param>
        ///// <returns>The current active window.</returns>
        //public static System.Windows.Window GetActiveWindow(this Application currentApp)
        //{
        //    return Application.Current.Windows.Cast<Window>().SingleOrDefault(x => x.IsActive);
        //}

        ///// <summary>
        ///// Returns a determination as to whether the input window is the current application active window..
        ///// </summary>
        ///// <param name="currentApp">The current application.</param>
        ///// <returns>The current active window.</returns>
        //public static bool IsWindowActive(this Application currentApp, Window window)
        //{
        //    var activeWindow = Application.Current.GetActiveWindow();
        //    return activeWindow.Equals(window);
        //}

        //#endregion
    }
}
