﻿// -----------------------------------------------------------------------
// <copyright file="SystemMessage.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.Global
{
    using System.Collections.Generic;
    using AutoSaleOrderProcessor.BusinessLogic;
    using AutoSaleOrderProcessor.Model.BusinessObject;
    using AutoSaleOrderProcessor.Model.Enum;
    using AutoSaleOrderProcessor.ViewModel;

    /// <summary>
    /// Handling class for the system messaging
    /// </summary>
    public static class SystemMessage
    {
        /// <summary>
        /// The data manager reference.
        /// </summary>
        private static readonly DataManager DataManager = DataManager.Instance;

        /// <summary>
        /// Method that writes the message. 
        /// </summary>
        /// <param name="infoType">The message type</param>
        /// <param name="message">The message to write</param>
        public static void Write(MessageType infoType, string message)
        {
            DataManager.WriteMessage(infoType, message);
            ViewModelLocator.SystemMessageStatic.StatusBarInformation = ViewModelLocator.SystemMessageStatic.GetSystemMessages();
        }

        /// <summary>
        /// Find the system messages for the current sessions.
        /// </summary>
        /// <returns>Return a collection of syatem message data.</returns>
        public static IList<Information> GetSystemMessages()
        {
            return DataManager.GetMessages();
        }
    }
}

