﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationMenu.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.Model.BusinessObject
{
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class which models the application menu.
    /// </summary>
    public class ApplicationMenu
    {
        /// <summary>
        /// Gets or sets the display name for the node.
        /// </summary>
        public string TreeNodeText { get; set; }

        /// <summary>
        /// Gets or sets the image to appear.
        /// </summary>
        public BitmapImage DisplayImage { get; set; }

        /// <summary>
        /// Gets or sets the node indent value.
        /// </summary>
        public Thickness Indent { get; set; }

        /// <summary>
        /// Gets or sets the self referential application menu nodes.
        /// </summary>
        public ObservableCollection<ApplicationMenu> ApplicationMenuNodes { get; set; }

        /// <summary>
        /// Gets or sets the 'pressed node' command.
        /// </summary>
        public ICommand Command { get; set; }
    }
}

