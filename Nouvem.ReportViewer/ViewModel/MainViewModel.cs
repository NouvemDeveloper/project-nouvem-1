﻿// -----------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.IO;
    using AutoSaleOrderProcessor.BusinessLogic;
    using AutoSaleOrderProcessor.Global;
    using AutoSaleOrderProcessor.Model.BusinessObject;
    using AutoSaleOrderProcessor.Model.Enum;
    using AutoSaleOrderProcessor.ReportService;
    using AutoSaleOrderProcessor.Properties;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;

    public class MainViewModel : ViewModelBase
    {
        #region field

        /// <summary>
        /// The data manager.
        /// </summary>
        private readonly DataManager dataManager = DataManager.Instance;

        /// <summary>
        /// The report service web service helper object reference.
        /// </summary>
        private ReportingService2010 reportingService = new ReportingService2010();

        /// <summary>
        /// The report names to display.
        /// </summary>
        private ObservableCollection<Report> reports = new ObservableCollection<Report>();

        /// <summary>
        /// The parent folder id.
        /// </summary>
        private int folderId = 1000;

        /// <summary>
        /// The report id.
        /// </summary>
        private int nodeId = 1;

        /// <summary>
        /// The selected report.
        /// </summary>
        private Report selectedReport;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            // Handle the refreshing of the ui.
            this.RefreshCommand = new RelayCommand(this.RefreshCommandExecute);

            // Handle the ui load event.
            this.OnLoadingCommand = new RelayCommand(this.RefreshCommandExecute);

            // Handle the un load event.
            this.OnUnloadedCommand = new RelayCommand(ViewModelLocator.ClearMain);

            #endregion

            this.reportingService.Credentials = System.Net.CredentialCache.DefaultCredentials;
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets or set the reports.
        /// </summary>
        public ObservableCollection<Report> Reports
        {
            get
            {
                return this.reports;
            }

            set
            {
                this.reports = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected report.
        /// </summary>
        public Report SelectedReport
        {
            get
            {
                return this.selectedReport;
            }

            set
            {
                this.selectedReport = value;
                this.RaisePropertyChanged();

                if (value != null && !string.IsNullOrEmpty(value.Path))
                {
                    // send the report path to the view for selection.
                    Messenger.Default.Send(value.Path);
                }
            }
        }
       
        #endregion

        #region command
       
        /// <summary>
        /// Gets the main form load event.
        /// </summary>
        public RelayCommand OnLoadingCommand { get; set; }

        /// <summary>
        /// Gets the main form load event.
        /// </summary>
        public RelayCommand OnUnloadedCommand { get; set; }

        /// <summary>
        /// Gets the command to refresh the ui.
        /// </summary>
        public RelayCommand RefreshCommand { get; set; }

        #endregion

        #region private

        #region command execute

        /// <summary>
        /// Refreshes the ui.
        /// </summary>
        private void RefreshCommandExecute()
        {
            this.Reports = new ObservableCollection<Report>();
            this.reportingService.Url = Settings.Default.WebServiceURL;
            this.SetReportMenu(null, new Report());
            SystemMessage.Write(MessageType.Priority, "Reports refreshed");
        }

        #endregion

        #region helper

        /// <summary>
        /// Recursively populates the report server folders and reports.
        /// </summary>
        /// <param name="folderItems">The report server items to iterate.</param>
        /// <param name="folder">The parent folder.</param>
        private void SetReportMenu(CatalogItem[] folderItems, Report folder)
        {
            try
            {
                if (folderItems == null)
                {
                    folderItems = this.reportingService.ListChildren("/", false);
                }

                foreach (var item in folderItems)
                {
                    // if it's a report parent folder, recursively iterate it's contents.
                    if (item.TypeName.Equals("Folder") && item.Name != "Data Sources" && item.Name != "Datasets")
                    {
                        var localFolder = new Report
                        {
                            NodeID = this.folderId,
                            ParentID = folder.NodeID,
                            Name = item.Name
                        };

                        this.Reports.Add(localFolder);
                        this.folderId++;

                        var localItems = this.reportingService.ListChildren(item.Path, false);
                        this.SetReportMenu(localItems, localFolder);
                    }
                    else if (item.TypeName.Equals("Report"))
                    {
                        var report = new Report
                        {
                            NodeID = this.nodeId,
                            ParentID = folder.NodeID,
                            Name = item.Name,
                            Path = item.Path
                        };

                        this.Reports.Add(report);
                        this.nodeId++;
                    }
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }
       
        #endregion

        #endregion
    }
}

