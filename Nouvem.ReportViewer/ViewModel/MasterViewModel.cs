﻿// -----------------------------------------------------------------------
// <copyright file="MainViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics;

namespace AutoSaleOrderProcessor.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using AutoSaleOrderProcessor.Model.BusinessObject;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    public class MasterViewModel : ViewModelBase
    {
        #region field

        /// <summary>
        /// The currently selected view model reference.
        /// </summary>
        private ViewModelBase currentViewModel;

        /// <summary>
        /// The main menu item reference.
        /// </summary>
        private ApplicationMenu applicationMenu = new ApplicationMenu();
       
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterViewModel"/> class.
        /// </summary>
        public MasterViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            // Handles the command to pass in the settings view.
            this.SelectSettingsCommand = new RelayCommand(() => this.CurrentViewModel = ViewModelLocator.SettingsStatic);

            // Handles the command to pass in the main view.
            this.SelectMainCommand = new RelayCommand(() => this.CurrentViewModel = ViewModelLocator.MainStatic);

            // Handles the command to close the application.
            this.SelectCloseCommand = new RelayCommand(() =>
            {
                Application.Current.Shutdown();
                Process.GetCurrentProcess().Kill();
            });

            #endregion
           
            this.SetMainMenu();
            this.CurrentViewModel = ViewModelLocator.MainStatic;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the application menu collection.
        /// </summary>
        public ApplicationMenu ApplicationMenu
        {
            get
            {
                return this.applicationMenu;
            }

            set
            {
                if (value != null)
                {
                    this.applicationMenu = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the current view model to be displayed in the content area.
        /// </summary>
        public ViewModelBase CurrentViewModel
        {
            get
            {
                return this.currentViewModel;
            }

            set
            {
                if (this.currentViewModel == value)
                {
                    return;
                }
       
                this.currentViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to pass in the settings view.
        /// </summary>
        public RelayCommand SelectSettingsCommand { get; set; }

        /// <summary>
        /// Gets the command to pass in the main view.
        /// </summary>
        public RelayCommand SelectMainCommand { get; set; }

        /// <summary>
        /// Gets the command to close the application.
        /// </summary>
        public RelayCommand SelectCloseCommand { get; set; }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Method that sets the main menu tree.
        /// </summary>
        private void SetMainMenu()
        {
            this.ApplicationMenu = new ApplicationMenu
            {
                ApplicationMenuNodes =
                    new ObservableCollection<ApplicationMenu>
                    {
                        new ApplicationMenu
                        {
                            TreeNodeText = "Settings", 
                            DisplayImage = new BitmapImage(new Uri("pack://application:,,,/Design/Image/Sales.png", UriKind.Absolute)),
                            Command = this.SelectSettingsCommand,
                            Indent = new Thickness(0,0,0,0)
                        },

                        new ApplicationMenu
                        {
                            TreeNodeText = "Reports", 
                            DisplayImage = new BitmapImage(new Uri("pack://application:,,,/Design/Image/Production.png", UriKind.Absolute)),
                            Command = this.SelectMainCommand,
                            Indent = new Thickness(0,0,0,0)
                        },

                        new ApplicationMenu
                        {
                            TreeNodeText = "Close", 
                            DisplayImage = new BitmapImage(new Uri("pack://application:,,,/Design/Image/exit.png", UriKind.Absolute)),
                            Command = this.SelectCloseCommand,
                            Indent = new Thickness(0,0,0,0)
                        }
                    }
            };
        }

        #endregion
    }
}
