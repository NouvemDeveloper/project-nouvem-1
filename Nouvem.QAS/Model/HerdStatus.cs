﻿// -----------------------------------------------------------------------
// <copyright file="HerdStatus.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.QAS.Model
{
    using System;

    /// <summary>
    /// Class which represents the HerdStatus result returned from the WS call.
    /// </summary>
    public class HerdStatus
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HerdStatus"/> class.
        /// </summary>
        protected HerdStatus()
        {
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets or sets a value indicating the Herd No. submitted for checking.
        /// </summary>
        public string HerdNo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the name of the registered herd owner.
        /// </summary>
        public string HerdOwner { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the date of expiry of the producers certificate.
        /// </summary>
        public DateTime? CertValidUntil { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the producer was nominated by your plant.
        /// </summary>
        public bool? NominatedByYourMeatPlant { get; set; }

        /// <summary>
        /// Gets or sets a value indicating any error which has resulted from querying the WS.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets a value indicating any error which has resulted from querying the WS.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the scope of the most recent audit. (beef, lamb, etc)
        /// </summary>
        public string Scope { get; set; }

        /// <summary>
        /// Gets a value indicating whether the current cert is valid.
        /// </summary>
        public bool IsValidCert
        {
            get
            {
                return this.CertValidUntil.HasValue && this.CertValidUntil >= DateTime.Today;
            }
        }

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="HerdStatus"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="HerdStatus"/> object.</returns>
        public static HerdStatus CreateNewHerdStatus()
        {
            return new HerdStatus();
        }

        /// <summary>
        /// Create a new <see cref="HerdStatus"/> with specific parameters.
        /// </summary>
        /// <param name="herdNo">The herd no.</param>
        /// <param name="herdOwner">The owner of the herd.</param>
        /// <param name="certValidUntil">The date of expiry of the producers certificate</param>
        /// <param name="nominatedByYourMeatPlant">Whether or not the producer was nominated by your plant</param>
        /// <param name="error">Any error which has resulted from querying the WS</param>
        /// <param name="scope">The scope of the last check.</param>
        /// <returns>A new <see cref="HerdStatus"/> object with the parameters specified.</returns>
        public static HerdStatus CreateHerdStatus(
            string herdNo,
            string herdOwner,
            DateTime? certValidUntil,
            bool? nominatedByYourMeatPlant,
            string error,
            string scope)
        {
            return new HerdStatus
            {
                HerdNo = herdNo,
                HerdOwner = herdOwner,
                CertValidUntil = certValidUntil,
                NominatedByYourMeatPlant = nominatedByYourMeatPlant,
                Error = error,
                Scope = scope
            };
        }

        #endregion

        #endregion
    }
}

