﻿// -----------------------------------------------------------------------
// <copyright file="WeightDataReceivedArgs.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Indicator
{
    using System;

    /// <summary>
    /// Event arguments class for the weight received event.
    /// </summary>
    public class BufferDataReceivedArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeightDataReceivedArgs "/> class.
        /// </summary>
        /// <param name="bufferData">The weight value to broadcast.</param>
        public BufferDataReceivedArgs(string bufferData)
        {
            this.BufferData = bufferData;
        }

        /// <summary>
        /// Gets or sets the serial data,
        /// </summary>
        public string BufferData { get; set; }
    }
}


