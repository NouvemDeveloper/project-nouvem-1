﻿// -----------------------------------------------------------------------
// <copyright file="Indicator.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO.Ports;
using System.Windows.Threading;

namespace Nouvem.Indicator
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using System.Windows.Forms;
    using Microsoft.VisualBasic;
    using Nouvem.Security;
    using Nouvem.Shared;

    /// <summary>
    /// Handles the application indicator functionality.
    /// </summary>
    public class Indicator : SerialBase
    {
        #region field

        /// <summary>
        /// The buffer to hold the incoming data stream.
        /// </summary>
        private static string DataBuffer;

        /// <summary>
        /// The buffer to hold the incoming data stream.
        /// </summary>
        private static string DataReader;

        /// <summary>
        /// The path the alibi data files are stored.
        /// </summary>
        private readonly string filesPath;

        /// <summary>
        /// The test mode flag.
        /// </summary>
        private readonly bool testMode;

        /// <summary>
        /// Ignores any validation checks.
        /// </summary>
        private bool ignoreValidation;

        /// <summary>
        /// The checksum path.
        /// </summary>
        private readonly string checksumFilePath;

        /// <summary>
        /// The indicator type.
        /// </summary>
        private readonly IndicatorModel indicator;

        /// <summary>
        /// Collection holder, for the incoming parsed weights.
        /// </summary>
        private readonly IList<string> weights;

        /// <summary>
        /// The scales weight
        /// </summary>
        private string weight;

        /// <summary>
        /// Falg, as to whether the weight has dropped below the drop below value;
        /// </summary>
        private bool weightDroppedBelow = true;

        /// <summary>
        /// The weight mode.
        /// </summary>
        private decimal tare;

        /// <summary>
        /// The recorded scales weight
        /// </summary>
        private string recordedWeight;

        /// <summary>
        /// Holds the value as to whether there has been movement on the scales since the last saved weight.
        /// </summary>
        private bool movementOnScales;

        /// <summary>
        /// Holds the value as to whether we are using stable reads/tolerance for motion, as opposed to the motion flag on the string.
        /// </summary>
        private bool usingStableReadsForMotion;

        /// <summary>
        /// Holds the value as to whether we are ignoring the no motion warning.
        /// </summary>
        private bool ignoreNoMotionOnScalesWarning;

        /// <summary>
        /// The scales in motion flag.
        /// </summary>
        private bool scalesInMotion;

        /// <summary>
        /// The drop below value.
        /// </summary>
        private decimal? dropBelow;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Indicator"/> 
        /// </summary>
        public Indicator()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Indicator"/> class with arguments.
        /// </summary>
        /// <param name="port">The name of the com port.</param>
        /// <param name="baudRate">The baud rate setting.</param>
        /// <param name="parity">The parity setting.</param>
        /// <param name="stopBits">The stop bits value.</param>
        /// <param name="dataBits">The data bits value.</param>
        /// <param name="indicator">The connected indicator type.</param>
        /// <param name="filePath">The path the indicator data files are stored.</param>
        /// <param name="scalesDivision">The scales division value.</param>
        /// <param name="stableReads">The required stable reads value.</param>
        /// <param name="minWeightDivisions">The minimum weight allowed in scale divisions.</param>
        /// <param name="checksumFilePath">The check sum file path.</param>
        /// <param name="waitTime">The buffer read wait time.</param>
        /// <param name="tolerance">The stable read tolerance level.</param>
        /// <param name="usingStableReadsForMotion">Flag, as to whether we are using stable reads/tolerance for motion.</param>
        /// <param name="ignoreNoMotionOnScalesWarning">Flag, as to whether we are ignoring no motion.</param>
        /// <param name="disturbBy">The value the scales must move by in order for a weight to be recorded.</param>
        /// <param name="dropBelow">The value the scales must drop below in order for a weight to be recorded.</param>
        public Indicator(
            string port,
            string baudRate,
            string parity,
            string stopBits,
            string dataBits,
            IndicatorModel indicator,
            string filePath,
            decimal scalesDivision = 0.01m,
            int stableReads = 3,
            int minWeightDivisions = 0,
            string checksumFilePath = @"C:\Nouvem\Indicator\Checksum\Checksum.xml",
            int waitTime = 0,
            decimal tolerance = 0,
            bool usingStableReadsForMotion = false,
            bool ignoreNoMotionOnScalesWarning = false,
            decimal disturbBy = 0.1M,
            decimal? dropBelow = null, 
            bool verifyChecksum = false,
            bool testMode = false)
            : base(port, baudRate, parity, stopBits, dataBits)
        {
            this.filesPath = filePath;
            this.weights = new List<string>();
            this.indicator = indicator;
            this.StableReads = stableReads;
            this.MinimumWeight = minWeightDivisions * scalesDivision;
            this.Rounding = scalesDivision.DecimalPrecision();
            this.GetLastAlibiNumber();
            this.ThreadWaitTime = 0;
            this.checksumFilePath = checksumFilePath;
            this.Tolerance = tolerance;
            this.usingStableReadsForMotion = usingStableReadsForMotion;
            this.ignoreNoMotionOnScalesWarning = ignoreNoMotionOnScalesWarning;
            this.DisturbBy = disturbBy;
            this.dropBelow = dropBelow;
            this.testMode = testMode;

            if (this.indicator == IndicatorModel.DEM_GSE355
                || this.indicator == IndicatorModel.DINI_DFWLID
                || this.indicator == IndicatorModel.DEM_R320
                || this.indicator == IndicatorModel.CSW20
                || this.indicator == IndicatorModel.GSE465)
            {
                this.ThreadWaitTime = waitTime;
            }

            if (verifyChecksum)
            {
                this.VerifyChecksum();
            }
        }

        #endregion

        #region event

        /// <summary>
        /// Event used to pass on the incoming serial data.
        /// </summary>
        public event EventHandler<WeightDataReceivedArgs> WeightDataReceived;

        /// <summary>
        /// Event used to pass on the incoming buffer data.
        /// </summary>
        public event EventHandler<BufferDataReceivedArgs> BufferDataReceived;

        #endregion

        #region enum

        /// <summary>
        /// Enum that models the indicator types.
        /// </summary>
        public enum IndicatorModel
        {
            /// <summary>
            /// CSW20 indicator type
            /// </summary>
            CSW20,

            /// <summary>
            /// Gse355 type.
            /// </summary>
            GSE355,

            /// <summary>
            /// Gse355 type.
            /// </summary>
            GSE355_WithMotion,

            /// <summary>
            /// Gse465 type.
            /// </summary>
            GSE465,

            /// <summary>
            /// THe systec it1000 type.
            /// </summary>
            SYSTEC_IT1000,

            /// <summary>
            /// DEM_Gse355 type.
            /// </summary>
            DEM_GSE355,

            /// <summary>
            /// DEM_R320 type.
            /// </summary>
            DEM_R320,

            /// <summary>
            /// GENERIC type.
            /// </summary>
            GENERIC,

            /// <summary>
            /// 
            /// </summary>
            DINI_DFWLID,

            TEST
        }

        #endregion

        #region public interface

        #region properties

        /// <summary>
        /// Gets or sets a value indicating whether the minimum weight value is to be ignored.
        /// </summary>
        public bool IgnoreMinimumWeight { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the scales disturb by value is to be ignored.
        /// </summary>
        public bool IgnoreDisturbBy { get; set; }

        /// <summary>
        /// The tolerance division count.
        /// </summary>
        public decimal Tolerance { get; set; }

        /// <summary>
        /// The value the scales must move by in order for a weight to be recorded.
        /// </summary>
        public decimal DisturbBy { get; set; }

        /// <summary>
        /// The string terminating character.
        /// </summary>
        public char TerminatingCharacter { get; set; }

        /// <summary>
        /// The length of the data string to read.
        /// </summary>
        public int StringToReadLength { get; set; }

        /// <summary>
        /// The weight start position.
        /// </summary>
        public int WeightStartPos { get; set; }

        /// <summary>
        /// The weight string length.
        /// </summary>
        public int WeightLength { get; set; }

        /// <summary>
        /// The motion start position.
        /// </summary>
        public int MotionStartPos { get; set; }

        /// <summary>
        /// The motion string length.
        /// </summary>
        public int MotionLength { get; set; }

        /// <summary>
        /// The motion charcater.
        /// </summary>
        public string MotionString { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are auto weighing.
        /// </summary>
        public bool AutoWeigh { get; set; }

        /// <summary>
        /// Gets the incoming weight
        /// </summary>
        public string Weight
        {
            get
            {
                return this.weight;
            }

            private set
            {
                var decimalValue = value.ToDecimal();
                this.weight = Math.Round(decimalValue - this.Tare, this.Rounding).ToString();
                if (this.dropBelow.HasValue)
                {
                    if (decimalValue < this.dropBelow)
                    {
                        this.weightDroppedBelow = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the current recorded weight
        /// </summary>
        public string RecordedWeight
        {
            get
            {
                return this.recordedWeight;
            }

            private set
            {
                this.recordedWeight = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current weight is stable, or not.
        /// </summary>
        public bool IsWeightStable { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the scales are in motion, or not.
        /// </summary>
        public bool ScalesInMotion
        {
            get
            {
                return this.scalesInMotion;
            }

            private set
            {
                if (value)
                {
                    // once there has been any movement on the scales, the weight can be saved.
                    if (this.RecordedWeight != null && this.Weight != "0" && this.RecordedWeight.ToDecimal().GetDifference(this.Weight.ToDecimal()) >= this.DisturbBy)
                    {
                        this.movementOnScales = true;
                        if (!this.usingStableReadsForMotion)
                        {
                            this.weights.Clear();
                        }
                    }
                }

                this.scalesInMotion = value;
            }
        }

        /// <summary>
        /// Gets or sets the weight mode (net or gross).
        /// </summary>
        public string WeightMode { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the weight has dropped below the drop by weight.
        /// </summary>
        public bool HasWeightDroppedBelow
        {
            get
            {
                if (!this.dropBelow.HasValue)
                {
                    return true;
                }

                return this.weightDroppedBelow;
            }
        }

        /// <summary>
        /// Gets or sets the tare weight.
        /// </summary>
        public decimal Tare
        {
            get
            {
                return this.tare;
            }

            set
            {
                this.tare = value;
                this.WeightMode = value > 0 ? "NET" : "GROSS";
            }
        }

        /// <summary>
        /// Gets or sets the rounding.
        /// </summary>
        public int Rounding { get; set; }

        /// <summary>
        /// Gets the alibi number attached to each saved weight.
        /// </summary>
        public long Alibi { get; private set; }

        /// <summary>
        /// Gets or sets the stable reads required.
        /// </summary>
        public int StableReads { get; set; }

        /// <summary>
        /// Gets or sets the minimum weight required before a weight can be saved
        /// </summary>
        public decimal MinimumWeight { get; set; }

        /// <summary>
        /// Gets or sets the strinns with no motion variance level
        /// </summary>
        public int NoMotionVariance { get; set; }

        /// <summary>
        /// Gets the certification checksum value.
        /// </summary>
        public string CertifiedChecksum { get; private set; }

        /// <summary>
        /// Gets the current application checksum.
        /// </summary>
        public string CurrentChecksum { get; private set; }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Gets the total.
        /// </summary>
        public decimal Total { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Sets the attributes to ignore if dealing with qty based products.
        /// </summary>
        /// <param name="ignore">The ignore or not flag.</param>
        public void IgnoreValidation(bool ignore)
        {
            this.ignoreValidation = ignore;
        }

        /// <summary>
        /// Sets the data buffer weight string variables.
        /// </summary>
        /// <param name="stringToReadLength">The length of the string to read</param>
        /// <param name="weightStartPos">The weight start pos.</param>
        /// <param name="weightLength">The length of the weight</param>
        /// <param name="motionStartPos">The motion start position.</param>
        /// <param name="motionLength">The motion length.</param>
        /// <param name="motionString">The motion string.</param>
        /// <param name="terminatingChar">The terminating character.</param>
        public void SetPortDataVariables(int stringToReadLength, int weightStartPos, int weightLength, int motionStartPos, int motionLength, string motionString, char terminatingChar)
        {
            this.StringToReadLength = stringToReadLength;
            this.WeightStartPos = weightStartPos;
            this.WeightLength = weightLength;
            this.MotionStartPos = motionStartPos;
            this.MotionLength = motionLength;
            this.MotionString = motionString;
            this.TerminatingCharacter = terminatingChar;
        }

        /// <summary>
        /// Calculates the total.
        /// </summary>
        /// <param name="unitPrice">The unit price to use.</param>
        /// <returns>A calculated total.</returns>
        public decimal CalculateTotal(decimal unitPrice)
        {
            this.UnitPrice = unitPrice;
            this.Total = decimal.Round(this.Weight.ToDecimal() * unitPrice, this.Rounding);
            return this.Total;
        }

        /// <summary>
        /// Method that validates a weight, and if successfully validated then it saves it to file.
        /// </summary>
        /// <returns>a flag indicating a successfully validated, and saved weight.</returns>
        public string SaveWeightData(bool autoWeigh = false)
        {
            this.AutoWeigh = autoWeigh;

            // increment the alibi number.
            this.Alibi++;

            #region validation

            var validationError = this.ValidateWeight();

            if (validationError != string.Empty)
            {
                // weight validation failure
                return validationError;
            }

            #endregion

            this.RecordedWeight = this.Weight;
            this.movementOnScales = false;
            this.weightDroppedBelow = false;

            // weight validated, so we save.
            var worker = new BackgroundWorker();
            worker.DoWork += (sender, args) => this.SaveData();
            worker.RunWorkerAsync();

            return string.Empty;
        }

        /// <summary>
        /// Sets the attributes to ignore if dealing with qty based products.
        /// </summary>
        /// <param name="ignore">The ignore or not flag.</param>
        public void SetQtyBasedProductValues(bool ignore)
        {
            this.IgnoreDisturbBy = ignore;
            this.IgnoreMinimumWeight = ignore;
        }

        /// <summary>
        /// Gets the alibi file data corresponding to the input alibi number.
        /// </summary>
        /// <param name="alibiNo">The alibi number to search for.</param>
        /// <returns>The alibi data.</returns>
        public AlibiWeightInfo GetAlibiData(long alibiNo)
        {
            var alibiFile = new XDocument();
            var alibiData = new AlibiWeightInfo();

            if (!Directory.Exists(this.filesPath))
            {
                return null; //throw new DirectoryNotFoundException(Properties.Message.InvalidFilePath);
            }

            try
            {
                Directory.EnumerateFiles(this.filesPath).ToList().ForEach(file =>
                {
                    var filePath = Path.Combine(this.filesPath, file);

                    Security.DecryptXmlFile(ref alibiFile, filePath);

                    // if the data file contains the alibi number then we retrieve its elements.
                    var localAlibiData = alibiFile.Element("root").Elements("alibi")
                        .FirstOrDefault(x => x.FirstAttribute.Value.ToInt() == alibiNo);

                    if (localAlibiData != null)
                    {
                        alibiData.Alibi = localAlibiData.FirstAttribute.Value.ToLong();
                        alibiData.Date = localAlibiData.Element("date").Value;
                        alibiData.Time = localAlibiData.Element("time").Value;
                        alibiData.NetWeight = localAlibiData.Element("weight").Value.ToDecimal();
                        alibiData.Tare = localAlibiData.Element("tare").Value.ToDecimal();
                        alibiData.UnitPrice = localAlibiData.Element("unitprice").Value.ToDecimal();
                        alibiData.Total = localAlibiData.Element("total").Value.ToDecimal();
                    }
                });
            }
            catch (Exception e)
            {
                
            }

            return alibiData;
        }

        /// <summary>
        /// Retrieves all the available port names.
        /// </summary>
        /// <returns>A collection of available port names.</returns>
        public string[] RetrievePorts()
        {
            return System.IO.Ports.SerialPort.GetPortNames();
        }

        /// <summary>
        /// Method that searches for data corresponding to an alibi number,
        /// or data corresponding to a range of alibi numbers. 
        /// </summary>
        /// <param name="alibiNumber">the record to search for, or the start record to search for when searching for a range of records.</param>
        /// <param name="endAlibiNumber">the end record to search for when searching for a range of records</param>
        /// <returns>a collection of alibi records</returns>
        public IList<AlibiData> RetrieveData(long alibiNumber, long endAlibiNumber = 0)
        {
            var data = new List<AlibiData>();
            string dataString = string.Empty;
            var xmlDocument = new XDocument();

            try
            {
                if (Directory.Exists(this.filesPath))
                {
                    // search the data files.
                    foreach (var file in Directory.EnumerateFiles(this.filesPath))
                    {
                        var filePath = Path.Combine(this.filesPath, file);

                        // Decrypt and load the file.
                        Security.DecryptXmlFile(ref xmlDocument, filePath);

                        if (xmlDocument != null)
                        {
                            // We are only searching for 1 record.
                            if (endAlibiNumber == 0)
                            {
                                // if the data file contains the alibi number then we retrieve its elements.
                                var alibiDataRecord = xmlDocument.Element("root").Elements("alibi")
                                    .Where(x => x.FirstAttribute.Value.ToInt() == alibiNumber)
                                    .Select(x => new AlibiData
                                    {
                                        Alibi = x.FirstAttribute.Value.ToLong(),
                                        Date = x.Element("date").Value,
                                        Time = x.Element("time").Value,
                                        Weight = x.Element("weight").Value.ToDecimal(),
                                        Tare = x.Element("tare").Value.ToDecimal(),
                                        UnitPrice = x.Element("unitprice").Value.ToDecimal(),
                                        Total = x.Element("total").Value.ToDecimal()
                                    })
                                    .FirstOrDefault();

                                // if the alibi record has been found then add it to our collection and exit.
                                if (alibiDataRecord.Alibi > 0)
                                {
                                    data.Add(alibiDataRecord);
                                    break;
                                }
                            }
                            else
                            {
                                // We are searching for a range of records.
                                var localData = xmlDocument.Element("root").Elements("alibi")
                                    .Where(
                                        x =>
                                            x.FirstAttribute.Value.ToInt() >= alibiNumber &&
                                            x.FirstAttribute.Value.ToInt() <= endAlibiNumber)
                                    .Select(x => new AlibiData
                                    {
                                        Alibi = x.FirstAttribute.Value.ToLong(),
                                        Date = x.Element("date").Value,
                                        Time = x.Element("time").Value,
                                        Weight = x.Element("weight").Value.ToDecimal(),
                                        Tare = x.Element("tare").Value.ToDecimal(),
                                        UnitPrice = x.Element("unitprice").Value.ToDecimal(),
                                        Total = x.Element("total").Value.ToDecimal()
                                    }).ToList();

                                // add our records to our collection
                                if (localData.Any())
                                {
                                    localData.ForEach(record => data.Add(record));
                                }

                                // Encrypt and save the file.
                                Security.EncryptXmlFile(xmlDocument, filePath);
                            }
                        }
                    }
                }
                else
                {
                    throw new DirectoryNotFoundException("Directory not found");
                }
            }
            catch (Exception)
            {
                throw;
            }

            // order the data by alibi number
            var orderedData = data.OrderBy(x => x.Alibi);

            return orderedData.ToList();
        }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the incoming serial data reader, and adds indicator string parsing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected override void SerialPortOnDataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            base.SerialPortOnDataReceived(sender, e);
            this.ReadSerialData();
        }

        ///// <summary>
        ///// Catches the pin change events.
        ///// </summary>
        ///// <param name="sender">The sender.</param>
        ///// <param name="e">The event args.</param>
        //protected override void SerialPortOnPinChanged(object sender, SerialPinChangedEventArgs e)
        //{
        //    base.SerialPortOnPinChanged(sender, e);

        //    if ((e.EventType == SerialPinChange.CtsChanged || e.EventType == SerialPinChange.DsrChanged) && !this.IsPortOpen)
        //    {

        //    }
        //}

        /// <summary>
        /// Reads, and parses, the incoming serial data string.
        /// </summary>
        protected override void ReadSerialData()
        {
            int pos;
            int startPos;
            int endPos;
            int stringLength;
            int weightLength;
            int weightStartPos;
            int statusStartPos;
            int statusLength;
            string status;
            var fullDataString = string.Empty;

            char stx = Strings.Chr(02);
            char etx = Strings.Chr(03);
            char carriageReturn = Strings.Chr(13);

            try
            {
                // append the incoming port data to our buffer.

                if (this.testMode)
                {
                    this.OnBufferDataReceived(new BufferDataReceivedArgs(this.PortData));
                }

                DataBuffer += this.PortData;

                // TODO add indicator strings as we get them.
                switch (this.indicator)
                {
                    case IndicatorModel.TEST:

                        this.Weight =
                            DataReader.RemoveNonDecimals();
                        this.ScalesInMotion =
                            DataReader.Contains("M");
                     
                        this.OnWeightDataReceived(new WeightDataReceivedArgs(this.Weight, true, this.ScalesInMotion));
                        DataBuffer = string.Empty;

                        return;

                    case IndicatorModel.CSW20:

                        // String 2:
                        stringLength = 16;
                        weightLength = 8;
                        weightStartPos = 5;
                        statusStartPos = 1;
                        statusLength = 1;
                        
                        if (DataBuffer.Contains(carriageReturn))
                        {
                            // get the position of the last occurance of carriage return in the string buffer
                            startPos = Strings.InStrRev(DataBuffer, carriageReturn.ToString());

                            if (startPos > stringLength)
                            {
                                // get the full reading(weight,status,whitespace,nulls etc.)
                                fullDataString = DataBuffer.Substring((startPos - 1) - stringLength, stringLength);

                                // get the weight
                                this.Weight = fullDataString.Substring(weightStartPos, weightLength).Replace(" ", string.Empty);

                                // get the status
                                status = fullDataString.Substring(statusStartPos, statusLength);

                                this.ScalesInMotion = status.ToUpper() == "M";

                                // move into the end of the buffer, clearing it as we go
                                DataBuffer = DataBuffer.Substring(this.PortData.Length, DataBuffer.Length - this.PortData.Length);
                            }
                        }

                        break;

                    case IndicatorModel.DINI_DFWLID:
                        const string motion = "US";
                        endPos = Strings.InStrRev(DataBuffer, carriageReturn.ToString());
                        if (endPos > 0 && endPos > 15)
                        {
                            // find previous occurrence of stx
                            startPos = endPos - 18;
                            var fullString = DataBuffer.Substring(startPos, 14);

                            this.Weight = fullString.Substring(6, fullString.Length - 6).Trim();
                            this.ScalesInMotion = fullString.StartsWith(motion);

                            if (DataBuffer.Length > 30)
                            {
                                // move into the end of the buffer, clearing it as we go
                                DataBuffer = DataBuffer.Substring(DataBuffer.Length - 30, 30);
                            }
                        }

                        break;

                    case IndicatorModel.SYSTEC_IT1000:
                        /* Protocol
                         * 15 ascii charcaters + ctlf - 13th character always space - S = Stable/SD = Motion
                         * SD     10.88 kg        
                         * S     110.88 kg                         
                         */

                        endPos = Strings.InStrRev(DataBuffer, "kg");

                        if (endPos > 0)
                        {
                            // find previous occurrence of stx
                            fullDataString = DataBuffer.Substring(endPos - 14, 13);
                            this.ScalesInMotion = fullDataString.Substring(0, 2) == "SD";
                            this.Weight = fullDataString.Substring(3, 10).Trim();
                        }

                        if (DataBuffer.Length == 256)
                        {
                            // move into the end of the buffer, clearing it as we go
                            DataBuffer = DataBuffer = string.Empty;
                        }

                        break;

                    case IndicatorModel.GSE355:
                        //endPos = Strings.InStrRev(DataBuffer, stx.ToString());

                        //if (endPos > 0)
                        //{
                        //    // find previous occurrence of stx
                        //    startPos = Strings.InStrRev(DataBuffer, stx.ToString(), endPos - 1);

                        //    if (startPos > 0 && endPos > 0)
                        //    {
                        //        this.Weight = DataBuffer.Substring(0, 9).Trim();
                        //        this.ScalesInMotion = false;

                        //        DataBuffer = string.Empty;
                        //    }
                        //}

                        //break;

                        this.WeightStartPos = 5;
                        this.WeightLength = 7;
                        this.MotionLength = 1;
                        this.MotionStartPos = 8;
                        this.MotionString = "";
                        this.TerminatingCharacter = (char)13;
                        this.StringToReadLength = 13;

                        var loDataBuffer = DataBuffer;

                        for (int i = 0; i < DataBuffer.Length; i++)
                        {
                            var bufferChar = DataBuffer.ElementAt(i);
                            DataReader += bufferChar;
                            if (bufferChar == this.TerminatingCharacter && DataReader.Length >= this.StringToReadLength)
                            {
                                DataReader = DataReader.Substring(DataReader.Length - this.StringToReadLength,
                                    this.StringToReadLength);

                                this.Weight =
                                    DataReader.Substring(this.WeightStartPos - 1, this.WeightLength).RemoveNonDecimals();

                                this.ScalesInMotion =
                                    DataReader.Substring(this.MotionStartPos - 1, this.MotionLength)
                                        .Equals(this.MotionString);

                                DataReader = string.Empty;

                                this.IsWeightStable = this.CheckForStableReads();

                                if (this.usingStableReadsForMotion)
                                {
                                    this.ScalesInMotion = !this.IsWeightStable;
                                }

                                if (this.Weight != null)
                                {
                                    // Broadcast our weight data to all interested subscribers.
                                    this.OnWeightDataReceived(new WeightDataReceivedArgs(this.Weight, this.IsWeightStable, this.ScalesInMotion));
                                }
                            }
                            else
                            {
                                loDataBuffer = DataReader;
                            }
                        }

                        DataBuffer = loDataBuffer;
                        return;

                    case IndicatorModel.GSE355_WithMotion:
                        //endPos = Strings.InStrRev(DataBuffer, stx.ToString());

                        //if (endPos > 0)
                        //{
                        //    // find previous occurrence of stx
                        //    startPos = Strings.InStrRev(DataBuffer, stx.ToString(), endPos - 1);

                        //    if (startPos > 0 && endPos > 0)
                        //    {
                        //        this.Weight = DataBuffer.Substring(0, 9).Trim();
                        //        this.ScalesInMotion = false;

                        //        DataBuffer = string.Empty;
                        //    }
                        //}

                        //break;

                        //this.WeightStartPos = 5;
                        //this.WeightLength = 7;
                        //this.MotionLength = 1;
                        //this.MotionStartPos = 8;
                        //this.MotionString = "";
                        this.TerminatingCharacter = (char)13;
                        //this.StringToReadLength = 13;

                        pos = Strings.InStrRev(DataBuffer, this.TerminatingCharacter.ToString());
                        if (pos > this.StringToReadLength)
                        {
                            DataReader = DataBuffer.Substring(pos - (this.StringToReadLength + 1), this.StringToReadLength + 1);

                            //this.Weight =
                            //    DataReader.Substring(this.WeightStartPos - 1, this.WeightLength).RemoveNonDecimals();

                            //this.ScalesInMotion =
                            //    DataReader.Substring(this.MotionStartPos - 1, this.MotionLength)
                            //        .Contains(this.MotionString);
                            this.Weight =
                               DataReader.RemoveNonDecimals();

                            this.ScalesInMotion =
                                DataReader.Contains(this.MotionString);

                            DataReader = string.Empty;

                            this.IsWeightStable = this.CheckForStableReads();

                            if (this.usingStableReadsForMotion)
                            {
                                this.ScalesInMotion = !this.IsWeightStable;
                            }

                            if (this.Weight != null)
                            {
                                // Broadcast our weight data to all interested subscribers.
                                this.OnWeightDataReceived(new WeightDataReceivedArgs(this.Weight, this.IsWeightStable, this.ScalesInMotion));
                                DataBuffer = string.Empty;
                            }
                        }

                        return;

                    case IndicatorModel.DEM_GSE355:

                        stringLength = 12;
                        weightLength = 7;
                        weightStartPos = 6;
                        statusStartPos = 1;
                        statusLength = 1;

                        // motion is 'D' on this string
                        const string demMotion = "M";

                        //endPos = Strings.InStr(DataBuffer, stx.ToString());
                        startPos = Strings.InStr(DataBuffer, stx.ToString());

                        //if (startPos < 0 || DataBuffer.Length < startPos + stringLength)
                        //{
                        //    return;
                        //}
                        if (DataBuffer.Length < startPos + 20)
                        {
                            DataBuffer = string.Empty;
                            return;
                        }

                        fullDataString = DataBuffer.Substring(startPos, 20).Trim();
                        this.ScalesInMotion = fullDataString.Contains(demMotion);

                        var wgtStringEndPos = Strings.InStr(fullDataString, " ");
                        var localWgt = fullDataString.Substring(0, wgtStringEndPos);

                        this.Weight = localWgt.Replace("+", "");

                        //this.Weight = localData.Replace("G", "");

                        //DataBuffer = string.Empty;
                        if (DataBuffer.Length > 30)
                        {
                            // move into the end of the buffer, clearing it as we go
                            DataBuffer = DataBuffer.Substring(DataBuffer.Length - 30, 30);
                        }

                        break;

                    case IndicatorModel.GSE465:

                        //******************************************//

                        // R320
                        //this.WeightStartPos = 1;
                        //this.WeightLength = 8;
                        //this.MotionLength = 1;
                        //this.MotionStartPos = 10;
                        //this.MotionString = "M";
                        //this.TerminatingCharacter = (char)3;
                        //this.StringToReadLength = 16;

                        //*******************************************//

                        // GSE465
                        this.WeightStartPos = 3;
                        this.WeightLength = 6;
                        this.MotionLength = 1;
                        this.MotionStartPos = 8;
                        this.MotionString = "";
                        this.TerminatingCharacter = (char)13;
                        this.StringToReadLength = 27;

                        //*******************************************//

                        var locDataBuffer = DataBuffer;

                        for (int i = 0; i < DataBuffer.Length; i++)
                        {
                            var bufferChar = DataBuffer.ElementAt(i);
                            DataReader += bufferChar;
                            if (bufferChar == this.TerminatingCharacter && DataReader.Length >= this.StringToReadLength)
                            {
                                DataReader = DataReader.Substring(DataReader.Length - this.StringToReadLength,
                                    this.StringToReadLength);

                                this.Weight =
                                    DataReader.Substring(this.WeightStartPos - 1, this.WeightLength).RemoveNonDecimals();

                                this.ScalesInMotion =
                                    DataReader.Substring(this.MotionStartPos - 1, this.MotionLength)
                                        .Equals(this.MotionString);

                                DataReader = string.Empty;

                                this.IsWeightStable = this.CheckForStableReads();

                                if (this.usingStableReadsForMotion)
                                {
                                    this.ScalesInMotion = !this.IsWeightStable;
                                }

                                if (this.Weight != null)
                                {
                                    // Broadcast our weight data to all interested subscribers.
                                    this.OnWeightDataReceived(new WeightDataReceivedArgs(this.Weight, this.IsWeightStable, this.ScalesInMotion));
                                }
                            }
                            else
                            {
                                locDataBuffer = DataReader;
                            }
                        }

                        DataBuffer = locDataBuffer;
                        return;

                        //fullDataString = string.Empty;
                        //weightLength = 8;
                        //weightStartPos = 2;
                        ////  7.60.0 01000101100
                        ////  13.80.0 01000101100
                        //if (DataBuffer.Contains(carriageReturn))
                        //{
                        //    endPos = Strings.InStrRev(DataBuffer, " ");
                        //    fullDataString = DataBuffer.Substring(0, endPos - 1).Trim();

                        //    if (fullDataString.Length > 7)
                        //    {
                        //        fullDataString = fullDataString.Substring(fullDataString.Length - 7, 5).Trim();
                        //        var count = fullDataString.Count(x => x == '.');
                        //        if (count == 2)
                        //        {
                        //            int place = fullDataString.LastIndexOf(".");
                        //            fullDataString = fullDataString.Remove(place, 1);
                        //        }

                        //        if (fullDataString.IsNumeric() && !fullDataString.StartsWith("01"))
                        //        {

                        //            // there is no motion on this string.
                        //            this.Weight = fullDataString;
                        //            this.ScalesInMotion = false;
                        //        }
                        //    }                           
                        //}

                        //// move into the end of the buffer, clearing it as we go
                        //if (DataBuffer.Length > 0)
                        //{
                        //    DataBuffer = DataBuffer.Substring((DataBuffer.Length / 2), DataBuffer.Length - (DataBuffer.Length / 2));
                        //}

                        break;

                    case IndicatorModel.DEM_R320:

                        stringLength = 12;
                        weightLength = 7;
                        weightStartPos = 6;
                        statusStartPos = 1;
                        statusLength = 1;

                        // motion is 'D' on this string
                        const string systecMotion = "M";

                        //endPos = Strings.InStr(DataBuffer, stx.ToString());
                        startPos = Strings.InStr(DataBuffer, stx.ToString());

                        //if (startPos < 0 || DataBuffer.Length < startPos + stringLength)
                        //{
                        //    return;
                        //}                        

                        fullDataString = DataBuffer.Substring(startPos, 10).Trim();
                        this.ScalesInMotion = fullDataString.Contains(systecMotion);

                        var localData = fullDataString.Replace("M", "");
                        this.Weight = localData.Replace("G", "");

                        if (DataBuffer.Length > 30)
                        {
                            // move into the end of the buffer, clearing it as we go
                            DataBuffer = DataBuffer.Substring(DataBuffer.Length - 30, 30);
                        }

                        break;

                    case IndicatorModel.GENERIC:

                        //******************************************//

                        // R320
                        //this.WeightStartPos = 1;
                        //this.WeightLength = 8;
                        //this.MotionLength = 1;
                        //this.MotionStartPos = 10;
                        //this.MotionString = "M";
                        //this.TerminatingCharacter = (char)3;
                        //this.StringToReadLength = 16;

                        //*******************************************//

                        // GSE465
                        //this.WeightStartPos = 4;
                        //this.WeightLength = 4;
                        //this.MotionLength = 1;
                        //this.MotionStartPos = 8;
                        //this.MotionString = "";
                        //this.TerminatingCharacter = (char)13;
                        //this.StringToReadLength = 27;

                        //*******************************************//

                        //*****************************************//
                        // CSW 20
                        //this.WeightStartPos = 4;
                        //this.WeightLength = 4;
                        //this.MotionLength = 1;
                        //this.MotionStartPos = 8;
                        //this.MotionString = "M";
                        //this.TerminatingCharacter = (char)13;
                        //this.StringToReadLength = 16;

                        //DataBuffer = "SG? B     1.6 kg";
                        //*******************************************//

                        // System1000
                        // SD      43.9 kg
                        //this.WeightStartPos = 7;
                        //this.WeightLength = 6;
                        //this.MotionLength = 1;
                        //this.MotionStartPos = 2;
                        //this.MotionString = "D";
                        //this.TerminatingCharacter = (char)71;
                        //this.StringToReadLength = 12;

                        //// jadever jik 8
                        //this.WeightStartPos = 1;
                        //this.WeightLength = 15;
                        ////this.MotionLength = 1;
                        ////this.MotionStartPos = 2;
                        //this.MotionString = "U";
                        //this.TerminatingCharacter = (char)107;
                        //this.StringToReadLength = 15;

                        //*******************************************//

                        // DataBuffer = "   +10.0 kg Gross  S0";
                        //var a = "     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z\r\n  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0\r\n.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg\r\n    0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z\r\nkg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0\r\nG Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg\r\n  0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg\r\n     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G\r\nZ  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg\r\n0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg\r\n     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z\r\n kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.\r\n0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg\r\n   0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  k\r\ng     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G\r\n Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg\r\n 0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg\r\n     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z  kg     0.0G Z\r\n  kg     0.0G Z  kg     0.0G Z  kg";

                        //DataBuffer = "     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z\r\n  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     0\r\n.0G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg\r\n    2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z\r\nkg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4\r\nG Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg\r\n  2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg\r\n     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G\r\nZ  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg\r\n2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg\r\n     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z\r\n kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     0.\r\n0G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg\r\n   2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  k\r\ng     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G\r\n Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg\r\n 2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg\r\n     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z  kg     2.4G Z\r\n  kg     2.4G Z  kg     2.4G Z  kg";
                        //DataBuffer = "US/GS +  101.1 kg"; //ST/GS +    0.0 kg US/GS +  101.1 kg US/GS +  101.1 kg 


                        pos = Strings.InStrRev(DataBuffer, this.TerminatingCharacter.ToString());
                        if (pos > this.StringToReadLength)
                        {
                            DataReader = DataBuffer.Substring(pos - (this.StringToReadLength + 1), this.StringToReadLength + 1);
                            if (this.WeightStartPos > 0)
                            {
                                this.Weight = DataReader.Substring(this.WeightStartPos - 1,this.WeightLength).RemoveNonDecimals();
                                this.ScalesInMotion = DataReader.Substring(this.MotionStartPos - 1, this.MotionLength).Contains(this.MotionString);
                            }
                            else
                            {
                                this.Weight =
                                    DataReader.RemoveNonDecimals();

                                this.ScalesInMotion =
                                    DataReader.Contains(this.MotionString);
                            }

                            DataReader = string.Empty;

                            this.IsWeightStable = this.CheckForStableReads();

                            if (this.usingStableReadsForMotion)
                            {
                                this.ScalesInMotion = !this.IsWeightStable;
                            }

                            if (this.Weight != null)
                            {
                                // Broadcast our weight data to all interested subscribers.
                                this.OnWeightDataReceived(new WeightDataReceivedArgs(this.Weight, this.IsWeightStable, this.ScalesInMotion));
                                DataBuffer = string.Empty;
                            }
                        }

                        return;



                    //case IndicatorModel.GENERIC:

                    //    //******************************************//

                    //    // R320
                    //    //this.WeightStartPos = 1;
                    //    //this.WeightLength = 8;
                    //    //this.MotionLength = 1;
                    //    //this.MotionStartPos = 10;
                    //    //this.MotionString = "M";
                    //    //this.TerminatingCharacter = (char)3;
                    //    //this.StringToReadLength = 16;

                    //    //*******************************************//

                    //    // GSE465
                    //    //this.WeightStartPos = 4;
                    //    //this.WeightLength = 4;
                    //    //this.MotionLength = 1;
                    //    //this.MotionStartPos = 8;
                    //    //this.MotionString = "";
                    //    //this.TerminatingCharacter = (char)13;
                    //    //this.StringToReadLength = 27;

                    //    //*******************************************//

                    //    // System1000
                    //    // SD      43.9 kg
                    //    this.WeightStartPos = 7;
                    //    this.WeightLength = 6;
                    //    this.MotionLength = 1;
                    //    this.MotionStartPos = 2;
                    //    this.MotionString = "D";
                    //    this.TerminatingCharacter = (char)13;
                    //    this.StringToReadLength = 16;

                    //    //*******************************************//

                    //    var localDataBuffer = DataBuffer;

                    //    for (int i = 0; i < DataBuffer.Length; i++)
                    //    {
                    //        var bufferChar = DataBuffer.ElementAt(i);
                    //        DataReader += bufferChar;
                    //        if (bufferChar == this.TerminatingCharacter && DataReader.Length >= this.StringToReadLength)
                    //        {
                    //            DataReader = DataReader.Substring(DataReader.Length - this.StringToReadLength,
                    //                this.StringToReadLength);

                    //            this.Weight =
                    //                DataReader.Substring(this.WeightStartPos - 1, this.WeightLength).RemoveNonDecimals();

                    //            this.ScalesInMotion =
                    //                DataReader.Substring(this.MotionStartPos - 1, this.MotionLength)
                    //                    .Contains(this.MotionString);

                    //            DataReader = string.Empty;

                    //            this.IsWeightStable = this.CheckForStableReads();

                    //            if (this.usingStableReadsForMotion)
                    //            {
                    //                this.ScalesInMotion = !this.IsWeightStable;
                    //            }

                    //            if (this.Weight != null)
                    //            {
                    //                // Broadcast our weight data to all interested subscribers.
                    //                this.OnWeightDataReceived(new WeightDataReceivedArgs(this.Weight, this.IsWeightStable, this.ScalesInMotion));
                    //            }
                    //        }
                    //        else
                    //        {
                    //            localDataBuffer = DataReader;
                    //        }
                    //    }

                    //    DataBuffer = localDataBuffer;
                    //    return;
                }

                this.IsWeightStable = this.CheckForStableReads();

                if (this.usingStableReadsForMotion)
                {
                    this.ScalesInMotion = this.IsWeightStable;
                }

                if (this.Weight != null)
                {
                    // Broadcast our weight data to all interested subscribers.
                    this.OnWeightDataReceived(new WeightDataReceivedArgs(this.Weight, this.IsWeightStable, this.ScalesInMotion));
                }
            }
            catch (Exception ex)
            {
                DataBuffer = string.Empty;
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Retrieves the last stored alibi number.
        /// </summary>
        private void GetLastAlibiNumber()
        {
            var alibiFile = new XDocument();

            if (!Directory.Exists(this.filesPath))
            {
                return; //throw new DirectoryNotFoundException(Properties.Message.InvalidFilePath);
            }

            var mostRecentFile = Directory.EnumerateFiles(this.filesPath).LastOrDefault();

            if (mostRecentFile == null)
            {
                return;
            }

            try
            {
                var filePath = Path.Combine(this.filesPath, mostRecentFile);
                Security.DecryptXmlFile(ref alibiFile, filePath);

                if (alibiFile.Element("root").HasElements)
                {
                    var localAlibi = alibiFile.Element("root").Elements("alibi").Max(x => x.FirstAttribute.Value.ToInt());
                    this.Alibi = localAlibi;
                }

                Security.EncryptXmlFile(alibiFile, filePath);
            }
            catch (Exception e)
            {
            }
        }

        /// <summary>
        /// Method that attempts to save a weight and it's associated data, to an xml file.
        /// </summary>
        /// <returns>a flag indicating a successful save or not.</returns>
        private bool SaveData()
        {
            var file = string.Format("{0}.xml", DateTime.Today.ToString("yyyyMMdd"));
            var filePath = Path.Combine(this.filesPath ?? string.Empty, file);

            var alibiFile = new XDocument();

            try
            {
                if (!Directory.Exists(this.filesPath))
                {
                    // create the disretory, if it doesn't exist
                    Directory.CreateDirectory(this.filesPath);
                }

                if (!File.Exists(filePath))
                {
                    // first weight of the day, so create the file header.
                    alibiFile = new XDocument(
                                     new XDeclaration("1.0", "utf-8", "yes"),
                                     new XElement("root", new XAttribute("date", DateTime.Now.ToLongDateString())));

                    Security.EncryptXmlFile(alibiFile, filePath);
                }

                Security.DecryptXmlFile(ref alibiFile, filePath);
                var checksum = Security.GenerateStringChecksum(string.Format("{0}{1}", this.Alibi, this.Weight));

                var newNode =
                    new XElement("alibi", new XAttribute("number", this.Alibi.ToString()),
                    new XElement("date", DateTime.Today.ToString("dd/MM/yyyy")),
                    new XElement("time", DateTime.Now.ToLongTimeString()),
                    new XElement("weight", this.Weight),
                    new XElement("tare", this.Tare),
                    new XElement("unitprice", this.UnitPrice.ToString()),
                    new XElement("total", this.Total.ToString()),
                    new XElement("checksum", checksum));

                alibiFile.Element("root").Add(newNode);

                Security.EncryptXmlFile(alibiFile, filePath);
                return true;
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// Checks if we x amount of identical weights read.
        /// </summary>
        /// <returns>A flag, indicating whether the current weight is stable or not.</returns>
        private bool CheckForStableReads()
        {
            try
            {
                if (this.weights.Count() < this.StableReads)
                {
                    this.weights.Add(this.Weight);
                }
                else
                {
                    if (this.StableReads > 0)
                    {
                        this.weights.RemoveAt(0);
                    }

                    this.weights.Add(this.Weight);
                }

                if (!this.usingStableReadsForMotion)
                {
                    return this.weights.Count() >= this.StableReads && this.weights.Distinct().Count() == 1;
                }
                else
                {
                    if (this.weights.Count < this.StableReads)
                    {
                        return false;
                    }

                    var minWgt = this.weights.Min(x => x).ToDecimal();
                    var maxWgt = this.weights.Max(x => x).ToDecimal();
                    return minWgt.GetDifference(maxWgt) <= this.Tolerance;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Verifies that the dll has not been tampered with. If it has,
        /// the generated checksum vale will not match the stored checksum value,
        /// and the application must legally shut down.
        /// </summary>
        private void VerifyChecksum()
        {
            try
            {
                //var storedChecksumPath = Path.Combine(this.filesPath, "Nouvem.Indicator.dll");
                var currentChecksum = this.GenerateFileChecksum();

                if (!File.Exists(this.checksumFilePath))
                {
                    System.Windows.Forms.MessageBox.Show("Alibi checksum does not match certificate checksum. Application terminating..");
                    Environment.Exit(0);
                }

                // compare our checksums.
                var doc = XDocument.Load(this.checksumFilePath);
                var storedChecksum = doc.Root.Value;

                this.CertifiedChecksum = storedChecksum;
                this.CurrentChecksum = currentChecksum;

                if (storedChecksum != currentChecksum)
                {
                    // They don't match...
                    //throw new Exception("Checksums don't match");
                    System.Windows.Forms.MessageBox.Show("Alibi checksum does not match certificate checksum. Application terminating..");
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Checksums don't match");
                System.Windows.Forms.MessageBox.Show("Alibi checksum does not match certificate checksum. Application terminating..");
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Generates a checksum over this dll.
        /// </summary>
        /// <returns>A hexidecimal checksum generated over the file.</returns>
        private string GenerateFileChecksum()
        {
            var localPath = Path.Combine(Application.StartupPath, "Nouvem.Indicator.dll");
            return File.Exists(localPath) ? Security.GenerateChecksum(localPath) : string.Empty;
        }

        /// <summary>
        /// Validate the weight to be saved.
        /// </summary>
        /// <returns>An empty string if validated, or the error if not.</returns>
        private string ValidateWeight()
        {
            if (this.ignoreValidation)
            {
                return string.Empty;
            }

            if (!this.movementOnScales)
            {
                if (!this.ignoreNoMotionOnScalesWarning && !this.IgnoreDisturbBy)
                {
                    return Properties.Message.NoMovementOnScales;
                }
                else
                {
                    this.IsWeightStable = true;
                }
            }

            if (!this.AutoWeigh)
            {
                if (this.ScalesInMotion)
                {
                    return Properties.Message.ScalesInMotion;
                }
            }

            if (!this.IsWeightStable)
            {
                return Properties.Message.WeightNotStable;
            }

            if (!this.IgnoreMinimumWeight && this.Weight.ToDecimal() < this.MinimumWeight)
            {
                return Properties.Message.BelowMinimumWeight;
            }

            if (!this.HasWeightDroppedBelow)
            {
                return string.Format(Properties.Message.DropBelowNotReached, this.dropBelow);
            }

            return string.Empty;
        }

        /// <summary>
        /// Creates a new event, to broadcast the incoming serial data.
        /// </summary>
        /// <param name="e">The event argument.</param>
        private void OnBufferDataReceived(BufferDataReceivedArgs e)
        {
            if (this.BufferDataReceived != null)
            {
                this.BufferDataReceived(this, e);
            }
        }

        /// <summary>
        /// Creates a new event, to broadcast the incoming serial data.
        /// </summary>
        /// <param name="e">The event argument.</param>
        private void OnWeightDataReceived(WeightDataReceivedArgs e)
        {
            if (this.WeightDataReceived != null)
            {
                this.WeightDataReceived(this, e);
            }
        }

        #endregion
    }
}
