﻿// -----------------------------------------------------------------------
// <copyright file="SerialBase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Indicator
{
    using System;
    using System.IO.Ports;

    /// <summary>
    /// Base serial class that reads the incoming serial data.
    /// </summary>
    public abstract class SerialBase
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SerialBase"/> class.
        /// </summary>
        protected SerialBase()
        {
            this.SerialPort = new SerialPort();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SerialBase"/> class with arguments.
        /// </summary>
        /// <param name="port">The name of the com port.</param>
        /// <param name="baudRate">The baud rate setting.</param>
        /// <param name="parity">The parity setting.</param>
        /// <param name="stopBits">The stop bits value.</param>
        /// <param name="dataBits">The data bits value.</param>
        protected SerialBase(string port, string baudRate, string parity, string stopBits, string dataBits): this()
        {           
            this.Port = port;
            this.BaudRate = baudRate;
            this.Parity = parity;
            this.DataBits = dataBits;
            this.StopBits = stopBits;
            this.SerialPort.DataReceived += this.SerialPortOnDataReceived;
            //this.SerialPort.PinChanged += this.SerialPortOnPinChanged;
        }

        #endregion     

        #region public interface

        #region properties

        /// <summary>
        /// Gets or sets a reference to the serial port.
        /// </summary>
        public SerialPort SerialPort { get; set; }

        /// <summary>
        /// Gets a vlaue indicating whether the port is open or not.
        /// </summary>
        public bool IsPortOpen
        {
            get
            {
                return this.SerialPort.IsOpen;
            }
        }

        /// <summary>
        /// Gets the incoming serial port data.
        /// </summary>
        public string PortData { get; private set; }

        /// <summary>
        /// Gets or sets the port name
        /// </summary>
        protected string Port { get; set; }

        /// <summary>
        /// Gets or sets the baud rate.
        /// </summary>
        protected string BaudRate { get; set; }

        /// <summary>
        /// Gets or sets the parity value.
        /// </summary>
        protected string Parity { get; set; }

        /// <summary>
        /// Gets or sets the stop bits value.
        /// </summary>
        protected string StopBits { get; set; }

        /// <summary>
        /// Gets or sets the data bits value.
        /// </summary>
        protected string DataBits { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Reads, and parses, the incoming serial data string.
        /// </summary>
        public void WriteToPort(string data)
        {
            if (data == null)
            {
                return;

            }

            if (this.SerialPort.IsOpen)
            {
                this.SerialPort.WriteLine(data);
            }
        }

        /// <summary>
        /// Returns the available ports.
        /// </summary>
        /// <returns>A cxollection of available port names.</returns>
        public string[] GetPorts()
        {
            return SerialPort.GetPortNames();
        }

        /// <summary>
        /// Opens a connection to the serial port.
        /// </summary>
        public void OpenPort()
        {
            if (this.SerialPort.IsOpen)
            {
                return;
            }
  
            this.SetPortValues();

            // open the port
            this.SerialPort.Open();
        }

        /// <summary>
        /// Closes a connection to the serial port.
        /// </summary>
        public void ClosePort()
        {
            if (!this.SerialPort.IsOpen)
            {
                return;
            }

            this.SerialPort.Close();
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Gets or sets the thread wait time.
        /// </summary>
        protected int ThreadWaitTime { get; set; }

        /// <summary>
        /// Reads the incoming serial data. (Handling logic overriden in base class(s)
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event argument.</param>
        protected virtual void SerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                System.Threading.Thread.Sleep(this.ThreadWaitTime);
                this.PortData = this.SerialPort.ReadExisting();

            }
            catch (Exception)
            {
                // if there's an issue, just move on.
            }
        }

        ///// <summary>
        ///// Catches the pin change events.
        ///// </summary>
        ///// <param name="sender">The sender.</param>
        ///// <param name="serialPinChangedEventArgs">The event args.</param>
        //protected virtual void SerialPortOnPinChanged(object sender, SerialPinChangedEventArgs serialPinChangedEventArgs)
        //{
        //}

        /// <summary>
        /// Read and parse the port data.
        /// </summary>
        protected abstract void ReadSerialData();

        #endregion

        #region private

        /// <summary>
        /// Sets the serial port property values.
        /// </summary>
        private void SetPortValues()
        {
            this.SerialPort.PortName = this.Port;
            this.SerialPort.BaudRate = Convert.ToInt32(this.BaudRate);
            this.SerialPort.Parity = (Parity)Enum.Parse(typeof(Parity), this.Parity);
            this.SerialPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), this.StopBits);
            this.SerialPort.DataBits = Convert.ToInt32(this.DataBits);
        }

        #endregion
    }
}
