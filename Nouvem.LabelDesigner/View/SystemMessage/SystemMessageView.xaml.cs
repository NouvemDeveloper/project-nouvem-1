﻿// -----------------------------------------------------------------------
// <copyright file="LabelDesignerViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.SystemMessage
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for SystemMessageView.xaml
    /// </summary>
    public partial class SystemMessageView : UserControl
    {
        public SystemMessageView()
        {
            this.InitializeComponent();
        }
    }
}
