﻿// -----------------------------------------------------------------------
// <copyright file="NouvemMessageBoxView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.UserInput
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Global;

    /// <summary>
    /// Interaction logic for NouvemMessageBoxView.xaml
    /// </summary>
    public partial class NouvemMessageBoxView
    {
        public NouvemMessageBoxView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseMessageBoxWindow, x => this.Close());
        }
    }
}
