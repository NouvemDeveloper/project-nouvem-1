﻿// -----------------------------------------------------------------------
// <copyright file="SaveAsView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Neodynamic.Windows.WPF.ThermalLabelEditor;

namespace Nouvem.LabelDesigner.View.Utility
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.LabelDesigner.Global;

    /// <summary>
    /// Interaction logic for SaveAsView.xaml
    /// </summary>
    public partial class SaveAsView : Window
    {
        public SaveAsView(ThermalLabelEditor editor)
        {
            this.InitializeComponent();
            this.Loaded += (sender, args) => this.TextBoxSaveAs.Focus();
            Messenger.Default.Register<string>(this, Token.CloseMessageBoxWindow, s => this.Close());
            Messenger.Default.Send(editor, Token.SaveAsEditor);
        }
    }
}
