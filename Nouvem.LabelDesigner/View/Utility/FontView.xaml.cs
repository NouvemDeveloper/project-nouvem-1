﻿// -----------------------------------------------------------------------
// <copyright file="FontView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.LabelDesigner.Model.DataLayer;

namespace Nouvem.LabelDesigner.View.Utility
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using DevExpress.Xpf.Editors;
    using GalaSoft.MvvmLight.Messaging;
    using Neodynamic.SDK.Printing;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// Interaction logic for FontView.xaml
    /// </summary>
    public partial class FontView : UserControl
    {
        #region field

        /// <summary>
        /// The current font reference.
        /// </summary>
        private Font currentFont = new Font();

        /// <summary>
        /// The original font details reference.
        /// </summary>
        private Tuple<Font, string> originalFontDetails;

        /// <summary>
        /// Determines if we are setting a font.
        /// </summary>
        private bool settingFont;

        /// <summary>
        /// Reference to the current foreground editor.
        /// </summary>
        private PopupColorEdit currentForegroundEdit;

        /// <summary>
        /// Reference to the current background editor.
        /// </summary>
        private PopupColorEdit currentBackgroundEdit;

        #endregion

        #region constructor

        public FontView()
        {
            this.InitializeComponent();
            this.SetFonts();

            this.Loaded += (sender, args) =>
            {
                // register for the incoming font
                Messenger.Default.Register<Tuple<Font, string, Neodynamic.SDK.Printing.Color, Neodynamic.SDK.Printing.Color>>(this, Token.OpenFont, this.SetFont);

                // send for the editor item font
                Messenger.Default.Send(Token.Message, Token.OpenFont);

                // register for a font size change.
                Messenger.Default.Register<double>(this, Token.SetFontSize, i =>
                {
                    this.TextBoxFontSize.Text = i.ToString();
                });

                //// register for a font size change.
                //Messenger.Default.Register<string>(this, Token.SetFontName, s =>
                //{
                //    var index = 0;
                //    if (s.Equals("ZPL Font 0"))
                //    {
                //        index = 1;
                //    }
                 
                //    this.ListBoxFonts.SelectedIndex = index;
                //});
            };

            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);

            // Removes the 'Standard Colors' palette.
            //this.PopupColorEditBackground.Palettes.Remove(this.PopupColorEditBackground.Palettes["Standard Colors"]);
            //this.PopupColorEditForeground.Palettes.Remove(this.PopupColorEditBackground.Palettes["Standard Colors"]);
            //this.PopupColorEditBackground.Palettes.Remove(this.PopupColorEditBackground.Palettes["Theme Colors"]);
            //this.PopupColorEditForeground.Palettes.Remove(this.PopupColorEditBackground.Palettes["Theme Colors"]);

            // Adds a new palette with just black and white colors.
            this.PopupColorEditBackground.Palettes.Add(new CustomPalette("Custom RGB Colors", new List<System.Windows.Media.Color> { Colors.Black, Colors.White }));
            this.PopupColorEditForeground.Palettes.Add(new CustomPalette("Custom RGB Colors", new List<System.Windows.Media.Color> { Colors.Black, Colors.White }));
        }

        #endregion

        #region public interface

        public void SetFonts()
        {
            this.ListBoxFonts.DisplayMemberPath = "Alias";
            foreach (var font in LabelDesignerGlobal.LabelFonts)
            {
                this.ListBoxFonts.Items.Add(font);
            }
        }

        /// <summary>
        /// Sets the font to the incoming font.
        /// </summary>
        /// <param name="fontDetails">The incoming font details.(Font and text to format)</param>
        public void SetFont(Tuple<Font, string, Neodynamic.SDK.Printing.Color, Neodynamic.SDK.Printing.Color> fontDetails)
        {
            this.settingFont = true;

            var font = fontDetails.Item1;
            var textToFormat = fontDetails.Item2;
            var backgroundColor = fontDetails.Item3;
            var foregroundColor = fontDetails.Item4;

            this.currentFont.UpdateFrom(font);
            this.originalFontDetails = Tuple.Create(font, textToFormat);

            // set font name to listbox
            //this.ListBoxFonts.SelectedIndex = ListBoxFonts.Items.IndexOf(font.Name);
            this.ListBoxFonts.SelectedValue = font.Name;
            this.ListBoxFonts.ScrollIntoView(this.ListBoxFonts.SelectedItem);

            // set style
            //((ListBoxItem)this.ListBoxFontFamily.Items[0]).IsSelected = font.Bold || font.Name == Font.NativePrinterFontB;
            //((ListBoxItem)this.ListBoxFontFamily.Items[1]).IsSelected = font.Italic;
            this.CheckBoxStrikethrough.IsChecked = font.Strikeout;
            this.CheckBoxUnderline.IsChecked = font.Underline;

            // Set font size. (NOTE: NativePrinterFonts (A, B & S) have fixed sizes)
            if (font.IsNativePrinterFont)
            {
                font.Size = Font.GetNearestSizeForNativePrinterFonts(font);
            }

            var fontSizeInPt = Neodynamic.Windows.ThermalLabelEditor.UnitUtils.Convert(font.Unit, font.Size, FontUnit.Point, 2);

            this.TextBoxFontSize.Text = fontSizeInPt.ToString();

            // preview settings
            this.TextBlockPreview.Text = textToFormat;
            this.TextBlockPreview.FontFamily = new FontFamily(font.Name);
            this.TextBlockPreview.FontSize = fontSizeInPt / 72d * 96d; // size is in points and needs to be converted to WPF DIU
            this.TextBlockPreview.FontStyle = font.IsNativePrinterFont == false && font.Italic ? FontStyles.Italic : FontStyles.Normal;
            this.TextBlockPreview.FontWeight = font.IsNativePrinterFont == false && font.Bold ? FontWeights.Bold : FontWeights.Normal;

            if (font.Name == Font.NativePrinterFontB)
            {
                // NativePrinterFontB must be always Bold!
                //TextBlockPreview.FontWeight = FontWeights.Bold;
            }

            if (font.IsNativePrinterFont == false && font.Strikeout)
            {
                TextBlockPreview.TextDecorations.Add(TextDecorations.Strikethrough);
            }

            if (font.IsNativePrinterFont == false && font.Underline)
            {
                TextBlockPreview.TextDecorations.Add(TextDecorations.Underline);
            }

            this.TextBlockPreview.Background = backgroundColor.PrinterColorToBrush();
            this.TextBlockPreview.Foreground = foregroundColor.PrinterColorToBrush();
            this.PopupColorEditBackground.Color = backgroundColor.PrinterColorToColor();
            this.PopupColorEditForeground.Color = foregroundColor.PrinterColorToColor();

            this.settingFont = false;
        }

        /// <summary>
        /// Gets the current font.
        /// </summary>
        /// <returns>The current font.</returns>
        public Font GetFont()
        {
            // set the font name
            if (this.ListBoxFonts.SelectedItem != null && this.ListBoxFonts.SelectedItem is LabelFont)
            {
                this.currentFont.Name = (this.ListBoxFonts.SelectedItem as LabelFont).Name;
                this.currentFont.NameAtPrinterStorage = (this.ListBoxFonts.SelectedItem as LabelFont).NameAtPrinterStorage;
                var codePage = (this.ListBoxFonts.SelectedItem as LabelFont).CodePage;
                if (!string.IsNullOrEmpty(codePage))
                {
                    try
                    {
                        var localCodePage = (CodePage) Enum.Parse(typeof(CodePage), codePage);
                        this.currentFont.CodePage = localCodePage;
                    }
                    catch (Exception ex)
                    {
                        var log = new Logging.Logger();
                        log.LogError(this.GetType(), string.Format("Codepage:{0}, Error:{1}", codePage, ex.Message));
                    }
                }
            }

            // set the style
            //this.currentFont.Bold = ((ListBoxItem)this.ListBoxFontFamily.Items[1]).IsSelected;
            //this.currentFont.Italic = ((ListBoxItem)this.ListBoxFontFamily.Items[2]).IsSelected;
            this.currentFont.Strikeout = this.CheckBoxStrikethrough.IsChecked.ToBool();
            this.currentFont.Underline = this.CheckBoxUnderline.IsChecked.ToBool();

            // set font size
            this.currentFont.Size = this.TextBoxFontSize.Text.ToFontDouble();
            this.currentFont.Unit = FontUnit.Point;



            //if (this.currentFont.Name == "NativePrinterFontB")
            //{
            //    //this.currentFont.Bold = true;
            //}

            //if (this.currentFont.Name.Equals("Regular"))
            //{
            //    this.currentFont.Name = "Segoe UI";
            //    this.currentFont.NameAtPrinterStorage = "E:SEGOEUI";
            //}
            //else if (this.currentFont.Name.Equals("Bold"))
            //{
            //    this.currentFont.Name = "ZPL Font 0";
            //    this.currentFont.NameAtPrinterStorage = "E:ZPLFONT0";
            //}
            //else if (this.currentFont.Name.Equals("Arial"))
            //{
            //    this.currentFont.Name = "Arial";
            //    this.currentFont.NameAtPrinterStorage = "E:ARIALR";
            //}
            //else if (this.currentFont.Name.Equals("Arial Bold"))
            //{
            //    this.currentFont.Name = "Arial Bold";
            //    this.currentFont.NameAtPrinterStorage = "E:ARIALB";
            //}
            //else if (this.currentFont.Name.Equals("Arial Narrow"))
            //{
            //    this.currentFont.Name = "Arial Narrow";
            //    this.currentFont.NameAtPrinterStorage = "E:ARIALNAR";
            //}
            //else if (this.currentFont.Name.Equals("Arial Narrow Bold"))
            //{
            //    this.currentFont.Name = "Arial Narrow Bold";
            //    this.currentFont.NameAtPrinterStorage = "E:ARIALNAB";
            //    //this.currentFont.Bold = true;
            //}
            //else if (this.currentFont.Name.Equals("Arabic Typesetting"))
            //{
            //    this.currentFont.Name = "Arabic Typesetting";
            //    this.currentFont.NameAtPrinterStorage = "E:ARABICTR";
            //}
           
            return this.currentFont;
        }

        /// <summary>
        /// Returns the selected colours.
        /// </summary>
        /// <returns>The converted printer colours.</returns>
        public Tuple<Neodynamic.SDK.Printing.Color, Neodynamic.SDK.Printing.Color> GetColours()
        {
            return Tuple.Create(this.PopupColorEditBackground.Color.ColorToPrinterColor(),
                this.PopupColorEditForeground.Color.ColorToPrinterColor());
        }

        #endregion

        #region private 

        /// <summary>
        /// Handles the change of font.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ListBoxFonts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ListBoxFonts.SelectedItem == null)
            {
                return;
            }
            
            //var selectedFont = ListBoxFonts.SelectedValue.ToString();
            var selectedFont = (this.ListBoxFonts.SelectedItem as LabelFont).Name;

            //if (selectedFont.Equals("Regular"))
            //{
            //    selectedFont = "Segoe UI";
            //}

            //if (selectedFont.Equals("Bold"))
            //{
            //    selectedFont = "ZPL Font 0";
            //}

            this.TextBlockPreview.FontFamily = new FontFamily(selectedFont);
            this.SendFont();
        }

        /// <summary>
        /// Handles the change of size selection.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ListBoxFontSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.TextBoxFontSize.Text = ((ListBoxItem)ListBoxFontSize.SelectedItem).Content.ToString();
            this.SendFont();
        }

        /// <summary>
        /// Handles the text box size change.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void TextBoxFontSize_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var fontSizeInPts = this.TextBoxFontSize.Text.ToDouble();
                var selectedFont = this.ListBoxFonts.SelectedValue;
                
                if (selectedFont != null && this.IsNativePrinterFont(selectedFont.ToString()))
                {
                    var tmpFont = new Font
                    {
                        Name = selectedFont.ToString(),
                        Unit = FontUnit.Point,
                        Size = fontSizeInPts
                    };

                    var selectedSize = fontSizeInPts;

                    // recalculate size for Native printer font
                    fontSizeInPts = Font.GetNearestSizeForNativePrinterFonts(tmpFont);

                    if (selectedSize > fontSizeInPts || selectedSize < fontSizeInPts)
                    {
                        // let the user know that their selected size has been reformatted to match native printer fonts.
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.SizeReformatted, selectedSize, fontSizeInPts));
                    }

                    // update textbox
                    TextBoxFontSize.Text = fontSizeInPts.ToString();
                }

                //size is in points and needs to be converted to WPF DIU
                TextBlockPreview.FontSize = fontSizeInPts/72d*96d;
            }
            catch
            {
            }
        }

        /// <summary>
        /// Handles the change of font style.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ListBoxFontFamily_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (this.ListBoxFonts.SelectedValue != null)
            //{
            //    var selectedFont = this.ListBoxFonts.SelectedValue.ToString();

            //    if (!this.IsNativePrinterFont(selectedFont))
            //    {
            //        switch (ListBoxFontFamily.SelectedIndex)
            //        {
            //            case 0:
            //                //this.TextBlockPreview.B;
            //                this.currentFont.Bold = false;
            //                break;
            //            case 1:
            //                //this.TextBlockPreview.FontStyle = FontStyles.Italic;
            //                this.currentFont.Bold = true;
            //                break;
            //        }

            //        this.TextBlockPreview.FontFamily = new FontFamily(selectedFont);
            //        this.SendFont();
            //    }
            //    else
            //    {
            //        // SystemMessage.Write(MessageType.Issue, string.Format(Message.FontStyleNotPermitted, selectedFont));
            //    }
            //}
        }

        /// <summary>
        /// handles the strikethrough selection.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void CheckBoxStrikethrough_Checked(object sender, RoutedEventArgs e)
        {
            if (this.ListBoxFonts.SelectedValue != null)
            {
                var selectedFont = this.ListBoxFonts.SelectedValue.ToString();

                if (!this.IsNativePrinterFont(selectedFont))
                {
                    if (this.CheckBoxStrikethrough.IsChecked == true)
                    {
                        this.TextBlockPreview.TextDecorations.Add(TextDecorations.Strikethrough);
                    }
                    else
                    {
                        if (this.TextBlockPreview.TextDecorations.Contains(TextDecorations.Strikethrough[0]))
                        {
                            this.TextBlockPreview.TextDecorations.Remove(TextDecorations.Strikethrough[0]);
                        }
                    }

                    this.SendFont();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.StrikethroughNotPermitted, selectedFont));
                    this.CheckBoxStrikethrough.IsChecked = false;
                }
            }
        }

        /// <summary>
        /// handles the underline selection.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void CheckBoxUnderline_Checked(object sender, RoutedEventArgs e)
        {
            if (this.ListBoxFonts.SelectedValue != null)
            {
                var selectedFont = this.ListBoxFonts.SelectedValue.ToString();

                if (!this.IsNativePrinterFont(selectedFont))
                {
                    if (this.CheckBoxUnderline.IsChecked == true)
                    {
                        this.TextBlockPreview.TextDecorations.Add(TextDecorations.Underline);
                    }
                    else
                    {
                        if (this.TextBlockPreview.TextDecorations.Contains(TextDecorations.Underline[0]))
                        {
                            this.TextBlockPreview.TextDecorations.Remove(TextDecorations.Underline[0]);
                        }
                    }
                   
                    this.SendFont();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.UnderlineNotPermitted, selectedFont));
                    this.CheckBoxUnderline.IsChecked = false;
                }
            }
        }

        /// <summary>
        /// Handles the background colour change.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void PopupColorEditBackground_ColorChanged(object sender, RoutedEventArgs e)
        {
            var color = this.PopupColorEditBackground.Color.ColorToBrush();
            if (!this.IsPrinterColour(color))
            {
                SystemMessage.Write(MessageType.Issue, Message.NotPrinterCompatibleColour);
                this.PopupColorEditBackground.Color = this.currentBackgroundEdit.Color;
                this.PopupColorEditBackground.Text = this.currentBackgroundEdit.Text;
                return;
            }

            this.TextBlockPreview.Background = color;

            // create a deep copy, to hold the current data.
            this.currentBackgroundEdit = new PopupColorEdit
            {
                Color = this.PopupColorEditBackground.Color,
                Text = this.PopupColorEditBackground.Text
            };

            this.SendFont();
        }

        /// <summary>
        /// Handles the foreground colour change.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void PopupColorEditForeground_ColorChanged(object sender, RoutedEventArgs e)
        {
            var color = this.PopupColorEditForeground.Color.ColorToBrush();

            if (!this.IsPrinterColour(color))
            {
                SystemMessage.Write(MessageType.Issue, Message.NotPrinterCompatibleColour);
                this.PopupColorEditForeground.Color = currentForegroundEdit.Color;
                this.PopupColorEditForeground.Text = currentForegroundEdit.Text;
                return;
            }

            this.TextBlockPreview.Foreground = color;

            // create a deep copy, to hold the current data.
            this.currentForegroundEdit = new PopupColorEdit
            {
                Color = this.PopupColorEditForeground.Color,
                Text = this.PopupColorEditForeground.Text
            };

            this.SendFont();
        }

        /// <summary>
        /// Determines if the input font is a native printer font.
        /// </summary>
        /// <param name="font">The font to check.</param>
        /// <returns>A flag, indicating a native printer font or not.</returns>
        private bool IsNativePrinterFont(string font)
        {
            return font == Font.NativePrinterFontA || font == Font.NativePrinterFontB || font == Font.NativePrinterFontS;
        }

        /// <summary>
        /// Determines if a color is printer compatible.
        /// </summary>
        /// <param name="brush">The color to check.</param>
        /// <returns>A flag, indicating a color is printer compatible.</returns>
        private bool IsPrinterColour(Brush brush)
        {
            if (brush is SolidColorBrush)
            {
                return (brush as SolidColorBrush).Color.Equals(Colors.White)
                       || (brush as SolidColorBrush).Color.Equals(Colors.Black);
            }

            return false;
        }

        /// <summary>
        /// Send a font change to all observers.
        /// </summary>
        private void SendFont()
        {
            if (this.settingFont)
            {
                // settins up a font, so we ignore.
                return;
            }

            Messenger.Default.Send(Tuple.Create(this.GetFont(), this.GetColours()), Token.SetFont);
        }

        #endregion

        private void ListBoxFontSize_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.ListBoxFontSize.SelectedItem is ListBoxItem)
            {
                this.TextBoxFontSize.Text = ((ListBoxItem)this.ListBoxFontSize.SelectedItem).Content.ToString();
                this.SendFont();
            }
        }
    }
}
