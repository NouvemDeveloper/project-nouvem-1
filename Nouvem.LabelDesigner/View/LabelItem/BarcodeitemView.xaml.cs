﻿// -----------------------------------------------------------------------
// <copyright file="BarcodeitemView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.LabelItem
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BarcodeitemView.xaml
    /// </summary>
    public partial class BarcodeitemView : UserControl
    {
        public BarcodeitemView()
        {
            this.InitializeComponent();
        }
    }
}
