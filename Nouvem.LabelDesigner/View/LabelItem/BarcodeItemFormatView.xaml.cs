﻿// -----------------------------------------------------------------------
// <copyright file="BarcodeItemFormatView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.LabelItem
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for BarcodeItemFormatView.xaml
    /// </summary>
    public partial class BarcodeItemFormatView : UserControl
    {
        public BarcodeItemFormatView()
        {
            this.InitializeComponent();
        }
    }
}
