﻿// -----------------------------------------------------------------------
// <copyright file="Gs1CompressedView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.LabelItem
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for Gs1CompressedView.xaml
    /// </summary>
    public partial class Gs1CompressedView : UserControl
    {
        public Gs1CompressedView()
        {
            this.InitializeComponent();
        }
    }
}
