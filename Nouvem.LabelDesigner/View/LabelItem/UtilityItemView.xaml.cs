﻿// -----------------------------------------------------------------------
// <copyright file="UtilityItemView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.LabelItem
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for UtilityItemView.xaml
    /// </summary>
    public partial class UtilityItemView : UserControl
    {
        public UtilityItemView()
        {
            this.InitializeComponent();
        }
    }
}
