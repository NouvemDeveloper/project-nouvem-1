﻿// -----------------------------------------------------------------------
// <copyright file="PrinterSetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Print
{
    using System.Threading;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for Ethernel.xaml
    /// </summary>
    public partial class EthernelView : UserControl
    {
        public EthernelView()
        {
            this.InitializeComponent();
            ThreadPool.QueueUserWorkItem(x => this.TextBoxIpHost.Dispatcher.Invoke(() => this.TextBoxIpHost.Focus()));
        }
    }
}
