﻿// -----------------------------------------------------------------------
// <copyright file="PrinterSetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.View.Print
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for PrinterSetUpView.xaml
    /// </summary>
    public partial class PrinterSetUpView : UserControl
    {
        public PrinterSetUpView()
        {
            this.InitializeComponent();
        }
    }
}
