﻿// -----------------------------------------------------------------------
// <copyright file="LabelDesigner.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.ViewModel
{
    using System;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Nouvem.LabelDesigner.BusinessLogic;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.Logging;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// Base view model.
    /// </summary>
    public abstract class LabelDesignerViewModelBase : ViewModelBase
    {
        #region field

        /// <summary>
        /// The application logger.
        /// </summary>
        protected Logger Log = new Logger();

        /// <summary>
        /// Gets the singleton data manager reference.
        /// </summary>
        protected DataManager DataManager = DataManager.Instance;

        /// <summary>
        /// Gets the singleton print manager reference.
        /// </summary>
        protected PrintManager PrintManager = PrintManager.Instance;

        /// <summary>
        /// Gets the singleton label manager reference.
        /// </summary>
        protected LabelManager LabelManager = LabelManager.Instance;

        /// <summary>
        /// Flag, that notifies of a label item selection/loading.
        /// </summary>
        protected bool labelItemLoading;

        /// <summary>
        /// The current control mode.
        /// </summary>
        protected ControlMode CurrentMode;

        /// <summary>
        /// The control button content.
        /// </summary>
        private string controlButtonContent;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the LabelDesignerViewModelBase class.
        /// </summary>
        protected LabelDesignerViewModelBase()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            // Switch the control mode.
            this.ControlButtonCommand = new RelayCommand<string>(this.ControlCommandExecute);

            // Handle the selection of the control button.
            this.ControlButtonSelectedCommand = new RelayCommand(this.ControlSelectionCommandExecute);

            // Handle the selection of the cancel button.
            this.CancelButtonSelectedCommand = new RelayCommand(this.CancelSelectionCommandExecute);

            #endregion

            this.Locator = new ViewModelLocator();
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region property

        /// <summary>
        /// Gets the ViewModelLocator reference.
        /// </summary>
        protected ViewModelLocator Locator { get; private set; }

        /// <summary>
        /// Gets or sets the control button content.
        /// </summary>
        public string ControlButtonContent
        {
            get
            {
                return this.controlButtonContent;
            }

            set
            {
                this.controlButtonContent = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the current view model.
        /// </summary>
        public LabelDesignerViewModelBase CurrentViewModel
        {
            get
            {
                return ViewModelLocator.LabelDesignerStatic.CurrentViewModel;
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to put the data into 'add' mode.
        /// </summary>
        public ICommand ControlButtonCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selection of the control button.
        /// </summary>
        public ICommand ControlButtonSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selection of the cancel button.
        /// </summary>
        public ICommand CancelButtonSelectedCommand { get; private set; }

        #endregion

        #region protected virtual

        /// <summary>
        /// Method that handles the change of control mode.
        /// </summary>
        /// <param name="mode">The new mode.</param>
        protected virtual void ControlCommandExecute(string mode)
        {
            var controlMode = (ControlMode)Enum.Parse(typeof(ControlMode), mode);
            this.SetControlMode(controlMode);
        }

        protected virtual void ControlButtonSelectedCommandExecute(string controlSelection)
        {
        }

        #endregion

        #region private

        /// <summary>
        /// Set the control mode on user data entry.
        /// </summary>
        /// <typeparam name="T">The generic type parameter..</typeparam>
        /// <param name="newValue">The generic new value to compare.</param>
        /// <param name="oldValue">The generic old value to compare.</param>
        protected virtual void SetMode<T>(T newValue, T oldValue)
        {
            // ignore if we in add or update mode.
            if (this.CurrentMode == ControlMode.Add || this.CurrentMode == ControlMode.Update)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.OK || this.CurrentMode == ControlMode.Find)
            {
                if (newValue is ValueType && !newValue.Equals(oldValue))
                {
                    if (this.CheckForEntity())
                    {
                        //There is a partner currently selected, so we are updating
                        this.SetControlMode(ControlMode.Update);
                        return;
                    }

                    // no partner selected, so we are searching.
                    this.SetControlMode(ControlMode.Find);
                    return;
                }

                // check for a change of value
                if ((oldValue != null && newValue != null) && !newValue.Equals(oldValue))
                {
                    if (this.CheckForEntity())
                    {
                        //There is a partner currently selected, so we are updating
                        this.SetControlMode(ControlMode.Update);
                        return;
                    }

                    // no partner selected, so we are searching.
                    this.SetControlMode(ControlMode.Find);
                }
            }
        }

        /// <summary>
        /// Method that sets the application mode.
        /// </summary>
        /// <param name="mode">The mode to set.</param>
        /// <remarks>Note - Add authorisations checks are handled in the overriding ControlCommandExecute methods, 
        ///          but update authorisation checks need to handled here, as there is no cntr U.</remarks> 
        protected void SetControlMode(ControlMode mode)
        {
            switch (mode)
            {
                case ControlMode.OK:
                    this.CurrentMode = ControlMode.OK;
                    this.ControlButtonContent = Strings.OK;
                    break;

                case ControlMode.Add:
                    this.CurrentMode = ControlMode.Add;
                    this.ControlButtonContent = Strings.Add;
                    break;

                case ControlMode.Find:
                    this.CurrentMode = ControlMode.Find;
                    this.ControlButtonContent = Strings.Find;
                    break;

                case ControlMode.Update:
                     this.CurrentMode = ControlMode.Update;
                     this.ControlButtonContent = Strings.Update;
                    break;
            }
        }

        /// <summary>
        /// Determine if an entity has been selected.
        /// </summary>
        /// <returns>A flag as to whether an entity is selected.</returns>
        /// <remarks>This must be overridden in the calling class, to check for it's local selected entity.</remarks>
        protected virtual bool CheckForEntity()
        {
            return true;
        }

        /// <summary>
        /// Method that switches the view, by setting it's view model as the current view model.
        /// </summary>
        /// <param name="newView"></param>
        protected bool SwitchView(LabelDesignerViewModelBase newView)
        {
            if (this.Locator.LabelDesigner.CurrentViewModel != newView)
            {
                this.Locator.LabelDesigner.CurrentViewModel = newView;
                //this.Locator.LabelDesigner.MenuExpanded = true;
                return true;
            }

            return false;
        }

        #endregion

        #region abstract

        /// <summary>
        /// Abstract control selection method.
        /// </summary>
        protected abstract void ControlSelectionCommandExecute();

        /// <summary>
        /// Abstract cancel selection command.
        /// </summary>
        protected abstract void CancelSelectionCommandExecute();

        #endregion
    }
}
