﻿// -----------------------------------------------------------------------
// <copyright file="FontViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.ViewModel.Utility
{
    using System;

    public class FontViewModel : LabelDesignerViewModelBase
    {
        #region private

        /// <summary>
        /// The preview text field.
        /// </summary>
        private string previewText;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="FontViewModel"/> class.
        /// </summary>
        public FontViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }
        }

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the preview text.
        /// </summary>
        public string PreviewText
        {
            get
            {
                return this.previewText;
            }

            set
            {
                this.previewText = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region private



        #endregion


        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }
    }
}
