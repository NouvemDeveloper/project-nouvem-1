// -----------------------------------------------------------------------
// <copyright file="MenuViewModelr.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.ViewModel
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Main menu interaction logic.
    /// </summary>
    public class MenuViewModel : LabelDesignerViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MenuViewModel class.
        /// </summary>
        public MenuViewModel()
        {
            if (IsInDesignMode)
            {
                return;
            }
        }

        protected override void ControlSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }
    }
}