﻿// -----------------------------------------------------------------------
// <copyright file="TextItemViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.LabelDesigner.ViewModel.LabelItem
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using DevExpress.Xpf.Grid;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Neodynamic.SDK.Printing;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.BusinessObject;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.LabelDesigner.Properties;
    using Nouvem.Shared.Localisation;

    public class TextItemViewModel : LabelDesignerViewModelBase
    {
        #region field

        /// <summary>
        /// The incoming text item.
        /// </summary>
        private TextItem textItem;

        /// <summary>
        /// The selected label data.
        /// </summary>
        private LabelData selectedLabelData;

        /// <summary>
        /// The data source items.
        /// </summary>
        private ObservableCollection<LabelData> dataSourceFields;

        /// <summary>
        /// The bindable barcode string.
        /// </summary>
        private string barcodeData;

        /// <summary>
        /// The selected tab index.
        /// </summary>
        private int selectedTab;

        /// <summary>
        /// The text item text.
        /// </summary>
        private string textItemText;

        /// <summary>
        /// The text allignment value.
        /// </summary>
        private string selectedAllignment;

        /// <summary>
        /// The text rotation value.
        /// </summary>
        private string selectedRotation;

        /// <summary>
        /// The sample text field.
        /// </summary>
        private string sampleText;

        /// <summary>
        /// The text mode string.(sample or free text)
        /// </summary>
        private string textMode;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the TextItemViewModel class.
        /// </summary>
        public TextItemViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Nothing selected on the editor, so don't allow font access.
            Messenger.Default.Register<string>(Token.Message, Token.NoLabelItemSelected, x => this.SelectedTab = 0);

            // register for a selected label item change.
            Messenger.Default.Register<Item>(this, Token.SelectedItemChange, item =>
            {
                if (item is TextItem)
                {
                    this.TextItem = item as TextItem;

                    if (!string.IsNullOrEmpty(this.TextItem.DataField))
                    {
                        this.SelectedLabelData =
                          this.DataSourceFields.FirstOrDefault(x => x.Field.Equals(this.TextItem.DataField));
                    }
                }
            });

            #endregion

            #region command

            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>((e) =>
            {
                #region validation

                if (e == null || e.Row == null)
                {
                    return;
                }

                #endregion

                if (e.Row is LabelData)
                {
                    var isSelected = (bool)e.Value;
                    //Messenger.Default.Send(isSelected, Token.StringBuilder);
                    this.UpdateAssignedVariable(isSelected);
                }
            });

            // Handler for the load event.
            this.OnLoadingCommand = new RelayCommand(() =>
            {
                // register for data change
                //Messenger.Default.Register<bool>(this, Token.StringBuilder, this.UpdateAssignedVariable);

                if (LabelDesignerGlobal.SelectedLabelData != null)
                {
                    this.DataSourceFields = new ObservableCollection<LabelData>(LabelDesignerGlobal.SelectedLabelData);
                }
            });

            /* Handler for the unload event (unregister the label data IsSelected message to 
             * avoid a clash, as the barcode builder uses the same class) */
            this.OnUnloadedCommand = new RelayCommand(() => Messenger.Default.Unregister<bool>(this, Token.StringBuilder, this.UpdateAssignedVariable));

            #endregion

            if (LabelDesignerGlobal.SelectedLabelData != null)
            {
                this.DataSourceFields = new ObservableCollection<LabelData>(LabelDesignerGlobal.SelectedLabelData);
            }
 
            this.SelectedAllignment = Constant.Left;
            this.SelectedRotation = "0";
            this.TextMode = Strings.FreeText;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the text mode string (sample or free text).
        /// </summary>
        public string TextMode
        {
            get
            {
                return this.textMode;
            }

            set
            {
                this.textMode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the sample text.
        /// </summary>
        public string SampleText
        {
            get
            {
                return this.sampleText;
            }

            set
            {
                this.sampleText = value;
                this.RaisePropertyChanged();
                this.TextItem.Text = value;
                this.UpdateItem();
            }
        }

        public TextItem TextItem
        {
            get
            {
                return this.textItem;
            }

            set
            {
                this.textItem = value;
                this.ParseItem();
            }
        }

        /// <summary>
        /// Gets or sets the selected label data.
        /// </summary>
        public LabelData SelectedLabelData
        {
            get
            {
                return this.selectedLabelData;
            }

            set
            {
                this.selectedLabelData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the barcode string.
        /// </summary>
        public string BarcodeData
        {
            get
            {
                return this.barcodeData;
            }

            set
            {
                this.barcodeData = value;
                this.RaisePropertyChanged();
                Messenger.Default.Send(value, Token.UpdateBarcodeCode);
            }
        }

        /// <summary>
        /// Gets or sets the label data.
        /// </summary>
        public ObservableCollection<LabelData> DataSourceFields
        {
            get
            {
                return this.dataSourceFields;
            }

            set
            {
                this.dataSourceFields = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected text item text.
        /// </summary>
        public string TextItemText
        {
            get
            {
                return this.textItemText;
            }

            set
            {
                this.textItemText = value;
                this.RaisePropertyChanged();

                // send the text item text change to the editor.
                Messenger.Default.Send(value, Token.SelectedItemUpdate);
            }
        }

        /// <summary>
        /// Gets or sets the selected rotation value.
        /// </summary>
        public string SelectedRotation
        {
            get
            {
                return this.selectedRotation; 
            }

            set
            {
                this.selectedRotation = value;
                this.RaisePropertyChanged();

                if (!string.IsNullOrEmpty(value))
                {
                    var convertedValue = value.ToInt();

                    // send the text item text change to the editor.
                    Messenger.Default.Send(convertedValue, Token.SelectedItemUpdate);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected text allignment value.
        /// </summary>
        public string SelectedAllignment
        {
            get
            {
                return this.selectedAllignment;
            }

            set
            {
                this.selectedAllignment = value;
                this.RaisePropertyChanged();

                var convertedValue = (TextAlignment) Enum.Parse(typeof (TextAlignment), value);

                // send the text item text change to the editor.
                Messenger.Default.Send(convertedValue, Token.SelectedItemUpdate);}
        }

        /// <summary>
        /// Gets or sets the selected tab index.
        /// </summary>
        public int SelectedTab
        {
            get
            {
                return this.selectedTab;
            }

            set
            {
                this.selectedTab = value;
                this.RaisePropertyChanged();
                if (value == 1)
                {
                    // font selected. Instruct the editor to send it's current font to the font view.
                    //Messenger.Default.Send(Token.Message, Token.OpenFont);
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a label selection.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the ui load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unload event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        /// <summary>
        /// Assigns/unassigns a data source variable to the focused text item.
        /// </summary>
        /// <param name="b">The assign/unassign flag.</param>
        private void UpdateAssignedVariable(bool b)
        {
            #region validation

            if (this.selectedLabelData == null || this.labelItemLoading)
            {
                return;
            }

            #endregion

            if (b)
            {
                this.DeselectFields();
                SystemMessage.Write(MessageType.Priority, string.Format(Message.LabelVariableAssigned, this.selectedLabelData.Field));
                this.TextItem.DataField = this.selectedLabelData.Field;
                this.TextItemText = this.selectedLabelData.SampleData;
                this.TextItem.Text = this.selectedLabelData.SampleData;
                this.UpdateItem();
                this.TextMode = Strings.Sample;
            }
            else
            {
                this.TextItem.DataField = string.Empty;
                this.TextItem.Text = string.Empty;
                this.TextItemText = string.Empty;
                this.UpdateItem();
                this.TextMode = Strings.FreeText;
                SystemMessage.Write(MessageType.Priority, string.Format(Message.LabelVariableRemoved, this.selectedLabelData.Field));
            }
        }

        /// <summary>
        /// Handles the change to the current text item, parsing the ui to match it's fields.
        /// </summary>
        private void ParseItem()
        {
            this.labelItemLoading = true;
            var itemVariable = this.textItem.DataField.Trim();

            // Uncheck the IsSelected values, applying a check only to the item variable match.
            if (this.DataSourceFields != null)
            {
                foreach (var field in this.DataSourceFields)
                {
                    field.IsSelected = field.Field.Trim().Equals(itemVariable);
                }
            }

            this.TextItemText = this.TextItem.Text;
            this.SelectedRotation = this.TextItem.RotationAngle.ToString();
            this.SelectedAllignment = this.TextItem.TextAlignment.ToString();
            this.labelItemLoading = false;
        }

        /// <summary>
        /// Notifies the editor of a change to the barcode item.
        /// </summary>
        private void UpdateItem()
        {
            Messenger.Default.Send<Item>(this.textItem, Token.SelectedItemUpdate);
        }

        private void DeselectFields()
        {
            try
            {
                this.labelItemLoading = true;
                foreach (var field in this.DataSourceFields)
                {
                    if (!field.Field.Equals(this.selectedLabelData.Field))
                    {
                        field.IsSelected = false;
                    }
                }
            }
            finally
            {
                this.labelItemLoading = false;
            }
        }

        #endregion
    }
}
