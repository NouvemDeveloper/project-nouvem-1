﻿// -----------------------------------------------------------------------
// <copyright file="PrintEngine.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Nouvem.LabelDesigner.BusinessLogic;
using Nouvem.LabelDesigner.ViewModel;

namespace Nouvem.LabelDesigner.Global
{
    using System;
    using System.Drawing.Printing;
    using System.IO;
    using System.IO.Ports;
    using Neodynamic.SDK.Printing;
    using Nouvem.LabelDesigner.Properties;
    using Nouvem.Logging;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// Entry point class for internal and external print operations.
    /// </summary>
    public static class PrintEngine
    {
        #region field

        /// <summary>
        /// The print job reference.
        /// </summary>
        public static readonly PrintJob PrintJob = new PrintJob();

        /// <summary>
        /// Gets or sets the printer settings.
        /// </summary>
        public static readonly Neodynamic.SDK.Printing.PrinterSettings printSettings = new Neodynamic.SDK.Printing.PrinterSettings();

        /// <summary>
        /// The logger reference.
        /// </summary>
        private static readonly ILogger Logger = new Logger();

        /// <summary>
        /// The current label.
        /// </summary>
        private static ThermalLabel label;

        #endregion

        #region public interface

        /// <summary>
        /// Gets or sets the thermal label.
        /// </summary>
        public static ThermalLabel Label
        {
            get
            {
                return label;
            }

            set
            {
                label = value;
            }
        }

        /// <summary>
        /// Gets the printer settings.
        /// </summary>
        public static Neodynamic.SDK.Printing.PrinterSettings PrintSettings
        {
            get
            {
                return printSettings;
            }
        }

        /// <summary>
        /// Gets or sets the label data source.
        /// </summary>
        public static string DataSource { get; set; }

        /// <summary>
        /// Prints the label.
        /// </summary>
        /// <param name="dataSourceId">The data source id (0 = default sp display record)</param>
        /// <returns>A string, indicating a successful print, or error if there's an issue.</returns>
        public static string Print(int dataSourceId = 0)
        {
            try
            {
                var dataSource = DataManager.Instance.GetTestLabelData();
                label.DataSource = dataSource;

                if (Settings.Default.PrintMode.Equals(Model.Enum.PrintMode.Image.ToString()))
                {
                    return ExportToImage(Label, true);
                }

                //var textItem = new TextItem();
                //textItem.Width = 1.5;
                //textItem.Height = 0.5;
                //textItem.X = 2;
                //textItem.Y = 1;
                //textItem.DataField = "MultiLineText1";
                //textItem.Font.Name = "Segoe UI";
                //textItem.Font.NameAtPrinterStorage = "E:SEGOEUI";
                //label.Items.Add(textItem);
               
                using (var printJob = new PrintJob(PrintSettings))
                {
                    printJob.Copies = Settings.Default.Copies;
                   
                    printJob.ThermalLabel = label;
                    printJob.PrintOrientation = (PrintOrientation)Enum.Parse(typeof(PrintOrientation), Settings.Default.PrintOrientation);
                    printJob.Copies = Settings.Default.Copies;

                    var copies = ViewModelLocator.PrinterSetUpStatic.Copies;
                    if (copies < 1)
                    {
                        copies = 1;
                    }

                    for (int i = 0; i < copies; i++)
                    {
                        printJob.Print();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(typeof(PrintEngine), string.Format("{0} Stack - {1}", ex.Message, ex.StackTrace));
               
                if (Label == null)
                {
                    return Message.NoLabelLoaded;
                }

                return ex.Message;
            }

            return string.Empty;
        }

        /// <summary>
        /// Sets the print job settings.
        /// </summary>
        public static void SetSettings()
        {
            try
            {
                // general
                if (string.IsNullOrEmpty(Settings.Default.PrintMode))
                {
                    Settings.Default.PrintMode = CommunicationType.USB.ToString();
                }

                PrintSettings.Communication.CommunicationType = (CommunicationType)Enum.Parse(typeof(CommunicationType), Settings.Default.PrintMode);
                PrintSettings.Dpi = Settings.Default.DPI;
                PrintSettings.ProgrammingLanguage = (ProgrammingLanguage)Enum.Parse(typeof(ProgrammingLanguage), Settings.Default.PrinterLanguage);

                // serial
                PrintSettings.Communication.SerialPortName = Settings.Default.ComPort;
                PrintSettings.Communication.SerialPortBaudRate = Settings.Default.BaudRate.ToInt();
                PrintSettings.Communication.SerialPortParity = (Parity)Enum.Parse(typeof(Parity), Settings.Default.Parity);
                PrintSettings.Communication.SerialPortDataBits = Settings.Default.DataBits.ToInt();
                PrintSettings.Communication.SerialPortStopBits = (StopBits)Enum.Parse(typeof(StopBits), Settings.Default.StopBits);
                PrintSettings.Communication.SerialPortFlowControl = (Handshake)Enum.Parse(typeof(Handshake), Settings.Default.Handshake);

                // parallel
                PrintSettings.Communication.ParallelPortName = Settings.Default.ParallelPortName;

                // usb
                if (PrintSettings.Communication.CommunicationType == CommunicationType.USB)
                {
                    PrintSettings.PrinterName = Settings.Default.PrinterName;
                    PrintSettings.UseDefaultPrinter = Settings.Default.UseDefaultPrinter;
                }

                // ethernet
                PrintSettings.Communication.NetworkIPAddress = System.Net.IPAddress.None;

                if (PrintSettings.Communication.CommunicationType == CommunicationType.Network)
                {
                    try
                    {
                        PrintSettings.Communication.NetworkPort = Settings.Default.Port;

                        // try ip
                        PrintSettings.Communication.NetworkIPAddress = System.Net.IPAddress.Parse(Settings.Default.IPHost);
                    }
                    catch
                    {
                        // try host name
                        PrintSettings.PrinterName = Settings.Default.IPHost;
                    }
                }

                // print
                PrintJob.PrinterSettings = PrintSettings;
                PrintJob.PrintOrientation = (PrintOrientation)Enum.Parse(typeof(PrintOrientation), Settings.Default.PrintOrientation); 
                PrintJob.Copies = Settings.Default.Copies; 
            }
            catch (Exception ex)
            {
                Logger.LogError(typeof(PrintEngine), ex.Message);
            }
        }

        /// <summary>
        /// Exports a label to image.
        /// </summary>
        /// <param name="label">The label to export.</param>
        /// <param name="print">Flag, which indicates whether the label is to be printed.</param>
        public static string ExportToImage(ThermalLabel label, bool print)
        {
            try
            {
                var imageFormat = (ImageFormat) Enum.Parse(typeof (ImageFormat), Settings.Default.SelectedImageType);
                using (var printJob = new PrintJob())
                {
                    var fileName = Path.Combine(Settings.Default.ExportImagePath, "LabelImage.Png");
                    printJob.ThermalLabel = label;
                    printJob.ExportToImage(fileName, new ImageSettings(imageFormat), 203);

                    if (print)
                    {
                        PrintLabel(fileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(typeof(PrintEngine), ex.Message);
                return Message.LabelExportError;
            }

            return string.Empty;
        }

        /// <summary>
        /// Exports a label to pdf.
        /// </summary>
        /// <param name="label">The label to export.</param>
        public static string ExportToPdf(ThermalLabel label)
        {
            try
            {
                using (var printJob = new PrintJob())
                {
                    var fileName = Path.Combine(Settings.Default.ExportImagePath, "LabelImage.pdf");
                    printJob.ThermalLabel = label;
                    printJob.ExportToPdf(fileName, 203);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(typeof(PrintEngine), ex.Message);
                return Message.LabelExportError;
            }

            return string.Empty;
        }

        /// <summary>
        /// Exports a label to xml.
        /// </summary>
        /// <param name="label">The label to export.</param>
        public static string ExportToXml(ThermalLabel label)
        {
            try
            {
                var fileName = Path.Combine(Settings.Default.ExportImagePath, "LabelImage.xml");
                using (var printJob = new PrintJob())
                {
                    File.WriteAllText(fileName, label.GetXmlTemplate());
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(typeof(PrintEngine), ex.Message);
                return Message.LabelExportError;
            }

            return string.Empty;
        }

        /// <summary>
        /// Prints a label image.
        /// </summary>
        /// <param name="file">The file path of the label to print.</param>
        internal static void PrintLabel(string file)
        {
            var pd = new PrintDocument();

            // Disable the print pop up.
            pd.PrintController = new StandardPrintController();

            // Set the default margins
            pd.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            pd.PrinterSettings.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);

            try
            {
                pd.PrintPage += (sender, args) =>
                {
                    System.Drawing.Image i = System.Drawing.Image.FromFile(file);

                    // Adjust the size of the image to the page to print the full image without losing any part of the image.
                    System.Drawing.Rectangle m = args.MarginBounds;

                    // Logic below maintains the aspect ratio.
                    if ((double)i.Width / (double)i.Height > (double)m.Width / (double)m.Height) // image is wider
                    {
                        m.Height = (int)((double)i.Height / (double)i.Width * (double)m.Width);
                    }
                    else
                    {
                        m.Width = (int)((double)i.Width / (double)i.Height * (double)m.Height);
                    }

                    // Calculating optimal orientation.
                    pd.DefaultPageSettings.Landscape = m.Width > m.Height;

                    // Putting image in center of page.
                    m.Y = (int)((((PrintDocument)(sender)).DefaultPageSettings.PaperSize.Height - m.Height) / 2);
                    m.X = (int)((((PrintDocument)(sender)).DefaultPageSettings.PaperSize.Width - m.Width) / 2);
                    args.Graphics.DrawImage(i, m);
                };

                pd.Print();
            }
            catch (Exception ex)
            {
                Logger.LogError(typeof(PrintEngine), ex.Message);
                throw;
            }
        }

        #endregion
    }

    //public class Test
    //{
    //    public string MultiLineText1 { get; set; }
    //}
}
