﻿// -----------------------------------------------------------------------
// <copyright file="App.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Threading;
using Microsoft.Win32;
using Neodynamic.SDK.Printing;
using Nouvem.LabelDesigner.View.Designer;
using Nouvem.Logging;

namespace Nouvem.LabelDesigner
{
    using System;
    using System.Data.Entity.Core.EntityClient;
    using System.Data.SqlClient;
    using System.IO;
    using System.Xml.Linq;
    using System.Windows;
    using Neodynamic.Windows.WPF.ThermalLabelEditor;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.LabelDesigner.Properties;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        private Logger log = new Logger();

        /// <summary>
        /// Overriden base startup.
        /// </summary>
        /// <param name="e">Start up event argument.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
         
            ThermalLabel.LicenseOwner = "nouvem limited-Ultimate Edition-Developer License";
            ThermalLabel.LicenseKey = "TGYVQ5YUM3758TPAJS26FKM5ZERNVWKQQ4J8FTDTTAHB4UUGBZ5Q";
            ThermalLabelEditor.LicenseOwner = "nouvem limited-Developer License";
            ThermalLabelEditor.LicenseKey = "UTR85UARVC2NNMJWCG8PG8Z4EX7U6TZWT2C359EX4YKN6CNBYAEA";
            
            //if (string.IsNullOrEmpty(Settings.Default.ConnectionString) || string.IsNullOrEmpty(Settings.Default.ApplicationConnString))
            //{
            this.GetConnectionSettings();
            //}

            //this.SetConnectionString();
        }

        //public static void StartApp()
        //{

        //    Application app = new Application();
        //    app.Run(new DesignerContainerView());
          
        //}

        /// <summary>
        /// Uses the back up file connection string.
        /// </summary>
        /// <returns>A flag, indicating whether a connection can be made to the database.</returns>
        private bool GetConnectionSettings()
        {
            try
            {
                var key = Registry.CurrentUser.OpenSubKey(@"Nouvem\Settings");
                if (key != null)
                {
                    var conn = key.GetValue(Constant.ConnectionString);
                    var appConnString = key.GetValue(Constant.ApplicationConnString);
                    var user = key.GetValue(Constant.LoggedInUser);
                    key.Close();

                    Settings.Default.ConnectionString = conn != null ? Security.Security.DecryptString((byte[])conn).Replace("NouvemEntities", "NouvemLabelEntities") : string.Empty;
                    Settings.Default.ApplicationConnString = appConnString != null ? Security.Security.DecryptString((byte[])appConnString) : string.Empty;
                    Settings.Default.LoggedInUser = user != null ? user.ToString() : string.Empty;
                    return true;
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Handler for the application unhandled exceptions.
        /// </summary>
        /// <param name="sender">The sender object</param>
        /// <param name="e">The event args</param>
        private void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // log it
            this.log.LogError(this.GetType(), string.Format("OnDispatcherUnhandledException()  Message: {0}, Inner Exception: {1},  Stack Trace: {2}", e.Exception.Message, e.Exception.InnerException, e.Exception.StackTrace));
        }
    }
}
