﻿// -----------------------------------------------------------------------
// <copyright file="LabelRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Forms;

namespace Nouvem.LabelDesigner.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using Nouvem.LabelDesigner.Global;
    using Nouvem.LabelDesigner.Model.DataLayer.Interface;
    using Nouvem.LabelDesigner.Model.Enum;
    using Nouvem.LabelDesigner.Properties;
    using Nouvem.Logging;

    public class LabelRepository : ILabelRepository
    {
        /// <summary>
        /// The logger reference.
        /// </summary>
        private ILogger log = new Logger();

        /// <summary>
        /// Retrieve all the labels.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<Label> GetLabels()
        {
            this.log.LogInfo(this.GetType(), "GetLabels(): Attempting to retrieve all the labels");
            var labels = new List<Label>();

            try
            {
                using (var entities = new NouvemLabelEntities())
                {
                    labels = entities.Labels.OrderBy(x => x.Name).ToList();
                    this.log.LogInfo(this.GetType(), string.Format("{0} labels retrieved", labels.Count()));
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                SystemMessage.Write(MessageType.Issue, string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return labels;
        }

        /// <summary>
        /// Retrieve all the label fonts.
        /// </summary>
        /// <returns>A collection of labels.</returns>
        public IList<LabelFont> GetLabelFonts()
        {
            this.log.LogInfo(this.GetType(), "GetLabelFontss(): Attempting to retrieve all the label fonts");
            var labels = new List<LabelFont>();

            try
            {
                using (var entities = new NouvemLabelEntities())
                {
                    labels = entities.LabelFonts.ToList();
                    this.log.LogInfo(this.GetType(), string.Format("{0} label fonts retrieved", labels.Count()));
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                SystemMessage.Write(MessageType.Issue, string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return labels;
        }

        /// <summary>
        /// Saves a label.
        /// </summary>
        /// <param name="label">The label to save.</param>
        /// <returns>A flag, indicating a successful save or not.</returns>
        public bool SaveLabel(Label label)
        {
            this.log.LogInfo(this.GetType(), string.Format("SaveLabel(): Attempting to save label - Name:{0}", label.Name));

            try
            {
                using (var entities = new NouvemLabelEntities())
                {
                    var dblabel = entities.Labels.FirstOrDefault(x => x.Name.Trim().Equals(label.Name.Trim()) && !x.Deleted);
                    if (dblabel == null)
                    {
                        // new label
                        entities.Labels.Add(label);
                    }
                    else
                    {
                        // update label
                        dblabel.Name = label.Name;
                        dblabel.LabelImage = label.LabelImage;
                        dblabel.LabelFile = label.LabelFile;
                        dblabel.DataSource = label.DataSource;
                        dblabel.Deleted = label.Deleted;
                        dblabel.Orientation = label.Orientation;
                    }
                 
                    entities.SaveChanges();
                    this.log.LogInfo(this.GetType(), string.Format("SaveLabel(): Label - Name:{0} successfully saved", label.Name));
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                SystemMessage.Write(MessageType.Issue, string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieve all the label groups.
        /// </summary>
        /// <returns>A collection of label groups.</returns>
        public IList<LabelGroup> GetLabelGroups()
        {
            this.log.LogInfo(this.GetType(), "GetLabelGroups(): Attempting to retrieve all the label groups");
            var labelGroups = new List<LabelGroup>();

            try
            {
                using (var entities = new NouvemLabelEntities())
                {
                    labelGroups = entities.LabelGroups.Where(x => !x.Deleted).ToList();
                    this.log.LogInfo(this.GetType(), string.Format("{0} label groups retrieved", labelGroups.Count()));
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                SystemMessage.Write(MessageType.Issue, string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                MessageBox.Show(Shared.Localisation.Message.DatabaseConnectionFailure);
            }

            return labelGroups;
        }

        /// <summary>
        /// Add or updates the label groups list.
        /// </summary>
        /// <param name="labelGroups">The label groups to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateLabelGroups(IList<LabelGroup> labelGroups)
        {
            this.log.LogInfo(this.GetType(), "AddOrUpdateLabelGroups(): Attempting to update the label groups");

            try
            {
                using (var entities = new NouvemLabelEntities())
                {
                    labelGroups.ToList().ForEach(x =>
                    {
                        if (x.LabelGroupID== 0)
                        {
                            // new
                            var newLabelGroup = new LabelGroup
                            {
                                Name = x.Name,
                                Deleted = false
                            };

                            entities.LabelGroups.Add(newLabelGroup);
                        }
                        else
                        {
                            // update
                            var dbLabelGroup =
                                entities.LabelGroups.FirstOrDefault(
                                    group => group.LabelGroupID == x.LabelGroupID && !group.Deleted);

                            if (dbLabelGroup != null)
                            {
                                dbLabelGroup.Name = x.Name;
                                dbLabelGroup.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.log.LogInfo(this.GetType(), "Label groups successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                SystemMessage.Write(MessageType.Issue, string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the gs1's.
        /// </summary>
        /// <returns>A collection of gs1's.</returns>
        public IList<Gs1AIMaster> GetGs1Master()
        {
            this.log.LogInfo(this.GetType(), "GetGs1Master: Attempting to retrieve all the gs1 masters");
            var gs1s = new List<Gs1AIMaster>();

            try
            {
                using (var entities = new NouvemLabelEntities())
                {
                    gs1s = entities.Gs1AIMaster.Where(x => !x.Deleted).ToList();
                    this.log.LogInfo(this.GetType(), string.Format("{0} lists retrieved", gs1s.Count()));
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                SystemMessage.Write(MessageType.Issue, string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return gs1s;
        }

        /// <summary>
        /// Retrieves the label sp names.
        /// </summary>
        /// <returns>A collection of sp label names.</returns>
        public IList<string> GetLabelSpNames()
        {
            var names = new List<string>();

            try
            {
                using (var conn = new SqlConnection(Settings.Default.ApplicationConnString))
                {
                    var command = new SqlCommand("GetLabelSpNames", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    var dataReader = command.ExecuteReader();
                    var dataResult = new DataTable();
                    dataResult.Load(dataReader);
                    conn.Close();

                    foreach (DataRow dr in dataResult.Rows)
                    {
                        names.Add(dr["Name"].ToString());
                    }}
          
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                SystemMessage.Write(MessageType.Issue, string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return names;
        }

        /// <summary>
        /// Retrieves the label data schema.
        /// </summary>
        /// <param name="name">The sp name.</param>
        /// <returns>A data table.(Schema only).</returns>
        public DataTable GetLabelData(string name)
        {
            var dataResult = new DataTable();

            try
            {
                using (var conn = new SqlConnection(Settings.Default.ApplicationConnString))
                {
                    var command = new SqlCommand(name, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    //command.Parameters.Add(new SqlParameter("@StockTransactionID", 0));
                    conn.Open();
                    var dataReader = command.ExecuteReader(CommandBehavior.SchemaOnly);
                    dataResult = dataReader.GetSchemaTable();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                //SystemMessage.Write(MessageType.Issue, string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Updates the test label data.
        /// </summary>
        /// <param name="testData">The test data.</param>
        public void UpdateTestLabelData(TestLabelData testData)
        {

            try
            {
                using (var entities = new NouvemLabelEntities())
                {
                    var data = entities.TestLabelDatas.FirstOrDefault(x => x.TestLabelDataID == testData.TestLabelDataID);
                    if (data != null)
                    {
                        data = testData;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Deletes the label.
        /// </summary>
        /// <param name="label">The label to delete</param>
        public bool DeleteLabel(Label label)
        {
            this.log.LogInfo(this.GetType(), string.Format("Attempting to delete label id:{0}", label.LabelID));
            try
            {
                using (var entities = new NouvemLabelEntities())
                {
                    var dbLabel = entities.Labels.FirstOrDefault(x => x.LabelID == label.LabelID);
                    if (dbLabel != null)
                    {
                        dbLabel.Deleted = true;
                        entities.SaveChanges();
                        this.log.LogInfo(this.GetType(), "Label deleted");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the test data.
        /// </summary>
        public void UpdateTestLabelData(DataTable dt)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Settings.Default.ApplicationConnString))
                {
                    conn.Open();
                    var command = new SqlCommand("UpdateTestLabelData", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@datatable", dt);
                    command.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Retrieves the label data result row.
        /// </summary>
        public DataTable GetTestLabelData()
        {
            var dataResult = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(Settings.Default.ApplicationConnString))
                {
                    conn.Open();
                    var command = new SqlCommand("LabelData", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    //command.Parameters.Add(new SqlParameter("@StockTransactionID", 0));
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }
    }
}
