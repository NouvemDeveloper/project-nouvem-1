﻿// -----------------------------------------------------------------------
// <copyright file="Gs1Data.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Nouvem.LabelDesigner.Global;
using Nouvem.LabelDesigner.ViewModel;

namespace Nouvem.LabelDesigner.Model.BusinessObject
{
    using System.ComponentModel;
    using GalaSoft.MvvmLight.Messaging;

    public class Gs1Data : INotifyPropertyChanged
    {
        public Gs1Data()
        {
            
        }
        /// <summary>
        /// The gs1 selection flag.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Gets or sets the application identifier.
        /// </summary>
        public string AI { get; set; }

        /// <summary>
        /// Gets or sets the identifier description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the data length.
        /// </summary>
        public int DataLength { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the data is of fixed length.
        /// </summary>
        public bool IsFixedLength { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the gs1 is selected.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.NotifyPropertyChanged("IsSelected");
                //Messenger.Default.Send(value, Token.Gs1Selected);
            }
        }

        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The event raise.
        /// </summary>
        /// <param name="propertyName">The property whose value will be broadcast.</param>
        private void NotifyPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
