﻿// -----------------------------------------------------------------------
// <copyright file="LabelData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesigner.Model.BusinessObject
{
    using System;
    using System.ComponentModel;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.LabelDesigner.Global;

    public class LabelData : INotifyPropertyChanged
    {
        /// <summary>
        /// The selection field.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Gets or sets the stored procedure identifier.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the stored procedure field.
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// Gets or sets the stored procedure field sample data.
        /// </summary>
        public string SampleData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the field is selected for the label.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
                //Messenger.Default.Send(value, Token.StringBuilder);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the field is a fixed length (false = variable length).
        /// </summary>
        public bool IsFixedLength { get; set; }

        /// <summary>
        /// Gets or sets a the length of the field data.
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// Gets or sets a the field data type.
        /// </summary>
        public Type DataType { get; set; }

        /// <summary>
        /// Gets or sets a the character used to pad left on a fixed length field.
        /// </summary>
        public char PadLeftChar { get; set; }

        /// <summary>
        /// Gets or sets a the character used to pad right on a fixed length field.
        /// </summary>
        public char PadRightChar { get; set; }

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
