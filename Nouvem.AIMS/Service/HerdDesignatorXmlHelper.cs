﻿// -----------------------------------------------------------------------
// <copyright file="Errors.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Service
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using Nouvem.AIMS.BusinessLogic;
    using Nouvem.Shared;

    /// <summary>
    /// Helper class for the web service calls which is used to generate and parse xml herd designator requests.
    /// </summary>
    public class HerdDesignatorXmlHelper
    {
        #region Fields

        /// <summary>
        /// Instance of the designator object.
        /// </summary>
        private HerdDesignator designator;

        /// <summary>
        /// The version of the xml being sent.
        /// </summary>
        private string version;

        /// <summary>
        /// The encoding of the xml being sent.
        /// </summary>
        private string encoding;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="HerdDesignatorXmlHelper"/> class.
        /// </summary>
        /// <param name="designator">The <see cref="HerdDesignatorXmlHelper"/> to be processed for transmission to AIMs.</param>
        /// <param name="version">The version of the xml file which defaults to 1.0.</param>
        /// <param name="encoding">The encoding of the xml file which defaults to UTF-8.</param>
        public HerdDesignatorXmlHelper(HerdDesignator designator, string version = "1.0", string encoding = "UTF-8")
        {
            this.designator = designator;
            this.version = version;
            this.encoding = encoding;
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Method to create an xml request which can be sent to the AIMs service.
        /// </summary>
        /// <returns>An xml document which can be sent to the service.</returns>
        public XmlDocument CreateXmlRequest(string fileName = "", bool canSave = false, string logPath = null)
        {
            var xmlDocument = new XDocument();

            if (this.designator != null)
            {
                xmlDocument = new XDocument(
                new XDeclaration(this.version, this.encoding, "true"),
                new XElement(
                    "batchJob",
                        new XAttribute("fileName", this.designator.FileName != null ? this.designator.FileName : string.Empty),
                        new XAttribute("dateGen", this.designator.DateGen != null ? this.designator.DateGen : string.Empty),
                        new XAttribute("numTrans", this.designator.NumTrans != null ? this.designator.NumTrans : string.Empty)));

                if (xmlDocument != null)
                {
                    var newNode = new XElement(
                        "batchTrans",
                        new XAttribute("transType", this.designator.TransType != null ? this.designator.TransType : string.Empty),
                        new XElement(
                            "herdDesignatorRequest",
                            new XElement("speciesId", this.designator.SpeciesId != null ? this.designator.SpeciesId : string.Empty),
                            new XElement("businessId", this.designator.BusinessId != null ? this.designator.BusinessId : string.Empty)));

                    xmlDocument.Element("batchJob").Add(newNode);
                }
            }

            if (canSave)
            {
                // Save save if specified to do so
                string toAimsPath = System.IO.Path.Combine(logPath, "toAims");

                if (Directory.Exists(toAimsPath))
                {
                    string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                    xmlDocument.Save(fullPath);
                }
            }

            return xmlDocument.ToXmlDocument();
        }

        /// <summary>
        /// Extract the HerdDesignator object retrieve from the xml response from the AIMs.
        /// </summary>
        /// <param name="xmlResponse">The response from the service call.</param>
        /// <returns>A HerdDesignator object detailing the service call response.</returns>
        public HerdDesignator ParseXmlResponse(string xmlResponse, string fileName = "", bool canSave = false, string logPath = "")
        {
            var designatorLocal = HerdDesignator.CreateNewHerdDesignator();

            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var linqToXmlDocument = XDocument.Parse(xmlResponse);

                if (linqToXmlDocument != null && this.designator != null)
                {
                    // Load the individual sections as the return format varies depending on the response.
                    var jobDescendants = linqToXmlDocument.Descendants("batchJobResponse");
                    var transDescendants = linqToXmlDocument.Descendants("batchTransResponse");
                    var herdDescendants = linqToXmlDocument.Descendants("herdDesignatorResponse");

                    if (!transDescendants.Any() && !herdDescendants.Any())
                    {
                        designatorLocal = (from itemJob in linqToXmlDocument.Descendants("batchJobResponse")
                                           select HerdDesignator.CreateHerdDesignator(
                                              itemJob.FindInElement("fileName"),
                                              itemJob.FindInElement("dateGen"),
                                              itemJob.FindInElement("numTrans"),
                                              string.Empty,
                                              string.Empty,
                                              string.Empty,
                                              itemJob.FindInElement("batchError"))).First<HerdDesignator>();

                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            designatorLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }

                    }
                    else if (!transDescendants.Any() && herdDescendants.Any())
                    {
                        designatorLocal = (from itemJob in linqToXmlDocument.Descendants("batchJobResponse")
                                           from itemHerd in linqToXmlDocument.Descendants("herdDesignatorResponse")
                                           select HerdDesignator.CreateHerdDesignator(
                                              itemJob.FindInElement("fileName"),
                                              itemJob.FindInElement("dateGen"),
                                              itemJob.FindInElement("numTrans"),
                                              string.Empty,
                                              itemHerd.FindInElement("speciesId"),
                                              itemHerd.FindInElement("businessId"),
                                              itemHerd.FindInElement("status"),
                                              itemJob.FindInElement("batchError"),
                                              itemHerd.FindInElement("regionId"),
                                              itemHerd.FindInElement("herdWithinRegionId"))).First<HerdDesignator>();

                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            designatorLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }
                    }
                    else if (transDescendants.Any() && !herdDescendants.Any())
                    {
                        designatorLocal = (from itemJob in linqToXmlDocument.Descendants("batchJobResponse")
                                           from itemTrans in linqToXmlDocument.Descendants("batchTransResponse")
                                           select HerdDesignator.CreateHerdDesignator(
                                              itemJob.FindInElement("fileName"),
                                              itemJob.FindInElement("dateGen"),
                                              itemJob.FindInElement("numTrans"),
                                              itemTrans.FindInElement("transType"),
                                              string.Empty,
                                              string.Empty,
                                              itemJob.FindInElement("batchError"),
                                              string.Empty,
                                              string.Empty)).First<HerdDesignator>();

                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            designatorLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }
                    }
                    else
                    {
                        designatorLocal = (from itemJob in linqToXmlDocument.Descendants("batchJobResponse")
                                           from itemTrans in linqToXmlDocument.Descendants("batchTransResponse")
                                           from itemHerd in linqToXmlDocument.Descendants("herdDesignatorResponse")
                                           select HerdDesignator.CreateHerdDesignator(
                                              itemJob.FindInElement("fileName"),
                                              itemJob.FindInElement("dateGen"),
                                              itemJob.FindInElement("numTrans"),
                                              itemTrans.FindInElement("transType"),
                                              itemHerd.FindInElement("speciesId"),
                                              itemHerd.FindInElement("businessId"),
                                              itemHerd.FindInElement("status"),
                                              itemJob.FindInElement("batchError"),
                                              itemHerd.FindInElement("regionId"),
                                              itemHerd.FindInElement("herdWithinRegionId"))).First<HerdDesignator>();

                        // Get, and decipher, the batch errors.
                        var batchErrors = linqToXmlDocument.Descendants("batchError").Select(x => x.Value);

                        foreach (var item in batchErrors)
                        {
                            designatorLocal.Error += Errors.GetErrors()
                            .Where(x => x.Key == item)
                            .Select(x => x.Value).FirstOrDefault() + ",";
                        }
                    }
                }

                if (canSave)
                {
                    string toAimsPath = System.IO.Path.Combine(logPath, "fromAims");

                    if (Directory.Exists(toAimsPath))
                    {
                        string fullPath = System.IO.Path.Combine(toAimsPath, fileName);
                        linqToXmlDocument.Save(fullPath);
                    }
                }
            }

            return designatorLocal;
        }

        #endregion
    }
}
