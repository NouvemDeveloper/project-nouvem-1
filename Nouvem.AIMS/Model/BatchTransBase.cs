﻿// -----------------------------------------------------------------------
// <copyright file="BatchTransBase.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Model
{
    /// <summary>
    /// Class which models the common attributes of the BatchTrans node.
    /// </summary>
    public class BatchTransBase
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BatchTransBase"/> class.
        /// </summary>
        protected BatchTransBase()
        {
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets or sets the transaction type. MOV is a movement transaction request, for example.
        /// </summary>
        public string TransType { get; set; }

        #endregion
    }
}

