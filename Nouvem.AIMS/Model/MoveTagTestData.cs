﻿// -----------------------------------------------------------------------
// <copyright file="MovementHistoryResponse.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Class that holds the historic test data results stored in the AIMS response file
    /// </summary>
    public class MoveTagTestData
    {
        public string TestTypeCode { get; set; }
        public string TestDate { get; set; }
    }
}

