﻿// -----------------------------------------------------------------------
// <copyright file="CountryEligibility.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Class that holds the country eligibility details data results stored in the AIMS response file
    /// </summary>
    public class CountryEligibility
    {
        /// <summary>
        /// The country code sent back 
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// The associated value
        /// </summary>
        public string Value { get; set; }
    }
}
