﻿// -----------------------------------------------------------------------
// <copyright file="Movement.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AIMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Class to hold an animals previous movements
    /// </summary>
    public class Movement
    {
        /// <summary>
        /// location id where animal was moved from
        /// </summary>
        public string MoveFromID { get; set; }

        /// <summary>
        /// location id where animal was moved to
        /// </summary>
        public string MoveToID { get; set; }

        /// <summary>
        /// Date animal was moved
        /// </summary>
        public string MovementDate { get; set; }

        /// <summary>
        /// Date animal was moved
        /// </summary>
        public string MovementOffDate { get; set; }

        /// <summary>
        /// Date animal was moved
        /// </summary>
        public string SubLocation { get; set; }
    }
}
