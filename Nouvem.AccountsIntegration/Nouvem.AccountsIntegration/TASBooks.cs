﻿// -----------------------------------------------------------------------
// <copyright file="TASBooks.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration
{
    using System;
    using System.Collections.Generic;
    using Nouvem.AccountsIntegration.BusinessObject;
    using TASLink;

    public class TASBooks : IAccountsIntegration
    {
        #region field

        /// <summary>
        /// The tas company reference.
        /// </summary>
        private TASBooksAccountingCompany company = new TASBooksAccountingCompany();

        private TradingPartners partners;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TASBooks"/> class.
        /// </summary>
        public TASBooks()
        {            
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region method

        /// <summary>
        /// Create accounts db connection.
        /// </summary>
        /// <param name="path">The db path.</param>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string Connect(string path, string userName, string password)
        {
            this.company = new TASBooksAccountingCompany();
            if (this.company.logIntoTASDataset(path, userName, password))
            {
                return string.Empty;
            }

            // error
            return this.company.Status.userReadableDescription;
        }

        /// <summary>
        /// Posts the sale order .
        /// </summary>
        /// <param name="header">The sale order data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostSaleOrder(InvoiceHeader header)
        {
            var saleOrder = this.company.SalesOrderHeader;
            saleOrder.set_SalesPersonCode(header.SalesPersonCode);
            saleOrder.set_SalesOrderDate(header.InvoiceDate);

            if (saleOrder.SetHeaderInfo(SalesOrderTypes.SalesOrderType_Invoice, 
                header.TradingPartnerCode))
            {
                foreach (var detail in header.Details)
                {
                    if (!saleOrder.AddSalesOrderProductLine(detail.ProductCode, detail.Quantity, UnitSellingPrice_:detail.UnitPrice))
                    {
                        return this.company.Status.userReadableDescription;
                    }
                }

                saleOrder.set_CustomerOrderNumber(header.CustomerOrderNo);
                saleOrder.set_Description(header.Description);
                saleOrder.set_DeliveryAddressLine1(header.DeliveryAddressLine1);
                saleOrder.set_DeliveryAddressLine2(header.DeliveryAddressLine2);
                saleOrder.set_DeliveryAddressLine3(header.DeliveryAddressLine3);
                saleOrder.set_DeliveryAddressLine4(header.DeliveryAddressLine4);

                if (saleOrder.Save())
                {
                    return string.Empty;
                }
            }

            return this.company.Status.userReadableDescription;
        }

        /// <summary>
        /// Exports a purchase order to Sage.
        /// </summary>
        /// <param name="header">The purchase order details.</param>
        /// <returns>Empty string if successful, otherwise error message.</returns>
        public string PostPurchaseOrder(BusinessObject.InvoiceHeader header)
        {
            return "";
        }

        /// <summary>
        /// Posts the sale order invoice.
        /// </summary>
        /// <param name="header">The invoice data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostSalesInvoice(InvoiceHeader header)
        {
            var stockTransaction = this.company.StockTransactionHeader;
            stockTransaction.set_UseTASInvoiceNumbering(header.UseTASInvoiceNumbering);
            stockTransaction.set_SalesPersonCode(header.SalesPersonCode);
           
            if (stockTransaction.SetHeaderInfo(
                header.TradingPartnerCode,
                StockTransactionGroups.Sale,
                StockTransactionTypes.StockTransactionType_Invoice,
                header.InvoiceDate,
                header.CustomerOrderNo,
                header.InvoiceNo,
                header.Description))
            {
                foreach (var detail in header.Details)
                {
                    if (!stockTransaction.AddStockTransactionProductLine(
                        detail.ProductCode,
                        detail.Quantity,
                        detail.UnitPrice))
                    {
                        return this.company.Status.userReadableDescription;
                    }
                }

                stockTransaction.set_DeliveryAddressLine1(header.DeliveryAddressLine1);
                stockTransaction.set_DeliveryAddressLine2(header.DeliveryAddressLine2);
                stockTransaction.set_DeliveryAddressLine3(header.DeliveryAddressLine3);
                stockTransaction.set_DeliveryAddressLine4(header.DeliveryAddressLine4);

                if (stockTransaction.Save())
                {
                    return string.Empty;
                }
            }

            return this.company.Status.userReadableDescription;
        }

        /// <summary>
        /// Retrieves the tas partners.
        /// </summary>
        /// <returns>The tas partners.</returns>
        public Tuple<List<BusinessPartner>, string> GetPartners(string path)
        {
            var localPartners = new List<BusinessPartner>();
            var error = string.Empty;
            this.partners = this.company.TradingPartners;
            this.partners.get_IncludeSuppliers();

            try
            {
                for (int i = 1; i <= this.partners.Count; i++)
                {
                    var partner = new BusinessPartner();
                    partner.Name = this.partners[i].get_Name();
                    partner.AddressLine1 = this.partners[i].get_addressLine1();
                    partner.AddressLine2 = this.partners[i].get_addressLine2();
                    partner.AddressLine3 = this.partners[i].get_addressLine3();
                    partner.Country = this.partners[i].get_Country();
                    partner.CustomerAccountCodeID = this.partners[i].get_Customer_AccountCodeID();
                    partner.CustomerAccountsContact = this.partners[i].get_Customer_AccountsContact();
                    partner.CustomerAccountsContactEmailAddress = this.partners[i].get_Customer_AccountsContactEmailAddress();
                    partner.CustomerAccountsContactMobileTelNumber = this.partners[i].get_Customer_AccountsContactMobileTelNumber();
                    partner.CustomersAccountsContactSalutation = this.partners[i].get_Customer_AccountsContactSalutation();
                    partner.CustomerAccountsContactTelNumber = this.partners[i].get_Customer_AccountsContactTelNumber();               
                    partner.CustomerNotes = this.partners[i].get_Customer_Notes();
                    partner.CustomerPostCode = this.partners[i].get_Customer_PostCode();
                    partner.SalesContact = this.partners[i].get_Customer_SalesContact();
                    partner.SalesContactEmailAddress = this.partners[i].get_Customer_SalesContactEmailAddress();
                    partner.SalesContactFaxNumber = this.partners[i].get_Customer_SalesContactFaxNumber();
                    partner.SalesContactMobileTelNumber = this.partners[i].get_Customer_SalesContactMobileTelNumber();
                    partner.SalesContactSalutation = this.partners[i].get_Customer_SalesContactSalutation();
                    partner.SalesContactTelNumber = this.partners[i].get_Customer_SalesContactTelNumber();
                    partner.SupplierAccountCodeID = this.partners[i].get_Supplier_AccountCodeID();
                    partner.SupplierAccountsContact = this.partners[i].get_Supplier_AccountsContact();
                    partner.SupplierAccountsContactEmailAddress = this.partners[i].get_Supplier_AccountsContactEmailAddress();
                    partner.SupplierAccountsContactMobileTelNumber = this.partners[i].get_Supplier_AccountsContactMobileTelNumber();
                    partner.SuppliersAccountsContactSalutation = this.partners[i].get_Supplier_AccountsContactSalutation();
                    partner.SupplierAccountsContactTelNumber = this.partners[i].get_Supplier_AccountsContactTelNumber();

                    localPartners.Add(partner);               
                }
            }
            catch (Exception ex)
            {
                error = ex.Message;      
            }           
                    
            return Tuple.Create(localPartners, error);
        }

        /// <summary>
        /// Close session and release resource.
        /// </summary>
        public void Logout()
        {
            if (this.company != null)
            {
                this.company.logOutOfTASDataset();
                this.KillComObject(this.company);
            }
        }

        /// <summary>
        /// Post a purchase invoice.
        /// </summary>
        /// <param name="header">The header data.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostPurchaseInvoice(BusinessObject.InvoiceHeader header)
        {
            return string.Empty;
        }

        /// <summary>
        /// Post a purchase invoice.
        /// </summary>
        /// <param name="header">The header data.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        public string PostPurchaseInvoiceCredit(InvoiceHeader header)
        {
            return string.Empty;
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Explicity kills the com object.
        /// </summary>
        /// <param name="comObject">The com object to kill.</param>
        private void KillComObject(object comObject)
        {
            if (comObject != null)
            {
                do{} while (System.Runtime.InteropServices.Marshal.ReleaseComObject(comObject) > 0);
                comObject = null;
            }
        }

        public string ExportProducts(IList<BusinessObject.Product> products)
        {
            throw new NotImplementedException();
        }

        public Tuple<List<BusinessObject.Product>, string> GetProducts(string directory)
        {
            throw new NotImplementedException();
        }

        public Tuple<List<Prices>, string> GetPrices(string path)
        {
            throw new NotImplementedException();
        }

        public Tuple<List<BusinessObject.Product>, string> GetSpecialPrices(string path)
        {
            throw new NotImplementedException();
        }

        public string PostSalesCreditNote(InvoiceHeader header)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
