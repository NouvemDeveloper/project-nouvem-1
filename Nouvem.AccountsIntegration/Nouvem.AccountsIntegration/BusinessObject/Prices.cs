﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.AccountsIntegration.BusinessObject
{
    public class Prices
    {
        /// <summary>
        /// Gets or sets the associated product code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public double CustomerPrice { get; set; }

        /// <summary>
        /// Gets or sets the edit date.
        /// </summary>
        public DateTime EditDate { get; set; }

        /// <summary>
        /// Gets or sets the associated price band.
        /// </summary>
        public string PriceBand { get; set; }

        /// <summary>
        /// Gets or sets the associated price band.
        /// </summary>
        public string Product { get; set; }

        /// <summary>
        /// The product code.
        /// </summary>
        public string PartnerCode { get; set; }

        /// <summary>
        /// The product name.
        /// </summary>
        public string PartnerName { get; set; }
    }
}
