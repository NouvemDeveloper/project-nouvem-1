﻿



-- =============================================
-- Author:		brian murray
-- Create date: 19/07/16
-- Description:	Sets the 0 cost prices, using the cost price - @percentage
-- =============================================
CREATE PROCEDURE [dbo].[DM_UPdateCostPriceMethods] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   DECLARE 
	 @PriceListDetailId int,
	 @INMasterId int,	
	 @CostPriceListId INT = (SELECT PriceListID FROM PriceList WHERE CurrentPriceListName = 'Cost Price List'),
	 @BasePriceListId INT = (SELECT PriceListID FROM PriceList WHERE CurrentPriceListName = 'Base Price List')


	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT p.PriceListDetailID,
			p.INMasterID			
     FROM PriceListDetail p where p.PriceListID = @CostPriceListId
 
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO @PriceListDetailId,
		  @INMasterId
		 

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	    DECLARE @BasePriceMethod int = (SELECT NouPriceMethodID FROM PriceListDetail where PriceListID = @BasePriceListId and INMasterID = @INMasterId)
		DECLARE @BaseOrderMethod int = (SELECT NouOrderMethodID FROM PriceListDetail where PriceListID = @BasePriceListId and INMasterID = @INMasterId)

		if (@BasePriceMethod is not null)
		update PriceListDetail set NouOrderMethodID = @BaseOrderMethod, NouPriceMethodID = @BasePriceMethod where PriceListDetailID = @PriceListDetailId
	   

	 FETCH NEXT FROM PRICE_CURSOR
		INTO @PriceListDetailId,
		  @INMasterId
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
	
END