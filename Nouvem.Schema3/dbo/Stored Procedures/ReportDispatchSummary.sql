﻿


-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the dispatch summary data.
-- =============================================
CREATE PROCEDURE [dbo].[ReportDispatchSummary] 
	-- Add the parameters for the stored procedure here
	@DispatchID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @BasePriceListId int = (SELECT PriceListID FROM PriceList WHERE CurrentPriceListName = 'Base Price List')

SELECT        
				       
		SUM(dd.WeightDelivered) as WeightDelivered,
		SUM(dd.QuantityDelivered) as Qty,
		MAX(dd.UnitPriceAfterDiscount) as UnitPriceAfterDiscount,
		AVG(dd.UnitPrice) as UnitPrice,
		SUM(dd.TotalExclVAT) as TotalExclVAT,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		i.INGroupID,
		g.Name AS 'GroupName',
		b.BPMasterID,
		b.Name,
       CASE WHEN (SELECT NouPriceMethodID FROM PriceListDetail WHERE INMasterID = MAX(i.INMasterID) AND PriceListDetail.PriceListID = @BasePriceListId) = 1 
		THEN 
		CASE WHEN SUM(dd.QuantityDelivered) = 0 THEN 0 ELSE SUM(dd.TotalExclVAT) / SUM(dd.QuantityDelivered) END 
		ELSE 
		CASE WHEN SUM(dd.WeightDelivered) = 0 THEN 0 ELSE SUM(dd.TotalExclVAT) / SUM(dd.WeightDelivered) END 
		END AS 'Avg Price'	   

    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN BPMaster b
	      ON d.BPMasterID_Customer = b.BPMasterID	
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID	
	   INNER JOIN INGroup g
	      ON g.INGroupID = i.INGroupID
       
     WHERE dd.Deleted IS NULL

	   GROUP BY i.Code,i.Name,i.INGroupID,g.Name,b.BPMasterID, b.Name

    

     END
