﻿
-- =============================================
-- Author:		Brian Murray
-- Create date: 16/05/2015
-- Description:	Removes all licences details for the db (for testing purposes)
-- =============================================
CREATE PROCEDURE [dbo].[RemoveLicences] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update usermaster set LicenceDetailID = NULL 
	update devicemaster set LicenceDetailID = null
	delete NouLicenceTypeRule
    delete from LicenceDetail
    delete from Licence
   
END

