﻿-- =============================================
-- Author:		brian murray
-- Create date: 22/08/16
-- Description:	Updates a dispatch dockets totals to match it's lines totals.
-- =============================================
CREATE PROCEDURE UpdateDispatchDocketTotal
	-- Add the parameters for the stored procedure here
	@ARDispatchId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @Total decimal(18,2) = (SELECT SUM(TotalExclVAT) FROM ARDispatchDetail WHERE ARDispatchID = @ARDispatchId and deleted is null)
    Update ARDispatch SET SubTotalExVAT  = @Total, GrandTotalIncVAT = @Total where ARDispatchID = @ARDispatchId
END