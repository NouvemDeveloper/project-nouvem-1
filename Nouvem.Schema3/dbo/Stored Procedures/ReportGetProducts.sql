﻿-- =============================================
-- Author:		brian murray
-- Create date: 05/04/2016
-- Description:	Retrieves the products
-- =============================================
CREATE PROCEDURE ReportGetProducts
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT INMasterID, INGroupID, Code, Name FROM INMaster
END
