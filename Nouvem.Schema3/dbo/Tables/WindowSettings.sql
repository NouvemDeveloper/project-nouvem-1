﻿CREATE TABLE [dbo].[WindowSettings] (
    [WindowSettingsID] INT           IDENTITY (1, 1) NOT NULL,
    [UserMasterID]     INT           NOT NULL,
    [Name]             NVARCHAR (50) NOT NULL,
    [PositionTop]      FLOAT (53)    NOT NULL,
    [PositionLeft]     FLOAT (53)    NOT NULL,
    [PositionWidth]    FLOAT (53)    NOT NULL,
    [PositionHeight]   FLOAT (53)    NOT NULL,
    [GridData]         XML           NULL,
    [Deleted]          BIT           NOT NULL,
    CONSTRAINT [PK_WindowSettings] PRIMARY KEY CLUSTERED ([WindowSettingsID] ASC),
    CONSTRAINT [FK_WindowSettings_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID])
);

