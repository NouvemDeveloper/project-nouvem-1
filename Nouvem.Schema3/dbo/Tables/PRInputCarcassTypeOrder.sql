﻿CREATE TABLE [dbo].[PRInputCarcassTypeOrder] (
    [PRInputCarcassTypeOrderID] INT      IDENTITY (1, 1) NOT NULL,
    [PROrderID]                 INT      NOT NULL,
    [CarcassTypeID]             INT      NOT NULL,
    [Selected]                  BIT      NOT NULL,
    [Deleted]                   DATETIME NULL,
    CONSTRAINT [PK_PRInputCarcassTypeOrder] PRIMARY KEY CLUSTERED ([PRInputCarcassTypeOrderID] ASC),
    CONSTRAINT [FK_PRInputCarcassTypeOrder_PROrder] FOREIGN KEY ([PROrderID]) REFERENCES [dbo].[PROrder] ([PROrderID])
);

