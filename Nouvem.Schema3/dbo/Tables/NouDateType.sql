﻿CREATE TABLE [dbo].[NouDateType] (
    [NouDateTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [DateType]      VARCHAR (20) NOT NULL,
    [Deleted]       BIT          NOT NULL,
    CONSTRAINT [PK_NouDateType] PRIMARY KEY CLUSTERED ([NouDateTypeID] ASC)
);

