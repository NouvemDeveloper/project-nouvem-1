﻿CREATE TABLE [dbo].[UserGroup ] (
    [UserGroupID] INT           IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (50) NOT NULL,
    [Deleted]     BIT           NOT NULL,
    CONSTRAINT [PK_UserGroups] PRIMARY KEY CLUSTERED ([UserGroupID] ASC)
);

