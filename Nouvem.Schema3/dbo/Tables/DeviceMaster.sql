﻿CREATE TABLE [dbo].[DeviceMaster] (
    [DeviceID]        INT           IDENTITY (1, 1) NOT NULL,
    [DeviceMAC]       NVARCHAR (20) NOT NULL,
    [DeviceIP]        NVARCHAR (50) NOT NULL,
    [DeviceName]      NVARCHAR (50) NOT NULL,
    [DeviceAlias]     NCHAR (10)    NULL,
    [Remark]          NCHAR (10)    NULL,
    [ActiveFrom]      DATE          NULL,
    [ActiveTo]        DATE          NULL,
    [InActiveFrom]    DATE          NULL,
    [InActiveTo]      DATE          NULL,
    [LicenceDetailID] INT           NULL,
    CONSTRAINT [PK_Device] PRIMARY KEY CLUSTERED ([DeviceID] ASC),
    CONSTRAINT [FK_DeviceMaster_LicenceDetail] FOREIGN KEY ([LicenceDetailID]) REFERENCES [dbo].[LicenceDetail] ([LicenceDetailID])
);

