﻿CREATE TABLE [dbo].[NouCategory] (
    [CategoryID]  INT            IDENTITY (1, 1) NOT NULL,
    [CAT]         NCHAR (1)      NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (100) NOT NULL,
    [Deleted]     DATETIME       NULL,
    CONSTRAINT [PK_nouCategory] PRIMARY KEY CLUSTERED ([CategoryID] ASC)
);

