﻿CREATE TABLE [dbo].[PRSpecOutput] (
    [PROutputID] INT      IDENTITY (1, 1) NOT NULL,
    [PRSpecID]   INT      NOT NULL,
    [INMasterID] INT      NOT NULL,
    [Selected]   BIT      NOT NULL,
    [Deleted]    DATETIME NULL,
    CONSTRAINT [PK_ProductionOutput] PRIMARY KEY CLUSTERED ([PROutputID] ASC),
    CONSTRAINT [fk_PRSpecOutput_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]),
    CONSTRAINT [FK_PRSpecOutput_PRSpec] FOREIGN KEY ([PRSpecID]) REFERENCES [dbo].[PRSpec] ([PRSpecID])
);

