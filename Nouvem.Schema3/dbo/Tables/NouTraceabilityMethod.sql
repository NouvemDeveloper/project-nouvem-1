﻿CREATE TABLE [dbo].[NouTraceabilityMethod] (
    [NouTraceabilityMethodID] INT        IDENTITY (1, 1) NOT NULL,
    [Method]                  NCHAR (20) NOT NULL,
    [Deleted]                 DATETIME   NULL,
    CONSTRAINT [PK_NouTraceabilityMethod] PRIMARY KEY CLUSTERED ([NouTraceabilityMethodID] ASC)
);

