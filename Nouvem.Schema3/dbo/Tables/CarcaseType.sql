﻿CREATE TABLE [dbo].[CarcaseType] (
    [CarcaseTypeID]  INT           IDENTITY (1, 1) NOT NULL,
    [Description]    NVARCHAR (50) NOT NULL,
    [NouSidePortion] INT           NOT NULL,
    [CategoryID]     INT           NOT NULL,
    [UserMasterID]   INT           NOT NULL,
    [DeviceMasterID] INT           NOT NULL,
    [CreationStamp]  DATETIME      NOT NULL,
    [Deleted]        NCHAR (10)    NULL,
    CONSTRAINT [PK_CarcaseType] PRIMARY KEY CLUSTERED ([CarcaseTypeID] ASC)
);

