﻿CREATE TABLE [dbo].[NouTraceabilityType] (
    [NouTraceabilityTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [TraceabilityType]      NVARCHAR (50) NOT NULL,
    [Deleted]               BIT           NOT NULL,
    CONSTRAINT [PK_sStyles] PRIMARY KEY CLUSTERED ([NouTraceabilityTypeID] ASC)
);

