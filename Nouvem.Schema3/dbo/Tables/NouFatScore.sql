﻿CREATE TABLE [dbo].[NouFatScore] (
    [NouFatScore] INT       IDENTITY (1, 1) NOT NULL,
    [Value]       NCHAR (1) NOT NULL,
    [Deleted]     DATETIME  NULL,
    CONSTRAINT [PK_NouFatScore] PRIMARY KEY CLUSTERED ([NouFatScore] ASC)
);

