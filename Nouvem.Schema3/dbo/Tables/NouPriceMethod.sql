﻿CREATE TABLE [dbo].[NouPriceMethod] (
    [NouPriceMethodID] INT          IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (30) NOT NULL,
    [Number]           INT          NOT NULL,
    [Deleted]          BIT          NOT NULL,
    CONSTRAINT [PK_NouPriceMethodID] PRIMARY KEY NONCLUSTERED ([NouPriceMethodID] ASC)
);

