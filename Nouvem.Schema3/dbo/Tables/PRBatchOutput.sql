﻿CREATE TABLE [dbo].[PRBatchOutput] (
    [PRBatchOutputID] INT IDENTITY (1, 1) NOT NULL,
    [PRSpecID]        INT NOT NULL,
    CONSTRAINT [PK_PRBatchOutput] PRIMARY KEY CLUSTERED ([PRBatchOutputID] ASC)
);

