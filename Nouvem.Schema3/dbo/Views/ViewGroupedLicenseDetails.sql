﻿  CREATE VIEW [dbo].[ViewGroupedLicenseDetails]
  AS
  SELECT n.LicenceName, n.IsDevice, MAX(ld.LicenceQTY) AS Quantity,
        SUM(CASE WHEN um.LicenceDetailID is not null or dm.LicenceDetailID is not null 
	             THEN 1 ELSE 0 END) AS 'Allocated'
        
  from Licence l 
	     inner join LicenceDetail ld on ld.LicenceID = l.LicenceID
	     inner join NouLicenceType n on ld.NouLicenceTypeID = n.NouLicenceTypeID
		 left join UserMaster um on um.LicenceDetailID = ld.LicenceDetailID
		 left join DeviceMaster dm on dm.LicenceDetailID = ld.LicenceDetailID
  WHERE l.ValidFrom <= CONVERT(DATE,getdate()) and l.ValidTo >= CONVERT(DATE,getdate())
        AND ld.Deleted = 'false'
  GROUP BY n.LicenceName, n.IsDevice