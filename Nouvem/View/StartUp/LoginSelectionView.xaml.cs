﻿// -----------------------------------------------------------------------
// <copyright file="LoginSelectionView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.StartUp
{
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for LoginSelectionView.xaml
    /// </summary>
    public partial class LoginSelectionView : UserControl
    {
        public LoginSelectionView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxUserId.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToPasswordBox, x => this.PasswordBoxPassword.Focus());
        }

        private void PasswordBoxPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.PasswordBoxPassword.Password, Token.UserPassword);
        }
    }
}
