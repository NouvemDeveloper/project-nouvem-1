﻿
using System;
using System.Diagnostics;
using DevExpress.Xpf.Core;

namespace Nouvem.View.StartUp
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for NouvemSplashScreenView.xaml
    /// </summary>
    public partial class NouvemSplashScreenView : Window, ISplashScreen {
        public NouvemSplashScreenView()
        {
            InitializeComponent();
            this.board.Completed += OnAnimationCompleted;
            this.Trace();
        }

        private void Trace()
        {
            CustomTraceListener myTraceListener = new CustomTraceListener();
            Debug.Listeners.Add(myTraceListener);
            this.TextBoxTrace.Text = myTraceListener.ToString();
        }

        #region ISplashScreen
        public void Progress(double value) {
            progressBar.Value = value;
        }
        public void CloseSplashScreen() {
            this.board.Begin(this);
        }
        public void SetProgressState(bool isIndeterminate) {
            progressBar.IsIndeterminate = isIndeterminate;
        }
        #endregion

        #region Event Handlers
        void OnAnimationCompleted(object sender, EventArgs e) {
            this.board.Completed -= OnAnimationCompleted;
            this.Close();
        }
        #endregion
    }
}

