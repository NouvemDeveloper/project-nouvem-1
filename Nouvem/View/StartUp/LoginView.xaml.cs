﻿// -----------------------------------------------------------------------
// <copyright file="LoginView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Nouvem.View.StartUp
{
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            this.InitializeComponent();
            this.Unloaded += this.OnUnloaded;

            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxUserId.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToPasswordBox, x => this.PasswordBoxPassword.Focus());
            Messenger.Default.Register<string>(this, Token.PasswordRemembered, s =>
            {
                this.PasswordBoxPassword.Password = s;
                this.ControlView.Focus();
            });
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Messenger.Default.Unregister(this);
            this.Unloaded -= this.OnUnloaded;
        }

        private void PasswordBoxPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.PasswordBoxPassword.Password, Token.UserPassword);
        }
    }
}
