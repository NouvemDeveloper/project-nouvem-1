﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationLoadingView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Logging;

namespace Nouvem.View.StartUp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Controls;
    using System.Windows.Threading;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// Interaction logic for ApplicationLoadingView.xaml
    /// </summary>
    public partial class ApplicationLoadingView : Window
    {
        /// <summary>
        /// The module load count.
        /// </summary>
        private int count = 1;

        /// <summary>
        /// The form load timer reference.
        /// </summary>
        private DispatcherTimer loadTimer;

        /// <summary>
        /// The loading modules.
        /// </summary>
        private readonly IList<string> assemblies;

        private ILogger logger = new Logging.Logger();

        public ApplicationLoadingView()
        {
            try
            {
                this.InitializeComponent();
                var assembly = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                var mediaPath = Path.Combine(assembly, "nouvem.mov");
                this.logger.LogInfo(this.GetType(), "Loading application view..");
                this.VideoControl.LoadedBehavior = MediaState.Play;
                this.VideoControl.Source = new Uri(mediaPath);
                Messenger.Default.Register<string>(this, Token.CloseLoadingWindow, s =>
                {
                    this.Close();
                });

                this.assemblies = new List<string>
                {
                    "Application starting...",
                    "Registering UI creation",
                    "Checking database connection",
                    "Generating Nouvem password",
                    "Registering unhandled exceptions",
                    "Creating folder structure",
                    "Deleting old log files",
                    "Retrieving routes",
                    "Setting locale",
                    "Retrieving devices",
                    "Setting local device settings",
                    "Retrieving business partners",
                    "Retrieving price lists",
                    "Retrieving authorisations",
                    "Retrieving products",
                    "Retrieving transaction types",
                    "Retrieving locations",
                    "Retrieving document statuses",
                    "Removing old log ins",
                    "Retrieving traceability data",
                    "Retrieving special prices",
                    "Sending start up command to printers",
                    "Retrieving attribute allocation data"
                };

                this.loadTimer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(150) };
                this.loadTimer.Tick += this.LoadTimerOnTick;
                this.Loaded += this.OnLoaded;
                this.Closing += this.OnClosing;
            }
            catch (Exception e)
            {
                this.logger.LogError(this.GetType(), e.Message);
            }
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            if (this.loadTimer != null)
            {
                this.loadTimer.Tick -= this.LoadTimerOnTick;
                this.loadTimer = null;
            }

            this.Loaded -= this.OnLoaded;
            this.Closing -= this.OnClosing;
        }

        private void OnLoaded(object o, RoutedEventArgs routedEventArgs)
        {
            this.logger.LogInfo(this.GetType(), "Application view loaded");
            if (this.loadTimer != null)
            {
                this.loadTimer.Start();
            }
        }

        private void LoadTimerOnTick(object o, EventArgs eventArgs)
        {
            if (this.count == this.assemblies.Count)
            {
                this.loadTimer?.Stop();
                this.TextBoxTrace.Text = Strings.ModulesLoaded;
                return;
            }

            if (this.TextBoxTrace.Dispatcher != null)
            {
                this.TextBoxTrace.Dispatcher.Invoke(() =>
                {
                    this.TextBoxTrace.Text = this.assemblies.ElementAt(this.count);
                    this.count++;
                });
            }
        }
    }
}
