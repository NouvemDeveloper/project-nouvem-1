﻿// -----------------------------------------------------------------------
// <copyright file="LoginContainerview.cs="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Core;

namespace Nouvem.View.StartUp
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginContainerView : Window
    {
        public LoginContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Unregister(this);
            };

            //this.Loaded += (sender, args) =>
            //{
            //    DXSplashScreen.Close();
            //    this.Activate();
            //};
        
            Messenger.Default.Register<string>(this, Token.CloseLogin, x => this.Close());
        }
    }
}
