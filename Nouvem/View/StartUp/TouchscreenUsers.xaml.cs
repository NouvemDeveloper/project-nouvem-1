﻿using System;
// -----------------------------------------------------------------------
// <copyright file="TouchscreenUsers.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.StartUp
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TouchscreenUsers.xaml
    /// </summary>
    public partial class TouchscreenUsers : Window
    {
        public TouchscreenUsers()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseLogin, s =>
            {

                this.Close();
            });
        }
    }
}
