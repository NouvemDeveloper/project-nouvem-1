﻿// -----------------------------------------------------------------------
// <copyright file="INMasterView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;

namespace Nouvem.View.Inventory
{
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for INMasterView.xaml
    /// </summary>
    public partial class INMasterView : UserControl
    {
        public INMasterView()
        {
            this.InitializeComponent();
            ThreadPool.QueueUserWorkItem(x => this.TextBoxCode.Dispatcher.Invoke(() => this.ComboBoxProductCode.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedInventoryControl, x => this.ComboBoxProductCode.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.TextBoxCode.Focus());
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.TabControl.SelectedIndex == 3)
            {
                this.CheckBoxShowAll.Visibility = Visibility.Visible;
            }
            else
            {
                this.CheckBoxShowAll.Visibility = Visibility.Hidden;
            }
        }
    }
}
