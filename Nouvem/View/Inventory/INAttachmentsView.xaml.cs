﻿// -----------------------------------------------------------------------
// <copyright file="INAttachmentsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Inventory
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for INAttachmentsView.xaml
    /// </summary>
    public partial class INAttachmentsView : UserControl
    {
        public INAttachmentsView()
        {
            this.InitializeComponent();
        }
    }
}
