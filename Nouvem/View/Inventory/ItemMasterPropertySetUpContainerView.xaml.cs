﻿// -----------------------------------------------------------------------
// <copyright file="ItemMasterPropertySetUpContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Inventory
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ItemMasterPropertySetUpContainerView.xaml
    /// </summary>
    public partial class ItemMasterPropertySetUpContainerView : Window
    {
        public ItemMasterPropertySetUpContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseItemMasterSetUpWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
