﻿// -----------------------------------------------------------------------
// <copyright file="INGeneralView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Inventory
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for INGeneralView.xaml
    /// </summary>
    public partial class INGeneralView : UserControl
    {
        public INGeneralView()
        {
            this.InitializeComponent();
        }
    }
}
