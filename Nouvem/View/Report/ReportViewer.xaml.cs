﻿// -----------------------------------------------------------------------
// <copyright file="ReportViewer.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using System.Linq;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Report;

namespace Nouvem.View.Report
{
    using System;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Reporting.WinForms;
    using Microsoft.Office.Interop.Outlook;
    using Nouvem.Global;
    using Nouvem.Logging;
    using Nouvem.Model.Enum;

    /// <summary>
    /// Interaction logic for ReportViewer.xaml
    /// </summary>
    public partial class ReportViewer : Window
    {
        /// <summary>
        /// The application logger.
        /// </summary>
        private Logger log = new Logger();

        /// <summary>
        /// Count when displaying multiple reports.
        /// </summary>
        private int reportCount;

        /// <summary>
        /// The current report data.
        /// </summary>
        private ReportData currentReport;

        /// <summary>
        /// Multiple reports flag.
        /// </summary>
        private bool disableMultipleReports;

        public ReportViewer(ReportData report = null)
        {
            this.InitializeComponent();
            this.DataContext = new ReportViewerViewModel();
        
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.SetExportFormat();
            this.ReportingViewer.ZoomMode = ZoomMode.Percent;
            if (ApplicationSettings.SSRSZoom == 0)
            {
                this.ReportingViewer.ZoomPercent = 100;
            }
            else
            {
                this.ReportingViewer.ZoomPercent = ApplicationSettings.SSRSZoom;
            }

            Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
            
            Messenger.Default.Register<string>(this, Token.CloseReportViewer, s => this.Close());
            Messenger.Default.Register<bool>(this, Token.DisableMultipleReports, b => this.disableMultipleReports = b);
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.AttachReportToEmail, s =>
            {
                if (this.currentReport.Name.CompareIgnoringCase(ApplicationSettings.KeypakReport))
                {
                    Messenger.Default.Send(this.currentReport, Token.EmailingReport);
                    return;
                }

                this.AttachReportToEmail();
            });

            Messenger.Default.Register<string>(this, Token.AttachReportsToEmail, s => this.AttachReportsToEmail());

            this.ReportingViewer.ProcessingMode = ProcessingMode.Remote;
            this.ReportingViewer.ServerReport.ReportServerCredentials.NetworkCredentials
            = new System.Net.NetworkCredential(ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain);
            this.ReportingViewer.ServerReport.Timeout = 1200000;
           
            this.Loaded += (sender, args) =>
            {
                try
                {
                    this.log.LogDebug(this.GetType(), string.Format("Loading report viewer - Report Server URL:{0}", ApplicationSettings.ReportServerPath));
                    this.ReportingViewer.ServerReport.ReportServerUrl = new Uri(ApplicationSettings.ReportServerPath);
                    this.log.LogDebug(this.GetType(), "Loaded");
                    if (report != null)
                    {
                        this.SetReportPath(report);
                    }
                }
                catch (System.Exception ex)
                {
                    NouvemMessageBox.Show(string.Format("Path issue:{0} {1}", ApplicationSettings.ReportServerPath, ex.Message));
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.ReportViewerHeight = this.Height;
                WindowSettings.ReportViewerLeft = this.Left;
                WindowSettings.ReportViewerTop = this.Top;
                WindowSettings.ReportViewerWidth = this.Width;
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
                ApplicationSettings.SSRSZoom = this.ReportingViewer.ZoomPercent;
            };

            // Register for the incoming report to display.
            Messenger.Default.Register<Model.BusinessObject.ReportData>(this, Token.SetReportPath, r =>
                {
                    this.SetReportPath(r);
                });

            // Register for the incoming report to display.
            Messenger.Default.Register<Model.BusinessObject.ReportData>(this, Token.SetReportPathAndExport, r =>
            {
                try
                {
                    var reportPath = r.Path;
                    var reportParameters = r.ReportParameters;
                    this.ReportingViewer.ServerReport.ReportPath = reportPath;

                    if (reportParameters != null)
                    {
                        // We are programmically setting the parameters.
                        this.ReportingViewer.ServerReport.SetParameters(reportParameters);
                    }

                    this.ReportingViewer.RefreshReport();
                    this.ExportReports();
                    this.log.LogDebug(this.GetType(), "Report Printed");
                }
                catch (System.Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                }
            });
        }

        /// <summary>
        /// Exports the current report out to a pdf file
        /// </summary>
        /// <returns>Path to the file that was generated</returns>
        private string ExportReport()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;
            var exportType = ApplicationSettings.SSRSEmailExportFormat;

            //Render the report to a byte array
            byte[] bytes;
            if (this.ReportingViewer.ProcessingMode == ProcessingMode.Local)
            {
                bytes = this.ReportingViewer.LocalReport.Render(exportType, null, out mimeType,
                    out encoding, out filenameExtension, out streamids, out warnings);
            }
            else
            {
                bytes = this.ReportingViewer.ServerReport.Render(exportType, null, out mimeType,
                   out encoding, out filenameExtension, out streamids, out warnings);
            }

            //Write report out to temporary PDF file
            var filename = Path.Combine(Settings.Default.ExportReportPath, string.Format("Report.{0}",exportType));
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            //return path to saved file
            return filename;
        }

        /// <summary>
        /// Attach report to the selected email account.
        /// </summary>
        private void AttachReportToEmail()
        {
            try
            {
                //Write report out to temporary PDF file
                var reportFilename = this.ExportReport();

                this.log.LogDebug(this.GetType(), string.Format("AttachReportToEmail(): Type:{0} To:{1}, Subject:{2}, Body:{3}",
                                                                ApplicationSettings.SSRSEmailType, ApplicationSettings.SSRSEmailTo,
                                                                ApplicationSettings.SSRSEmailSubject, ApplicationSettings.SSRSEmailBody));

                if (ApplicationSettings.SSRSEmailType.Equals("Outlook"))
                {
                    Microsoft.Office.Interop.Outlook.Application app = new Microsoft.Office.Interop.Outlook.Application();
                    var mailItem = (MailItem)app.CreateItem(OlItemType.olMailItem);
                    mailItem.To = ApplicationSettings.SSRSEmailTo;
                    mailItem.Subject = ApplicationSettings.SSRSEmailSubject;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = ApplicationSettings.SSRSEmailBody;

                    mailItem.Attachments.Add(reportFilename, OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                    mailItem.Display(true); 
                }

                //// Create a new email using outlook
                //ApplicationClass outlookApp = new ApplicationClass();
                //MailItem mailItem = (MailItem)outlookApp.CreateItem(OlItemType.olMailItem);
                //mailItem.To = "mail@yourdomain.com";
                //mailItem.Subject = "Your Report";
                //mailItem.Body = "Please find your report attached";
                //mailItem.Attachments.Add(reportFilename, (int)OlAttachmentType.olByValue, 1, reportFilename);

                //Remove the temp file once attached
                Array.ForEach(Directory.GetFiles(Path.Combine("@", Settings.Default.ExportReportPath)), File.Delete);

                // Display the window
                //mailItem.Display(false);
            }
            catch (System.Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        private void SetReportPath(ReportData r)
        {
            if (this.disableMultipleReports)
            {
                return;
            }

            try
            {
                this.currentReport = r;
                var reportPath = r.Path;
                var reportParameters = r.ReportParameters;
                this.ReportingViewer.ServerReport.ReportPath = reportPath;
                this.log.LogDebug(this.GetType(), string.Format("Opening report - Report name:{0}, Report Path:{1}, URI:{2}, IsUri:{3}",
                    this.currentReport.Name, reportPath, this.ReportingViewer.ServerReport.ReportServerUrl.AbsoluteUri, this.ReportingViewer.ServerReport.ReportServerUrl.IsAbsoluteUri));

                if (reportParameters != null)
                {
                    // We are programmically setting the parameters.
                    this.ReportingViewer.ServerReport.SetParameters(reportParameters);
                }

                if (!string.IsNullOrWhiteSpace(this.currentReport.PrinterName))
                {
                    this.ReportingViewer.PrinterSettings.PrinterName = this.currentReport.PrinterName;
                }

                this.ReportingViewer.RefreshReport();
                this.log.LogDebug(this.GetType(), "Report Printed");
            }
            catch (System.Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }

        /// <summary>
        /// Exports the current report out to a pdf file
        /// </summary>
        /// <returns>Path to the file that was generated</returns>
        private string ExportReports()
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;
            var exportType = ApplicationSettings.SSRSEmailExportFormat;

            //Render the report to a byte array
            byte[] bytes;
            if (this.ReportingViewer.ProcessingMode == ProcessingMode.Local)
            {
                bytes = this.ReportingViewer.LocalReport.Render(exportType, null, out mimeType,
                    out encoding, out filenameExtension, out streamids, out warnings);
            }
            else
            {
                bytes = this.ReportingViewer.ServerReport.Render(exportType, null, out mimeType,
                   out encoding, out filenameExtension, out streamids, out warnings);
            }

            //Write report out to temporary PDF file
            var reportSuffix = string.Format("Report{0}.{1}", this.reportCount, exportType);
            var filename = Path.Combine(Settings.Default.ExportReportPath, reportSuffix);
            using (FileStream fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            this.reportCount++;

            //return path to saved file
            return filename;
        }

        /// <summary>
        /// Attach reports to the selected email account.
        /// </summary>
        private void AttachReportsToEmail()
        {
            try
            {
                if (ApplicationSettings.SSRSEmailType.Equals("Outlook"))
                {
                    Microsoft.Office.Interop.Outlook.Application app = new Microsoft.Office.Interop.Outlook.Application();
                    var mailItem = (MailItem)app.CreateItem(OlItemType.olMailItem);
                    mailItem.To = ApplicationSettings.SSRSEmailTo;
                    mailItem.Subject = ApplicationSettings.SSRSEmailSubject;
                    mailItem.BodyFormat = OlBodyFormat.olFormatHTML;
                    mailItem.HTMLBody = ApplicationSettings.SSRSEmailBody;

                    var exportType = ApplicationSettings.SSRSEmailExportFormat;
                    var searchpattern = string.Format("*.{0}", exportType);
                    foreach (var file in Directory.GetFiles(Settings.Default.ExportReportPath, searchpattern))
                    {
                        mailItem.Attachments.Add(file, OlAttachmentType.olByValue, Type.Missing, Type.Missing);
                    }
                    
                    mailItem.Display(true);
                }

                //// Create a new email using outlook
                //ApplicationClass outlookApp = new ApplicationClass();
                //MailItem mailItem = (MailItem)outlookApp.CreateItem(OlItemType.olMailItem);
                //mailItem.To = "mail@yourdomain.com";
                //mailItem.Subject = "Your Report";
                //mailItem.Body = "Please find your report attached";
                //mailItem.Attachments.Add(reportFilename, (int)OlAttachmentType.olByValue, 1, reportFilename);

                //Remove the temp file once attached
                Array.ForEach(Directory.GetFiles(Path.Combine("@", Settings.Default.ExportReportPath)), File.Delete);

                // Display the window
                //mailItem.Display(false);
            }
            catch (System.Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        private void ReportingViewer_PrintingBegin(object sender, ReportPrintEventArgs e)
        {
            var param = this.ReportingViewer.ServerReport.GetParameters();
            if (param != null && param.Any())
            {
                var values = param.First().Values;
                Messenger.Default.Send(values.ToList(), Token.ReportValues);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var param = this.ReportingViewer.ServerReport.GetParameters();
                var validValues = param.First().ValidValues;
                var name = param.First().Name;


                var localValue = param.First().Values.First();
                var localLabel = validValues.First(x => x.Value == localValue).Label;
                var newValue = string.Empty;
                for (int i = 0; i < validValues.Count; i++)
                {
                    if (validValues.ElementAt(i).Label == localLabel && i > 0)
                    {
                        newValue = validValues.ElementAt(i - 1).Value;
                        break;
                    }
                }

                if (newValue != string.Empty)
                {
                    var newParam = new ReportParameter
                    {
                        Name = name,
                        Values = { newValue }
                    };

                    this.ReportingViewer.ServerReport.SetParameters(new List<ReportParameter> { newParam });
                    this.ReportingViewer.RefreshReport();
                }
            }
            catch (System.Exception)
            {
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                var param = this.ReportingViewer.ServerReport.GetParameters();
                var validValues = param.First().ValidValues;
                var name = param.First().Name;


                var localValue = param.First().Values.First();
                var localLabel = validValues.First(x => x.Value == localValue).Label;
                var newValue = string.Empty;
                for (int i = 0; i < validValues.Count; i++)
                {
                    if (validValues.ElementAt(i).Label == localLabel && (i + 1) < validValues.Count)
                    {
                        newValue = validValues.ElementAt(i + 1).Value;
                        break;
                    }
                }

                if (newValue != string.Empty)
                {
                    var newParam = new ReportParameter
                    {
                        Name = name,
                        Values = { newValue }
                    };

                    this.ReportingViewer.ServerReport.SetParameters(new List<ReportParameter> { newParam });
                    this.ReportingViewer.RefreshReport();
                }
            }
            catch (System.Exception)
            {
            }
        }

        private void SetExportFormat()
        {
            if (string.IsNullOrWhiteSpace(ApplicationSettings.SSRSEmailExportFormat)
                    || ApplicationSettings.SSRSEmailExportFormat == Constant.PDF)
            {
                this.RadioButtonPDF.IsChecked = true;
                return;
            }

            if (ApplicationSettings.SSRSEmailExportFormat == Constant.CSV)
            {
                this.RadioButtonCSV.IsChecked = true;
                return;
            }

            if (ApplicationSettings.SSRSEmailExportFormat == Constant.EXCEL)
            {
                this.RadioButtonEXCEL.IsChecked = true;
                return;
            }

            if (ApplicationSettings.SSRSEmailExportFormat == Constant.WORD)
            {
                this.RadioButtonWORD.IsChecked = true;
                return;
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            ApplicationSettings.SSRSEmailExportFormat = Constant.PDF;
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            ApplicationSettings.SSRSEmailExportFormat = Constant.CSV;
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            ApplicationSettings.SSRSEmailExportFormat = Constant.EXCEL;
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {
            ApplicationSettings.SSRSEmailExportFormat = Constant.WORD;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {

        }
    }
}
