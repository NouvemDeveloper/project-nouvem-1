﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenSaleSearchDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Controls;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Nouvem.Shared;

namespace Nouvem.View.Report
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for touchscreenReportSaleSearchDataView.xaml
    /// </summary>
    public partial class TouchscreenReportSaleSearchDataView : Window
    {
        public TouchscreenReportSaleSearchDataView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlSearchData.View.ShowSearchPanel(true);
            var gridPath = Settings.Default.TouchscreenReportSalesSearchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                    }
                    catch
                    {
                    }
                }

                Messenger.Default.Send(Token.Message, Token.TouchscreenMode);

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };
        }
       
        private void DatePicker_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is DatePicker)
            {
                (sender as DatePicker).IsDropDownOpen = true;
            }
        }

        private void DatePicker_PreviewMouseUp_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is DatePicker)
            {
                (sender as DatePicker).IsDropDownOpen = true;
            }
        }
    }
}


