﻿// -----------------------------------------------------------------------
// <copyright file="ReportView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Report
{
    using System;
    using System.IO;
    using System.Windows.Controls;
    using DevExpress.Xpf.Grid;
    using DevExpress.Xpf.Printing;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ReportView.xaml
    /// </summary>
    public partial class ReportView : UserControl
    {
        public ReportView()
        {
            this.InitializeComponent();
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-IE");

            Messenger.Default.Register<string>(this, Token.DisplayCalender, s =>
            {
                if (s.Equals(Constant.Start))
                {
                    this.DatePickerStartDate.IsDropDownOpen = true;
                }
                else
                {
                    this.DatePickerEndDate.IsDropDownOpen = true;
                }
            });

            this.Unloaded += (sender, args) =>
            {
                Settings.Default.Save();
                this.GridControlSalesData.SaveLayoutToXml(Settings.Default.EposSalesDataGrid);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.EposSalesDataGrid))
                {
                    this.GridControlSalesData.RestoreLayoutFromXml(Settings.Default.EposSalesDataGrid);
                }
            };
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            
            //this.TableViewProducts.ShowPrintPreview(this);
            this.ShowPrintPreview(this.TableViewProducts);
        }

        /// <summary>
        /// Replaces the standard grid ShowPrintPreview, so we can customise it.
        /// </summary>
        /// <param name="grid">The grid we are previewing.</param>
        private void ShowPrintPreview(DataViewBase grid)
        {
            var preview = new DocumentPreviewWindow();
            var link = new PrintableControlLink(grid as IPrintableControl);
            link.Landscape = true;
            var model = new LinkPreviewModel(link);
            preview.Model = model;
            link.CreateDocument(true);
            preview.ShowDialog();
        }

        private void Button_Click_1(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                this.TableViewProducts.ExportToXlsx(Settings.Default.EposExportToExcelPath);
                Messenger.Default.Send(Token.Message, Token.DisplayConfirmationBox);
            }
            catch (Exception ex)
            {
                Messenger.Default.Send(ex.Message, Token.DisplayErrorBox);
            }
            
        }

        private void Button_Click_2(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                this.TableViewProducts.ExportToPdf(Settings.Default.EposExportToPdfPath);
                Messenger.Default.Send(Token.Message, Token.DisplayConfirmationBox);
            }
            catch (Exception ex)
            {
                Messenger.Default.Send(ex.Message, Token.DisplayErrorBox);
            }
        }
    }
}
