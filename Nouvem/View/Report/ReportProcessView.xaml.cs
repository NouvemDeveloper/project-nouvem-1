﻿// -----------------------------------------------------------------------
// <copyright file="ReportProcessView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Report
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.ViewModel.Report;

    /// <summary>
    /// Interaction logic for ReportProcessView.xaml
    /// </summary>
    public partial class ReportProcessView : Window
    {
        public ReportProcessView()
        {
            this.InitializeComponent();
            this.DataContext = new ReportProcessViewModel();

            Messenger.Default.Register<string>(this, Token.CloseReportProcessWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.ReportProcessMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.ReportProcessHeight = this.Height;
                WindowSettings.ReportProcessLeft = this.Left;
                WindowSettings.ReportProcessTop = this.Top;
                WindowSettings.ReportProcessWidth = this.Width;
                WindowSettings.ReportProcessMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}


