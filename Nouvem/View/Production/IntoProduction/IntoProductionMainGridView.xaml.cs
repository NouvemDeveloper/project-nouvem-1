﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.Production.IntoProduction
{
    /// <summary>
    /// Interaction logic for IntoProductionMainGridView.xaml
    /// </summary>
    public partial class IntoProductionMainGridView : UserControl
    {
        public IntoProductionMainGridView()
        {
            InitializeComponent();
            Messenger.Default.Register<bool>(this, Token.EnableProductSelectionOnIntoProduction,
                this.EnableProductionSelection);
        }

        private void EnableProductionSelection(bool enable)
        {
            if (enable)
            {
                this.LabelProduct.Visibility = Visibility.Visible;
            }
            else
            {
                this.LabelProduct.Visibility = Visibility.Collapsed;
            }
        }
    }
}
