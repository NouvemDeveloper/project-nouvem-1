﻿// -----------------------------------------------------------------------
// <copyright file="IntoProductionTouchscreenView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.Enum;
using Nouvem.Properties;

namespace Nouvem.View.Production.IntoProduction
{
    /// <summary>
    /// Interaction logic for IntoProductionTouchscreenView.xaml
    /// </summary>
    public partial class IntoProductionTouchscreenView : UserControl
    {
        public IntoProductionTouchscreenView()
        {
            this.InitializeComponent();
        }
    }
}
