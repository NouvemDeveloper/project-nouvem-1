﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenSpecificationSelectionView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Production.IntoProduction
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenSpecificationSelectionView.xaml
    /// </summary>
    public partial class TouchscreenSpecificationSelectionView : UserControl
    {
        /// <summary>
        /// The grid scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public TouchscreenSpecificationSelectionView()
        {
            this.InitializeComponent();

            this.GridControlSpecification.View.HideSearchPanel();
            this.GridControlSpecification.View.SearchString = string.Empty;
            this.GridControlSpecification.View.ShowFilterPanelMode = ShowFilterPanelMode.Never;

            this.Unloaded += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                this.GridControlSpecification.View.SearchString = string.Empty;
                this.GridControlSpecification.FilterString = string.Empty;
                this.GridControlSpecification.SaveLayoutToXml(Settings.Default.SuppliersDataGrid);
                ScrollViewer = null;
            };

            this.Loaded += (sender, args) =>
            {
                #region registration

                Messenger.Default.Register<string>(this, Token.FilterSuppliers, s =>
                {
                    var filter = string.Format("([Name] LIKE '{0}%')", s);
                    this.GridControlSpecification.FilterString = filter;
                });

                #region scrolling

                Messenger.Default.Register<string>(this, Token.ScrollSuppliers, s =>
                {
                    if (ScrollViewer == null)
                    {
                        ScrollViewer = GetScrollViewer(this.GridControlSpecification) as ScrollViewer;
                    }

                    if (ScrollViewer == null)
                    {
                        return;
                    }

                    if (s.Equals(Constant.Up))
                    {
                        // scroll up
                        ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - ApplicationSettings.TouchScreenSuppliersVerticalOffset);
                        return;
                    }

                    // scoll down
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + ApplicationSettings.TouchScreenSuppliersVerticalOffset);
                });

                #endregion

                #endregion

                this.GridControlSpecification.View.SearchString = string.Empty;
                // TODO Reimplement when we purchase 15.1
                //if (File.Exists(Settings.Default.SuppliersDataGrid))
                //{
                //    this.GridControlSpecification.RestoreLayoutFromXml(Settings.Default.SuppliersDataGrid);
                //}
            };

            // disable sort/filter card selection (otherwise we'll move to the main or order screen).
            this.GridControlSpecification.EndGrouping += (sender, args) => (sender as GridControl).View.FocusedRowHandle = GridControl.InvalidRowHandle;
            this.GridControlSpecification.EndSorting += (sender, args) => (sender as GridControl).View.FocusedRowHandle = GridControl.InvalidRowHandle;
            this.GridControlSpecification.FilterChanged += (sender, args) => (sender as GridControl).View.FocusedRowHandle = GridControl.InvalidRowHandle;
        }

        /// <summary>
        /// Gets a handle on the scroll bar, so we can programically manipulate.
        /// </summary>
        /// <param name="o"></param>
        /// <returns>The grid control scroll viewer.</returns>
        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Workaround, to enable the grid settings storage when auto populate columns is on.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        /// <remarks>TODO: No need for this from version 15.1 up. (Remove).</remarks>
        private void GridControlSpecification_AutoGeneratedColumns(object sender, RoutedEventArgs e)
        {
            for (var i = 0; i < this.GridControlSpecification.Columns.Count; i++)
            {
                this.GridControlSpecification.Columns[i].Name = string.Format("Column{0}", i);
            }

            if (File.Exists(Settings.Default.SpecificationSetUpGrid))
            {
                this.GridControlSpecification.RestoreLayoutFromXml(Settings.Default.SpecificationSetUpGrid);
            }
        }
     }
}

