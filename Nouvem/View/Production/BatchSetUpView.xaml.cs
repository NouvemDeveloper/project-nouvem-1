﻿// -----------------------------------------------------------------------
// <copyright file="BatchSetUpView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using Nouvem.Logging;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Model.Event;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.View.Utility;

namespace Nouvem.View.Production
{
    using System.ComponentModel;
    using System.Windows;
    using System.Threading;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for BatchSetUpView.xaml
    /// </summary>
    public partial class BatchSetUpView : Window
    {
        private readonly string gridPath;
        private readonly string gridSecondaryPath;
        private readonly string gridDetailsPath;
        private readonly string gridIngredientsPath;
        private NotesView notesView;
        private int gridRowHandle;
        private int rowHandle;
        private ILogger log = new Logger();

        public BatchSetUpView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseBatchSetUpWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            ThreadPool.QueueUserWorkItem(x => this.TextBoxBatch.Dispatcher.Invoke(() => this.TextBoxBatch.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedInventoryControl, x => this.ComboBoxSpecs.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.TextBoxBatch.Focus());
            Messenger.Default.Register<string>(this, Token.CollapseGroups, s => this.TreeListViewData.CollapseAllNodes());
            Messenger.Default.Register<bool>(this, Token.ProductionTypeSelected, b =>
            {
                //this.TabItemReceipe.Visibility = !b ? Visibility.Visible : Visibility.Collapsed;
                //this.TabItemContents.Visibility = !b ? Visibility.Visible : Visibility.Collapsed;
                this.TabItemIngredients.Visibility = !b ? Visibility.Visible : Visibility.Collapsed;
                this.GridColumnMixes.Visible = !b;
                //this.TabControl.SelectedIndex = !b ? 0 : 1;
            });

            this.WindowState = WindowSettings.BatchSetUpMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.gridPath = Settings.Default.SpecMainGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.gridSecondaryPath = Settings.Default.SpecSecondaryGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.gridDetailsPath = Settings.Default.BatchDetailsGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.gridIngredientsPath = Settings.Default.BatchIngredientsGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Loaded += this.OnLoaded;
            this.Closing += this.OnClosing;
            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void OnLoaded(object o, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPath))
            {
                this.GridControlProducts.RestoreLayoutFromXml(this.gridPath);
            }

            if (File.Exists(this.gridSecondaryPath))
            {
                this.GridControlSelectedProducts.RestoreLayoutFromXml(this.gridSecondaryPath);
            }

            if (File.Exists(this.gridDetailsPath))
            {
                this.GridControlSaleDetails1.RestoreLayoutFromXml(this.gridDetailsPath);
            }

            if (File.Exists(this.gridIngredientsPath))
            {
                this.GridControlIngredients.RestoreLayoutFromXml(this.gridIngredientsPath);
            }
        }

        private void OnClosing(object o, CancelEventArgs cancelEventArgs)
        {
            if (this.notesView != null)
            {
                this.notesView.DataReceived -= NotesViewOnDataReceived;
                this.notesView = null;
            }

            WindowSettings.BatchSetUpHeight = this.Height;
            WindowSettings.BatchSetUpLeft = this.Left;
            WindowSettings.BatchSetUpTop = this.Top;
            WindowSettings.BatchSetUpWidth = this.Width;
            WindowSettings.BatchSetUpMaximised = this.WindowState.ToBool();
            Messenger.Default.Send(false, Token.WindowStatus);
            Messenger.Default.Unregister(this);
            this.GridControlProducts.SaveLayoutToXml(this.gridPath);
            this.GridControlSelectedProducts.SaveLayoutToXml(this.gridSecondaryPath);
            this.GridControlSaleDetails1.SaveLayoutToXml(this.gridDetailsPath);
            this.GridControlIngredients.SaveLayoutToXml(this.gridIngredientsPath);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
        }

        private void CreateNotes(string note)
        {
            this.notesView = new NotesView();
            this.notesView.DataReceived += NotesViewOnDataReceived;
            this.notesView.SetNote(note);
            this.notesView.Show();
        }

        private void NotesViewOnDataReceived(object sender, StringArgs e)
        {
            if (this.gridRowHandle < 0)
            {
                this.gridRowHandle = this.GridControlIngredients.VisibleRowCount - 2;
            }

            this.GridControlIngredients.SetCellValue(this.gridRowHandle, "Notes", e.Data);
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            //this.rowHandle = this.TableViewData.FocusedRowHandle;
            //if (this.rowHandle < 0)
            //{
            //    return;
            //}

            if (this.GridControlIngredients.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlIngredients.CurrentColumn == this.GridControlIngredients.Columns["Notes"])
                {
                    this.gridRowHandle = this.rowHandle;
                    var value = this.GridControlIngredients.GetCellValue(this.gridRowHandle, "Notes");
                    var localValue = value == null ? string.Empty : value.ToString();
                    this.CreateNotes(localValue);
                }
            }
        }

        private void CheckBoxExpand_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.TreeListViewData.ExpandAllNodes();
        }

        private void CheckBoxExpand_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.TreeListViewData.CollapseAllNodes();
        }

        private void TableViewData_ValidateRow(object sender, GridRowValidationEventArgs e)
        {
            try
            {
                var count = ((ObservableCollection<SaleDetail>) this.GridControlSaleDetails1.ItemsSource).Count;
                if (count > 1)
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.MoreThanOneReceipeProductEntered;
                    SystemMessage.Write(MessageType.Issue, Message.NoIssueMethodEntered);
                    return;
                }
            }
            catch (Exception)
            {

            }
        }

        private void TableViewData1_CellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            this.log.LogInfo(this.GetType(), "TableViewData1_CellValueChanging");
            Messenger.Default.Send(e, Token.RecipeCellChange);
        }
    }
}

