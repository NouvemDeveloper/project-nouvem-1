﻿// -----------------------------------------------------------------------
// <copyright file="BatchEditView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Input;
using DevExpress.Xpf.Grid;

namespace Nouvem.View.Production
{
    using System.ComponentModel;
    using System.IO;
    using System.Windows;
    using DevExpress.Xpf.Printing;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for BatchEditView.xaml
    /// </summary>
    public partial class BatchEditView : Window
    {
        private readonly string gridPath;

        public BatchEditView()
        {
            this.InitializeComponent();

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseBatchEditWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.gridPath = Settings.Default.BatchEditGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += this.OnClosing;
            this.Loaded += this.OnLoaded;

            this.WindowState = WindowSettings.BatchEditMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPath))
            {
                this.GridControlSearchData.RestoreLayoutFromXml(this.gridPath);
            }

            Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
            {
                try
                {
                    var preview = new DocumentPreviewWindow();
                    var link = new PrintableControlLink(this.TableView);
                    link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                    var model = new LinkPreviewModel(link);
                    preview.Model = model;
                    link.CreateDocument(true);
                    preview.Show();
                }
                catch
                {
                }
            });
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            WindowSettings.BatchEditHeight = this.Height;
            WindowSettings.BatchEditLeft = this.Left;
            WindowSettings.BatchEditTop = this.Top;
            WindowSettings.BatchEditWidth = this.Width;
            WindowSettings.BatchEditMaximised = this.WindowState.ToBool();
            this.GridControlSearchData.FilterCriteria = null;
            Messenger.Default.Send(false, Token.WindowStatus);
            this.GridControlSearchData.View.SearchString = string.Empty;
            this.GridControlSearchData.SaveLayoutToXml(this.gridPath);
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var rowHandle = this.TableView.GetRowHandleByMouseEventArgs(e);
            if (rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlSearchData.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlSearchData.CurrentColumn == this.GridControlSearchData.Columns["PopUpNote"])
                {
                    Messenger.Default.Send(Token.Message, Token.ShowBatchEditNotesWindow);
                }
            }
        }
    }
}


