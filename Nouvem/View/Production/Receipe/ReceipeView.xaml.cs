﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Grid;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Model.Event;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.View.Utility;
using Nouvem.ViewModel.Production.Receipe;

namespace Nouvem.View.Production.Receipe
{
    /// <summary>
    /// Interaction logic for ReceipeView.xaml
    /// </summary>
    public partial class ReceipeView : Window
    {
        private readonly string gridPath;
        private NotesView notesView;
        private int gridRowHandle;
        private int rowHandle;

        public ReceipeView(int inMasterId)
        {
            this.InitializeComponent();

            if (inMasterId > 0)
            {
                Messenger.Default.Register<string>(this, Token.CloseReceipeWithProductWindow, x => this.Close());
                this.DataContext = new ReceipeViewModel(inMasterId);
                this.Top = this.Top + ApplicationSettings.RecipeScreenOffset;
                this.Left = this.Left + ApplicationSettings.RecipeScreenOffset;
                ApplicationSettings.RecipeScreenOffset += 20;
            }
            else
            {
                Messenger.Default.Register<string>(this, Token.CloseReceipeWindow, x => this.Close());
                this.DataContext = new ReceipeViewModel();
            }

            this.WindowState = WindowSettings.RecipeMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.gridPath = Settings.Default.RecipeGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Loaded += this.OnLoaded;
            this.Closing += this.OnClosing;
            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void OnLoaded(object o, RoutedEventArgs routedEventArgs)
        {
            if (File.Exists(this.gridPath))
            {
                this.GridControlSaleDetails.RestoreLayoutFromXml(this.gridPath);
            }
        }

        private void OnClosing(object o, CancelEventArgs cancelEventArgs)
        {
            if (this.notesView != null)
            {
                this.notesView.DataReceived -= NotesViewOnDataReceived;
                this.notesView = null;
            }

            ApplicationSettings.RecipeScreenOffset = 20;
            WindowSettings.RecipeHeight = this.Height;
            WindowSettings.RecipeLeft = this.Left;
            WindowSettings.RecipeTop = this.Top;
            WindowSettings.RecipeWidth = this.Width;
            WindowSettings.RecipeMaximised = this.WindowState.ToBool();
            Messenger.Default.Send(false, Token.WindowStatus);
            Messenger.Default.Unregister(this);
            this.GridControlSaleDetails.SaveLayoutToXml(this.gridPath);
            this.Closing -= this.OnClosing;
            this.Loaded -= this.OnLoaded;
        }

        private void CreateNotes(string note)
        {
            this.notesView = new NotesView();
            this.notesView.DataReceived += NotesViewOnDataReceived;
            this.notesView.SetNote(note);
            this.notesView.Show();
        }

        private void NotesViewOnDataReceived(object sender, StringArgs e)
        {
            if (this.gridRowHandle < 0)
            {
                this.gridRowHandle = this.GridControlSaleDetails.VisibleRowCount - 2;
            }

            this.GridControlSaleDetails.SetCellValue(this.gridRowHandle, "Notes", e.Data);
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            //this.rowHandle = this.TableViewData.FocusedRowHandle;
            //if (this.rowHandle < 0)
            //{
            //    return;
            //}

            if (this.GridControlSaleDetails.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlSaleDetails.CurrentColumn == this.GridControlSaleDetails.Columns["Notes"])
                {
                    this.gridRowHandle = this.rowHandle;
                    var value = this.GridControlSaleDetails.GetCellValue(this.gridRowHandle, "Notes");
                    var localValue = value == null ? string.Empty : value.ToString();
                    this.CreateNotes(localValue);
                }
            }
        }

        private void TableViewData_ValidateRow(object sender, GridRowValidationEventArgs e)
        {
            try
            {
                var issueMethod = (e.Row as SaleDetail).NouIssueMethodID;

                if (issueMethod.IsNullOrZero())
                {
                    e.IsValid = false;
                    e.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Critical;
                    e.ErrorContent = Message.NoIssueMethodEntered;
                    SystemMessage.Write(MessageType.Issue, Message.NoIssueMethodEntered);
                    return;
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
