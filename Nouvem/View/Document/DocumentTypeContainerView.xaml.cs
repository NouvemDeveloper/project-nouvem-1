﻿// -----------------------------------------------------------------------
// <copyright file="DocumentTypeContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Document
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ContainerContainerView.xaml
    /// </summary>
    public partial class DocumentTypeContainerView : Window
    {
        public DocumentTypeContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseDocumentTypeWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
