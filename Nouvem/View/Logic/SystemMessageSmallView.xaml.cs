﻿// -----------------------------------------------------------------------
// <copyright file="SystemMessageSmallView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Global;

namespace Nouvem.View.Logic
{
    using System;
    using System.Windows;
    using System.Windows.Threading;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for SystemMessageSmallView.xaml
    /// </summary>
    public partial class SystemMessageSmallView : UserControl
    {
        private readonly DispatcherTimer timer = new DispatcherTimer();

        public SystemMessageSmallView()
        {
            this.InitializeComponent();
            this.Loaded += this.OnLoaded;
            this.Unloaded += this.OnUnloaded;
            this.timer.Tick += this.TimerOnTick;
            this.timer.Interval = TimeSpan.FromMinutes(1);
            this.timer.Start();
            this.SetTime();

            if (ApplicationSettings.HideProcessSelection)
            {
                this.TextBoxProcess.Width = 0;
            }
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            this.SetTime();
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.timer.Stop();
            this.timer.Tick -= this.TimerOnTick;
            this.Loaded -= this.OnLoaded;
            this.Unloaded -= this.OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.TextBoxMainMessage.Background = Brushes.White;
        }

        private void SetTime()
        {
            this.TextBoxTime.Text = DateTime.Now.ToString("dd/MM/yy HH:mm");
        }
    }
}
