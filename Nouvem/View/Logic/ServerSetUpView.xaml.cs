﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
// -----------------------------------------------------------------------
// <copyright file="ServerSetUpContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Logic
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ServerSetUpView.xaml
    /// </summary>
    public partial class ServerSetUpView : UserControl
    {
        public ServerSetUpView()
        {
            this.InitializeComponent();
            System.Threading.ThreadPool.QueueUserWorkItem(
                (a) =>
                {
                    System.Threading.Thread.Sleep(100);
                    this.TextBoxCompanyName.Dispatcher.Invoke(() => this.TextBoxCompanyName.Focus());
                });

            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxCompanyName.Focus());
            Messenger.Default.Register<string>(this, Token.DBStoredPassword, s =>
            {
                this.PasswordBoxPassword.Password = s;
                this.TextBoxCompanyName.Focus();
            });
        }

        private void PasswordBoxPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.PasswordBoxPassword.Password, Token.DBPassword);
        }
    }
}
