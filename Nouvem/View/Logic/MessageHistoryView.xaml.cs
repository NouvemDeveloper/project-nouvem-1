﻿// -----------------------------------------------------------------------
// <copyright file="MessageHistoryView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Logic
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for MessageHistoryView.xaml
    /// </summary>
    public partial class MessageHistoryView : Window
    {
        public MessageHistoryView()
        {
            this.InitializeComponent();

            //Messenger.Default.Register<string>(this, Token.CloseBPMasterWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Unregister(this);
            };
        }
    }
}
