﻿// -----------------------------------------------------------------------
// <copyright file="ScannerFactoryView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using GalaSoft.MvvmLight.Messaging;

namespace Nouvem.View.Scanner
{
    using System.Windows;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerFactoryView.xaml
    /// </summary>
    public partial class ScannerFactoryView : Window
    {
        public ScannerFactoryView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseScannerFactory, x => this.Close());
            if (ApplicationSettings.ScannerEmulatorMode)
            {
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
                this.Left = ApplicationSettings.ScannerEmulatorLeft;
                this.Top = ApplicationSettings.ScannerEmulatorTop;
                this.MinHeight = 300;
                this.MinWidth = 300;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

            this.Closing += (sender, args) =>
            {
                ApplicationSettings.ScannerEmulatorHeight = this.Height;
                ApplicationSettings.ScannerEmulatorWidth = this.Width;
                ApplicationSettings.ScannerEmulatorLeft = this.Left;
                ApplicationSettings.ScannerEmulatorTop = this.Top;
            };
        }
    }
}
