﻿// -----------------------------------------------------------------------
// <copyright file="ScannerSequencerView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Windows.Threading;

namespace Nouvem.View.Scanner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerSequencerView.xaml
    /// </summary>
    public partial class ScannerSequencerView : Window
    {
        public ScannerSequencerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.FocusToButton, s => this.TextBoxBarcode.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToHerdNo, s => this.TextBoxBarcode.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToEartag, s => this.TextBoxBarcode.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.TextBoxBarcode.Focus());

            if (ApplicationSettings.ScannerEmulatorMode)
            {
                //this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
                this.Left = ApplicationSettings.ScannerEmulatorLeft;
                this.Top = ApplicationSettings.ScannerEmulatorTop;
                this.MinHeight = 300;
                this.MinWidth = 300;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

            this.Closing += this.OnClosing;
            this.Loaded += this.OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.TextBoxBarcode.Focus();
            }));
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            Messenger.Default.Unregister(this);
            this.Loaded -= this.OnLoaded;
            this.Closing -= this.OnClosing;
        }

        private void UserControl_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }
    }
}

