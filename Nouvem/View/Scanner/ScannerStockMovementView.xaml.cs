﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Editors;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;

namespace Nouvem.View.Scanner
{
    /// <summary>
    /// Interaction logic for ScannerStockMovementView.xaml
    /// </summary>
    public partial class ScannerStockMovementView : UserControl
    {
        public ScannerStockMovementView()
        {
            this.InitializeComponent();


#if DEBUG
            this.TextBoxBarcode.Height = 30;
            this.TextBoxBarcode.Width = 30;
#endif

            ThreadPool.QueueUserWorkItem(x => this.TextBoxBarcode.Dispatcher.Invoke(() => this.TextBoxBarcode.Focus()));

            //Messenger.Default.Register<string>(this, Token.CloseFactoryWindow, s => this.GridControlDetails.SaveLayoutToXml(Settings.Default.ScannerStockTakePath));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.TextBoxBarcode.Focus());

            Messenger.Default.Register<bool>(this, Token.ShowScannerMoveButton, b =>
            {
                if (b)
                {
                    this.ButtonMove.Visibility = Visibility.Visible;
                    this.GridScanner.Visibility = Visibility.Collapsed;
                }
                else
                {
                    this.ButtonMove.Visibility = Visibility.Collapsed;
                    this.GridScanner.Visibility = Visibility.Visible;
                }
            });

            this.Loaded += (sender, args) =>
            {
                //if (File.Exists(Settings.Default.ScannerStockTakePath))
                //{
                //    this.GridControlDetails.RestoreLayoutFromXml(Settings.Default.ScannerStockTakePath);
                //}

                this.TextBoxBarcode.Focus();
            };

            this.ButtonMove.Visibility = Visibility.Collapsed;
        }

        private void UserControl_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            //this.TextBoxBarcode.Focus();
        }

        private void ComboBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is ComboBoxEdit)
            {
                var localCombo = (sender as ComboBoxEdit);
                localCombo.ShowPopup();
            }

            //this.TextBoxBarcode.Focus();
        }

        private void ComboBoxEdit_PopupClosed(object sender, DevExpress.Xpf.Editors.ClosePopupEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }
    }
}
