﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DevExpress.Xpf.Core.Native;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Shared;

namespace Nouvem.View.Scanner
{
    /// <summary>
    /// Interaction logic for StockMoveBatchGridView.xaml
    /// </summary>
    public partial class StockMoveBatchGridView : UserControl
    {
        private int rowHandle;
        private int gridRowHandle;
        private bool isQty;

        public StockMoveBatchGridView()
        {
            InitializeComponent();

            Messenger.Default.Register<string>(this, KeypadTarget.BatchStockMove, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.TableViewData.FocusedRowHandle = this.GridControlMoves.GetRowHandleByListIndex(this.rowHandle);
                    int row = this.TableViewData.FocusedRowHandle;
                    var localColumn = this.isQty ? "Qty" : "Wgt";
                    if (this.rowHandle < 0)
                    {
                        this.rowHandle = (this.GridControlMoves.ItemsSource as ICollection).Count - 1;
                    }

                    this.GridControlMoves.SetCellValue(this.rowHandle, localColumn, s.ToDecimal());
                    this.GridControlMoves.View.CommitEditing();
                }
            });
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            //this.rowHandle = this.TableViewData.FocusedRowHandle;
            //if (this.rowHandle < 0)
            //{
            //    return;
            //}

            if (this.GridControlMoves.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlMoves.CurrentColumn == this.GridControlMoves.Columns["Qty"])
                {
                    this.isQty = true;
                    this.gridRowHandle = this.rowHandle;
                    Global.Keypad.Show(KeypadTarget.BatchStockMove, true, ApplicationSettings.ScannerEmulatorMode);
                }
                else if (this.GridControlMoves.CurrentColumn == this.GridControlMoves.Columns["Wgt"])
                {
                    this.isQty = false;
                    this.gridRowHandle = this.rowHandle;
                    Global.Keypad.Show(KeypadTarget.BatchStockMove, true, ApplicationSettings.ScannerEmulatorMode);
                }
            }
        }

       

        private void GridControlMoves_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {

            this.rowHandle = this.TableViewData.GetRowHandleByMouseEventArgs(e);
            //this.rowHandle = this.TableViewData.FocusedRowHandle;
            //if (this.rowHandle < 0)
            //{
            //    return;
            //}

            if (this.GridControlMoves.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlMoves.CurrentColumn == this.GridControlMoves.Columns["Qty"])
                {
                    this.isQty = true;
                    this.gridRowHandle = this.rowHandle;
                    Global.Keypad.Show(KeypadTarget.BatchStockMove, true, ApplicationSettings.ScannerEmulatorMode);
                }
                else if (this.GridControlMoves.CurrentColumn == this.GridControlMoves.Columns["Wgt"])
                {
                    this.isQty = false;
                    this.gridRowHandle = this.rowHandle;
                    Global.Keypad.Show(KeypadTarget.BatchStockMove, true, ApplicationSettings.ScannerEmulatorMode);
                    this.TableViewData.FocusedRowHandle = this.GridControlMoves.GetRowHandleByListIndex(this.rowHandle);
                }
                else
                {
                    
                }
            }
        }

        private void TableViewData_CellValueChanged(object sender, DevExpress.Xpf.Grid.CellValueChangedEventArgs e)
        {
            if (!e.Cell.Property.Equals("BatchID"))
            {
                return;
            }

            this.TableViewData.FocusedRowHandle = this.GridControlMoves.GetRowHandleByListIndex(-2147483647);
        }
    }
}
