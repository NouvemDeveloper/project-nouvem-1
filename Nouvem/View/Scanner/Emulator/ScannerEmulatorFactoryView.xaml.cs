﻿// -----------------------------------------------------------------------
// <copyright file="ScannerEmulatorFactoryView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Scanner.Emulator
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerEmulatorFactoryView.xaml
    /// </summary>
    public partial class ScannerEmulatorFactoryView : Window
    {
        public ScannerEmulatorFactoryView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseEmulator, s => this.Close());
        }
    }
}
