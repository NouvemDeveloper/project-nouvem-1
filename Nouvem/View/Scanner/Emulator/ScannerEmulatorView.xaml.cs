﻿using System;
using System.Windows.Controls;

// -----------------------------------------------------------------------
// <copyright file="ScannerEmulatorView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Scanner.Emulator
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerEmulatorView.xaml
    /// </summary>
    public partial class ScannerEmulatorView : UserControl
    {
        public ScannerEmulatorView()
        {
            this.InitializeComponent();
        }
    }
}
