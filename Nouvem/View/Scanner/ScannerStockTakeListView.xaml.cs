﻿// -----------------------------------------------------------------------
// <copyright file="ScannerStockTakeListView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Scanner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for ScannerStockTakeListView.xaml
    /// </summary>
    public partial class ScannerStockTakeListView : Window
    {
        public ScannerStockTakeListView()
        {
            this.InitializeComponent();

            if (ApplicationSettings.ScannerEmulatorMode)
            {
                this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this.Height = ApplicationSettings.ScannerEmulatorHeight;
                this.Width = ApplicationSettings.ScannerEmulatorWidth;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }

            Messenger.Default.Register<string>(this, Token.CloseWindow, s => this.Close());
        }
    }
}
