﻿// -----------------------------------------------------------------------
// <copyright file="ScannerStockTakeView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Threading;
using System.Windows.Input;
using DevExpress.Xpf.Editors;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Logging;

namespace Nouvem.View.Scanner
{
    using System.IO;
    using System.Windows.Controls;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ScannerStockTakeView.xaml
    /// </summary>
    public partial class ScannerStockTakeView : UserControl
    {
        public ScannerStockTakeView()
        {
            this.InitializeComponent();


#if DEBUG
            this.TextBoxBarcode.Height = 30;
            this.TextBoxBarcode.Width = 30;
#endif

            ThreadPool.QueueUserWorkItem(x => this.TextBoxBarcode.Dispatcher.Invoke(() => this.TextBoxBarcode.Focus()));
           
            Messenger.Default.Register<string>(this, Token.CloseFactoryWindow, s => this.GridControlDetails.SaveLayoutToXml(Settings.Default.ScannerStockTakePath));
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.TextBoxBarcode.Focus());
            this.Unloaded += (sender, args) =>
            {
                this.GridControlDetails.SaveLayoutToXml(Settings.Default.ScannerStockTakePath);
            };
       
            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.ScannerStockTakePath))
                {
                    try
                    {
                        this.GridControlDetails.RestoreLayoutFromXml(Settings.Default.ScannerStockTakePath);
                    }
                    catch (Exception e)
                    {
                        var log = new Logger();
                        log.LogError(this.GetType(), e.Message);
                    }
                }

                this.TextBoxBarcode.Focus();
            };
        }

        private void UserControl_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
           
            this.TextBoxBarcode.Focus();
        }

        private void ComboBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is ComboBoxEdit)
            {
                var localCombo = (sender as ComboBoxEdit);
                localCombo.ShowPopup();
            }

            //this.TextBoxBarcode.Focus();
        }

        private void ComboBoxEdit_PopupClosed(object sender, DevExpress.Xpf.Editors.ClosePopupEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }

        private void GridControlDetails_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.TextBoxBarcode.Focus();
        }
    }
}
