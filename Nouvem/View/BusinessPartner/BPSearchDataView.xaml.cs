﻿// -----------------------------------------------------------------------
// <copyright file="BPSearchDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;
using Nouvem.Shared;

namespace Nouvem.View.BusinessPartner
{
    using System.IO;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for BPSearchDataView.xaml
    /// </summary>
    public partial class BPSearchDataView : Window
    {
        public BPSearchDataView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.SearchDataMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlSearchData.View.ShowSearchPanel(true);
            var gridPath = Settings.Default.SearchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Closing += (sender, args) =>
            {
                WindowSettings.SearchDataHeight = this.Height;
                WindowSettings.SearchDataLeft = this.Left;
                WindowSettings.SearchDataTop = this.Top;
                WindowSettings.SearchDataWidth = this.Width;
                WindowSettings.SearchDataMaximised = this.WindowState.ToBool();
                this.GridControlSearchData.FilterCriteria = null;
                Messenger.Default.Send(false, Token.WindowStatus);
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                this.GridControlSearchData.FilterCriteria = null;
            };

            this.Loaded += (sender, args) =>
                {
                    if (File.Exists(gridPath))
                    {
                        this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                    }

                    Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                    {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                    });
                };

            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);
        }
    }
}
