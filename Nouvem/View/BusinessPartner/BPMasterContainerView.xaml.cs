﻿// -----------------------------------------------------------------------
// <copyright file="BPMasterWindowView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.BusinessPartner
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for BPMasterWindowView.xaml
    /// </summary>
    public partial class BPMasterContainerView : Window
    {
        public BPMasterContainerView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.BPMasterMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseBPMasterWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });
           
            this.Closing += (sender, args) =>
            {
                WindowSettings.BPMasterHeight = this.Height;
                WindowSettings.BPMasterLeft = this.Left;
                WindowSettings.BPMasterTop = this.Top;
                WindowSettings.BPMasterWidth = this.Width;
                WindowSettings.BPMasterMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
