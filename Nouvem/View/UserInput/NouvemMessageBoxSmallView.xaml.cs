﻿// -----------------------------------------------------------------------
// <copyright file="NouvemMessageBoxSmallView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows.Threading;

namespace Nouvem.View.UserInput
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for NouvemMessageBoxSmallView.xaml
    /// </summary>
    public partial class NouvemMessageBoxSmallView : Window
    {
        /// <summary>
        /// timer used for the flash message
        /// </summary>
        private DispatcherTimer timer;

        public NouvemMessageBoxSmallView(bool flashMessage = false)
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseMessageBoxWindow, x => this.Close());

            if (flashMessage)
            {
                this.ButtonLeft.Visibility = Visibility.Hidden;
                this.ButtonRight.Visibility = Visibility.Hidden;
                this.timer = new DispatcherTimer();
                this.timer.Interval = TimeSpan.FromSeconds(ApplicationSettings.MessageBoxFlashTime);
                this.timer.Tick += this.TimerOnTick;
                this.timer.Start();
            }

            this.Closing += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                if (this.timer != null)
                {
                    this.timer.Stop();
                    this.timer.Tick -= this.TimerOnTick;
                    this.timer = null;
                }
            };
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            this.Close();
        }
    }
}
