﻿// -----------------------------------------------------------------------
// <copyright file="AttributeMasterSetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;
using Nouvem.Shared;

namespace Nouvem.View.Attribute
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for AttributeMasterSetUpView.xaml
    /// </summary>
    public partial class AttributeMasterSetUpView : Window
    {
        public AttributeMasterSetUpView()
        {
            this.InitializeComponent();
            ThreadPool.QueueUserWorkItem(x => this.TextBoxCode.Dispatcher.Invoke(() => this.TextBoxCode.Focus()));
            Messenger.Default.Register<string>(this, Token.CloseAttributeMasterSetUpWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s =>
            {
                ThreadPool.QueueUserWorkItem(x => this.TextBoxCode.Dispatcher.Invoke(() => this.TextBoxCode.Focus()));
            });

            this.WindowState = WindowSettings.AttributeMasterMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.AttributeMasterHeight = this.Height;
                WindowSettings.AttributeMasterLeft = this.Left;
                WindowSettings.AttributeMasterTop = this.Top;
                WindowSettings.AttributeMasterWidth = this.Width;
                WindowSettings.AttributeMasterMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
