﻿// -----------------------------------------------------------------------
// <copyright file="TemplateNameView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Attribute
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TemplateNameView.xaml
    /// </summary>
    public partial class TemplateNameView : Window
    {
        public TemplateNameView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseTraceabilityNameWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.TemplateNameMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.TemplateNameHeight = this.Height;
                WindowSettings.TemplateNameLeft = this.Left;
                WindowSettings.TemplateNameTop = this.Top;
                WindowSettings.TemplateNameWidth = this.Width;
                WindowSettings.TemplateNameMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
