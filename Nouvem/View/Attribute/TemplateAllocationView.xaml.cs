﻿// -----------------------------------------------------------------------
// <copyright file="TemplateAllocationView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Windows.Input;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Attribute
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TemplateAllocationView.xaml
    /// </summary>
    public partial class TemplateAllocationView : Window
    {
        public TemplateAllocationView()
        {
            this.InitializeComponent();

            this.ComboBoxEditTypes.Visibility = Visibility.Hidden;
            Messenger.Default.Register<string>(this, Token.CloseTraceabilityTemplateAllocationWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridColumnAssignedRequired.Header = Strings.ReqBeforeContinue;

            Messenger.Default.Register<bool>(this, Token.ShowStandardColumns, b =>
            {
                if (b)
                {
                    this.ComboBoxEditTypes.Visibility = Visibility.Visible;
                    var selectedType = (this.ComboBoxEditTypes.SelectedItem as CollectionData);

                    if (selectedType != null && !selectedType.Data.CompareIgnoringCaseAndWhitespace(Strings.Standard))
                    {
                        this.GridColumnAssignedBatch.Visible = false;
                        this.GridColumnAssignedTransaction.Visible = false;
                        this.GridColumnAssignedReset.Visible = false;
                        this.GridColumnVisibleProcesses.Visible = true;
                        this.GridColumnEditProcesses.Visible = false;
                        this.GridColumnAssignedRequiredComp.Visible = false;
                        this.GridColumnAssignedTraceabilityTab.Visible = false;
                        this.GridColumnAssignedRequired.Header = Strings.HardStop;
                    }
                    else
                    {
                        this.GridColumnAssignedBatch.Visible = b;
                        this.GridColumnAssignedTransaction.Visible = b;
                        this.GridColumnAssignedReset.Visible = b;
                        this.GridColumnVisibleProcesses.Visible = b;
                        this.GridColumnEditProcesses.Visible = b;
                        this.GridColumnAssignedRequiredComp.Visible = b;
                        this.GridColumnAssignedTraceabilityTab.Visible = b;
                        this.GridColumnAssignedRequired.Header = Strings.ReqBeforeContinue;
                    }
                }
                else
                {
                    //this.CheckBoxTransactions.Visibility = Visibility.Hidden;
                    this.GridColumnAssignedBatch.Visible = b;
                    this.GridColumnAssignedTransaction.Visible = b;
                    this.GridColumnAssignedReset.Visible = b;
                    this.GridColumnVisibleProcesses.Visible = b;
                    this.GridColumnEditProcesses.Visible = b;
                    this.GridColumnAssignedRequiredComp.Visible = b;
                    this.GridColumnAssignedTraceabilityTab.Visible = b;
                    this.GridColumnAssignedRequired.Header = Strings.HardStop;
                }
            });

            this.WindowState = WindowSettings.AttributeAllocationMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            var gridPath = Settings.Default.TemplateAllocationGrid.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            var gridPathAvailable = Settings.Default.TemplateAllocationAvailableGrid.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlAssignedTraceability.RestoreLayoutFromXml(gridPath);
                }

                if (File.Exists(gridPathAvailable))
                {
                    this.GridControlAvailableTraceability.RestoreLayoutFromXml(gridPathAvailable);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.Show();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlAssignedTraceability.SaveLayoutToXml(gridPath);
                this.GridControlAvailableTraceability.SaveLayoutToXml(gridPathAvailable);
                WindowSettings.AttributeAllocationHeight = this.Height;
                WindowSettings.AttributeAllocationLeft = this.Left;
                WindowSettings.AttributeAllocationTop = this.Top;
                WindowSettings.AttributeAllocationWidth = this.Width;
                WindowSettings.AttributeAllocationMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void Grid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var rowHandle = this.TableView.GetRowHandleByMouseEventArgs(e);
            if (rowHandle == GridControl.InvalidRowHandle)
            {
                return;
            }

            if (this.GridControlAssignedTraceability.IsValidRowHandle(rowHandle))
            {
                if (this.GridControlAssignedTraceability.CurrentColumn == this.GridControlAssignedTraceability.Columns["DefaultSQL"])
                {
                    Messenger.Default.Send(Token.Message, Token.ShowSqlWindow);
                }
            }
        }
    }
}

