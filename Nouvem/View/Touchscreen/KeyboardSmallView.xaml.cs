﻿// -----------------------------------------------------------------------
// <copyright file="KeyboardSmallView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.Touchscreen
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for KeyboardView.xaml
    /// </summary>
    public partial class KeyboardSmallView : UserControl
    {
        public KeyboardSmallView()
        {
            this.InitializeComponent();
            this.Loaded += (sender, args) => this.TextBoxInput.Focus();
            this.Keyboard.Closed += (sender, args) =>
            {
                Messenger.Default.Send(this.TextBoxInput.Text, Token.KeyboardValueEntered);
                Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
            };
        }
    }
}
