﻿// -----------------------------------------------------------------------
// <copyright file="ProductsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;

namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for ProductsView.xaml
    /// </summary>
    public partial class ProductsView : UserControl
    {
        /// <summary>
        /// The grid scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public ProductsView()
        {
            this.InitializeComponent();
           
            this.Unloaded += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                ScrollViewer = null;
            };

            this.Loaded += (sender, args) =>
            {
                this.GridSearchBox.Height = 0;
                Messenger.Default.Register<bool>(this, Token.DisplayKeyboard, b =>
                {
                    if (b)
                    {
                        this.DisplayKeyboard();
                    }
                    else
                    {
                        this.Keyboard.IsOpen = false;
                        this.GridSearchBox.Height = 0;
                    }
                });

                this.GridControlProducts.SelectionMode =
                    ApplicationSettings.SortMode ? SelectionMode.Multiple : SelectionMode.Single;

                this.GridControlPoductGroups.SelectionMode =
                    ApplicationSettings.SortGroupMode ? SelectionMode.Multiple : SelectionMode.Single;

                Messenger.Default.Register<string>(this, Token.GetSortProducts, s =>
                {
                    var orders = new List<InventoryItem>();
                    foreach (var order in this.GridControlProducts.SelectedItems)
                    {
                        if (order != null && order is InventoryItem)
                        {
                            orders.Add((InventoryItem)order);
                        }
                    }

                    Messenger.Default.Send(orders, Token.SendSortProducts);
                });

                Messenger.Default.Register<string>(this, Token.GetSortGroupProducts, s =>
                {
                    var orders = new List<INGroup>();
                    foreach (var order in this.GridControlPoductGroups.SelectedItems)
                    {
                        if (order != null && order is INGroup)
                        {
                            orders.Add((INGroup)order);
                        }
                    }

                    Messenger.Default.Send(orders, Token.SendSortGroupProducts);
                });


                Messenger.Default.Register<string>(this, Token.ScrollProducts, s =>
                {
                    if (ScrollViewer == null)
                    {
                        ScrollViewer = GetScrollViewer(this.GridControlProducts) as ScrollViewer;
                    }

                    if (ScrollViewer == null)
                    {
                        return;
                    }

                    if (s.Equals(Constant.Up))
                    {
                        // scroll up
                        ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - ApplicationSettings.TouchScreenProductionProductsVerticalOffset);
                        return;
                    }

                    // scoll down
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + ApplicationSettings.TouchScreenProductionProductsVerticalOffset);
                });
            };
        }

        /// <summary>
        /// Gets a handle on the scroll bar, so we can programically manipulate.
        /// </summary>
        /// <param name="o"></param>
        /// <returns>The grid control scroll viewer.</returns>
        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Open the keyboard.
        /// </summary>
        private void DisplayKeyboard()
        {
            this.GridSearchBox.Height = 50;
            this.Keyboard.IsOpen = true;
            this.TextBoxSearch.Focus();
        }

        /// <summary>
        /// Handle the keyboard closing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameters.</param>
        private void Keyboard_Closed(object sender, System.EventArgs e)
        {
            this.GridSearchBox.Height = 0;
            //Messenger.Default.Send(Token.Message, Token.ClearSearch);
        }

        /// <summary>
        /// Needed for immediate selection of the group selection within the data template.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ItemOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = true;
        }

        /// <summary>
        /// Needed for immediate selection of the group selection within the data template.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ItemOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = false;
        }
    }
}
