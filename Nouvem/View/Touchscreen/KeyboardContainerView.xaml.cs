﻿// -----------------------------------------------------------------------
// <copyright file="KeyboardContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;

namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;

    /// <summary>
    /// Interaction logic for KeyboardContainerView.xaml
    /// </summary>
    public partial class KeyboardContainerView : Window
    {
        private readonly bool usingPassword;
        private ViewType target;

        public KeyboardContainerView(ViewType target, bool usingPassword = false)
        {
            this.InitializeComponent();
            this.usingPassword = usingPassword;
            this.target = target;

            this.TextBoxQuestion.Height = 0;
            Messenger.Default.Register<string>(this, Token.ShowAttributeQuestion, s =>
            {
                this.TextBoxQuestion.Text = s;
                this.TextBoxQuestion.Height = 90;
            });

            if (usingPassword)
            {
                this.TextBoxInput.Visibility = Visibility.Collapsed;
                this.TextBoxInput.IsEnabled = false;
                this.PasswordBoxInput.Visibility = Visibility.Visible;
                this.PasswordBoxInput.IsEnabled = true;
            }
            else
            {
                this.TextBoxInput.Visibility = Visibility.Visible;
                this.TextBoxInput.IsEnabled = true;
                this.PasswordBoxInput.Visibility = Visibility.Collapsed;
                this.PasswordBoxInput.IsEnabled = false;
            }

            Messenger.Default.Register<string>(this, Token.CloseContainerWindow, x => this.Close());

            this.Loaded += this.OnLoaded;
            this.Keyboard.Closed += this.KeyboardOnClosed;
            this.Closing += this.OnClosing;
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            Messenger.Default.Unregister(this);
            this.Keyboard.Closed -= this.KeyboardOnClosed;
            this.Loaded -= this.OnLoaded;
            this.Closing -= this.OnClosing;
        }

        private void KeyboardOnClosed(object sender, EventArgs eventArgs)
        {
            var text = this.usingPassword ? this.PasswordBoxInput.Password : this.TextBoxInput.Text;
            if (this.target == ViewType.APReceiptDetails)
            {
                Messenger.Default.Send(text, Token.KeyboardValueEnteredForBatch);
            }
            else if (this.target == ViewType.APReceiptTouchscreen)
            {
                Messenger.Default.Send(text, Token.KeyboardValueEnteredForReceiptBatch);
            }
            else if (target == ViewType.APReceipt)
            {
                Messenger.Default.Send(text, Token.KeyboardValueEnteredForPalletNo);
            }
            else if (this.target == ViewType.TransactionDetails)
            {
                Messenger.Default.Send(text, Token.LabelSearch);
            }
            else if (this.target == ViewType.ARDispatch)
            {
                Messenger.Default.Send(text, Token.BatchAndProductMismatchReason);
            }
            else
            {
                Messenger.Default.Send(text, Token.KeyboardValueEntered);
            }

            Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (this.usingPassword)
            {
                this.PasswordBoxInput.Focus();
            }
            else
            {
                this.TextBoxInput.Focus();
            }
        }
    }
}

