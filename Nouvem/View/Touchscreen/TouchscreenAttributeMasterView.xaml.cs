﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenTraceabilityView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows.Media;
using Nouvem.Logging;
using Nouvem.Shared.Localisation;

namespace Nouvem.View.Touchscreen
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.BusinessLogic;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for TouchscreenTraceabilityView.xaml
    /// </summary>
    public partial class TouchscreenAttributeMasterView : UserControl
    {
        #region field

        private Logger log = new Logger();

        /// <summary>
        /// The traceability selected tab.
        /// </summary>
        private static int? selectedTab;

        /// <summary>
        /// The current traceability text box.
        /// </summary>
        private static TextBox selectedTextBox;

        /// <summary>
        /// Loading traceability data flag.
        /// </summary>
        private bool loadingTraceabilityData;

        /// <summary>
        /// The current control allocation data.
        /// </summary>
        private AttributeAllocationData currentAllocationData;

        #endregion

        #region constructor

        public TouchscreenAttributeMasterView()
        {
            this.InitializeComponent();
            this.ButtonSave.Click += this.ButtonSaveOnClick;
            this.ButtonSaveNonStandard.Click += this.ButtonSaveOnClick;
            this.Loaded += this.OnLoaded;
            this.Unloaded += this.OnUnloaded;
            this.TabControlTraceability.SelectionChanged += this.TabControlTraceability_SelectionChanged;
            this.GridNonStandard.Visibility = Visibility.Collapsed;
            this.GridStandard.Visibility = Visibility.Visible;
        }

        #endregion

        #region private

        #region helper

        /// <summary>
        /// Creates the controls based on the incoming attribute data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        private void SetTraceability(List<AttributeAllocationData> data)
        {
            #region validation

            this.loadingTraceabilityData = true;

            if (this.TabControlTraceability.Items != null)
            {
                this.TabControlTraceability.Items.Clear();
            }

            if (this.TabControlTraceability.Items == null)
            {
                return;
            }

            #endregion

            var labelStyle = this.FindResource("StyleLabelTouchscreenLarge") as Style;
            var tabs = data.Where(x => x.AttributeTabName != null).OrderBy(tab => tab.AttributeTabName.OrderIndex).Select(x => x.AttributeTabName.Name).Distinct().ToArray();
            var tabIndex = -1;
            foreach (var tabData in tabs)
            {
                var tabItem = new TabItem { Header = tabData.Trim(), FontSize = 22 };
                var attributeData = data.Where(x => x.AttributeTabName != null && x.AttributeTabName.Name == tabData).ToList();
                var allTabItemsInvisible = attributeData.TrueForAll(x => !x.IsVisible);
                if (allTabItemsInvisible)
                {
                    // all the tab items are invisible, so don't create tab.
                    continue;
                }

                tabIndex++;
                var controlCount = 0;
                var masterStackPanel = new StackPanel { Orientation = Orientation.Horizontal };
                var stackPanel = new StackPanel { Orientation = Orientation.Vertical };

                var sequencedAttributeData = attributeData.OrderBy(x => x.AttributionAllocation.Sequence);
                foreach (var attributeAllocationData in sequencedAttributeData)
                {
                    if (!attributeAllocationData.IsVisible)
                    {
                        // attribute marked as invisible, so don't create.
                        continue;
                    }

                    if (attributeAllocationData.PropertyInfo == null)
                    {
                        this.log.LogError(this.GetType(), string.Format("CreateAttribute: PropertyInfo Null: AttributeMasterID:{0}", attributeAllocationData.AttributeMaster.AttributeMasterID));
                        continue;
                    }

                    attributeAllocationData.TabIndex = tabIndex;
                    var textBox = new TextBox
                    {
                        Tag = attributeAllocationData,
                        Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth,
                        Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                        FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center,
                        IsReadOnly = true,
                        IsHitTestVisible = attributeAllocationData.IsEditable,
                        Margin = new Thickness(0, 1, 0, 1)
                    };

                    textBox.PreviewMouseUp += this.TextBox_PreviewMouseUp;
                    var binding = new Binding
                    {
                        Source = attributeAllocationData.BindingViewModel,
                        Path = new PropertyPath(attributeAllocationData.PropertyInfo.Name),
                        UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                    };

                    BindingOperations.SetBinding(textBox, TextBox.TextProperty, binding);

                    var stackPanelFreeText = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                    stackPanelFreeText.Children.Add(new Label
                    {
                        Content = attributeAllocationData.AttributeMaster.Description,
                        Style = labelStyle,
                        Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                        Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                        FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                    });

                    stackPanelFreeText.Children.Add(textBox);
                    stackPanel.Children.Add(stackPanelFreeText);
                    controlCount++;

                    if (controlCount == ApplicationSettings.TouchscreenTraceabilityControlsStackedNumber)
                    {
                        masterStackPanel.Children.Add(stackPanel);
                        stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                        controlCount = 0;
                    }
                }

                if (controlCount > 0)
                {
                    var filler = ApplicationSettings.TouchscreenTraceabilityControlsStackedNumber - controlCount;
                    if (filler > 0)
                    {
                        for (int i = 0; i < filler; i++)
                        {
                            var stackPanelFiller = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                            stackPanelFiller.Children.Add(new System.Windows.Controls.Label
                            {
                                Visibility = Visibility.Hidden,
                                Style = labelStyle,
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                            });

                            stackPanel.Children.Add(stackPanelFiller);
                        }
                    }

                    masterStackPanel.Children.Add(stackPanel);
                }

                tabItem.Content = masterStackPanel;
                this.TabControlTraceability.Items.Add(tabItem);
            }

            if (this.TabControlTraceability.Items.Count > 0)
            {
                if (selectedTab.HasValue && this.TabControlTraceability.Items.Count >= selectedTab && selectedTab >= 0)
                {
                    this.TabControlTraceability.SelectedIndex = selectedTab.ToInt();
                }
                else
                {
                    this.TabControlTraceability.SelectedIndex = 0;
                }
            }

            this.loadingTraceabilityData = false;
        }

        /// <summary>
        /// Handes an unfilled required attribute, highlighting the associated control.
        /// </summary>
        /// <param name="attributeAllocationId">The unfilled attribute allocation id.</param>
        private void HandleUnfilledRequiredAttribute(int attributeAllocationId)
        {
            try
            {
                var control = GetEmptyControl(this.TabControlTraceability, attributeAllocationId);
                if (control != null)
                {
                    if (control is TextBox)
                    {
                        var textBox = (control as TextBox);
                        textBox.BorderBrush = Brushes.Red;
                        if (textBox.Tag != null && textBox.Tag is AttributeAllocationData)
                        {
                            this.TabControlTraceability.SelectedIndex = (textBox.Tag as AttributeAllocationData).TabIndex;
                        }
                    }
                }
            }
            catch (Exception e)
            {
               this.log.LogError(this.GetType(), e.Message);
            }
        }

        /// <summary>
        /// Finds the associated unfilled attribute control.
        /// </summary>
        /// <param name="o">The tab control.</param>
        /// <param name="allocationDataId">The attribute allocation data id to search for.</param>
        /// <returns>An associated textbox, or null if not found.</returns>
        public static DependencyObject GetEmptyControl(DependencyObject o, int allocationDataId)
        {
            if (o is TextBox)
            {
                var control = (TextBox) o;
                if (control.Tag != null && control.Tag is AttributeAllocationData)
                {
                    var data = (AttributeAllocationData) control.Tag;
                    if (data.AttributionAllocation != null && data.AttributionAllocation.AttributeAllocationID == allocationDataId)
                    {
                        return o;
                    }
                }
            }

            foreach (var child in LogicalTreeHelper.GetChildren(o))
            {
                if (child is DependencyObject)
                {
                    var result = GetEmptyControl((DependencyObject)child, allocationDataId);
                    if (result != null)
                    {
                        return result;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Clears the ui.
        /// </summary>
        private void ClearForm()
        {
            if (this.TabControlTraceability != null && this.TabControlTraceability.Items != null)
            {
                this.TabControlTraceability.Items.Clear();
            }
        }

        #endregion

        #region events

        /// <summary>
        /// The handler for the text box selections.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        void TextBox_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.currentAllocationData = null;
            if (sender is TextBox)
            {
                selectedTextBox = (sender as TextBox);
                var bc = new BrushConverter();
                selectedTextBox.BorderBrush = (Brush)bc.ConvertFrom("#FFA9A9A9");

                #region validation

                if (ApplicationSettings.DisallowStandardAttributeChange &&
                    !string.IsNullOrWhiteSpace(selectedTextBox.Text))
                {
                    return;
                }

                #endregion

                var attributeData = selectedTextBox.Tag as AttributeAllocationData;
                if (attributeData == null)
                {
                    return;
                }

                this.currentAllocationData = attributeData;

                if (string.IsNullOrEmpty(selectedTextBox.Text) && !this.currentAllocationData.UserCanAdd.ToBool())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoPermissionToAddAttribute);
                    NouvemMessageBox.Show(Message.NoPermissionToAddAttribute, touchScreen: true);
                    return;
                }

                if (!string.IsNullOrEmpty(selectedTextBox.Text) && !this.currentAllocationData.UserCanEdit.ToBool())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoPermissionToEditAttribute);
                    NouvemMessageBox.Show(Message.NoPermissionToEditAttribute, touchScreen: true);
                    return;
                }

                if (attributeData.TraceabilityType.CompareIgnoringCase(Constant.Calender))
                {
                    Messenger.Default.Send(Token.Message, Token.CreateCalender);
                }
                else if (attributeData.TraceabilityType.CompareIgnoringCase(Constant.Weight))
                {
                    Messenger.Default.Send(Token.Message, Token.CreateIndicator);
                }
                else if (attributeData.TraceabilityType.CompareIgnoringCase(Constant.ManualEntry))
                {
                    Messenger.Default.Send(ViewType.Keyboard);
                }
                else if (attributeData.TraceabilityType.CompareIgnoringCase(Constant.ManualNumericEntry))
                {
                    Keypad.Show(KeypadTarget.Traceability, displayMessage: attributeData.AttributeMaster.MessageText);
                }
                else if (attributeData.TraceabilityType.CompareIgnoringCase(Constant.Collection))
                {
                    var collectionData = attributeData.AttributeMaster.Collection.Split(',')
                            .Select(x => x.ToString())
                            .ToList();

                    Messenger.Default.Send(collectionData, Token.DisplayTouchscreenCollection);
                    Messenger.Default.Send(ViewType.CollectionDisplay);
                }
                else if (attributeData.TraceabilityType.CompareIgnoringCase(Constant.SQL))
                {
                    var script = attributeData.AttributeMaster.SQL;
                    if (script != null)
                    {
                        var scriptData = DataManager.Instance.GetData(script).ToList().Select(x => x.ToString()).ToList();
                        if (scriptData.Any())
                        {
                            Messenger.Default.Send(scriptData, Token.DisplayTouchscreenCollection);
                            Messenger.Default.Send(ViewType.CollectionDisplay);
                        }
                    }
                }

                Messenger.Default.Send(attributeData.AttributeMaster.MessageText, Token.ShowAttributeQuestion);
            }
        }

        /// <summary>
        /// Stores the last tab selection.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void TabControlTraceability_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!this.loadingTraceabilityData)
            {
                selectedTab = this.TabControlTraceability.SelectedIndex;
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Messenger.Default.Unregister(this);

            Messenger.Default.Register<int>(this, Token.EmptyRequiredAttribute, this.HandleUnfilledRequiredAttribute);
            Messenger.Default.Register<string>(this, Token.FocusToAttributeButton, s => this.ButtonSave.Focus());
            Messenger.Default.Register<string>(this, Token.ClearAttributeControls, s => this.ClearForm());

            Messenger.Default.Register<List<AttributeAllocationData>>(this, Token.SetTraceability, this.SetTraceability);
            if (selectedTab.HasValue)
            {
                this.TabControlTraceability.SelectedIndex = selectedTab.ToInt();
            }

            // Register for the no response entered for non standard response question.
            Messenger.Default.Register<string>(this, Token.NoResponseGiven, s =>
            {
                if (selectedTextBox != null)
                {
                    selectedTextBox.Text = string.Empty;
                }
            });

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<string>(this, Token.CollectionDisplayValue, s =>
            {
                if (selectedTextBox != null)
                {
                    selectedTextBox.Text = s;

                    if (this.currentAllocationData != null)
                    {
                        this.currentAllocationData.TraceabilityValue = s;
                        Messenger.Default.Send(this.currentAllocationData, Token.CheckAttributeValue);
                    }
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.Traceability, s =>
            {
                if (!string.IsNullOrEmpty(s) && selectedTextBox != null)
                {
                    selectedTextBox.Text = s;

                    if (this.currentAllocationData != null)
                    {
                        this.currentAllocationData.TraceabilityValue = s;
                        Messenger.Default.Send(this.currentAllocationData, Token.CheckAttributeValue);
                    }
                }
            });

            // Register for the incoming calender selection.
            Messenger.Default.Register<string>(this, Token.CalenderDisplayValue, s =>
            {
                if (selectedTextBox != null)
                {
                    selectedTextBox.Text = s;
                    if (this.currentAllocationData != null)
                    {
                        this.currentAllocationData.TraceabilityValue = s;
                        Messenger.Default.Send(this.currentAllocationData, Token.CheckDates);

                        if (this.currentAllocationData != null)
                        {
                            this.currentAllocationData.TraceabilityValue = s;
                            Messenger.Default.Send(this.currentAllocationData, Token.CheckAttributeValue);
                        }
                    }
                }
            });

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
            {
                if (c.Identifier.Equals(Constant.TouchscreenCollection) && selectedTextBox != null)
                {
                    selectedTextBox.Text = c.Data;

                    if (this.currentAllocationData != null)
                    {
                        this.currentAllocationData.TraceabilityValue = c.Data;
                        Messenger.Default.Send(this.currentAllocationData, Token.CheckAttributeValue);
                    }
                }
            });

            // Register for the incoming keyboard selection.
            Messenger.Default.Register<string>(this, Token.KeyboardValueEntered, t =>
            {
                if (selectedTextBox != null)
                {
                    selectedTextBox.Text = t;

                    if (this.currentAllocationData != null)
                    {
                        this.currentAllocationData.TraceabilityValue = t;
                        Messenger.Default.Send(this.currentAllocationData, Token.CheckAttributeValue);
                    }
                }
            });

            Messenger.Default.Send(Token.Message, Token.TouchscreenUiLoaded);
            this.GridNonStandard.Visibility = Visibility.Collapsed;
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            selectedTab = this.TabControlTraceability.SelectedIndex;
            this.ButtonSave.Click -= this.ButtonSaveOnClick;
            this.ButtonSaveNonStandard.Click -= this.ButtonSaveOnClick;
            this.Loaded -= this.OnLoaded;
            this.Unloaded -= this.OnUnloaded;
            this.TabControlTraceability.SelectionChanged -= this.TabControlTraceability_SelectionChanged;
            Messenger.Default.Unregister(this);
            BindingOperations.ClearAllBindings(this);
        }

        private void ButtonSaveOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            Messenger.Default.Send(Token.Message, Token.SaveBatchAttributes);
        }

        #endregion

        #endregion
    }
}



