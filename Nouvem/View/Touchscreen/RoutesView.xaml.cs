﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.View.Touchscreen
{
    /// <summary>
    /// Interaction logic for RoutesView.xaml
    /// </summary>
    public partial class RoutesView : Window
    {
        public RoutesView()
        {
            this.InitializeComponent();
            this.TextBoxQuestion.Height = 0;
            this.GridSearchBox.Height = 0;
#if !DEBUG
            this.Topmost = true;
#endif

            Messenger.Default.Register<string>(this, Token.CloseContainerWindow, x =>
            {
                this.Close();
            });

            this.Unloaded += this.OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Messenger.Default.Unregister(this);
            BindingOperations.ClearAllBindings(this);
            this.Unloaded -= this.OnUnloaded;
        }
    }
}
