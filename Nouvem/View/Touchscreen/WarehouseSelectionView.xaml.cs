﻿// -----------------------------------------------------------------------
// <copyright file="WarehouseSelectionView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Touchscreen
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for WarehouseSelectionView.xaml
    /// </summary>
    public partial class WarehouseSelectionView : Window
    {
        public WarehouseSelectionView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWarehouseSelectionWindow, s => this.Close());
        }
    }
}
