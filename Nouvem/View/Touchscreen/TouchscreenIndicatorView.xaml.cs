﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenIndicatorView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.BusinessLogic;

namespace Nouvem.View.Touchscreen
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TouchscreenIndicatorView.xaml
    /// </summary>
    public partial class TouchscreenIndicatorView : Window
    {
        public TouchscreenIndicatorView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.ShowAttributeQuestion, s =>
            {
                this.TextBoxQuestion.Text = s;
                this.TextBoxQuestion.Height = 90;
            });

            this.Closing += this.OnClosing;
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            IndicatorManager.Instance.OpenIndicatorPort();
            Messenger.Default.Unregister(this);
            this.Closing -= this.OnClosing;
        }

        private void Button_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            var wgt = ViewModel.ViewModelLocator.IndicatorStatic.Weight;
            Messenger.Default.Send(wgt.ToString(), Token.KeyboardValueEntered);
            this.Close();
        }

        private void Button_PreviewMouseUp_1(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }
}

