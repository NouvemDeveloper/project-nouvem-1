﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Plant
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Data;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TouchscreenWarehouseView.xaml
    /// </summary>
    public partial class TouchscreenWarehouseView : Window
    {
        public TouchscreenWarehouseView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseHiddenWindows, s => this.Close());

            Messenger.Default.Register<bool>(this, Token.CloseTouchscreenWarehouseWindow, b =>
            {
                if (b)
                {
                    this.Topmost = false;
                    this.Visibility = Visibility.Collapsed;
                }
                else
                {
                    this.Topmost = true;
                    this.Visibility = Visibility.Visible;
                }
            });

            Messenger.Default.Register<bool>(this, Token.DisplayWarehouseKeyboard, b => this.DisplayKeyboard(b));

            this.Closing += this.OnClosing;
            this.GridSearchBox.Height = 0;
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            Messenger.Default.Unregister(this);
            BindingOperations.ClearAllBindings(this);
            this.Closing -= this.OnClosing;
        }

        /// <summary>
        /// Open the keyboard.
        /// </summary>
        private void DisplayKeyboard(bool open)
        {
            if (open)
            {
                this.GridSearchBox.Height = 50;
                this.Keyboard.IsOpen = true;
                this.TextBoxSearch.Focus();
            }
            else
            {
                this.GridSearchBox.Height = 0;
                this.Keyboard.IsOpen = false;
            }
        }

        /// <summary>
        /// Handle the keyboard closing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameters.</param>
        private void Keyboard_Closed(object sender, System.EventArgs e)
        {
            this.GridSearchBox.Height = 0;
        }
    }
}

