﻿// -----------------------------------------------------------------------
// <copyright file="AuditContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Audit
{
    using System;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for AuditContainerView.xaml
    /// </summary>
    public partial class AuditContainerView : Window
    {
        public AuditContainerView(Tuple<string, int> data)
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseAuditWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Send(data, Token.AuditDataReceived);
        }
    }
}