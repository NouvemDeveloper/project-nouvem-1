﻿// -----------------------------------------------------------------------
// <copyright file="PaymentProposalSearchView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Printing;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Payments
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for PaymentProposalSearchView.xaml
    /// </summary>
    public partial class PaymentProposalSearchView : Window
    {
        public PaymentProposalSearchView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.ClosePaymentSearchWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.GetPaymentSelected, x => this.GetFilteredPayment());

            this.WindowState = WindowSettings.PaymentProposalSearchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            var gridPath = Settings.Default.PaymentProposalSearchGridpath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlSearchData.FilterCriteria = null;
                this.GridControlSearchData.View.SearchString = string.Empty;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                WindowSettings.PaymentProposalSearchHeight = this.Height;
                WindowSettings.PaymentProposalSearchLeft = this.Left;
                WindowSettings.PaymentProposalSearchTop = this.Top;
                WindowSettings.PaymentProposalSearchWidth = this.Width;
                WindowSettings.PaymentProposalSearchMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void GetFilteredPayment()
        {
            if (this.GridControlSearchData.VisibleRowCount == 1)
            {
                var dataRow = this.GridControlSearchData.GetRow(this.GridControlSearchData.GetRowHandleByVisibleIndex(0));
                if (dataRow is Sale)
                {
                    Messenger.Default.Send((dataRow as Sale).SaleID, Token.PaymentSelected);
                }
            }
        }
    }
}


