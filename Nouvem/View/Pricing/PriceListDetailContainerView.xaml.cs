﻿// -----------------------------------------------------------------------
// <copyright file="PriceListDetailContainerView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.View.Pricing
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for PriceListDetailContainerView.xaml
    /// </summary>
    public partial class PriceListDetailContainerView : Window
    {
        public PriceListDetailContainerView()
        {
            this.InitializeComponent();

            var gridPath = Settings.Default.PriceListDetailPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            Messenger.Default.Register<string>(this, Token.ClosePriceListDetailWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s => this.Topmost = false);
            this.WindowState = WindowSettings.PriceListDetailMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlPriceListDetail.FilterCriteria = null;
                this.GridControlPriceListDetail.View.SearchString = string.Empty;
                this.GridControlPriceListDetail.SaveLayoutToXml(gridPath);
                WindowSettings.PriceListDetailHeight = this.Height;
                WindowSettings.PriceListDetailLeft = this.Left;
                WindowSettings.PriceListDetailTop = this.Top;
                WindowSettings.PriceListDetailWidth = this.Width;
                WindowSettings.PriceListDetailMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlPriceListDetail.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }


        private void SetCheckedGroupPriceRows(bool check)
        {
            for (int i = 0; i < this.GridControlPriceListDetail.VisibleRowCount; i++)
            {
                var dataRow = this.GridControlPriceListDetail.GetRow(this.GridControlPriceListDetail.GetRowHandleByVisibleIndex(i));
                if (dataRow is PriceDetail)
                {
                    (dataRow as PriceDetail).GroupPriceChange = check;
                }

            }
        }

        private void CheckBox_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.SetCheckedGroupPriceRows(true);
        }

        private void CheckBox_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            this.SetCheckedGroupPriceRows(false);
        }
    }
}
