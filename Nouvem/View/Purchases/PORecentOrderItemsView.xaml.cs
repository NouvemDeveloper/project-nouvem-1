﻿// -----------------------------------------------------------------------
// <copyright file="PORecentOrderItemsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Printing;

namespace Nouvem.View.Purchases
{
    using System;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
   
    /// <summary>
    /// Interaction logic for PORecentOrderItemsView.xaml
    /// </summary>
    public partial class PORecentOrderItemsView : UserControl
    {
        public PORecentOrderItemsView()
        {
            this.InitializeComponent();
            this.Loaded += (sender, args) =>
            {
                this.SetFocus();
                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Unloaded += (sender, args) => Messenger.Default.Unregister(this);
        }

        private void SetFocus()
        {
            if (this.GridControlRecentOrderItems.VisibleRowCount == 0)
            {
                return;
            }

            var rowHandle = 0;

            if (this.GridControlRecentOrderItems.IsValidRowHandle(rowHandle))
            {
                this.GridControlRecentOrderItems.CurrentColumn = this.GridControlRecentOrderItems.Columns["QuantityOrdered"];
                this.TableViewData.FocusedRowHandle = rowHandle;
                this.Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.GridControlRecentOrderItems.Focus();
                    this.TableViewData.ShowEditor();
                }), System.Windows.Threading.DispatcherPriority.Render);
            }
        }
    }
}
