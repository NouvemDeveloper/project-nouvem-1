﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Purchases.APReceipt
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenContainerView.xaml
    /// </summary>
    public partial class TouchscreenContainerView : UserControl
    {
        /// <summary>
        /// The grid scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public TouchscreenContainerView()
        {
            this.InitializeComponent();

            //this.GridControlContainers.View.HideSearchPanel();
            //this.GridControlContainers.View.SearchString = string.Empty;
            //this.GridControlContainers.View.ShowFilterPanelMode = ShowFilterPanelMode.Never;

            this.Unloaded += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                ScrollViewer = null;
            };

            this.Loaded += (sender, args) =>
            {
                #region registration

                #region scrolling

                Messenger.Default.Register<string>(this, Token.ScrollProducts, s =>
                {
                    if (ScrollViewer == null)
                    {
                        ScrollViewer = GetScrollViewer(this.GridControlContainers) as ScrollViewer;
                    }

                    if (ScrollViewer == null)
                    {
                        return;
                    }

                    if (s.Equals(Constant.Up))
                    {
                        // scroll up
                        ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - Settings.Default.TouchScreenProductsVerticalOffset);
                        return;
                    }

                    // scoll down
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + Settings.Default.TouchScreenProductsVerticalOffset);
                });

                #endregion

                Messenger.Default.Register<string>(this, Token.DisplayKeyboard, s =>
                {
                    //this.GridControlContainers.View.ShowSearchPanel(true);
                    this.Keyboard.IsOpen = true;
                });

                Messenger.Default.Register<string>(this, Token.FilterProducts, s =>
                {
                    //var filter = string.Format("([Name] LIKE '{0}%')", s);
                    //this.GridControlContainers.FilterString = filter;
                });

                #endregion
            };
        }

        /// <summary>
        /// Gets a handle on the scroll bar, so we can programically manipulate.
        /// </summary>
        /// <param name="o"></param>
        /// <returns>The grid control scroll viewer.</returns>
        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        /// <summary>
        /// Needed for immediate selection of the group selection within the data template.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ItemOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = true;
        }

        /// <summary>
        /// Needed for immediate selection of the group selection within the data template.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event args.</param>
        private void ItemOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = false;
        }

        /// <summary>
        /// Handle the keyboard closing.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event parameters.</param>
        private void Keyboard_Closed(object sender, System.EventArgs e)
        {
            //this.GridControlContainers.View.HideSearchPanel();
            //this.GridControlContainers.View.ShowFilterPanelMode = ShowFilterPanelMode.Never;
        }
    }
}
