﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenProductsExpandGroupsView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Nouvem.View.Purchases.APReceipt
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenProductsExpandGroupsView .xaml
    /// </summary>
    public partial class TouchscreenProductsExpandGroupsView : UserControl
    {
        /// <summary>
        /// The grid scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public TouchscreenProductsExpandGroupsView()
        {
            this.InitializeComponent();
            this.ListViewProducts.Visibility = Visibility.Collapsed;

            this.Unloaded += this.OnUnloaded;
            this.Loaded += this.OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            #region registration

            Messenger.Default.Register<bool>(this, Token.ShowProducts, b =>
            {
                if (b)
                {
                    this.ListViewPoductGroups.Visibility = Visibility.Collapsed;
                    this.ListViewProducts.Visibility = Visibility.Visible;
                }
                else
                {
                    this.ListViewPoductGroups.Visibility = Visibility.Visible;
                    this.ListViewProducts.Visibility = Visibility.Collapsed;
                }
            });

            Messenger.Default.Register<string>(this, Token.ScrollProducts, s =>
            {
                if (ScrollViewer == null)
                {
                    ScrollViewer = GetScrollViewer(this.GridMaster) as ScrollViewer;
                }

                if (ScrollViewer == null)
                {
                    return;
                }

                if (s.Equals(Constant.Up))
                {
                    // scroll up
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - ApplicationSettings.TouchScreenProductsVerticalOffset);
                    return;
                }

                // scoll down
                ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + ApplicationSettings.TouchScreenProductsVerticalOffset);
            });

            #endregion
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Messenger.Default.Unregister(this);
            ScrollViewer = null;
            this.Loaded -= this.OnLoaded;
            this.Unloaded -= this.OnUnloaded;
        }

        /// <summary>
        /// Gets a handle on the scroll bar, so we can programically manipulate.
        /// </summary>
        /// <param name="o"></param>
        /// <returns>The grid control scroll viewer.</returns>
        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }
    }
}

