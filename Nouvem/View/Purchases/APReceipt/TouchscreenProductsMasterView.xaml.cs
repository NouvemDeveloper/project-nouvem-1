﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Purchases.APReceipt
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TouchscreenProductsMasterView.xaml
    /// </summary>
    public partial class TouchscreenProductsMasterView : UserControl
    {
        /// <summary>
        /// The grid scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public TouchscreenProductsMasterView()
        {
            this.InitializeComponent();

            this.Unloaded += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                ScrollViewer = null;
            };
    
            this.Loaded += (sender, args) =>
            {
                #region registration

                #region scrolling

                Messenger.Default.Register<string>(this, Token.ScrollProducts, s =>
                {
                    if (ScrollViewer == null)
                    {
                        ScrollViewer = GetScrollViewer(this.GridControlProducts) as ScrollViewer;
                    }

                    if (ScrollViewer == null)
                    {
                        return;
                    }

                    if (s.Equals(Constant.Up))
                    {
                        // scroll up
                        ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - ApplicationSettings.TouchScreenProductsVerticalOffset);
                        return;
                    }

                    // scoll down
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + ApplicationSettings.TouchScreenProductsVerticalOffset);
                });

                #endregion
      
                #endregion
            };
        }

        /// <summary>
        /// Gets a handle on the scroll bar, so we can programically manipulate.
        /// </summary>
        /// <param name="o"></param>
        /// <returns>The grid control scroll viewer.</returns>
        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }
    }
}
