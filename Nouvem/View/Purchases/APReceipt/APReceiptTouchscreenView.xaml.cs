﻿// -----------------------------------------------------------------------
// <copyright file="APReceiptTouchscreenView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Purchases.APReceipt
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for APReceiptTouchscreenView.xaml
    /// </summary>
    public partial class APReceiptTouchscreenView : UserControl
    {
        public APReceiptTouchscreenView()
        {
            this.InitializeComponent();
        }
    }
}
