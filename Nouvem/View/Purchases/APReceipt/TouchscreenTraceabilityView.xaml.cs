﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenTraceabilityView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.SqlTypes;
using DevExpress.XtraPrinting.Native;
using Nouvem.ViewModel;

namespace Nouvem.View.Purchases.APReceipt
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Logging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Model.DataLayer.Repository;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for TouchscreenTraceabilityView.xaml
    /// </summary>
    [Obsolete("Deprecated", true)]
    public partial class TouchscreenTraceabilityView : UserControl
    {
        #region field

        /// <summary>
        /// The traceability selected tab.
        /// </summary>
        private static int? selectedTab;

        /// <summary>
        /// The current batch traceability data.
        /// </summary>
        private static List<TraceabilityData> BatchTraceabilityData;

        /// <summary>
        /// The batch numbers.
        /// </summary>
        private static readonly HashSet<string> BatchNumbers = new HashSet<string>();

        /// <summary>
        /// The batch control contents.
        /// </summary>
        private static IList<StackPanel> BatchContentControls = new List<StackPanel>();

        /// <summary>
        /// The traceability data holder.
        /// </summary>
        private IList<Tuple<string, bool, TraceabilityData>> traceabilityData = new List<Tuple<string, bool, TraceabilityData>>();

        /// <summary>
        /// The logging reference.
        /// </summary>
        private ILogger log = new Logger();

        /// <summary>
        /// The current traceability combo box.
        /// </summary>
        private static ComboBox selectedComboBox;

        /// <summary>
        /// The current traceability text box.
        /// </summary>
        private static TextBox selectedTextBox;

        /// <summary>
        /// The data respository.
        /// </summary>
        private IDynamicRepository repository = new DynamicRepository();

        /// <summary>
        /// The current batch number.
        /// </summary>
        private string batchNo;

        /// <summary>
        /// The free text box control count.
        /// </summary>
        private int freeTextCount = 1;

        /// <summary>
        /// The collection control count.
        /// </summary>
        private int dataCount = 1;

        /// <summary>
        /// The date picker control count.
        /// </summary>
        private int dateCount = 1;

        #endregion

        #region constructor

        private TouchscreenTraceabilityView()
        {
            this.InitializeComponent();

            this.Loaded += (sender, args) =>
            {
                Messenger.Default.Unregister(this);
                if (selectedTab.HasValue)
                {
                    this.TabControlTraceability.SelectedIndex = selectedTab.ToInt();
                }

                // Register for the incoming combo collection selection.
                Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
                {
                    if (c.Identifier.Equals(Constant.TouchscreenTraceability) && selectedComboBox != null)
                    {
                        selectedComboBox.SelectedValue = c.Data;
                    }
                });
               
                // Register for the incoming keyboard selection.
                Messenger.Default.Register<string>(this, Token.KeyboardValueEntered, t =>
                {
                    if (selectedTextBox != null)
                    {
                        selectedTextBox.Text = t;
                    }
                });

                // Register for a request to send the current items traceability data.
                Messenger.Default.Register<string>(this, Token.GetTouchscreenTraceabilityData, s =>
                {
                    this.GetTraceabilityData();
                });

                // Register for the incoming batch traceability data.
                Messenger.Default.Register<List<TraceabilityData>>(this, Token.CreateTouchscreenDynamicControls, data =>
                {
                    this.CreateBatchControls(data);
                });

                // Register for the incoming serial traceability data.
                Messenger.Default.Register<List<TraceabilityData>>(this, Token.CreateTouchscreenDynamicSerialColumns,
                    data =>
                    {
                        this.CreateSerialControls(data);
                    });

                // Register for the attributes reset.
                Messenger.Default.Register<string>(this, Token.ClearData, s => this.ClearData());

                // Register for the attributes reset.
                Messenger.Default.Register<string>(this, Token.ClearAndResetData, s => this.ClearAndResetData());
               
                Messenger.Default.Register<BatchNumber>(this, Token.NewTouchscreenBatchNumberGenerated, this.UpdateBatch);

                Messenger.Default.Register<string>(this, Token.ResetSerialData, s => this.ResetSerialData());

                Messenger.Default.Register<Tuple<List<BarcodeParse>, string>>(this, this.SetSerialData);

                Messenger.Default.Send(Token.Message, Token.TouchscreenUiLoaded);

                this.ignoreDateChanges = false;
            };

            this.Unloaded += (sender, args) =>
            {
                this.ignoreDateChanges = true;
                selectedTab = this.TabControlTraceability.SelectedIndex;
                Messenger.Default.Unregister(this);
            };
        }

        #endregion

        #region private

        #region create controls

        #region batch

        /// <summary>
        /// Creates the dynamic serial traceability controls.
        /// </summary>
        /// <param name="traceData">The control data.</param>
        /// <param name="updatingBatchNo">Are we updating the batch no.</param>
        private void CreateBatchControls(List<TraceabilityData> traceData, bool updatingBatchNo = false)
        {
            //NouvemGlobal.SelectingAndWeighingProductAtDispatch = false;
            this.ignoreDateChanges = true;
            if (!NouvemGlobal.ScanningAtDispatch || this.CallingModule() != ViewType.ARDispatch || NouvemGlobal.SelectingAndWeighingProductAtDispatch || traceData.TrueForAll(x => string.IsNullOrEmpty(x.TraceabilityValue)))
            {
                foreach (var data in traceData)
                {
                    data.ScanningAtDispatch = false;
                }
            }

            try
            {
                this.TabItemBatchTraceability.Visibility = Visibility.Collapsed;
                this.TabItemBatchQuality.Visibility = Visibility.Collapsed;
                this.TabItemBatchDates.Visibility = Visibility.Collapsed;

                if (!traceData.Any())
                {
                    this.log.LogDebug(this.GetType(), "CreateBatchControls(): No trace data. Exiting");
                    return;
                }

                this.log.LogDebug(this.GetType(), "CreateBatchControls(): Creating batch controls.");
                this.TabItemBatchTraceability.Style = this.FindResource("StyleTabItemBatchTouchscreen") as Style;
                this.TabItemBatchQuality.Style = this.FindResource("StyleTabItemBatchTouchscreen") as Style;
                this.TabItemBatchDates.Style = this.FindResource("StyleTabItemBatchTouchscreen") as Style;

                // this.TabControlTraceability.SelectedIndex = 0;
                BatchTraceabilityData = traceData;
                var labelStyle = this.FindResource("StyleLabelTouchscreenLarge") as Style;
                var datePickerStyle = this.FindResource("StyleDatePickerExtraLarge") as Style;
                var comboBoxStyle = this.FindResource("StyleComboBoxTouchScreen") as Style;

                if (traceData.First().BatchNumber != null)
                {
                    this.batchNo = traceData.First().BatchNumber.Number;
                }

                if (this.batchNo == null)
                {
                    return;
                }

                var containsBatch = BatchNumbers.Contains(this.batchNo);
                var batchValuesSet = this.BatchTraceabilityValuesSet(this.batchNo);
                this.log.LogDebug(this.GetType(), string.Format("Contains Batch:{0} - Values Set:{1}", containsBatch, batchValuesSet));

                if (!batchValuesSet)
                {

                }

                // check if the batch controls exist i.e. we are working with an existing batch
                if (containsBatch && batchValuesSet)
                {
                    this.log.LogDebug(this.GetType(),
                        string.Format("Batch Number:{0} exists. Using existing batch controls", batchNo));
                    this.SwitchBatchTraceabilityData(this.batchNo);
                    return;
                }

                BatchNumbers.Add(this.batchNo);

                if (!NouvemGlobal.BatchValuesSet.ContainsKey(this.batchNo))
                {
                    NouvemGlobal.BatchValuesSet.Add(this.batchNo, false);
                }

                var dataGroups = traceData.GroupBy(x => x.TraceabilityMethod);

                foreach (var dataGroup in dataGroups)
                {
                    var traceabilityMethod = dataGroup.Key;
                    var masterName = string.Format("StackPanelBatch{0}{1}", traceabilityMethod, batchNo);
                    this.log.LogDebug(this.GetType(), string.Format("Master Name:{0}", masterName));

                    var masterStackPanel = new StackPanel { Name = masterName, Orientation = Orientation.Horizontal };
                    var stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                    var controlCount = 0;

                    foreach (var data in dataGroup)
                    {
                        var description = string.Format("{0}:", data.TraceabilityMasterDescription);
                        this.log.LogDebug(this.GetType(), string.Format("Traceability method:{0}", description));
                        this.log.LogDebug(this.GetType(), string.Format("data.TraceabilityType:{0}", data.TraceabilityType));

                        if (data.TraceabilityType.Equals(Constant.ManualEntry))
                        {
                            var value = data.TraceabilityValue;

                            // free text
                            var controlName = string.Format("TextBox{0}", this.freeTextCount).Replace(" ", "");
                            var textBox = new TextBox
                            {
                                Name = controlName,
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize,
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                                Margin = new Thickness(0, 1, 0, 1)
                            };

                            textBox.PreviewMouseUp += this.TextBox_PreviewMouseUp;
                            if (!string.IsNullOrEmpty(value) && !updatingBatchNo)
                            {
                                textBox.Text = value;
                                textBox.IsHitTestVisible = false;
                            }

                            this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));

                            this.RegisterName(controlName, textBox);

                            var stackPanelFreeText = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                            stackPanelFreeText.Children.Add(new System.Windows.Controls.Label
                            {
                                Content = description,
                                Style = labelStyle,
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                            });

                            stackPanelFreeText.Children.Add(textBox);

                            stackPanel.Children.Add(stackPanelFreeText);

                            traceabilityData.Add(Tuple.Create(controlName, true, data));
                            this.freeTextCount++;
                        }
                        else if (data.TraceabilityType.Equals(Constant.Calender))
                        {
                            // datetime
                            var datePicker = new DatePicker
                            {
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                Style = datePickerStyle,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize,
                                SelectedDate = DateTime.Today,
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                                Margin = new Thickness(0, 1, 0, 1),
                                Tag = data.TraceabilityMasterID
                            };

                            var dateType = NouvemGlobal.DateTypes.FirstOrDefault(x => x.NouDateTypeID == data.TraceabilityTypeID);

                            if (data.ScanningAtDispatch)
                            {
                                // scanning at dispatch, so just recall value.
                                datePicker.SelectedDate = data.TraceabilityValue.ToDate();
                            }
                            else if (dateType != null && dateType.DateType.Equals(DateType.CurrentDate))
                            {
                                datePicker.SelectedDate = DateTime.Today;
                            }
                            else if (data.DateDays != null && data.DateDays.Any())
                            {
                                var daysForward =
                                    data.DateDays.FirstOrDefault(x => x.INMasterID == TraceabilityData.INMasterId && x.DateMasterID == data.TraceabilityMasterID);

                                if (daysForward != null && daysForward.Days > 0)
                                {
                                    //var dateType = NouvemGlobal.DateTypes.FirstOrDefault(x => x.NouDateTypeID == data.TraceabilityTypeID);

                                    if (dateType.DateType.Equals(DateType.ForwardDate))
                                    {
                                        datePicker.SelectedDate = DateTime.Today.Add(TimeSpan.FromDays(daysForward.Days.ToInt()));
                                    }
                                    else if (dateType.DateType.Equals(DateType.BackDate))
                                    {
                                        datePicker.SelectedDate = DateTime.Today.Add(TimeSpan.FromDays(-daysForward.Days.ToInt()));
                                    }
                                }
                                else if (!string.IsNullOrEmpty(data.TraceabilityValue) && ApplicationSettings.ResetSerialAttributesManually)
                                {
                                    datePicker.SelectedDate = data.TraceabilityValue.ToDate();
                                    datePicker.IsHitTestVisible = false;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(data.TraceabilityValue) && !updatingBatchNo)
                                {
                                    datePicker.SelectedDate = data.TraceabilityValue.ToDate();
                                    datePicker.IsHitTestVisible = false;
                                }
                            }

                            datePicker.PreviewMouseUp += this.DatePicker_PreviewMouseUp;
                            datePicker.SelectedDateChanged += this.DatePicker_SelectedDateChanged;

                            var controlName = string.Format("DatePicker{0}", this.dateCount).Replace(" ", "");
                            this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));

                            this.RegisterName(controlName, datePicker);

                            var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                            stackPanelCalender.Children.Add(new System.Windows.Controls.Label
                            {
                                Content = description,
                                Style = labelStyle,
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                            });

                            stackPanelCalender.Children.Add(datePicker);
                            //stackPanelCalender.Children.Add(new TextBlock { Width = 10, IsHitTestVisible = false });
                            stackPanel.Children.Add(stackPanelCalender);

                            traceabilityData.Add(Tuple.Create(controlName, true, data));
                            this.dateCount++;
                        }
                        else if (data.TraceabilityType.Equals(Constant.Collection))
                        {
                            // collection
                            var collectionData = data.TraceabilityMasterCollection.Split(',')
                                .Select(x => x.ToString())
                                .ToList();

                            var controlName = string.Format("ComboBox{0}", this.dataCount).Replace(" ", "");
                            var combo = new ComboBox
                            {
                                Name = controlName,
                                ItemsSource = collectionData,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize,
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                                Margin = new Thickness(0, 1, 0, 1)
                            };

                            combo.PreviewMouseUp += this.ComboOnPreviewMouseUp;
                            if (!string.IsNullOrEmpty(data.TraceabilityValue) && !updatingBatchNo)
                            {
                                combo.Text = data.TraceabilityValue;
                                combo.IsHitTestVisible = false;
                            }

                            this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));
                            this.RegisterName(controlName, combo);

                            var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                            stackPanelCalender.Children.Add(new System.Windows.Controls.Label
                            {
                                Content = description,
                                Style = labelStyle,
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                            });

                            stackPanelCalender.Children.Add(combo);
                            // stackPanelCalender.Children.Add(new TextBlock { Width = 10, IsHitTestVisible = false });
                            stackPanel.Children.Add(stackPanelCalender);

                            traceabilityData.Add(Tuple.Create(controlName, true, data));
                            this.dataCount++;
                        }
                        else
                        {
                            // sql
                            var script = data.TraceabilityMasterSQL;
                            var commandData = this.repository.GetData(script).ToList().Select(x => x.ToString());

                            var controlName = string.Format("ComboBox{0}", this.dataCount).Replace(" ", "");
                            var combo = new ComboBox
                            {
                                Name = controlName,
                                ItemsSource = commandData,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize,
                                HorizontalAlignment = HorizontalAlignment.Center,
                                VerticalAlignment = VerticalAlignment.Center,
                                Margin = new Thickness(0, 1, 0, 1)
                            };

                            combo.PreviewMouseUp += this.ComboOnPreviewMouseUp;
                            if (!string.IsNullOrEmpty(data.TraceabilityValue) && !updatingBatchNo)
                            {
                                combo.Text = data.TraceabilityValue;
                                //combo.IsHitTestVisible = false;
                            }

                            this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));

                            this.RegisterName(controlName, combo);

                            var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                            stackPanelCalender.Children.Add(new System.Windows.Controls.Label
                            {
                                Content = description,
                                Style = labelStyle,
                                Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                                Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                            });

                            stackPanelCalender.Children.Add(combo);
                            // stackPanelCalender.Children.Add(new TextBlock { Width = 10, IsHitTestVisible = false });
                            stackPanel.Children.Add(stackPanelCalender);

                            traceabilityData.Add(Tuple.Create(controlName, true, data));
                            this.dataCount++;
                        }

                        controlCount++;
                        if (controlCount == ApplicationSettings.TouchscreenTraceabilityControlsStackedNumber)
                        {
                            masterStackPanel.Children.Add(stackPanel);
                            stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                            controlCount = 0;
                        }
                    }

                    if (controlCount > 0)
                    {
                        var filler = ApplicationSettings.TouchscreenTraceabilityControlsStackedNumber - controlCount;
                        if (filler > 0)
                        {
                            for (int i = 0; i < filler; i++)
                            {
                                var stackPanelFiller = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                                stackPanelFiller.Children.Add(new System.Windows.Controls.Label
                                {
                                    Visibility = Visibility.Hidden,
                                    Style = labelStyle,
                                    Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                                    Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                                    FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                                });

                                stackPanel.Children.Add(stackPanelFiller);
                            }
                        }

                        masterStackPanel.Children.Add(stackPanel);
                    }

                    try
                    {
                        this.RegisterName(masterName, masterStackPanel);
                    }
                    catch (Exception ex)
                    {
                        this.log.LogError(this.GetType(), ex.Message);
                        if (BatchNumbers.Contains(this.batchNo))
                        {
                            this.log.LogDebug(this.GetType(), "Register Name Error");
                            if (containsBatch)
                            {
                                this.SwitchBatchTraceabilityData(this.batchNo);
                            }

                            return;
                        }
                    }

                    if (traceabilityMethod.Equals(Constant.Traceability))
                    {
                        this.ContentControlTraceability.Content = masterStackPanel;
                        this.TabItemBatchTraceability.Visibility = Visibility.Visible;
                        // this.TabItemBatchTraceability.Style = this.FindResource("StyleTabItemTreaceabilityWithValues") as Style;
                    }
                    else if (traceabilityMethod.Equals(Constant.Quality))
                    {
                        this.ContentControlQuality.Content = masterStackPanel;
                        this.TabItemBatchQuality.Visibility = Visibility.Visible;
                        //this.TabItemBatchQuality.Style = this.FindResource("StyleTabItemTreaceabilityWithValues") as Style;
                    }
                    else
                    {
                        this.ContentControlDates.Content = masterStackPanel;
                        this.TabItemBatchDates.Visibility = Visibility.Visible;
                        //this.TabItemBatchDates.Style = this.FindResource("StyleTabItemTreaceabilityWithValues") as Style;
                    }

                    BatchContentControls.Add(masterStackPanel);
                }
            }
            finally
            {
                this.ignoreDateChanges = false;
            }
        }

        #endregion

        #region serial

        /// <summary>
        /// Creates the dynamic serial traceability controls.
        /// </summary>
        /// <param name="traceData">The control data.</param>
        private void CreateSerialControls(List<TraceabilityData> traceData)
        {
            //NouvemGlobal.SelectingAndWeighingProductAtDispatch = false;
            this.ignoreDateChanges = true;
            this.log.LogDebug(this.GetType(), "CreateSerialControls(): Creating serial controls.");
            this.TabItemSerialTraceability.Visibility = Visibility.Collapsed;
            this.TabItemSerialQuality.Visibility = Visibility.Collapsed;
            this.TabItemSerialDates.Visibility = Visibility.Collapsed;

            if (!NouvemGlobal.ScanningAtDispatch || this.CallingModule() != ViewType.ARDispatch || NouvemGlobal.SelectingAndWeighingProductAtDispatch || traceData.TrueForAll(x => string.IsNullOrEmpty(x.TraceabilityValue)))
            {
                foreach (var data in traceData)
                {
                    data.ScanningAtDispatch = false;
                }
            }

            if (!traceData.Any())
            {
                this.log.LogDebug(this.GetType(), "CreateSerialControls(): No trace data. Exiting");
                return;
            }

            var batchNo = string.Empty;

            if (traceData.First().BatchNumber != null)
            {
                batchNo = traceData.First().BatchNumber.Number;
            }

            //var batchNo = traceData.First().BatchNumber.Number;

            var labelStyle = this.FindResource("StyleLabelTouchscreenLarge") as Style;
            var datePickerStyle = this.FindResource("StyleDatePickerExtraLarge") as Style;
            var comboBoxStyle = this.FindResource("StyleComboBoxTouchScreen") as Style;

            var dataGroups = traceData.GroupBy(x => x.TraceabilityMethod);

            var freeTextCounter = 1;
            var dataCounter = 1;
            var dateCounter = 1;
            var collectionCounter = 1;

            foreach (var dataGroup in dataGroups)
            {
                var traceabilityMethod = dataGroup.Key;
                var masterName = string.Format("StackPanelSerial{0}{1}", traceabilityMethod, batchNo);
                this.log.LogDebug(this.GetType(), string.Format("Master Name:{0}", masterName));

                var masterStackPanel = new StackPanel { Name = masterName, Orientation = Orientation.Horizontal };
                var stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                var controlCount = 0;

                foreach (var data in dataGroup)
                {
                    var description = string.Format("{0}:", data.TraceabilityMasterDescription);
                    this.log.LogDebug(this.GetType(), string.Format("Traceability method:{0}", description));

                    if (data.TraceabilityType.Equals(Constant.ManualEntry))
                    {
                        var value = data.TraceabilityValue;

                        // free text
                        var controlName = string.Format("TextBox{0}", this.freeTextCount).Replace(" ", "");
                        var textBox = new TextBox
                        {
                            Name = controlName,
                            Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth,
                            Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                            FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize, 
                            HorizontalAlignment = HorizontalAlignment.Center, 
                            VerticalAlignment = VerticalAlignment.Center, 
                            Margin = new Thickness(0,1,0,1)
                        };

                        textBox.PreviewMouseUp += this.TextBox_PreviewMouseUp;
                 
                        if (!string.IsNullOrEmpty(value))
                        {
                            textBox.Text = value;
                        }
                        
                        data.DynamicColumnName = string.Format("FreeText{0}", freeTextCounter);
                        this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));

                        this.RegisterName(controlName, textBox);

                        var stackPanelFreeText = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelFreeText.Children.Add(new System.Windows.Controls.Label
                        {
                            Content = description, 
                            Style = labelStyle,
                            Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                            Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                            FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                        });

                        stackPanelFreeText.Children.Add(textBox);

                        stackPanel.Children.Add(stackPanelFreeText);

                        traceabilityData.Add(Tuple.Create(controlName, false, data));
                        this.freeTextCount++;
                        freeTextCounter++;
                    }
                    else if (data.TraceabilityType.Equals(Constant.Calender))
                    {
                        // datetime
                        var datePicker = new DatePicker
                        {
                            Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth,
                            Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight, 
                            FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize,
                            Style = datePickerStyle, 
                            SelectedDate = DateTime.Today, 
                            HorizontalAlignment = HorizontalAlignment.Center, 
                            VerticalAlignment = VerticalAlignment.Center, 
                            Tag = data.TraceabilityMasterID,
                            Margin = new Thickness(0,1,0,1)
                        };

                        var dateType = NouvemGlobal.DateTypes.FirstOrDefault(x => x.NouDateTypeID == data.TraceabilityTypeID);

                        if (data.ScanningAtDispatch)
                        {
                            // scanning at dispatch, so just recall value.
                            datePicker.SelectedDate = data.TraceabilityValue.ToDate();
                        }
                        else if (dateType != null && dateType.DateType.Equals(DateType.CurrentDate))
                        {
                            datePicker.SelectedDate = DateTime.Today;
                        }
                        else if (dateType != null && data.DateDays != null && data.DateDays.Any())
                        {
                            var daysForward =
                                data.DateDays.FirstOrDefault(x => x.INMasterID == TraceabilityData.INMasterId && x.DateMasterID == data.TraceabilityMasterID);

                            if (daysForward != null && daysForward.Days > 0)
                            {
                                //var dateType = NouvemGlobal.DateTypes.FirstOrDefault(x => x.NouDateTypeID == data.TraceabilityTypeID);

                                if (dateType.DateType.Equals(DateType.ForwardDate))
                                {
                                    if (data.DateMasterBasedOn != null)
                                    {
                                        TraceabilityData basedOnDate = null;

                                        if (BatchTraceabilityData != null)
                                        {
                                            basedOnDate = BatchTraceabilityData.Where(x => x.TraceabilityType.Equals(Constant.Calender))
                                                .FirstOrDefault(
                                                    x => x.TraceabilityMasterID == data.DateMasterBasedOn.DateMasterID);
                                        }

                                        if (basedOnDate == null)
                                        {
                                            basedOnDate =
                                           traceData.Where(x => x.TraceabilityType.Equals(Constant.Calender))
                                               .FirstOrDefault(
                                                   x => x.TraceabilityMasterID == data.DateMasterBasedOn.DateMasterID);
                                        }
                                       
                                        if (basedOnDate != null)
                                        {
                                            if (!string.IsNullOrEmpty(basedOnDate.TraceabilityValue))
                                            {
                                                if (basedOnDate.TraceabilityValue.ToDate() > "01/01/01".ToDate())
                                                {
                                                    datePicker.SelectedDate = basedOnDate.TraceabilityValue.ToDate().Add(TimeSpan.FromDays(daysForward.Days.ToInt()));
                                                }
                                                else
                                                {
                                                    datePicker.SelectedDate = DateTime.Today.Add(TimeSpan.FromDays(daysForward.Days.ToInt()));
                                                }
                                            }
                                            else
                                            {
                                                DateTime? basedOnDateDate = null;
                                                var basedOnDateType = NouvemGlobal.DateTypes.FirstOrDefault(x => x.NouDateTypeID == basedOnDate.TraceabilityTypeID);
                                                if (basedOnDateType != null && basedOnDateType.DateType.Equals(DateType.CurrentDate))
                                                {
                                                    basedOnDateDate = DateTime.Today;
                                                }
                                                else if (basedOnDateType != null && basedOnDate.DateDays != null && basedOnDate.DateDays.Any())
                                                {
                                                    var basedOnDaysForward =
                                                        basedOnDate.DateDays.FirstOrDefault(x => x.INMasterID == TraceabilityData.INMasterId && x.DateMasterID == basedOnDate.TraceabilityMasterID);

                                                    if (basedOnDaysForward != null)
                                                    {
                                                        //var dateType = NouvemGlobal.DateTypes.FirstOrDefault(x => x.NouDateTypeID == data.TraceabilityTypeID);

                                                        if (basedOnDateType.DateType.Equals(DateType.ForwardDate))
                                                        {
                                                            basedOnDateDate = DateTime.Today.Add(TimeSpan.FromDays(daysForward.Days.ToInt()));
                                                        }
                                                        else if (basedOnDateType.DateType.Equals(DateType.BackDate))
                                                        {
                                                            basedOnDateDate = DateTime.Today.Add(TimeSpan.FromDays(-daysForward.Days.ToInt()));
                                                        }
                                                    }
                                                    else if (!string.IsNullOrEmpty(basedOnDate.TraceabilityValue) && ApplicationSettings.ResetSerialAttributesManually)
                                                    {
                                                        basedOnDateDate = basedOnDate.TraceabilityValue.ToDate();
                                                    }
                                                }

                                                if (basedOnDateDate.HasValue)
                                                {
                                                    datePicker.SelectedDate = basedOnDateDate;
                                                }
                                                else
                                                {
                                                    datePicker.SelectedDate = DateTime.Today.Add(TimeSpan.FromDays(daysForward.Days.ToInt()));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            datePicker.SelectedDate = DateTime.Today.Add(TimeSpan.FromDays(daysForward.Days.ToInt()));
                                        }
                                    }
                                    else
                                    {
                                        datePicker.SelectedDate = DateTime.Today.Add(TimeSpan.FromDays(daysForward.Days.ToInt()));
                                    }
                                   
                                }
                                else if (dateType.DateType.Equals(DateType.BackDate))
                                {
                                    datePicker.SelectedDate = DateTime.Today.Add(TimeSpan.FromDays(-daysForward.Days.ToInt()));
                                }
                            }
                            else if (!string.IsNullOrEmpty(data.TraceabilityValue) && ApplicationSettings.ResetSerialAttributesManually)
                            {
                                datePicker.SelectedDate = data.TraceabilityValue.ToDate();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(data.TraceabilityValue) && ApplicationSettings.ResetSerialAttributesManually)
                            {
                                var localDate = data.TraceabilityValue.ToDate();
                                if (localDate == "01/01/01".ToDate())
                                {
                                    datePicker.SelectedDate = null;
                                }
                                else
                                {
                                    datePicker.SelectedDate = localDate;
                                }
                            }
                        }

                        datePicker.PreviewMouseUp += this.DatePicker_PreviewMouseUp;
                        datePicker.SelectedDateChanged += this.DatePicker_SelectedDateChanged;

                        var controlName = string.Format("DatePicker{0}", this.dateCount).Replace(" ", "");
            
                        data.DynamicColumnName = string.Format("Date{0}", dateCounter);
                        this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));

                        this.RegisterName(controlName, datePicker);

                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label
                        {
                            Content = description, 
                            Style = labelStyle,
                            Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                            Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                            FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                        });

                        stackPanelCalender.Children.Add(datePicker);
                        //stackPanelCalender.Children.Add(new TextBlock { Width = 10, IsHitTestVisible = false });
                        stackPanel.Children.Add(stackPanelCalender);

                        this.traceabilityData.Add(Tuple.Create(controlName, false, data));
                        this.dateCount++;
                        dateCounter++;
                    }
                    else if (data.TraceabilityType.Equals(Constant.Collection))
                    {
                        // collection
                        var collectionData = data.TraceabilityMasterCollection.Split(',')
                            .Select(x => x.ToString())
                            .ToList();

                        var controlName = string.Format("ComboBox{0}", this.dataCount).Replace(" ", "");
                        var combo = new ComboBox
                        {
                            Name = controlName, 
                            ItemsSource = collectionData,
                            Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                            Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth, 
                            FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize,
                            Style = comboBoxStyle, 
                            HorizontalAlignment = HorizontalAlignment.Center, 
                            VerticalAlignment = VerticalAlignment.Center, 
                            Margin = new Thickness(0,1,0,1)
                        };

                        combo.PreviewMouseUp += this.ComboOnPreviewMouseUp;
                        if (!string.IsNullOrEmpty(data.TraceabilityValue))
                        {
                            combo.Text = data.TraceabilityValue;
                        }
                        
                        data.DynamicColumnName = string.Format("Collection{0}", collectionCounter);
                        this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));

                        this.RegisterName(controlName, combo);

                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label
                        {
                            Content = description, 
                            Style = labelStyle,
                            Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                            Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                            FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                        });

                        stackPanelCalender.Children.Add(combo);
                        // stackPanelCalender.Children.Add(new TextBlock { Width = 10, IsHitTestVisible = false });
                        stackPanel.Children.Add(stackPanelCalender);

                        traceabilityData.Add(Tuple.Create(controlName, false, data));
                        this.dataCount++;
                        collectionCounter++;
                    }
                    else
                    {
                        // sql
                        var script = data.TraceabilityMasterSQL;
                        var commandData = this.repository.GetData(script).ToList().Select(x => x.ToString());

                        var controlName = string.Format("ComboBox{0}", this.dataCount).Replace(" ", "");
                        var combo = new ComboBox
                        {
                            Name = controlName, 
                            ItemsSource = commandData,
                            Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                            Width = ApplicationSettings.TouchscreenTraceabilityControlsWidth, 
                            Style = comboBoxStyle, 
                            FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize,
                            HorizontalAlignment = HorizontalAlignment.Center, 
                            VerticalAlignment = VerticalAlignment.Center, 
                            Margin = new Thickness(0,1,0,1)
                        };

                        combo.PreviewMouseUp += this.ComboOnPreviewMouseUp;

                        if (!string.IsNullOrEmpty(data.TraceabilityValue))
                        {
                            combo.Text = data.TraceabilityValue;
                        }
                        
                        data.DynamicColumnName = string.Format("Data{0}", dataCounter);
                        this.log.LogDebug(this.GetType(), string.Format("Registering control:{0}", controlName));

                        this.RegisterName(controlName, combo);

                        var stackPanelCalender = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0, 0, 5, 0) };
                        stackPanelCalender.Children.Add(new System.Windows.Controls.Label
                        {
                            Content = description, 
                            Style = labelStyle,
                            Width = ApplicationSettings.TouchscreenTraceabilityControlsLabelWidth,
                            Height = ApplicationSettings.TouchscreenTraceabilityControlsHeight,
                            FontSize = ApplicationSettings.TouchscreenTraceabilityControlsFontSize
                        });

                        stackPanelCalender.Children.Add(combo);
                        // stackPanelCalender.Children.Add(new TextBlock { Width = 10, IsHitTestVisible = false });
                        stackPanel.Children.Add(stackPanelCalender);

                        traceabilityData.Add(Tuple.Create(controlName, false, data));
                        this.dataCount++;
                        dataCounter++;
                    }

                    controlCount++;

                    if (controlCount == ApplicationSettings.TouchscreenTraceabilityControlsStackedNumber)
                    {
                        masterStackPanel.Children.Add(stackPanel);
                        stackPanel = new StackPanel { Orientation = Orientation.Vertical };
                        controlCount = 0;
                    }
                }

                if (controlCount > 0)
                {
                    masterStackPanel.Children.Add(stackPanel);
                }

                //this.RegisterName(masterName, masterStackPanel);
                this.TabItemSerialTraceability.Style = this.FindResource("StyleTabItemTouchscreen") as Style;
                this.TabItemSerialQuality.Style = this.FindResource("StyleTabItemTouchscreen") as Style;
                this.TabItemSerialDates.Style = this.FindResource("StyleTabItemTouchscreen") as Style;

                if (traceabilityMethod.Equals(Constant.Traceability))
                {
                    this.ContentControlSerialTraceability.Content = masterStackPanel;
                    this.TabItemSerialTraceability.Visibility = Visibility.Visible;
                    //this.TabItemSerialTraceability.Style = this.FindResource("StyleTabItemSerialWithValues") as Style;
                }
                else if (traceabilityMethod.Equals(Constant.Quality))
                {
                    this.ContentControlSerialQuality.Content = masterStackPanel;
                    this.TabItemSerialQuality.Visibility = Visibility.Visible;
                    //this.TabItemSerialQuality.Style = this.FindResource("StyleTabItemSerialWithValues") as Style;
                }
                else
                {
                    this.ContentControlSerialDates.Content = masterStackPanel;
                    this.TabItemSerialDates.Visibility = Visibility.Visible;
                    //this.TabItemSerialDates.Style = this.FindResource("StyleTabItemSerialWithValues") as Style;
                }
            }

            this.SetTabIndex();
            this.ignoreDateChanges = false;
        }

        #endregion

        #endregion

        #region switch batch traceability

        /// <summary>
        /// Transaction record change, associated with different batch number. 
        /// We re-display the associated batch traceability controls.
        /// </summary>
        /// <param name="batchNo">The batch number of the controls to display.</param>
        private void SwitchBatchTraceabilityData(string batchNo)
        {
            // find the associated registered stack panels, and display them.
            var stackPanelTraceabilityName = string.Format("StackPanelBatch{0}{1}", Constant.Traceability, batchNo);
            var stackPanelTraceability = this.FindStackPanel(stackPanelTraceabilityName) as StackPanel;
            if (stackPanelTraceability != null)
            {
                this.ContentControlTraceability.Content = stackPanelTraceability;
                this.TabItemBatchTraceability.Visibility = Visibility.Visible;
                //this.TabItemBatchTraceability.Style = this.FindResource("StyleTabItemTreaceabilityWithValues") as Style;
            }

            var stackPanelQualityName = string.Format("StackPanelBatch{0}{1}", Constant.Quality, batchNo);
            var stackPanelQuality = this.FindStackPanel(stackPanelQualityName) as StackPanel;
            if (stackPanelQuality != null)
            {
                this.ContentControlQuality.Content = stackPanelQuality;
                this.TabItemBatchQuality.Visibility = Visibility.Visible;
                //this.TabItemBatchQuality.Style = this.FindResource("StyleTabItemTreaceabilityWithValues") as Style;
            }

            var stackPanelDatesName = string.Format("StackPanelBatch{0}{1}", Constant.Date, batchNo);
            var stackPanelDates = this.FindStackPanel(stackPanelDatesName) as StackPanel;
            if (stackPanelDates != null)
            {
                this.ContentControlDates.Content = stackPanelDates;
                this.TabItemBatchDates.Visibility = Visibility.Visible;
                //this.TabItemBatchDates.Style = this.FindResource("StyleTabItemTreaceabilityWithValues") as Style;
            }
        }
        #endregion

        #region update batch

        /// <summary>
        /// New batch generated. Update the batch traceability controls.
        /// </summary>
        /// <param name="batchNo">The new batch number.</param>
        private void UpdateBatch(BatchNumber batchNo)
        {
            if (BatchTraceabilityData == null)
            {
                return;
            }

            this.log.LogDebug(this.GetType(), string.Format("UpdateBatch(): New batch selected Batch No:{0}. Updating.", batchNo.Number));
       
            // Add the newly created batch number to our traceability data.
            foreach (var data in BatchTraceabilityData)
            {
                data.BatchNumber = batchNo;
            }

            this.batchNo = batchNo.Number;
            //this.CreateBatchControls(BatchTraceabilityData, true);
        }

        #endregion

        #region reset serial data

        /// <summary>
        /// Resets the serial control value.
        /// </summary>
        private void ResetSerialData()
        {
            this.ignoreDateChanges = true;

            //foreach (var data in this.traceabilityData.Where(x => !x.Item2 && this.batchNo.Equals(x.Item3.BatchNumber.Number)))
            foreach (var data in traceabilityData.Where(x => !x.Item2))
            {
                var controlName = data.Item1;
                var control = this.FindName(controlName);
                if (control is TextBox)
                {
                    (control as TextBox).Text = string.Empty;
                    continue;
                }

                if (control is ComboBox)
                {
                    (control as ComboBox).Text = string.Empty;
                    continue;
                }

                if (control is DatePicker)
                {
                    (control as DatePicker).SelectedDate = null;
                }
            }

            this.ignoreDateChanges = false;
        }

        #endregion

        #region get traceability data

        /// <summary>
        /// Retrieves the batch traceability data.
        /// </summary>
        private void GetTraceabilityData()
        {
            this.log.LogDebug(this.GetType(), "GetTraceabilityData(): Retrieving data");
            var error = string.Empty;
            var batchTraceabilityResults = new List<TraceabilityResult>();

            foreach (var data in this.traceabilityData)
            {
                string value;
                var controlName = data.Item1;
                var batchAttribute = data.Item2;
                var localTraceabilityData = data.Item3;
                var required = data.Item3.Required.ToBool();

                this.log.LogDebug(this.GetType(), string.Format("Control Name:{0}", controlName));

                var control = this.FindName(controlName);
                if (control == null)
                {
                    continue;
                }

                var traceabilityMasterId = localTraceabilityData.TraceabilityMethod.Equals(Constant.Traceability)
                   ? localTraceabilityData.TraceabilityMasterID
                   : (int?)null;

                var dateMasterId = localTraceabilityData.TraceabilityMethod.Equals(Constant.Date)
                   ? localTraceabilityData.TraceabilityMasterID
                   : (int?)null;

                var qualityMasterId = localTraceabilityData.TraceabilityMethod.Equals(Constant.Quality)
                   ? localTraceabilityData.TraceabilityMasterID
                   : (int?)null;

                if (control is TextBox)
                {
                    value = (control as TextBox).Text;
                    if (string.IsNullOrWhiteSpace(value) && required)
                    {
                        batchTraceabilityResults.Clear();
                        error = "error";
                    }
                    
                    var result = new TraceabilityResult
                    {
                        FieldName = batchAttribute ? controlName : localTraceabilityData.DynamicColumnName,
                        NouTraceabilityMethod = localTraceabilityData.TraceabilityMethod,
                        TraceabilityMasterID = traceabilityMasterId,
                        DatemasterID = dateMasterId,
                        QualityMasterID = qualityMasterId,
                        TraceabilityValue = value,
                        BatchAttribute = batchAttribute,
                        BatchNo = this.batchNo
                    };

                    batchTraceabilityResults.Add(result);
                    this.log.LogDebug(this.GetType(), string.Format("Value found:{0}", value));
                    continue;
                }

                if (control is DatePicker)
                {
                    value = (control as DatePicker).SelectedDate.ToString();
                    if (string.IsNullOrWhiteSpace(value) && required)
                    {
                        batchTraceabilityResults.Clear();
                        error = "error";
                    }

                    var result = new TraceabilityResult
                    {
                        FieldName = batchAttribute ? controlName : localTraceabilityData.DynamicColumnName,
                        NouTraceabilityMethod = localTraceabilityData.TraceabilityMethod,
                        TraceabilityValue = value,
                        TraceabilityMasterID = traceabilityMasterId,
                        DatemasterID = dateMasterId,
                        QualityMasterID = qualityMasterId,
                        BatchAttribute = batchAttribute,
                        BatchNo = this.batchNo
                    };

                    batchTraceabilityResults.Add(result);
                    this.log.LogDebug(this.GetType(), string.Format("Value found:{0}", value));
                    continue;
                }

                if (control is ComboBox)
                {
                    value = (control as ComboBox).Text;
                    if (string.IsNullOrWhiteSpace(value) && required)
                    {
                        batchTraceabilityResults.Clear();
                        error = "error";
                    }

                    var result = new TraceabilityResult
                    {
                        FieldName = batchAttribute ? controlName : localTraceabilityData.DynamicColumnName,
                        NouTraceabilityMethod = localTraceabilityData.TraceabilityMethod,
                        TraceabilityValue = value,
                        TraceabilityMasterID = traceabilityMasterId,
                        DatemasterID = dateMasterId,
                        QualityMasterID = qualityMasterId,
                        BatchAttribute = batchAttribute,
                        BatchNo = this.batchNo
                    };

                    this.log.LogDebug(this.GetType(), string.Format("Value found:{0}", value));
                    batchTraceabilityResults.Add(result);
                }
            }

            this.log.LogDebug(this.GetType(), string.Format("Sending {0} traceability results", batchTraceabilityResults.Count));
            Messenger.Default.Send(Tuple.Create(error, batchTraceabilityResults), Token.TraceabilityValues);
        }

        /// <summary>
        /// Retieves the stored panel.
        /// </summary>
        /// <param name="name">The panel name to search for.</param>
        /// <returns>A stored stackpanel.</returns>
        private StackPanel FindStackPanel(string name)
        {
            return BatchContentControls.FirstOrDefault(x => x.Name.Equals(name));
        }

        /// <summary>
        /// Clears the attributes.
        /// </summary>
        private void ClearData()
        {
            
            this.log.LogDebug(this.GetType(), "ClearData(): Clearing the attributes..");
            this.traceabilityData.Clear();
            this.ContentControlTraceability.Content = null;
            this.ContentControlQuality.Content = null;
            this.ContentControlDates.Content = null;
            this.ContentControlSerialTraceability.Content = null;
            this.ContentControlSerialQuality.Content = null;
            this.ContentControlSerialDates.Content = null;
            this.log.LogDebug(this.GetType(), "Attributes cleared.");
        }

        /// <summary>
        /// Clears the attributes.
        /// </summary>
        private void ClearAndResetData()
        {
            this.log.LogDebug(this.GetType(), "ClearAndResetData(): Clearing the attributes..");
            this.traceabilityData.Clear();
            this.ContentControlTraceability.Content = null;
            this.ContentControlQuality.Content = null;
            this.ContentControlDates.Content = null;
            this.ContentControlSerialTraceability.Content = null;
            this.ContentControlSerialQuality.Content = null;
            this.ContentControlSerialDates.Content = null;
            this.log.LogDebug(this.GetType(), "Attributes cleared.");
            this.TabItemBatchTraceability.Visibility = Visibility.Visible;
            this.TabItemBatchQuality.Visibility = Visibility.Visible;
            this.TabItemBatchDates.Visibility = Visibility.Visible;
            this.TabItemSerialTraceability.Visibility = Visibility.Visible;
            this.TabItemSerialQuality.Visibility = Visibility.Visible;
            this.TabItemSerialDates.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Ensures that the first tab item is selected if the selected tab iindex is out of range.
        /// </summary>
        private void SetTabIndex()
        {
            var visibleList = new List<int>();
            if (this.TabItemBatchTraceability.Visibility == Visibility.Visible)
            {
                visibleList.Add(0);
            }

            if (this.TabItemBatchDates.Visibility == Visibility.Visible)
            {
                visibleList.Add(1);
            }
           
            if (this.TabItemBatchQuality.Visibility == Visibility.Visible)
            {
                visibleList.Add(2);
            }

            if (this.TabItemSerialTraceability.Visibility == Visibility.Visible)
            {
                visibleList.Add(3);
            }

            if (this.TabItemSerialDates.Visibility == Visibility.Visible)
            {
                visibleList.Add(4);
            }

            if (this.TabItemSerialQuality.Visibility == Visibility.Visible)
            {
                visibleList.Add(5);
            }

            if (visibleList.Any())
            {
                if ((selectedTab == 0 && this.TabItemBatchTraceability.Visibility != Visibility.Visible)
                  || (selectedTab == 1 && this.TabItemBatchQuality.Visibility != Visibility.Visible)
                    || (selectedTab == 2 && this.TabItemBatchDates.Visibility != Visibility.Visible)
                    || (selectedTab == 3 && this.TabItemSerialTraceability.Visibility != Visibility.Visible)
                    || (selectedTab == 4 && this.TabItemSerialQuality.Visibility != Visibility.Visible)
                    || (selectedTab == 5 && this.TabItemSerialDates.Visibility != Visibility.Visible)
                    || (selectedTab == null && this.TabControlTraceability.SelectedIndex == -1)
                    )
                {
                    this.TabControlTraceability.SelectedIndex = visibleList.First();
                }
            }
        }

        //private void HandleWeightError()
        //{
        //    var localBatchNo = string.Empty;
        //    foreach (var data in this.traceabilityData)
        //    {
        //        var isBatchAttribute = data.Item2;
        //        var traceData = data.Item3;
        //        localBatchNo = traceData.BatchNumber.Number;
        //        if (isBatchAttribute)
        //        {
        //            if (string.IsNullOrEmpty(traceData.TraceabilityValue))
        //            {
        //                this.log.LogDebug(this.GetType(), string.Format("Empty batch attributes found. Unregistering the stack panels and removing the batch no:{0}", localBatchNo));

        //                var stackPanelTraceabilityName = string.Format("StackPanelBatch{0}{1}", Constant.Traceability, localBatchNo);
        //                var stackPanelQualityName = string.Format("StackPanelBatch{0}{1}", Constant.Quality, localBatchNo);
        //                var stackPanelDatesName = string.Format("StackPanelBatch{0}{1}", Constant.Date, localBatchNo);

        //                if (this.FindName(stackPanelTraceabilityName) != null)
        //                {
        //                    this.UnregisterName(stackPanelTraceabilityName);
        //                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelTraceabilityName));
        //                    if (control != null)
        //                    {
        //                        BatchContentControls.Remove(control);
        //                    }
        //                }

        //                if (this.FindName(stackPanelQualityName) != null)
        //                {
        //                    this.UnregisterName(stackPanelQualityName);
        //                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelQualityName));
        //                    if (control != null)
        //                    {
        //                        BatchContentControls.Remove(control);
        //                    }
        //                }

        //                if (this.FindName(stackPanelDatesName) != null)
        //                {
        //                    this.UnregisterName(stackPanelDatesName);
        //                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelDatesName));
        //                    if (control != null)
        //                    {
        //                        BatchContentControls.Remove(control);
        //                    }
        //                }

        //                BatchNumbers.Remove(localBatchNo);
        //                break;
        //            }
        //        }
        //    }

        //    if (!BatchNumbers.Contains(localBatchNo))
        //    {
        //        // clear everything
        //        this.traceabilityData.Clear();
        //        this.ContentControlTraceability.Content = null;
        //        this.ContentControlQuality.Content = null;
        //        this.ContentControlDates.Content = null;
        //    }
        //    else
        //    {
        //        // clear only the serial attributes.
        //        var localbatchAttributes = this.traceabilityData.Where(x => x.Item2);
        //        this.traceabilityData = localbatchAttributes.ToList();
        //    }
        //}

        /// <summary>
        /// Determines if all the batch traceability values have been set.
        /// </summary>
        /// <param name="localBatchNo">The batch traceability data.</param>
        /// <returns>Flag, as to whether the batch traceability values have been set.</returns>
        private bool BatchTraceabilityValuesSet(string localBatchNo)
        {
            if (!NouvemGlobal.CheckBatchValuesSet)
            {
                return true;
            }

            if (!NouvemGlobal.BatchValuesSet.ContainsKey(localBatchNo))
            {
                return false;
            }

            var areValuesSet = NouvemGlobal.BatchValuesSet[localBatchNo];
            if (!areValuesSet)
            {
                var stackPanelTraceabilityName = string.Format("StackPanelBatch{0}{1}", Constant.Traceability, localBatchNo);
                var stackPanelQualityName = string.Format("StackPanelBatch{0}{1}", Constant.Quality, localBatchNo);
                var stackPanelDatesName = string.Format("StackPanelBatch{0}{1}", Constant.Date, localBatchNo);

                if (this.FindName(stackPanelTraceabilityName) != null)
                {
                    this.UnregisterName(stackPanelTraceabilityName);
                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelTraceabilityName));
                    if (control != null)
                    {
                        BatchContentControls.Remove(control);
                    }
                }

                if (this.FindName(stackPanelQualityName) != null)
                {
                    this.UnregisterName(stackPanelQualityName);
                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelQualityName));
                    if (control != null)
                    {
                        BatchContentControls.Remove(control);
                    }
                }

                if (this.FindName(stackPanelDatesName) != null)
                {
                    this.UnregisterName(stackPanelDatesName);
                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelDatesName));
                    if (control != null)
                    {
                        BatchContentControls.Remove(control);
                    }
                }

                if (this.FindStackPanel(stackPanelTraceabilityName) != null)
                {
                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelTraceabilityName));
                    if (control != null)
                    {
                        BatchContentControls.Remove(control);
                    }
                }

                if (this.FindStackPanel(stackPanelQualityName) != null)
                {
                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelQualityName));
                    if (control != null)
                    {
                        BatchContentControls.Remove(control);
                    }
                }

                if (this.FindStackPanel(stackPanelDatesName) != null)
                {
                    var control = BatchContentControls.FirstOrDefault(x => x.Name.Equals(stackPanelDatesName));
                    if (control != null)
                    {
                        BatchContentControls.Remove(control);
                    }
                }

                BatchNumbers.Remove(localBatchNo);
                return false;
            }
          
            return true;
        }

        #endregion

        #region event handlers

        void TextBox_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is TextBox)
            {
                selectedTextBox = (sender as TextBox);
                Messenger.Default.Send(ViewType.Keyboard);
            }
        }

        void DatePicker_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is DatePicker)
            {
                (sender as DatePicker).IsDropDownOpen = true;
            }
        }

        private bool setControl = true;
        private bool ignoreDateChanges = true;
  
        void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            if (!(sender is DatePicker) || this.ignoreDateChanges)
            {
                return;
            }

            try
            {
                if (this.setControl)
                {
                    var dateMasterBasedOnId = (int)(sender as DatePicker).Tag;
                    var dateMasterBasedOnDate = sender.ToString().ToDate();

                    foreach (var item in this.traceabilityData)
                    {
                        var controlName = item.Item1;
                        var data = item.Item3;
                        if (data.DateMasterBasedOn != null && data.DateMasterBasedOn.DateMasterID == dateMasterBasedOnId && !data.ScanningAtDispatch)
                        {
                            if (data.DateDays != null && data.DateDays.Any())
                            {
                                var dateType = NouvemGlobal.DateTypes.FirstOrDefault(x => x.NouDateTypeID == data.TraceabilityTypeID);

                                var daysForward =
                                    data.DateDays.FirstOrDefault(x => x.INMasterID == TraceabilityData.INMasterId && x.DateMasterID == data.TraceabilityMasterID);

                                if (daysForward == null)
                                {
                                    break;
                                }

                                if (dateType != null)
                                {
                                    if (dateType.DateType == DateType.ForwardDate)
                                    {
                                        var controlToUpdate = this.FindName(controlName);
                                        if (controlToUpdate != null && controlToUpdate is DatePicker)
                                        {
                                            if (dateMasterBasedOnDate > "01/01/01".ToDate())
                                            {
                                                this.setControl = false;
                                                (controlToUpdate as DatePicker).SelectedDate = dateMasterBasedOnDate.Add(TimeSpan.FromDays(daysForward.Days.ToInt())); 
                                            }
                                        }
                                    }
                                    else if (dateType.DateType == DateType.BackDate)
                                    {
                                        var controlToUpdate = this.FindName(controlName);
                                        if (controlToUpdate != null && controlToUpdate is DatePicker)
                                        {
                                            if (dateMasterBasedOnDate > "01/01/01".ToDate())
                                            {
                                                this.setControl = false;
                                                (controlToUpdate as DatePicker).SelectedDate = dateMasterBasedOnDate.Add(TimeSpan.FromDays(-daysForward.Days.ToInt())); 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                this.setControl = true;
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("DatePicker_SelectedDateChanged:{0}", ex.Message));
                this.setControl = true;
            }
        }

        private void ComboOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            var items = new List<object>();
            if (sender is ComboBox)
            {
                var localCombo = (sender as ComboBox);
                localCombo.IsDropDownOpen = false;
                selectedComboBox = localCombo;

                foreach (var item in localCombo.Items)
                {
                    items.Add(item);
                }

                // Send our combo collection to the display module.
                Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.CollectionDisplay);
            }
        }

        #endregion

        #region misc

        /// <summary>
        /// Gets the module that called the menu.
        /// </summary>
        /// <returns>The calling module.</returns>
        private ViewType CallingModule()
        {
            if (!ViewModelLocator.IsAPReceiptTouchscreenNull())
            {
                return ViewType.APReceipt;
            }

            if (!ViewModelLocator.IsIntoProductionNull())
            {
                return ViewType.IntoProduction;
            }

            if (!ViewModelLocator.IsOutOfProductionNull())
            {
                return ViewType.OutOfProduction;
            }

            return ViewType.ARDispatch;
        }

        /// <summary>
        /// Resets the serial control value.
        /// </summary>
        private void SetSerialData(Tuple<List<BarcodeParse>, string> scannedata)
        {
            var localData = scannedata.Item1;
            var scannerValue = scannedata.Item2;
            try
            {
                foreach (var barcodeParse in localData)
                {
                    this.log.LogDebug(this.GetType(), string.Format("SetSerialData():{0},{1},{2},{3},{4}", barcodeParse.BPMasterID, barcodeParse.INMasterID, barcodeParse.Attribute, barcodeParse.StartPos, barcodeParse.EndPos));
                    if (barcodeParse.Attribute.CompareIgnoringCase("KillNumber"))
                    {
                        var startPos = barcodeParse.StartPos;
                        var endPos = barcodeParse.EndPos;

                        var localTrace =
                            this.traceabilityData.FirstOrDefault(
                                x => !x.Item2 && x.Item3.TraceabilityMasterCode.CompareIgnoringCase("KillNumber"));

                        if (localTrace != null)
                        {
                            var controlName = localTrace.Item1;
                            var control = this.FindName(controlName);
                            if (control is TextBox)
                            {
                                (control as TextBox).Text = scannerValue.Substring(startPos - 1, endPos);
                            }
                        }

                        continue;
                    }

                    if (barcodeParse.Attribute.CompareIgnoringCase("Grade"))
                    {
                        var startPos = barcodeParse.StartPos;
                        var endPos = barcodeParse.EndPos;

                        var localTrace =
                            this.traceabilityData.FirstOrDefault(
                                x => !x.Item2 && x.Item3.TraceabilityMasterCode.CompareIgnoringCase("Grade"));

                        if (localTrace != null)
                        {
                            var controlName = localTrace.Item1;
                            var control = this.FindName(controlName);
                            if (control is TextBox)
                            {
                                (control as TextBox).Text = scannerValue.Substring(startPos - 1, endPos);
                            }
                        }

                        continue;
                    }

                    if (barcodeParse.Attribute.CompareIgnoringCase("SupplierBatch"))
                    {
                        var startPos = barcodeParse.StartPos;
                        var endPos = barcodeParse.EndPos;

                        var localTrace =
                            this.traceabilityData.FirstOrDefault(
                                x => !x.Item2 && x.Item3.TraceabilityMasterCode.CompareIgnoringCase("SupplierBatch"));

                        if (localTrace != null)
                        {
                            var controlName = localTrace.Item1;
                            var control = this.FindName(controlName);
                            if (control is TextBox)
                            {
                                (control as TextBox).Text = scannerValue.Substring(startPos - 1, endPos);
                            }
                        }

                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
            }
        }

        #endregion
    }

    #endregion
}
