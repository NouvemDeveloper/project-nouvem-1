﻿// -----------------------------------------------------------------------
// <copyright file="APReceiptContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.Xpf.Editors;

namespace Nouvem.View.Purchases.APReceipt
{
    using System;
    using System.Windows;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Controls;
    using DevExpress.Utils;
    using DevExpress.Xpf.Editors.Settings;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Logging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Model.DataLayer.Repository;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for APReceiptDetailsContainerView.xaml
    /// </summary>
    public partial class APReceiptDetailsContainerView : Window
    {
        #region constructor

        public APReceiptDetailsContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<bool>(this, Token.CloseAPReceiptDetailsWindow, b =>
            {
                this.Close();
            });

            Messenger.Default.Register<CollectionData>(this, Token.Macro, this.HandleMacro);
            Messenger.Default.Register<bool>(this, Token.ShowGridProduct, b =>
            {
                if (b)
                {
                    this.GridColumnProduct.Visible = true;
                    this.GridColumnProduct.Width = 170;
                }
                else
                {
                    this.GridColumnProduct.Visible = false;
                    this.GridColumnProduct.Width = 0;
                }
            });

            this.GridColumnProduct.Visible = false;
            this.GridColumnProduct.Width = 0;

            this.WindowState = WindowSettings.IntakeDetailsMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.IntakeDetailsHeight = this.Height;
                WindowSettings.IntakeDetailsLeft = this.Left;
                WindowSettings.IntakeDetailsTop = this.Top;
                WindowSettings.IntakeDetailsWidth = this.Width;
                WindowSettings.IntakeDetailsMaximised = this.WindowState.ToBool();
                Messenger.Default.Unregister(this);
            };
        }

        #endregion

        private void HandleMacro(CollectionData data)
        {
            this.HideColumns();
            if (!data.RowData.Any())
            {
                return;
            }

            foreach (var localData in data.RowData)
            {

                if (localData.Item1.CompareIgnoringCase("Attribute1")) { this.GridColumn1.Header = localData.Item2; this.GridColumn1.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute2")) { this.GridColumn2.Header = localData.Item2; this.GridColumn2.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute3")) { this.GridColumn3.Header = localData.Item2; this.GridColumn3.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute4")) { this.GridColumn4.Header = localData.Item2; this.GridColumn4.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute5")) { this.GridColumn5.Header = localData.Item2; this.GridColumn5.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute6")) { this.GridColumn6.Header = localData.Item2; this.GridColumn6.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute7")) { this.GridColumn7.Header = localData.Item2; this.GridColumn7.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute8")) { this.GridColumn8.Header = localData.Item2; this.GridColumn8.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute9")) { this.GridColumn9.Header = localData.Item2; this.GridColumn9.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute10")) { this.GridColumn10.Header = localData.Item2; this.GridColumn10.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute11")) { this.GridColumn11.Header = localData.Item2; this.GridColumn11.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute12")) { this.GridColumn12.Header = localData.Item2; this.GridColumn12.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute13")) { this.GridColumn13.Header = localData.Item2; this.GridColumn13.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute14")) { this.GridColumn14.Header = localData.Item2; this.GridColumn14.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute15")) { this.GridColumn15.Header = localData.Item2; this.GridColumn15.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute16")) { this.GridColumn16.Header = localData.Item2; this.GridColumn16.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute17")) { this.GridColumn17.Header = localData.Item2; this.GridColumn17.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute18")) { this.GridColumn18.Header = localData.Item2; this.GridColumn18.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute19")) { this.GridColumn19.Header = localData.Item2; this.GridColumn19.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute20")) { this.GridColumn20.Header = localData.Item2; this.GridColumn20.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute21")) { this.GridColumn21.Header = localData.Item2; this.GridColumn21.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute22")) { this.GridColumn22.Header = localData.Item2; this.GridColumn22.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute23")) { this.GridColumn23.Header = localData.Item2; this.GridColumn23.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute24")) { this.GridColumn24.Header = localData.Item2; this.GridColumn24.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute25")) { this.GridColumn25.Header = localData.Item2; this.GridColumn25.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute26")) { this.GridColumn26.Header = localData.Item2; this.GridColumn26.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute27")) { this.GridColumn27.Header = localData.Item2; this.GridColumn27.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute28")) { this.GridColumn28.Header = localData.Item2; this.GridColumn28.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute29")) { this.GridColumn29.Header = localData.Item2; this.GridColumn29.Visible = true; }
                if (localData.Item1.CompareIgnoringCase("Attribute30")) { this.GridColumn30.Header = localData.Item2; this.GridColumn30.Visible = true; }
            }
        }

        private void HideColumns()
        {
            this.GridColumn1.Visible = false;
            this.GridColumn2.Visible = false;
            this.GridColumn3.Visible = false;
            this.GridColumn4.Visible = false;
            this.GridColumn5.Visible = false;
            this.GridColumn6.Visible = false;
            this.GridColumn7.Visible = false;
            this.GridColumn8.Visible = false;
            this.GridColumn9.Visible = false;
            this.GridColumn10.Visible = false;
            this.GridColumn11.Visible = false;
            this.GridColumn12.Visible = false;
            this.GridColumn13.Visible = false;
            this.GridColumn14.Visible = false;
            this.GridColumn15.Visible = false;
            this.GridColumn16.Visible = false;
            this.GridColumn17.Visible = false;
            this.GridColumn18.Visible = false;
            this.GridColumn19.Visible = false;
            this.GridColumn20.Visible = false;
            this.GridColumn21.Visible = false;
            this.GridColumn22.Visible = false;
            this.GridColumn23.Visible = false;
            this.GridColumn24.Visible = false;
            this.GridColumn25.Visible = false;
            this.GridColumn26.Visible = false;
            this.GridColumn27.Visible = false;
            this.GridColumn28.Visible = false;
            this.GridColumn29.Visible = false;
            this.GridColumn30.Visible = false;
        }
    }
}
