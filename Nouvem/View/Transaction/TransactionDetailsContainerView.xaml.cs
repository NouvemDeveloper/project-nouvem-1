﻿// -----------------------------------------------------------------------
// <copyright file="TransactionDetailsContainerView" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Transaction
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for TransactionDetailsContainerView.xaml
    /// </summary>
    public partial class TransactionDetailsContainerView : Window
    {
        public TransactionDetailsContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseTransactionDetailsWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}

