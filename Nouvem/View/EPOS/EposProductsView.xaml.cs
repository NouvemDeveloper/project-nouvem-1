﻿// -----------------------------------------------------------------------
// <copyright file="EposProductsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for EposProductsView.xaml
    /// </summary>
    public partial class EposProductsView : UserControl
    {
        /// <summary>
        /// The listview scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public EposProductsView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.EposScroll, s =>
            {
                if (ScrollViewer == null)
                {
                    ScrollViewer = GetScrollViewer(this.ListViewProducts) as ScrollViewer;
                }

                if (ScrollViewer == null)
                {
                    return;
                }

                if (s.Equals(Constant.Up))
                {
                    // scroll up
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - 100);
                    return;
                }

                // scoll down
                ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + 100);
            });
        }

        private void ItemOnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = true;
        }

        private void ItemOnPreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ((ListBoxItem)sender).IsSelected = false;
        }

        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }}
}
