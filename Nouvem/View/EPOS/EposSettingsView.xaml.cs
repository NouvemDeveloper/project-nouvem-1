﻿// -----------------------------------------------------------------------
// <copyright file="EposSettingsView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for EposSettingsView.xaml
    /// </summary>
    public partial class EposSettingsView : UserControl
    {
        public EposSettingsView()
        {
            this.InitializeComponent();
        }
    }
}
