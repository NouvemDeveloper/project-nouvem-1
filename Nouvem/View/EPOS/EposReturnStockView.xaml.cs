﻿// -----------------------------------------------------------------------
// <copyright file="EposReturnStockView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for EposReturnStockView.xaml
    /// </summary>
    public partial class EposReturnStockView : UserControl
    {
        public EposReturnStockView()
        {
            this.InitializeComponent();
        }
    }
}
