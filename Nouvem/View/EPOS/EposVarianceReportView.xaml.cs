﻿// -----------------------------------------------------------------------
// <copyright file="EposVarianceReportView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using Nouvem.Properties;

namespace Nouvem.View.EPOS
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for EposVarianceReportView.xaml
    /// </summary>
    public partial class EposVarianceReportView : UserControl
    {
        public EposVarianceReportView()
        {
            this.InitializeComponent();
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-IE");

            this.Unloaded += (sender, args) =>
            {
                Settings.Default.Save();
                this.GridControlVariance.SaveLayoutToXml(Settings.Default.EposVarianceDataGrid);
            };

            this.Loaded += (sender, args) =>
            {
                var log = new Logging.Logger();
                log.LogInfo(this.GetType(), "EposVarianceReportView(): Loading");
                if (File.Exists(Settings.Default.EposVarianceDataGrid))
                {
                    this.GridControlVariance.RestoreLayoutFromXml(Settings.Default.EposVarianceDataGrid);
                }
            };
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.TableViewProducts.ShowPrintPreview(this);
        }
    }
}
