﻿// -----------------------------------------------------------------------
// <copyright file="EPOSSettingsContainerView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for EPOSSettingsContainerView.xaml
    /// </summary>
    public partial class EPOSSettingsContainerView : Window
    {
        public EPOSSettingsContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseEposSettingsWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
