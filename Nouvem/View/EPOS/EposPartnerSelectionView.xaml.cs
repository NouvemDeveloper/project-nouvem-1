﻿// -----------------------------------------------------------------------
// <copyright file="EposPartnerSelectionView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Media;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for EposPartnerSelectionView.xaml
    /// </summary>
    public partial class EposPartnerSelectionView : UserControl
    {
        /// <summary>
        /// The grid control scrollviewer.
        /// </summary>
        private static ScrollViewer ScrollViewer;

        public EposPartnerSelectionView()
        {
            this.InitializeComponent();
            //this.Keyboard.IsOpen = true;
            this.Unloaded += (sender, args) =>
            {
                Settings.Default.Save();
                this.GridControlPartners.View.SearchString = string.Empty;
                this.GridControlPartners.SaveLayoutToXml(Settings.Default.EposPartnersGridPath);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.EposPartnersGridPath))
                {
                    this.GridControlPartners.RestoreLayoutFromXml(Settings.Default.EposPartnersGridPath);
                }
            };

            Messenger.Default.Register<bool>(this, Token.DisplayKeyboard, b =>
            {
               // this.Keyboard.IsOpen = b;
                this.GridControlPartners.View.ShowSearchPanel(b);
            });

            Messenger.Default.Register<string>(this, Token.NavigationCommand, s => this.ButtonNavigate.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent)));

            Messenger.Default.Register<string>(this, Token.EposPartnerScroll, s =>
            {
                if (ScrollViewer == null)
                {
                    ScrollViewer = GetScrollViewer(this.GridControlPartners) as ScrollViewer;
                }

                if (ScrollViewer == null)
                {
                    return;
                }

                if (s.Equals(Constant.Up))
                {
                    // scroll up
                    ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset - 100);
                    return;
                }

                // scoll down
                ScrollViewer.ScrollToVerticalOffset(ScrollViewer.VerticalOffset + 100);
            });
        }

        public static DependencyObject GetScrollViewer(DependencyObject o)
        {
            // Return the DependencyObject if it is a ScrollViewer
            if (o is ScrollViewer)
            {
                return o;
            }

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(o); i++)
            {
                var child = VisualTreeHelper.GetChild(o, i);

                var result = GetScrollViewer(child);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
       }
    }
}
