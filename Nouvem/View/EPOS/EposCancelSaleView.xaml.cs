﻿// -----------------------------------------------------------------------
// <copyright file="EposCancelSaleView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.EPOS
{
    using System.Windows.Controls;using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for EposCancelSaleView.xaml
    /// </summary>
    public partial class EposCancelSaleView : UserControl
    {
        public EposCancelSaleView()
        {
            this.InitializeComponent();
            //Messenger.Default.Register<bool>(this, Token.DisplayCalender, b =>
            //{
            //    this.Keyboard.IsOpen = false;
            //    this.DatePickerSaleDate.IsDropDownOpen = b;
            //});
           
            //Messenger.Default.Register<bool>(this, Token.DisplayKeyboard, b =>
            //{
            //    this.Keyboard.IsOpen = b;//    if (b)
            //    {
            //        this.TextBoxInvoiceNo.Focus();
            //        this.DatePickerSaleDate.IsDropDownOpen = false;
            //        //Messenger.Default.Send(Token.Message, Token.CalenderClosed);
            //    }
            //});

            this.Loaded += (sender, args) => this.TextBoxInvoiceNo.Focus();
        }
    }
}
