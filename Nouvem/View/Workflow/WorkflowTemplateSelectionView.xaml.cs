﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowTemplateSelectionView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Workflow
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for WorkflowTemplateSelectionView.xaml
    /// </summary>
    public partial class WorkflowTemplateSelectionView : Window
    {
        public WorkflowTemplateSelectionView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWorkflowSelection, s => this.Close());
            this.Topmost = true;
            this.Closing += (sender, args) => Messenger.Default.Unregister(this);
        }
    }
}
