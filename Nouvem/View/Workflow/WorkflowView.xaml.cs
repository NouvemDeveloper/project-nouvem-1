﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows.Threading;
using Nouvem.BusinessLogic;
using Nouvem.Properties;
using Nouvem.ViewModel;

namespace Nouvem.View.Workflow
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for WorkflowView.xaml
    /// </summary>
    public partial class WorkflowView : Window
    {
        private DispatcherTimer applicationIdleTimer;

        public WorkflowView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseWorkflow, s => this.Close());
#if !DEBUG
            this.Topmost = true;
#endif

            this.Closing += (sender, args) =>
            {
                if (ApplicationSettings.LogOutOnWorkflowIdle)
                {
                    this.applicationIdleTimer.Tick -= this.ApplicationIdleTimerOnTick;
                }

                Messenger.Default.Unregister(this);
            };

            if (ApplicationSettings.LogOutOnWorkflowIdle)
            {
                Messenger.Default.Register<string>(this, Token.Scanner, s => this.Reset());
                this.applicationIdleTimer = new DispatcherTimer();
                this.applicationIdleTimer.Interval = TimeSpan.FromMinutes(ApplicationSettings.LogOutOnWorkflowTime);
                this.applicationIdleTimer.Start();
                this.applicationIdleTimer.Tick += this.ApplicationIdleTimerOnTick;
            }
        }

        private void ApplicationIdleTimerOnTick(object o, EventArgs eventArgs)
        {
            this.LogOut();
        }

        private void Window_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.Reset();
        }

        /// <summary>
        /// Resets the timer.
        /// </summary>
        private void Reset()
        {
            if (ApplicationSettings.LogOutOnWorkflowIdle)
            {
                this.applicationIdleTimer.Stop();
                this.applicationIdleTimer.Start();
            }
        }

        private void LogOut()
        {
            Settings.Default.AllowMultipleInstancesOfApplication = true;
            Settings.Default.Save();
            //ViewModelLocator.CleanUpTouchscreens();
            var data = DataManager.Instance;
            data.LogOut();
            //this.DataManager.SaveDeviceSettings();
            System.Windows.Forms.Application.Restart();
            Application.Current.Shutdown();
            Process.GetCurrentProcess().Kill();
        }
    }
}


