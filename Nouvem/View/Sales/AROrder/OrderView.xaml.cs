﻿// -----------------------------------------------------------------------
// <copyright file="OrderView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Sales.AROrder
{
    using System.Globalization;
    using System.Threading;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for OrderView.xaml
    /// </summary>
    public partial class OrderView : UserControl
    {
        public OrderView()
        {
            this.InitializeComponent();
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-IE");
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-IE");
            ThreadPool.QueueUserWorkItem(x => this.ComboBoxEditCode.Dispatcher.Invoke(() => this.ComboBoxEditCode.Focus()));
            //Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.ComboBoxEditCode.Focus());
        }

        private void TextBox_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            Messenger.Default.Send(Token.Message, Token.FocusToGrid);
        }
    }
}
