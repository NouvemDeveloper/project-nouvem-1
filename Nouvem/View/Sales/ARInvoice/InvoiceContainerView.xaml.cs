﻿// -----------------------------------------------------------------------
// <copyright file="InvoiceContainerView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.Sales.ARInvoice
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for InvoiceContainerView.xaml
    /// </summary>
    public partial class InvoiceContainerView : Window
    {
        public InvoiceContainerView()
        {
            this.InitializeComponent();

            this.WindowState = WindowSettings.InvoiceMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Register<string>(this, Token.CloseInvoiceWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.SetTopMost, s =>
            {
                this.Topmost = true;
                this.Topmost = false;
            });

            this.Closing += (sender, args) =>
            {
                WindowSettings.InvoiceHeight = this.Height;
                WindowSettings.InvoiceLeft = this.Left;
                WindowSettings.InvoiceTop = this.Top;
                WindowSettings.InvoiceWidth = this.Width;
                WindowSettings.InvoiceMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
