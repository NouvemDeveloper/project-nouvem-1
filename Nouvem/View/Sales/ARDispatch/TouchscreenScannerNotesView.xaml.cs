﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenNotesView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Globalization;
using System.Threading;
using System.Windows.Threading;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.ViewModel;

namespace Nouvem.View.Sales.ARDispatch
{
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for TouchscreenNotesView.xaml
    /// </summary>
    public partial class TouchscreenScannerNotesView : Window
    {
        public TouchscreenScannerNotesView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseHiddenWindows, s => this.Close());
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-IE");
            Messenger.Default.Register<CollectionData>(this, Token.SetDataContext, d =>
            {
                this.DataContext = d.DataContext;
            });
            //this.Topmost = true;

            Messenger.Default.Register<bool>(this, Token.CloseNotesWindow, b =>
            {
                if (b)
                {
                    this.Visibility = Visibility.Collapsed;
                    //this.Topmost = false;
                    if (this.Keyboard != null)
                    {
                        this.Keyboard.IsOpen = false;
                    }
                }
                else
                {
                    this.Visibility = Visibility.Visible;
                    //this.Topmost = true;

                    if (ApplicationSettings.Customer == Customer.Ballon)
                    {
                        this.TabControlMain.SelectedIndex = 2;
                    }
                    else
                    {
                        this.TabControlMain.SelectedIndex = 0;
                        if (this.TextBoxDocket != null)
                        {
                            this.TextBoxDocket.Focus();
                        }
                    }
                }
            });

            Messenger.Default.Register<bool>(this, Token.DisplayNotesKeyboard, b =>
            {
                if (this.Keyboard != null)
                {
                    this.Keyboard.IsOpen = b;
                }
            });

            Messenger.Default.Register<bool>(this, Token.DisplayNotesContainerKeyboard, b =>
            {
                if (this.Keyboard != null)
                {
                    this.Keyboard.IsOpen = true;
                }
            });

            this.Closing += (sender, args) => Messenger.Default.Unregister(this);
        }

        private void TabControlMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.TabControlMain.SelectedIndex == 0)
            {
                if (this.ButtonKeyboard != null)
                {
                    this.ButtonKeyboard.Visibility = Visibility.Visible;
                }

                if (this.Keyboard != null)
                {
                    this.Keyboard.IsOpen = true;
                }

                if (this.TextBoxDocket != null)
                {
                    this.TextBoxDocket.Focus();
                }
            }
            else if (this.TabControlMain.SelectedIndex == 1)
            {
                if (this.ButtonKeyboard != null)
                {
                    this.ButtonKeyboard.Visibility = Visibility.Visible;
                }

                if (this.Keyboard != null)
                {
                    this.Keyboard.IsOpen = true;
                }

                if (this.TextBoxPORef != null)
                {
                    this.TextBoxPORef.Focus();
                }
            }
            else
            {
                if (this.ButtonKeyboard != null)
                {
                    this.ButtonKeyboard.Visibility = Visibility.Collapsed;
                }

                if (this.Keyboard != null)
                {
                    this.Keyboard.IsOpen = false;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.Keyboard != null)
            {
                this.Keyboard.IsOpen = true;
            }
        }
    }
}


