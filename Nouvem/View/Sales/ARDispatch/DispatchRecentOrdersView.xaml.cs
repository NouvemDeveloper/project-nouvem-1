﻿// -----------------------------------------------------------------------
// <copyright file="DispatchRecentOrdersView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Sales.ARDispatch
{
    using System;
    using Nouvem.Global;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for SalesRecentOrdersView.xaml
    /// </summary>
    public partial class DispatchRecentOrdersView : UserControl
    {
        public DispatchRecentOrdersView()
        {
            this.InitializeComponent();

            this.GridControlRecentOrders.PreviewMouseDown += this.Grid_PreviewMouseDown;
            this.TableViewData.MouseLeftButtonUp += this.TableView_MouseLeftButtonUp;
            this.ShowAllRecentOrders(false);

            Messenger.Default.Register<string>(this, Token.SetRecentOrdersFocus, s => this.SetFocus());
            Messenger.Default.Register<bool>(this, Token.ShowAllRecentOrders, this.ShowAllRecentOrders);
            var gridPath = Settings.Default.DispatchRecentOrdersGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlRecentOrders.RestoreLayoutFromXml(gridPath);
                }

                this.SetFocus();
                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Unloaded += (sender, args) =>
            {
                this.GridControlRecentOrders.SaveLayoutToXml(gridPath);
                //Messenger.Default.Unregister(this);
            };
        }

        private void SetFocus()
        {
            if (this.GridControlRecentOrders.VisibleRowCount == 0)
            {
                return;
            }

            var rowHandle = 0;

            if (this.GridControlRecentOrders.IsValidRowHandle(rowHandle))
            {
                this.GridControlRecentOrders.CurrentColumn = this.GridControlRecentOrders.Columns["Ordered"];
                this.TableViewData.FocusedRowHandle = rowHandle;
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    this.GridControlRecentOrders.Focus();
                    this.TableViewData.ShowEditor();
                }), System.Windows.Threading.DispatcherPriority.Render);
            }
        }

        private void Grid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var hitInfo = ((TableView)this.GridControlRecentOrders.View).CalcHitInfo(e.OriginalSource as DependencyObject);
                if (hitInfo == null)
                {
                    return;
                }

                var currentColumn = (hitInfo).Column;
                var currentColumnHeader = currentColumn.ActualColumnChooserHeaderCaption.ToString().Trim();
                if (currentColumnHeader.Equals("Product") || currentColumnHeader.Equals("Ordered") || currentColumnHeader.Equals(string.Empty))
                {
                    return;
                }

                if (hitInfo.HitTest == TableViewHitTest.ColumnHeader || hitInfo.HitTest == TableViewHitTest.ColumnHeaderFilterButton)
                {
                    this.GridControlRecentOrders.UnselectAll();

                    if (this.GridControlRecentOrders.View is TableView)
                    {
                        (this.GridControlRecentOrders.View as TableView)
                       .SelectCells(0, currentColumn, this.GridControlRecentOrders.VisibleRowCount - 1, currentColumn);

                        // Let the vm know that a previous order has been selected.
                        Messenger.Default.Send(currentColumnHeader, Token.RecentOrderItemSelected);
                    }
                }
            }
            catch
            {
            }
        }


        private void TableView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var tableView = sender as TableView;
            var column = this.GridControlRecentOrders.CurrentColumn.ActualColumnChooserHeaderCaption;

            if (column.Equals("Product") || column.Equals("Ordered") || column.Equals(string.Empty))
            {
                return;
            }

            if (tableView != null)
            {
                try
                {
                    var hitInfo = tableView.CalcHitInfo(e.OriginalSource as DependencyObject);
                    if (hitInfo.InRowCell)
                    {
                        var selectedCellValue = tableView.Grid.GetCellValue(hitInfo.RowHandle, hitInfo.Column).ToString();

                        // Let the vm know that a previous order item has been selected.
                        Messenger.Default.Send(selectedCellValue, Token.RecentOrderItemSelected);
                    }
                }
                catch
                {
                }
            }

            
        }

        #region show recent orders

        private void ShowAllRecentOrders(bool show)
        {
            this.TableViewData.AutoWidth = !show;

            this.GridColumnOrder21.Visible = show;
            this.GridColumnOrder22.Visible = show;
            this.GridColumnOrder23.Visible = show;
            this.GridColumnOrder24.Visible = show;
            this.GridColumnOrder25.Visible = show;
            this.GridColumnOrder26.Visible = show;
            this.GridColumnOrder27.Visible = show;
            this.GridColumnOrder28.Visible = show;
            this.GridColumnOrder29.Visible = show;
            this.GridColumnOrder30.Visible = show;
            this.GridColumnOrder31.Visible = show;
            this.GridColumnOrder32.Visible = show;
            this.GridColumnOrder33.Visible = show;
            this.GridColumnOrder34.Visible = show;
            this.GridColumnOrder35.Visible = show;
            this.GridColumnOrder36.Visible = show;
            this.GridColumnOrder37.Visible = show;
            this.GridColumnOrder38.Visible = show;
            this.GridColumnOrder39.Visible = show;
            this.GridColumnOrder40.Visible = show;
            this.GridColumnOrder41.Visible = show;
            this.GridColumnOrder42.Visible = show;
            this.GridColumnOrder43.Visible = show;
            this.GridColumnOrder44.Visible = show;
            this.GridColumnOrder45.Visible = show;
            this.GridColumnOrder46.Visible = show;
            this.GridColumnOrder47.Visible = show;
            this.GridColumnOrder48.Visible = show;
            this.GridColumnOrder49.Visible = show;
            this.GridColumnOrder50.Visible = show;
            this.GridColumnOrder51.Visible = show;
            this.GridColumnOrder52.Visible = show;
            this.GridColumnOrder53.Visible = show;
            this.GridColumnOrder54.Visible = show;
            this.GridColumnOrder55.Visible = show;
            this.GridColumnOrder56.Visible = show;
            this.GridColumnOrder57.Visible = show;
            this.GridColumnOrder58.Visible = show;
            this.GridColumnOrder59.Visible = show;
            this.GridColumnOrder60.Visible = show;
            this.GridColumnOrder61.Visible = show;
            this.GridColumnOrder62.Visible = show;
            this.GridColumnOrder63.Visible = show;
            this.GridColumnOrder64.Visible = show;
            this.GridColumnOrder65.Visible = show;
            this.GridColumnOrder66.Visible = show;
            this.GridColumnOrder67.Visible = show;
            this.GridColumnOrder68.Visible = show;
            this.GridColumnOrder69.Visible = show;
            this.GridColumnOrder70.Visible = show;
            this.GridColumnOrder71.Visible = show;
            this.GridColumnOrder72.Visible = show;
            this.GridColumnOrder73.Visible = show;
            this.GridColumnOrder74.Visible = show;
            this.GridColumnOrder75.Visible = show;
            this.GridColumnOrder76.Visible = show;
            this.GridColumnOrder77.Visible = show;
            this.GridColumnOrder78.Visible = show;
            this.GridColumnOrder79.Visible = show;
            this.GridColumnOrder80.Visible = show;
            this.GridColumnOrder81.Visible = show;
            this.GridColumnOrder82.Visible = show;
            this.GridColumnOrder83.Visible = show;
            this.GridColumnOrder84.Visible = show;
            this.GridColumnOrder85.Visible = show;
            this.GridColumnOrder86.Visible = show;
            this.GridColumnOrder87.Visible = show;
            this.GridColumnOrder88.Visible = show;
            this.GridColumnOrder89.Visible = show;
            this.GridColumnOrder90.Visible = show;
            this.GridColumnOrder91.Visible = show;
            this.GridColumnOrder92.Visible = show;
            this.GridColumnOrder93.Visible = show;
            this.GridColumnOrder94.Visible = show;
            this.GridColumnOrder95.Visible = show;
            this.GridColumnOrder96.Visible = show;
            this.GridColumnOrder97.Visible = show;
            this.GridColumnOrder98.Visible = show;
            this.GridColumnOrder99.Visible = show;
            this.GridColumnOrder100.Visible = show;
          
            if (show) { this.GridColumnOrder2.Width = 80; }
            if (show) { this.GridColumnOrder3.Width = 80; }
            if (show) { this.GridColumnOrder4.Width = 80; }
            if (show) { this.GridColumnOrder5.Width = 80; }
            if (show) { this.GridColumnOrder6.Width = 80; }
            if (show) { this.GridColumnOrder7.Width = 80; }
            if (show) { this.GridColumnOrder8.Width = 80; }
            if (show) { this.GridColumnOrder9.Width = 80; }
            if (show) { this.GridColumnOrder10.Width = 80; }
            if (show) { this.GridColumnOrder11.Width = 80; }
            if (show) { this.GridColumnOrder12.Width = 80; }
            if (show) { this.GridColumnOrder13.Width = 80; }
            if (show) { this.GridColumnOrder14.Width = 80; }
            if (show) { this.GridColumnOrder15.Width = 80; }
            if (show) { this.GridColumnOrder16.Width = 80; }
            if (show) { this.GridColumnOrder17.Width = 80; }
            if (show) { this.GridColumnOrder18.Width = 80; }
            if (show) { this.GridColumnOrder19.Width = 80; }
            if (show) { this.GridColumnOrder20.Width = 80; }
            this.GridColumnOrder21.Width = show ? 80 : 0;
            this.GridColumnOrder22.Width = show ? 80 : 0;
            this.GridColumnOrder23.Width = show ? 80 : 0;
            this.GridColumnOrder24.Width = show ? 80 : 0;
            this.GridColumnOrder25.Width = show ? 80 : 0;
            this.GridColumnOrder26.Width = show ? 80 : 0;
            this.GridColumnOrder27.Width = show ? 80 : 0;
            this.GridColumnOrder28.Width = show ? 80 : 0;
            this.GridColumnOrder29.Width = show ? 80 : 0;
            this.GridColumnOrder30.Width = show ? 80 : 0;
            this.GridColumnOrder31.Width = show ? 80 : 0;
            this.GridColumnOrder32.Width = show ? 80 : 0;
            this.GridColumnOrder33.Width = show ? 80 : 0;
            this.GridColumnOrder34.Width = show ? 80 : 0;
            this.GridColumnOrder35.Width = show ? 80 : 0;
            this.GridColumnOrder36.Width = show ? 80 : 0;
            this.GridColumnOrder37.Width = show ? 80 : 0;
            this.GridColumnOrder38.Width = show ? 80 : 0;
            this.GridColumnOrder39.Width = show ? 80 : 0;
            this.GridColumnOrder80.Width = show ? 80 : 0;
            this.GridColumnOrder41.Width = show ? 80 : 0;
            this.GridColumnOrder42.Width = show ? 80 : 0;
            this.GridColumnOrder43.Width = show ? 80 : 0;
            this.GridColumnOrder44.Width = show ? 80 : 0;
            this.GridColumnOrder45.Width = show ? 80 : 0;
            this.GridColumnOrder46.Width = show ? 80 : 0;
            this.GridColumnOrder47.Width = show ? 80 : 0;
            this.GridColumnOrder48.Width = show ? 80 : 0;
            this.GridColumnOrder49.Width = show ? 80 : 0;
            this.GridColumnOrder50.Width = show ? 80 : 0;
            this.GridColumnOrder51.Width = show ? 80 : 0;
            this.GridColumnOrder52.Width = show ? 80 : 0;
            this.GridColumnOrder53.Width = show ? 80 : 0;
            this.GridColumnOrder54.Width = show ? 80 : 0;
            this.GridColumnOrder55.Width = show ? 80 : 0;
            this.GridColumnOrder56.Width = show ? 80 : 0;
            this.GridColumnOrder57.Width = show ? 80 : 0;
            this.GridColumnOrder58.Width = show ? 80 : 0;
            this.GridColumnOrder59.Width = show ? 80 : 0;
            this.GridColumnOrder60.Width = show ? 80 : 0;
            this.GridColumnOrder61.Width = show ? 80 : 0;
            this.GridColumnOrder62.Width = show ? 80 : 0;
            this.GridColumnOrder63.Width = show ? 80 : 0;
            this.GridColumnOrder64.Width = show ? 80 : 0;
            this.GridColumnOrder65.Width = show ? 80 : 0;
            this.GridColumnOrder66.Width = show ? 80 : 0;
            this.GridColumnOrder67.Width = show ? 80 : 0;
            this.GridColumnOrder68.Width = show ? 80 : 0;
            this.GridColumnOrder69.Width = show ? 80 : 0;
            this.GridColumnOrder70.Width = show ? 80 : 0;
            this.GridColumnOrder71.Width = show ? 80 : 0;
            this.GridColumnOrder72.Width = show ? 80 : 0;
            this.GridColumnOrder73.Width = show ? 80 : 0;
            this.GridColumnOrder74.Width = show ? 80 : 0;
            this.GridColumnOrder75.Width = show ? 80 : 0;
            this.GridColumnOrder76.Width = show ? 80 : 0;
            this.GridColumnOrder77.Width = show ? 80 : 0;
            this.GridColumnOrder78.Width = show ? 80 : 0;
            this.GridColumnOrder79.Width = show ? 80 : 0;
            this.GridColumnOrder80.Width = show ? 80 : 0;
            this.GridColumnOrder81.Width = show ? 80 : 0;
            this.GridColumnOrder82.Width = show ? 80 : 0;
            this.GridColumnOrder83.Width = show ? 80 : 0;
            this.GridColumnOrder84.Width = show ? 80 : 0;
            this.GridColumnOrder85.Width = show ? 80 : 0;
            this.GridColumnOrder86.Width = show ? 80 : 0;
            this.GridColumnOrder87.Width = show ? 80 : 0;
            this.GridColumnOrder88.Width = show ? 80 : 0;
            this.GridColumnOrder89.Width = show ? 80 : 0;
            this.GridColumnOrder90.Width = show ? 80 : 0;
            this.GridColumnOrder91.Width = show ? 80 : 0;
            this.GridColumnOrder92.Width = show ? 80 : 0;
            this.GridColumnOrder93.Width = show ? 80 : 0;
            this.GridColumnOrder94.Width = show ? 80 : 0;
            this.GridColumnOrder95.Width = show ? 80 : 0;
            this.GridColumnOrder96.Width = show ? 80 : 0;
            this.GridColumnOrder97.Width = show ? 80 : 0;
            this.GridColumnOrder98.Width = show ? 80 : 0;
            this.GridColumnOrder99.Width = show ? 80 : 0;
            this.GridColumnOrder100.Width = show ? 80 : 0;

        }

        #endregion
    }
}

