﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DevExpress.Xpf.Grid;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;

namespace Nouvem.View.Sales.ARDispatch
{
    /// <summary>
    /// Interaction logic for DispatchGridView.xaml
    /// </summary>
    public partial class DispatchGridView : UserControl
    {
        public DispatchGridView()
        {
            this.InitializeComponent();

            if (ApplicationSettings.Customer == Customer.Woolleys || ApplicationSettings.ChangePriceOnDispatchTouchscreen)
            {
                this.GridColumnPriceMethod.Width = 15;
                this.GridColumnPrice.Width = 20;
                this.GridColumnCode.Width = 0;
                this.GridColumnCode.Visible = false;
                this.GridColumnCode.IsEnabled = false;
            }
            else
            {
                this.GridColumnCode.Width = 35;
                this.GridColumnPrice.Width = 0;
                this.GridColumnPriceMethod.Width = 0;
                this.GridColumnPrice.Visible = false;
                this.GridColumnPrice.IsEnabled = false;
                this.GridControlBandPrice.Visible = false;
                this.GridControlBandPrice.Width = 0;
                this.GridControlBandPriceMethod.Visible = false;
                this.GridControlBandPriceMethod.Width = 0;
            }

            if (!ApplicationSettings.ShowBatchDispatchTouchscreenLine)
            {
                this.GridColumnBatch.Visible = false;
                this.GridColumnBatch.Width = 0;
            }
            else
            {
                this.GridColumnBatch.Width = 40;
            }

            if (!ApplicationSettings.ShowSupplierDispatchTouchscreenLine)
            {
                this.GridColumnPartner.Visible = false;
                this.GridColumnPartner.Width = 0;
            }
            else
            {
                this.GridColumnPartner.Width = 40;
            }
        }

        private void TableViewData_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var hitInfo = this.TableViewData.CalcHitInfo(e.OriginalSource as DependencyObject);

                if (hitInfo == null)
                {
                    return;
                }

                var currentColumn = (hitInfo).Column;
                var currentColumnHeader = currentColumn.FieldName;
                if (currentColumnHeader == "OrderBatchNumber")
                {
                    Messenger.Default.Send(Token.Message, Token.DisplayBatchData);
                }
            }
            catch
            {
            }
        }
    }
}
