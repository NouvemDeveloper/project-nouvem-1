﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Grid;
using DevExpress.Xpf.Printing;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.ViewModel.Sales.ARDispatch;

namespace Nouvem.View.Sales.ARDispatch
{
    /// <summary>
    /// Interaction logic for QuickOrderView.xaml
    /// </summary>
    public partial class QuickOrderView : Window
    {
        public QuickOrderView()
        {
            this.InitializeComponent();
            this.DataContext = new QuickOrderViewModel();

            var gridPathBatch = Settings.Default.QuickOrderBatchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            var gridPathProduct = Settings.Default.QuickOrderProductGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            var gridPathOrder = Settings.Default.QuickOrderOrderGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            var gridPathBatchStock = Settings.Default.QuickOrderBatchStockGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            var gridPathProductStock = Settings.Default.QuickOrderProductStockGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            var gridPathOrderStock = Settings.Default.QuickOrderOrderStockGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            Messenger.Default.Register<string>(this, Token.CloseQuickOrderWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            Messenger.Default.Register<string>(this, Token.ShowQuickOrderView, s =>
                {
                    this.GridControlBatchView.Visibility = Visibility.Collapsed;
                    this.GridControlProductView.Visibility = Visibility.Collapsed;
                    this.GridControlOrderView.Visibility = Visibility.Collapsed;
                    this.GridControlBatchStockView.Visibility = Visibility.Collapsed;
                    this.GridControlProductStockView.Visibility = Visibility.Collapsed;
                    this.GridControlOrderStockView.Visibility = Visibility.Collapsed;
                    this.GridOrderStockView.Visibility = Visibility.Collapsed;

                    if (s == "Batch")
                    {
                        this.GridControlBatchView.Visibility = Visibility.Visible;
                        this.GridControlBatchStockView.Visibility = Visibility.Visible;
                    }
                    else if (s == "Product")
                    {
                        this.GridControlProductView.Visibility = Visibility.Visible;
                        this.GridControlProductStockView.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        this.GridOrderStockView.Visibility = Visibility.Visible;
                        this.GridControlOrderView.Visibility = Visibility.Visible;
                        this.GridControlOrderStockView.Visibility = Visibility.Visible;
                    }
                });

            this.WindowState = WindowSettings.QuickOrderMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Loaded += (sender, args) =>
            {
                #region grid set up
                if (File.Exists(gridPathBatch))
                {
                    try
                    {
                        this.GridControlBatchView.RestoreLayoutFromXml(gridPathBatch);
                    }
                    catch
                    {
                    }
                }

                if (File.Exists(gridPathProduct))
                {
                    try
                    {
                        this.GridControlProductView.RestoreLayoutFromXml(gridPathProduct);
                    }
                    catch
                    {
                    }
                }

                if (File.Exists(gridPathOrder))
                {
                    try
                    {
                        this.GridControlOrderView.RestoreLayoutFromXml(gridPathOrder);
                    }
                    catch
                    {
                    }
                }

                if (File.Exists(gridPathBatchStock))
                {
                    try
                    {
                        this.GridControlBatchStockView.RestoreLayoutFromXml(gridPathBatchStock);
                    }
                    catch
                    {
                    }
                }

                if (File.Exists(gridPathProductStock))
                {
                    try
                    {
                        this.GridControlProductStockView.RestoreLayoutFromXml(gridPathProductStock);
                    }
                    catch
                    {
                    }
                }

                if (File.Exists(gridPathOrderStock))
                {
                    try
                    {
                        this.GridControlOrderStockView.RestoreLayoutFromXml(gridPathOrderStock);
                    }
                    catch
                    {
                    }
                }
                #endregion
            };

            this.Closing += (sender, args) =>
            {
                this.ResetSearches();
                this.GridControlBatchView.SaveLayoutToXml(gridPathBatch);
                this.GridControlProductView.SaveLayoutToXml(gridPathProduct);
                this.GridControlOrderView.SaveLayoutToXml(gridPathOrder);
                this.GridControlBatchStockView.SaveLayoutToXml(gridPathBatchStock);
                this.GridControlProductStockView.SaveLayoutToXml(gridPathProductStock);
                this.GridControlBatchStockView.SaveLayoutToXml(gridPathOrderStock);
                WindowSettings.QuickOrderHeight = this.Height;
                WindowSettings.QuickOrderLeft = this.Left;
                WindowSettings.QuickOrderTop = this.Top;
                WindowSettings.QuickOrderWidth = this.Width;
                WindowSettings.QuickOrderMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }

        void OnInitNewRow(object sender, InitNewRowEventArgs e)
        {
            var view = (TableView)sender;
            Dispatcher.BeginInvoke(new Action(
                () => {
                    view.FocusedRowHandle = GridControl.InvalidRowHandle;
                    var newItemRowVisibleIndex = view.Grid.GetRowVisibleIndexByHandle(GridControl.NewItemRowHandle);
                    var editedRowVisibleIndex = newItemRowVisibleIndex;
                    switch (view.NewItemRowPosition)
                    {
                        case NewItemRowPosition.Bottom: editedRowVisibleIndex -= 1; break;
                        case NewItemRowPosition.Top: editedRowVisibleIndex += 1; break;
                    }
                    var editedRowHandle = view.Grid.GetRowHandleByVisibleIndex(editedRowVisibleIndex);
                    view.FocusedRowHandle = editedRowHandle;
                    view.ShowEditor();
                }
            ));
        }

        private void ResetSearches()
        {
            this.GridControlBatchView.FilterCriteria = null;
            this.GridControlBatchView.View.SearchString = string.Empty;
            this.GridControlProductView.FilterCriteria = null;
            this.GridControlProductView.View.SearchString = string.Empty;
            this.GridControlOrderView.FilterCriteria = null;
            this.GridControlOrderView.View.SearchString = string.Empty;
            this.GridControlBatchStockView.FilterCriteria = null;
            this.GridControlBatchStockView.View.SearchString = string.Empty;
            this.GridControlProductStockView.FilterCriteria = null;
            this.GridControlProductStockView.View.SearchString = string.Empty;
            this.GridControlOrderStockView.FilterCriteria = null;
            this.GridControlOrderStockView.View.SearchString = string.Empty;
        }

        private void BarButtonItemBatch_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.GridControlBatchStockView.SelectAll();
        }

        private void BarButtonItemProduct_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.GridControlProductStockView.SelectAll();
        }
    }
}
