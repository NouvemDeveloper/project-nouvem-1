﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Shared;

namespace Nouvem.View.Sales.ARDispatch
{
    /// <summary>
    /// Interaction logic for ApplyPriceView.xaml
    /// </summary>
    public partial class ApplyPriceView : Window
    {
        public ApplyPriceView()
        {
            this.InitializeComponent();
            this.Loaded += (sender, args) => this.TextBoxPrice.Focus();
            this.Closing += (sender, args) =>
            {
                WindowSettings.ApplyPriceHeight = this.Height;
                WindowSettings.ApplyPriceLeft = this.Left;
                WindowSettings.ApplyPriceTop = this.Top;
                WindowSettings.ApplyPriceWidth = this.Width;
            };
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.TextBoxPrice.Text.ToDecimal(), Token.ApplyPrice);
            this.Close();
        }

        private void TextBoxPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.TextBoxPrice.Text != string.Empty && !this.TextBoxPrice.Text.IsDecimalSequence())
            {
                this.TextBoxPrice.Text = this.TextBoxPrice.Text.Substring(0,this.TextBoxPrice.Text.Length - 1);
            }
        }
    }
}
