﻿// -----------------------------------------------------------------------
// <copyright file="SequencerMainDataView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.KillLine.Sequencer
{
    using System.Windows.Controls;
    using System.Threading;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for SequencerMainDataView.xaml
    /// </summary>
    public partial class SequencerMainDataView : UserControl
    {
        public SequencerMainDataView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.FocusToEartag, s =>
            {
                ThreadPool.QueueUserWorkItem(x => this.TextBoxEartag.Dispatcher.Invoke(() => this.TextBoxEartag.Focus()));
            });

            ThreadPool.QueueUserWorkItem(x => this.TextBoxEartag.Dispatcher.Invoke(() => this.TextBoxEartag.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToHerdNo, s => this.TextBoxHerd.Focus());
            Messenger.Default.Register<bool>(this, Token.DisableSequencerHerdNo, b => this.TextBoxHerd.IsReadOnly = true);
        }
    }
}
