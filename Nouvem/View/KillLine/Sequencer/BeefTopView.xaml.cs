﻿// -----------------------------------------------------------------------
// <copyright file="BeefTopView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.KillLine.Sequencer
{
    using System.IO;
    using System.Windows.Controls;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for BeefTopView.xaml
    /// </summary>
    public partial class BeefTopView : UserControl
    {
        public BeefTopView()
        {
            this.InitializeComponent();
            var gridPassportPath = Settings.Default.SequencerPassportGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
            this.Unloaded += (sender, args) =>
            {
                this.GridControlCurrentPassport.SaveLayoutToXml(gridPassportPath);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPassportPath))
                {
                    this.GridControlCurrentPassport.RestoreLayoutFromXml(gridPassportPath);
                }
            };
        }
    }
}
