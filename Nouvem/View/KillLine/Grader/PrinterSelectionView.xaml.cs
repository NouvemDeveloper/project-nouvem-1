﻿// -----------------------------------------------------------------------
// <copyright file="PrinterSelectionView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.KillLine.Grader
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for PrinterSelectionView.xaml
    /// </summary>
    public partial class PrinterSelectionView : Window
    {
        public PrinterSelectionView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.ClosePrinterSelectionWindow, s => this.Close());
            this.Closing += (sender, args) => Messenger.Default.Unregister(this);
        }
    }
}
