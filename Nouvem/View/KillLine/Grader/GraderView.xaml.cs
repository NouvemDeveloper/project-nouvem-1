﻿// -----------------------------------------------------------------------
// <copyright file="GraderView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Logging;

namespace Nouvem.View.KillLine.Grader
{
    using System.IO;
    using System.Windows.Controls;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    /// <summary>
    /// Interaction logic for SequencerView.xaml
    /// </summary>
    public partial class GraderView : UserControl
    {
        public GraderView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<int>(this, Token.ScrollToRow, i =>
            {
                try
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.GridControlStockDetails.View.ScrollIntoView(i);
                    }));
                }
                catch (Exception e)
                {

                }
            });

            var gridPath = Settings.Default.GraderGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));
  
            this.Unloaded += (sender, args) =>
            {
                try
                {
                    this.GridControlStockDetails.SaveLayoutToXml(gridPath);
                }
                catch (Exception e)
                {
                    var log = new Logger();
                    log.LogError(this.GetType(),e.Message);
                }
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    try
                    {
                        this.GridControlStockDetails.RestoreLayoutFromXml(gridPath);
                    }
                    catch (Exception e)
                    {
                        var log = new Logger();
                        log.LogError(this.GetType(), e.Message);
                    }
                }
            };
        }
    }
}

