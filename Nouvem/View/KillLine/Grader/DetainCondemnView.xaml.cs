﻿// -----------------------------------------------------------------------
// <copyright file="DetainCondemnView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Windows.Controls;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;

namespace Nouvem.View.KillLine.Grader
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for DetainCondemnView.xaml
    /// </summary>
    public partial class DetainCondemnView : Window
    {
        public DetainCondemnView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseDetainCondemnWindow, s => this.Close());
        }

        private void ComboBox_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //var items = new List<object>();
            var items = new List<CollectionData>();
            var localCombo = (sender as ComboBox);
            localCombo.IsDropDownOpen = false;

            foreach (var item in localCombo.Items)
            {
                items.Add(new CollectionData { Data = (item as NouFatColour).Name, ID = (item as NouFatColour).NouFatColourID, Identifier = "DetainCondemn" });
            }

            // Send our combo collection to the display module.
            Messenger.Default.Send(items, Token.DisplayTouchscreenCollection);
            Messenger.Default.Send(ViewType.CollectionDisplay);
        }
    }
}
