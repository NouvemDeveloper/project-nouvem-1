﻿// -----------------------------------------------------------------------
// <copyright file="LairageAnimalsSearchView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.KillLine.Lairage
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for LairageIntakeView.xaml
    /// </summary>
    public partial class LairageAnimalsSearchView : Window
    {
        public LairageAnimalsSearchView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseLairageAnimalsSearchWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());
            this.GridControlStockDetails.FilterCriteria = null;
            this.GridControlStockDetails.View.SearchString = string.Empty;

            this.WindowState = WindowSettings.LairageAnimalsSearchMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            var gridPath = Settings.Default.LairageAnimalSearchGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlStockDetails.RestoreLayoutFromXml(gridPath);
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableViewStockData);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlStockDetails.FilterCriteria = null;
                this.GridControlStockDetails.SaveLayoutToXml(gridPath);
                WindowSettings.LairageAnimalsSearchHeight = this.Height;
                WindowSettings.LairageAnimalsSearchLeft = this.Left;
                WindowSettings.LairageAnimalsSearchTop = this.Top;
                WindowSettings.LairageAnimalsSearchWidth = this.Width;
                WindowSettings.LairageAnimalsSearchMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void TabItem_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        private void BarButtonItem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.GridControlStockDetails.SelectAll();
        }
    }
}

