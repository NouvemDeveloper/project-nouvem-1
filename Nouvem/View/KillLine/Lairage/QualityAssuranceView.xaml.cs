﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.BusinessLogic;
using Nouvem.Global;

namespace Nouvem.View.KillLine.Lairage
{
    /// <summary>
    /// Interaction logic for QualityAssuranceView.xaml
    /// </summary>
    public partial class QualityAssuranceView : Window
    {
        public QualityAssuranceView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this,Token.CloseLairageWindow, s => this.Close());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, s => this.TextBoxEartag.Focus());
            this.Loaded += (sender, args) => this.TextBoxHerd.Focus();
            this.Closing += (sender, args) => Messenger.Default.Unregister(this);
        }
    }
}
