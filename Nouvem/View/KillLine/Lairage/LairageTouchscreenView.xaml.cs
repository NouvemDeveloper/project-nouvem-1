﻿// -----------------------------------------------------------------------
// <copyright file="LairageTouchscreenView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared.Localisation;

namespace Nouvem.View.KillLine.Lairage
{
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for LairageTouchscreenView.xaml
    /// </summary>
    public partial class LairageTouchscreenView : UserControl
    {
        public LairageTouchscreenView()
        {
            InitializeComponent();
            Messenger.Default.Register<bool>(this, Token.DisplayLairageKeyboard, b => this.Keyboard.IsOpen = b);
            this.ButtonExpand.Content = Strings.Expand;
            this.GridKill.Height = 200;
        }

        private void ButtonExpand_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.ButtonExpand.Content.ToString() == Strings.Expand)
            {
                this.GridKill.Height = 700;
                this.ButtonExpand.Content = Strings.Compress;
            }
            else
            {
                this.GridKill.Height = 200;
                this.ButtonExpand.Content = Strings.Expand;
            }
        }
    }
}
