﻿// -----------------------------------------------------------------------
// <copyright file="AccountsPurchasesView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.View.Account
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for AccountsPurchasesView.xaml
    /// </summary>
    public partial class AccountsPurchasesView : Window
    {
        public AccountsPurchasesView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseAccountsPurchases, s => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.AccountsPurchasesMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            var gridPath = Settings.Default.AccountsPurchasesGridPath.Replace(".", string.Format("{0}.", NouvemGlobal.UserId.ToInt()));

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(gridPath))
                {
                    this.GridControlSearchData.RestoreLayoutFromXml(gridPath);
                }

                if (!ApplicationSettings.UseLairagesForPurchaseAccounts)
                {
                    this.GridColumnKillDate.Visible = false;
                    this.GridColumnNotKilled.Visible = false;
                    this.GridColumnKilled.Visible = false;
                }

                Messenger.Default.Register<string>(this, Token.DisplayPrintPreview, s =>
                {
                    try
                    {
                        var preview = new DocumentPreviewWindow();
                        var link = new PrintableControlLink(this.TableView);
                        link.PaperKind = System.Drawing.Printing.PaperKind.A4;
                        var model = new LinkPreviewModel(link);
                        preview.Model = model;
                        link.CreateDocument(true);
                        preview.ShowDialog();
                    }
                    catch
                    {
                    }
                });
            };

            this.Closing += (sender, args) =>
            {
                this.GridControlSearchData.FilterCriteria = null;
                this.GridControlSearchData.SaveLayoutToXml(gridPath);
                WindowSettings.AccountsPurchasesHeight = this.Height;
                WindowSettings.AccountsPurchasesLeft = this.Left;
                WindowSettings.AccountsPurchasesTop = this.Top;
                WindowSettings.AccountsPurchasesWidth = this.Width;
                WindowSettings.AccountsPurchasesMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }

        private void BarButtonItem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            this.GridControlSearchData.SelectAll();
        }
    }
}



