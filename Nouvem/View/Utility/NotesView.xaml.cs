﻿// -----------------------------------------------------------------------
// <copyright file="NotesView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Threading;
using Nouvem.Keypad;
using Nouvem.Logging;
using Nouvem.Model.Event;

namespace Nouvem.View.Utility
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for NotesView.xaml
    /// </summary>
    public partial class NotesView : Window
    {
        /// <summary>
        /// Event used to broadcast the touch pad data.
        /// </summary>
        public event EventHandler<StringArgs> DataReceived;


        public NotesView(string notes = "")
        {
            this.InitializeComponent();
            Messenger.Default.Send(true, Token.WindowStatus);
            this.TextBoxNotes.Text = notes;
            ThreadPool.QueueUserWorkItem(x => this.TextBoxNotes.Dispatcher.Invoke(() => this.TextBoxNotes.Focus()));

            this.Closing += (sender, args) =>
            {
                WindowSettings.NotesHeight = this.Height;
                WindowSettings.NotesLeft = this.Left;
                WindowSettings.NotesTop = this.Top;
                WindowSettings.NotesWidth = this.Width;

                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };
        }

        public void SetNote(string note)
        {
            this.TextBoxNotes.Text = note;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var log = new Logger();
            log.LogInfo(this.GetType(), $"NotesView: Sending note:{this.TextBoxNotes.Text}");
            this.OnDataReceived(new StringArgs(this.TextBoxNotes.Text));
            Messenger.Default.Send(this.TextBoxNotes.Text, Token.NotesEntered);
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Creates a new event, to broadcast the touch pad data.
        /// </summary>
        /// <param name="e">The event argument.</param>
        private void OnDataReceived(StringArgs e)
        {
            if (this.DataReceived != null)
            {
                this.DataReceived(this, e);
            }
        }
    }
}
