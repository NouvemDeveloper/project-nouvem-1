﻿// -----------------------------------------------------------------------
// <copyright file="UserSetUpContainerView .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.View.User
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for UserSetUpContainerView.xaml
    /// </summary>
    public partial class UserSetUpContainerView : Window
    {
        public UserSetUpContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Send(true, Token.WindowStatus);
            Messenger.Default.Register<string>(this, Token.CloseUserSetUpWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.UserMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            this.Closing += (sender, args) =>
            {
                WindowSettings.UserHeight = this.Height;
                WindowSettings.UserLeft = this.Left;
                WindowSettings.UserTop = this.Top;
                WindowSettings.UserWidth = this.Width;
                WindowSettings.UserMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };
        }
    }
}
