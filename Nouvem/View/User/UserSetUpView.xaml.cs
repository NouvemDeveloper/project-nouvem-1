﻿// -----------------------------------------------------------------------
// <copyright file="UserSetUpView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.User
{
    using System.Threading;
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    /// <summary>
    /// Interaction logic for UserSetUpView.xaml
    /// </summary>
    public partial class UserSetUpView : UserControl
    {
        public UserSetUpView()
        {
            this.InitializeComponent();

            if (!NouvemGlobal.IsNouvemUser)
            {
                this.TextBoxDecryptedPassword.Visibility = Visibility.Collapsed;
            }

            ThreadPool.QueueUserWorkItem(x => this.TextBoxUserName.Dispatcher.Invoke(() => this.TextBoxUserName.Focus()));
            Messenger.Default.Register<string>(this, Token.FocusToDataUpdate, x => this.ComboBoxName.Focus());
            Messenger.Default.Register<string>(this, Token.FocusToSelectedControl, x => this.TextBoxUserName.Focus());
            Messenger.Default.Register<string>(this, Token.ResetConfirmationPassword, x => this.PasswordBoxConfirm.Password = string.Empty);
            Messenger.Default.Register<string>(
                this,
                Token.ResetPasswords,
                x =>
                    {
                        this.PasswordBoxConfirm.Password = x;
                        this.PasswordBoxPassword.Password = x;
                    });
        }

        private void PasswordBoxConfirm_LostFocus(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.PasswordBoxConfirm.Password, Token.UserPasswordConfirm);
        }

        private void PasswordBoxPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(this.PasswordBoxPassword.Password, Token.UserPassword);
        }

        private void PasswordBoxConfirm_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Messenger.Default.Send(true, Token.UserPassword);
        }
    }
}
