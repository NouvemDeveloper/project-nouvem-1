﻿// -----------------------------------------------------------------------
// <copyright file="UserGroupContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using DevExpress.Xpf.Printing;
using Nouvem.Shared;

namespace Nouvem.View.User
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for UserGroupContainerView.xaml
    /// </summary>
    public partial class UserGroupContainerView : Window
    {
        public UserGroupContainerView()
        {
            this.InitializeComponent();

            Messenger.Default.Register<string>(this, Token.CloseUserGroupWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.WindowState = WindowSettings.UserGroupMaximised.ToWindowsState();
            this.SystemMessageView.Height = this.WindowState == WindowState.Maximized ? ApplicationSettings.SystemMessageBoxHeight : 0;
            this.StateChanged += (sender, args) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.SystemMessageView.Height = ApplicationSettings.SystemMessageBoxHeight;
                }
                else
                {
                    this.SystemMessageView.Height = 0;
                }
            };

            Messenger.Default.Send(true, Token.WindowStatus);

            this.Closing += (sender, args) =>
            {
                WindowSettings.UserGroupHeight = this.Height;
                WindowSettings.UserGroupLeft = this.Left;
                WindowSettings.UserGroupTop = this.Top;
                WindowSettings.UserGroupWidth = this.Width;
                WindowSettings.UserGroupMaximised = this.WindowState.ToBool();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            this.Loaded += (sender, args) =>
            {
                
            };
        }
    }
}
