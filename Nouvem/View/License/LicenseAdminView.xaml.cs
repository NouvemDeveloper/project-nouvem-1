﻿// -----------------------------------------------------------------------
// <copyright file="LicenseAdminView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.License
{
    using System.IO;
    using System.Windows.Controls;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for LicenseAdminView.xaml
    /// </summary>
    public partial class LicenseAdminView : UserControl
    {
        public LicenseAdminView()
        {
            this.InitializeComponent();

            this.Unloaded += (sender, args) =>
            {
                this.GridControlLicensees.SaveLayoutToXml(Settings.Default.DevicesGridPath);
                this.GridControlLicenseDetails.SaveLayoutToXml(Settings.Default.LicenseDetailsGridPath);
            };

            this.Loaded += (sender, args) =>
            {
                if (File.Exists(Settings.Default.DevicesGridPath))
                {
                    this.GridControlLicensees.RestoreLayoutFromXml(Settings.Default.DevicesGridPath);
                }

                if (File.Exists(Settings.Default.LicenseDetailsGridPath))
                {
                    this.GridControlLicenseDetails.RestoreLayoutFromXml(Settings.Default.LicenseDetailsGridPath);
                }
            };
        }
    }
}
