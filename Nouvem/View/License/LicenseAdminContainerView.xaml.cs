﻿// -----------------------------------------------------------------------
// <copyright file="DeviceSetUpContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.License
{
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;

    /// <summary>
    /// Interaction logic for LicenseAdminContainerView.xaml
    /// </summary>
    public partial class LicenseAdminContainerView : Window
    {
        public LicenseAdminContainerView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseLicenseAdminWindow, x => this.Close());
            Messenger.Default.Register<string>(this, Token.CloseAllWindows, x => this.Close());

            this.Closing += (sender, args) =>
            {
                Settings.Default.Save();
                Messenger.Default.Send(false, Token.WindowStatus);
                Messenger.Default.Unregister(this);
            };

            Messenger.Default.Send(true, Token.WindowStatus);
        }
    }
}
