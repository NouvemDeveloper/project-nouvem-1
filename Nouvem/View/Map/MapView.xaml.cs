﻿// -----------------------------------------------------------------------
// <copyright file="MapView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.View.Map
{
    using System;
    using System.Threading;
    using System.Windows.Controls;
    using DevExpress.Xpf.Map;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for MapView.xaml
    /// </summary>
    public partial class MapView : UserControl
    {
        public MapView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<Tuple<string, string, GeoPoint, int>>(
                this, 
                tuple => this.SearchDataProvider.Search(tuple.Item1, tuple.Item2, tuple.Item3, tuple.Item4));

            ThreadPool.QueueUserWorkItem(x => this.TextBoxKeywords.Dispatcher.Invoke(() => this.TextBoxKeywords.Focus()));
        }
    }
}
