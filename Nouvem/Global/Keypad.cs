﻿// -----------------------------------------------------------------------
// <copyright file="Keypad.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.ViewModel;

namespace Nouvem.Global
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Properties;

    public static class Keypad
    {
        /// <summary>
        /// The keypad reference.
        /// </summary>
        private static Nouvem.Keypad.Keypad keypad;

        /// <summary>
        /// The keypad reference.
        /// </summary>
        private static Nouvem.Keypad.KeypadSides keypadSides;

        /// <summary>
        /// The keypad reference.
        /// </summary>
        public static bool HoldKepadValue { get; set; }

        /// <summary>
        /// Displays the keypad, passing it's input to the target string.
        /// </summary>
        /// <param name="target">The calling target.</param>
        /// <param name="isScanner">Is this to run on a handheld flag.</param>
        public static void Show(KeypadTarget target, bool isScanner = false, bool isEmulator = false, bool setTopmost = false, bool showHold = false, string displayMessage = "", bool useKeypadSides = false)
        {
            var top = Settings.Default.TouchPadTop > 0 ? Settings.Default.TouchPadTop : 300;
            var left = Settings.Default.TouchPadLeft > 0 ? Settings.Default.TouchPadLeft : 300;
            var width = Settings.Default.TouchPadWidth > 0 ? Settings.Default.TouchPadWidth : 500;
            var height = Settings.Default.TouchPadHeight > 0 ? Settings.Default.TouchPadHeight : 500;

            if (isEmulator)
            {
                height = ApplicationSettings.ScannerEmulatorHeight;
                width = ApplicationSettings.ScannerEmulatorWidth;
            }

            if (!useKeypadSides)
            {

                keypad = new Nouvem.Keypad.Keypad(top, left, height, width, isScanner, isEmulator, HoldKepadValue, showHold, displayMessage);

                // Send the incoming data back to it's interested observer(s)
                keypad.DataReceived += (sender, args) =>
                {
                    HoldKepadValue = false;
                    var holdValue = args.IsChecked;

                    Messenger.Default.Send(args.Data, target);
                    HoldKepadValue = holdValue;
                    ViewModelLocator.IndicatorStatic.TareToHold = HoldKepadValue;
                };

                keypad.PositionDataReceived += (o, args) =>
                {
                    Settings.Default.TouchPadTop = args.Top;
                    Settings.Default.TouchPadLeft = args.Left;
                    Settings.Default.TouchPadHeight = args.Height;
                    Settings.Default.TouchPadWidth = args.Width;
                    Settings.Default.Save();
                };

                keypad.Show();
                keypad.Topmost = setTopmost;
                keypad.Focus();
            }
            else
            {

                keypadSides = new Nouvem.Keypad.KeypadSides(top, left, height, width, isScanner, isEmulator, HoldKepadValue, showHold, displayMessage);

                // Send the incoming data back to it's interested observer(s)
                keypadSides.DataReceived += (sender, args) =>
                {
                    HoldKepadValue = false;
                    var holdValue = args.IsChecked;

                    Messenger.Default.Send(args.Data, target);
                    HoldKepadValue = holdValue;
                    ViewModelLocator.IndicatorStatic.TareToHold = HoldKepadValue;
                };

                keypadSides.PositionDataReceived += (o, args) =>
                {
                    Settings.Default.TouchPadTop = args.Top;
                    Settings.Default.TouchPadLeft = args.Left;
                    Settings.Default.TouchPadHeight = args.Height;
                    Settings.Default.TouchPadWidth = args.Width;
                    Settings.Default.Save();
                };

                keypadSides.Show();
                keypadSides.Topmost = setTopmost;
                keypadSides.Focus();
            }
        }
      
    }

    /// <summary>
    /// Token enumeration for the keypad result data.
    /// </summary>
    public enum KeypadTarget
    {
        /// <summary>
        /// Data used for the tare container.
        /// </summary>
        TareContainer,

        /// <summary>
        /// Data used for the indicator weight.
        /// </summary>
        Weight,

        /// <summary>
        /// Data used for the indicator weight.
        /// </summary>
        Sequencer,

        /// <summary>
        /// Data used for the touchscreen quantity entry.
        /// </summary>
        Quantity,

        /// <summary>
        /// Data used for the touchscreen into production recipe quantity entry.
        /// </summary>
        RecipeQuantity,

        /// <summary>
        /// Used at dispatch and into production for auto capturing scanned carcass labels.
        /// </summary>
        AutoBoxCount,

        /// <summary>
        /// Data used for the touchscreen pieces entry.
        /// </summary>
        Pieces,

        /// <summary>
        /// Data used for the touchscreen palletisation module.
        /// </summary>
        Palletisation,

        /// <summary>
        /// Data used for the touchscreen temperature entry.
        /// </summary>
        Temp,

        /// <summary>
        /// Data used for the touchscreen traceability entry.
        /// </summary>
        Traceability,

        /// <summary>
        /// Data used for the manual serial entry.
        /// </summary>
        ManualSerial,

        /// <summary>
        /// Data used for the goods in scan.
        /// </summary>
        IntakeScan,

        /// <summary>
        /// Data used for the goods in scan.
        /// </summary>
        CarcassChange,

        /// <summary>
        /// Data used for the goods in scan.
        /// </summary>
        KillNoChange,

        /// <summary>
        /// Data used for the tare calculator.
        /// </summary>
        TareCalculator,

        /// <summary>
        /// Data used for changing a weight on a grader carcass side.
        /// </summary>
        GraderWeightChange,

        /// <summary>
        /// Data used for the out of production pieces.
        /// </summary>
        ProductionPieces,

        /// <summary>
        /// Data used for the touchscreen labels to print qty.
        /// </summary>
        Labels,

        /// <summary>
        /// Data used for the touchscreen labels to print qty.
        /// </summary>
        RecipeQty,

        /// <summary>
        /// Data used for the touchscreen labels to print qty.
        /// </summary>
        PlusManualWeight,

        /// <summary>
        /// Data used for the touchscreen stock labels to print qty.
        /// </summary>
        StockLabels,

        /// <summary>
        /// Data used for the touchscreen login to enable a user login password.
        /// </summary>
        TouchscreenLogin,

        /// <summary>
        /// Data used for the handheld stock take.
        /// </summary>
        StockTake,

        /// <summary>
        /// Data used for the handheld stock move.
        /// </summary>
        StockMovement,

        /// <summary>
        /// Data used for the handheld batch stock move values entry.
        /// </summary>
        BatchStockMove,

        /// <summary>
        /// Data used for the handheld into production.
        /// </summary>
        IntoProduction,

        /// <summary>
        /// Data used for the youchscreen dispatch price changing.
        /// </summary>
        ChangePriceAtDispatch
    }
}

