﻿// -----------------------------------------------------------------------
// <copyright file="CommitHelper.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Global.Dependency
{
    using System;
    using System.Windows;
    using System.Windows.Threading;
    using DevExpress.Xpf.Grid;
    using DevExpress.Xpf.Grid.TreeList;

    /// <summary>
    /// Allows a grid control cell value change to bind it's value immediately when changed.
    /// </summary>
    ///<remarks>Note: This approach is needed with the current devexpress version 14.6. 15.1 + versions have a property, EnableImmediatePosting, that does this.</remarks>.
    public class CommitHelper
    {
        /// <summary>
        /// Register the property.
        /// </summary>
        public static readonly DependencyProperty CommitOnValueChangedProperty = 
            DependencyProperty.RegisterAttached("CommitOnValueChanged", typeof(bool), typeof(CommitHelper), new PropertyMetadata(CommitOnValueChangedPropertyChanged));

        /// <summary>
        /// Sets the change value.
        /// </summary>
        /// <param name="element">The element to change.</param>
        /// <param name="value">The value of the change.</param>
        public static void SetCommitOnValueChanged(GridColumnBase element, bool value)
        {
            element.SetValue(CommitOnValueChangedProperty, value);
        }

        /// <summary>
        /// Gets the current value.
        /// </summary>
        /// <param name="element">The element whose value to get.</param>
        /// <returns>The current value of the element.</returns>
        public static bool GetCommitOnValueChanged(GridColumnBase element)
        {
            return (bool)element.GetValue(CommitOnValueChangedProperty);
        }

        /// <summary>
        /// Sengs the changed cell value as the property is changed.
        /// </summary>
        /// <param name="source">The source object.</param>
        /// <param name="e">The event args.</param>
        private static void CommitOnValueChangedPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            var col = source as GridColumnBase;
            if (col.View == null)
            {
                Dispatcher.CurrentDispatcher.BeginInvoke(new Action<GridColumnBase, bool>((column, subscribe) => { ToggleCellValueChanging(column, subscribe); }), col, (bool)e.NewValue);
            }
            else
            {
                ToggleCellValueChanging(col, (bool)e.NewValue);
            }
        }

        /// <summary>
        /// Subscribe/unsubscribe the gid cell.
        /// </summary>
        /// <param name="col">The grid column.</param>
        /// <param name="subscribe">Attach/detach event flag.</param>
        private static void ToggleCellValueChanging(GridColumnBase col, bool subscribe)
        {
            if (col.View == null)
            {
                return;
            }

            if (subscribe)
            {
                if (col.View is TreeListView)
                {
                    (col.View as TreeListView).CellValueChanging += TreeCellValueChanging;
                }
                else
                {
                    (col.View as GridViewBase).CellValueChanging += GridCellValueChanging;
                }
            }
            else
            {
                if (col.View is TreeListView)
                {
                    (col.View as TreeListView).CellValueChanging -= TreeCellValueChanging;
                }
                else
                {
                    (col.View as GridViewBase).CellValueChanging -= GridCellValueChanging;
                }
            }
        }

        /// <summary>
        /// Post the changing tree view cell value.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arg.</param>
        static void TreeCellValueChanging(object sender, TreeListCellValueChangedEventArgs e)
        {
            if ((bool)e.Column.GetValue(CommitOnValueChangedProperty))
            {
                (sender as DataViewBase).PostEditor();
                e.Handled = true;
            }
        }

        /// <summary>
        /// Post the changing grid value.
        /// </summary>
        /// <param name="sender">The sender object.</param>
        /// <param name="e">The event arg.</param>
        static void GridCellValueChanging(object sender, CellValueChangedEventArgs e)
        {
            if ((bool)e.Column.GetValue(CommitOnValueChangedProperty))
            {
                (sender as DataViewBase).PostEditor();
                e.Handled = true;
            }
        }
    }
}