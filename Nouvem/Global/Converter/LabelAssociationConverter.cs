﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Nouvem.Global.Converter
{
    public class LabelAssociationConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!values.Any())
            {
                return null;
            }

            if (values[0] is int && values[1] is int?)
            {
                var value1 = (int)values[0];
                var value2 = (int?)values[1];

                var convert = value1.ToString();

                if (value2.HasValue)
                {
                    convert = string.Format("{0}   ({1})", convert, value2);
                }

                return convert;
            }

            return null;


        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { };
        }
    }
}
