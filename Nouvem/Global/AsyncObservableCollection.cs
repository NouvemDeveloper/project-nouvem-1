﻿// -----------------------------------------------------------------------
// <copyright file="AsyncObservableCollection.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Nouvem.Global
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Threading;

    /// <summary>
    /// Class which models an asynchronous collection of generic types which can be updated from multiple threads.
    /// </summary>
    /// <typeparam name="T">The type contained in the enumeration.</typeparam>
    public class AsyncObservableCollection<T> : ObservableCollection<T>
    {
        /// <summary>
        /// Set the context to synchronize with.
        /// </summary>
        private SynchronizationContext synchronizationContext = SynchronizationContext.Current;

        /// <summary>
        /// Initializes a new instance of the AsyncObservableCollection class.
        /// </summary>
        public AsyncObservableCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the AsyncObservableCollection class.
        /// </summary>
        /// <param name="list">The collection to synchronize.</param>
        public AsyncObservableCollection(IEnumerable<T> list)
            : base(list)
        {
        }

        /// <summary>
        /// Manages the alteration of the collection.
        /// </summary>
        /// <param name="e">The collection changed event.</param>
        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (SynchronizationContext.Current == this.synchronizationContext)
            {
                // Execute the CollectionChanged event on the current thread
                this.RaiseCollectionChanged(e);
            }
            else
            {
                // Raises the CollectionChanged event on the creator thread
                this.synchronizationContext.Send(this.RaiseCollectionChanged, e);
            }
        }

        /// <summary>
        /// Manage the change in a property.
        /// </summary>
        /// <param name="e">The event.</param>
        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (SynchronizationContext.Current == this.synchronizationContext)
            {
                // Execute the PropertyChanged event on the current thread
                this.RaisePropertyChanged(e);
            }
            else
            {
                // Raises the PropertyChanged event on the creator thread
                this.synchronizationContext.Send(this.RaisePropertyChanged, e);
            }
        }

        /// <summary>
        /// Raise a collection changed event.
        /// </summary>
        /// <param name="param">The parameter relating to the change in the collection.</param>
        private void RaiseCollectionChanged(object param)
        {
            // We are in the creator thread, call the base implementation directly
            base.OnCollectionChanged((NotifyCollectionChangedEventArgs)param);
        }

        /// <summary>
        /// Manage the changed property.
        /// </summary>
        /// <param name="param">The parameter of the changed property.</param>
        private void RaisePropertyChanged(object param)
        {
            // We are in the creator thread, call the base implementation directly.
            base.OnPropertyChanged((PropertyChangedEventArgs)param);
        }
    }
}

