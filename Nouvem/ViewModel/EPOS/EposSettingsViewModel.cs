﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Nouvem.ViewModel.EPOS
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;

    public class EposSettingsViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The client address line 1.
        /// </summary>
        private string addressLine1;

        /// <summary>
        /// The client address line 2.
        /// </summary>
        private string addressLine2;

        /// <summary>
        /// The client address line 3.
        /// </summary>
        private string addressLine3;

        /// <summary>
        /// The application price lists.
        /// </summary>
        private ObservableCollection<PriceList> priceLists;

        /// <summary>
        /// The selected price list.
        /// </summary>
        private PriceList selectedPriceList;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EposSettingsViewModel"/> class.
        /// </summary>
        public EposSettingsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration



            #endregion

            #region command handler

            // Gets the application price lists.
            this.OnLoadingCommand = new RelayCommand(() =>
            {
                this.GetPriceLists();
                this.AddressLine1 = Settings.Default.EposAddressLine1;
                this.AddressLine2 = Settings.Default.EposAddressLine2;
                this.AddressLine3 = Settings.Default.EposAddressLine3;
            });

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected price list.
        /// </summary>
        public PriceList SelectedPriceList
        {
            get
            {
                return this.selectedPriceList;
            }

            set
            {
                this.selectedPriceList = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the price lists.
        /// </summary>
        public ObservableCollection<PriceList> PriceLists
        {
            get
            {
                return this.priceLists;
            }

            set
            {
                this.priceLists = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the client address line 1.
        /// </summary>
        public string AddressLine1
        {
            get
            {
                return this.addressLine1;
            }

            set
            {
                this.addressLine1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the client address line 1.
        /// </summary>
        public string AddressLine2
        {
            get
            {
                return this.addressLine2;
            }

            set
            {
                this.addressLine2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the client address line 3.
        /// </summary>
        public string AddressLine3
        {
            get
            {
                return this.addressLine3;
            }

            set
            {
                this.addressLine3 = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        #endregion

        #region method



        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            Settings.Default.EposAddressLine1 = this.AddressLine1;
            Settings.Default.EposAddressLine2 = this.AddressLine2;
            Settings.Default.EposAddressLine3 = this.AddressLine3;

            if (this.selectedPriceList != null)
            {
                Settings.Default.BasePriceListId = this.selectedPriceList.PriceListID;
            }
           
            Settings.Default.Save();
            this.Close();
        }

        /// <summary>
        /// Closes the ui.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution



        #endregion

        #region helper

        /// <summary>
        /// Gets the price lists.
        /// </summary>
        private void GetPriceLists()
        {
            this.PriceLists = new ObservableCollection<PriceList>(this.DataManager.GetAllPriceLists());
            this.SelectedPriceList =
                this.PriceLists.FirstOrDefault(x => x.PriceListID == Settings.Default.BasePriceListId);
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseEposSettingsWindow);
        }

        #endregion

        #endregion
}
}
