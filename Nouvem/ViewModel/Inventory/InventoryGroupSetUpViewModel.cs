﻿// -----------------------------------------------------------------------
// <copyright file="InventoryGroupSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

namespace Nouvem.ViewModel.Inventory
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class InventoryGroupSetUpViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected group.
        /// </summary>
        private INGroup selectedGroup;

        /// <summary>
        /// The groups to update.
        /// </summary>
        private HashSet<INGroup> groupsToUpdate = new HashSet<INGroup>();

        /// <summary>
        /// The groups.
        /// </summary>
        private ObservableCollection<INGroup> inventoryGroups;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the InventoryGroupSetUpViewModel class.
        /// </summary>
        public InventoryGroupSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.SetControlMode(ControlMode.Update);
                if (this.SelectedGroup != null && !this.groupsToUpdate.Contains(this.selectedGroup))
                {
                    if (this.SelectedGroup.INGroupID == 0)
                    {
                        if (ApplicationSettings.SetDefaultGroupTemplates)
                        {
                            if (ApplicationSettings.TraceabilityDefaultTemplateId > 0)
                            {
                                this.SelectedGroup.TraceabilityTemplateNameID =
                                    ApplicationSettings.TraceabilityDefaultTemplateId;
                            }
                        }
                    }

                    this.groupsToUpdate.Add(this.SelectedGroup);
                }
            });

            #endregion

            #region instantiation

            this.InventoryGroups = new ObservableCollection<INGroup>();
            this.InventoryParentGroups = new ObservableCollection<INGroup>();

            #endregion
           
            this.GetInventoryGroups();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the inventory groups.
        /// </summary>
        public ObservableCollection<INGroup> InventoryGroups
        {
            get
            {
                return this.inventoryGroups;
            }

            set
            {
                this.inventoryGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the inventory parent groups.
        /// </summary>
        public ObservableCollection<INGroup> InventoryParentGroups { get; set; }
     
        /// <summary>
        /// Gets or sets the selected group.
        /// </summary>
        public INGroup SelectedGroup
        {
            get
            {
                return this.selectedGroup;
            }

            set
            {
                this.selectedGroup = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the inventory groups.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            #region validation

            if (this.SelectedGroup == null)
            {
                return;
            }

            if (this.InventoryGroups.Any(x => x.ParentInGroupID == this.SelectedGroup.INGroupID))
            {
                SystemMessage.Write(MessageType.Issue, Message.GroupIsParentGroup);
                return;
            }

            if (NouvemGlobal.InventoryItems.Any(x => x.Master.INGroupID == this.SelectedGroup.INGroupID))
            {
                SystemMessage.Write(MessageType.Issue, Message.GroupIsProductGroup);
                return;
            }

            #endregion

            this.SelectedGroup.Deleted = true;
            if (this.DataManager.AddOrUpdateInventoryGroups(new List<INGroup> { this.SelectedGroup }))
            {
                this.groupsToUpdate.Clear();
                SystemMessage.Write(MessageType.Priority, Message.GroupRemoved);
                this.GetInventoryGroups();
                this.UpdateInventoryGroups();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.GroupNotRemoved);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.AddOrUpdateInventoryGroups();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the inventory groups.
        /// </summary>
        private void AddOrUpdateInventoryGroups()
        {
            foreach (var inGroup in this.groupsToUpdate)
            {
                var checkForParentMatch = this.InventoryGroupAndParentMatch(inGroup);
                if (checkForParentMatch != string.Empty)
                {
                    SystemMessage.Write(MessageType.Issue, String.Format(Message.INGroupParentMatch, checkForParentMatch));
                    return;
                }
            }

            try
            {
                if (this.DataManager.AddOrUpdateInventoryGroups(this.groupsToUpdate.ToList()))
                {
                    this.groupsToUpdate.Clear();
                    SystemMessage.Write(MessageType.Priority, Message.INGroupsUpdated);
                    this.GetInventoryGroups();
                    this.UpdateInventoryGroups();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.INGroupsNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.INGroupsNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Determines if the selected inventory group is also the selected inventory parent group.
        /// </summary>
        /// <returns>Flag which determines if the selected inventory group is also the selected inventory parent group.</returns>
        private string InventoryGroupAndParentMatch(INGroup group)
        {
            if (group.INGroupID == group.ParentInGroupID)
            {
                return group.Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// Makes a call to inventory groups vm to update.
        /// </summary>
        private void UpdateInventoryGroups()
        {
            this.Locator.InventoryGroup.UpdateInventoryGroups();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearInventoryGroupSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseInventoryGroupSetUpWindow);
        }

        /// <summary>
        /// Gets the application inventory groups.
        /// </summary>
        private void GetInventoryGroups()
        {
            var groups = this.DataManager.GetInventoryGroups().OrderBy(x => x.INGroupID).ToList();
            this.InventoryParentGroups = new ObservableCollection<INGroup>(groups);
            this.InventoryGroups = new ObservableCollection<INGroup>(groups);
            this.InventoryGroups.Insert(0, new INGroup());
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            return true;
        }

        #endregion
    }
}
