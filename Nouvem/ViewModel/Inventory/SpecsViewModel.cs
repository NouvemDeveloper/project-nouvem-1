﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Win32;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Interface;

namespace Nouvem.ViewModel.Inventory
{
    public class SpecsViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// The selected inventory item.
        /// </summary>
        private InventoryItem selectedInventoryItem;

        /// <summary>
        /// The spec name.
        /// </summary>
        private string specName;

        /// <summary>
        /// The cutting spec.
        /// </summary>
        private string cuttingSpec;

        /// <summary>
        /// The packing spec.
        /// </summary>
        private string packingSpec;

        /// <summary>
        /// The item photo image.
        /// </summary>
        private byte[] itemPhoto;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool searchMode;

        /// <summary>
        /// The selected product specs.
        /// </summary>
        private ObservableCollection<ProductionSpecification> specs = new ObservableCollection<ProductionSpecification>();

        /// <summary>
        /// The selected product spec.
        /// </summary>
        private ProductionSpecification selectedSpec;

        /// <summary>
        /// The selected partner.
        /// </summary>
        private Model.BusinessObject.BusinessPartner selectedPartner;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecsViewModel"/> class.
        /// </summary>
        public SpecsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Sale>(this, Token.SearchSpecSelected, (s) =>
            {
                var localSpec = this.DataManager.GetsProductSpecificationById(s.SaleID);
                if (localSpec.INMasterSpecificationID > 0)
                {
                    this.SelectedInventoryItem =
                        this.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == localSpec.INMasterID);
                    this.SelectedSpec =
                        this.Specs.FirstOrDefault(x => x.INMasterSpecificationID == localSpec.INMasterSpecificationID);
                }
            });

            Messenger.Default.Register<ProductionSpecification>(this, Token.DrillDownToSpec, (s) =>
            {
                this.SetTouchscreenSpecification(s);
            });

            Messenger.Default.Register<Tuple<int?,int?>>(this, Token.DrillDownToSpec, this.SetSpecification);

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    this.ControlChange(x);
                });

            #endregion

            #region command handler

            this.OnClosingCommand = new RelayCommand(() =>
            {
                this.Close();
            });

            this.UpdateCommand = new RelayCommand(() =>
            {
                if (this.SelectedSpec != null)
                {
                    this.SetControlMode(ControlMode.Update);
                }
                else
                {
                    this.SetControlMode(ControlMode.Add);
                }
            });

            this.UploadImageCommand = new RelayCommand(this.UploadImageCommandExecute);

            #endregion

            this.ItemPhoto = new Uri("pack://application:,,,/Design/Image/NoImageAvailable.jpg").UriToBytes();
            this.GetPartners();
            this.GetInventoryItems();
            this.SearchMode = true;
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected partner.
        /// </summary>
        public Model.BusinessObject.BusinessPartner SelectedPartner
        {
            get
            {
                return this.selectedPartner;
            }

            set
            {
                this.selectedPartner = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in find mode.
        /// </summary>
        public ObservableCollection<ProductionSpecification> Specs
        {
            get
            {
                return this.specs;
            }

            set
            {
                this.specs = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected specification.
        /// </summary>
        public ProductionSpecification SelectedSpec
        {
            get
            {
                return this.selectedSpec;
            }

            set
            {
                this.selectedSpec = value;
                this.RaisePropertyChanged();
                this.HandleSelectedSpec();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in find mode.
        /// </summary>
        public bool SearchMode
        {
            get
            {
                return this.searchMode;
            }

            set
            {
                this.searchMode = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    // Find sale order mode, so set focus to inventory search combo box.
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedInventoryControl);
                }
            }
        }

        /// <summary>
        /// Gets or sets the set up item photo image.
        /// </summary>
        public byte[] ItemPhoto
        {
            get
            {
                return this.itemPhoto;
            }

            set
            {
                this.SetMode(value, this.itemPhoto);
                this.itemPhoto = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the spec name.
        /// </summary>
        public string SpecName
        {
            get
            {
                return this.specName;
            }

            set
            {
                this.SetMode(value, this.specName);
                this.specName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the cutting spec.
        /// </summary>
        public string CuttingSpec
        {
            get
            {
                return this.cuttingSpec;
            }

            set
            {
                this.SetMode(value, this.cuttingSpec);
                this.cuttingSpec = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the packing spec.
        /// </summary>
        public string PackingSpec
        {
            get
            {
                return this.packingSpec;
            }

            set
            {
                this.SetMode(value, this.packingSpec);
                this.packingSpec = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected inventory item.
        /// </summary>
        public InventoryItem SelectedInventoryItem
        {
            get
            {
                return this.selectedInventoryItem;
            }

            set
            {
                this.selectedInventoryItem = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleSelectedInventoryItem();
                }
            }
        }

        /// <summary>
        /// Gets or sets the inventory items.
        /// </summary>
        public IList<InventoryItem> InventoryItems { get; set; }

        /// <summary>
        /// Gets or sets the partners
        /// </summary>
        public IList<Model.BusinessObject.BusinessPartner> Partners { get; set; } =
            new List<Model.BusinessObject.BusinessPartner>();

        #endregion

        #region command

        /// <summary>
        /// Gets the command to upload an item image.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to upload an item image.
        /// </summary>
        public ICommand UploadImageCommand { get; private set; }

        /// <summary>
        /// Gets the command to upload an item image.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.SelectedSpec == null)
            {
                this.SelectedSpec = this.Specs.LastOrDefault();
                return;
            }

            var previousItem =
                this.Specs.TakeWhile(
                    item => item.INMasterSpecificationID != this.SelectedSpec.INMasterSpecificationID)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedSpec = previousItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.SelectedSpec == null)
            {
                this.SelectedSpec = this.Specs.FirstOrDefault();
                return;
            }

            var nextItem =
                this.Specs.SkipWhile(
                    item => item.INMasterSpecificationID != this.SelectedSpec.INMasterSpecificationID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedSpec = nextItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (this.SelectedSpec == null)
            {
                this.SelectedSpec = this.Specs.FirstOrDefault();
                return;
            }

            var firstItem = this.Specs.FirstOrDefault();

            if (firstItem != null)
            {
                this.SelectedSpec = firstItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (this.SelectedSpec == null)
            {
                this.SelectedSpec = this.Specs.LastOrDefault();
                return;
            }

            var lastItem = this.Specs.LastOrDefault();

            if (lastItem != null)
            {
                this.SelectedSpec = lastItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            var localSpec = this.DataManager.GetsProductSpecificationLastEdit();
            if (localSpec.INMasterSpecificationID > 0)
            {
                this.SelectedInventoryItem =
                    this.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == localSpec.INMasterID);
                this.SelectedSpec =
                    this.Specs.FirstOrDefault(x => x.INMasterSpecificationID == localSpec.INMasterSpecificationID);
            }
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            if (this.CurrentMode == ControlMode.Add)
            {
                this.SearchMode = false;
                this.ClearForm();
                this.SetControlMode(ControlMode.Add);
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
            }

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SearchMode = true;
                this.ClearForm();
                this.SetControlMode(ControlMode.Find);
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedInventoryControl);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddOrUpdateSpecification();
                    break;

                case ControlMode.Find:
                    Messenger.Default.Send(ViewType.SpecsSearch);
                    break;

                case ControlMode.Update:
                    this.AddOrUpdateSpecification();
                    break; 

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Determines if the current sale already exists in the db.
        /// </summary>
        /// <returns>A flag, as to whether the current sale already exists in the db.</returns>
        protected override bool CheckForEntity()
        {
            return this.selectedSpec != null;
        }
        
        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handler to upload an item image
        /// </summary>
        private void UploadImageCommandExecute()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "ALL FILES|*.*|PNG|*.png|BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|TIFF|*.tif;*.tiff";

            if (openFileDialog.ShowDialog() == true)
            {
                this.ItemPhoto = File.ReadAllBytes(openFileDialog.FileName);
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Sets the specification from an order screen drill down.
        /// </summary>
        /// <param name="data">The partner/product ids.</param>
        private void SetSpecification(Tuple<int?, int?> data)
        {
            var partnerId = data.Item1;
            var productId = data.Item2;
            if (partnerId.HasValue)
            {
                var localSpec = this.DataManager.GetsProductSpecificationByPartner(partnerId.ToInt(), productId.ToInt());
                if (localSpec != null && localSpec.INMasterSpecificationID > 0)
                {
                    this.SelectedInventoryItem =
                        this.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == localSpec.INMasterID);
                    this.SelectedSpec =
                        this.Specs.FirstOrDefault(x => x.INMasterSpecificationID == localSpec.INMasterSpecificationID);
                }
                else
                {
                    this.SelectedInventoryItem = this.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == productId);
                    this.SelectedPartner = this.Partners.FirstOrDefault(x => x.Details.BPMasterID == partnerId);
                    if (this.Specs.Count == 0)
                    {
                        this.SetControlMode(ControlMode.Add);
                        this.SearchMode = false;
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                        }));
                    }
                }
            }
            else
            {
                this.SelectedInventoryItem = this.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == productId);
            }
        }

        /// <summary>
        /// Sets the specification from an order screen drill down.
        /// </summary>
        /// <param name="spec">The spec data.</param>
        private void SetTouchscreenSpecification(ProductionSpecification spec)
        {
            this.CuttingSpec = spec.CuttingSpec;
            this.PackingSpec = spec.PackingSpec;
            this.ItemPhoto = spec.SpecImage;
        }

        private void AddOrUpdateSpecification()
        {
            #region validation

            if (string.IsNullOrWhiteSpace(this.SpecName))
            {
                SystemMessage.Write(MessageType.Issue, Message.NoSpecNameEntered);
                NouvemMessageBox.Show(Message.NoSpecNameEntered);
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                return;
            }

            #endregion

            var spec = new ProductionSpecification
            {
                INMasterSpecificationID = this.SelectedSpec != null ? this.SelectedSpec.INMasterSpecificationID : 0,
                Name = this.SpecName,
                INMasterID = this.SelectedInventoryItem.Master.INMasterID,
                CuttingSpec = this.CuttingSpec,
                PackingSpec = this.PackingSpec,
                SpecImage = this.ItemPhoto,
                Partners = this.Partners.Where(x => x.IsSelected).Select(x => x.Details.BPMasterID).ToList()
            };

            if (this.DataManager.AddOrUpdateProductSpecification(spec))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }
        }

        /// <summary>
        /// Handler for a selected product.
        /// </summary>
        private void HandleSelectedInventoryItem()
        {
            this.Specs = new ObservableCollection<ProductionSpecification>(this.DataManager.GetsProductSpecifications(this.SelectedInventoryItem.Master.INMasterID));
            if (this.CurrentMode == ControlMode.Add)
            {
                return;
            }

            this.SelectedSpec = this.Specs.FirstOrDefault();
        }

        /// <summary>
        /// Handler for a selected product spec.
        /// </summary>
        private void HandleSelectedSpec()
        {
            if (this.SelectedSpec == null)
            {
                this.ClearPartners();
                this.CuttingSpec = string.Empty;
                this.PackingSpec = string.Empty;
                this.SpecName = string.Empty;
                this.Specs.Clear();
                this.ItemPhoto = new Uri("pack://application:,,,/Design/Image/NoImageAvailable.jpg").UriToBytes();
                return;
            }

            this.SpecName = this.SelectedSpec.Name;
            this.PackingSpec = this.SelectedSpec.PackingSpec;
            this.CuttingSpec = this.SelectedSpec.CuttingSpec;
            this.ItemPhoto = this.SelectedSpec.SpecImage;
            this.ClearPartners();
            var localPartnersToTick = this.Partners.Where(x => this.SelectedSpec.Partners.Contains(x.Details.BPMasterID));
            foreach (var businessPartner in localPartnersToTick)
            {
                businessPartner.IsSelected = true;
            }
        }

        /// <summary>
        /// Gets the inventory items.
        /// </summary>
        private void GetInventoryItems()
        {
            if (ApplicationSettings.TouchScreenMode)
            {
                return;
            }

            var activeItems = NouvemGlobal.InventoryItems.Where(x => x.Master.Deleted == null && (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                                                     || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo));
            this.InventoryItems = activeItems.ToList();
        }

        /// <summary>
        /// Gets the spec partners.
        /// </summary>
        private void GetPartners()
        {
            if (ApplicationSettings.TouchScreenMode)
            {
                return;
            }

            foreach (var localPartner in NouvemGlobal.BusinessPartners)
            {
                localPartner.IsSelected = false;
                this.Partners.Add(localPartner);
            }
        }

        private void ClearForm()
        {
            this.ClearPartners();
            this.SelectedInventoryItem = null;
            this.CuttingSpec = string.Empty;
            this.PackingSpec = string.Empty;
            this.SpecName = string.Empty;
            this.Specs.Clear();
            this.SelectedSpec = null;
            this.ItemPhoto = new Uri("pack://application:,,,/Design/Image/NoImageAvailable.jpg").UriToBytes();
            this.SetControlMode(ControlMode.Find);
        }

        /// <summary>
        /// Unticks the partners.
        /// </summary>
        private void ClearPartners()
        {
            foreach (var businessPartner in this.Partners)
            {
                businessPartner.IsSelected = false;
            }
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Unregister(this);
            Messenger.Default.Send(Token.Message, Token.CloseSpecWindow);
        }

        #endregion

        #endregion
    }
}
