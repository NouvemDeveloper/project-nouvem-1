﻿// -----------------------------------------------------------------------
// <copyright file="InventoryGroupViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Editors;

namespace Nouvem.ViewModel.Inventory
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Interface;
    using Microsoft.Win32;

    public class InventoryMasterViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        #region master

        /// <summary>
        /// The selected method.
        /// </summary>
        private NouPriceMethod selectedPriceMethod;

        /// <summary>
        /// The selected method.
        /// </summary>
        private NouOrderMethod selectedOrderMethod;

        /// <summary>
        /// The print piece labels flag.
        /// </summary>
        private bool printPieceLabels;

        /// <summary>
        /// The print piece labels flag.
        /// </summary>
        private bool reprintLabelAtDispatch;

        /// <summary>
        /// active box checked flag.
        /// </summary>
        private bool checkBoxActive;

        /// <summary>
        /// inactive box checked flag.
        /// </summary>
        private bool checkBoxInactive;

        /// <summary>
        /// The selected inventory item.
        /// </summary>
        private InventoryItem selectedInventoryItem;

        /// <summary>
        /// The associated partner.
        /// </summary>
        private ViewBusinessPartner selectedPartner;

        /// <summary>
        /// The selected location.
        /// </summary>
        private Warehouse selectedStockLocation;

        /// <summary>
        /// The selected inventory item.
        /// </summary>
        private ObservableCollection<InventoryItem> inventoryItems;

        /// <summary>
        /// The product stock locations.
        /// </summary>
        private IList<Warehouse> stockLocations;

        /// <summary>
        /// The selected inventory item snapshot.
        /// </summary>
        private InventoryItem selectedInventoryItemSnapshot;

        /// <summary>
        /// The inventory item name.
        /// </summary>
        private string itemName;

        /// <summary>
        /// The inventory item code.
        /// </summary>
        private string itemCode;

        /// <summary>
        /// Is the item a stock item flag
        /// </summary>
        private bool isStockItem;

        /// <summary>
        /// Is the item a sales item flag
        /// </summary>
        private bool isSalesItem;

        /// <summary>
        /// Is the item a purchase  item flag
        /// </summary>
        private bool isPurchaseItem;

        /// <summary>
        /// Is the item a fixed asset flag
        /// </summary>
        private bool isFixedAsset;

        /// <summary>
        /// Is the item a production product.
        /// </summary>
        private bool isProductionProduct;

        /// <summary>
        /// Is the item a deduction product.
        /// </summary>
        private bool isDeductionProduct;

        /// <summary>
        /// Is the item a deduction product.
        /// </summary>
        private string deductionRate;

        /// <summary>
        /// Is the item a deduction product.
        /// </summary>
        private bool applyDeductionToDocket;

        /// <summary>
        /// Is the item a production product.
        /// </summary>
        private bool isReceipeProduct;

        /// <summary>
        /// The selected inventory group reference.
        /// </summary>
        private INGroup inventoryGroup;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool searchMode;

        /// <summary>
        /// The inventory groups reference.
        /// </summary>
        private ObservableCollection<INGroup> inventoryGroups;

        /// <summary>
        /// The item photo image.
        /// </summary>
        private byte[] itemPhoto;

        /// <summary>
        /// The date days.
        /// </summary>
        private IList<DateDay> dateDays = new List<DateDay>();

        #endregion

        #region traceability template

        /// <summary>
        /// Is the Traceability template to be inherited.
        /// </summary>
        private bool inheritTraceabilityTemplate;

        /// <summary>
        /// Is the Traceability template to be selected manually.
        /// </summary>
        private bool manualTraceabilityTemplate;

        /// <summary>
        /// The traceability allocated reference collection.
        /// </summary>
        private ObservableCollection<AttributeAllocationData> traceabilityTemplateAllocations;

        /// <summary>
        /// All the traceability allocated reference collection.
        /// </summary>
        private IList<AttributeAllocationData> allTraceabilityTemplateAllocations;

        /// <summary>
        /// The selected traceability template name.
        /// </summary>
        private AttributeTemplate selectedTraceabilityTemplateName;

        #endregion

        #region weight

        /// <summary>
        /// The selected scale.
        /// </summary>
        private CollectionData selectedLinkedScales;

        /// <summary>
        /// The nominal weight.
        /// </summary>
        private bool accumulateTares;

        /// <summary>
        /// The nominal weight.
        /// </summary>
        private string nominalWeight;

        /// <summary>
        /// The tne weight.
        /// </summary>
        private string tne;

        /// <summary>
        /// The t1 weight.
        /// </summary>
        private string t1Weight;

        /// <summary>
        /// The t2 weight.
        /// </summary>
        private string t2Weight;

        /// <summary>
        /// The min weight.
        /// </summary>
        private string minWeight;

        /// <summary>
        /// The max weight.
        /// </summary>
        private string maxWeight;

        /// <summary>
        /// The weight required flag.
        /// </summary>
        private bool? weightRequired;

        /// <summary>
        /// The selected box container type.
        /// </summary>
        private Container selectedBoxContainerType;

        /// <summary>
        /// The selected pieces container type.
        /// </summary>
        private Container selectedPiecesContainerType;

        /// <summary>
        /// The selected pallet container type.
        /// </summary>
        private Container selectedPalletContainerType;

        /// <summary>
        /// The selected linked product container type.
        /// </summary>
        private Container selectedLinkedProductContainerType;

        /// <summary>
        /// The containers reference.
        /// </summary>
        private ObservableCollection<Container> containers;

        #endregion

        #region attachment

        /// <summary>
        /// The selected inventory item attachments (including attachments marked for removal)
        /// </summary>
        private IList<INAttachment> allInventoryAttachments;

        /// <summary>
        /// The selected inventory item attachments.
        /// </summary>
        private ObservableCollection<INAttachment> inventoryAttachments;

        /// <summary>
        /// The selected inventory attachment.
        /// </summary>
        private INAttachment selectedAttachment;

        #endregion

        #region region remarks

        /// <summary>
        /// The inventory notes.
        /// </summary>
        private string inventoryNotes;

        #endregion

        #region general

        /// <summary>
        /// The price lists reference.
        /// </summary>
        private ObservableCollection<PriceList> priceLists;

        /// <summary>
        /// The active from date.
        /// </summary>
        private DateTime? activeFromDate;

        /// <summary>
        /// The active to date.
        /// </summary>
        private DateTime? activeToDate;

        /// <summary>
        /// The inactive from date.
        /// </summary>
        private DateTime? inactiveFromDate;

        /// <summary>
        /// The inactive from date.
        /// </summary>
        private DateTime? inactiveToDate;

        /// <summary>
        /// The typical pieces of stock associated with this item
        /// </summary>
        private int typicalPieces;

        /// <summary>
        /// The qty per box associated with this item
        /// </summary>
        private int qtyPerBox;

        /// <summary>
        /// The selected vat code reference.
        /// </summary>
        private VATCode selectedVatCode;

        /// <summary>
        /// The selected department reference.
        /// </summary>
        private Department selectedDepartment;

        /// <summary>
        /// The selected price list reference.
        /// </summary>
        private PriceList selectedPriceList;

        /// <summary>
        /// The selected price list item price.
        /// </summary>
        private decimal? priceListItemPrice;

        /// <summary>
        /// The stored selected price in NouvemGlobal.PriceListDetails.
        /// </summary>
        private PriceDetail selectedItemPrice;

        /// <summary>
        /// The application vat codes.
        /// </summary>
        public ObservableCollection<VATCode> vatCodes;

        #endregion

        #region label detail

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelTextName;

        /// <summary>
        /// The multi label text.
        /// </summary>
        private string labelMultiText1;

        /// <summary>
        /// The multi label text.
        /// </summary>
        private string labelMultiText2;

        /// <summary>
        /// The barcode text.
        /// </summary>
        private string barcode1;

        /// <summary>
        /// The barcode text.
        /// </summary>
        private string barcode2;

        /// <summary>
        /// The barcode text.
        /// </summary>
        private string barcode3;

        /// <summary>
        /// The barcode text.
        /// </summary>
        private string barcode4;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText1;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText2;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText3;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText4;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText5;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText6;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText7;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText8;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText9;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText10;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText11;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText12;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText13;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText14;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText15;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText16;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText17;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText18;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText19;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText20;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText21;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText22;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText23;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText24;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText25;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText26;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText27;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText28;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText29;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText30;

        #endregion

        #region inventory

        /// <summary>
        /// The show all stock flag.
        /// </summary>
        private bool showAll;

        /// <summary>
        /// The ability to change stock mode flag.
        /// </summary>
        private bool canChangeStockMode;

        /// <summary>
        /// The application stock modes.
        /// </summary>
        private ObservableCollection<NouStockMode> stockModes;

        /// <summary>
        /// The product stock data.
        /// </summary>
        private ObservableCollection<ProductStockData> productStockData;

        /// <summary>
        /// The product stock mode.
        /// </summary>
        private NouStockMode stockMode;

        /// <summary>
        /// The minimum product stock level.
        /// </summary>
        private decimal? minimumStockLevel;

        /// <summary>
        /// The po lead time in days.
        /// </summary>
        private int? orderLeadTime;

        /// <summary>
        /// The po to auto copy from.
        /// </summary>
        private int? purchaseOrderToCopyFrom;

        /// <summary>
        /// The pr order to auto copy from.
        /// </summary>
        private int? productionOrderToCopyFrom;

        /// <summary>
        /// The pr order to auto copy from.
        /// </summary>
        private string productionOrderReference;

        #endregion

        #region accounts

        /// <summary>
        /// The sales nominal code string.
        /// </summary>
        private string salesNominalCode;

        /// <summary>
        /// The sales dept nominal code string.
        /// </summary>
        private string salesNominalDeptCode;

        /// <summary>
        /// The purchase nominal code string.
        /// </summary>
        private string purchaseNominalCode;

        /// <summary>
        /// The purchase dept nominal code string.
        /// </summary>
        private string purchaseNominalDeptCode;

        #endregion

        #region properties

        /// <summary>
        /// The properties collection reference.
        /// </summary>
        private ObservableCollection<InventoryProperty> inventoryProperties;

        /// <summary>
        /// The selected inventory property.
        /// </summary>
        private InventoryProperty selectedProperty;

        #endregion

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the InventoryGroupViewModel class.
        /// </summary>
        public InventoryMasterViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<Sale>(this, Token.ProductionSelected, x =>
                {
                    if (!this.DataManager.IsProductRecipeProduct(this.selectedInventoryItem.Master.INMasterID, x.SaleID))
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(string.Format(Message.ProductNotRecipeProduct, this.selectedInventoryItem.Master.Name, x.Reference));
                        }));

                        return;
                    }

                    this.ProductionOrderToCopyFrom = x.Number;
                    this.ProductionOrderReference = x.Reference;
                });

            Messenger.Default.Register<Sale>(this, Token.SearchSaleSelected, x =>
            {
                if (!this.DataManager.IsProductOnPurchaseOrder(this.selectedInventoryItem.Master.INMasterID, x.SaleID))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(string.Format(Message.ProductNotOnPurchaseOrder,this.selectedInventoryItem.Master.Name, x.Number), NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            Messenger.Default.Send(ViewType.APOrder);
                            var poOrder = this.DataManager.GetAPOrderByNumber(x.Number);
                            this.Locator.APOrder.Sale = poOrder;
                        }
                    }));

                    return;
                }

                this.PurchaseOrderToCopyFrom = x.Number;
            });

            Messenger.Default.Register<string>(this, Token.CloseSearchSaleWindow, x => ViewModelLocator.ClearAPOrder());

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    this.ControlChange(x);
                    //// Only change control if the BPMasterWindowView is the active window.
                    //if (this.IsFormActive)
                    //{

                    //}
                });

            Messenger.Default.Register<string>(this, Token.RefreshInventory, s => this.RefreshCollections());

            #endregion

            #region command registration

            this.DrillDownToProductionOrderCommand = new RelayCommand(() =>
            {
                if (this.selectedInventoryItem == null || this.selectedInventoryItem.Master.INMasterID == 0)
                {
                    return;
                }

                if (this.ProductionOrderToCopyFrom.IsNullOrZero())
                {
                    return;
                }

                Messenger.Default.Send(ViewType.BatchSetUp);
                Messenger.Default.Send(this.ProductionOrderToCopyFrom.ToInt(), Token.ProductionSelected);
            });

            this.DrillDownToProductionOrdersCommand = new RelayCommand(() =>
            {
                if (this.selectedInventoryItem == null || this.selectedInventoryItem.Master.INMasterID == 0)
                {
                    return;
                }

                if (this.selectedInventoryItem.Master.ReceipeProduct.IsNullOrFalse())
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.ProductIsIngredientProduct, this.selectedInventoryItem.Master.Name));
                    return;
                }

                Messenger.Default.Send(ViewType.BatchEdit);
                this.Locator.BatchEdit.SearchMode = true;
                this.Locator.BatchEdit.SelectedSale =
                    this.Locator.BatchEdit.FilteredSales.FirstOrDefault(x =>
                        x.Number == this.ProductionOrderToCopyFrom);
            });

            this.DrillDownToPurchaseOrderCommand = new RelayCommand(() =>
            {
                if (this.selectedInventoryItem == null || this.selectedInventoryItem.Master.INMasterID == 0)
                {
                    return;
                }

                if (this.PurchaseOrderToCopyFrom.IsNullOrZero())
                {
                    return;
                }

                Messenger.Default.Send(ViewType.APOrder);
                var poOrder = this.DataManager.GetAPOrderByNumber(this.PurchaseOrderToCopyFrom.ToInt());
                this.Locator.APOrder.Sale = poOrder;
            });

            this.DrillDownToPurchaseOrdersCommand = new RelayCommand(() =>
            {
                if (this.selectedInventoryItem == null || this.selectedInventoryItem.Master.INMasterID == 0)
                {
                    return;
                }

                if (this.selectedInventoryItem.Master.ReceipeProduct.IsTrue())
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.ProductIsRecipeProduct, this.selectedInventoryItem.Master.Name));
                    return;
                }

                var orderStatuses = new List<int?>
                {
                    NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                    NouvemGlobal.NouDocStatusActiveEdit.NouDocStatusID,
                    NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                    NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                    NouvemGlobal.NouDocStatusCancelled.NouDocStatusID,
                    NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                    NouvemGlobal.NouDocStatusExported.NouDocStatusID,
                };

                var poOrders = this.DataManager.GetAllAPOrders(orderStatuses, ApplicationSettings.SalesSearchFromDate, DateTime.Today);
                Messenger.Default.Send(Tuple.Create(poOrders, ViewType.APOrder), Token.SearchForSale);
                this.Locator.SalesSearchData.INMasterMode = true;
                this.Locator.SalesSearchData.SetView(ViewType.APOrder);
                this.Locator.SalesSearchData.SelectedSale =
                    this.Locator.SalesSearchData.FilteredSales.FirstOrDefault(x =>
                        x.Number == this.PurchaseOrderToCopyFrom);
            });

            // Drill down to the tare set up screen.
            this.DrillDownToTareCommand = new RelayCommand<string>(this.DrillDownToTareCommandExecute);

            // Command to handle a partner name edit.
            this.ItemNameEditCommand = new RelayCommand<ProcessNewValueEventArgs>(this.HandleNameEdit);

            // handle the price list price change.
            this.PriceChangedCommand = new RelayCommand(this.PriceChangedCommandExecute);

            this.PriceItemPriceFocusCommand = new RelayCommand(() =>
            {
                if (this.PriceListItemPrice == null)
                {
                    if (this.selectedInventoryItem == null || this.selectedInventoryItem.Master.INMasterID == 0 || this.selectedPriceList == null)
                    {
                        return;
                    }

                    // ask the user if they want to add the product to the price list
                    NouvemMessageBox.Show(Message.AddProductToPriceLIstConfirmation, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        Messenger.Default.Send(ViewType.PriceListDetail);
                        this.Locator.PriceListDetail.AddProductToPriceListDetail(this.SelectedPriceList.PriceListID, this.selectedInventoryItem.Master);
                    }
                }
            });

            // Handle the user search
            //this.FindItemCommand = new RelayCommand<KeyEventArgs>(e => this.FindItemCommandExecute(), e => e.Key == Key.F1);

            // Handle the user search
            this.FindProductCommand = new RelayCommand(this.FindItemCommandExecute);

            // Command handler to select/deselect the selected partner properties.
            this.SelectAllPropertiesCommand = new RelayCommand<string>(this.SelectAllPropertiesCommandExecute);

            // Command handler for a property selection.
            this.PropertySelectedCommand = new RelayCommand(this.PropertySelectedCommandExecute);

            // Command handler to add an attachment to the db.
            this.AttachFileCommand = new RelayCommand(this.AttachFileCommandExecute);

            // Command to handle the pricing drill down.
            this.DrillDownToPricingCommand = new RelayCommand(this.DrillDownToPricingCommandExecute);

            // Command to handle the warehouse down.
            this.DrillDownToWarehouseCommand = new RelayCommand(this.DrillDownToWarehouseCommandExecute);

            // Command to handle the actual price drill down.
            this.DrillDownToActualPriceCommand = new RelayCommand(this.DrillDownToActualPriceCommandExecute);

            // Command to handle the vat drill down.
            this.DrillDownToVatCommand = new RelayCommand(() => Messenger.Default.Send(Tuple.Create(ViewType.VatSetUp, 0), Token.DrillDown));

            // Command to handle a dates days change.
            this.UpdateCommand = new RelayCommand(() =>
            {
                if (this.selectedInventoryItem != null && this.selectedInventoryItem.Master.INMasterID == 0)
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            });

            // Command handler to upload an item image.
            this.UploadImageCommand = new RelayCommand(() =>
            {
                if (this.selectedInventoryItem.Master.INMasterID == 0)
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }

                this.UploadImageCommandExecute();
            });

            // Command handler to display an attachment.
            this.DisplayFileCommand = new RelayCommand(this.DisplayFileCommandExecute);

            // Command handler to delete an attachment from the db.
            this.DeleteFileCommand = new RelayCommand(this.DeleteFileCommandExecute);

            // Specify the products for the list.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Specify the products for the list.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion

            #region instantiation

            this.InventoryGroups = new ObservableCollection<INGroup>();
            this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplate>();
            this.TraceabilityTemplateAllocations = new ObservableCollection<AttributeAllocationData>();
            this.allTraceabilityTemplateAllocations = new List<AttributeAllocationData>();
            this.InventoryAttachments = new ObservableCollection<INAttachment>();
            this.allInventoryAttachments = new List<INAttachment>();
            this.Containers = new ObservableCollection<Container>();
            this.PriceLists = new ObservableCollection<PriceList>();
            this.INProperties = new List<INProperty>();
            this.InventoryProperties = new ObservableCollection<InventoryProperty>();
            this.stockModes = new ObservableCollection<NouStockMode>();
            this.productStockData = new ObservableCollection<ProductStockData>();

            #endregion

            this.GetPriceMethods();
            this.GetOrderMethods();
            this.GetPartners();
            this.GetScales();
            this.CheckForNewEntities(EntityType.INMaster);
            this.GetStockLocations();
            this.GetInventoryItems();
            this.GetInventoryGroups();
            this.GetStockModes();
            this.GetTraceabilityTemplateNames();
            this.GetTraceabilityTemplateAllocations();
            this.GetContainers();
            this.GetVatCodes();
            this.GetDepartments();
            this.GetPriceLists();
            this.GetProperties();
            this.CreateBlankInventoryItem();
            this.SetControlMode(ControlMode.Find);
            this.SetLabelNames();
        }

        #endregion

        #region public interface

        #region property

        #region master

        /// <summary>
        /// Get or sets the price methods.
        /// </summary>
        public ObservableCollection<NouPriceMethod> PriceMethods { get; set; }

        /// <summary>
        /// The selected price method.
        /// </summary>
        public NouPriceMethod SelectedPriceMethod
        {
            get
            {
                return this.selectedPriceMethod;
            }

            set
            {
                this.SetMode(value, this.selectedPriceMethod);
                this.selectedPriceMethod = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The selected order method.
        /// </summary>
        public NouOrderMethod SelectedOrderMethod
        {
            get
            {
                return this.selectedOrderMethod;
            }

            set
            {
                this.SetMode(value, this.selectedOrderMethod);
                this.selectedOrderMethod = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// The associated partners.
        /// </summary>
        public IList<ViewBusinessPartner> BusinessPartners { get; set; }

        /// <summary>
        /// The selected product stock location.
        /// </summary>
        public Warehouse SelectedStockLocation
        {
            get
            {
                return this.selectedStockLocation;
            }

            set
            {
                this.SetMode(value, this.selectedStockLocation);
                this.selectedStockLocation = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock locations.
        /// </summary>
        public ViewBusinessPartner SelectedPartner
        {
            get
            {
                return this.selectedPartner;
            }

            set
            {
                this.SetMode(value, this.selectedPartner);
                this.selectedPartner = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock locations.
        /// </summary>
        public IList<Warehouse> StockLocations
        {
            get
            {
                return this.stockLocations;
            }

            set
            {
                this.stockLocations = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner code.
        /// </summary>
        public bool PrintPieceLabels
        {
            get
            {
                return this.printPieceLabels;
            }

            set
            {
                this.SetMode(value, this.printPieceLabels);
                this.printPieceLabels = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner code.
        /// </summary>
        public bool ReprintLabelAtDispatch
        {
            get
            {
                return this.reprintLabelAtDispatch;
            }

            set
            {
                this.SetMode(value, this.reprintLabelAtDispatch);
                this.reprintLabelAtDispatch = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the inactive chaeck box is checked.
        /// </summary>
        public bool CheckBoxInactive
        {
            get
            {
                return this.checkBoxInactive;
            }

            set
            {
                this.SetMode(value, this.checkBoxInactive);
                this.checkBoxInactive = value;
                this.RaisePropertyChanged();

                if (value && this.SelectedInventoryItem != null && !this.InactiveFromDate.HasValue &&
                    !this.InactiveToDate.HasValue)
                {
                    this.InactiveFromDate = DateTime.Today;
                    this.InactiveToDate = DateTime.Today.AddYears(100);
                }
                else if (!value)
                {
                    this.InactiveFromDate = null;
                    this.InactiveToDate = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the active chaeck box is checked.
        /// </summary>
        public bool CheckBoxActive
        {
            get
            {
                return this.checkBoxActive;
            }

            set
            {
                this.SetMode(value, this.checkBoxActive);
                this.checkBoxActive = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Get or sets the selected inventory group.
        /// </summary>
        public INGroup InventoryGroup
        {
            get
            {
                return this.inventoryGroup;
            }

            set
            {
                if (value == null || value.Name.Equals(Strings.NoneSelected))
                {
                    this.ManualTraceabilityTemplate = true;
                }
                else
                {
                    this.InheritTraceabilityTemplate = true;
                    this.SetMode(value, this.InventoryGroup);
                }

                this.inventoryGroup = value;
                this.RaisePropertyChanged();
                this.DisplayGroupTemplates();
            }
        }

        /// <summary>
        /// Get or sets the inventory item name.
        /// </summary>
        public string ItemName
        {
            get
            {
                return this.itemName;
            }

            set
            {
                this.SetMode(value, this.ItemName);
                this.itemName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the inventory item code.
        /// </summary>
        public string ItemCode
        {
            get
            {
                return this.itemCode;
            }

            set
            {
                this.SetMode(value, this.ItemCode);
                this.itemCode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets a value indicating whether the item is a stock item.
        /// </summary>
        public bool IsStockItem
        {
            get
            {
                return this.isStockItem;
            }

            set
            {
                if (value && this.IsFixedAsset)
                {
                    // a fixed asset cannot be a stock or sales item
                    SystemMessage.Write(MessageType.Issue, Message.FixedAssetNotApplicable);
                    this.IsStockItem = false;
                    return;
                }

                this.SetMode(value, this.IsStockItem);
                this.isStockItem = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets a value indicating whether the item is a sales item.
        /// </summary>
        public bool IsSalesItem
        {
            get
            {
                return this.isSalesItem;
            }

            set
            {
                if (value && this.IsFixedAsset)
                {
                    // a fixed asset cannot be a stock or sales item
                    SystemMessage.Write(MessageType.Issue, Message.FixedAssetNotApplicable);
                    this.IsSalesItem = false;
                    return;
                }

                this.SetMode(value, this.IsSalesItem);
                this.isSalesItem = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets a value indicating whether the item is a purchase item.
        /// </summary>
        public bool IsPurchaseItem
        {
            get
            {
                return this.isPurchaseItem;
            }

            set
            {
                this.SetMode(value, this.IsPurchaseItem);
                this.isPurchaseItem = value;
                this.RaisePropertyChanged();
            }
        }


        /// <summary>
        /// Get or sets a value indicating whether the item is a production item.
        /// </summary>
        public bool IsProductionProduct
        {
            get
            {
                return this.isProductionProduct;
            }

            set
            {
                if (value && this.IsFixedAsset)
                {
                    // a fixed asset cannot be a stock or sales item
                    SystemMessage.Write(MessageType.Issue, Message.FixedAssetNotApplicable);
                    this.IsProductionProduct = false;
                    return;
                }

                this.SetMode(value, this.IsProductionProduct);
                this.isProductionProduct = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets a value indicating whether the item is a deduction item.
        /// </summary>
        public bool IsDeductionProduct
        {
            get
            {
                return this.isDeductionProduct;
            }

            set
            {
                this.SetMode(value, this.isDeductionProduct);
                this.isDeductionProduct = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Get or sets a value indicating whether the item is a deduction item.
        /// </summary>
        public string DeductionRate
        {
            get
            {
                return this.deductionRate;
            }

            set
            {
                if (!string.IsNullOrEmpty(value) && value.Length > 0)
                {
                    if (!value.IsDecimalSequence())
                    {
                        value = value.Substring(0, value.Length - 1);
                    }
                }

                this.SetMode(value, this.deductionRate);
                this.deductionRate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets a value indicating whether the item is a production item.
        /// </summary>
        public bool ApplyDeductionToDocket
        {
            get
            {
                return this.applyDeductionToDocket;
            }

            set
            {
                this.SetMode(value, this.applyDeductionToDocket);
                this.applyDeductionToDocket = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets a value indicating whether the item is a production item.
        /// </summary>
        public bool IsReceipeProduct
        {
            get
            {
                return this.isReceipeProduct;
            }

            set
            {
                this.SetMode(value, this.IsReceipeProduct);
                this.isReceipeProduct = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets a value indicating whether the item is a fixed asset.
        /// </summary>
        public bool IsFixedAsset
        {
            get
            {
                return this.isFixedAsset;
            }

            set
            {
                if (value && (this.IsStockItem || this.isSalesItem || this.isProductionProduct))
                {
                    // a fixed asset cannot be a stock or sales item
                    SystemMessage.Write(MessageType.Issue, Message.FixedAssetNotApplicable);
                    this.IsFixedAsset = false;
                    return;
                }

                this.SetMode(value, this.IsFixedAsset);
                this.isFixedAsset = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in find mode.
        /// </summary>
        public bool SearchMode
        {
            get
            {
                return this.searchMode;
            }

            set
            {
                this.searchMode = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    // Find sale order mode, so set focus to inventory search combo box.
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedInventoryControl);
                }
            }
        }

        /// <summary>
        /// Get or sets the inventory groups.
        /// </summary>
        public ObservableCollection<INGroup> InventoryGroups
        {
            get
            {
                return this.inventoryGroups;
            }

            set
            {
                this.inventoryGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected inventory item.
        /// </summary>
        public InventoryItem SelectedInventoryItem
        {
            get
            {
                return this.selectedInventoryItem;
            }

            set
            {
                this.selectedInventoryItem = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    if (this.InventoryItems != null && !this.InventoryItems.Contains(value))
                    {
                        this.InventoryItems.Add(value);
                    }

                    this.selectedInventoryItemSnapshot = value.Copy;
                    this.HandleSelectedInventoryItem();
                }
            }
        }

        /// <summary>
        /// Gets or sets the inventory items.
        /// </summary>
        public ObservableCollection<InventoryItem> InventoryItems
        {
            get
            {
                return this.inventoryItems;
            }

            set
            {
                this.inventoryItems = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region traceability template

        /// <summary>
        /// Get or sets a value indicating whether a traceability template is inherited from the item group.
        /// </summary>
        public bool InheritTraceabilityTemplate
        {
            get
            {
                return this.inheritTraceabilityTemplate;
            }

            set
            {
                this.SetMode(value, this.inheritTraceabilityTemplate);
                this.inheritTraceabilityTemplate = value;
                this.RaisePropertyChanged();
                if (!this.EntitySelectionChange && value && this.InventoryGroup != null)
                {
                    this.SelectedTraceabilityTemplateName =
                        this.TraceabilityTemplateNames.FirstOrDefault(
                            x =>
                                x.AttributeTemplateID ==
                                this.InventoryGroup.AttributeTemplateID);
                }
            }
        }

        /// <summary>
        /// Get or sets a value indicating whether a Traceability template is to be manually selected.
        /// </summary>
        public bool ManualTraceabilityTemplate
        {
            get
            {
                return this.manualTraceabilityTemplate;
            }

            set
            {
                this.SetMode(value, this.manualTraceabilityTemplate);
                this.manualTraceabilityTemplate = value;
                this.RaisePropertyChanged();
                if (!this.EntitySelectionChange && value && this.selectedInventoryItem?.TraceabilityTemplate != null)
                {
                    this.SelectedTraceabilityTemplateName =
                        this.TraceabilityTemplateNames.FirstOrDefault(
                            x =>
                                x.AttributeTemplateID ==
                                this.selectedInventoryItem.TraceabilityTemplate.AttributeTemplateID);
                }
            }
        }

        /// <summary>
        /// Get or sets the traceability template allocations.
        /// </summary>
        public ObservableCollection<AttributeAllocationData> TraceabilityTemplateAllocations
        {
            get
            {
                return this.traceabilityTemplateAllocations;
            }

            set
            {
                this.traceabilityTemplateAllocations = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the traceability template names.
        /// </summary>
        public ObservableCollection<AttributeTemplate> TraceabilityTemplateNames { get; set; }

        /// <summary>
        /// Get or sets the selected traceability template name.
        /// </summary>
        public AttributeTemplate SelectedTraceabilityTemplateName
        {
            get
            {
                return this.selectedTraceabilityTemplateName;
            }

            set
            {
                this.SetMode(value, this.SelectedTraceabilityTemplateName);
                this.selectedTraceabilityTemplateName = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.SetTraceabilityAllocations();

                    //if (this.IsFormLoaded)
                    //{
                    //    this.SetControlMode(ControlMode.Update);
                    //}
                }
            }
        }

        #endregion

        #region weight

        /// <summary>
        /// Get or sets the linked scales.
        /// </summary>
        public bool AccumulateTares
        {
            get
            {
                return this.accumulateTares;
            }

            set
            {
                this.SetMode(value, this.accumulateTares);
                this.accumulateTares = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the linked scales.
        /// </summary>
        public CollectionData SelectedLinkedScales
        {
            get
            {
                return this.selectedLinkedScales;
            }

            set
            {
                this.SetMode(value, this.selectedLinkedScales);
                this.selectedLinkedScales = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the linked scales.
        /// </summary>
        public IList<CollectionData> Scales { get; set; }

        /// <summary>
        /// Get or sets a value indicating whether a weight is required for transactions for this product.
        /// </summary>
        public bool? WeightRequired
        {
            get
            {
                return this.weightRequired;
            }

            set
            {
                this.SetMode(value, this.weightRequired);
                this.weightRequired = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the nominal weght.
        /// </summary>
        public string NominalWeight
        {
            get
            {
                return this.nominalWeight;
            }

            set
            {
                this.SetMode(value, this.NominalWeight);
                this.nominalWeight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the tne.
        /// </summary>
        public string TNE
        {
            get
            {
                return this.tne;
            }

            set
            {
                this.SetMode(value, this.TNE);
                this.tne = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the t1 weght.
        /// </summary>
        public string T1Weight
        {
            get
            {
                return this.t1Weight;
            }

            set
            {
                this.SetMode(value, this.T1Weight);
                this.t1Weight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the t2 weght.
        /// </summary>
        public string T2Weight
        {
            get
            {
                return this.t2Weight;
            }

            set
            {
                this.SetMode(value, this.T2Weight);
                this.t2Weight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the min weght.
        /// </summary>
        public string MinWeight
        {
            get
            {
                return this.minWeight;
            }

            set
            {
                this.SetMode(value, this.MinWeight);
                this.minWeight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the max weght.
        /// </summary>
        public string MaxWeight
        {
            get
            {
                return this.maxWeight;
            }

            set
            {
                this.SetMode(value, this.MaxWeight);
                this.maxWeight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application containers.
        /// </summary>
        public ObservableCollection<Container> Containers
        {
            get
            {
                return this.containers;
            }

            set
            {
                this.containers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the selected box container type.
        /// </summary>
        public Container SelectedBoxContainerType
        {
            get
            {
                return this.selectedBoxContainerType;
            }

            set
            {
                this.SetMode(value, this.SelectedBoxContainerType);
                this.selectedBoxContainerType = value;
                this.RaisePropertyChanged();

                if (value != null && value.Name.Equals(Strings.DefineNew) && !this.EntitySelectionChange)
                {
                    // open the container set up window
                    Messenger.Default.Send(ViewType.ContainerSetUp);
                }
            }
        }

        /// <summary>
        /// Get or sets the selected pieces container type.
        /// </summary>
        public Container SelectedPiecesContainerType
        {
            get
            {
                return this.selectedPiecesContainerType;
            }

            set
            {
                this.SetMode(value, this.SelectedPiecesContainerType);
                this.selectedPiecesContainerType = value;
                this.RaisePropertyChanged();

                if (value != null && value.Name.Equals(Strings.DefineNew) && !this.EntitySelectionChange)
                {
                    // open the container set up window
                    Messenger.Default.Send(ViewType.ContainerSetUp);
                }
            }
        }

        /// <summary>
        /// Get or sets the selected pallet container type.
        /// </summary>
        public Container SelectedPalletContainerType
        {
            get
            {
                return this.selectedPalletContainerType;
            }

            set
            {
                this.SetMode(value, this.SelectedPalletContainerType);
                this.selectedPalletContainerType = value;
                this.RaisePropertyChanged();

                if (value != null && value.Name.Equals(Strings.DefineNew) && !this.EntitySelectionChange)
                {
                    // open the container set up window
                    Messenger.Default.Send(ViewType.ContainerSetUp);
                }
            }
        }

        /// <summary>
        /// Get or sets the selected pallet container type.
        /// </summary>
        public Container SelectedLinkedProductContainerType
        {
            get
            {
                return this.selectedLinkedProductContainerType;
            }

            set
            {
                this.SetMode(value, this.selectedLinkedProductContainerType);
                this.selectedLinkedProductContainerType = value;
                this.RaisePropertyChanged();

                if (value != null && value.Name.Equals(Strings.DefineNew) && !this.EntitySelectionChange)
                {
                    // open the container set up window
                    Messenger.Default.Send(ViewType.ContainerSetUp);
                }
            }
        }

        #endregion

        #region general

        /// <summary>
        /// Gets or sets the set up item photo image.
        /// </summary>
        public byte[] ItemPhoto
        {
            get
            {
                return this.itemPhoto;
            }

            set
            {
                this.itemPhoto = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the selected vat code.
        /// </summary>
        public VATCode SelectedVATCode
        {
            get
            {
                return this.selectedVatCode;
            }

            set
            {
                this.SetMode(value, this.SelectedVATCode);
                this.selectedVatCode = value;
                this.RaisePropertyChanged();

                if (value != null && !this.EntitySelectionChange && value.Code.Equals(Strings.DefineNew))
                {
                    // shortcut to the vat set up screen
                    Messenger.Default.Send(ViewType.VatSetUp);
                }
            }
        }

        /// <summary>
        /// Get or sets the typical pieces count.
        /// </summary>
        public int TypicalPieces
        {
            get
            {
                return this.typicalPieces;
            }

            set
            {
                this.SetMode(value, this.TypicalPieces);
                this.typicalPieces = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the qty per box.
        /// </summary>
        public int QtyPerBox
        {
            get
            {
                return this.qtyPerBox;
            }

            set
            {
                this.SetMode(value, this.QtyPerBox);
                this.qtyPerBox = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application vat codes.
        /// </summary>
        public ObservableCollection<VATCode> VatCodes
        {
            get
            {
                return this.vatCodes;
            }

            set
            {
                this.vatCodes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application departments.
        /// </summary>
        public ObservableCollection<Department> Departments { get; set; }

        /// <summary>
        /// Get or sets the selected department.
        /// </summary>
        public Department SelectedDepartment
        {
            get
            {
                return this.selectedDepartment;
            }

            set
            {
                this.SetMode(value, this.SelectedDepartment);
                this.selectedDepartment = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application price lists.
        /// </summary>
        public ObservableCollection<PriceList> PriceLists
        {
            get
            {
                return this.priceLists;
            }

            set
            {
                this.priceLists = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the selected price list.
        /// </summary>
        public PriceList SelectedPriceList
        {
            get
            {
                return this.selectedPriceList;
            }

            set
            {
                this.selectedPriceList = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleSelectedPriceList();
                }
            }
        }

        /// <summary>
        /// Get or sets the price list item price.
        /// </summary>
        public decimal? PriceListItemPrice
        {
            get
            {
                return this.priceListItemPrice;
            }

            set
            {
                this.SetMode(value, this.priceListItemPrice);
                this.priceListItemPrice = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the active from date.
        /// </summary>
        public DateTime? ActiveFromDate
        {
            get
            {
                return this.activeFromDate;
            }

            set
            {
                this.SetMode(value, this.activeFromDate);
                this.activeFromDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the active to date.
        /// </summary>
        public DateTime? ActiveToDate
        {
            get
            {
                return this.activeToDate;
            }

            set
            {
                this.SetMode(value, this.activeToDate);
                this.activeToDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner inactive from date.
        /// </summary>
        public DateTime? InactiveFromDate
        {
            get
            {
                return this.inactiveFromDate;
            }

            set
            {
                this.SetMode(value, this.inactiveFromDate);
                this.inactiveFromDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the iinactive to date.
        /// </summary>
        public DateTime? InactiveToDate
        {
            get
            {
                return this.inactiveToDate;
            }

            set
            {
                this.SetMode(value, this.inactiveToDate);
                this.inactiveToDate = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region label detail

        /// <summary>
        /// Gets or sets the static label text name.
        /// </summary>
        public string LabelTextName
        {
            get
            {
                return this.labelTextName;
            }

            set
            {
                this.labelTextName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the bacode text.
        /// </summary>
        public string Barcode1
        {
            get
            {
                return this.barcode1;
            }

            set
            {
                this.SetMode(value, this.barcode1);
                this.barcode1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the bacode text.
        /// </summary>
        public string Barcode2
        {
            get
            {
                return this.barcode2;
            }

            set
            {
                this.SetMode(value, this.barcode2);
                this.barcode2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the bacode text.
        /// </summary>
        public string Barcode3
        {
            get
            {
                return this.barcode3;
            }

            set
            {
                this.SetMode(value, this.barcode3);
                this.barcode3 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the bacode text.
        /// </summary>
        public string Barcode4
        {
            get
            {
                return this.barcode4;
            }

            set
            {
                this.SetMode(value, this.barcode4);
                this.barcode4 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelMultiText1
        {
            get
            {
                return this.labelMultiText1;
            }

            set
            {
                this.SetMode(value, this.labelMultiText1);
                this.labelMultiText1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelMultiText2
        {
            get
            {
                return this.labelMultiText2;
            }

            set
            {
                this.SetMode(value, this.labelMultiText2);
                this.labelMultiText2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText1
        {
            get
            {
                return this.labelText1;
            }

            set
            {
                this.SetMode(value, this.labelText1);
                this.labelText1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText2
        {
            get
            {
                return this.labelText2;
            }

            set
            {
                this.SetMode(value, this.labelText2);
                this.labelText2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText3
        {
            get
            {
                return this.labelText3;
            }

            set
            {
                this.SetMode(value, this.labelText3);
                this.labelText3 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText4
        {
            get
            {
                return this.labelText4;
            }

            set
            {
                this.SetMode(value, this.labelText4);
                this.labelText4 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText5
        {
            get
            {
                return this.labelText5;
            }

            set
            {
                this.SetMode(value, this.labelText5);
                this.labelText5 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText6
        {
            get
            {
                return this.labelText6;
            }

            set
            {
                this.SetMode(value, this.labelText6);
                this.labelText6 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText7
        {
            get
            {
                return this.labelText7;
            }

            set
            {
                this.SetMode(value, this.labelText7);
                this.labelText7 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText8
        {
            get
            {
                return this.labelText8;
            }

            set
            {
                this.SetMode(value, this.labelText8);
                this.labelText8 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText9
        {
            get
            {
                return this.labelText9;
            }

            set
            {
                this.SetMode(value, this.labelText9);
                this.labelText9 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText10
        {
            get
            {
                return this.labelText10;
            }

            set
            {
                this.SetMode(value, this.labelText10);
                this.labelText10 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText11
        {
            get
            {
                return this.labelText11;
            }

            set
            {
                this.SetMode(value, this.labelText11);
                this.labelText11 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText12
        {
            get
            {
                return this.labelText12;
            }

            set
            {
                this.SetMode(value, this.labelText12);
                this.labelText12 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText13
        {
            get
            {
                return this.labelText13;
            }

            set
            {
                this.SetMode(value, this.labelText13);
                this.labelText13 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText14
        {
            get
            {
                return this.labelText14;
            }

            set
            {
                this.SetMode(value, this.labelText14);
                this.labelText14 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText15
        {
            get
            {
                return this.labelText15;
            }

            set
            {
                this.SetMode(value, this.labelText15);
                this.labelText15 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText16
        {
            get
            {
                return this.labelText16;
            }

            set
            {
                this.SetMode(value, this.labelText16);
                this.labelText16 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText17
        {
            get
            {
                return this.labelText17;
            }

            set
            {
                this.SetMode(value, this.labelText17);
                this.labelText17 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText18
        {
            get
            {
                return this.labelText18;
            }

            set
            {
                this.SetMode(value, this.labelText18);
                this.labelText18 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText19
        {
            get
            {
                return this.labelText19;
            }

            set
            {
                this.SetMode(value, this.labelText19);
                this.labelText19 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText20
        {
            get
            {
                return this.labelText20;
            }

            set
            {
                this.SetMode(value, this.labelText20);
                this.labelText20 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText21
        {
            get
            {
                return this.labelText21;
            }

            set
            {
                this.SetMode(value, this.labelText21);
                this.labelText21 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText22
        {
            get
            {
                return this.labelText22;
            }

            set
            {
                this.SetMode(value, this.labelText22);
                this.labelText22 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText23
        {
            get
            {
                return this.labelText23;
            }

            set
            {
                this.SetMode(value, this.labelText23);
                this.labelText23 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText24
        {
            get
            {
                return this.labelText24;
            }

            set
            {
                this.SetMode(value, this.labelText24);
                this.labelText24 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText25
        {
            get
            {
                return this.labelText25;
            }

            set
            {
                this.SetMode(value, this.labelText25);
                this.labelText25 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText26
        {
            get
            {
                return this.labelText26;
            }

            set
            {
                this.SetMode(value, this.labelText26);
                this.labelText26 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText27
        {
            get
            {
                return this.labelText27;
            }

            set
            {
                this.SetMode(value, this.labelText27);
                this.labelText27 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText28
        {
            get
            {
                return this.labelText28;
            }

            set
            {
                this.SetMode(value, this.labelText28);
                this.labelText28 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText29
        {
            get
            {
                return this.labelText29;
            }

            set
            {
                this.SetMode(value, this.labelText29);
                this.labelText29 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText30
        {
            get
            {
                return this.labelText30;
            }

            set
            {
                this.SetMode(value, this.labelText30);
                this.labelText30 = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region attachment

        /// <summary>
        /// Gets the item attachments.
        /// </summary>
        public ObservableCollection<INAttachment> InventoryAttachments
        {
            get
            {
                return this.inventoryAttachments;
            }

            set
            {
                this.inventoryAttachments = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected partner attachment.
        /// </summary>
        public INAttachment SelectedAttachment
        {
            get
            {
                return this.selectedAttachment;
            }

            set
            {
                this.selectedAttachment = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region remarks

        /// <summary>
        /// Get or sets the inventory notes.
        /// </summary>
        public string InventoryNotes
        {
            get
            {
                return this.inventoryNotes;
            }

            set
            {
                this.SetMode(value, this.InventoryNotes);
                this.inventoryNotes = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region inventory

        /// <summary>
        /// Gets or sets the stock modes.
        /// </summary>
        public ObservableCollection<NouStockMode> StockModes
        {
            get
            {
                return this.stockModes;
            }

            set
            {
                this.stockModes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock modes.
        /// </summary>
        public ObservableCollection<ProductStockData> ProductStockData
        {
            get
            {
                return this.productStockData;
            }

            set
            {
                this.productStockData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the product stock mode.
        /// </summary>
        public NouStockMode StockMode
        {
            get
            {
                return this.stockMode;
            }

            set
            {
                if (value != null)
                {
                    this.SetMode(value, this.stockMode);
                }

                this.stockMode = value;
                this.RaisePropertyChanged();
                var modeId = value?.NouStockModeID;
                Messenger.Default.Send(modeId, Token.StockModeChange);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the product stock mode can be changed i.e. it's null or there's no transactions recorded against it.
        /// </summary>
        public bool ShowAll
        {
            get
            {
                return this.showAll;
            }

            set
            {
                this.showAll = value;
                this.RaisePropertyChanged();
                this.GetProductStockData();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the product stock mode can be changed i.e. it's null or there's no transactions recorded against it.
        /// </summary>
        public bool CanChangeStockMode
        {
            get
            {
                return this.canChangeStockMode;
            }

            set
            {
                this.canChangeStockMode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the minimum stock level before re-ordering.
        /// </summary>
        public decimal? MinimumStockLevel
        {
            get
            {
                return this.minimumStockLevel;
            }

            set
            {
                this.SetMode(value, this.minimumStockLevel);
                this.minimumStockLevel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the leadt time for product delivery.
        /// </summary>
        public int? OrderLeadTime
        {
            get
            {
                return this.orderLeadTime;
            }

            set
            {
                this.SetMode(value, this.orderLeadTime);
                this.orderLeadTime = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the po to auto creat orders from.
        /// </summary>
        public int? PurchaseOrderToCopyFrom
        {
            get
            {
                return this.purchaseOrderToCopyFrom;
            }

            set
            {
                this.SetMode(value, this.purchaseOrderToCopyFrom);
                this.purchaseOrderToCopyFrom = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the po to auto creat orders from.
        /// </summary>
        public int? ProductionOrderToCopyFrom
        {
            get
            {
                return this.productionOrderToCopyFrom;
            }

            set
            {
                this.productionOrderToCopyFrom = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the po to auto creat orders from.
        /// </summary>
        public string ProductionOrderReference
        {
            get
            {
                return this.productionOrderReference;
            }

            set
            {
                this.SetMode(value, this.productionOrderReference);
                this.productionOrderReference = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region accounts

        /// <summary>
        /// Get or sets the sales nominal code.
        /// </summary>
        public string SalesNominalCode
        {
            get
            {
                return this.salesNominalCode;
            }

            set
            {
                this.SetMode(value, this.SalesNominalCode);
                this.salesNominalCode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the sales nominal code.
        /// </summary>
        public string SalesNominalDeptCode
        {
            get
            {
                return this.salesNominalDeptCode;
            }

            set
            {
                this.SetMode(value, this.SalesNominalDeptCode);
                this.salesNominalDeptCode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the purchase nominal code.
        /// </summary>
        public string PurchaseNominalCode
        {
            get
            {
                return this.purchaseNominalCode;
            }

            set
            {
                this.SetMode(value, this.PurchaseNominalCode);
                this.purchaseNominalCode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the purchase dept nominal code.
        /// </summary>
        public string PurchaseNominalDeptCode
        {
            get
            {
                return this.purchaseNominalDeptCode;
            }

            set
            {
                this.SetMode(value, this.PurchaseNominalDeptCode);
                this.purchaseNominalDeptCode = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region properties

        /// <summary>
        /// Get or sets the selected property.
        /// </summary>
        public InventoryProperty SelectedProperty
        {
            get
            {
                return this.selectedProperty;
            }

            set
            {
                this.selectedProperty = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inventory properties.
        /// </summary>
        public ObservableCollection<InventoryProperty> InventoryProperties
        {
            get
            {
                return this.inventoryProperties;
            }

            set
            {
                this.inventoryProperties = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the inventory properties.
        /// </summary>
        public IList<INProperty> INProperties { get; set; }

        #endregion

        #endregion

        #region command

        public ICommand DrillDownToProductionOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down to the po search screen.
        /// </summary>
        public ICommand DrillDownToProductionOrdersCommand { get; private set; }

        public ICommand DrillDownToPurchaseOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down to the po search screen.
        /// </summary>
        public ICommand DrillDownToPurchaseOrdersCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down to the tare.
        /// </summary>
        public ICommand DrillDownToTareCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle an name change.
        /// </summary>
        public ICommand ItemNameEditCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a cell value change.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a price list item focus.
        /// </summary>
        public ICommand PriceItemPriceFocusCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the iprice list price change.
        /// </summary>
        public ICommand PriceChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the item search.
        /// </summary>
        public ICommand FindItemCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the item search.
        /// </summary>
        public ICommand FindProductCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selection of a property.
        /// </summary>
        public ICommand PropertySelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to select/deselect all the inventory properties.
        /// </summary>
        public ICommand SelectAllPropertiesCommand { get; private set; }

        /// <summary>
        /// Gets the command to attach the selected attachment.
        /// </summary>
        public ICommand AttachFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to upload an item image.
        /// </summary>
        public ICommand UploadImageCommand { get; private set; }

        /// <summary>
        /// Gets the command to display the selected attachment.
        /// </summary>
        public ICommand DisplayFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to delete the selected attachment.
        /// </summary>
        public ICommand DeleteFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unload.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the pricing drill down.
        /// </summary>
        public ICommand DrillDownToPricingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the actual price drill down.
        /// </summary>
        public ICommand DrillDownToActualPriceCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the warehouse drill down.
        /// </summary>
        public ICommand DrillDownToWarehouseCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the vat drill down.
        /// </summary>
        public ICommand DrillDownToVatCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Adds a new product.
        /// </summary>
        /// <param name="item">The product to add.</param>
        public void AddItem(InventoryItem item)
        {
            if (this.InventoryGroups == null)
            {
                this.GetInventoryGroups();
            }

            if (this.VatCodes == null)
            {
                this.GetVatCodes();
            }

            if (item.Master.INMasterID > 0)
            {
                this.SelectedInventoryItem = item;
                this.UpdateInventoryItem();
                return;
            }

            this.ItemCode = item.Code;
            this.ItemName = item.Name;

            if (string.IsNullOrEmpty(item.GroupName))
            {
                this.InventoryGroup = this.InventoryGroups.FirstOrDefault(x => x.Name.CompareIgnoringCase("No Group"));
            }
            else
            {
                this.InventoryGroup = this.InventoryGroups.FirstOrDefault(x => x.Name.Equals(item.GroupName));
            }

            if (this.InventoryGroup == null)
            {
                var newGroup = this.DataManager.UpdateInventoryGroup(new INGroup { Name = item.GroupName }, null, null);
                this.InventoryGroups.Add(newGroup);
                this.InventoryGroup = newGroup;
            }

            this.SelectedVATCode = this.VatCodes.FirstOrDefault();
            this.AddInventoryItem();
        }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.SelectedInventoryItem == null)
            {
                this.SelectedInventoryItem = NouvemGlobal.InventoryItems.LastOrDefault();
                return;
            }

            var previousItem =
                NouvemGlobal.InventoryItems.TakeWhile(
                    item => item.Master.INMasterID != this.SelectedInventoryItem.Master.INMasterID)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedInventoryItem = previousItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.SelectedInventoryItem == null)
            {
                this.SelectedInventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault();
                return;
            }

            var nextItem =
                NouvemGlobal.InventoryItems.SkipWhile(
                    item => item.Master.INMasterID != this.SelectedInventoryItem.Master.INMasterID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedInventoryItem = nextItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (this.SelectedInventoryItem == null)
            {
                this.SelectedInventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault();
                return;
            }

            var firstItem = NouvemGlobal.InventoryItems.FirstOrDefault();

            if (firstItem != null)
            {
                this.SelectedInventoryItem = firstItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (this.SelectedInventoryItem == null)
            {
                this.SelectedInventoryItem = NouvemGlobal.InventoryItems.LastOrDefault();
                return;
            }

            var lastItem = NouvemGlobal.InventoryItems.LastOrDefault();

            if (lastItem != null)
            {
                this.SelectedInventoryItem = lastItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            if (this.InventoryItems == null || !this.InventoryItems.Any())
            {
                return;
            }

            this.SelectedInventoryItem = this.InventoryItems.OrderByDescending(x => x.Master.EditDate).First();
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        #endregion

        #endregion

        #region internal

        /// <summary>
        /// Updates the inventory groups. (Called when a new group(s) is dynamically added)
        /// </summary>
        internal void UpdateInventoryGroups()
        {
            this.GetInventoryGroups();

            if (this.InventoryGroups.Count >= 2)
            {
                // Select the most receently created group (the group before define new)
                this.InventoryGroup = this.inventoryGroups.ElementAt(this.inventoryGroups.Count - 2);
            }
        }

        #endregion

        #region protected override

        /// <summary>
        /// Handle the drilldown command.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            if (this.InventoryGroup == null || this.InventoryGroup.Name.Equals(Strings.NoneSelected))
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.InventoryGroup, this.InventoryGroup.INGroupID), Token.DrillDown);
        }

        /// <summary>
        /// Determine if an inventory item has been selected.
        /// </summary>
        /// <returns></returns>
        protected override bool CheckForEntity()
        {
            return this.SelectedInventoryItem != null && this.SelectedInventoryItem.Master.INMasterID > 0;
        }

        /// <summary>
        /// Override the control mode set, to only apply when a selected entity is not being changed.
        /// </summary>
        /// <typeparam name="T">The generic type.</typeparam>
        /// <param name="newValue">The old value to be changed from.</param>
        /// <param name="oldValue">The value to change to.</param>
        protected override void SetMode<T>(T newValue, T oldValue)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            base.SetMode(newValue, oldValue);
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                // Moving into add new partner mode, so clear the partner fields.
                this.ClearForm();
                this.IsProductionProduct = true;
                this.IsSalesItem = true;
                this.IsPurchaseItem = true;
                this.SetControlMode(ControlMode.Add);
                this.SearchMode = false;
                if (ApplicationSettings.AutoCreateProductCode)
                {
                    var nextCode = this.DataManager.GetNextProductCode();
                    this.ItemCode = nextCode.IsNullOrZero() ? string.Empty : nextCode.ToString();
                }

                Messenger.Default.Send(Token.Message, Token.FocusToDataUpdate);
            }

            if (this.CurrentMode == ControlMode.Find)
            {
                // Moving into find partner mode, so clear the partner fields.
                this.ClearForm();
                this.SetControlMode(ControlMode.Find);
                this.SearchMode = true;
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedInventoryControl);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddInventoryItem();
                    break;

                case ControlMode.Find:
                    this.DisplaySearchScreen();
                    break;

                case ControlMode.Update:
                    this.UpdateInventoryItem();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Overrides the command to display the audit screen.
        /// </summary>
        /// <param name="s">The command parameter.</param>
        protected override void AuditDisplayCommandExecute(string s)
        {
            base.AuditDisplayCommandExecute(s);

            if (s.Equals(Constant.INMaster) && this.selectedInventoryItem.Master.INMasterID > 0)
            {
                Messenger.Default.Send(Tuple.Create(Constant.INMaster, this.selectedInventoryItem.Master.INMasterID), Token.AuditData);
            }
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Drills down to the tare set up screen.
        /// </summary>
        /// <param name="type">The container to selected.</param>
        private void DrillDownToTareCommandExecute(string type)
        {
            var tareId = 0;
            if (type.Equals(Constant.Piece) && this.SelectedPiecesContainerType != null)
            {
                tareId = this.SelectedPiecesContainerType.ContainerID;
            }
            else if (type.Equals(Constant.Box) && this.SelectedBoxContainerType != null)
            {
                tareId = this.SelectedBoxContainerType.ContainerID;
            }
            else if (type.Equals(Constant.Pallet) && this.SelectedPalletContainerType != null)
            {
                tareId = this.SelectedPalletContainerType.ContainerID;
            }
            else if (type.Equals(Constant.Linked) && this.SelectedLinkedProductContainerType != null)
            {
                tareId = this.SelectedLinkedProductContainerType.ContainerID;
            }

            Messenger.Default.Send(Tuple.Create(ViewType.ContainerSetUp, tareId), Token.DrillDown);
        }

        /// <summary>
        /// Handles a partner combobox edit.
        /// </summary>
        /// <param name="e">The edit parameter.</param>
        private void HandleNameEdit(ProcessNewValueEventArgs e)
        {
            #region validation

            if (this.CurrentMode != ControlMode.Update || this.SelectedInventoryItem == null || this.SelectedInventoryItem.Master == null || e == null || string.IsNullOrEmpty(e.DisplayText))
            {
                return;
            }

            #endregion

            var newItemName = e.DisplayText;
            this.SelectedInventoryItem.Master.Name = newItemName;
            this.ItemName = newItemName;
        }

        /// <summary>
        /// Handler for the drill down to the price list details.
        /// </summary>
        private void DrillDownToPricingCommandExecute()
        {
            if (this.SelectedPriceList == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.PriceListDetail, this.SelectedPriceList.PriceListID), Token.DrillDown);
        }

        /// <summary>
        /// Handler for the drill down to the warehouse.
        /// </summary>
        private void DrillDownToWarehouseCommandExecute()
        {
            if (this.SelectedPriceList == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.Warehouse, this.SelectedPriceList.PriceListID), Token.DrillDown);
        }

        /// <summary>
        /// Handler for the drill down to the price list containing the selected inventory item, highlighting it.
        /// </summary>
        private void DrillDownToActualPriceCommandExecute()
        {
            #region validation

            if (this.SelectedPriceList == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDown);
                return;
            }

            if (this.SelectedInventoryItem == null || this.SelectedInventoryItem.Master.INMasterID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.CannotDrillDownToPrice);
                return;
            }

            #endregion

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.PriceListDetail, this.SelectedPriceList.PriceListID), Token.DrillDown);
            Messenger.Default.Send(this.selectedInventoryItem.Master.INMasterID, Token.HightlightInventoryItem);
        }

        /// <summary>
        /// Sets the control mode when the price lkist price is entered/mofified.
        /// </summary>
        /// <remarks>Note-setting UpdateSourceTrigger=PropertyChanged on the View's PriceListIytemPrice leads to strange behaviour
        /// i.e. a decimal point can't be added by the user at runtime</remarks>
        private void PriceChangedCommandExecute()
        {
            if (this.SelectedInventoryItem != null && this.selectedInventoryItem.Master.INMasterID == 0)
            {
                this.SetControlMode(ControlMode.Add);
                return;
            }

            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Method that handles the selection/deselection of a selected partners properties.
        /// </summary>
        /// <param name="mode">The selection mode(select/deselect).</param>
        private void SelectAllPropertiesCommandExecute(string mode)
        {
            if (this.SelectedInventoryItem != null && this.SelectedInventoryItem.Master.INMasterID != 0)
            {
                this.SetControlMode(ControlMode.Update);
            }
            else
            {
                this.SetControlMode(ControlMode.Add);
            }

            this.InventoryProperties.Clear();

            if (mode.Equals(Constant.Select))
            {
                // select all
                this.INProperties.ToList().ForEach(x => this.InventoryProperties.Add(new InventoryProperty { Details = x, IsSelected = true }));
            }
            else
            {
                // deselect all
                this.INProperties.ToList().ForEach(x => this.InventoryProperties.Add(new InventoryProperty { Details = x, IsSelected = false }));
            }
        }

        /// <summary>
        /// Handler for the command to attach a new file to the db.
        /// </summary>
        private void AttachFileCommandExecute()
        {
            var openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                var localFile = File.ReadAllBytes(openFileDialog.FileName);
                var localName = openFileDialog.SafeFileName;

                var localAttachment = new INAttachment { AttachmentDate = DateTime.Now, FileName = localName, File = localFile };

                this.InventoryAttachments.Add(localAttachment);
                this.allInventoryAttachments.Add(localAttachment);

                if (!this.CheckForEntity())
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Handler to upload an item image
        /// </summary>
        private void UploadImageCommandExecute()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "ALL FILES|*.*|PNG|*.png|BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|TIFF|*.tif;*.tiff";

            if (openFileDialog.ShowDialog() == true)
            {
                this.ItemPhoto = File.ReadAllBytes(openFileDialog.FileName);
            }
        }

        /// <summary>
        /// Handler for the command to display a new file to the db.
        /// </summary>
        private void DisplayFileCommandExecute()
        {
            #region validation

            if (this.SelectedAttachment == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoAttachmentSelected);
                return;
            }

            #endregion

            try
            {
                var file = this.SelectedAttachment.File;
                var fileName = this.SelectedAttachment.FileName;

                var path = Path.Combine(Settings.Default.AtachmentsPath, fileName);
                File.WriteAllBytes(path, file);
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Handler for the command to delete a file from the db.
        /// </summary>
        private void DeleteFileCommandExecute()
        {
            if (this.SelectedAttachment != null)
            {
                if (this.selectedInventoryItem.Master.INMasterID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }

                // mark it for removal.
                this.selectedAttachment.Deleted = true;
                this.InventoryAttachments.Remove(this.SelectedAttachment);
            }
        }

        /// <summary>
        /// handles the ui load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.SetControlMode(ControlMode.Find);
            this.IsFormLoaded = true;
            NouvemGlobal.UpdateInventoryItems();
            this.GetInventoryGroups();
            this.GetDateDays();
            this.GetPriceLists();
            this.SearchMode = true;
            this.ShowAll = ApplicationSettings.ShowAllStock;
        }

        /// <summary>
        /// handles the ui unload event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            ApplicationSettings.ShowAllStock = this.ShowAll;
            this.IsFormLoaded = false;
            this.Close();
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets the partners.
        /// </summary>
        private void GetPartners()
        {
            this.BusinessPartners = new List<ViewBusinessPartner>(NouvemGlobal.BusinessPartners.Select(x => x.Details));
            this.BusinessPartners.Insert(0, new ViewBusinessPartner());
        }

        /// <summary>
        /// Sets the localised label names.
        /// </summary>
        private void GetScales()
        {
            this.Scales = new List<CollectionData>
            {
                new CollectionData {ID = 1, Data = "Scales 1"},
                new CollectionData {ID = 2, Data = "Scales 2"},
                new CollectionData {ID = 3, Data = "Scales 3"}
            };
        }

        /// <summary>
        /// Sets the stock locations.
        /// </summary>
        private void GetStockLocations()
        {
            this.StockLocations = new List<Warehouse>(NouvemGlobal.WarehouseLocations);
            this.StockLocations.Insert(0, new Warehouse { Name = Strings.TerminalLocation });
            this.SelectedStockLocation = this.StockLocations.First();
        }

        /// <summary>
        /// Refreshes the vat codes and containers, and price lists.
        /// </summary>
        private void RefreshCollections()
        {
            var localMode = this.CurrentMode;
            var localPieceContainer = this.selectedPiecesContainerType;
            var localBoxContainer = this.selectedBoxContainerType;
            var localPalletContainer = this.selectedPalletContainerType;
            var localLinkedProductContainer = this.selectedLinkedProductContainerType;

            var localSelectedPriceList = this.selectedPriceList;
            this.GetContainers();
            this.GetVatCodes();
            this.GetPriceLists();

            if (localSelectedPriceList != null)
            {
                this.SelectedPriceList =
                    this.PriceLists.FirstOrDefault(x => x.PriceListID == localSelectedPriceList.PriceListID);
            }

            if (localPieceContainer != null)
            {
                this.SelectedPiecesContainerType =
                    this.Containers.FirstOrDefault(x => x.ContainerID == localPieceContainer.ContainerID);
            }

            if (localBoxContainer != null)
            {
                this.SelectedBoxContainerType =
                    this.Containers.FirstOrDefault(x => x.ContainerID == localBoxContainer.ContainerID);
            }

            if (localPalletContainer != null)
            {
                this.SelectedPalletContainerType =
                    this.Containers.FirstOrDefault(x => x.ContainerID == localPalletContainer.ContainerID);
            }

            if (localLinkedProductContainer != null)
            {
                this.SelectedLinkedProductContainerType =
                    this.Containers.FirstOrDefault(x => x.ContainerID == localLinkedProductContainer.ContainerID);
            }

            this.SetControlMode(localMode);
        }

        /// <summary>
        /// Handler for a property selection, setting the control button.
        /// </summary>
        private void PropertySelectedCommandExecute()
        {
            if (this.CheckForEntity())
            {
                this.SetControlMode(ControlMode.Update);
                return;
            }

            this.SetControlMode(ControlMode.Add);
        }

        /// <summary>
        /// Makes a call to search for an item.
        /// </summary>
        private void FindItemCommandExecute()
        {
            this.DisplaySearchScreen();
        }

        /// <summary>
        /// Method that clears the ui, by creating a blank inventory item.
        /// </summary>
        private void ClearForm()
        {
            Task.Factory.StartNew(this.GetDateDays);
            this.ProductStockData?.Clear();
            this.MRPStockData?.Clear();
            this.StockMode = null;
            this.ResetInventoryTotals();
            this.CreateBlankInventoryItem();
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        /// <summary>
        /// Creates an inventory item, using the current fields, for adding or updating.
        /// </summary>
        /// <returns>A flag, indicating a successful item creation.</returns>
        private bool CreateInventoryItem()
        {
            try
            {
                this.selectedInventoryItem.Master.Code = this.ItemCode;
                this.selectedInventoryItem.Master.Name = this.ItemName;
                this.selectedInventoryItem.Master.StockItem = this.IsStockItem;
                this.selectedInventoryItem.Master.SalesItem = this.IsSalesItem;
                this.selectedInventoryItem.Master.PurchaseItem = this.IsPurchaseItem;
                this.selectedInventoryItem.Master.FixedAsset = this.IsFixedAsset;
                this.selectedInventoryItem.Master.ProductionProduct = this.IsProductionProduct;
                this.selectedInventoryItem.Master.ReceipeProduct = this.IsReceipeProduct;
                this.selectedInventoryItem.Master.SalesNominalCode = this.salesNominalCode;
                this.selectedInventoryItem.Master.SalesNominalDeptCode = this.salesNominalDeptCode;
                this.selectedInventoryItem.Master.PurchaseNominalCode = this.purchaseNominalCode;
                this.selectedInventoryItem.Master.PurchaseNominalDeptCode = this.PurchaseNominalDeptCode;
                this.selectedInventoryItem.Master.Remarks = this.InventoryNotes;
                this.selectedInventoryItem.Master.TypicalPieces = this.TypicalPieces;
                this.selectedInventoryItem.Master.QtyPerBox = this.QtyPerBox;
                this.selectedInventoryItem.Master.WeightRequired = this.WeightRequired;
                this.selectedInventoryItem.Master.AccumulateTares = this.AccumulateTares;
                this.selectedInventoryItem.Master.ActiveFrom = this.ActiveFromDate;
                this.selectedInventoryItem.Master.ActiveTo = this.ActiveToDate;
                this.selectedInventoryItem.Master.InActiveFrom = this.InactiveFromDate;
                this.selectedInventoryItem.Master.PrintPieceLabels = this.PrintPieceLabels;
                this.selectedInventoryItem.Master.ReprintLabelAtDispatch = this.ReprintLabelAtDispatch;
                this.selectedInventoryItem.Master.InActiveTo = this.InactiveToDate;
                this.selectedInventoryItem.Group = this.InventoryGroup;
                this.selectedInventoryItem.TraceabilityTemplate = this.SelectedTraceabilityTemplateName;
                this.selectedInventoryItem.VatCode = this.SelectedVATCode;
                this.selectedInventoryItem.Master.Scales =
                    this.selectedLinkedScales != null ? this.selectedLinkedScales.ID : (int?)null;
                this.selectedInventoryItem.Department = this.SelectedDepartment;
                this.selectedInventoryItem.BoxTareContainer = this.SelectedBoxContainerType;
                this.selectedInventoryItem.PiecesTareContainer = this.SelectedPiecesContainerType;
                this.selectedInventoryItem.PalletTareContainer = this.SelectedPalletContainerType;
                this.selectedInventoryItem.LinkedProductTareContainer = this.SelectedLinkedProductContainerType;
                this.selectedInventoryItem.Master.NominalWeight = this.NominalWeight.ToNullableDecimal();
                this.selectedInventoryItem.Master.MinWeight = this.MinWeight.ToNullableDecimal();
                this.selectedInventoryItem.Master.MaxWeight = this.MaxWeight.ToNullableDecimal();
                this.selectedInventoryItem.Master.ItemImage = this.ItemPhoto;
                this.selectedInventoryItem.Master.MinimumStockLevel = this.MinimumStockLevel;
                this.selectedInventoryItem.Master.OrderLeadTime = this.OrderLeadTime;
                this.selectedInventoryItem.Master.IsDeduction = this.IsDeductionProduct;
                this.selectedInventoryItem.Master.DeductionRate = this.DeductionRate.ToDecimal();
                this.selectedInventoryItem.Master.ApplyDeductionToDocket = this.ApplyDeductionToDocket;
                this.selectedInventoryItem.Master.APOrderID_Copy = this.PurchaseOrderToCopyFrom;
                this.selectedInventoryItem.Master.PROrderID_Copy = this.ProductionOrderToCopyFrom;
                this.selectedInventoryItem.Master.Reference = this.ProductionOrderReference;
                this.selectedInventoryItem.Master.BPMasterID =
                    this.selectedPartner != null && this.selectedPartner.BPMasterID > 0
                        ? this.selectedPartner.BPMasterID
                        : (int?)null;
                this.selectedInventoryItem.Attachments = this.allInventoryAttachments;
                this.selectedInventoryItem.Properties =
                    this.InventoryProperties.Where(x => x.IsSelected).Select(x => x.Details).ToList();
                this.selectedInventoryItem.StockMode = this.StockMode;
                this.CheckBoxInactive = false;
                this.CheckBoxActive = false;
                if (this.SelectedStockLocation == null ||
                    this.SelectedStockLocation.Name.Equals(Strings.TerminalLocation))
                {
                    this.selectedInventoryItem.Master.WarehouseID = null;
                }
                else
                {
                    this.selectedInventoryItem.Master.WarehouseID = this.SelectedStockLocation.WarehouseID;
                }

                if (this.SelectedPriceMethod == null || this.SelectedPriceMethod.Name.Equals(Strings.NoneSelected))
                {
                    this.selectedInventoryItem.Master.NouPriceMethodID = null;
                }
                else
                {
                    this.selectedInventoryItem.Master.NouPriceMethodID = this.SelectedPriceMethod.NouPriceMethodID;
                }

                if (this.SelectedOrderMethod == null || this.SelectedOrderMethod.Name.Equals(Strings.NoneSelected))
                {
                    this.selectedInventoryItem.Master.NouOrderMethodID = null;
                }
                else
                {
                    this.selectedInventoryItem.Master.NouOrderMethodID = this.SelectedOrderMethod.NouOrderMethodID;
                }

                // get the selected pricing data.
                this.SelectedInventoryItem.SelectedPriceData = new Tuple<PriceList, decimal?>(this.SelectedPriceList, this.priceListItemPrice);

                #region label items

                if (
                    !string.IsNullOrEmpty(this.LabelText1)
                    || !string.IsNullOrEmpty(this.LabelText1)
                    || !string.IsNullOrEmpty(this.LabelText2)
                    || !string.IsNullOrEmpty(this.LabelText3)
                    || !string.IsNullOrEmpty(this.LabelText4)
                    || !string.IsNullOrEmpty(this.LabelText5)
                    || !string.IsNullOrEmpty(this.LabelText6)
                    || !string.IsNullOrEmpty(this.LabelText7)
                    || !string.IsNullOrEmpty(this.LabelText8)
                    || !string.IsNullOrEmpty(this.LabelText9)
                    || !string.IsNullOrEmpty(this.LabelText10)
                    || !string.IsNullOrEmpty(this.LabelText11)
                    || !string.IsNullOrEmpty(this.LabelText12)
                    || !string.IsNullOrEmpty(this.LabelText13)
                    || !string.IsNullOrEmpty(this.LabelText14)
                    || !string.IsNullOrEmpty(this.LabelText15)
                    || !string.IsNullOrEmpty(this.LabelText16)
                    || !string.IsNullOrEmpty(this.LabelText17)
                    || !string.IsNullOrEmpty(this.LabelText18)
                    || !string.IsNullOrEmpty(this.LabelText19)
                    || !string.IsNullOrEmpty(this.LabelText20)
                    || !string.IsNullOrEmpty(this.LabelText21)
                    || !string.IsNullOrEmpty(this.LabelText22)
                    || !string.IsNullOrEmpty(this.LabelText23)
                    || !string.IsNullOrEmpty(this.LabelText24)
                    || !string.IsNullOrEmpty(this.LabelText25)
                    || !string.IsNullOrEmpty(this.LabelText26)
                    || !string.IsNullOrEmpty(this.LabelText27)
                    || !string.IsNullOrEmpty(this.LabelText28)
                    || !string.IsNullOrEmpty(this.LabelText29)
                    || !string.IsNullOrEmpty(this.LabelText30)
                    || !string.IsNullOrEmpty(this.LabelMultiText1)
                    || !string.IsNullOrEmpty(this.LabelMultiText2)
                    || !string.IsNullOrEmpty(this.Barcode1)
                    || !string.IsNullOrEmpty(this.Barcode2)
                    || !string.IsNullOrEmpty(this.Barcode3)
                    || !string.IsNullOrEmpty(this.Barcode4)
                    )
                {
                    if (this.selectedInventoryItem.LabelField == null)
                    {
                        this.selectedInventoryItem.LabelField = new ProductLabelField();
                    }

                    this.selectedInventoryItem.LabelField.Field1 = this.LabelText1;
                    this.selectedInventoryItem.LabelField.Field2 = this.LabelText2;
                    this.selectedInventoryItem.LabelField.Field3 = this.LabelText3;
                    this.selectedInventoryItem.LabelField.Field4 = this.LabelText4;
                    this.selectedInventoryItem.LabelField.Field5 = this.LabelText5;
                    this.selectedInventoryItem.LabelField.Field6 = this.LabelText6;
                    this.selectedInventoryItem.LabelField.Field7 = this.LabelText7;
                    this.selectedInventoryItem.LabelField.Field8 = this.LabelText8;
                    this.selectedInventoryItem.LabelField.Field9 = this.LabelText9;
                    this.selectedInventoryItem.LabelField.Field10 = this.LabelText10;
                    this.selectedInventoryItem.LabelField.Field11 = this.LabelText11;
                    this.selectedInventoryItem.LabelField.Field12 = this.LabelText12;
                    this.selectedInventoryItem.LabelField.Field13 = this.LabelText13;
                    this.selectedInventoryItem.LabelField.Field14 = this.LabelText14;
                    this.selectedInventoryItem.LabelField.Field15 = this.LabelText15;
                    this.selectedInventoryItem.LabelField.Field16 = this.LabelText16;
                    this.selectedInventoryItem.LabelField.Field17 = this.LabelText17;
                    this.selectedInventoryItem.LabelField.Field18 = this.LabelText18;
                    this.selectedInventoryItem.LabelField.Field19 = this.LabelText19;
                    this.selectedInventoryItem.LabelField.Field20 = this.LabelText20;
                    this.selectedInventoryItem.LabelField.Field21 = this.LabelText21;
                    this.selectedInventoryItem.LabelField.Field22 = this.LabelText22;
                    this.selectedInventoryItem.LabelField.Field23 = this.LabelText23;
                    this.selectedInventoryItem.LabelField.Field24 = this.LabelText24;
                    this.selectedInventoryItem.LabelField.Field25 = this.LabelText25;
                    this.selectedInventoryItem.LabelField.Field26 = this.LabelText26;
                    this.selectedInventoryItem.LabelField.Field27 = this.LabelText27;
                    this.selectedInventoryItem.LabelField.Field28 = this.LabelText28;
                    this.selectedInventoryItem.LabelField.Field29 = this.LabelText29;
                    this.selectedInventoryItem.LabelField.Field30 = this.LabelText30;
                    this.selectedInventoryItem.LabelField.MultiLineField1 = this.LabelMultiText1;
                    this.selectedInventoryItem.LabelField.MultiLineField2 = this.LabelMultiText2;
                    this.selectedInventoryItem.LabelField.Barcode1 = this.Barcode1;
                    this.selectedInventoryItem.LabelField.Barcode2 = this.Barcode2;
                    this.selectedInventoryItem.LabelField.Barcode3 = this.Barcode3;
                    this.selectedInventoryItem.LabelField.Barcode4 = this.Barcode4;
                }
                else
                {
                    // no label field data.
                    this.selectedInventoryItem.LabelField = null;
                }

                #endregion
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method that validates an inventory item for adds or updates.
        /// </summary>
        /// <returns>A flag, indicating validation or not.</returns>
        private string ValidateInventoryItem(bool validateUpdate = false)
        {
            var error = string.Empty;

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateProducts)
            {
                this.SetControlMode(ControlMode.OK);
                return Message.AccessDenied;
            }

            if (string.IsNullOrWhiteSpace(this.ItemCode))
            {
                error = Message.CodeEmpty;
                return error;
            }

            if (!validateUpdate)
            {
                if (NouvemGlobal.InventoryItems.Any(x => x.Master.Code == this.ItemCode))
                {
                    error = string.Format(Message.CodeExists, this.itemCode);
                    return error;
                }
            }

            if (string.IsNullOrWhiteSpace(this.ItemName))
            {
                error = Message.NoNameSelected;
                return error;
            }

            if (this.ItemCode.Length > 30)
            {
                error = Message.CodeTooLong;
                return error;
            }

            if (this.InventoryGroup == null || this.InventoryGroup.Name.Equals(Strings.NoneSelected) || this.InventoryGroup.Name == string.Empty)
            {
                error = Message.NoInventoryGroupSelected;
                return error;
            }

            if (this.SelectedVATCode == null)
            {
                error = Message.NoVatCodeSelected;
                return error;
            }

            if (this.SelectedLinkedProductContainerType != null && this.SelectedLinkedProductContainerType.ContainerID > 0)
            {
                if (!this.SelectedLinkedProductContainerType.Name.CompareIgnoringCase(this.ItemName))
                {
                    error = string.Format(Message.LinkedProductTareNameMismatch, this.SelectedLinkedProductContainerType.Name, this.ItemName);
                    return error;
                }

                var linkedProduct =
                    this.DataManager.GetLinkedProductTareProduct(this.SelectedLinkedProductContainerType.ContainerID);
                if (linkedProduct != null && linkedProduct.INMasterID != this.selectedInventoryItem.Master.INMasterID)
                {
                    error = string.Format(Message.LinkedProductTareAlreadylinked, linkedProduct.Name);
                    return error;
                }
            }

            return error;
        }

        /// <summary>
        /// Method that determines if a search can be made.
        /// </summary>
        /// <returns>A flag, indicating a valid search.</returns>
        private bool IsSearchValid()
        {
            return !(string.IsNullOrWhiteSpace(this.ItemName) && string.IsNullOrWhiteSpace(this.ItemCode)
                   && (this.InventoryGroup == null || this.InventoryGroup.Name.Equals(Strings.NoneSelected)));
        }

        /// <summary>
        /// Locates a search item based on the user search parameter(s).
        /// </summary>
        [Obsolete]
        private void FindInventoryItem()
        {
            #region validation

            if (!this.IsSearchValid())
            {
                SystemMessage.Write(MessageType.Issue, Message.EnterSearchTerm);
                return;
            }

            #endregion

            List<InventoryItem> foundData;

            // get the search term(s)
            var nameSearchTerm = this.ItemName ?? string.Empty;
            var codeSearchTerm = this.ItemCode ?? string.Empty;
            var groupSearchTerm = this.InventoryGroup != null && this.InventoryGroup.Name != Strings.NoneSelected ? this.InventoryGroup.Name : string.Empty;

            // check for search all or search all including inactive items.
            var searchTerm = nameSearchTerm.Equals(Constant.SearchAll) || codeSearchTerm.Equals(Constant.SearchAll)
                                 ? Constant.SearchAll
                                 : nameSearchTerm.Equals(Constant.SearchAllIncludeInactive) || codeSearchTerm.Equals(Constant.SearchAllIncludeInactive)
                                 ? Constant.SearchAllIncludeInactive
                                 : string.Empty;

            #region search filter

            if (searchTerm.Equals(Constant.SearchAllIncludeInactive))
            {
                // we're taking everything, including inactive items.
                foundData = NouvemGlobal.InventoryItems.ToList();
            }
            else if (searchTerm.Equals(Constant.SearchAll))
            {
                // we're taking all the active partners
                foundData = NouvemGlobal.InventoryItems.Where(
                       item =>
                       ((item.Master.ActiveFrom == null || item.Master.ActiveFrom <= DateTime.Today)
                        && (item.Master.ActiveTo == null || item.Master.ActiveTo >= DateTime.Today))
                        && (((item.Master.InActiveFrom == null || item.Master.InActiveTo == null)
                            || !(DateTime.Today >= item.Master.InActiveFrom && DateTime.Today <= item.Master.InActiveTo))))
                            .ToList();
            }
            else if (nameSearchTerm != string.Empty && codeSearchTerm.Equals(string.Empty)
                    && groupSearchTerm.Equals(string.Empty))
            {
                // searching by name only
                foundData =
                    NouvemGlobal.InventoryItems.Where(
                    item => ((item.Master.ActiveFrom == null || item.Master.ActiveFrom <= DateTime.Today) && (item.Master.ActiveTo == null || item.Master.ActiveTo >= DateTime.Today))
                        && (((item.Master.InActiveFrom == null || item.Master.InActiveTo == null) || !(DateTime.Today >= item.Master.InActiveFrom && DateTime.Today <= item.Master.InActiveTo)))
                        && item.Master.Name.StartsWithIgnoringCase(nameSearchTerm))
                        .ToList();
            }
            else if (nameSearchTerm.Equals(string.Empty) && codeSearchTerm != string.Empty
                && groupSearchTerm.Equals(string.Empty))
            {
                // searching by code only
                foundData =
                    NouvemGlobal.InventoryItems.Where(
                    item => ((item.Master.ActiveFrom == null || item.Master.ActiveFrom <= DateTime.Today) && (item.Master.ActiveTo == null || item.Master.ActiveTo >= DateTime.Today))
                        && (((item.Master.InActiveFrom == null || item.Master.InActiveTo == null) || !(DateTime.Today >= item.Master.InActiveFrom && DateTime.Today <= item.Master.InActiveTo)))
                        && item.Master.Code.StartsWithIgnoringCase(codeSearchTerm))
                        .ToList();
            }
            else if (nameSearchTerm.Equals(string.Empty) && codeSearchTerm.Equals(string.Empty)
                && groupSearchTerm != string.Empty)
            {
                // searching by group only
                foundData =
                    NouvemGlobal.InventoryItems.Where(
                    item => ((item.Master.ActiveFrom == null || item.Master.ActiveFrom <= DateTime.Today) && (item.Master.ActiveTo == null || item.Master.ActiveTo >= DateTime.Today))
                        && (((item.Master.InActiveFrom == null || item.Master.InActiveTo == null) || !(DateTime.Today >= item.Master.InActiveFrom && DateTime.Today <= item.Master.InActiveTo)))
                        && item.Group.Name.Equals(groupSearchTerm))
                        .ToList();
            }
            else if (nameSearchTerm != string.Empty && codeSearchTerm != string.Empty
                && groupSearchTerm.Equals(string.Empty))
            {
                // searching by name and code
                foundData =
                    NouvemGlobal.InventoryItems.Where(
                    item => ((item.Master.ActiveFrom == null || item.Master.ActiveFrom <= DateTime.Today) && (item.Master.ActiveTo == null || item.Master.ActiveTo >= DateTime.Today))
                        && (((item.Master.InActiveFrom == null || item.Master.InActiveTo == null) || !(DateTime.Today >= item.Master.InActiveFrom && DateTime.Today <= item.Master.InActiveTo)))
                        && item.Master.Name.StartsWithIgnoringCase(nameSearchTerm) && item.Master.Code.StartsWithIgnoringCase(codeSearchTerm))
                        .ToList();
            }
            else if (nameSearchTerm != string.Empty && codeSearchTerm.Equals(string.Empty)
               && groupSearchTerm != string.Empty)
            {
                // searching by name and group
                foundData =
                    NouvemGlobal.InventoryItems.Where(
                    item => ((item.Master.ActiveFrom == null || item.Master.ActiveFrom <= DateTime.Today) && (item.Master.ActiveTo == null || item.Master.ActiveTo >= DateTime.Today))
                        && (((item.Master.InActiveFrom == null || item.Master.InActiveTo == null) || !(DateTime.Today >= item.Master.InActiveFrom && DateTime.Today <= item.Master.InActiveTo)))
                        && item.Master.Name.StartsWithIgnoringCase(nameSearchTerm) && item.Group.Name.Equals(groupSearchTerm))
                        .ToList();
            }
            else if (nameSearchTerm.Equals(string.Empty) && codeSearchTerm != string.Empty
              && groupSearchTerm != string.Empty)
            {
                // searching by code and group 
                foundData =
                    NouvemGlobal.InventoryItems.Where(
                    item => ((item.Master.ActiveFrom == null || item.Master.ActiveFrom <= DateTime.Today) && (item.Master.ActiveTo == null || item.Master.ActiveTo >= DateTime.Today))
                        && (((item.Master.InActiveFrom == null || item.Master.InActiveTo == null) || !(DateTime.Today >= item.Master.InActiveFrom && DateTime.Today <= item.Master.InActiveTo)))
                        && item.Master.Code.StartsWithIgnoringCase(codeSearchTerm) && item.Group.Name.Equals(groupSearchTerm))
                        .ToList();
            }
            else
            {
                // searching by full name, group, and name
                foundData =
                    NouvemGlobal.InventoryItems.Where(
                        item =>
                        ((item.Master.ActiveFrom == null || item.Master.ActiveFrom <= DateTime.Today)
                         && (item.Master.ActiveTo == null || item.Master.ActiveTo >= DateTime.Today))
                        && (((item.Master.InActiveFrom == null || item.Master.InActiveTo == null)
                             || !(DateTime.Today >= item.Master.InActiveFrom
                                  && DateTime.Today <= item.Master.InActiveTo)))
                        && item.Master.Name.StartsWithIgnoringCase(nameSearchTerm)
                        && item.Master.Code.StartsWithIgnoringCase(codeSearchTerm)
                        && item.Group.Name.Equals(groupSearchTerm)).ToList();
            }

            #endregion

            if (!foundData.Any())
            {
                SystemMessage.Write(MessageType.Issue, Message.NoItemFound);
                return;
            }

            if (foundData.Count == 1)
            {
                // The item has been found, so no need to show the search window.
                this.SelectedInventoryItem = foundData.First();
                return;
            }

            this.Locator.InventorySearchData.SetFilteredItems(foundData);

            // Multiple matches, so show them in the search grid.
            Messenger.Default.Send(ViewType.InventorySearchData);
        }

        /// <summary>
        /// Displays the search screen
        /// </summary>
        private void DisplaySearchScreen()
        {
            this.Locator.InventorySearchData.SetFilteredItems(this.InventoryItems.Where(x => (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                         || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)).ToList());

            // Multiple matches, so show them in the search grid.
            Messenger.Default.Send(ViewType.InventorySearchData);
            this.Locator.InventorySearchData.SelectedSearchItem = null;
            if (this.SelectedInventoryItem != null && this.SelectedInventoryItem.Master.INMasterID > 0)
            {
                this.Locator.InventorySearchData.SelectedSearchItem = this.SelectedInventoryItem;
            }
        }

        /// <summary>
        /// Updates the date days.
        /// </summary>
        private void UpdateDateDays()
        {
            var localDateDays = new List<DateDay>();
            foreach (var template in this.TraceabilityTemplateAllocations)
            {
                template.DateDay.INMasterID = this.selectedInventoryItem.Master.INMasterID;
                template.DateDay.AttributeMasterID = template.AttributeMaster.AttributeMasterID;
                localDateDays.Add(template.DateDay);
            }

            this.DataManager.AddOrUpdateDateDays(localDateDays);
        }

        /// <summary>
        /// Makes a call to add the current inventory item.
        /// </summary>
        private void AddInventoryItem()
        {
            #region validation

            var error = this.ValidateInventoryItem();
            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                this.Log.LogError(this.GetType(), $"AddInventoryItem() - {error}");
                return;
            }

            #endregion

            if (!this.CreateInventoryItem())
            {
                SystemMessage.Write(MessageType.Issue, Message.Error);
                return;
            }

            if (this.DataManager.AddOrUpdateInventoryItem(this.selectedInventoryItem, this.selectedInventoryItemSnapshot))
            {
                this.UpdateDateDays();
                SystemMessage.Write(MessageType.Priority, Message.InventoryItemCreated);
                Messenger.Default.Send(Tuple.Create(this.selectedInventoryItem, true), Token.RefreshProducts);

                var localPriceListId = 0;
                INMaster localINMaster = null;
                var localPrice = this.priceListItemPrice.ToDecimal();
                if (this.SelectedPriceList != null)
                {
                    localPriceListId = this.SelectedPriceList.PriceListID;
                }

                if (this.selectedInventoryItem != null)
                {
                    localINMaster = this.selectedInventoryItem.Master;
                }

                if (this.selectedInventoryItem != null)
                {
                    this.UpdateGlobalItems(this.selectedInventoryItem.Master.INMasterID);
                }
             
                this.GetInventoryItems();
                this.SetControlMode(ControlMode.OK);
                this.ClearForm();

                if (localPriceListId > 0 && localINMaster != null)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        Messenger.Default.Send(ViewType.PriceListDetail);
                        this.Locator.PriceListDetail.AddProductToPriceListDetail(localPriceListId, localINMaster, localPrice);
                    }));
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
            }
        }

        /// <summary>
        /// Makes a call to update the current selected inventory item.
        /// </summary>
        private void UpdateInventoryItem()
        {
            #region validation

            var error = this.ValidateInventoryItem(true);
            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            if (!this.CreateInventoryItem())
            {
                SystemMessage.Write(MessageType.Issue, Message.Error);
                return;
            }

            if (this.DataManager.AddOrUpdateInventoryItem(this.selectedInventoryItem, this.selectedInventoryItemSnapshot))
            {
                SystemMessage.Write(MessageType.Priority, Message.InventoryItemUpdated);
                Task.Factory.StartNew(this.UpdateDateDays);
                if (this.selectedItemPrice != null)
                {
                    this.selectedItemPrice.Price = this.priceListItemPrice.ToDecimal();
                }

                this.UpdateGlobalItems(this.selectedInventoryItem.Master.INMasterID);
                this.GetInventoryItems();
                this.SetControlMode(ControlMode.OK);
                this.selectedInventoryItemSnapshot = this.selectedInventoryItem;
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DatabaseError);
            }
        }

        /// <summary>
        /// Updates the global items collection, with the newly updated item values.
        /// </summary>
        private void UpdateGlobalItems(int id)
        {
            // update the item in our glabal collections.
            NouvemGlobal.AddInventoryItemByID(id);
            Messenger.Default.Send(Tuple.Create(this.selectedInventoryItem, false), Token.RefreshProducts);
        }

        /// <summary>
        /// Sets the localised label names.
        /// </summary>
        private void SetLabelNames()
        {
            this.LabelTextName = Strings.Text;
        }

        /// <summary>
        /// Creates a default inventory item.
        /// </summary>
        private void CreateBlankInventoryItem()
        {
            var localMaster = new INMaster
            {
                Code = string.Empty,
                Name = string.Empty,
                StockItem = false,
                SalesItem = false,
                PurchaseItem = false,
                FixedAsset = false,
                ProductionProduct = false,
                ReceipeProduct = false,
                MinWeight = 0,
                MaxWeight = 0,
                NominalWeight = 0,
                SalesNominalCode = string.Empty,
                SalesNominalDeptCode = string.Empty,
                PurchaseNominalCode = string.Empty,
                PurchaseNominalDeptCode = string.Empty,
                TypicalPieces = 0,
                Remarks = string.Empty
            };

            var localGroup = new INGroup { Name = string.Empty };
            var localContainer = new Container { Name = string.Empty, Tare = 0 };
            var localDepartment = new Department { Code = string.Empty, Name = string.Empty };
            // var localVatCode = new VATCode {Code = string.Empty, Description = string.Empty};
            var localVatCode = this.VatCodes.FirstOrDefault();
            var localTraceabilityTemplate = new AttributeTemplate { Name = string.Empty };
            var localLabelField = new ProductLabelField();
            this.SelectedInventoryItem = new InventoryItem
            {
                Master = localMaster,
                Group = localGroup,
                BoxTareContainer = localContainer,
                PiecesTareContainer = localContainer,
                PalletTareContainer = localContainer,
                Department = localDepartment,
                VatCode = localVatCode,
                TraceabilityTemplate = localTraceabilityTemplate,
                LabelField = localLabelField
            };

            this.SelectedPriceList = null;
            this.PriceListItemPrice = null;
            this.InheritTraceabilityTemplate = true;
            this.InventoryGroup = this.InventoryGroups.FirstOrDefault(x => x.Name.Equals(Strings.NoneSelected));
        }

        /// <summary>
        /// Method that parses the selected inventory item.
        /// </summary>
        private void HandleSelectedInventoryItem()
        {
            if (this.selectedInventoryItem == null)
            {
                return;
            }

            try
            {
                // Set the flag that we are changing an inventory item, so that control changes are to be ignored.
                this.EntitySelectionChange = true;

                this.CheckBoxInactive = false;
                this.CheckBoxActive = false;

                if (this.selectedInventoryItem.Master != null)
                {
                    this.ResetInventoryTotals();
                    if (this.selectedInventoryItem.Master.INMasterID > 0)
                    {
                        this.selectedInventoryItem.Master.ItemImage =
                            this.DataManager.GetImage(this.selectedInventoryItem.Master.INMasterID);
                    }

                    this.ItemCode = this.selectedInventoryItem.Master.Code;
                    this.ItemName = this.selectedInventoryItem.Master.Name;
                    this.PrintPieceLabels = this.selectedInventoryItem.Master.PrintPieceLabels.ToBool();
                    this.ReprintLabelAtDispatch = this.selectedInventoryItem.Master.ReprintLabelAtDispatch.ToBool();
                    this.WeightRequired = this.selectedInventoryItem.Master.WeightRequired;
                    this.AccumulateTares = this.selectedInventoryItem.Master.AccumulateTares.ToBool();
                    this.IsStockItem = this.selectedInventoryItem.Master.StockItem.ToBool();
                    this.IsSalesItem = this.selectedInventoryItem.Master.SalesItem.ToBool();
                    this.IsPurchaseItem = this.selectedInventoryItem.Master.PurchaseItem.ToBool();
                    this.IsFixedAsset = this.selectedInventoryItem.Master.FixedAsset.ToBool();
                    this.IsProductionProduct = this.selectedInventoryItem.Master.ProductionProduct.ToBool();
                    this.IsReceipeProduct = this.selectedInventoryItem.Master.ReceipeProduct.ToBool();
                    this.SalesNominalCode = this.selectedInventoryItem.Master.SalesNominalCode;
                    this.SalesNominalDeptCode = this.selectedInventoryItem.Master.SalesNominalDeptCode;
                    this.PurchaseNominalCode = this.selectedInventoryItem.Master.PurchaseNominalCode;
                    this.PurchaseNominalDeptCode = this.selectedInventoryItem.Master.PurchaseNominalDeptCode;
                    this.InventoryNotes = this.selectedInventoryItem.Master.Remarks;
                    this.NominalWeight = this.selectedInventoryItem.Master.NominalWeight.ToString();
                    this.MinWeight = this.selectedInventoryItem.Master.MinWeight.ToString();
                    this.MaxWeight = this.selectedInventoryItem.Master.MaxWeight.ToString();
                    this.ActiveFromDate = this.selectedInventoryItem.Master.ActiveFrom;
                    this.ActiveToDate = this.selectedInventoryItem.Master.ActiveTo;
                    this.InactiveFromDate = this.selectedInventoryItem.Master.InActiveFrom;
                    this.InactiveToDate = this.selectedInventoryItem.Master.InActiveTo;
                    this.TypicalPieces = this.selectedInventoryItem.Master.TypicalPieces.ToInt();
                    this.IsDeductionProduct = this.selectedInventoryItem.Master.IsDeduction.ToBool();
                    this.DeductionRate = this.selectedInventoryItem.Master.DeductionRate.ToString();
                    this.ApplyDeductionToDocket = this.selectedInventoryItem.Master.ApplyDeductionToDocket.ToBool();
                    this.QtyPerBox = this.selectedInventoryItem.Master.QtyPerBox.ToInt();
                    this.MinimumStockLevel = this.selectedInventoryItem.Master.MinimumStockLevel;
                    this.OrderLeadTime = this.selectedInventoryItem.Master.OrderLeadTime;
                    this.PurchaseOrderToCopyFrom = this.selectedInventoryItem.Master.APOrderID_Copy;
                    this.ProductionOrderToCopyFrom = this.selectedInventoryItem.Master.PROrderID_Copy;
                    this.ProductionOrderReference = this.selectedInventoryItem.Master.Reference;
                    this.ItemPhoto = this.selectedInventoryItem.Master.ItemImage ?? new Uri("pack://application:,,,/Design/Image/NoImageAvailable.jpg").UriToBytes();
                    if (this.InactiveFromDate != null && this.InactiveToDate != null &&
                        DateTime.Today >= this.InactiveFromDate && DateTime.Today <= this.InactiveToDate)
                    {
                        this.CheckBoxInactive = true;
                    }

                    this.SelectedPriceList = this.PriceLists.FirstOrDefault(x => x.PriceListID == NouvemGlobal.BasePriceList.PriceListID);
                    this.EntitySelectionChange = true;

                    if (this.selectedInventoryItem.StockMode != null)
                    {
                        this.StockMode = this.StockModes.FirstOrDefault(x => x.NouStockModeID == this.selectedInventoryItem.StockMode.NouStockModeID);
                    }

                    if (this.selectedInventoryItem.Master.BPMasterID.HasValue)
                    {
                        this.SelectedPartner = this.BusinessPartners.FirstOrDefault(x =>
                            x.BPMasterID == this.selectedInventoryItem.Master.BPMasterID);
                    }
                    else
                    {
                        this.SelectedPartner = this.BusinessPartners.FirstOrDefault();
                    }

                    if (this.selectedInventoryItem.Master.NouPriceMethodID == null)
                    {
                        this.SelectedPriceMethod = this.PriceMethods.FirstOrDefault();
                    }
                    else
                    {
                        this.SelectedPriceMethod =
                            this.PriceMethods.FirstOrDefault(
                                x => x.NouPriceMethodID == this.selectedInventoryItem.Master.NouPriceMethodID);
                    }

                    if (this.selectedInventoryItem.Master.NouOrderMethodID == null)
                    {
                        this.SelectedOrderMethod = this.OrderMethods.FirstOrDefault();
                    }
                    else
                    {
                        this.SelectedOrderMethod =
                            this.OrderMethods.FirstOrDefault(
                                x => x.NouOrderMethodID == this.selectedInventoryItem.Master.NouOrderMethodID);
                    }

                    this.CanChangeStockMode = true;
                    if (this.selectedInventoryItem.Master.INMasterID > 0 && this.StockMode != null)
                    {
                        // if there are transactions recorded against this product, then stock mode cannot be changed.
                        this.CanChangeStockMode = !this.DataManager.AreTransactionsRecordedAgainstProduct(this.selectedInventoryItem.Master.INMasterID);
                    }

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.GetProductStockData();
                    }));
                }

                this.SelectedLinkedScales =
                    this.Scales.FirstOrDefault(x => x.ID == this.selectedInventoryItem.Master.Scales);

                if (this.selectedInventoryItem.Group != null)
                {
                    this.InventoryGroup = this.InventoryGroups.FirstOrDefault(x => x.INGroupID == this.selectedInventoryItem.Group.INGroupID);
                }

                if (this.selectedInventoryItem.TraceabilityTemplate != null)
                {
                    this.SelectedTraceabilityTemplateName =
                        this.TraceabilityTemplateNames.FirstOrDefault(
                            x =>
                                x.AttributeTemplateID ==
                                this.selectedInventoryItem.TraceabilityTemplate.AttributeTemplateID);
                }
                else
                {
                    this.SelectedTraceabilityTemplateName = null;
                }

                if (this.InventoryGroup != null && this.selectedInventoryItem?.TraceabilityTemplate != null &&
                    this.InventoryGroup.AttributeTemplateID !=
                    this.selectedInventoryItem.TraceabilityTemplate.AttributeTemplateID)
                {
                    this.ManualTraceabilityTemplate = true;
                }

                if (this.selectedInventoryItem.VatCode != null)
                {
                    this.SelectedVATCode =
                        this.VatCodes.FirstOrDefault(
                            x =>
                                x.VATCodeID == this.selectedInventoryItem.VatCode.VATCodeID &&
                                x.Code != Strings.DefineNew);
                }
                else
                {
                    this.SelectedVATCode = null;
                }

                if (this.selectedInventoryItem.Department != null)
                {
                    this.SelectedDepartment =
                        this.Departments.FirstOrDefault(
                            x => x.DepartmentID == this.selectedInventoryItem.Department.DepartmentID);
                }
                else
                {
                    this.SelectedDepartment = null;
                }

                if (this.selectedInventoryItem.BoxTareContainer != null)
                {
                    this.SelectedBoxContainerType = this.Containers.FirstOrDefault(x => x.ContainerID == this.selectedInventoryItem.BoxTareContainer.ContainerID);
                }
                else
                {
                    this.SelectedBoxContainerType =
                        this.Containers.FirstOrDefault(x => x.Name == string.Empty && x.Tare <= 0);
                }

                if (this.selectedInventoryItem.PiecesTareContainer != null)
                {
                    this.SelectedPiecesContainerType = this.Containers.FirstOrDefault(x => x.ContainerID == this.selectedInventoryItem.PiecesTareContainer.ContainerID);
                }
                else
                {
                    this.SelectedPiecesContainerType =
                        this.Containers.FirstOrDefault(x => x.Name == string.Empty && x.Tare <= 0);
                }

                if (this.selectedInventoryItem.PalletTareContainer != null)
                {
                    this.SelectedPalletContainerType =
                        this.Containers.FirstOrDefault(
                            x => x.ContainerID == this.selectedInventoryItem.PalletTareContainer.ContainerID);
                }
                else
                {
                    this.SelectedPalletContainerType =
                        this.Containers.FirstOrDefault(x => x.Name == string.Empty && x.Tare <= 0);
                }

                if (this.selectedInventoryItem.LinkedProductTareContainer != null)
                {
                    this.SelectedLinkedProductContainerType =
                        this.Containers.FirstOrDefault(
                            x => x.ContainerID == this.selectedInventoryItem.LinkedProductTareContainer.ContainerID);
                }
                else
                {
                    this.SelectedLinkedProductContainerType =
                        this.Containers.FirstOrDefault(x => x.Name == string.Empty && x.Tare <= 0);
                }

                if (this.SelectedInventoryItem.Master.WarehouseID == null)
                {
                    this.SelectedStockLocation = this.StockLocations.First(x => x.Name.Equals(Strings.TerminalLocation));
                }
                else
                {
                    this.SelectedStockLocation =
                        this.StockLocations.FirstOrDefault(
                            x => x.WarehouseID == this.SelectedInventoryItem.Master.WarehouseID);
                }

                // get the associated item attachments
                this.allInventoryAttachments =
                    this.DataManager.GetInventoryAttachments(this.selectedInventoryItem.Master.INMasterID);
                this.InventoryAttachments = new ObservableCollection<INAttachment>(this.allInventoryAttachments);

                // get the associated item properties
                var propertIds = NouvemGlobal.INPropertySelections
                    .Where(x => x.INMasterID == this.selectedInventoryItem.Master.INMasterID)
                    .Select(property => property.INPropertyID);

                this.InventoryProperties = new ObservableCollection<InventoryProperty>();

                foreach (var property in NouvemGlobal.INProperties)
                {
                    this.InventoryProperties.Add(new InventoryProperty { Details = property, IsSelected = propertIds.Contains(property.INPropertyID) });
                }

                #region label

                this.LabelText1 = string.Empty;
                this.LabelText2 = string.Empty;
                this.LabelText3 = string.Empty;
                this.LabelText4 = string.Empty;
                this.LabelText5 = string.Empty;
                this.LabelText6 = string.Empty;
                this.LabelText7 = string.Empty;
                this.LabelText8 = string.Empty;
                this.LabelText9 = string.Empty;
                this.LabelText10 = string.Empty;
                this.LabelText11 = string.Empty;
                this.LabelText12 = string.Empty;
                this.LabelText13 = string.Empty;
                this.LabelText14 = string.Empty;
                this.LabelText15 = string.Empty;
                this.LabelText16 = string.Empty;
                this.LabelText17 = string.Empty;
                this.LabelText18 = string.Empty;
                this.LabelText19 = string.Empty;
                this.LabelText20 = string.Empty;
                this.LabelText21 = string.Empty;
                this.LabelText22 = string.Empty;
                this.LabelText23 = string.Empty;
                this.LabelText24 = string.Empty;
                this.LabelText25 = string.Empty;
                this.LabelText26 = string.Empty;
                this.LabelText27 = string.Empty;
                this.LabelText28 = string.Empty;
                this.LabelText29 = string.Empty;
                this.LabelText30 = string.Empty;
                this.LabelMultiText1 = string.Empty;
                this.LabelMultiText2 = string.Empty;
                this.Barcode1 = string.Empty;
                this.Barcode2 = string.Empty;
                this.Barcode3 = string.Empty;
                this.Barcode4 = string.Empty;

                if (this.selectedInventoryItem.LabelField != null)
                {
                    this.LabelText1 = this.selectedInventoryItem.LabelField.Field1;
                    this.LabelText2 = this.selectedInventoryItem.LabelField.Field2;
                    this.LabelText3 = this.selectedInventoryItem.LabelField.Field3;
                    this.LabelText4 = this.selectedInventoryItem.LabelField.Field4;
                    this.LabelText5 = this.selectedInventoryItem.LabelField.Field5;
                    this.LabelText6 = this.selectedInventoryItem.LabelField.Field6;
                    this.LabelText7 = this.selectedInventoryItem.LabelField.Field7;
                    this.LabelText8 = this.selectedInventoryItem.LabelField.Field8;
                    this.LabelText9 = this.selectedInventoryItem.LabelField.Field9;
                    this.LabelText10 = this.selectedInventoryItem.LabelField.Field10;
                    this.LabelText11 = this.selectedInventoryItem.LabelField.Field11;
                    this.LabelText12 = this.selectedInventoryItem.LabelField.Field12;
                    this.LabelText13 = this.selectedInventoryItem.LabelField.Field13;
                    this.LabelText14 = this.selectedInventoryItem.LabelField.Field14;
                    this.LabelText15 = this.selectedInventoryItem.LabelField.Field15;
                    this.LabelText16 = this.selectedInventoryItem.LabelField.Field16;
                    this.LabelText17 = this.selectedInventoryItem.LabelField.Field17;
                    this.LabelText18 = this.selectedInventoryItem.LabelField.Field18;
                    this.LabelText19 = this.selectedInventoryItem.LabelField.Field19;
                    this.LabelText20 = this.selectedInventoryItem.LabelField.Field20;
                    this.LabelText21 = this.selectedInventoryItem.LabelField.Field21;
                    this.LabelText22 = this.selectedInventoryItem.LabelField.Field22;
                    this.LabelText23 = this.selectedInventoryItem.LabelField.Field23;
                    this.LabelText24 = this.selectedInventoryItem.LabelField.Field24;
                    this.LabelText25 = this.selectedInventoryItem.LabelField.Field25;
                    this.LabelText26 = this.selectedInventoryItem.LabelField.Field26;
                    this.LabelText27 = this.selectedInventoryItem.LabelField.Field27;
                    this.LabelText28 = this.selectedInventoryItem.LabelField.Field28;
                    this.LabelText29 = this.selectedInventoryItem.LabelField.Field29;
                    this.LabelText30 = this.selectedInventoryItem.LabelField.Field30;
                    this.LabelMultiText1 = this.selectedInventoryItem.LabelField.MultiLineField1;
                    this.LabelMultiText2 = this.selectedInventoryItem.LabelField.MultiLineField2;
                    this.Barcode1 = this.selectedInventoryItem.LabelField.Barcode1;
                    this.Barcode2 = this.selectedInventoryItem.LabelField.Barcode2;
                    this.Barcode3 = this.selectedInventoryItem.LabelField.Barcode3;
                    this.Barcode4 = this.selectedInventoryItem.LabelField.Barcode4;
                }

                #endregion
            }
            finally
            {
                this.EntitySelectionChange = false;
                this.SetControlMode(ControlMode.OK);
                this.SearchMode = false;
            }
        }

        /// <summary>
        /// Gets the application vat codes.
        /// </summary>
        private void GetVatCodes()
        {
            this.VatCodes = new ObservableCollection<VATCode>(this.DataManager.GetVATCodes());
            this.VatCodes.Add(new VATCode { Code = Strings.DefineNew });
        }

        /// <summary>
        /// Gets the application departments.
        /// </summary>
        private void GetDepartments()
        {
            this.Departments = new ObservableCollection<Department>(this.DataManager.GetDepartments().OrderBy(x => x.Name));
        }

        /// <summary>
        /// Gets the application departments.
        /// </summary>
        private void GetPriceLists()
        {
            this.PriceLists = new ObservableCollection<PriceList>(NouvemGlobal.PriceLists);
            if (NouvemGlobal.SpecialPriceList != null)
            {
                var specPrice = this.PriceLists.FirstOrDefault(x => x.PriceListID == NouvemGlobal.SpecialPriceList.PriceListID);
                if (specPrice != null)
                {
                    this.PriceLists.Remove(specPrice);
                }
            }

            this.SelectedPriceList = this.PriceLists.FirstOrDefault(x => x.PriceListID == NouvemGlobal.BasePriceList.PriceListID);
        }

        /// <summary>
        /// Gets the application properties.
        /// </summary>
        private void GetProperties()
        {
            this.INProperties = new List<INProperty>(this.DataManager.GetItemMasterProperties());
            this.INProperties.ToList().ForEach(x => this.InventoryProperties.Add(new InventoryProperty { Details = x, IsSelected = false }));
        }

        /// <summary>
        /// Gets the application containers.
        /// </summary>
        private void GetContainers()
        {
            this.Containers = new ObservableCollection<Container>(this.DataManager.GetContainers());
            this.Containers.Add(new Container { Name = string.Empty, Tare = 0 });
            this.Containers.Add(new Container { Name = Strings.DefineNew });
        }

        /// <summary>
        /// Gets the traceability template names.
        /// </summary>
        private void GetTraceabilityTemplateNames()
        {
            this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplate>(this.DataManager.GetAttributeTemplateNames());
        }

        /// <summary>
        /// Gets the traceability template allocations.
        /// </summary>
        private void GetTraceabilityTemplateAllocations()
        {
            this.allTraceabilityTemplateAllocations = NouvemGlobal.AttributeAllocationData; // this.DataManager.GetAttributeAllocations();
        }

        /// <summary>
        /// Get the selected products stock data.
        /// </summary>
        private void GetProductStockData()
        {
            if (this.selectedInventoryItem?.Master != null && this.selectedInventoryItem.Master.INMasterID > 0)
            {
                if (this.ShowAll)
                {
                    this.ProductStockData = new ObservableCollection<ProductStockData>(
                        this.DataManager.GetProductStockData(this.selectedInventoryItem.Master.INMasterID));
                }
                else
                {
                    if (this.StockMode == null || this.StockMode.NouStockModeID == 1)
                    {
                        this.ProductStockData = new ObservableCollection<ProductStockData>(
                            this.DataManager.GetProductStockData(this.selectedInventoryItem.Master.INMasterID));
                    }
                    else if (this.StockMode.NouStockModeID == 4 || this.StockMode.NouStockModeID == 7)
                    {
                        this.ProductStockData = new ObservableCollection<ProductStockData>(
                            this.DataManager.GetProductStockData(this.selectedInventoryItem.Master.INMasterID).Where(x => x.ProductStock.Qty.ToDecimal() > 0));
                    }
                    else if (this.StockMode.NouStockModeID == 5 || this.StockMode.NouStockModeID == 8)
                    {
                        this.ProductStockData = new ObservableCollection<ProductStockData>(
                            this.DataManager.GetProductStockData(this.selectedInventoryItem.Master.INMasterID).Where(x => x.ProductStock.Wgt.ToDecimal() > 0));
                    }
                    else if (this.StockMode.NouStockModeID == 6 || this.StockMode.NouStockModeID == 9)
                    {
                        this.ProductStockData = new ObservableCollection<ProductStockData>(
                            this.DataManager.GetProductStockData(this.selectedInventoryItem.Master.INMasterID).Where(x => x.ProductStock.Qty.ToDecimal() > 0 || x.ProductStock.Wgt.ToDecimal() > 0));
                    }
                }

                var wgt = this.ProductStockData.Sum(x => x.ProductStock.Wgt);
                var qty = this.ProductStockData.Sum(x => x.ProductStock.Qty);
                this.MRPStockData = new ObservableCollection<ProductStockData>(this.DataManager.GetMRPData(this.selectedInventoryItem.Master.INMasterID, wgt.ToDecimal(), qty.ToDecimal()));
            }
        }

        /// <summary>
        /// Gets the application stock modes.
        /// </summary>
        private void GetStockModes()
        {
            this.StockModes = new ObservableCollection<NouStockMode>(this.DataManager.GetStockModes());
        }

        /// <summary>
        /// Gets the application inventory groups.
        /// </summary>
        private void GetInventoryGroups()
        {
            this.InventoryGroups.Clear();
            this.InventoryGroups.Add(new INGroup { Name = Strings.NoneSelected });
            foreach (var group in this.DataManager.GetInventoryGroups().OrderBy(x => x.Name))
            {
                this.InventoryGroups.Add(group);
            }

            this.InventoryGroup = this.InventoryGroups.FirstOrDefault(x => x.Name.Equals(Strings.NoneSelected));
        }

        /// <summary>
        /// Refresh the local data.
        /// </summary>
        private void RefreshData()
        {
            this.GetDateDays();
            NouvemGlobal.GetInventoryItems();
        }

        /// <summary>
        /// Gets the inventory items.
        /// </summary>
        private void GetInventoryItems()
        {
            var activeItems = NouvemGlobal.InventoryItems.Where(x => x.Master.Deleted == null && (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                           || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo));
            //var activeItems = NouvemGlobal.InventoryItems.Where(x => x.Master.Deleted == null);
            this.InventoryItems = new ObservableCollection<InventoryItem>(activeItems);
        }

        /// <summary>
        /// Update the current traceability template allocation to corrrespond with the selected traceability template name.
        /// </summary>
        private void SetTraceabilityAllocations()
        {
            this.TraceabilityTemplateAllocations.Clear();
            foreach (var template in this.allTraceabilityTemplateAllocations.Where(x => x.AttributeTemplate != null && x.AttributionAllocation.AttributeTemplateID
                == this.selectedTraceabilityTemplateName.AttributeTemplateID))
            {
                var dateDay = new DateDay();
                if (this.selectedInventoryItem != null)
                {
                    var localDateDay = this.dateDays.FirstOrDefault(x =>
                        x.INMasterID == this.selectedInventoryItem.Master.INMasterID &&
                        x.AttributeMasterID == template.AttributeMaster.AttributeMasterID);

                    if (localDateDay != null)
                    {
                        dateDay = localDateDay;
                    }
                }

                template.DateDay = dateDay;
                this.TraceabilityTemplateAllocations.Add(template);
            }
        }

        /// <summary>
        /// Gets the application order methods.
        /// </summary>
        private void GetPriceMethods()
        {
            this.PriceMethods = new ObservableCollection<NouPriceMethod>(this.DataManager.GetPriceMethods());
            this.PriceMethods.Insert(0, new NouPriceMethod { Name = Strings.NoneSelected });
        }

        /// <summary>
        /// Gets the application order methods.
        /// </summary>
        private void GetOrderMethods()
        {
            this.OrderMethods = this.DataManager.GetOrderMethods();
            this.OrderMethods.Insert(0, new NouOrderMethod { Name = Strings.NoneSelected });
        }

        /// <summary>
        /// Handles the inventory group change, by selected it's associated templates.
        /// </summary>
        private void DisplayGroupTemplates()
        {
            if (this.InventoryGroup == null)
            {
                return;
            }

            // traceability
            if (this.InventoryGroup.AttributeTemplateID.IsNullOrZero())
            {
                // no associated traceability template, so clear.
                this.SelectedTraceabilityTemplateName = null;
                this.TraceabilityTemplateAllocations.Clear();
            }
            else
            {
                // apply the traceability template data
                this.SelectedTraceabilityTemplateName
                    = this.TraceabilityTemplateNames.FirstOrDefault(x => x.AttributeTemplateID == this.InventoryGroup.AttributeTemplateID);
            }
        }

        /// <summary>
        /// Reset the static inventory totals.
        /// </summary>
        private void ResetInventoryTotals()
        {
            Model.BusinessObject.ProductStockData.CommittedStockQty = 0;
            Model.BusinessObject.ProductStockData.CommittedStockWgt = 0;
            Model.BusinessObject.ProductStockData.StockWgt = 0;
            Model.BusinessObject.ProductStockData.StockQty = 0;
        }

        /// <summary>
        /// Handles the changing of a price list.
        /// </summary>
        private void HandleSelectedPriceList()
        {
            if (this.selectedInventoryItem == null || this.selectedInventoryItem.Master.INMasterID == 0)
            {
                return;
            }

            this.EntitySelectionChange = true;

            // reset the price first.
            this.PriceListItemPrice = null;

            try
            {
                // get the price for the selected item matching the selected price list.
                this.selectedItemPrice = this.DataManager.GetPriceListDetail(this.SelectedPriceList.PriceListID,
                    this.selectedInventoryItem.Master.INMasterID);

                if (this.selectedItemPrice != null)
                {
                    //if (this.selectedItemPrice.Price > 0)
                    //{
                        this.PriceListItemPrice = this.selectedItemPrice.Price;
                    //}
                }
                else
                {
                    // ask the user if they want to add the product to the price list
                    //NouvemMessageBox.Show(Message.AddProductToPriceLIstConfirmation, NouvemMessageBoxButtons.YesNo);
                    //if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    //{
                    //    Messenger.Default.Send(ViewType.PriceListDetail);
                    //    this.Locator.PriceListDetail.AddProductToPriceListDetail(this.SelectedPriceList.PriceListID, this.selectedInventoryItem.Master);
                    //}
                }
            }
            finally
            {
                this.EntitySelectionChange = false;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearInventoryMaster();
            Messenger.Default.Send(Token.Message, Token.CloseINMasterWindow);
        }

        /// <summary>
        /// Gets the date days.
        /// </summary>
        private void GetDateDays()
        {
            this.dateDays = this.DataManager.GetDateDays();
        }

        #endregion

        #endregion
    }
}



