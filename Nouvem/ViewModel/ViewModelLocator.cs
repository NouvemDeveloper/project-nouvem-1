// -----------------------------------------------------------------------
// <copyright file="ViewModelLocator.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Nouvem"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using Nouvem.Global;
using Nouvem.ViewModel.Attribute;
using Nouvem.ViewModel.Audit;
using Nouvem.ViewModel.Currency;
using Nouvem.ViewModel.Document;
using Nouvem.ViewModel.KillLine.Grader;
using Nouvem.ViewModel.KillLine.Lairage;
using Nouvem.ViewModel.KillLine.Sequencer;
using Nouvem.ViewModel.Map;
using Nouvem.ViewModel.Payments;
using Nouvem.ViewModel.Purchases.APOrder;
using Nouvem.ViewModel.Purchases.APQuote;
using Nouvem.ViewModel.Purchases.APReceipt;
using Nouvem.ViewModel.Quality;
using Nouvem.ViewModel.Sales;
using Nouvem.ViewModel.Sales.AROrder;
using Nouvem.ViewModel.Sales.ARQuote;
using Nouvem.ViewModel.Plant;
using Nouvem.ViewModel.Production;
using Nouvem.ViewModel.Production.IntoProduction;
using Nouvem.ViewModel.Production.OutOfProduction;
using Nouvem.ViewModel.Production.Receipe;
using Nouvem.ViewModel.Purchases.APInvoice;
using Nouvem.ViewModel.Sales.ARReturn;
using Nouvem.ViewModel.Region;
using Nouvem.ViewModel.Report;
using Nouvem.ViewModel.Route;
using Nouvem.ViewModel.Sales.ARDispatch;
using Nouvem.ViewModel.Sales.ARInvoice;
using Nouvem.ViewModel.Scanner;
using Nouvem.ViewModel.Stock;
using Nouvem.ViewModel.Telesales;
using Nouvem.ViewModel.Touchscreen;
using Nouvem.ViewModel.Transaction;
using Nouvem.ViewModel.Utility;
using Nouvem.ViewModel.Workflow;
using Nouvem.ViewModel.Workflow.Response;

namespace Nouvem.ViewModel
{
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Ioc;
    using Nouvem.ViewModel.Account;
    using Nouvem.ViewModel.BusinessPartner;
    using Nouvem.ViewModel.Container;
    using Nouvem.ViewModel.Country;
    using Nouvem.ViewModel.Date;
    using Nouvem.ViewModel.EPOS;
    using Nouvem.ViewModel.Exit;
    using Nouvem.ViewModel.Inventory;
    using Nouvem.ViewModel.Label;
    using Nouvem.ViewModel.License;
    using Nouvem.ViewModel.Logic;
    using Nouvem.ViewModel.Master;
    using Nouvem.ViewModel.Plant;
    using Nouvem.ViewModel.Pricing;
    using Nouvem.ViewModel.StartUp;
    using Nouvem.ViewModel.Traceability;
    using Nouvem.ViewModel.UOM;
    using Nouvem.ViewModel.User;
    using Nouvem.ViewModel.UserInput;

    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        #region field

        /// <summary>
        /// The message box view model reference.
        /// </summary>
        private static NouvemMessageBoxViewModel messageBox = new NouvemMessageBoxViewModel();

        /// <summary>
        /// The login view model reference.
        /// </summary>
        private static LoginViewModel loginViewModel;

        /// <summary>
        /// The sequencer view model reference.
        /// </summary>
        private static ScannerSequencerMainViewModel scannerSequencerMainViewModel;

        /// <summary>
        /// The sequencer view model reference.
        /// </summary>
        private static ScannerSequencerVerificationViewModel scannerSequencerVerificationViewModel;

        /// <summary>
        /// The login view model reference.
        /// </summary>
        //private static ReceipeViewModel receipeViewModel;

        /// <summary>
        /// The tele sales set up view model reference.
        /// </summary>
        private static TelesalesSetUpViewModel telesalesSetUpViewModel;

        /// <summary>
        /// The sequencer view model reference.
        /// </summary>
        private static ScannerSequencerViewModel scannerSequencerViewModel;

        /// <summary>
        /// The login view model reference.
        /// </summary>
        private static BeefTopViewModel beefTopViewModel;

        /// <summary>
        /// The login view model reference.
        /// </summary>
        private static CreditNoteCreationViewModel creditNoteCreationViewModel;

        /// <summary>
        /// The stock movement view model reference.
        /// </summary>
        private static StockMovementPanelViewModel stockMovementPanelViewModel;

        /// <summary>
        /// The login view model reference.
        /// </summary>
        private static SheepTopViewModel sheepTopViewModel;

        /// <summary>
        /// The login user view model reference.
        /// </summary>
        private static LoginUserViewModel loginUserViewModel;

        /// <summary>
        /// The contact view model reference.
        /// </summary>
        private static MasterViewModel masterViewModel;

        /// <summary>
        /// The login view model reference.
        /// </summary>
        private static BatchEditViewModel batchEditViewModel;

        /// <summary>
        /// The login view model reference.
        /// </summary>
        private static IntoProductionMainGridViewModel intoProductionMainGridViewModel;

        /// <summary>
        /// The login view model reference.
        /// </summary>
        private static IntoProductionConformanceViewModel intoProductionConformanceViewModel;

        /// <summary>
        /// The login view model reference.
        /// </summary>
        private static NeedleViewModel needleViewModel;

        /// <summary>
        /// The contact view model reference.
        /// </summary>
        private static AlertUsersSetUpViewModel alertUsersSetUpViewModel;

        /// <summary>
        /// The entry view model reference.
        /// </summary>
        private static EntryViewModel entryViewModel;

        /// <summary>
        /// The entry view model reference.
        /// </summary>
        private static SpecificationsViewModel specificationsViewModel;

        /// <summary>
        /// The entry view model reference.
        /// </summary>
        private static BatchSetUpViewModel batchSetUpViewModel;

        /// <summary>
        /// The entry view model reference.
        /// </summary>
        private static ExitViewModel exitViewModel;

        /// <summary>
        /// The login user view model reference.
        /// </summary>
        private static StockMoveGridViewModel stockMoveGridViewModel;

        /// <summary>
        /// The login user view model reference.
        /// </summary>
        private static StockMoveGroupedGridViewModel stockMoveGroupedGridViewModel;

        private static StockMoveBatchGridViewModel stockMoveBatchGridViewModel;

        /// <summary>
        /// The attribute master set up view model reference.
        /// </summary>
        private static AttributeMasterSetUpViewModel attributeMasterSetUpViewModel;

        /// <summary>
        /// The attribute master set up view model reference.
        /// </summary>
        private static AttributeSearchViewModel attributeSearchViewModel;

        /// <summary>
        /// The entry view model reference.
        /// </summary>
        private static DetainCondemnViewModel detainCondemnViewModel;

        /// <summary>
        /// The entry view model reference.
        /// </summary>
        private static DispatchGridViewModel dispatchGridViewModel;

        /// <summary>
        /// The entry view model reference.
        /// </summary>
        private static DispatchCountViewModel dispatchCountViewModel;

        /// <summary>
        /// The entry view model reference.
        /// </summary>
        private static DispatchTransactionsViewModel dispatchTransactionsViewModel;

        /// <summary>
        /// The stock take view model reference.
        /// </summary>
        private static StockTakeViewModel stockTakeViewModel;

        /// <summary>
        /// The payment deductions view model reference.
        /// </summary>
        private static PaymentDeductionsViewModel paymentDeductionsViewModel;

        /// <summary>
        /// The payment view model reference.
        /// </summary>
        private static PaymentViewModel paymentViewModel;

        /// <summary>
        /// The tare calculator view model reference.
        /// </summary>
        private static TareCalculatorViewModel tareCalculatorViewModel;

        /// <summary>
        /// The special prices view model reference.
        /// </summary>
        private static SpecialPricesViewModel specialPricesViewModel;

        /// <summary>
        /// The special prices view model reference.
        /// </summary>
        private static AllSalesViewModel allSalesViewModel;

        /// <summary>
        /// The special prices view model reference.
        /// </summary>
        private static DispatchContainerViewModel dispatchContainerViewModel;

        /// <summary>
        /// The stock movement view model reference.
        /// </summary>
        private static StockMovementViewModel stockMovementViewModel;

        /// <summary>
        /// The label selection view model reference.
        /// </summary>
        private static LabelSelectionViewModel labelSelectionViewModel;

        /// <summary>
        /// The label selection view model reference.
        /// </summary>
        private static ReportSearchViewModel reportSearchViewModel;

        /// <summary>
        /// The label selection view model reference.
        /// </summary>
        private static ReportSetUpViewModel reportSetUpViewModel;

        /// <summary>
        /// The label selection view model reference.
        /// </summary>
        private static ReportFoldersViewModel reportFoldersViewModel;

        /// <summary>
        /// The warehouse selection view model reference.
        /// </summary>
        private static WarehouseSelectionViewModel warehouseSelectionViewModel;

        /// <summary>
        /// The lairage view model reference.
        /// </summary>
        private static LairageViewModel lairageViewModel;

        // <summary>
        /// The lairage view model reference.
        /// </summary>
        private static LairageGraderComboViewModel lairageGraderComboViewModel;

        /// <summary>
        /// The lairage view model reference.
        /// </summary>
        private static LairageTouchscreenViewModel lairageTouchscreenViewModel;

        /// <summary>
        /// The lairage view model reference.
        /// </summary>
        private static LairageSearchViewModel lairageSearchViewModel;

        /// <summary>
        /// The sync partners view model reference.
        /// </summary>
        private static SyncPartnersViewModel syncPartnersViewModel;

        /// <summary>
        /// The report viewer view model reference.
        /// </summary>
        private static ReportViewerViewModel reportViewerViewModel;

        /// <summary>
        /// The login selection view model reference.
        /// </summary>
        private static LoginSelectionViewModel loginSelectionViewModel;

        /// <summary>
        /// The stock movement view model reference.
        /// </summary>
        private static StockMovementTouchscreenViewModel stockMovementTouchscreenViewModel;

        /// <summary>
        /// The blank view model reference.
        /// </summary>
        private static BlankViewModel blankViewModel;

        /// <summary>
        /// The blank view model reference.
        /// </summary>
        private static TelesalesViewModel telesalesViewModel;

        /// <summary>
        /// The blank view model reference.
        /// </summary>
        private static TemplateAllocationViewModel templateAllocationViewModel;
        
        /// <summary>
        /// The business partner master view model reference.
        /// </summary>
        private static BPMasterViewModel bpMasterViewModel;

        /// <summary>
        /// The label selection view model reference.
        /// </summary>
        private static ProcessSelectionViewModel processSelectionViewModel;

        /// <summary>
        /// The search data view model reference.
        /// </summary>
        private static BPSearchDataViewModel bpSearchDataViewModel;

        /// <summary>
        /// The system message view model reference.
        /// </summary>
        private static SystemMessageViewModel systemMessageViewModel;

        /// <summary>
        /// The grader view model reference.
        /// </summary>
        private static TelesalesSearchViewModel telesalesSearchViewModel;

        /// <summary>
        /// The grader view model reference.
        /// </summary>
        private static GraderViewModel graderViewModel;

        /// <summary>
        /// The grade view model reference.
        /// </summary>
        private static GradeViewModel gradeViewModel;

        /// <summary>
        /// The grade view model reference.
        /// </summary>
        private static PigViewModel pigViewModel;

        /// <summary>
        /// The grade historic view model reference.
        /// </summary>
        private static GradeHistoricViewModel gradeHistoricViewModel;

        /// <summary>
        /// The grade historic view model reference.
        /// </summary>
        private static GradeFivePointViewModel gradeFivePointViewModel;

        /// <summary>
        /// The grader panel view model reference.
        /// </summary>
        private static GraderPanelViewModel graderPanelViewModel;

        /// <summary>
        /// The grader panel view model reference.
        /// </summary>
        private static GraderPigPanelViewModel graderPigPanelViewModel;

        /// <summary>
        /// The sequencer view model reference.
        /// </summary>
        private static SequencerViewModel sequencerViewModel;

        /// <summary>
        /// The sequencer panel view model reference.
        /// </summary>
        private static SequencerPanelViewModel sequencerPanelViewModel;

        /// <summary>
        /// The sequencer panel view model reference.
        /// </summary>
        private static LairagePanelViewModel lairagePanelViewModel;

        /// <summary>
        /// The sequencer panel view model reference.
        /// </summary>
        private static SequencerSheepPanelViewModel sequencerSheepPanelViewModel;

        /// <summary>
        /// The sequencer main data view model reference.
        /// </summary>
        private static SequencerMainDataViewModel sequencerMainDataViewModel;

        /// <summary>
        /// The report product data view model reference.
        /// </summary>
        private static ReportProductDataViewModel reportProductDataViewModel;

        /// <summary>
        /// The report transaction data view model reference.
        /// </summary>
        private static ReportTransactionDataViewModel reportTransactionDataViewModel;

        /// <summary>
        /// The report transaction data view model reference.
        /// </summary>
        private static ReportTransactionViewModel reportTransactionViewModel;

        /// <summary>
        /// The scanner customers view model reference.
        /// </summary>
        private static ScannerCustomersViewModel scannerCustomersViewModel;

        /// <summary>
        /// The scanner customers view model reference.
        /// </summary>
        private static ScannerPalletisationViewModel scannerPalletisationViewModel;

        /// <summary>
        /// The splash screen view model reference.
        /// </summary>
        private static NouvemSplashScreenViewModel nouvemSplashScreenViewModel;

        /// <summary>
        /// The user master view model reference.
        /// </summary>
        private static UserMasterViewModel userMasterViewModel;

        /// <summary>
        /// The touchscreen dispatch panel view model reference.
        /// </summary>
        private static TouchscreenWarehouseViewModel touchscreenWarehouseViewModel;

        /// <summary>
        /// The search data view model reference.
        /// </summary>
        private static UserSearchDataViewModel userSearchDataViewModel;

        /// <summary>
        /// The user group view model reference.
        /// </summary>
        private static UserGroupViewModel userGroupViewModel;

        /// <summary>
        /// The user group authorisations view model reference.
        /// </summary>
        private static UserGroupAuthorisationsViewModel userGroupAuthorisationsViewModel;

        /// <summary>
        /// The password view model reference.
        /// </summary>
        private static PasswordEntryViewModel passwordEntry;

        /// <summary>
        /// The menu view model reference.
        /// </summary>
        private static MenuViewModel menuViewModel;

        /// <summary>
        /// The sql generator view model reference.
        /// </summary>
        private static SqlGeneratorViewModel sqlGeneratorViewModel;

        /// <summary>
        /// The data generator view model reference.
        /// </summary>
        private static DataGeneratorViewModel dataGeneratorViewModel;

        /// <summary>
        /// The server set up view model reference.
        /// </summary>
        private static ServerSetUpViewModel serverSetUpViewModel;

        /// <summary>
        /// The device set up view model reference.
        /// </summary>
        private static DeviceSetUpViewModel deviceSetUpViewModel;

        /// <summary>
        /// The touchscreen Production Orders view model reference.
        /// </summary>
        private static TouchscreenProductionOrdersViewModel touchscreenProductionOrdersViewModel;

        /// <summary>
        /// The touchscreen Production Orders view model reference.
        /// </summary>
        private static TouchscreenIntakeBatchesViewModel touchscreenIntakeBatchesViewModel;

        /// <summary>
        /// The touchscreen production panel view model reference.
        /// </summary>
        private static TouchscreenProductionButcheryPanelViewModel touchscreenProductionButcheryPanelViewModel;

        /// <summary>
        /// The touchscreen out of production panel view model reference.
        /// </summary>
        private static TouchscreenProductionPorkPanelViewModel touchscreenProductionPorkPanelViewModel;

        /// <summary>
        /// The device search view model reference.
        /// </summary>
        private static DeviceSearchViewModel deviceSearchViewModel;

        /// <summary>
        /// The transaction manager view model reference.
        /// </summary>
        private static TransactionManagerViewModel transactionManagerViewModel;

        /// <summary>
        /// The license admin view model reference.
        /// </summary>
        private static LicenseAdminViewModel licenseAdminViewModel;

        /// <summary>
        /// The label association view model reference.
        /// </summary>
        private static LabelAssociationViewModel labelAssociationViewModel;

        /// <summary>
        /// The license import view model reference.
        /// </summary>
        private static LicenseImportViewModel licenseImportViewModel;

        /// <summary>
        /// The GS1AI view model reference.
        /// </summary>
        private static GS1AIViewModel gS1AIViewModel;

        /// <summary>
        /// The currency view model reference.
        /// </summary>
        private static CurrencyViewModel currencyViewModel;

        /// <summary>
        /// The Traceability Master view model reference.
        /// </summary>
        private static TraceabilityMasterViewModel traceabilityMasterViewModel;

        /// <summary>
        /// The Traceability name view model reference.
        /// </summary>
        private static TraceabilityNameViewModel traceabilityNameViewModel;

        /// <summary>
        /// The stock reconciliation view model reference.
        /// </summary>
        private static StockReconciliationViewModel stockReconciliationViewModel;

        /// <summary>
        /// The traceability template allocation view model reference.
        /// </summary>
        private static TraceabilityTemplateAllocationViewModel traceabilityTemplateAllocationViewModel;

        /// <summary>
        /// The date view model reference.
        /// </summary>
        private static DateViewModel dateViewModel;

        /// <summary>
        /// The touch screen view model reference.
        /// </summary>
        private static TouchscreenViewModel touchscreenViewModel;

        /// <summary>
        /// The touch screen suppliers view model reference.
        /// </summary>
        private static TouchscreenSuppliersViewModel touchscreenSuppliersViewModel;

        /// <summary>
        /// The touch screen orders view model reference.
        /// </summary>
        private static TouchscreenOrdersViewModel touchscreenOrdersViewModel;

        /// <summary>
        /// The touch screen products view model reference.
        /// </summary>
        private static TouchscreenProductsViewModel touchscreenProductsViewModel;

        /// <summary>
        /// The touch screen products view model reference.
        /// </summary>
        private static TouchscreenProductsMasterViewModel touchscreenProductsMasterViewModel;

        /// <summary>
        /// The touch screen products view model reference.
        /// </summary>
        private static TouchscreenProductsExpandGroupsViewModel touchscreenProductsExpandGroupsViewModel;

        /// <summary>
        /// The touch screen containers view model reference.
        /// </summary>
        private static TouchscreenContainerViewModel touchscreenContainerViewModel;

        /// <summary>
        /// The touch screen containers view model reference.
        /// </summary>
        private static SystemInformationViewModel systemInformationViewModel;

        /// <summary>
        /// The factory screen view model reference.
        /// </summary>
        private static FactoryScreenViewModel factoryScreenViewModel;

        /// <summary>
        /// The quality view model reference.
        /// </summary>
        private static QualityViewModel qualityViewModel;

        /// <summary>
        /// The purchases quote view model reference.
        /// </summary>
        private static APQuoteViewModel apQuoteViewModel;

        /// <summary>
        /// The purchases order view model reference.
        /// </summary>
        private static APOrderViewModel apOrderViewModel;

        /// <summary>
        /// The purchases receipt view model reference.
        /// </summary>
        private static APReceiptViewModel apReceiptViewModel;

        /// <summary>
        /// The purchases receipt touchscreen view model reference.
        /// </summary>
        private static APReceiptTouchscreenViewModel apReceiptTouchscreenViewModel;

        /// <summary>
        /// The purchases receipt details view model reference.
        /// </summary>
        private static APReceiptDetailsViewModel apReceiptDetailsViewModel;

        /// <summary>
        /// The purchases receipt details edit view model reference.
        /// </summary>
        private static APReceiptDetailsEditViewModel apReceiptDetailsEditViewModel;

        /// <summary>
        /// The dispatch details edit view model reference.
        /// </summary>
        private static ARDispatchDetailsViewModel arDispatchDetailsViewModel;

        /// <summary>
        /// The production details edit view model reference.
        /// </summary>
        //private static ProductionDetailsViewModel productionDetailsViewModel;

        /// <summary>
        /// The collection display view model reference.
        /// </summary>
        private static CollectionDisplayViewModel collectionDisplayViewModel = new CollectionDisplayViewModel();

        /// <summary>
        /// The keyboard view model reference.
        /// </summary>
        private static KeyboardViewModel keyboardViewModel = new KeyboardViewModel();

        /// <summary>
        /// The map view model reference.
        /// </summary>
        private static MapViewModel mapViewModel;

        /// <summary>
        /// The template name view model reference.
        /// </summary>
        private static TemplateNameViewModel templateNameViewModel;

        /// <summary>
        /// The template group view model reference.
        /// </summary>
        private static TemplateGroupViewModel templateGroupViewModel;

        /// <summary>
        /// The template group view model reference.
        /// </summary>
        private static WeightBandGroupSetUpViewModel weightBandGroupSetUpViewModel;

        /// <summary>
        /// The template group view model reference.
        /// </summary>
        private static WeightBandSetUpViewModel weightBandSetUpViewModel;

        /// <summary>
        /// The template group view model reference.
        /// </summary>
        private static WeightBandPricingViewModel weightBandPricingViewModel;

        /// <summary>
        /// The date name view model reference.
        /// </summary>
        private static DateNameViewModel dateNameViewModel;

        /// <summary>
        /// The quality name view model reference.
        /// </summary>
        private static QualityNameViewModel qualityNameViewModel;

        /// <summary>
        /// The sales search data view model reference.
        /// </summary>
        private static SalesSearchDataViewModel salesSearchDataViewModel;

        /// <summary>
        /// The report sales search data view model reference.
        /// </summary>
        private static ReportSalesSearchDataViewModel reportSalesSearchDataViewModel;

        /// <summary>
        /// The report sales search data view model reference.
        /// </summary>
        private static TouchscreenReportSalesSearchDataViewModel touchscreenReportSalesSearchDataViewModel;

        /// <summary>
        /// The date template allocation view model reference.
        /// </summary>
        private static DateTemplateAllocationViewModel dateTemplateAllocationViewModel;

        /// <summary>
        /// The quality template allocation view model reference.
        /// </summary>
        private static QualityTemplateAllocationViewModel qualityTemplateAllocationViewModel;

        /// <summary>
        /// The country master view model reference.
        /// </summary>
        private static CountryMasterViewModel countryMasterViewModel;

        /// <summary>
        /// The container view model reference.
        /// </summary>
        private static ContainerViewModel containerViewModel;

        /// <summary>
        /// The warehouse view model reference.
        /// </summary>
        private static WarehouseViewModel warehouseViewModel;

        /// <summary>
        /// The warehouse location view model reference.
        /// </summary>
        private static WarehouseLocationViewModel warehouseLocationViewModel;

        /// <summary>
        /// The partner properties set up view model reference.
        /// </summary>
        private static BPPropertySetUpViewModel bPPropertySetUpViewModel;

        /// <summary>
        /// The item master properties set up view model reference.
        /// </summary>
        private static ItemMasterPropertySetUpViewModel itemMasterPropertySetUpViewModel;

        /// <summary>
        /// The plant view model reference.
        /// </summary>
        private static PlantViewModel plantViewModel;

        /// <summary>
        /// The uom view model reference.
        /// </summary>
        private static UOMViewModel uomViewModel;

        /// <summary>
        /// The pricing master view model reference.
        /// </summary>
        private static PrinterSelectionViewModel printerSelectionViewModel;

        /// <summary>
        /// The pricing master view model reference.
        /// </summary>
        private static PricingMasterViewModel pricingMasterViewModel;

        /// <summary>
        /// The price list detail view model reference.
        /// </summary>
        private static PriceListDetailViewModel priceListDetailViewModel;

        /// <summary>
        /// The product selection view model reference.
        /// </summary>
        private static ProductSelectionViewModel productSelectionViewModel;

        /// <summary>
        /// The user change password view model reference.
        /// </summary>
        private static UserChangePasswordViewModel userChangePasswordViewModel;

        /// <summary>
        /// The inventory master view model reference.
        /// </summary>
        private static InventoryMasterViewModel inventoryMasterViewModel;

        /// <summary>
        /// The inventory group view model reference.
        /// </summary>
        private static InventoryGroupViewModel inventoryGroupViewModel;

        /// <summary>
        /// The inventory group set up view model reference.
        /// </summary>
        private static InventoryGroupSetUpViewModel inventoryGroupSetUpViewModel;

        /// <summary>
        /// The accountsview model reference.
        /// </summary>
        private static AccountsViewModel accountsViewModel;

        /// <summary>
        /// The accountsview model reference.
        /// </summary>
        private static AccountsPurchasesViewModel accountsPurchasesViewModel;

        /// <summary>
        /// The inventory search data view model reference.
        /// </summary>
        private static InventorySearchDataViewModel inventorySearchDataViewModel;

        /// <summary>
        /// The epos view model reference.
        /// </summary>
        private static EposViewModel eposViewModel;

        /// <summary>
        /// The epos settings view model reference.
        /// </summary>
        private static EposSettingsViewModel eposSettingsViewModel;

        /// <summary>
        /// The epos loggedout view model reference.
        /// </summary>
        private static EposLoggedOutViewModel eposLoggedOutViewModel;

        /// <summary>
        /// The epos password change view model reference.
        /// </summary>
        private static EposPasswordChangeViewModel eposPasswordChangeViewModel;

        /// <summary>
        /// The vat set up view model reference.
        /// </summary>
        private static VatSetUpViewModel vatSetUpViewModel;

        /// <summary>
        /// The report view model reference.
        /// </summary>
        private static EposReportViewModel eposReportViewModel;

        /// <summary>
        /// The audit view model reference.
        /// </summary>
        private static AuditViewModel auditViewModel;

        /// <summary>
        /// The document type view model reference.
        /// </summary>
        private static DocumentTypeViewModel documentTypeViewModel;

        /// <summary>
        /// The document number view model reference.
        /// </summary>
        private static DocumentNumberViewModel documentNumberViewModel;

        /// <summary>
        /// The region view model reference.
        /// </summary>
        private static RegionViewModel regionViewModel;

        /// <summary>
        /// The ar dispatch view model reference.
        /// </summary>
        private static ARDispatchViewModel arDispatchViewModel;

        /// <summary>
        /// The ar dispatch view model reference.
        /// </summary>
        private static ARDispatch2ViewModel arDispatch2ViewModel;

        /// <summary>
        /// The scanner touchscren view model reference.
        /// </summary>
        private static ScannerTouchscreenViewModel scannerTouchscreenViewModel;

        /// <summary>
        /// The ar dispatch touchscreen view model reference.
        /// </summary>
        private static ARDispatchTouchscreenViewModel arDispatchTouchscreenViewModel;

        /// <summary>
        /// The route view model reference.
        /// </summary>
        private static RouteViewModel routeViewModel;

        /// <summary>
        /// The quote view model reference.
        /// </summary>
        private static QuoteViewModel quoteViewModel;

        /// <summary>
        /// The order view model reference.
        /// </summary>
        private static OrderViewModel orderViewModel;

        /// <summary>
        /// The lairage order view model reference.
        /// </summary>
        private static LairageOrderViewModel lairageOrderViewModel;

        /// <summary>
        /// The lairage order view model reference.
        /// </summary>
        private static LairageAnimalsSearchViewModel lairageAnimalsSearchViewModel;

        /// <summary>
        /// The order view model reference.
        /// </summary>
        private static Order2ViewModel order2ViewModel;

        /// <summary>
        /// The invoice view model reference.
        /// </summary>
        private static InvoiceViewModel invoiceViewModel;

        /// <summary>
        /// The invoice view model reference.
        /// </summary>
        private static APInvoiceViewModel apInvoiceViewModel;

        /// <summary>
        /// The invoice view model reference.
        /// </summary>
        private static ARReturnViewModel ARReturnViewModel;

        /// <summary>
        /// The invoice view model reference.
        /// </summary>
        private static ARReturnTouchscreenViewModel ARReturnTouchscreenViewModel;

        /// <summary>
        /// The invoice view model reference.
        /// </summary>
        private static ARReturnPanelViewModel ARReturnPanelViewModel;

        /// <summary>
        /// The invoice creation view model reference.
        /// </summary>
        private static InvoiceCreationViewModel invoiceCreationViewModel;

        /// <summary>
        /// The invoice creation view model reference.
        /// </summary>
        private static APInvoiceCreationViewModel apInvoiceCreationViewModel;

        /// <summary>
        /// The production batch set up view model reference.
        /// </summary>
        private static ProductionBatchSetUpViewModel productionBatchSetUpViewModel;

        /// <summary>
        /// The indicator view model reference.
        /// </summary>
        private static IndicatorViewModel indicatorViewModel;

        /// <summary>
        /// The partner group set up view model reference.
        /// </summary>
        private static BPGroupSetUpViewModel bPGroupSetUpViewModel;

        /// <summary>
        /// The edit transaction view model reference.
        /// </summary>
        private static EditTransactionViewModel editTransactionViewModel;

        /// <summary>
        /// The payment proposal creation view model reference.
        /// </summary>
        private static PaymentProposalCreationViewModel paymentProposalCreationViewModel;

        /// <summary>
        /// The payment proposal search view model reference.
        /// </summary>
        private static PaymentProposalSearchViewModel paymentProposalSearchViewModel;

        /// <summary>
        /// The document trail reference.
        /// </summary>
        private static DocumentTrailViewModel documentTrailViewModel;

        /// <summary>
        /// The alibi reference.
        /// </summary>
        private static AlibiViewModel alibiViewModel;

        /// <summary>
        /// The touchscreen dispatch panel view model reference.
        /// </summary>
        private static TouchscreenDispatchPanelViewModel touchscreenDispatchPanelViewModel;

        /// <summary>
        /// The touchscreen dispatch panel view model reference.
        /// </summary>
        private static TouchscreenDispatchBatchPanelViewModel touchscreenDispatchBatchPanelViewModel;

        /// <summary>
        /// The touchscreen dispatch panel record weight view model reference.
        /// </summary>
        private static TouchscreenDispatchPanelRecordWeightViewModel touchscreenDispatchPanelRecordWeightViewModel;

        /// <summary>
        /// The touchscreen dispatch panel record weight view model reference.
        /// </summary>
        private static TouchscreenDispatchPanelRecordWeightWithShippingViewModel touchscreenDispatchPanelRecordWeightWithShippingViewModel;

        /// <summary>
        /// The touchscreen dispatch panel record weight view model reference.
        /// </summary>
        private static TouchscreenDispatchPanelRecordWeightWithNotesViewModel touchscreenDispatchPanelRecordWeightWithNotesViewModel;

        /// <summary>
        /// The touchscreen receipt panel view model reference.
        /// </summary>
        private static TouchscreenReceiptPanelViewModel touchscreenReceiptPanelViewModel;

        /// <summary>
        /// The touchscreen receipt panel view model reference.
        /// </summary>
        private static TouchscreenReceiptPanelWithBarcodePrintViewModel touchscreenReceiptPanelWithBarcodePrintViewModel;

        /// <summary>
        /// The touchscreen receipt panel view model reference.
        /// </summary>
        private static TouchscreenReceiptPalletPanelViewModel touchscreenReceiptPalletPanelViewModel;

        /// <summary>
        /// The touchscreen receipt production panel view model reference.
        /// </summary>
        private static TouchscreenReceiptProductionPanelViewModel touchscreenReceiptProductionPanelViewModel;

        /// <summary>
        /// The into production view model reference.
        /// </summary>
        private static IntoProductionViewModel intoProductionViewModel;

        /// <summary>
        /// The out of production view model reference.
        /// </summary>
        private static OutOfProductionViewModel outOfProductionViewModel;

        /// <summary>
        /// The workflow selection view model reference.
        /// </summary>
        private static WorkflowKeyboardViewModel workflowKeyboardViewModel;

        /// <summary>
        /// The workflow selection view model reference.
        /// </summary>
        private static WorkflowCollectionViewModel workflowCollectionViewModel;

        /// <summary>
        /// The workflow selection view model reference.
        /// </summary>
        private static WorkflowCalenderViewModel workflowCalenderViewModel;

        /// <summary>
        /// The workflow selection view model reference.
        /// </summary>
        private static WorkflowIndicatorViewModel workflowIndicatorViewModel;

        /// <summary>
        /// The workflow view model reference.
        /// </summary>
        private static WorkflowKeypadViewModel workflowKeypadViewModel;

        /// <summary>
        /// The workflow view model reference.
        /// </summary>
        private static WorkflowAlertsViewModel workflowAlertsViewModel;

        /// <summary>
        /// The workflow view model reference.
        /// </summary>
        private static WorkflowViewModel workflowViewModel;

        /// <summary>
        /// The workflow view model reference.
        /// </summary>
        private static WorkflowSearchViewModel workflowSearchViewModel;

        /// <summary>
        /// The workflow summary view model reference.
        /// </summary>
        private static WorkflowSummaryViewModel workflowSummaryViewModel;

        /// <summary>
        /// The workflow selection view model reference.
        /// </summary>
        private static WorkflowTemplateSelectionViewModel workflowTemplateSelectionViewModel;

        /// <summary>
        /// The workflow selection view model reference.
        /// </summary>
        private static WorkflowSelectionViewModel workflowSelectionViewModel;

        /// <summary>
        /// The out of production view model reference.
        /// </summary>
        private static OutOfProductionHeaderViewModel outOfProductionHeaderViewModel;

        /// <summary>
        /// The out of production view model reference.
        /// </summary>
        private static OutOfProductionSplitHeaderViewModel outOfProductionSplitHeaderViewModel;

        /// <summary>
        /// The touchscreen production panel view model reference.
        /// </summary>
        private static TouchscreenProductionPanelViewModel touchscreenProductionPanelViewModel;

        /// <summary>
        /// The touchscreen production panel view model reference.
        /// </summary>
        private static TouchscreenProductionReceipePanelViewModel touchscreenProductionReceipePanelViewModel;

        /// <summary>
        /// The touchscreen production recall panel view model reference.
        /// </summary>
        private static TouchscreenProductionRecallPanelViewModel touchscreenProductionRecallPanelViewModel;

        /// <summary>
        /// The scanner orders view model reference.
        /// </summary>
        private static ScannerOrdersViewModel scannerOrdersViewModel;

        /// <summary>
        /// The scanner main dispatch view model reference.
        /// </summary>
        private static ScannerMainDispatchViewModel scannerMainDispatchViewModel;

        /// <summary>
        /// The touchscreen out of production panel view model reference.
        /// </summary>
        private static TouchscreenOutOfProductionPanelViewModel touchscreenOutOfProductionPanelViewModel;

        /// <summary>
        /// The touchscreen out of production panel view model reference.
        /// </summary>
        private static TouchscreenOutOfProductionWithPiecesPanelViewModel touchscreenOutOfProductionWithPiecesPanelViewModel;

        /// <summary>
        /// The production products view model reference.
        /// </summary>
        private static ProductionProductsViewModel productionProductsViewModel;

        /// <summary>
        /// The production products view model reference.
        /// </summary>
        private static ProductionProductsGridViewModel productionProductsGridViewModel;

        /// <summary>
        /// The production products card view model reference.
        /// </summary>
        private static ProductionProductsCardsViewModel productionProductsCardsViewModel;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            //ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            //SimpleIoc.Default.Register<MasterViewModel>();
        }

        #endregion

        #region property

        #region Master

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public static MasterViewModel MasterStatic
        {
            get
            {
                if (masterViewModel == null)
                {
                    CreateMaster();
                }

                //currentViewModel = masterViewModel;
                return masterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public MasterViewModel Master
        {
            get
            {
                return MasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Master property.
        /// </summary>
        public static void ClearMaster()
        {
            if (masterViewModel != null)
            {
                masterViewModel.Cleanup();
                masterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Master property.
        /// </summary>
        public static void CreateMaster()
        {
            if (masterViewModel == null)
            {
                masterViewModel = new MasterViewModel();
            }
        }

        #endregion        

        #region AlertUsersSetUp

        /// <summary>
        /// Gets the AlertUsersSetUp property.
        /// </summary>
        public static AlertUsersSetUpViewModel AlertUsersSetUpStatic
        {
            get
            {
                if (alertUsersSetUpViewModel == null)
                {
                    CreateAlertUsersSetUp();
                }

                //currentViewModel = alertUsersSetUpViewModel;
                return alertUsersSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the AlertUsersSetUp property.
        /// </summary>
        public AlertUsersSetUpViewModel AlertUsersSetUp
        {
            get
            {
                return AlertUsersSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the AlertUsersSetUp property.
        /// </summary>
        public static void ClearAlertUsersSetUp()
        {
            if (alertUsersSetUpViewModel != null)
            {
                alertUsersSetUpViewModel.Cleanup();
                alertUsersSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the AlertUsersSetUp property.
        /// </summary>
        public static void CreateAlertUsersSetUp()
        {
            if (alertUsersSetUpViewModel == null)
            {
                alertUsersSetUpViewModel = new AlertUsersSetUpViewModel();
            }
        }

        #endregion        

        #region AttributeSearch

        /// <summary>
        /// Gets the AttributeSearch property.
        /// </summary>
        public static AttributeSearchViewModel AttributeSearchStatic
        {
            get
            {
                if (attributeSearchViewModel == null)
                {
                    CreateAttributeSearch();
                }

                return attributeSearchViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public AttributeSearchViewModel AttributeSearch
        {
            get
            {
                return AttributeSearchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the AttributeSearch property.
        /// </summary>
        public static void ClearAttributeSearch()
        {
            if (attributeSearchViewModel != null)
            {
                attributeSearchViewModel.Cleanup();
                attributeSearchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the AttributeSearch property.
        /// </summary>
        public static void CreateAttributeSearch()
        {
            if (attributeSearchViewModel == null)
            {
                attributeSearchViewModel = new AttributeSearchViewModel();
            }
        }

        #endregion        

        #region AttributeMasterSetUp

        /// <summary>
        /// Gets the AttributeMasterSetUp property.
        /// </summary>
        public static AttributeMasterSetUpViewModel AttributeMasterSetUpStatic
        {
            get
            {
                if (attributeMasterSetUpViewModel == null)
                {
                    CreateAttributeMasterSetUp();
                }

                return attributeMasterSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public AttributeMasterSetUpViewModel AttributeMasterSetUp
        {
            get
            {
                return AttributeMasterSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the AttributeMasterSetUp property.
        /// </summary>
        public static void ClearAttributeMasterSetUp()
        {
            if (attributeMasterSetUpViewModel != null)
            {
                attributeMasterSetUpViewModel.Cleanup();
                attributeMasterSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the AttributeMasterSetUp property.
        /// </summary>
        public static void CreateAttributeMasterSetUp()
        {
            if (attributeMasterSetUpViewModel == null)
            {
                attributeMasterSetUpViewModel = new AttributeMasterSetUpViewModel();
            }
        }

        #endregion        

        #region Specifications

        /// <summary>
        /// Gets the Specifications property.
        /// </summary>
        public static SpecificationsViewModel SpecificationsStatic
        {
            get
            {
                if (specificationsViewModel == null)
                {
                    CreateSpecifications();
                }
         
                return specificationsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SpecificationsViewModel Specifications
        {
            get
            {
                return SpecificationsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Specifications property.
        /// </summary>
        public static void ClearSpecifications()
        {
            if (specificationsViewModel != null)
            {
                specificationsViewModel.Cleanup();
                specificationsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Specifications property.
        /// </summary>
        public static void CreateSpecifications()
        {
            if (specificationsViewModel == null)
            {
                specificationsViewModel = new SpecificationsViewModel();
            }
        }

        #endregion        

        #region BatchSetUp

        /// <summary>
        /// Gets the BatchSetUp property.
        /// </summary>
        public static BatchSetUpViewModel BatchSetUpStatic
        {
            get
            {
                if (batchSetUpViewModel == null)
                {
                    CreateBatchSetUp();
                }
         
                return batchSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public BatchSetUpViewModel BatchSetUp
        {
            get
            {
                return BatchSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BatchSetUp property.
        /// </summary>
        public static void ClearBatchSetUp()
        {
            if (batchSetUpViewModel != null)
            {
                batchSetUpViewModel.Cleanup();
                batchSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BatchSetUp property.
        /// </summary>
        public static void CreateBatchSetUp()
        {
            if (batchSetUpViewModel == null)
            {
                batchSetUpViewModel = new BatchSetUpViewModel();
            }
        }

        #endregion        

        #region Entry

        /// <summary>
        /// Gets the Entry property.
        /// </summary>
        public static EntryViewModel EntryStatic
        {
            get
            {
                if (entryViewModel == null)
                {
                    CreateEntry();
                }
         
                return entryViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public EntryViewModel Entry
        {
            get
            {
                return EntryStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Entry property.
        /// </summary>
        public static void ClearEntry()
        {
            if (entryViewModel != null)
            {
                entryViewModel.Cleanup();
                entryViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Entry property.
        /// </summary>
        public static void CreateEntry()
        {
            if (entryViewModel == null)
            {
                entryViewModel = new EntryViewModel();
            }
        }

        #endregion        

        #region Exit

        /// <summary>
        /// Gets the Exit property.
        /// </summary>
        public static ExitViewModel ExitStatic
        {
            get
            {
                if (exitViewModel == null)
                {
                    CreateExit();
                }

                return exitViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ExitViewModel Exit
        {
            get
            {
                return ExitStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Exit property.
        /// </summary>
        public static void ClearExit()
        {
            if (exitViewModel != null)
            {
                exitViewModel.Cleanup();
                exitViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Exit property.
        /// </summary>
        public static void CreateExit()
        {
            if (exitViewModel == null)
            {
                exitViewModel = new ExitViewModel();
            }
        }

        #endregion        

        #region DetainCondemn

        /// <summary>
        /// Gets the DetainCondemn property.
        /// </summary>
        public static DetainCondemnViewModel DetainCondemnStatic
        {
            get
            {
                if (detainCondemnViewModel == null)
                {
                    CreateDetainCondemn();
                }

                return detainCondemnViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public DetainCondemnViewModel DetainCondemn
        {
            get
            {
                return DetainCondemnStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DetainCondemn property.
        /// </summary>
        public static void ClearDetainCondemn()
        {
            if (detainCondemnViewModel != null)
            {
                detainCondemnViewModel.Cleanup();
                detainCondemnViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DetainCondemn property.
        /// </summary>
        public static void CreateDetainCondemn()
        {
            if (detainCondemnViewModel == null)
            {
                detainCondemnViewModel = new DetainCondemnViewModel();
            }
        }

        #endregion        

        #region DispatchGrid

        /// <summary>
        /// Gets the DispatchGrid property.
        /// </summary>
        public static DispatchGridViewModel DispatchGridStatic
        {
            get
            {
                if (dispatchGridViewModel == null)
                {
                    CreateDispatchGrid();
                }

                return dispatchGridViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public DispatchGridViewModel DispatchGrid
        {
            get
            {
                return DispatchGridStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DispatchGrid property.
        /// </summary>
        public static void ClearDispatchGrid()
        {
            if (dispatchGridViewModel != null)
            {
                dispatchGridViewModel.Cleanup();
                dispatchGridViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DispatchGrid property.
        /// </summary>
        public static void CreateDispatchGrid()
        {
            if (dispatchGridViewModel == null)
            {
                dispatchGridViewModel = new DispatchGridViewModel();
            }
        }

        #endregion        

        #region DispatchCount

        /// <summary>
        /// Gets the DispatchCount property.
        /// </summary>
        public static DispatchCountViewModel DispatchCountStatic
        {
            get
            {
                if (dispatchCountViewModel == null)
                {
                    CreateDispatchCount();
                }

                return dispatchCountViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public DispatchCountViewModel DispatchCount
        {
            get
            {
                return DispatchCountStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DispatchCount property.
        /// </summary>
        public static void ClearDispatchCount()
        {
            if (dispatchCountViewModel != null)
            {
                dispatchCountViewModel.Cleanup();
                dispatchCountViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DispatchCount property.
        /// </summary>
        public static void CreateDispatchCount()
        {
            if (dispatchCountViewModel == null)
            {
                dispatchCountViewModel = new DispatchCountViewModel();
            }
        }

        #endregion        

        #region DispatchTransactions

        /// <summary>
        /// Gets the DispatchTransactions property.
        /// </summary>
        public static DispatchTransactionsViewModel DispatchTransactionsStatic
        {
            get
            {
                if (dispatchTransactionsViewModel == null)
                {
                    CreateDispatchTransactions();
                }

                return dispatchTransactionsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public DispatchTransactionsViewModel DispatchTransactions
        {
            get
            {
                return DispatchTransactionsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DispatchTransactions property.
        /// </summary>
        public static void ClearDispatchTransactions()
        {
            if (dispatchTransactionsViewModel != null)
            {
                dispatchTransactionsViewModel.Cleanup();
                dispatchTransactionsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DispatchTransactions property.
        /// </summary>
        public static void CreateDispatchTransactions()
        {
            if (dispatchTransactionsViewModel == null)
            {
                dispatchTransactionsViewModel = new DispatchTransactionsViewModel();
            }
        }

        #endregion

        #region Needle

        /// <summary>
        /// Gets the Needle property.
        /// </summary>
        public static NeedleViewModel NeedleStatic
        {
            get
            {
                if (needleViewModel == null)
                {
                    CreateNeedle();
                }

                return needleViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public NeedleViewModel Needle
        {
            get
            {
                return NeedleStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Needle property.
        /// </summary>
        public static void ClearNeedle()
        {
            if (needleViewModel != null)
            {
                needleViewModel.Cleanup();
                needleViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Needle property.
        /// </summary>
        public static void CreateNeedle()
        {
            if (needleViewModel == null)
            {
                needleViewModel = new NeedleViewModel();
            }
        }

        #endregion

        #region IntoProductionMainGrid

        /// <summary>
        /// Gets the IntoProductionMainGrid property.
        /// </summary>
        public static IntoProductionMainGridViewModel IntoProductionMainGridStatic
        {
            get
            {
                if (intoProductionMainGridViewModel == null)
                {
                    CreateIntoProductionMainGrid();
                }

                return intoProductionMainGridViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public IntoProductionMainGridViewModel IntoProductionMainGrid
        {
            get
            {
                return IntoProductionMainGridStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the IntoProductionMainGrid property.
        /// </summary>
        public static void ClearIntoProductionMainGrid()
        {
            if (intoProductionMainGridViewModel != null)
            {
                intoProductionMainGridViewModel.Cleanup();
                intoProductionMainGridViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the IntoProductionMainGrid property.
        /// </summary>
        public static void CreateIntoProductionMainGrid()
        {
            if (intoProductionMainGridViewModel == null)
            {
                intoProductionMainGridViewModel = new IntoProductionMainGridViewModel();
            }
        }

        #endregion

        #region IntoProductionConformance

        /// <summary>
        /// Gets the IntoProductionConformance property.
        /// </summary>
        public static IntoProductionConformanceViewModel IntoProductionConformanceStatic
        {
            get
            {
                if (intoProductionConformanceViewModel == null)
                {
                    CreateIntoProductionConformance();
                }

                return intoProductionConformanceViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public IntoProductionConformanceViewModel IntoProductionConformance
        {
            get
            {
                return IntoProductionConformanceStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the IntoProductionConformance property.
        /// </summary>
        public static void ClearIntoProductionConformance()
        {
            if (intoProductionConformanceViewModel != null)
            {
                intoProductionConformanceViewModel.Cleanup();
                intoProductionConformanceViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the IntoProductionConformance property.
        /// </summary>
        public static void CreateIntoProductionConformance()
        {
            if (intoProductionConformanceViewModel == null)
            {
                intoProductionConformanceViewModel = new IntoProductionConformanceViewModel();
            }
        }

        #endregion

        #region BatchEdit

        /// <summary>
        /// Gets the BatchEdit property.
        /// </summary>
        public static BatchEditViewModel BatchEditStatic
        {
            get
            {
                if (batchEditViewModel == null)
                {
                    CreateBatchEdit();
                }

                return batchEditViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public BatchEditViewModel BatchEdit
        {
            get
            {
                return BatchEditStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BatchEdit property.
        /// </summary>
        public static void ClearBatchEdit()
        {
            if (batchEditViewModel != null)
            {
                batchEditViewModel.Cleanup();
                batchEditViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BatchEdit property.
        /// </summary>
        public static void CreateBatchEdit()
        {
            if (batchEditViewModel == null)
            {
                batchEditViewModel = new BatchEditViewModel();
            }
        }

        #endregion        

        #region BeefTop

        /// <summary>
        /// Gets the BeefTop property.
        /// </summary>
        public static BeefTopViewModel BeefTopStatic
        {
            get
            {
                if (beefTopViewModel == null)
                {
                    CreateBeefTop();
                }

                return beefTopViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public BeefTopViewModel BeefTop
        {
            get
            {
                return BeefTopStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BeefTop property.
        /// </summary>
        public static void ClearBeefTop()
        {
            if (beefTopViewModel != null)
            {
                beefTopViewModel.Cleanup();
                beefTopViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BeefTop property.
        /// </summary>
        public static void CreateBeefTop()
        {
            if (beefTopViewModel == null)
            {
                beefTopViewModel = new BeefTopViewModel();
            }
        }

        #endregion        

        #region SheepTop

        /// <summary>
        /// Gets the SheepTop property.
        /// </summary>
        public static SheepTopViewModel SheepTopStatic
        {
            get
            {
                if (sheepTopViewModel == null)
                {
                    CreateSheepTop();
                }

                return sheepTopViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SheepTopViewModel SheepTop
        {
            get
            {
                return SheepTopStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SheepTop property.
        /// </summary>
        public static void ClearSheepTop()
        {
            if (sheepTopViewModel != null)
            {
                sheepTopViewModel.Cleanup();
                sheepTopViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SheepTop property.
        /// </summary>
        public static void CreateSheepTop()
        {
            if (sheepTopViewModel == null)
            {
                sheepTopViewModel = new SheepTopViewModel();
            }
        }

        #endregion        

        #region Telesales

        /// <summary>
        /// Gets the Telesales property.
        /// </summary>
        public static TelesalesViewModel TelesalesStatic
        {
            get
            {
                if (telesalesViewModel == null)
                {
                    CreateTelesales();
                }

                return telesalesViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TelesalesViewModel Telesales
        {
            get
            {
                return TelesalesStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Telesales property.
        /// </summary>
        public static void ClearTelesales()
        {
            if (telesalesViewModel != null)
            {
                telesalesViewModel.Cleanup();
                telesalesViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Telesales property.
        /// </summary>
        public static void CreateTelesales()
        {
            if (telesalesViewModel == null)
            {
                telesalesViewModel = new TelesalesViewModel();
            }
        }

        #endregion        

        #region TelesalesSetUp

        /// <summary>
        /// Gets the TelesalesSetUp property.
        /// </summary>
        public static TelesalesSetUpViewModel TelesalesSetUpStatic
        {
            get
            {
                if (telesalesSetUpViewModel == null)
                {
                    CreateTelesalesSetUp();
                }

                return telesalesSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TelesalesSetUpViewModel TelesalesSetUp
        {
            get
            {
                return TelesalesSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TelesalesSetUp property.
        /// </summary>
        public static void ClearTelesalesSetUp()
        {
            if (telesalesSetUpViewModel != null)
            {
                telesalesSetUpViewModel.Cleanup();
                telesalesSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TelesalesSetUp property.
        /// </summary>
        public static void CreateTelesalesSetUp()
        {
            if (telesalesSetUpViewModel == null)
            {
                telesalesSetUpViewModel = new TelesalesSetUpViewModel();
            }
        }

        #endregion        

        //#region Receipe

        ///// <summary>
        ///// Gets the Receipe property.
        ///// </summary>
        //public static ReceipeViewModel ReceipeStatic
        //{
        //    get
        //    {
        //        if (receipeViewModel == null)
        //        {
        //            CreateReceipe();
        //        }

        //        return receipeViewModel;
        //    }
        //}

        ///// <summary>
        ///// Gets the Master property.
        ///// </summary>
        //public ReceipeViewModel Receipe
        //{
        //    get
        //    {
        //        return ReceipeStatic;
        //    }
        //}

        ///// <summary>
        ///// Provides a deterministic way to delete the Receipe property.
        ///// </summary>
        //public static void ClearReceipe()
        //{
        //    if (receipeViewModel != null)
        //    {
        //        receipeViewModel.Cleanup();
        //        receipeViewModel = null;
        //    }
        //}

        ///// <summary>
        ///// Provides a deterministic way to create the Receipe property.
        ///// </summary>
        //public static void CreateReceipe()
        //{
        //    if (receipeViewModel == null)
        //    {
        //        receipeViewModel = new ReceipeViewModel();
        //    }
        //}

        //#endregion        

        #region Login

        /// <summary>
        /// Gets the Login property.
        /// </summary>
        public static LoginViewModel LoginStatic
        {
            get
            {
                if (loginViewModel == null)
                {
                    CreateLogin();
                }

                return loginViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LoginViewModel Login
        {
            get
            {
                return LoginStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Login property.
        /// </summary>
        public static void ClearLogin()
        {
            if (loginViewModel != null)
            {
                loginViewModel.Cleanup();
                loginViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Login property.
        /// </summary>
        public static void CreateLogin()
        {
            if (loginViewModel == null)
            {
                loginViewModel = new LoginViewModel();
            }
        }

        #endregion        

        #region LoginUser

        /// <summary>
        /// Gets the LoginUser property.
        /// </summary>
        public static LoginUserViewModel LoginUserStatic
        {
            get
            {
                if (loginUserViewModel == null)
                {
                    CreateLoginUser();
                }

                return loginUserViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LoginUserViewModel LoginUser
        {
            get
            {
                return LoginUserStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LoginUser property.
        /// </summary>
        public static void ClearLoginUser()
        {
            if (loginUserViewModel != null)
            {
                loginUserViewModel.Cleanup();
                loginUserViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LoginUser property.
        /// </summary>
        public static void CreateLoginUser()
        {
            if (loginUserViewModel == null)
            {
                loginUserViewModel = new LoginUserViewModel();
            }
        }

        #endregion        

        #region Login Selection

        /// <summary>
        /// Gets the Login property.
        /// </summary>
        public static LoginSelectionViewModel LoginSelectionStatic
        {
            get
            {
                if (loginSelectionViewModel == null)
                {
                    CreateLoginSelection();
                }

                return loginSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LoginSelectionViewModel LoginSelection
        {
            get
            {
                return LoginSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LoginSelection property.
        /// </summary>
        public static void ClearLoginSelection()
        {
            if (loginSelectionViewModel != null)
            {
                loginSelectionViewModel.Cleanup();
                loginSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Login property.
        /// </summary>
        public static void CreateLoginSelection()
        {
            if (loginSelectionViewModel == null)
            {
                loginSelectionViewModel = new LoginSelectionViewModel();
            }
        }

        #endregion        

        #region BPMaster

        /// <summary>
        /// Gets the BPMaster property.
        /// </summary>
        public static BPMasterViewModel BPMasterStatic
        {
            get
            {
                if (bpMasterViewModel == null)
                {
                    CreateBPMaster();
                }
    
                return bpMasterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public BPMasterViewModel BPMaster
        {
            get
            {
                return BPMasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BPMaster property.
        /// </summary>
        public static void ClearBPMaster()
        {
            if (bpMasterViewModel != null)
            {
                bpMasterViewModel.Cleanup();
                bpMasterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BPMaster property.
        /// </summary>
        public static void CreateBPMaster()
        {
            if (bpMasterViewModel == null)
            {
                bpMasterViewModel = new BPMasterViewModel();
            }
        }

        #endregion        

        #region BPSearchData

        /// <summary>
        /// Gets the BPSearchData property.
        /// </summary>
        public static BPSearchDataViewModel BPSearchDataStatic
        {
            get
            {
                if (bpSearchDataViewModel == null)
                {
                    CreateBPSearchData();
                }
           
                return bpSearchDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public BPSearchDataViewModel BPSearchData
        {
            get
            {
                return BPSearchDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BPSearchData property.
        /// </summary>
        public static void ClearBPSearchData()
        {
            if (bpSearchDataViewModel != null)
            {
                bpSearchDataViewModel.Cleanup();
                bpSearchDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BPSearchData property.
        /// </summary>
        public static void CreateBPSearchData()
        {
            if (bpSearchDataViewModel == null)
            {
                bpSearchDataViewModel = new BPSearchDataViewModel();
            }
        }

        #endregion        

        #region StockMoveGrid

        /// <summary>
        /// Gets the StockMoveGrid property.
        /// </summary>
        public static StockMoveGridViewModel StockMoveGridStatic
        {
            get
            {
                if (stockMoveGridViewModel == null)
                {
                    CreateStockMoveGrid();
                }

                return stockMoveGridViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public StockMoveGridViewModel StockMoveGrid
        {
            get
            {
                return StockMoveGridStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the StockMoveGrid property.
        /// </summary>
        public static void ClearStockMoveGrid()
        {
            if (stockMoveGridViewModel != null)
            {
                stockMoveGridViewModel.Cleanup();
                stockMoveGridViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the StockMoveGrid property.
        /// </summary>
        public static void CreateStockMoveGrid()
        {
            if (stockMoveGridViewModel == null)
            {
                stockMoveGridViewModel = new StockMoveGridViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsStockMoveGridNull()
        {
            return stockMoveGridViewModel == null;
        }

        #endregion

        #region StockMoveBatchGrid

        /// <summary>
        /// Gets the StockMoveBatchGrid property.
        /// </summary>
        public static StockMoveBatchGridViewModel StockMoveBatchGridStatic
        {
            get
            {
                if (stockMoveBatchGridViewModel == null)
                {
                    CreateStockMoveBatchGrid();
                }

                return stockMoveBatchGridViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public StockMoveBatchGridViewModel StockMoveBatchGrid
        {
            get
            {
                return StockMoveBatchGridStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the StockMoveBatchGrid property.
        /// </summary>
        public static void ClearStockMoveBatchGrid()
        {
            if (stockMoveBatchGridViewModel != null)
            {
                stockMoveBatchGridViewModel.Cleanup();
                stockMoveBatchGridViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the StockMoveBatchGrid property.
        /// </summary>
        public static void CreateStockMoveBatchGrid()
        {
            if (stockMoveBatchGridViewModel == null)
            {
                stockMoveBatchGridViewModel = new StockMoveBatchGridViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsStockMoveBatchGridNull()
        {
            return stockMoveBatchGridViewModel == null;
        }

        #endregion        

        #region StockMoveGroupedGrid

        /// <summary>
        /// Gets the StockMoveGroupedGrid property.
        /// </summary>
        public static StockMoveGroupedGridViewModel StockMoveGroupedGridStatic
        {
            get
            {
                if (stockMoveGroupedGridViewModel == null)
                {
                    CreateStockMoveGroupedGrid();
                }

                return stockMoveGroupedGridViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public StockMoveGroupedGridViewModel StockMoveGroupedGrid
        {
            get
            {
                return StockMoveGroupedGridStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the StockMoveGroupedGrid property.
        /// </summary>
        public static void ClearStockMoveGroupedGrid()
        {
            if (stockMoveGroupedGridViewModel != null)
            {
                stockMoveGroupedGridViewModel.Cleanup();
                stockMoveGroupedGridViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the StockMoveGroupedGrid property.
        /// </summary>
        public static void CreateStockMoveGroupedGrid()
        {
            if (stockMoveGroupedGridViewModel == null)
            {
                stockMoveGroupedGridViewModel = new StockMoveGroupedGridViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsStockMoveGroupedGridNull()
        {
            return stockMoveGroupedGridViewModel == null;
        }

        #endregion        

        #region SystemMessage

        /// <summary>
        /// Gets the system message property.
        /// </summary>
        public static SystemMessageViewModel SystemMessageStatic
        {
            get
            {
                if (systemMessageViewModel == null)
                {
                    CreateSystemMessage();
                }

                return systemMessageViewModel;
            }
        }

        /// <summary>
        /// Gets the systemMessage property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public SystemMessageViewModel SystemMessage
        {
            get
            {
                return SystemMessageStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the systemMessage property.
        /// </summary>
        public static void ClearInformationBar()
        {
            if (systemMessageViewModel != null)
            {
                systemMessageViewModel.Cleanup();
                systemMessageViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the systemMessage property.
        /// </summary>
        public static void CreateSystemMessage()
        {
            if (systemMessageViewModel == null)
            {
                systemMessageViewModel = new SystemMessageViewModel();
            }
        }

        #endregion

        #region NouvemSplashScreen

        /// <summary>
        /// Gets the splash screen property.
        /// </summary>
        public static NouvemSplashScreenViewModel NouvemSplashScreenStatic
        {
            get
            {
                if (nouvemSplashScreenViewModel == null)
                {
                    CreateNouvemSplashScreen();
                }

                return nouvemSplashScreenViewModel;
            }
        }

        /// <summary>
        /// Gets the information bar property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public NouvemSplashScreenViewModel NouvemSplashScreen
        {
            get
            {
                return NouvemSplashScreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the splash screen property.
        /// </summary>
        public static void ClearNouvemSplashScreen()
        {
            if (nouvemSplashScreenViewModel != null)
            {
                nouvemSplashScreenViewModel.Cleanup();
                nouvemSplashScreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the splash screen property.
        /// </summary>
        public static void CreateNouvemSplashScreen()
        {
            if (nouvemSplashScreenViewModel == null)
            {
                nouvemSplashScreenViewModel = new NouvemSplashScreenViewModel();
            }
        }

        #endregion

        #region MessageBox

        /// <summary>
        /// Gets the MessageBox property.
        /// </summary>
        public static NouvemMessageBoxViewModel MessageBoxStatic
        {
            get
            {
                if (messageBox == null)
                {
                    CreateMessageBox();
                }

                return messageBox;
            }
        }

        /// <summary>
        /// Gets the MessageBox property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public NouvemMessageBoxViewModel MessageBox
        {
            get
            {
                return MessageBoxStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the MessageBox property.
        /// </summary>
        public static void ClearMessageBox()
        {
            if (messageBox != null)
            {
                messageBox.Cleanup();
                messageBox = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the MessageBox property.
        /// </summary>
        public static void CreateMessageBox()
        {
            if (messageBox == null)
            {
                messageBox = new NouvemMessageBoxViewModel();
            }
        }

        #endregion

        #region UserMaster

        /// <summary>
        /// Gets the UserMaster property.
        /// </summary>
        public static UserMasterViewModel UserMasterStatic
        {
            get
            {
                if (userMasterViewModel == null)
                {
                    CreateUserMaster();
                }
         
                return userMasterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public UserMasterViewModel UserMaster
        {
            get
            {
                return UserMasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the UserMaster property.
        /// </summary>
        public static void ClearUserMaster()
        {
            if (userMasterViewModel != null)
            {
                userMasterViewModel.Cleanup();
                userMasterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the UserMaster property.
        /// </summary>
        public static void CreateUserMaster()
        {
            if (userMasterViewModel == null)
            {
                userMasterViewModel = new UserMasterViewModel();
            }
        }

        #endregion        

        #region UserGroup

        /// <summary>
        /// Gets the UserGroup property.
        /// </summary>
        public static UserGroupViewModel UserGroupStatic
        {
            get
            {
                if (userGroupViewModel == null)
                {
                    CreateUserGroup();
                }
          
                return userGroupViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public UserGroupViewModel UserGroup
        {
            get
            {
                return UserGroupStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the UserGroup property.
        /// </summary>
        public static void ClearUserGroup()
        {
            if (userGroupViewModel != null)
            {
                userGroupViewModel.Cleanup();
                userGroupViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the UserGroup property.
        /// </summary>
        public static void CreateUserGroup()
        {
            if (userGroupViewModel == null)
            {
                userGroupViewModel = new UserGroupViewModel();
            }
        }

        #endregion        

        #region UserGroupAuthorisations

        /// <summary>
        /// Gets the UserGroupAuthorisations property.
        /// </summary>
        public static UserGroupAuthorisationsViewModel UserGroupAuthorisationsStatic
        {
            get
            {
                if (userGroupAuthorisationsViewModel == null)
                {
                    CreateUserGroupAuthorisations();
                }
          
                return userGroupAuthorisationsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public UserGroupAuthorisationsViewModel UserGroupAuthorisations
        {
            get
            {
                return UserGroupAuthorisationsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the UserGroupAuthorisations property.
        /// </summary>
        public static void ClearUserGroupAuthorisations()
        {
            if (userGroupAuthorisationsViewModel != null)
            {
                userGroupAuthorisationsViewModel.Cleanup();
                userGroupAuthorisationsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the UserGroupAuthorisations property.
        /// </summary>
        public static void CreateUserGroupAuthorisations()
        {
            if (userGroupAuthorisationsViewModel == null)
            {
                userGroupAuthorisationsViewModel = new UserGroupAuthorisationsViewModel();
            }
        }

        #endregion        

        #region UserSearchData

        /// <summary>
        /// Gets the UserSearchData property.
        /// </summary>
        public static UserSearchDataViewModel UserSearchDataStatic
        {
            get
            {
                if (userSearchDataViewModel == null)
                {
                    CreateUserSearchData();
                }

                return userSearchDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public UserSearchDataViewModel UserSearchData
        {
            get
            {
                return UserSearchDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the UserSearchData property.
        /// </summary>
        public static void ClearUserSearchData()
        {
            if (userSearchDataViewModel != null)
            {
                userSearchDataViewModel.Cleanup();
                userSearchDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the UserSearchData property.
        /// </summary>
        public static void CreateUserSearchData()
        {
            if (userSearchDataViewModel == null)
            {
                userSearchDataViewModel = new UserSearchDataViewModel();
            }
        }

        #endregion        

        #region UserChangePassword

        /// <summary>
        /// Gets the UserChangePassword property.
        /// </summary>
        public static UserChangePasswordViewModel UserChangePasswordStatic
        {
            get
            {
                if (userChangePasswordViewModel == null)
                {
                    CreateUserChangePassword();
                }
          
                return userChangePasswordViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public UserChangePasswordViewModel UserChangePassword
        {
            get
            {
                return UserChangePasswordStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the UserChangePassword property.
        /// </summary>
        public static void ClearUserChangePassword()
        {
            if (userChangePasswordViewModel != null)
            {
                userChangePasswordViewModel.Cleanup();
                userChangePasswordViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the UserChangePassword property.
        /// </summary>
        public static void CreateUserChangePassword()
        {
            if (userChangePasswordViewModel == null)
            {
                userChangePasswordViewModel = new UserChangePasswordViewModel();
            }
        }

        #endregion        

        #region PasswordEntry

        /// <summary>
        /// Gets the PasswordEntry property.
        /// </summary>
        public static PasswordEntryViewModel PasswordEntryStatic
        {
            get
            {
                if (passwordEntry == null)
                {
                    CreatePasswordEntry();
                }

                return passwordEntry;
            }
        }

        /// <summary>
        /// Gets the PasswordEntry property.
        /// </summary>
        public PasswordEntryViewModel PasswordEntry
        {
            get
            {
                return PasswordEntryStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PasswordEntry property.
        /// </summary>
        public static void PasswordEntryConfig()
        {
            if (passwordEntry != null)
            {
                passwordEntry.Cleanup();
                passwordEntry = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the PasswordEntry property.
        /// </summary>
        public static void CreatePasswordEntry()
        {
            if (passwordEntry == null)
            {
                passwordEntry = new PasswordEntryViewModel();
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PasswordEntry property.
        /// </summary>
        public static void ClearPasswordEntry()
        {
            if (passwordEntry != null)
            {
                passwordEntry.Cleanup();
                passwordEntry = null;
            }
        }

        #endregion
  
        #region ServerSetUp

        /// <summary>
        /// Gets the ServerSetUp property.
        /// </summary>
        public static ServerSetUpViewModel ServerSetUpStatic
        {
            get
            {
                if (serverSetUpViewModel == null)
                {
                    CreateServerSetUp();
                }
     
                return serverSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ServerSetUpViewModel ServerSetUp
        {
            get
            {
                return ServerSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ServerSetUp property.
        /// </summary>
        public static void ClearServerSetUp()
        {
            if (serverSetUpViewModel != null)
            {
                serverSetUpViewModel.Cleanup();
                serverSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ServerSetUp property.
        /// </summary>
        public static void CreateServerSetUp()
        {
            if (serverSetUpViewModel == null)
            {
                serverSetUpViewModel = new ServerSetUpViewModel();
            }
        }

        #endregion        

        #region DeviceSetUp

        /// <summary>
        /// Gets the DeviceSetUp property.
        /// </summary>
        public static DeviceSetUpViewModel DeviceSetUpStatic
        {
            get
            {
                if (deviceSetUpViewModel == null)
                {
                    CreateDeviceSetUp();
                }
         
                return deviceSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public DeviceSetUpViewModel DeviceSetUp
        {
            get
            {
                return DeviceSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DeviceSetUp property.
        /// </summary>
        public static void ClearDeviceSetUp()
        {
            if (deviceSetUpViewModel != null)
            {
                deviceSetUpViewModel.Cleanup();
                deviceSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DeviceSetUp property.
        /// </summary>
        public static void CreateDeviceSetUp()
        {
            if (deviceSetUpViewModel == null)
            {
                deviceSetUpViewModel = new DeviceSetUpViewModel();
            }
        }

        #endregion        

        #region DeviceSearch

        /// <summary>
        /// Gets the DeviceSearch property.
        /// </summary>
        public static DeviceSearchViewModel DeviceSearchStatic
        {
            get
            {
                if (deviceSearchViewModel == null)
                {
                    CreateDeviceSearch();
                }
        
                return deviceSearchViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public DeviceSearchViewModel DeviceSearch
        {
            get
            {
                return DeviceSearchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DeviceSearch property.
        /// </summary>
        public static void ClearDeviceSearch()
        {
            if (deviceSearchViewModel != null)
            {
                deviceSearchViewModel.Cleanup();
                deviceSearchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DeviceSearch property.
        /// </summary>
        public static void CreateDeviceSearch()
        {
            if (deviceSearchViewModel == null)
            {
                deviceSearchViewModel = new DeviceSearchViewModel();
            }
        }

        #endregion        

        #region LicenseAdmin

        /// <summary>
        /// Gets the LicenseAdmin property.
        /// </summary>
        public static LicenseAdminViewModel LicenseAdminStatic
        {
            get
            {
                if (licenseAdminViewModel == null)
                {
                    CreateLicenseAdmin();
                }

                return licenseAdminViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public LicenseAdminViewModel LicenseAdmin
        {
            get
            {
                return LicenseAdminStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LicenseAdmin property.
        /// </summary>
        public static void ClearLicenseAdmin()
        {
            if (licenseAdminViewModel != null)
            {
                licenseAdminViewModel.Cleanup();
                licenseAdminViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LicenseAdmin property.
        /// </summary>
        public static void CreateLicenseAdmin()
        {
            if (licenseAdminViewModel == null)
            {
                licenseAdminViewModel = new LicenseAdminViewModel();
            }
        }

        #endregion        

        #region LicenseImport

        /// <summary>
        /// Gets the LicenseImport property.
        /// </summary>
        public static LicenseImportViewModel LicenseImportStatic
        {
            get
            {
                if (licenseImportViewModel == null)
                {
                    CreateLicenseImport();
                }
  
                return licenseImportViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public LicenseImportViewModel LicenseImport
        {
            get
            {
                return LicenseImportStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LicenseImport property.
        /// </summary>
        public static void ClearLicenseImport()
        {
            if (licenseImportViewModel != null)
            {
                licenseImportViewModel.Cleanup();
                licenseImportViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LicenseImport property.
        /// </summary>
        public static void CreateLicenseImport()
        {
            if (licenseImportViewModel == null)
            {
                licenseImportViewModel = new LicenseImportViewModel();
            }
        }

        #endregion        

        #region GS1AI

        /// <summary>
        /// Gets the GS1AI property.
        /// </summary>
        public static GS1AIViewModel GS1AIStatic
        {
            get
            {
                if (gS1AIViewModel == null)
                {
                    CreateGS1AI();
                }
                
                return gS1AIViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public GS1AIViewModel GS1AI
        {
            get
            {
                return GS1AIStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the GS1AI property.
        /// </summary>
        public static void ClearGS1AI()
        {
            if (gS1AIViewModel != null)
            {
                gS1AIViewModel.Cleanup();
                gS1AIViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the GS1AI property.
        /// </summary>
        public static void CreateGS1AI()
        {
            if (gS1AIViewModel == null)
            {
                gS1AIViewModel = new GS1AIViewModel();
            }
        }

        #endregion        

        #region ScannerSequencerMain

        /// <summary>
        /// Gets the ScannerSequencerMain property.
        /// </summary>
        public static ScannerSequencerMainViewModel ScannerSequencerMainStatic
        {
            get
            {
                if (scannerSequencerMainViewModel == null)
                {
                    CreateScannerSequencerMain();
                }

                return scannerSequencerMainViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ScannerSequencerMainViewModel ScannerSequencerMain
        {
            get
            {
                return ScannerSequencerMainStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ScannerSequencerMain property.
        /// </summary>
        public static void ClearScannerSequencerMain()
        {
            if (scannerSequencerMainViewModel != null)
            {
                scannerSequencerMainViewModel.Cleanup();
                scannerSequencerMainViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ScannerSequencerMain property.
        /// </summary>
        public static void CreateScannerSequencerMain()
        {
            if (scannerSequencerMainViewModel == null)
            {
                scannerSequencerMainViewModel = new ScannerSequencerMainViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsScannerSequencerMainNull()
        {
            return scannerSequencerMainViewModel == null;
        }

        #endregion

        #region ScannerSequencerVerification

        /// <summary>
        /// Gets the ScannerSequencerVerification property.
        /// </summary>
        public static ScannerSequencerVerificationViewModel ScannerSequencerVerificationStatic
        {
            get
            {
                if (scannerSequencerVerificationViewModel == null)
                {
                    CreateScannerSequencerVerification();
                }

                return scannerSequencerVerificationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ScannerSequencerVerificationViewModel ScannerSequencerVerification
        {
            get
            {
                return ScannerSequencerVerificationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ScannerSequencerVerification property.
        /// </summary>
        public static void ClearScannerSequencerVerification()
        {
            if (scannerSequencerVerificationViewModel != null)
            {
                scannerSequencerVerificationViewModel.Cleanup();
                scannerSequencerVerificationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ScannerSequencerVerification property.
        /// </summary>
        public static void CreateScannerSequencerVerification()
        {
            if (scannerSequencerVerificationViewModel == null)
            {
                scannerSequencerVerificationViewModel = new ScannerSequencerVerificationViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsScannerSequencerVerificationNull()
        {
            return scannerSequencerVerificationViewModel == null;
        }

        #endregion

        #region ScannerSequencer

        /// <summary>
        /// Gets the ScannerSequencer property.
        /// </summary>
        public static ScannerSequencerViewModel ScannerSequencerStatic
        {
            get
            {
                if (scannerSequencerViewModel == null)
                {
                    CreateScannerSequencer();
                }

                return scannerSequencerViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ScannerSequencerViewModel ScannerSequencer
        {
            get
            {
                return ScannerSequencerStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ScannerSequencer property.
        /// </summary>
        public static void ClearScannerSequencer()
        {
            if (scannerSequencerViewModel != null)
            {
                scannerSequencerViewModel.Cleanup();
                scannerSequencerViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ScannerSequencer property.
        /// </summary>
        public static void CreateScannerSequencer()
        {
            if (scannerSequencerViewModel == null)
            {
                scannerSequencerViewModel = new ScannerSequencerViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsScannerSequencerNull()
        {
            return scannerSequencerViewModel == null;
        }

        #endregion

        #region StockMovement

        /// <summary>
        /// Gets the StockMovement property.
        /// </summary>
        public static StockMovementViewModel StockMovementStatic
        {
            get
            {
                if (stockMovementViewModel == null)
                {
                    CreateStockMovement();
                }
  
                return stockMovementViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public StockMovementViewModel StockMovement
        {
            get
            {
                return StockMovementStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the StockMovement property.
        /// </summary>
        public static void ClearStockMovement()
        {
            if (stockMovementViewModel != null)
            {
                stockMovementViewModel.Cleanup();
                stockMovementViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the StockMovement property.
        /// </summary>
        public static void CreateStockMovement()
        {
            if (stockMovementViewModel == null)
            {
                stockMovementViewModel = new StockMovementViewModel();
            }
        }

        #endregion        

        #region TareCalculator

        /// <summary>
        /// Gets the TareCalculator property.
        /// </summary>
        public static TareCalculatorViewModel TareCalculatorStatic
        {
            get
            {
                if (tareCalculatorViewModel == null)
                {
                    CreateTareCalculator();
                }
           
                return tareCalculatorViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TareCalculatorViewModel TareCalculator
        {
            get
            {
                return TareCalculatorStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TareCalculator property.
        /// </summary>
        public static void ClearTareCalculator()
        {
            if (tareCalculatorViewModel != null)
            {
                tareCalculatorViewModel.Cleanup();
                tareCalculatorViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TareCalculator property.
        /// </summary>
        public static void CreateTareCalculator()
        {
            if (tareCalculatorViewModel == null)
            {
                tareCalculatorViewModel = new TareCalculatorViewModel();
            }
        }

        #endregion        

        #region StockTake

        /// <summary>
        /// Gets the StockTake property.
        /// </summary>
        public static StockTakeViewModel StockTakeStatic
        {
            get
            {
                if (stockTakeViewModel == null)
                {
                    CreateStockTake();
                }
          
                return stockTakeViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public StockTakeViewModel StockTake
        {
            get
            {
                return StockTakeStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the StockTake property.
        /// </summary>
        public static void ClearStockTake()
        {
            if (stockTakeViewModel != null)
            {
                stockTakeViewModel.Cleanup();
                stockTakeViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the StockTake property.
        /// </summary>
        public static void CreateStockTake()
        {
            if (stockTakeViewModel == null)
            {
                stockTakeViewModel = new StockTakeViewModel();
            }
        }

        #endregion        
        
        #region PaymentDeductions

        /// <summary>
        /// Gets the PaymentDeductions property.
        /// </summary>
        public static PaymentDeductionsViewModel PaymentDeductionsStatic
        {
            get
            {
                if (paymentDeductionsViewModel == null)
                {
                    CreatePaymentDeductions();
                }
               
                return paymentDeductionsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public PaymentDeductionsViewModel PaymentDeductions
        {
            get
            {
                return PaymentDeductionsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PaymentDeductions property.
        /// </summary>
        public static void ClearPaymentDeductions()
        {
            if (paymentDeductionsViewModel != null)
            {
                paymentDeductionsViewModel.Cleanup();
                paymentDeductionsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the PaymentDeductions property.
        /// </summary>
        public static void CreatePaymentDeductions()
        {
            if (paymentDeductionsViewModel == null)
            {
                paymentDeductionsViewModel = new PaymentDeductionsViewModel();
            }
        }

        #endregion        

        #region Payment

        /// <summary>
        /// Gets the Payment property.
        /// </summary>
        public static PaymentViewModel PaymentStatic
        {
            get
            {
                if (paymentViewModel == null)
                {
                    CreatePayment();
                }
             
                return paymentViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public PaymentViewModel Payment
        {
            get
            {
                return PaymentStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Payment property.
        /// </summary>
        public static void ClearPayment()
        {
            if (paymentViewModel != null)
            {
                paymentViewModel.Cleanup();
                paymentViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Payment property.
        /// </summary>
        public static void CreatePayment()
        {
            if (paymentViewModel == null)
            {
                paymentViewModel = new PaymentViewModel();
            }
        }

        #endregion        

        #region Date

        /// <summary>
        /// Gets the Date property.
        /// </summary>
        public static DateViewModel DateStatic
        {
            get
            {
                if (dateViewModel == null)
                {
                    CreateDate();
                }
         
                return dateViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public DateViewModel Date
        {
            get
            {
                return DateStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Date property.
        /// </summary>
        public static void ClearDate()
        {
            if (dateViewModel != null)
            {
                dateViewModel.Cleanup();
                dateViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Date property.
        /// </summary>
        public static void CreateDate()
        {
            if (dateViewModel == null)
            {
                dateViewModel = new DateViewModel();
            }
        }

        #endregion        

        #region AllSales

        /// <summary>
        /// Gets the AllSales property.
        /// </summary>
        public static AllSalesViewModel AllSalesStatic
        {
            get
            {
                if (allSalesViewModel == null)
                {
                    CreateAllSales();
                }
      
                return allSalesViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public AllSalesViewModel AllSales
        {
            get
            {
                return AllSalesStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the AllSales property.
        /// </summary>
        public static void ClearAllSales()
        {
            if (allSalesViewModel != null)
            {
                allSalesViewModel.Cleanup();
                allSalesViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the AllSales property.
        /// </summary>
        public static void CreateAllSales()
        {
            if (allSalesViewModel == null)
            {
                allSalesViewModel = new AllSalesViewModel();
            }
        }

        #endregion        

        #region DispatchContainer

        /// <summary>
        /// Gets the DispatchContainer property.
        /// </summary>
        public static DispatchContainerViewModel DispatchContainerStatic
        {
            get
            {
                if (dispatchContainerViewModel == null)
                {
                    CreateDispatchContainer();
                }
      
                return dispatchContainerViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public DispatchContainerViewModel DispatchContainer
        {
            get
            {
                return DispatchContainerStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DispatchContainer property.
        /// </summary>
        public static void ClearDispatchContainer()
        {
            if (dispatchContainerViewModel != null)
            {
                dispatchContainerViewModel.Cleanup();
                dispatchContainerViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DispatchContainer property.
        /// </summary>
        public static void CreateDispatchContainer()
        {
            if (dispatchContainerViewModel == null)
            {
                dispatchContainerViewModel = new DispatchContainerViewModel();
            }
        }

        #endregion        

        #region SpecialPrices

        /// <summary>
        /// Gets the SpecialPrices property.
        /// </summary>
        public static SpecialPricesViewModel SpecialPricesStatic
        {
            get
            {
                if (specialPricesViewModel == null)
                {
                    CreateSpecialPrices();
                }
           
                return specialPricesViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SpecialPricesViewModel SpecialPrices
        {
            get
            {
                return SpecialPricesStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SpecialPrices property.
        /// </summary>
        public static void ClearSpecialPrices()
        {
            if (specialPricesViewModel != null)
            {
                specialPricesViewModel.Cleanup();
                specialPricesViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SpecialPrices property.
        /// </summary>
        public static void CreateSpecialPrices()
        {
            if (specialPricesViewModel == null)
            {
                specialPricesViewModel = new SpecialPricesViewModel();
            }
        }

        #endregion        
        
        #region ProductionProducts

        /// <summary>
        /// Gets the ProductionProducts property.
        /// </summary>
        public static ProductionProductsViewModel ProductionProductsStatic
        {
            get
            {
                if (productionProductsViewModel == null)
                {
                    CreateProductionProducts();
                }
           
                return productionProductsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ProductionProductsViewModel ProductionProducts
        {
            get
            {
                return ProductionProductsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ProductionProducts property.
        /// </summary>
        public static void ClearProductionProducts()
        {
            if (productionProductsViewModel != null)
            {
                productionProductsViewModel.Cleanup();
                productionProductsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ProductionProducts property.
        /// </summary>
        public static void CreateProductionProducts()
        {
            if (productionProductsViewModel == null)
            {
                productionProductsViewModel = new ProductionProductsViewModel();
            }
        }

        #endregion        

        #region ProductionProductsCards

        /// <summary>
        /// Gets the ProductionProductsCards property.
        /// </summary>
        public static ProductionProductsCardsViewModel ProductionProductsCardsStatic
        {
            get
            {
                if (productionProductsCardsViewModel == null)
                {
                    CreateProductionProductsCards();
                }
                
                return productionProductsCardsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ProductionProductsCardsViewModel ProductionProductsCards
        {
            get
            {
                return ProductionProductsCardsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ProductionProductsCards property.
        /// </summary>
        public static void ClearProductionProductsCards()
        {
            if (productionProductsCardsViewModel != null)
            {
                productionProductsCardsViewModel.Cleanup();
                productionProductsCardsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ProductionProductsCards property.
        /// </summary>
        public static void CreateProductionProductsCards()
        {
            if (productionProductsCardsViewModel == null)
            {
                productionProductsCardsViewModel = new ProductionProductsCardsViewModel();
            }
        }

        #endregion

        #region ProductionProductsGrid

        /// <summary>
        /// Gets the ProductionProductsGrid property.
        /// </summary>
        public static ProductionProductsGridViewModel ProductionProductsGridStatic
        {
            get
            {
                if (productionProductsGridViewModel == null)
                {
                    CreateProductionProductsGrid();
                }

                return productionProductsGridViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ProductionProductsGridViewModel ProductionProductsGrid
        {
            get
            {
                return ProductionProductsGridStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ProductionProductsGrid property.
        /// </summary>
        public static void ClearProductionProductsGrid()
        {
            if (productionProductsGridViewModel != null)
            {
                productionProductsGridViewModel.Cleanup();
                productionProductsGridViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ProductionProductsGrid property.
        /// </summary>
        public static void CreateProductionProductsGrid()
        {
            if (productionProductsGridViewModel == null)
            {
                productionProductsGridViewModel = new ProductionProductsGridViewModel();
            }
        }

        #endregion        

        #region StockMovementTouchscreen

        /// <summary>
        /// Gets the StockMovementTouchscreen property.
        /// </summary>
        public static StockMovementTouchscreenViewModel StockMovementTouchscreenStatic
        {
            get
            {
                if (stockMovementTouchscreenViewModel == null)
                {
                    CreateStockMovementTouchscreen();
                }

                return stockMovementTouchscreenViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public StockMovementTouchscreenViewModel StockMovementTouchscreen
        {
            get
            {
                return StockMovementTouchscreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the StockMovementTouchscreen property.
        /// </summary>
        public static void ClearStockMovementTouchscreen()
        {
            if (stockMovementTouchscreenViewModel != null)
            {
                stockMovementTouchscreenViewModel.Cleanup();
                stockMovementTouchscreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the StockMovementTouchscreen property.
        /// </summary>
        public static void CreateStockMovementTouchscreen()
        {
            if (stockMovementTouchscreenViewModel == null)
            {
                stockMovementTouchscreenViewModel = new StockMovementTouchscreenViewModel();
            }
        }

        #endregion

        #region LairageTouchscreen

        /// <summary>
        /// Gets the LairageTouchscreen property.
        /// </summary>
        public static LairageTouchscreenViewModel LairageTouchscreenStatic
        {
            get
            {
                if (lairageTouchscreenViewModel == null)
                {
                    CreateLairageTouchscreen();
                }
                
                return lairageTouchscreenViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LairageTouchscreenViewModel LairageTouchscreen
        {
            get
            {
                return LairageTouchscreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LairageTouchscreen property.
        /// </summary>
        public static void ClearLairageTouchscreen()
        {
            if (lairageTouchscreenViewModel != null)
            {
                lairageTouchscreenViewModel.Cleanup();
                lairageTouchscreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LairageTouchscreen property.
        /// </summary>
        public static void CreateLairageTouchscreen()
        {
            if (lairageTouchscreenViewModel == null)
            {
                lairageTouchscreenViewModel = new LairageTouchscreenViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsLairageTouchscreenNull()
        {
            return lairageTouchscreenViewModel == null;
        }

        #endregion

        #region LairageGraderCombo

        /// <summary>
        /// Gets the LairageGraderCombo property.
        /// </summary>
        public static LairageGraderComboViewModel LairageGraderComboStatic
        {
            get
            {
                if (lairageGraderComboViewModel == null)
                {
                    CreateLairageGraderCombo();
                }
          
                return lairageGraderComboViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LairageGraderComboViewModel LairageGraderCombo
        {
            get
            {
                return LairageGraderComboStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LairageGraderCombo property.
        /// </summary>
        public static void ClearLairageGraderCombo()
        {
            if (lairageGraderComboViewModel != null)
            {
                lairageGraderComboViewModel.Cleanup();
                lairageGraderComboViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LairageGraderCombo property.
        /// </summary>
        public static void CreateLairageGraderCombo()
        {
            if (lairageGraderComboViewModel == null)
            {
                lairageGraderComboViewModel = new LairageGraderComboViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsLairageGraderComboNull()
        {
            return lairageGraderComboViewModel == null;
        }

        #endregion        

        #region Lairage

        /// <summary>
        /// Gets the Lairage property.
        /// </summary>
        public static LairageViewModel LairageStatic
        {
            get
            {
                if (lairageViewModel == null)
                {
                    CreateLairage();
                }
    
                return lairageViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LairageViewModel Lairage
        {
            get
            {
                return LairageStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Lairage property.
        /// </summary>
        public static void ClearLairage()
        {
            if (lairageViewModel != null)
            {
                lairageViewModel.Cleanup();
                lairageViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Lairage property.
        /// </summary>
        public static void CreateLairage()
        {
            if (lairageViewModel == null)
            {
                lairageViewModel = new LairageViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsLairageNull()
        {
            return lairageViewModel == null;
        }

        #endregion        

        #region LairageSearch

        /// <summary>
        /// Gets the LairageSearch property.
        /// </summary>
        public static LairageSearchViewModel LairageSearchStatic
        {
            get
            {
                if (lairageSearchViewModel == null)
                {
                    CreateLairageSearch();
                }
            
                return lairageSearchViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LairageSearchViewModel LairageSearch
        {
            get
            {
                return LairageSearchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LairageSearch property.
        /// </summary>
        public static void ClearLairageSearch()
        {
            if (lairageSearchViewModel != null)
            {
                lairageSearchViewModel.Cleanup();
                lairageSearchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LairageSearch property.
        /// </summary>
        public static void CreateLairageSearch()
        {
            if (lairageSearchViewModel == null)
            {
                lairageSearchViewModel = new LairageSearchViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsLairageSearchNull()
        {
            return lairageSearchViewModel == null;
        }

        #endregion        

        #region SyncPartners

        /// <summary>
        /// Gets the SyncPartners property.
        /// </summary>
        public static SyncPartnersViewModel SyncPartnersStatic
        {
            get
            {
                if (syncPartnersViewModel == null)
                {
                    CreateSyncPartners();
                }
                
                return syncPartnersViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SyncPartnersViewModel SyncPartners
        {
            get
            {
                return SyncPartnersStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SyncPartners property.
        /// </summary>
        public static void ClearSyncPartners()
        {
            if (syncPartnersViewModel != null)
            {
                syncPartnersViewModel.Cleanup();
                syncPartnersViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SyncPartners property.
        /// </summary>
        public static void CreateSyncPartners()
        {
            if (syncPartnersViewModel == null)
            {
                syncPartnersViewModel = new SyncPartnersViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsSyncPartnersNull()
        {
            return syncPartnersViewModel == null;
        }

        #endregion        

        #region WorkflowIndicator

        /// <summary>
        /// Gets the WorkflowIndicator property.
        /// </summary>
        public static WorkflowIndicatorViewModel WorkflowIndicatorStatic
        {
            get
            {
                if (workflowIndicatorViewModel == null)
                {
                    CreateWorkflowIndicator();
                }

                return workflowIndicatorViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowIndicatorViewModel WorkflowIndicator
        {
            get
            {
                return WorkflowIndicatorStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowIndicator property.
        /// </summary>
        public static void ClearWorkflowIndicator()
        {
            if (workflowIndicatorViewModel != null)
            {
                workflowIndicatorViewModel.Cleanup();
                workflowIndicatorViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowIndicator property.
        /// </summary>
        public static void CreateWorkflowIndicator()
        {
            if (workflowIndicatorViewModel == null)
            {
                workflowIndicatorViewModel = new WorkflowIndicatorViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowIndicatorNull()
        {
            return workflowIndicatorViewModel == null;
        }

        #endregion        
        
        #region WorkflowCalender

        /// <summary>
        /// Gets the WorkflowCalender property.
        /// </summary>
        public static WorkflowCalenderViewModel WorkflowCalenderStatic
        {
            get
            {
                if (workflowCalenderViewModel == null)
                {
                    CreateWorkflowCalender();
                }

                return workflowCalenderViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowCalenderViewModel WorkflowCalender
        {
            get
            {
                return WorkflowCalenderStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowCalender property.
        /// </summary>
        public static void ClearWorkflowCalender()
        {
            if (workflowCalenderViewModel != null)
            {
                workflowCalenderViewModel.Cleanup();
                workflowCalenderViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowCalender property.
        /// </summary>
        public static void CreateWorkflowCalender()
        {
            if (workflowCalenderViewModel == null)
            {
                workflowCalenderViewModel = new WorkflowCalenderViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowCalenderNull()
        {
            return workflowCalenderViewModel == null;
        }

        #endregion        

        #region WorkflowCollection

        /// <summary>
        /// Gets the WorkflowCollection property.
        /// </summary>
        public static WorkflowCollectionViewModel WorkflowCollectionStatic
        {
            get
            {
                if (workflowCollectionViewModel == null)
                {
                    CreateWorkflowCollection();
                }
          
                return workflowCollectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowCollectionViewModel WorkflowCollection
        {
            get
            {
                return WorkflowCollectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowCollection property.
        /// </summary>
        public static void ClearWorkflowCollection()
        {
            if (workflowCollectionViewModel != null)
            {
                workflowCollectionViewModel.Cleanup();
                workflowCollectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowCollection property.
        /// </summary>
        public static void CreateWorkflowCollection()
        {
            if (workflowCollectionViewModel == null)
            {
                workflowCollectionViewModel = new WorkflowCollectionViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowCollectionNull()
        {
            return workflowCollectionViewModel == null;
        }

        #endregion        

        #region WorkflowSummary

        /// <summary>
        /// Gets the WorkflowSummary property.
        /// </summary>
        public static WorkflowSummaryViewModel WorkflowSummaryStatic
        {
            get
            {
                if (workflowSummaryViewModel == null)
                {
                    CreateWorkflowSummary();
                }

                return workflowSummaryViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowSummaryViewModel WorkflowSummary
        {
            get
            {
                return WorkflowSummaryStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowSummary property.
        /// </summary>
        public static void ClearWorkflowSummary()
        {
            if (workflowSummaryViewModel != null)
            {
                workflowSummaryViewModel.Cleanup();
                workflowSummaryViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowSummary property.
        /// </summary>
        public static void CreateWorkflowSummary()
        {
            if (workflowSummaryViewModel == null)
            {
                workflowSummaryViewModel = new WorkflowSummaryViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowSummaryNull()
        {
            return workflowSummaryViewModel == null;
        }

        #endregion   
     
        #region WorkflowSearch

        /// <summary>
        /// Gets the WorkflowSearch property.
        /// </summary>
        public static WorkflowSearchViewModel WorkflowSearchStatic
        {
            get
            {
                if (workflowSearchViewModel == null)
                {
                    CreateWorkflowSearch();
                }
         
                return workflowSearchViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowSearchViewModel WorkflowSearch
        {
            get
            {
                return WorkflowSearchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowSearch property.
        /// </summary>
        public static void ClearWorkflowSearch()
        {
            if (workflowSearchViewModel != null)
            {
                workflowSearchViewModel.Cleanup();
                workflowSearchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowSearch property.
        /// </summary>
        public static void CreateWorkflowSearch()
        {
            if (workflowSearchViewModel == null)
            {
                workflowSearchViewModel = new WorkflowSearchViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowSearchNull()
        {
            return workflowSearchViewModel == null;
        }

        #endregion        

        #region Workflow

        /// <summary>
        /// Gets the Workflow property.
        /// </summary>
        public static WorkflowViewModel WorkflowStatic
        {
            get
            {
                if (workflowViewModel == null)
                {
                    CreateWorkflow();
                }
         
                return workflowViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowViewModel Workflow
        {
            get
            {
                return WorkflowStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Workflow property.
        /// </summary>
        public static void ClearWorkflow()
        {
            if (workflowViewModel != null)
            {
                workflowViewModel.Cleanup();
                workflowViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Workflow property.
        /// </summary>
        public static void CreateWorkflow()
        {
            if (workflowViewModel == null)
            {
                workflowViewModel = new WorkflowViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowNull()
        {
            return workflowViewModel == null;
        }

        #endregion        

        #region WorkflowKeyboard

        /// <summary>
        /// Gets the WorkflowKeyboard property.
        /// </summary>
        public static WorkflowKeyboardViewModel WorkflowKeyboardStatic
        {
            get
            {
                if (workflowKeyboardViewModel == null)
                {
                    CreateWorkflowKeyboard();
                }
          
                return workflowKeyboardViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowKeyboardViewModel WorkflowKeyboard
        {
            get
            {
                return WorkflowKeyboardStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowKeyboard property.
        /// </summary>
        public static void ClearWorkflowKeyboard()
        {
            if (workflowKeyboardViewModel != null)
            {
                workflowKeyboardViewModel.Cleanup();
                workflowKeyboardViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowKeyboard property.
        /// </summary>
        public static void CreateWorkflowKeyboard()
        {
            if (workflowKeyboardViewModel == null)
            {
                workflowKeyboardViewModel = new WorkflowKeyboardViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowKeyboardNull()
        {
            return workflowKeyboardViewModel == null;
        }

        #endregion    
    
        #region WorkflowAlerts

        /// <summary>
        /// Gets the WorkflowAlerts property.
        /// </summary>
        public static WorkflowAlertsViewModel WorkflowAlertsStatic
        {
            get
            {
                if (workflowAlertsViewModel == null)
                {
                    CreateWorkflowAlerts();
                }

                return workflowAlertsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowAlertsViewModel WorkflowAlerts
        {
            get
            {
                return WorkflowAlertsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowAlerts property.
        /// </summary>
        public static void ClearWorkflowAlerts()
        {
            if (workflowAlertsViewModel != null)
            {
                workflowAlertsViewModel.Cleanup();
                workflowAlertsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowAlerts property.
        /// </summary>
        public static void CreateWorkflowAlerts()
        {
            if (workflowAlertsViewModel == null)
            {
                workflowAlertsViewModel = new WorkflowAlertsViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowAlertsNull()
        {
            return workflowAlertsViewModel == null;
        }

        #endregion        

        #region WorkflowKeypad

        /// <summary>
        /// Gets the WorkflowKeypad property.
        /// </summary>
        public static WorkflowKeypadViewModel WorkflowKeypadStatic
        {
            get
            {
                if (workflowKeypadViewModel == null)
                {
                    CreateWorkflowKeypad();
                }
        
                return workflowKeypadViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowKeypadViewModel WorkflowKeypad
        {
            get
            {
                return WorkflowKeypadStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowKeypad property.
        /// </summary>
        public static void ClearWorkflowKeypad()
        {
            if (workflowKeypadViewModel != null)
            {
                workflowKeypadViewModel.Cleanup();
                workflowKeypadViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowKeypad property.
        /// </summary>
        public static void CreateWorkflowKeypad()
        {
            if (workflowKeypadViewModel == null)
            {
                workflowKeypadViewModel = new WorkflowKeypadViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowKeypadNull()
        {
            return workflowKeypadViewModel == null;
        }

        #endregion        

        #region WorkflowSelection

        /// <summary>
        /// Gets the WorkflowSelection property.
        /// </summary>
        public static WorkflowSelectionViewModel WorkflowSelectionStatic
        {
            get
            {
                if (workflowSelectionViewModel == null)
                {
                    CreateWorkflowSelection();
                }

                return workflowSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowSelectionViewModel WorkflowSelection
        {
            get
            {
                return WorkflowSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowSelection property.
        /// </summary>
        public static void ClearWorkflowSelection()
        {
            if (workflowSelectionViewModel != null)
            {
                workflowSelectionViewModel.Cleanup();
                workflowSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowSelection property.
        /// </summary>
        public static void CreateWorkflowSelection()
        {
            if (workflowSelectionViewModel == null)
            {
                workflowSelectionViewModel = new WorkflowSelectionViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowSelectionNull()
        {
            return workflowSelectionViewModel == null;
        }

        #endregion        

        #region WorkflowTemplateSelection

        /// <summary>
        /// Gets the WorkflowTemplateSelection property.
        /// </summary>
        public static WorkflowTemplateSelectionViewModel WorkflowTemplateSelectionStatic
        {
            get
            {
                if (workflowTemplateSelectionViewModel == null)
                {
                    CreateWorkflowTemplateSelection();
                }
                
                return workflowTemplateSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WorkflowTemplateSelectionViewModel WorkflowTemplateSelection
        {
            get
            {
                return WorkflowTemplateSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WorkflowTemplateSelection property.
        /// </summary>
        public static void ClearWorkflowTemplateSelection()
        {
            if (workflowTemplateSelectionViewModel != null)
            {
                workflowTemplateSelectionViewModel.Cleanup();
                workflowTemplateSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WorkflowTemplateSelection property.
        /// </summary>
        public static void CreateWorkflowTemplateSelection()
        {
            if (workflowTemplateSelectionViewModel == null)
            {
                workflowTemplateSelectionViewModel = new WorkflowTemplateSelectionViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsWorkflowTemplateSelectionNull()
        {
            return workflowTemplateSelectionViewModel == null;
        }

        #endregion        

        #region IntoProduction

        /// <summary>
        /// Gets the IntoProduction property.
        /// </summary>
        public static IntoProductionViewModel IntoProductionStatic
        {
            get
            {
                if (intoProductionViewModel == null)
                {
                    CreateIntoProduction();
                }

                return intoProductionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public IntoProductionViewModel IntoProduction
        {
            get
            {
                return IntoProductionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the IntoProduction property.
        /// </summary>
        public static void ClearIntoProduction()
        {
            if (intoProductionViewModel != null)
            {
                intoProductionViewModel.Cleanup();
                intoProductionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the IntoProduction property.
        /// </summary>
        public static void CreateIntoProduction()
        {
            if (intoProductionViewModel == null)
            {
                intoProductionViewModel = new IntoProductionViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsIntoProductionNull()
        {
            return intoProductionViewModel == null;
        }

        #endregion        

        #region OutOfProduction

        /// <summary>
        /// Gets the OutOfProduction property.
        /// </summary>
        public static OutOfProductionViewModel OutOfProductionStatic
        {
            get
            {
                if (outOfProductionViewModel == null)
                {
                    CreateOutOfProduction();
                }

                return outOfProductionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public OutOfProductionViewModel OutOfProduction
        {
            get
            {
                return OutOfProductionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the OutOfProduction property.
        /// </summary>
        public static void ClearOutOfProduction()
        {
            if (outOfProductionViewModel != null)
            {
                outOfProductionViewModel.Cleanup();
                outOfProductionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the OutOfProduction property.
        /// </summary>
        public static void CreateOutOfProduction()
        {
            if (outOfProductionViewModel == null)
            {
                outOfProductionViewModel = new OutOfProductionViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsOutOfProductionNull()
        {
            return outOfProductionViewModel == null;
        }

        #endregion        

        #region OutOfProductionHeader

        /// <summary>
        /// Gets the OutOfProductionHeader property.
        /// </summary>
        public static OutOfProductionHeaderViewModel OutOfProductionHeaderStatic
        {
            get
            {
                if (outOfProductionHeaderViewModel == null)
                {
                    CreateOutOfProductionHeader();
                }
           
                return outOfProductionHeaderViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public OutOfProductionHeaderViewModel OutOfProductionHeader
        {
            get
            {
                return OutOfProductionHeaderStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the OutOfProductionHeader property.
        /// </summary>
        public static void ClearOutOfProductionHeader()
        {
            if (outOfProductionHeaderViewModel != null)
            {
                outOfProductionHeaderViewModel.Cleanup();
                outOfProductionHeaderViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the OutOfProductionHeader property.
        /// </summary>
        public static void CreateOutOfProductionHeader()
        {
            if (outOfProductionHeaderViewModel == null)
            {
                outOfProductionHeaderViewModel = new OutOfProductionHeaderViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsOutOfProductionHeaderNull()
        {
            return outOfProductionHeaderViewModel == null;
        }

        #endregion        

        #region OutOfProductionSplitHeader

        /// <summary>
        /// Gets the OutOfProductionSplitHeader property.
        /// </summary>
        public static OutOfProductionSplitHeaderViewModel OutOfProductionSplitHeaderStatic
        {
            get
            {
                if (outOfProductionSplitHeaderViewModel == null)
                {
                    CreateOutOfProductionSplitHeader();
                }
   
                return outOfProductionSplitHeaderViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public OutOfProductionSplitHeaderViewModel OutOfProductionSplitHeader
        {
            get
            {
                return OutOfProductionSplitHeaderStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the OutOfProductionSplitHeader property.
        /// </summary>
        public static void ClearOutOfProductionSplitHeader()
        {
            if (outOfProductionSplitHeaderViewModel != null)
            {
                outOfProductionSplitHeaderViewModel.Cleanup();
                outOfProductionSplitHeaderViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the OutOfProductionSplitHeader property.
        /// </summary>
        public static void CreateOutOfProductionSplitHeader()
        {
            if (outOfProductionSplitHeaderViewModel == null)
            {
                outOfProductionSplitHeaderViewModel = new OutOfProductionSplitHeaderViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsOutOfProductionSplitHeaderNull()
        {
            return outOfProductionSplitHeaderViewModel == null;
        }

        #endregion        

        #region ScannerCustomers

        /// <summary>
        /// Gets the ScannerCustomers property.
        /// </summary>
        public static ScannerCustomersViewModel ScannerCustomersStatic
        {
            get
            {
                if (scannerCustomersViewModel == null)
                {
                    CreateScannerCustomers();
                }
           
                return scannerCustomersViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ScannerCustomersViewModel ScannerCustomers
        {
            get
            {
                return ScannerCustomersStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ScannerCustomers property.
        /// </summary>
        public static void ClearScannerCustomers()
        {
            if (scannerCustomersViewModel != null)
            {
                scannerCustomersViewModel.Cleanup();
                scannerCustomersViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ScannerCustomers property.
        /// </summary>
        public static void CreateScannerCustomers()
        {
            if (scannerCustomersViewModel == null)
            {
                scannerCustomersViewModel = new ScannerCustomersViewModel();
            }
        }

        #endregion        

        #region ScannerPalletisation

        /// <summary>
        /// Gets the ScannerPalletisation property.
        /// </summary>
        public static ScannerPalletisationViewModel ScannerPalletisationStatic
        {
            get
            {
                if (scannerPalletisationViewModel == null)
                {
                    CreateScannerPalletisation();
                }
                
                return scannerPalletisationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ScannerPalletisationViewModel ScannerPalletisation
        {
            get
            {
                return ScannerPalletisationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ScannerPalletisation property.
        /// </summary>
        public static void ClearScannerPalletisation()
        {
            if (scannerPalletisationViewModel != null)
            {
                scannerPalletisationViewModel.Cleanup();
                scannerPalletisationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ScannerPalletisation property.
        /// </summary>
        public static void CreateScannerPalletisation()
        {
            if (scannerPalletisationViewModel == null)
            {
                scannerPalletisationViewModel = new ScannerPalletisationViewModel();
            }
        }

        #endregion        

        #region EditTransaction

        /// <summary>
        /// Gets the EditTransaction property.
        /// </summary>
        public static EditTransactionViewModel EditTransactionStatic
        {
            get
            {
                if (editTransactionViewModel == null)
                {
                    CreateEditTransaction();
                }
         
                return editTransactionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public EditTransactionViewModel EditTransaction
        {
            get
            {
                return EditTransactionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the EditTransaction property.
        /// </summary>
        public static void ClearEditTransaction()
        {
            if (editTransactionViewModel != null)
            {
                editTransactionViewModel.Cleanup();
                editTransactionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the EditTransaction property.
        /// </summary>
        public static void CreateEditTransaction()
        {
            if (editTransactionViewModel == null)
            {
                editTransactionViewModel = new EditTransactionViewModel();
            }
        }

        #endregion        

        #region TransactionManager

        /// <summary>
        /// Gets the TransactionManager property.
        /// </summary>
        public static TransactionManagerViewModel TransactionManagerStatic
        {
            get
            {
                if (transactionManagerViewModel == null)
                {
                    CreateTransactionManager();
                }
                
                return transactionManagerViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TransactionManagerViewModel TransactionManager
        {
            get
            {
                return TransactionManagerStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TransactionManager property.
        /// </summary>
        public static void ClearTransactionManager()
        {
            if (transactionManagerViewModel != null)
            {
                transactionManagerViewModel.Cleanup();
                transactionManagerViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TransactionManager property.
        /// </summary>
        public static void CreateTransactionManager()
        {
            if (transactionManagerViewModel == null)
            {
                transactionManagerViewModel = new TransactionManagerViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsTransactionManagerNull()
        {
            return transactionManagerViewModel == null;
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsTransactionManagerLoaded()
        {
            return transactionManagerViewModel != null && transactionManagerViewModel.IsFormLoaded && ApplicationSettings.ScanningForBatchAtProductionOrders;
        }

        #endregion        

        #region Label Selection

        /// <summary>
        /// Gets the LabelSelection property.
        /// </summary>
        public static LabelSelectionViewModel LabelSelectionStatic
        {
            get
            {
                if (labelSelectionViewModel == null)
                {
                    CreateLabelSelection();
                }
              
                return labelSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LabelSelectionViewModel LabelSelection
        {
            get
            {
                return LabelSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LabelSelection property.
        /// </summary>
        public static void ClearLabelSelection()
        {
            if (labelSelectionViewModel != null)
            {
                labelSelectionViewModel.Cleanup();
                labelSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LabelSelection property.
        /// </summary>
        public static void CreateLabelSelection()
        {
            if (labelSelectionViewModel == null)
            {
                labelSelectionViewModel = new LabelSelectionViewModel();
            }
        }

        #endregion        

        #region Warehouse Selection

        /// <summary>
        /// Gets the WarehouseSelection property.
        /// </summary>
        public static WarehouseSelectionViewModel WarehouseSelectionStatic
        {
            get
            {
                if (warehouseSelectionViewModel == null)
                {
                    CreateWarehouseSelection();
                }
              
                return warehouseSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WarehouseSelectionViewModel WarehouseSelection
        {
            get
            {
                return WarehouseSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WarehouseSelection property.
        /// </summary>
        public static void ClearWarehouseSelection()
        {
            if (warehouseSelectionViewModel != null)
            {
                warehouseSelectionViewModel.Cleanup();
                warehouseSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WarehouseSelection property.
        /// </summary>
        public static void CreateWarehouseSelection()
        {
            if (warehouseSelectionViewModel == null)
            {
                warehouseSelectionViewModel = new WarehouseSelectionViewModel();
            }
        }

        #endregion        

        #region Keyboard

        /// <summary>
        /// Gets the Keyboard property.
        /// </summary>
        public static KeyboardViewModel KeyboardStatic
        {
            get
            {
                if (keyboardViewModel == null)
                {
                    CreateKeyboard();
                }
               
                return keyboardViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public KeyboardViewModel Keyboard
        {
            get
            {
                return KeyboardStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Keyboard property.
        /// </summary>
        public static void ClearKeyboard()
        {
            if (keyboardViewModel != null)
            {
                keyboardViewModel.Cleanup();
                keyboardViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Keyboard property.
        /// </summary>
        public static void CreateKeyboard()
        {
            if (keyboardViewModel == null)
            {
                keyboardViewModel = new KeyboardViewModel();
            }
        }

        #endregion        

        #region Touchscreen

        /// <summary>
        /// Gets the Touchscreen property.
        /// </summary>
        public static TouchscreenViewModel TouchscreenStatic
        {
            get
            {
                if (touchscreenViewModel == null)
                {
                    CreateTouchscreen();
                }
              
                return touchscreenViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenViewModel Touchscreen
        {
            get
            {
                return TouchscreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Touchscreen property.
        /// </summary>
        public static void ClearTouchscreen()
        {
            if (touchscreenViewModel != null)
            {
                touchscreenViewModel.Cleanup();
                touchscreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Touchscreen property.
        /// </summary>
        public static void CreateTouchscreen()
        {
            if (touchscreenViewModel == null)
            {
                touchscreenViewModel = new TouchscreenViewModel();
            }
        }

        #endregion        

        #region TouchscreenReceiptPalletPanel

        /// <summary>
        /// Gets the TouchscreenReceiptPalletPanel property.
        /// </summary>
        public static TouchscreenReceiptPalletPanelViewModel TouchscreenReceiptPalletPanelStatic
        {
            get
            {
                if (touchscreenReceiptPalletPanelViewModel == null)
                {
                    CreateTouchscreenReceiptPalletPanel();
                }
             
                return touchscreenReceiptPalletPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenReceiptPalletPanelViewModel TouchscreenReceiptPalletPanel
        {
            get
            {
                return TouchscreenReceiptPalletPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenReceiptPalletPanel property.
        /// </summary>
        public static void ClearTouchscreenReceiptPalletPanel()
        {
            if (touchscreenReceiptPalletPanelViewModel != null)
            {
                touchscreenReceiptPalletPanelViewModel.Cleanup();
                touchscreenReceiptPalletPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenReceiptPalletPanel property.
        /// </summary>
        public static void CreateTouchscreenReceiptPalletPanel()
        {
            if (touchscreenReceiptPalletPanelViewModel == null)
            {
                touchscreenReceiptPalletPanelViewModel = new TouchscreenReceiptPalletPanelViewModel();
            }
        }

        #endregion  
      
        #region TouchscreenProductionButcheryPanel

        /// <summary>
        /// Gets the TouchscreenProductionButcheryPanel property.
        /// </summary>
        public static TouchscreenProductionButcheryPanelViewModel TouchscreenProductionButcheryPanelStatic
        {
            get
            {
                if (touchscreenProductionButcheryPanelViewModel == null)
                {
                    CreateTouchscreenProductionButcheryPanel();
                }


                return touchscreenProductionButcheryPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductionButcheryPanelViewModel TouchscreenProductionButcheryPanel
        {
            get
            {
                return TouchscreenProductionButcheryPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProductionButcheryPanel property.
        /// </summary>
        public static void ClearTouchscreenProductionButcheryPanel()
        {
            if (touchscreenProductionButcheryPanelViewModel != null)
            {
                touchscreenProductionButcheryPanelViewModel.Cleanup();
                touchscreenProductionButcheryPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProductionButcheryPanel property.
        /// </summary>
        public static void CreateTouchscreenProductionButcheryPanel()
        {
            if (touchscreenProductionButcheryPanelViewModel == null)
            {
                touchscreenProductionButcheryPanelViewModel = new TouchscreenProductionButcheryPanelViewModel();
            }
        }

        #endregion    
    
        #region TouchscreenWarehouse

        /// <summary>
        /// Gets the TouchscreenWarehouse property.
        /// </summary>
        public static TouchscreenWarehouseViewModel TouchscreenWarehouseStatic
        {
            get
            {
                if (touchscreenWarehouseViewModel == null)
                {
                    CreateTouchscreenWarehouse();
                }


                return touchscreenWarehouseViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenWarehouseViewModel TouchscreenWarehouse
        {
            get
            {
                return TouchscreenWarehouseStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenWarehouse property.
        /// </summary>
        public static void ClearTouchscreenWarehouse()
        {
            if (touchscreenWarehouseViewModel != null)
            {
                touchscreenWarehouseViewModel.Cleanup();
                touchscreenWarehouseViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenWarehouse property.
        /// </summary>
        public static void CreateTouchscreenWarehouse()
        {
            if (touchscreenWarehouseViewModel == null)
            {
                touchscreenWarehouseViewModel = new TouchscreenWarehouseViewModel();
            }
        }

        #endregion      
  
        #region StockMovementPanel

        /// <summary>
        /// Gets the StockMovementPanel property.
        /// </summary>
        public static StockMovementPanelViewModel StockMovementPanelStatic
        {
            get
            {
                if (stockMovementPanelViewModel == null)
                {
                    CreateStockMovementPanel();
                }


                return stockMovementPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public StockMovementPanelViewModel StockMovementPanel
        {
            get
            {
                return StockMovementPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the StockMovementPanel property.
        /// </summary>
        public static void ClearStockMovementPanel()
        {
            if (stockMovementPanelViewModel != null)
            {
                stockMovementPanelViewModel.Cleanup();
                stockMovementPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the StockMovementPanel property.
        /// </summary>
        public static void CreateStockMovementPanel()
        {
            if (stockMovementPanelViewModel == null)
            {
                stockMovementPanelViewModel = new StockMovementPanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenProductionOrders

        /// <summary>
        /// Gets the TouchscreenProductionOrders property.
        /// </summary>
        public static TouchscreenProductionOrdersViewModel TouchscreenProductionOrdersStatic
        {
            get
            {
                if (touchscreenProductionOrdersViewModel == null)
                {
                    CreateTouchscreenProductionOrders();
                }

                //currentViewModel = touchscreenProductionOrdersViewModel;
                return touchscreenProductionOrdersViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductionOrdersViewModel TouchscreenProductionOrders
        {
            get
            {
                return TouchscreenProductionOrdersStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProductionOrders property.
        /// </summary>
        public static void ClearTouchscreenProductionOrders()
        {
            if (touchscreenProductionOrdersViewModel != null)
            {
                touchscreenProductionOrdersViewModel.Cleanup();
                touchscreenProductionOrdersViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProductionOrders property.
        /// </summary>
        public static void CreateTouchscreenProductionOrders()
        {
            if (touchscreenProductionOrdersViewModel == null)
            {
                touchscreenProductionOrdersViewModel = new TouchscreenProductionOrdersViewModel();
            }
        }

        #endregion

        #region TouchscreenIntakeBatches

        /// <summary>
        /// Gets the TouchscreenIntakeBatches property.
        /// </summary>
        public static TouchscreenIntakeBatchesViewModel TouchscreenIntakeBatchesStatic
        {
            get
            {
                if (touchscreenIntakeBatchesViewModel == null)
                {
                    CreateTouchscreenIntakeBatches();
                }

                //currentViewModel = touchscreenIntakeBatchesViewModel;
                return touchscreenIntakeBatchesViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenIntakeBatchesViewModel TouchscreenIntakeBatches
        {
            get
            {
                return TouchscreenIntakeBatchesStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenIntakeBatches property.
        /// </summary>
        public static void ClearTouchscreenIntakeBatches()
        {
            if (touchscreenIntakeBatchesViewModel != null)
            {
                touchscreenIntakeBatchesViewModel.Cleanup();
                touchscreenIntakeBatchesViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenIntakeBatches property.
        /// </summary>
        public static void CreateTouchscreenIntakeBatches()
        {
            if (touchscreenIntakeBatchesViewModel == null)
            {
                touchscreenIntakeBatchesViewModel = new TouchscreenIntakeBatchesViewModel();
            }
        }

        #endregion        

        #region TouchscreenReceiptPanel

        /// <summary>
        /// Gets the TouchscreenReceiptPanel property.
        /// </summary>
        public static TouchscreenReceiptPanelViewModel TouchscreenReceiptPanelStatic
        {
            get
            {
                if (touchscreenReceiptPanelViewModel == null)
                {
                    CreateTouchscreenReceiptPanel();
                }
              
                return touchscreenReceiptPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenReceiptPanelViewModel TouchscreenReceiptPanel
        {
            get
            {
                return TouchscreenReceiptPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenReceiptPanel property.
        /// </summary>
        public static void ClearTouchscreenReceiptPanel()
        {
            if (touchscreenReceiptPanelViewModel != null)
            {
                touchscreenReceiptPanelViewModel.Cleanup();
                touchscreenReceiptPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenReceiptPanel property.
        /// </summary>
        public static void CreateTouchscreenReceiptPanel()
        {
            if (touchscreenReceiptPanelViewModel == null)
            {
                touchscreenReceiptPanelViewModel = new TouchscreenReceiptPanelViewModel();
            }
        }

        #endregion

        #region TouchscreenReceiptPanelWithBarcodePrint

        /// <summary>
        /// Gets the TouchscreenReceiptPanelWithBarcodePrint property.
        /// </summary>
        public static TouchscreenReceiptPanelWithBarcodePrintViewModel TouchscreenReceiptPanelWithBarcodePrintStatic
        {
            get
            {
                if (touchscreenReceiptPanelWithBarcodePrintViewModel == null)
                {
                    CreateTouchscreenReceiptPanelWithBarcodePrint();
                }
                
                return touchscreenReceiptPanelWithBarcodePrintViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenReceiptPanelWithBarcodePrintViewModel TouchscreenReceiptPanelWithBarcodePrint
        {
            get
            {
                return TouchscreenReceiptPanelWithBarcodePrintStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenReceiptPanelWithBarcodePrint property.
        /// </summary>
        public static void ClearTouchscreenReceiptPanelWithBarcodePrint()
        {
            if (touchscreenReceiptPanelWithBarcodePrintViewModel != null)
            {
                touchscreenReceiptPanelWithBarcodePrintViewModel.Cleanup();
                touchscreenReceiptPanelWithBarcodePrintViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenReceiptPanelWithBarcodePrint property.
        /// </summary>
        public static void CreateTouchscreenReceiptPanelWithBarcodePrint()
        {
            if (touchscreenReceiptPanelWithBarcodePrintViewModel == null)
            {
                touchscreenReceiptPanelWithBarcodePrintViewModel = new TouchscreenReceiptPanelWithBarcodePrintViewModel();
            }
        }

        #endregion        

        #region TouchscreenReceiptProductionPanel

        /// <summary>
        /// Gets the TouchscreenReceiptProductionPanel property.
        /// </summary>
        public static TouchscreenReceiptProductionPanelViewModel TouchscreenReceiptProductionPanelStatic
        {
            get
            {
                if (touchscreenReceiptProductionPanelViewModel == null)
                {
                    CreateTouchscreenReceiptProductionPanel();
                }
          
                return touchscreenReceiptProductionPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenReceiptProductionPanelViewModel TouchscreenReceiptProductionPanel
        {
            get
            {
                return TouchscreenReceiptProductionPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenReceiptProductionPanel property.
        /// </summary>
        public static void ClearTouchscreenReceiptProductionPanel()
        {
            if (touchscreenReceiptProductionPanelViewModel != null)
            {
                touchscreenReceiptProductionPanelViewModel.Cleanup();
                touchscreenReceiptProductionPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenReceiptProductionPanel property.
        /// </summary>
        public static void CreateTouchscreenReceiptProductionPanel()
        {
            if (touchscreenReceiptProductionPanelViewModel == null)
            {
                touchscreenReceiptProductionPanelViewModel = new TouchscreenReceiptProductionPanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenProductionReceipePanel

        /// <summary>
        /// Gets the TouchscreenProductionReceipePanel property.
        /// </summary>
        public static TouchscreenProductionReceipePanelViewModel TouchscreenProductionReceipePanelStatic
        {
            get
            {
                if (touchscreenProductionReceipePanelViewModel == null)
                {
                    CreateTouchscreenProductionReceipePanel();
                }
            
                return touchscreenProductionReceipePanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductionReceipePanelViewModel TouchscreenProductionReceipePanel
        {
            get
            {
                return TouchscreenProductionReceipePanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProductionReceipePanel property.
        /// </summary>
        public static void ClearTouchscreenProductionReceipePanel()
        {
            if (touchscreenProductionReceipePanelViewModel != null)
            {
                touchscreenProductionReceipePanelViewModel.Cleanup();
                touchscreenProductionReceipePanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProductionReceipePanel property.
        /// </summary>
        public static void CreateTouchscreenProductionReceipePanel()
        {
            if (touchscreenProductionReceipePanelViewModel == null)
            {
                touchscreenProductionReceipePanelViewModel = new TouchscreenProductionReceipePanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenProductionPanel

        /// <summary>
        /// Gets the TouchscreenProductionPanel property.
        /// </summary>
        public static TouchscreenProductionPanelViewModel TouchscreenProductionPanelStatic
        {
            get
            {
                if (touchscreenProductionPanelViewModel == null)
                {
                    CreateTouchscreenProductionPanel();
                }

                return touchscreenProductionPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductionPanelViewModel TouchscreenProductionPanel
        {
            get
            {
                return TouchscreenProductionPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProductionPanel property.
        /// </summary>
        public static void ClearTouchscreenProductionPanel()
        {
            if (touchscreenProductionPanelViewModel != null)
            {
                touchscreenProductionPanelViewModel.Cleanup();
                touchscreenProductionPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProductionPanel property.
        /// </summary>
        public static void CreateTouchscreenProductionPanel()
        {
            if (touchscreenProductionPanelViewModel == null)
            {
                touchscreenProductionPanelViewModel = new TouchscreenProductionPanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenProductionRecallRecallPanel

        /// <summary>
        /// Gets the TouchscreenProductionRecallPanel property.
        /// </summary>
        public static TouchscreenProductionRecallPanelViewModel TouchscreenProductionRecallPanelStatic
        {
            get
            {
                if (touchscreenProductionRecallPanelViewModel == null)
                {
                    CreateTouchscreenProductionRecallPanel();
                }
            
                return touchscreenProductionRecallPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductionRecallPanelViewModel TouchscreenProductionRecallPanel
        {
            get
            {
                return TouchscreenProductionRecallPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProductionRecallPanel property.
        /// </summary>
        public static void ClearTouchscreenProductionRecallPanel()
        {
            if (touchscreenProductionRecallPanelViewModel != null)
            {
                touchscreenProductionRecallPanelViewModel.Cleanup();
                touchscreenProductionRecallPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProductionRecallPanel property.
        /// </summary>
        public static void CreateTouchscreenProductionRecallPanel()
        {
            if (touchscreenProductionRecallPanelViewModel == null)
            {
                touchscreenProductionRecallPanelViewModel = new TouchscreenProductionRecallPanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenOutOfProductionPanel

        /// <summary>
        /// Gets the TouchscreenOutOfProductionPanel property.
        /// </summary>
        public static TouchscreenOutOfProductionPanelViewModel TouchscreenOutOfProductionPanelStatic
        {
            get
            {
                if (touchscreenOutOfProductionPanelViewModel == null)
                {
                    CreateTouchscreenOutOfProductionPanel();
                }
            
                return touchscreenOutOfProductionPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenOutOfProductionPanelViewModel TouchscreenOutOfProductionPanel
        {
            get
            {
                return TouchscreenOutOfProductionPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenOutOfProductionPanel property.
        /// </summary>
        public static void ClearTouchscreenOutOfProductionPanel()
        {
            if (touchscreenOutOfProductionPanelViewModel != null)
            {
                touchscreenOutOfProductionPanelViewModel.Cleanup();
                touchscreenOutOfProductionPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenOutOfProductionPanel property.
        /// </summary>
        public static void CreateTouchscreenOutOfProductionPanel()
        {
            if (touchscreenOutOfProductionPanelViewModel == null)
            {
                touchscreenOutOfProductionPanelViewModel = new TouchscreenOutOfProductionPanelViewModel();
            }
        }

        #endregion        
        
        #region TouchscreenOutOfProductionWithPiecesPanel

        /// <summary>
        /// Gets the TouchscreenOutOfProductionWithPiecesPanel property.
        /// </summary>
        public static TouchscreenOutOfProductionWithPiecesPanelViewModel TouchscreenOutOfProductionWithPiecesPanelStatic
        {
            get
            {
                if (touchscreenOutOfProductionWithPiecesPanelViewModel == null)
                {
                    CreateTouchscreenOutOfProductionWithPiecesPanel();
                }
                
                return touchscreenOutOfProductionWithPiecesPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenOutOfProductionWithPiecesPanelViewModel TouchscreenOutOfProductionWithPiecesPanel
        {
            get
            {
                return TouchscreenOutOfProductionWithPiecesPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenOutOfProductionWithPiecesPanel property.
        /// </summary>
        public static void ClearTouchscreenOutOfProductionWithPiecesPanel()
        {
            if (touchscreenOutOfProductionWithPiecesPanelViewModel != null)
            {
                touchscreenOutOfProductionWithPiecesPanelViewModel.Cleanup();
                touchscreenOutOfProductionWithPiecesPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenOutOfProductionWithPiecesPanel property.
        /// </summary>
        public static void CreateTouchscreenOutOfProductionWithPiecesPanel()
        {
            if (touchscreenOutOfProductionWithPiecesPanelViewModel == null)
            {
                touchscreenOutOfProductionWithPiecesPanelViewModel = new TouchscreenOutOfProductionWithPiecesPanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenDispatchPanel

        /// <summary>
        /// Gets the TouchscreenDispatchPanel property.
        /// </summary>
        public static TouchscreenDispatchPanelViewModel TouchscreenDispatchPanelStatic
        {
            get
            {
                if (touchscreenDispatchPanelViewModel == null)
                {
                    CreateTouchscreenDispatchPanel();
                }
              
                return touchscreenDispatchPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenDispatchPanelViewModel TouchscreenDispatchPanel
        {
            get
            {
                return TouchscreenDispatchPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenDispatchPanel property.
        /// </summary>
        public static void ClearTouchscreenDispatchPanel()
        {
            if (touchscreenDispatchPanelViewModel != null)
            {
                touchscreenDispatchPanelViewModel.Cleanup();
                touchscreenDispatchPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenDispatchPanel property.
        /// </summary>
        public static void CreateTouchscreenDispatchPanel()
        {
            if (touchscreenDispatchPanelViewModel == null)
            {
                touchscreenDispatchPanelViewModel = new TouchscreenDispatchPanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenDispatchBatchPanel

        /// <summary>
        /// Gets the TouchscreenDispatchBatchPanel property.
        /// </summary>
        public static TouchscreenDispatchBatchPanelViewModel TouchscreenDispatchBatchPanelStatic
        {
            get
            {
                if (touchscreenDispatchBatchPanelViewModel == null)
                {
                    CreateTouchscreenDispatchBatchPanel();
                }

                return touchscreenDispatchBatchPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenDispatchBatchPanelViewModel TouchscreenDispatchBatchPanel
        {
            get
            {
                return TouchscreenDispatchBatchPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenDispatchBatchPanel property.
        /// </summary>
        public static void ClearTouchscreenDispatchBatchPanel()
        {
            if (touchscreenDispatchBatchPanelViewModel != null)
            {
                touchscreenDispatchBatchPanelViewModel.Cleanup();
                touchscreenDispatchBatchPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenDispatchBatchPanel property.
        /// </summary>
        public static void CreateTouchscreenDispatchBatchPanel()
        {
            if (touchscreenDispatchBatchPanelViewModel == null)
            {
                touchscreenDispatchBatchPanelViewModel = new TouchscreenDispatchBatchPanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenDispatchPanelRecordWeight

        /// <summary>
        /// Gets the TouchscreenDispatchPanelRecordWeight property.
        /// </summary>
        public static TouchscreenDispatchPanelRecordWeightViewModel TouchscreenDispatchPanelRecordWeightStatic
        {
            get
            {
                if (touchscreenDispatchPanelRecordWeightViewModel == null)
                {
                    CreateTouchscreenDispatchPanelRecordWeight();
                }
              
                return touchscreenDispatchPanelRecordWeightViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenDispatchPanelRecordWeightViewModel TouchscreenDispatchPanelRecordWeight
        {
            get
            {
                return TouchscreenDispatchPanelRecordWeightStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenDispatchPanelRecordWeight property.
        /// </summary>
        public static void ClearTouchscreenDispatchPanelRecordWeight()
        {
            if (touchscreenDispatchPanelRecordWeightViewModel != null)
            {
                touchscreenDispatchPanelRecordWeightViewModel.Cleanup();
                touchscreenDispatchPanelRecordWeightViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenDispatchPanelRecordWeight property.
        /// </summary>
        public static void CreateTouchscreenDispatchPanelRecordWeight()
        {
            if (touchscreenDispatchPanelRecordWeightViewModel == null)
            {
                touchscreenDispatchPanelRecordWeightViewModel = new TouchscreenDispatchPanelRecordWeightViewModel();
            }
        }

        #endregion        

        #region TouchscreenDispatchPanelRecordWeightWithNotes

        /// <summary>
        /// Gets the TouchscreenDispatchPanelRecordWeightWithNotes property.
        /// </summary>
        public static TouchscreenDispatchPanelRecordWeightWithNotesViewModel TouchscreenDispatchPanelRecordWeightWithNotesStatic
        {
            get
            {
                if (touchscreenDispatchPanelRecordWeightWithNotesViewModel == null)
                {
                    CreateTouchscreenDispatchPanelRecordWeightWithNotes();
                }
           
                return touchscreenDispatchPanelRecordWeightWithNotesViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenDispatchPanelRecordWeightWithNotesViewModel TouchscreenDispatchPanelRecordWeightWithNotes
        {
            get
            {
                return TouchscreenDispatchPanelRecordWeightWithNotesStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenDispatchPanelRecordWeightWithNotes property.
        /// </summary>
        public static void ClearTouchscreenDispatchPanelRecordWeightWithNotes()
        {
            if (touchscreenDispatchPanelRecordWeightWithNotesViewModel != null)
            {
                touchscreenDispatchPanelRecordWeightWithNotesViewModel.Cleanup();
                touchscreenDispatchPanelRecordWeightWithNotesViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenDispatchPanelRecordWeightWithNotes property.
        /// </summary>
        public static void CreateTouchscreenDispatchPanelRecordWeightWithNotes()
        {
            if (touchscreenDispatchPanelRecordWeightWithNotesViewModel == null)
            {
                touchscreenDispatchPanelRecordWeightWithNotesViewModel = new TouchscreenDispatchPanelRecordWeightWithNotesViewModel();
            }
        }

        #endregion        

        #region TouchscreenDispatchPanelRecordWeightWithShipping

        /// <summary>
        /// Gets the TouchscreenDispatchPanelRecordWeightWithShipping property.
        /// </summary>
        public static TouchscreenDispatchPanelRecordWeightWithShippingViewModel TouchscreenDispatchPanelRecordWeightWithShippingStatic
        {
            get
            {
                if (touchscreenDispatchPanelRecordWeightWithShippingViewModel == null)
                {
                    CreateTouchscreenDispatchPanelRecordWeightWithShipping();
                }
        
                return touchscreenDispatchPanelRecordWeightWithShippingViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenDispatchPanelRecordWeightWithShippingViewModel TouchscreenDispatchPanelRecordWeightWithShipping
        {
            get
            {
                return TouchscreenDispatchPanelRecordWeightWithShippingStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenDispatchPanelRecordWeightWithShipping property.
        /// </summary>
        public static void ClearTouchscreenDispatchPanelRecordWeightWithShipping()
        {
            if (touchscreenDispatchPanelRecordWeightWithShippingViewModel != null)
            {
                touchscreenDispatchPanelRecordWeightWithShippingViewModel.Cleanup();
                touchscreenDispatchPanelRecordWeightWithShippingViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenDispatchPanelRecordWeightWithShipping property.
        /// </summary>
        public static void CreateTouchscreenDispatchPanelRecordWeightWithShipping()
        {
            if (touchscreenDispatchPanelRecordWeightWithShippingViewModel == null)
            {
                touchscreenDispatchPanelRecordWeightWithShippingViewModel = new TouchscreenDispatchPanelRecordWeightWithShippingViewModel();
            }
        }

        #endregion        

        #region TouchscreenSuppliers

        /// <summary>
        /// Gets the TouchscreenSuppliers property.
        /// </summary>
        public static TouchscreenSuppliersViewModel TouchscreenSuppliersStatic
        {
            get
            {
                if (touchscreenSuppliersViewModel == null)
                {
                    CreateTouchscreenSuppliers();
                }
             
                return touchscreenSuppliersViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenSuppliersViewModel TouchscreenSuppliers
        {
            get
            {
                return TouchscreenSuppliersStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenSuppliers property.
        /// </summary>
        public static void ClearTouchscreenSuppliers()
        {
            if (touchscreenSuppliersViewModel != null)
            {
                touchscreenSuppliersViewModel.Cleanup();
                touchscreenSuppliersViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenSuppliers property.
        /// </summary>
        public static void CreateTouchscreenSuppliers()
        {
            if (touchscreenSuppliersViewModel == null)
            {
                touchscreenSuppliersViewModel = new TouchscreenSuppliersViewModel();
            }
        }

        #endregion        
        
        #region TouchscreenOrders

        /// <summary>
        /// Gets the TouchscreenOrders property.
        /// </summary>
        public static TouchscreenOrdersViewModel TouchscreenOrdersStatic
        {
            get
            {
                if (touchscreenOrdersViewModel == null)
                {
                    CreateTouchscreenOrders();
                }
             
                return touchscreenOrdersViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenOrdersViewModel TouchscreenOrders
        {
            get
            {
                return TouchscreenOrdersStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenOrders property.
        /// </summary>
        public static void ClearTouchscreenOrders()
        {
            if (touchscreenOrdersViewModel != null)
            {
                touchscreenOrdersViewModel.Cleanup();
                touchscreenOrdersViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenOrders property.
        /// </summary>
        public static void CreateTouchscreenOrders()
        {
            if (touchscreenOrdersViewModel == null)
            {
                touchscreenOrdersViewModel = new TouchscreenOrdersViewModel();
            }
        }

        #endregion        

        #region TouchscreenProducts

        /// <summary>
        /// Gets the TouchscreenProducts property.
        /// </summary>
        public static TouchscreenProductsViewModel TouchscreenProductsStatic
        {
            get
            {
                if (touchscreenProductsViewModel == null)
                {
                    CreateTouchscreenProducts();
                }
            
                return touchscreenProductsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductsViewModel TouchscreenProducts
        {
            get
            {
                return TouchscreenProductsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProducts property.
        /// </summary>
        public static void ClearTouchscreenProducts()
        {
            if (touchscreenProductsViewModel != null)
            {
                touchscreenProductsViewModel.Cleanup();
                touchscreenProductsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProducts property.
        /// </summary>
        public static void CreateTouchscreenProducts()
        {
            if (touchscreenProductsViewModel == null)
            {
                touchscreenProductsViewModel = new TouchscreenProductsViewModel();
            }
        }

        #endregion    
    
        #region TouchscreenProductionPorkPanel

        /// <summary>
        /// Gets the TouchscreenProductionPorkPanel property.
        /// </summary>
        public static TouchscreenProductionPorkPanelViewModel TouchscreenProductionPorkPanelStatic
        {
            get
            {
                if (touchscreenProductionPorkPanelViewModel == null)
                {
                    CreateTouchscreenProductionPorkPanel();
                }


                return touchscreenProductionPorkPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductionPorkPanelViewModel TouchscreenProductionPorkPanel
        {
            get
            {
                return TouchscreenProductionPorkPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProductionPorkPanel property.
        /// </summary>
        public static void ClearTouchscreenProductionPorkPanel()
        {
            if (touchscreenProductionPorkPanelViewModel != null)
            {
                touchscreenProductionPorkPanelViewModel.Cleanup();
                touchscreenProductionPorkPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProductionPorkPanel property.
        /// </summary>
        public static void CreateTouchscreenProductionPorkPanel()
        {
            if (touchscreenProductionPorkPanelViewModel == null)
            {
                touchscreenProductionPorkPanelViewModel = new TouchscreenProductionPorkPanelViewModel();
            }
        }

        #endregion        

        #region TouchscreenProductsMaster

        /// <summary>
        /// Gets the TouchscreenProductsMaster property.
        /// </summary>
        public static TouchscreenProductsMasterViewModel TouchscreenProductsMasterStatic
        {
            get
            {
                if (touchscreenProductsMasterViewModel == null)
                {
                    CreateTouchscreenProductsMaster();
                }
                
                return touchscreenProductsMasterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductsMasterViewModel TouchscreenProductsMaster
        {
            get
            {
                return TouchscreenProductsMasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProductsMaster property.
        /// </summary>
        public static void ClearTouchscreenProductsMaster()
        {
            if (touchscreenProductsMasterViewModel != null)
            {
                touchscreenProductsMasterViewModel.Cleanup();
                touchscreenProductsMasterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProductsMaster property.
        /// </summary>
        public static void CreateTouchscreenProductsMaster()
        {
            if (touchscreenProductsMasterViewModel == null)
            {
                touchscreenProductsMasterViewModel = new TouchscreenProductsMasterViewModel();
            }
        }

        #endregion        

        #region TouchscreenProductsExpandGroups

        /// <summary>
        /// Gets the TouchscreenProductsExpandGroups property.
        /// </summary>
        public static TouchscreenProductsExpandGroupsViewModel TouchscreenProductsExpandGroupsStatic
        {
            get
            {
                if (touchscreenProductsExpandGroupsViewModel == null)
                {
                    CreateTouchscreenProductsExpandGroups();
                }
                
                return touchscreenProductsExpandGroupsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenProductsExpandGroupsViewModel TouchscreenProductsExpandGroups
        {
            get
            {
                return TouchscreenProductsExpandGroupsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenProductsExpandGroups property.
        /// </summary>
        public static void ClearTouchscreenProductsExpandGroups()
        {
            if (touchscreenProductsExpandGroupsViewModel != null)
            {
                touchscreenProductsExpandGroupsViewModel.Cleanup();
                touchscreenProductsExpandGroupsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenProductsExpandGroups property.
        /// </summary>
        public static void CreateTouchscreenProductsExpandGroups()
        {
            if (touchscreenProductsExpandGroupsViewModel == null)
            {
                touchscreenProductsExpandGroupsViewModel = new TouchscreenProductsExpandGroupsViewModel();
            }
        }

        #endregion        

        #region TouchscreenContainer

        /// <summary>
        /// Gets the TouchscreenContainer property.
        /// </summary>
        public static TouchscreenContainerViewModel TouchscreenContainerStatic
        {
            get
            {
                if (touchscreenContainerViewModel == null)
                {
                    CreateTouchscreenContainer();
                }
               
                return touchscreenContainerViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenContainerViewModel TouchscreenContainer
        {
            get
            {
                return TouchscreenContainerStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenContainer property.
        /// </summary>
        public static void ClearTouchscreenContainer()
        {
            if (touchscreenContainerViewModel != null)
            {
                touchscreenContainerViewModel.Cleanup();
                touchscreenContainerViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenContainer property.
        /// </summary>
        public static void CreateTouchscreenContainer()
        {
            if (touchscreenContainerViewModel == null)
            {
                touchscreenContainerViewModel = new TouchscreenContainerViewModel();
            }
        }

        #endregion        

        #region SqlGenerator

        /// <summary>
        /// Gets the SqlGenerator property.
        /// </summary>
        public static SqlGeneratorViewModel SqlGeneratorStatic
        {
            get
            {
                if (sqlGeneratorViewModel == null)
                {
                    CreateSqlGenerator();
                }
                
                return sqlGeneratorViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SqlGeneratorViewModel SqlGenerator
        {
            get
            {
                return SqlGeneratorStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SqlGenerator property.
        /// </summary>
        public static void ClearSqlGenerator()
        {
            if (sqlGeneratorViewModel != null)
            {
                sqlGeneratorViewModel.Cleanup();
                sqlGeneratorViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SqlGenerator property.
        /// </summary>
        public static void CreateSqlGenerator()
        {
            if (sqlGeneratorViewModel == null)
            {
                sqlGeneratorViewModel = new SqlGeneratorViewModel();
            }
        }

        #endregion     

        #region DataGenerator

        /// <summary>
        /// Gets the DataGenerator property.
        /// </summary>
        public static DataGeneratorViewModel DataGeneratorStatic
        {
            get
            {
                if (dataGeneratorViewModel == null)
                {
                    CreateDataGenerator();
                }
               
                return dataGeneratorViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public DataGeneratorViewModel DataGenerator
        {
            get
            {
                return DataGeneratorStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DataGenerator property.
        /// </summary>
        public static void ClearDataGenerator()
        {
            if (dataGeneratorViewModel != null)
            {
                dataGeneratorViewModel.Cleanup();
                dataGeneratorViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DataGenerator property.
        /// </summary>
        public static void CreateDataGenerator()
        {
            if (dataGeneratorViewModel == null)
            {
                dataGeneratorViewModel = new DataGeneratorViewModel();
            }
        }

        #endregion     
    
        #region Audit

        /// <summary>
        /// Gets the audit property.
        /// </summary>
        public static AuditViewModel AuditStatic
        {
            get
            {
                if (auditViewModel == null)
                {
                    CreateAudit();
                }
                
                return auditViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public AuditViewModel Audit
        {
            get
            {
                return AuditStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Audit property.
        /// </summary>
        public static void ClearAudit()
        {
            if (auditViewModel != null)
            {
                auditViewModel.Cleanup();
                auditViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Audit property.
        /// </summary>
        public static void CreateAudit()
        {
            if (auditViewModel == null)
            {
                auditViewModel = new AuditViewModel();
            }
        }

        #endregion        

        #region FactoryScreen

        /// <summary>
        /// Gets the factoryScreen property.
        /// </summary>
        public static FactoryScreenViewModel FactoryScreenStatic
        {
            get
            {
                if (factoryScreenViewModel == null)
                {
                    CreateFactoryScreen();
                }
  
                return factoryScreenViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public FactoryScreenViewModel FactoryScreen
        {
            get
            {
                return FactoryScreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the FactoryScreen property.
        /// </summary>
        public static void ClearFactoryScreen()
        {
            if (factoryScreenViewModel != null)
            {
                factoryScreenViewModel.Cleanup();
                factoryScreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the FactoryScreen property.
        /// </summary>
        public static void CreateFactoryScreen()
        {
            if (factoryScreenViewModel == null)
            {
                factoryScreenViewModel = new FactoryScreenViewModel();
            }
        }

        #endregion        

        #region SystemInformation

        /// <summary>
        /// Gets the systemInformation property.
        /// </summary>
        public static SystemInformationViewModel SystemInformationStatic
        {
            get
            {
                if (systemInformationViewModel == null)
                {
                    CreateSystemInformation();
                }

                return systemInformationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SystemInformationViewModel SystemInformation
        {
            get
            {
                return SystemInformationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SystemInformation property.
        /// </summary>
        public static void ClearSystemInformation()
        {
            if (systemInformationViewModel != null)
            {
                systemInformationViewModel.Cleanup();
                systemInformationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SystemInformation property.
        /// </summary>
        public static void CreateSystemInformation()
        {
            if (systemInformationViewModel == null)
            {
                systemInformationViewModel = new SystemInformationViewModel();
            }
        }

        #endregion        

        #region DocumentType

        /// <summary>
        /// Gets the documentType property.
        /// </summary>
        public static DocumentTypeViewModel DocumentTypeStatic
        {
            get
            {
                if (documentTypeViewModel == null)
                {
                    CreateDocumentType();
                }
            
                return documentTypeViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public DocumentTypeViewModel DocumentType
        {
            get
            {
                return DocumentTypeStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DocumentType property.
        /// </summary>
        public static void ClearDocumentType()
        {
            if (documentTypeViewModel != null)
            {
                documentTypeViewModel.Cleanup();
                documentTypeViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DocumentType property.
        /// </summary>
        public static void CreateDocumentType()
        {
            if (documentTypeViewModel == null)
            {
                documentTypeViewModel = new DocumentTypeViewModel();
            }
        }

        #endregion        

        #region DocumentNumber

        /// <summary>
        /// Gets the documentNumber property.
        /// </summary>
        public static DocumentNumberViewModel DocumentNumberStatic
        {
            get
            {
                if (documentNumberViewModel == null)
                {
                    CreateDocumentNumber();
                }
               
                return documentNumberViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public DocumentNumberViewModel DocumentNumber
        {
            get
            {
                return DocumentNumberStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DocumentNumber property.
        /// </summary>
        public static void ClearDocumentNumber()
        {
            if (documentNumberViewModel != null)
            {
                documentNumberViewModel.Cleanup();
                documentNumberViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DocumentNumber property.
        /// </summary>
        public static void CreateDocumentNumber()
        {
            if (documentNumberViewModel == null)
            {
                documentNumberViewModel = new DocumentNumberViewModel();
            }
        }

        #endregion        
        
        #region QualityName

        /// <summary>
        /// Gets the QualityName property.
        /// </summary>
        public static QualityNameViewModel QualityNameStatic
        {
            get
            {
                if (qualityNameViewModel == null)
                {
                    CreateQualityName();
                }
               
                return qualityNameViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public QualityNameViewModel QualityName
        {
            get
            {
                return QualityNameStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the QualityName property.
        /// </summary>
        public static void ClearQualityName()
        {
            if (qualityNameViewModel != null)
            {
                qualityNameViewModel.Cleanup();
                qualityNameViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the QualityName property.
        /// </summary>
        public static void CreateQualityName()
        {
            if (qualityNameViewModel == null)
            {
                qualityNameViewModel = new QualityNameViewModel();
            }
        }

        #endregion        

        #region Quality

        /// <summary>
        /// Gets the Quality property.
        /// </summary>
        public static QualityViewModel QualityStatic
        {
            get
            {
                if (qualityViewModel == null)
                {
                    CreateQuality();
                }
              
                return qualityViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public QualityViewModel Quality
        {
            get
            {
                return QualityStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Quality property.
        /// </summary>
        public static void ClearQuality()
        {
            if (qualityViewModel != null)
            {
                qualityViewModel.Cleanup();
                qualityViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Quality property.
        /// </summary>
        public static void CreateQuality()
        {
            if (qualityViewModel == null)
            {
                qualityViewModel = new QualityViewModel();
            }
        }

        #endregion        

        #region Menu

        /// <summary>
        /// Gets the Menu property.
        /// </summary>
        public static MenuViewModel MenuStatic
        {
            get
            {
                if (menuViewModel == null)
                {
                    CreateMenu();
                }
            
                return menuViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        public MenuViewModel Menu
        {
            get
            {
                return MenuStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Menu property.
        /// </summary>
        public static void ClearMenu()
        {
            if (menuViewModel != null)
            {
                menuViewModel.Cleanup();
                menuViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Menu property.
        /// </summary>
        public static void CreateMenu()
        {
            if (menuViewModel == null)
            {
                menuViewModel = new MenuViewModel();
            }
        }

        #endregion        

        #region ARDispatch

        /// <summary>
        /// Gets the ARDispatch property.
        /// </summary>
        public static ARDispatchViewModel ARDispatchStatic
        {
            get
            {
                if (arDispatchViewModel == null)
                {
                    CreateARDispatch();
                }
               
                return arDispatchViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        public ARDispatchViewModel ARDispatch
        {
            get
            {
                return ARDispatchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ARDispatch property.
        /// </summary>
        public static void ClearARDispatch()
        {
            if (arDispatchViewModel != null)
            {
                arDispatchViewModel.Cleanup();
                arDispatchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ARDispatch property.
        /// </summary>
        public static void CreateARDispatch()
        {
            if (arDispatchViewModel == null)
            {
                arDispatchViewModel = new ARDispatchViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsARDispatchNull()
        {
            return arDispatchViewModel == null;
        }

        #endregion        

        #region ARDispatch2

        /// <summary>
        /// Gets the ARDispatch2 property.
        /// </summary>
        public static ARDispatch2ViewModel ARDispatch2Static
        {
            get
            {
                if (arDispatch2ViewModel == null)
                {
                    CreateARDispatch2();
                }
                
                return arDispatch2ViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        public ARDispatch2ViewModel ARDispatch2
        {
            get
            {
                return ARDispatch2Static;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ARDispatch2 property.
        /// </summary>
        public static void ClearARDispatch2()
        {
            if (arDispatch2ViewModel != null)
            {
                arDispatch2ViewModel.Cleanup();
                arDispatch2ViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ARDispatch2 property.
        /// </summary>
        public static void CreateARDispatch2()
        {
            if (arDispatch2ViewModel == null)
            {
                arDispatch2ViewModel = new ARDispatch2ViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsARDispatch2Null()
        {
            return arDispatch2ViewModel == null;
        }

        #endregion        

        #region ARDispatchTouchscreen

        /// <summary>
        /// Gets the ARDispatchTouchscreen property.
        /// </summary>
        public static ARDispatchTouchscreenViewModel ARDispatchTouchscreenStatic
        {
            get
            {
                if (arDispatchTouchscreenViewModel == null)
                {
                    CreateARDispatchTouchscreen();
                }
                
                return arDispatchTouchscreenViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        public ARDispatchTouchscreenViewModel ARDispatchTouchscreen
        {
            get
            {
                return ARDispatchTouchscreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ARDispatchTouchscreen property.
        /// </summary>
        public static void ClearARDispatchTouchscreen()
        {
            if (arDispatchTouchscreenViewModel != null)
            {
                arDispatchTouchscreenViewModel.Cleanup();
                arDispatchTouchscreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ARDispatchTouchscreen property.
        /// </summary>
        public static void CreateARDispatchTouchscreen()
        {
            if (arDispatchTouchscreenViewModel == null)
            {
                arDispatchTouchscreenViewModel = new ARDispatchTouchscreenViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsARDispatchTouchscreenNull()
        {
            return arDispatchTouchscreenViewModel == null;
        }

        #endregion        

        #region ARDispatchDetails

        /// <summary>
        /// Gets the ARDispatchDetails property.
        /// </summary>
        public static ARDispatchDetailsViewModel ARDispatchDetailsStatic
        {
            get
            {
                if (arDispatchDetailsViewModel == null)
                {
                    CreateARDispatchDetails();
                }
             
                return arDispatchDetailsViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        public ARDispatchDetailsViewModel ARDispatchDetails
        {
            get
            {
                return ARDispatchDetailsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ARDispatchDetails property.
        /// </summary>
        public static void ClearARDispatchDetails()
        {
            if (arDispatchDetailsViewModel != null)
            {
                arDispatchDetailsViewModel.Cleanup();
                arDispatchDetailsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ARDispatchDetails property.
        /// </summary>
        public static void CreateARDispatchDetails()
        {
            if (arDispatchDetailsViewModel == null)
            {
                arDispatchDetailsViewModel = new ARDispatchDetailsViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsARDispatchDetailsNull()
        {
            return arDispatchDetailsViewModel == null;
        }

        #endregion        

        #region CollectionDisplay

        /// <summary>
        /// Gets the CollectionDisplay property.
        /// </summary>
        public static CollectionDisplayViewModel CollectionDisplayStatic
        {
            get
            {
                if (collectionDisplayViewModel == null)
                {
                    CreateCollectionDisplay();
                }
                
                return collectionDisplayViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public CollectionDisplayViewModel CollectionDisplay
        {
            get
            {
                return CollectionDisplayStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the CollectionDisplay property.
        /// </summary>
        public static void ClearCollectionDisplay()
        {
            if (collectionDisplayViewModel != null)
            {
                collectionDisplayViewModel.Cleanup();
                collectionDisplayViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the CollectionDisplay property.
        /// </summary>
        public static void CreateCollectionDisplay()
        {
            if (collectionDisplayViewModel == null)
            {
                collectionDisplayViewModel = new CollectionDisplayViewModel();
            }
        }

        #endregion        

        #region DateName

        /// <summary>
        /// Gets the DateName property.
        /// </summary>
        public static DateNameViewModel DateNameStatic
        {
            get
            {
                if (dateNameViewModel == null)
                {
                    CreateDateName();
                }
              
                return dateNameViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public DateNameViewModel DateName
        {
            get
            {
                return DateNameStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DateName property.
        /// </summary>
        public static void ClearDateName()
        {
            if (dateNameViewModel != null)
            {
                dateNameViewModel.Cleanup();
                dateNameViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DateName property.
        /// </summary>
        public static void CreateDateName()
        {
            if (dateNameViewModel == null)
            {
                dateNameViewModel = new DateNameViewModel();
            }
        }

        #endregion        

        #region DateTemplateAllocation

        /// <summary>
        /// Gets the DateTemplateAllocation property.
        /// </summary>
        public static DateTemplateAllocationViewModel DateTemplateAllocationStatic
        {
            get
            {
                if (dateTemplateAllocationViewModel == null)
                {
                    CreateDateTemplateAllocation();
                }
             
                return dateTemplateAllocationViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public DateTemplateAllocationViewModel DateTemplateAllocation
        {
            get
            {
                return DateTemplateAllocationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DateTemplateAllocation property.
        /// </summary>
        public static void ClearDateTemplateAllocation()
        {
            if (dateTemplateAllocationViewModel != null)
            {
                dateTemplateAllocationViewModel.Cleanup();
                dateTemplateAllocationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DateTemplateAllocation property.
        /// </summary>
        public static void CreateDateTemplateAllocation()
        {
            if (dateTemplateAllocationViewModel == null)
            {
                dateTemplateAllocationViewModel = new DateTemplateAllocationViewModel();
            }
        }

        #endregion        

        #region QualityTemplateAllocation

        /// <summary>
        /// Gets the QualityTemplateAllocation property.
        /// </summary>
        public static QualityTemplateAllocationViewModel QualityTemplateAllocationStatic
        {
            get
            {
                if (qualityTemplateAllocationViewModel == null)
                {
                    CreateQualityTemplateAllocation();
                }
            
                return qualityTemplateAllocationViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        public QualityTemplateAllocationViewModel QualityTemplateAllocation
        {
            get
            {
                return QualityTemplateAllocationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the QualityTemplateAllocation property.
        /// </summary>
        public static void ClearQualityTemplateAllocation()
        {
            if (qualityTemplateAllocationViewModel != null)
            {
                qualityTemplateAllocationViewModel.Cleanup();
                qualityTemplateAllocationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the QualityTemplateAllocation property.
        /// </summary>
        public static void CreateQualityTemplateAllocation()
        {
            if (qualityTemplateAllocationViewModel == null)
            {
                qualityTemplateAllocationViewModel = new QualityTemplateAllocationViewModel();
            }
        }

        #endregion        

        #region TraceabilityMaster

        /// <summary>
        /// Gets the TraceabilityMaster property.
        /// </summary>
        public static TraceabilityMasterViewModel TraceabilityMasterStatic
        {
            get
            {
                if (traceabilityMasterViewModel == null)
                {
                    CreateTraceabilityMaster();
                }
              
                return traceabilityMasterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public TraceabilityMasterViewModel TraceabilityMaster
        {
            get
            {
                return TraceabilityMasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TraceabilityMaster property.
        /// </summary>
        public static void ClearTraceabilityMaster()
        {
            if (traceabilityMasterViewModel != null)
            {
                traceabilityMasterViewModel.Cleanup();
                traceabilityMasterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TraceabilityMaster property.
        /// </summary>
        public static void CreateTraceabilityMaster()
        {
            if (traceabilityMasterViewModel == null)
            {
                traceabilityMasterViewModel = new TraceabilityMasterViewModel();
            }
        }

        #endregion        

        #region TraceabilityName

        /// <summary>
        /// Gets the TraceabilityName property.
        /// </summary>
        public static TraceabilityNameViewModel TraceabilityNameStatic
        {
            get
            {
                if (traceabilityNameViewModel == null)
                {
                    CreateTraceabilityName();
                }
            
                return traceabilityNameViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public TraceabilityNameViewModel TraceabilityName
        {
            get
            {
                return TraceabilityNameStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TraceabilityName property.
        /// </summary>
        public static void ClearTraceabilityName()
        {
            if (traceabilityNameViewModel != null)
            {
                traceabilityNameViewModel.Cleanup();
                traceabilityNameViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TraceabilityName property.
        /// </summary>
        public static void CreateTraceabilityName()
        {
            if (traceabilityNameViewModel == null)
            {
                traceabilityNameViewModel = new TraceabilityNameViewModel();
            }
        }

        #endregion        
  
        #region TraceabilityTemplateAllocation

        /// <summary>
        /// Gets the TraceabilityTemplateAllocation property.
        /// </summary>
        public static TraceabilityTemplateAllocationViewModel TraceabilityTemplateAllocationStatic
        {
            get
            {
                if (traceabilityTemplateAllocationViewModel == null)
                {
                    CreateTraceabilityTemplateAllocation();
                }

                return traceabilityTemplateAllocationViewModel;
            }
        }

        /// <summary>
        /// Gets the Name property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public TraceabilityTemplateAllocationViewModel TraceabilityTemplateAllocation
        {
            get
            {
                return TraceabilityTemplateAllocationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TraceabilityTemplateAllocation property.
        /// </summary>
        public static void ClearTraceabilityTemplateAllocation()
        {
            if (traceabilityTemplateAllocationViewModel != null)
            {
                traceabilityTemplateAllocationViewModel.Cleanup();
                traceabilityTemplateAllocationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TraceabilityTemplateAllocation property.
        /// </summary>
        public static void CreateTraceabilityTemplateAllocation()
        {
            if (traceabilityTemplateAllocationViewModel == null)
            {
                traceabilityTemplateAllocationViewModel = new TraceabilityTemplateAllocationViewModel();
            }
        }

        #endregion        

        #region CountryMaster

        /// <summary>
        /// Gets the CountryMaster property.
        /// </summary>
        public static CountryMasterViewModel CountryMasterStatic
        {
            get
            {
                if (countryMasterViewModel == null)
                {
                    CreateCountryMaster();
                }
              
                return countryMasterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public CountryMasterViewModel CountryMaster
        {
            get
            {
                return CountryMasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the CountryMaster property.
        /// </summary>
        public static void ClearCountryMaster()
        {
            if (countryMasterViewModel != null)
            {
                countryMasterViewModel.Cleanup();
                countryMasterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the CountryMaster property.
        /// </summary>
        public static void CreateCountryMaster()
        {
            if (countryMasterViewModel == null)
            {
                countryMasterViewModel = new CountryMasterViewModel();
            }
        }

        #endregion        

        #region Container

        /// <summary>
        /// Gets the Container property.
        /// </summary>
        public static ContainerViewModel ContainerStatic
        {
            get
            {
                if (containerViewModel == null)
                {
                    CreateContainer();
                }
              
                return containerViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ContainerViewModel Container
        {
            get
            {
                return ContainerStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Container property.
        /// </summary>
        public static void ClearContainer()
        {
            if (containerViewModel != null)
            {
                containerViewModel.Cleanup();
                containerViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Container property.
        /// </summary>
        public static void CreateContainer()
        {
            if (containerViewModel == null)
            {
                containerViewModel = new ContainerViewModel();
            }
        }

        #endregion        

        #region DocumentTrail

        /// <summary>
        /// Gets the DocumentTrail property.
        /// </summary>
        public static DocumentTrailViewModel DocumentTrailStatic
        {
            get
            {
                if (documentTrailViewModel == null)
                {
                    CreateDocumentTrail();
                }
              
                return documentTrailViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public DocumentTrailViewModel DocumentTrail
        {
            get
            {
                return DocumentTrailStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the DocumentTrail property.
        /// </summary>
        public static void ClearDocumentTrail()
        {
            if (documentTrailViewModel != null)
            {
                documentTrailViewModel.Cleanup();
                documentTrailViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the DocumentTrail property.
        /// </summary>
        public static void CreateDocumentTrail()
        {
            if (documentTrailViewModel == null)
            {
                documentTrailViewModel = new DocumentTrailViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsDocumentTrailNull()
        {
            return documentTrailViewModel == null;
        }

        #endregion        

        #region APQuote

        /// <summary>
        /// Gets the APQuote property.
        /// </summary>
        public static APQuoteViewModel APQuoteStatic
        {
            get
            {
                if (apQuoteViewModel == null)
                {
                    CreateAPQuote();
                }
                
                return apQuoteViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public APQuoteViewModel APQuote
        {
            get
            {
                return APQuoteStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the APQuote property.
        /// </summary>
        public static void ClearAPQuote()
        {
            if (apQuoteViewModel != null)
            {
                apQuoteViewModel.Cleanup();
                apQuoteViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the APQuote property.
        /// </summary>
        public static void CreateAPQuote()
        {
            if (apQuoteViewModel == null)
            {
                apQuoteViewModel = new APQuoteViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAPQuoteNull()
        {
            return apQuoteViewModel == null;
        }

        #endregion

        #region ARReturn

        /// <summary>
        /// Gets the ARReturn property.
        /// </summary>
        public static ARReturnViewModel ARReturnStatic
        {
            get
            {
                if (ARReturnViewModel == null)
                {
                    CreateARReturn();
                }

                return ARReturnViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ARReturnViewModel ARReturn
        {
            get
            {
                return ARReturnStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ARReturn property.
        /// </summary>
        public static void ClearARReturn()
        {
            if (ARReturnViewModel != null)
            {
                ARReturnViewModel.Cleanup();
                ARReturnViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ARReturn property.
        /// </summary>
        public static void CreateARReturn()
        {
            if (ARReturnViewModel == null)
            {
                ARReturnViewModel = new ARReturnViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsARReturnNull()
        {
            return ARReturnViewModel == null;
        }

        #endregion

        #region ARReturnTouchscreen

        /// <summary>
        /// Gets the ARReturnTouchscreen property.
        /// </summary>
        public static ARReturnTouchscreenViewModel ARReturnTouchscreenStatic
        {
            get
            {
                if (ARReturnTouchscreenViewModel == null)
                {
                    CreateARReturnTouchscreen();
                }

                return ARReturnTouchscreenViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ARReturnTouchscreenViewModel ARReturnTouchscreen
        {
            get
            {
                return ARReturnTouchscreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ARReturnTouchscreen property.
        /// </summary>
        public static void ClearARReturnTouchscreen()
        {
            if (ARReturnTouchscreenViewModel != null)
            {
                ARReturnTouchscreenViewModel.Cleanup();
                ARReturnTouchscreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ARReturnTouchscreen property.
        /// </summary>
        public static void CreateARReturnTouchscreen()
        {
            if (ARReturnTouchscreenViewModel == null)
            {
                ARReturnTouchscreenViewModel = new ARReturnTouchscreenViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsARReturnTouchscreenNull()
        {
            return ARReturnTouchscreenViewModel == null;
        }

        #endregion

        #region ARReturnPanel

        /// <summary>
        /// Gets the ARReturnPanel property.
        /// </summary>
        public static ARReturnPanelViewModel ARReturnPanelStatic
        {
            get
            {
                if (ARReturnPanelViewModel == null)
                {
                    CreateARReturnPanel();
                }

                return ARReturnPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ARReturnPanelViewModel ARReturnPanel
        {
            get
            {
                return ARReturnPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ARReturnPanel property.
        /// </summary>
        public static void ClearARReturnPanel()
        {
            if (ARReturnPanelViewModel != null)
            {
                ARReturnPanelViewModel.Cleanup();
                ARReturnPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ARReturnPanel property.
        /// </summary>
        public static void CreateARReturnPanel()
        {
            if (ARReturnPanelViewModel == null)
            {
                ARReturnPanelViewModel = new ARReturnPanelViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsARReturnPanelNull()
        {
            return ARReturnPanelViewModel == null;
        }

        #endregion        

        #region APOrder

        /// <summary>
        /// Gets the APOrder property.
        /// </summary>
        public static APOrderViewModel APOrderStatic
        {
            get
            {
                if (apOrderViewModel == null)
                {
                    CreateAPOrder();
                }
               
                return apOrderViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public APOrderViewModel APOrder
        {
            get
            {
                return APOrderStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the APOrder property.
        /// </summary>
        public static void ClearAPOrder()
        {
            if (apOrderViewModel != null)
            {
                apOrderViewModel.Cleanup();
                apOrderViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the APOrder property.
        /// </summary>
        public static void CreateAPOrder()
        {
            if (apOrderViewModel == null)
            {
                apOrderViewModel = new APOrderViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAPOrderNull()
        {
            return apOrderViewModel == null;
        }

        #endregion        

        #region APReceipt

        /// <summary>
        /// Gets the APReceipt property.
        /// </summary>
        public static APReceiptViewModel APReceiptStatic
        {
            get
            {
                if (apReceiptViewModel == null)
                {
                    CreateAPReceipt();
                }
               
                return apReceiptViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public APReceiptViewModel APReceipt
        {
            get
            {
                return APReceiptStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the APReceipt property.
        /// </summary>
        public static void ClearAPReceipt()
        {
            if (apReceiptViewModel != null)
            {
                apReceiptViewModel.Cleanup();
                apReceiptViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the APReceipt property.
        /// </summary>
        public static void CreateAPReceipt()
        {
            if (apReceiptViewModel == null)
            {
                apReceiptViewModel = new APReceiptViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAPReceiptNull()
        {
            return apReceiptViewModel == null;
        }

        #endregion        
        
        #region ScannerTouchscreen

        /// <summary>
        /// Gets the ScannerTouchscreen property.
        /// </summary>
        public static ScannerTouchscreenViewModel ScannerTouchscreenStatic
        {
            get
            {
                if (scannerTouchscreenViewModel == null)
                {
                    CreateScannerTouchscreen();
                }
             
                return scannerTouchscreenViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ScannerTouchscreenViewModel ScannerTouchscreen
        {
            get
            {
                return ScannerTouchscreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ScannerTouchscreen property.
        /// </summary>
        public static void ClearScannerTouchscreen()
        {
            if (scannerTouchscreenViewModel != null)
            {
                scannerTouchscreenViewModel.Cleanup();
                scannerTouchscreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ScannerTouchscreen property.
        /// </summary>
        public static void CreateScannerTouchscreen()
        {
            if (scannerTouchscreenViewModel == null)
            {
                scannerTouchscreenViewModel = new ScannerTouchscreenViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsScannerTouchscreenNull()
        {
            return scannerTouchscreenViewModel == null;
        }

        #endregion        

        #region ScannerOrders

        /// <summary>
        /// Gets the ScannerOrders property.
        /// </summary>
        public static ScannerOrdersViewModel ScannerOrdersStatic
        {
            get
            {
                if (scannerOrdersViewModel == null)
                {
                    CreateScannerOrders();
                }
                
                return scannerOrdersViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ScannerOrdersViewModel ScannerOrders
        {
            get
            {
                return ScannerOrdersStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ScannerOrders property.
        /// </summary>
        public static void ClearScannerOrders()
        {
            if (scannerOrdersViewModel != null)
            {
                scannerOrdersViewModel.Cleanup();
                scannerOrdersViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ScannerOrders property.
        /// </summary>
        public static void CreateScannerOrders()
        {
            if (scannerOrdersViewModel == null)
            {
                scannerOrdersViewModel = new ScannerOrdersViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsScannerOrdersNull()
        {
            return scannerOrdersViewModel == null;
        }

        #endregion        

        #region ScannerMainDispatch

        /// <summary>
        /// Gets the ScannerMainDispatch property.
        /// </summary>
        public static ScannerMainDispatchViewModel ScannerMainDispatchStatic
        {
            get
            {
                if (scannerMainDispatchViewModel == null)
                {
                    CreateScannerMainDispatch();
                }
               
                return scannerMainDispatchViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ScannerMainDispatchViewModel ScannerMainDispatch
        {
            get
            {
                return ScannerMainDispatchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ScannerMainDispatch property.
        /// </summary>
        public static void ClearScannerMainDispatch()
        {
            if (scannerMainDispatchViewModel != null)
            {
                scannerMainDispatchViewModel.Cleanup();
                scannerMainDispatchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ScannerMainDispatch property.
        /// </summary>
        public static void CreateScannerMainDispatch()
        {
            if (scannerMainDispatchViewModel == null)
            {
                scannerMainDispatchViewModel = new ScannerMainDispatchViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsScannerMainDispatchNull()
        {
            return scannerMainDispatchViewModel == null;
        }

        #endregion        

        #region APReceiptTouchscreen

        /// <summary>
        /// Gets the APReceiptTouchscreen property.
        /// </summary>
        public static APReceiptTouchscreenViewModel APReceiptTouchscreenStatic
        {
            get
            {
                if (apReceiptTouchscreenViewModel == null)
                {
                    CreateAPReceiptTouchscreen();
                }
                
                return apReceiptTouchscreenViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public APReceiptTouchscreenViewModel APReceiptTouchscreen
        {
            get
            {
                return APReceiptTouchscreenStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the APReceiptTouchscreen property.
        /// </summary>
        public static void ClearAPReceiptTouchscreen()
        {
            if (apReceiptTouchscreenViewModel != null)
            {
                apReceiptTouchscreenViewModel.Cleanup();
                apReceiptTouchscreenViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the APReceiptTouchscreen property.
        /// </summary>
        public static void CreateAPReceiptTouchscreen()
        {
            if (apReceiptTouchscreenViewModel == null)
            {
                apReceiptTouchscreenViewModel = new APReceiptTouchscreenViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAPReceiptTouchscreenNull()
        {
            return apReceiptTouchscreenViewModel == null;
        }

        #endregion        

        #region APReceiptDetails

        /// <summary>
        /// Gets the APReceiptDetails property.
        /// </summary>
        public static APReceiptDetailsViewModel APReceiptDetailsStatic
        {
            get
            {
                if (apReceiptDetailsViewModel == null)
                {
                    CreateAPReceiptDetails();
                }
                
                return apReceiptDetailsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public APReceiptDetailsViewModel APReceiptDetails
        {
            get
            {
                return APReceiptDetailsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the APReceiptDetails property.
        /// </summary>
        public static void ClearAPReceiptDetails()
        {
            if (apReceiptDetailsViewModel != null)
            {
                apReceiptDetailsViewModel.Cleanup();
                apReceiptDetailsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the APReceiptDetails property.
        /// </summary>
        public static void CreateAPReceiptDetails()
        {
            if (apReceiptDetailsViewModel == null)
            {
                apReceiptDetailsViewModel = new APReceiptDetailsViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAPReceiptDetailsNull()
        {
            return apReceiptDetailsViewModel == null;
        }

        #endregion        

        #region APReceiptDetailsEdit

        /// <summary>
        /// Gets the APReceiptDetailsEdit property.
        /// </summary>
        public static APReceiptDetailsEditViewModel APReceiptDetailsEditStatic
        {
            get
            {
                if (apReceiptDetailsEditViewModel == null)
                {
                    CreateAPReceiptDetailsEdit();
                }
                
                return apReceiptDetailsEditViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public APReceiptDetailsEditViewModel APReceiptDetailsEdit
        {
            get
            {
                return APReceiptDetailsEditStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the APReceiptDetailsEdit property.
        /// </summary>
        public static void ClearAPReceiptDetailsEdit()
        {
            if (apReceiptDetailsEditViewModel != null)
            {
                apReceiptDetailsEditViewModel.Cleanup();
                apReceiptDetailsEditViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the APReceiptDetailsEdit property.
        /// </summary>
        public static void CreateAPReceiptDetailsEdit()
        {
            if (apReceiptDetailsEditViewModel == null)
            {
                apReceiptDetailsEditViewModel = new APReceiptDetailsEditViewModel();
            }
        }

        #endregion        

        #region Accounts

        /// <summary>
        /// Gets the Accounts property.
        /// </summary>
        public static AccountsViewModel AccountsStatic
        {
            get
            {
                if (accountsViewModel == null)
                {
                    CreateAccounts();
                }
             
                return accountsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public AccountsViewModel Accounts
        {
            get
            {
                return AccountsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Accounts property.
        /// </summary>
        public static void ClearAccounts()
        {
            if (accountsViewModel != null)
            {
                accountsViewModel.Cleanup();
                accountsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Accounts property.
        /// </summary>
        public static void CreateAccounts()
        {
            if (accountsViewModel == null)
            {
                accountsViewModel = new AccountsViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAccountsNull()
        {
            return accountsViewModel == null;
        }

        #endregion        

        #region AccountsPurchases

        /// <summary>
        /// Gets the AccountsPurchases property.
        /// </summary>
        public static AccountsPurchasesViewModel AccountsPurchasesStatic
        {
            get
            {
                if (accountsPurchasesViewModel == null)
                {
                    CreateAccountsPurchases();
                }
      
                return accountsPurchasesViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public AccountsPurchasesViewModel AccountsPurchases
        {
            get
            {
                return AccountsPurchasesStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the AccountsPurchases property.
        /// </summary>
        public static void ClearAccountsPurchases()
        {
            if (accountsPurchasesViewModel != null)
            {
                accountsPurchasesViewModel.Cleanup();
                accountsPurchasesViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the AccountsPurchases property.
        /// </summary>
        public static void CreateAccountsPurchases()
        {
            if (accountsPurchasesViewModel == null)
            {
                accountsPurchasesViewModel = new AccountsPurchasesViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAccountsPurchasesNull()
        {
            return accountsPurchasesViewModel == null;
        }

        #endregion        

        #region TemplateGroup

        /// <summary>
        /// Gets the TemplateGroup property.
        /// </summary>
        public static TemplateGroupViewModel TemplateGroupStatic
        {
            get
            {
                if (templateGroupViewModel == null)
                {
                    CreateTemplateGroup();
                }
          
                return templateGroupViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TemplateGroupViewModel TemplateGroup
        {
            get
            {
                return TemplateGroupStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TemplateGroup property.
        /// </summary>
        public static void ClearTemplateGroup()
        {
            if (templateGroupViewModel != null)
            {
                templateGroupViewModel.Cleanup();
                templateGroupViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TemplateGroup property.
        /// </summary>
        public static void CreateTemplateGroup()
        {
            if (templateGroupViewModel == null)
            {
                templateGroupViewModel = new TemplateGroupViewModel();
            }
        }

        #endregion        

        #region WeightBandGroupSetUp

        /// <summary>
        /// Gets the WeightBandGroupSetUp property.
        /// </summary>
        public static WeightBandGroupSetUpViewModel WeightBandGroupSetUpStatic
        {
            get
            {
                if (weightBandGroupSetUpViewModel == null)
                {
                    CreateWeightBandGroupSetUp();
                }
                
                return weightBandGroupSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WeightBandGroupSetUpViewModel WeightBandGroupSetUp
        {
            get
            {
                return WeightBandGroupSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WeightBandGroupSetUp property.
        /// </summary>
        public static void ClearWeightBandGroupSetUp()
        {
            if (weightBandGroupSetUpViewModel != null)
            {
                weightBandGroupSetUpViewModel.Cleanup();
                weightBandGroupSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WeightBandGroupSetUp property.
        /// </summary>
        public static void CreateWeightBandGroupSetUp()
        {
            if (weightBandGroupSetUpViewModel == null)
            {
                weightBandGroupSetUpViewModel = new WeightBandGroupSetUpViewModel();
            }
        }

        #endregion        

        #region WeightBandSetUp

        /// <summary>
        /// Gets the WeightBandSetUp property.
        /// </summary>
        public static WeightBandSetUpViewModel WeightBandSetUpStatic
        {
            get
            {
                if (weightBandSetUpViewModel == null)
                {
                    CreateWeightBandSetUp();
                }

                return weightBandSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WeightBandSetUpViewModel WeightBandSetUp
        {
            get
            {
                return WeightBandSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WeightBandSetUp property.
        /// </summary>
        public static void ClearWeightBandSetUp()
        {
            if (weightBandSetUpViewModel != null)
            {
                weightBandSetUpViewModel.Cleanup();
                weightBandSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WeightBandSetUp property.
        /// </summary>
        public static void CreateWeightBandSetUp()
        {
            if (weightBandSetUpViewModel == null)
            {
                weightBandSetUpViewModel = new WeightBandSetUpViewModel();
            }
        }

        #endregion        

        #region WeightBandPricing

        /// <summary>
        /// Gets the WeightBandPricing property.
        /// </summary>
        public static WeightBandPricingViewModel WeightBandPricingStatic
        {
            get
            {
                if (weightBandPricingViewModel == null)
                {
                    CreateWeightBandPricing();
                }

                return weightBandPricingViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WeightBandPricingViewModel WeightBandPricing
        {
            get
            {
                return WeightBandPricingStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WeightBandPricing property.
        /// </summary>
        public static void ClearWeightBandPricing()
        {
            if (weightBandPricingViewModel != null)
            {
                weightBandPricingViewModel.Cleanup();
                weightBandPricingViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WeightBandPricing property.
        /// </summary>
        public static void CreateWeightBandPricing()
        {
            if (weightBandPricingViewModel == null)
            {
                weightBandPricingViewModel = new WeightBandPricingViewModel();
            }
        }

        #endregion        

        #region TemplateName

        /// <summary>
        /// Gets the TemplateName property.
        /// </summary>
        public static TemplateNameViewModel TemplateNameStatic
        {
            get
            {
                if (templateNameViewModel == null)
                {
                    CreateTemplateName();
                }
              
                return templateNameViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TemplateNameViewModel TemplateName
        {
            get
            {
                return TemplateNameStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TemplateName property.
        /// </summary>
        public static void ClearTemplateName()
        {
            if (templateNameViewModel != null)
            {
                templateNameViewModel.Cleanup();
                templateNameViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TemplateName property.
        /// </summary>
        public static void CreateTemplateName()
        {
            if (templateNameViewModel == null)
            {
                templateNameViewModel = new TemplateNameViewModel();
            }
        }

        #endregion        

        #region Map

        /// <summary>
        /// Gets the Map property.
        /// </summary>
        public static MapViewModel MapStatic
        {
            get
            {
                if (mapViewModel == null)
                {
                    CreateMap();
                }
          
                return mapViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public MapViewModel Map
        {
            get
            {
                return MapStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Map property.
        /// </summary>
        public static void ClearMap()
        {
            if (mapViewModel != null)
            {
                mapViewModel.Cleanup();
                mapViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Map property.
        /// </summary>
        public static void CreateMap()
        {
            if (mapViewModel == null)
            {
                mapViewModel = new MapViewModel();
            }
        }

        #endregion        

        #region Alibi

        /// <summary>
        /// Gets the Alibi property.
        /// </summary>
        public static AlibiViewModel AlibiStatic
        {
            get
            {
                if (alibiViewModel == null)
                {
                    CreateAlibi();
                }
                
                return alibiViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public AlibiViewModel Alibi
        {
            get
            {
                return AlibiStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Alibi property.
        /// </summary>
        public static void ClearAlibi()
        {
            if (alibiViewModel != null)
            {
                alibiViewModel.Cleanup();
                alibiViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Alibi property.
        /// </summary>
        public static void CreateAlibi()
        {
            if (alibiViewModel == null)
            {
                alibiViewModel = new AlibiViewModel();
            }
        }

        #endregion        

        #region PaymentProposalCreation

        /// <summary>
        /// Gets the PaymentProposalCreation property.
        /// </summary>
        public static PaymentProposalCreationViewModel PaymentProposalCreationStatic
        {
            get
            {
                if (paymentProposalCreationViewModel == null)
                {
                    CreatePaymentProposalCreation();
                }
                
                return paymentProposalCreationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public PaymentProposalCreationViewModel PaymentProposalCreation
        {
            get
            {
                return PaymentProposalCreationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PaymentProposalCreation property.
        /// </summary>
        public static void ClearPaymentProposalCreation()
        {
            if (paymentProposalCreationViewModel != null)
            {
                paymentProposalCreationViewModel.Cleanup();
                paymentProposalCreationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the PaymentProposalCreation property.
        /// </summary>
        public static void CreatePaymentProposalCreation()
        {
            if (paymentProposalCreationViewModel == null)
            {
                paymentProposalCreationViewModel = new PaymentProposalCreationViewModel();
            }
        }

        #endregion        

        #region PaymentProposalSearch

        /// <summary>
        /// Gets the PaymentProposalSearch property.
        /// </summary>
        public static PaymentProposalSearchViewModel PaymentProposalSearchStatic
        {
            get
            {
                if (paymentProposalSearchViewModel == null)
                {
                    CreatePaymentProposalSearch();
                }
              
                return paymentProposalSearchViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public PaymentProposalSearchViewModel PaymentProposalSearch
        {
            get
            {
                return PaymentProposalSearchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PaymentProposalSearch property.
        /// </summary>
        public static void ClearPaymentProposalSearch()
        {
            if (paymentProposalSearchViewModel != null)
            {
                paymentProposalSearchViewModel.Cleanup();
                paymentProposalSearchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the PaymentProposalSearch property.
        /// </summary>
        public static void CreatePaymentProposalSearch()
        {
            if (paymentProposalSearchViewModel == null)
            {
                paymentProposalSearchViewModel = new PaymentProposalSearchViewModel();
            }
        }

        #endregion        

        #region LabelAssociation

        /// <summary>
        /// Gets the LabelAssociation property.
        /// </summary>
        public static LabelAssociationViewModel LabelAssociationStatic
        {
            get
            {
                if (labelAssociationViewModel == null)
                {
                    CreateLabelAssociation();
                }
             
                return labelAssociationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LabelAssociationViewModel LabelAssociation
        {
            get
            {
                return LabelAssociationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LabelAssociation property.
        /// </summary>
        public static void ClearLabelAssociation()
        {
            if (labelAssociationViewModel != null)
            {
                labelAssociationViewModel.Cleanup();
                labelAssociationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LabelAssociation property.
        /// </summary>
        public static void CreateLabelAssociation()
        {
            if (labelAssociationViewModel == null)
            {
                labelAssociationViewModel = new LabelAssociationViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsLabelAssociationNull()
        {
            return labelAssociationViewModel == null;
        }

        #endregion        

        #region ReportViewer

        /// <summary>
        /// Gets the ReportViewer property.
        /// </summary>
        public static ReportViewerViewModel ReportViewerStatic
        {
            get
            {
                if (reportViewerViewModel == null)
                {
                    CreateReportViewer();
                }
              
                return reportViewerViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ReportViewerViewModel ReportViewer
        {
            get
            {
                return ReportViewerStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ReportViewer property.
        /// </summary>
        public static void ClearReportViewer()
        {
            if (reportViewerViewModel != null)
            {
                reportViewerViewModel.Cleanup();
                reportViewerViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ReportViewer property.
        /// </summary>
        public static void CreateReportViewer()
        {
            if (reportViewerViewModel == null)
            {
                reportViewerViewModel = new ReportViewerViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsReportViewerNull()
        {
            return reportViewerViewModel == null;
        }

        #endregion        

        #region BPPropertySetUp

        /// <summary>
        /// Gets the BPPropertySetUp property.
        /// </summary>
        public static BPPropertySetUpViewModel BPPropertySetUpStatic
        {
            get
            {
                if (bPPropertySetUpViewModel == null)
                {
                    CreateBPPropertySetUp();
                }
            
                return bPPropertySetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public BPPropertySetUpViewModel BPPropertySetUp
        {
            get
            {
                return BPPropertySetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BPPropertySetUp property.
        /// </summary>
        public static void ClearBPPropertySetUp()
        {
            if (bPPropertySetUpViewModel != null)
            {
                bPPropertySetUpViewModel.Cleanup();
                bPPropertySetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BPPropertySetUp property.
        /// </summary>
        public static void CreateBPPropertySetUp()
        {
            if (bPPropertySetUpViewModel == null)
            {
                bPPropertySetUpViewModel = new BPPropertySetUpViewModel();
            }
        }

        #endregion        

        #region BPGroupSetUp

        /// <summary>
        /// Gets the BPGroupSetUp Group.
        /// </summary>
        public static BPGroupSetUpViewModel BPGroupSetUpStatic
        {
            get
            {
                if (bPGroupSetUpViewModel == null)
                {
                    CreateBPGroupSetUp();
                }
          
                return bPGroupSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master Group.
        /// </summary>
        public BPGroupSetUpViewModel BPGroupSetUp
        {
            get
            {
                return BPGroupSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the BPGroupSetUp Group.
        /// </summary>
        public static void ClearBPGroupSetUp()
        {
            if (bPGroupSetUpViewModel != null)
            {
                bPGroupSetUpViewModel.Cleanup();
                bPGroupSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the BPGroupSetUp Group.
        /// </summary>
        public static void CreateBPGroupSetUp()
        {
            if (bPGroupSetUpViewModel == null)
            {
                bPGroupSetUpViewModel = new BPGroupSetUpViewModel();
            }
        }

        #endregion        

        #region ItemMasterPropertySetUp

        /// <summary>
        /// Gets the ItemMasterPropertySetUp property.
        /// </summary>
        public static ItemMasterPropertySetUpViewModel ItemMasterPropertySetUpStatic
        {
            get
            {
                if (itemMasterPropertySetUpViewModel == null)
                {
                    CreateItemMasterPropertySetUp();
                }
                
                return itemMasterPropertySetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ItemMasterPropertySetUpViewModel ItemMasterPropertySetUp
        {
            get
            {
                return ItemMasterPropertySetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ItemMasterPropertySetUp property.
        /// </summary>
        public static void ClearItemMasterPropertySetUp()
        {
            if (itemMasterPropertySetUpViewModel != null)
            {
                itemMasterPropertySetUpViewModel.Cleanup();
                itemMasterPropertySetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ItemMasterPropertySetUp property.
        /// </summary>
        public static void CreateItemMasterPropertySetUp()
        {
            if (itemMasterPropertySetUpViewModel == null)
            {
                itemMasterPropertySetUpViewModel = new ItemMasterPropertySetUpViewModel();
            }
        }

        #endregion        

        #region PrinterSelection

        /// <summary>
        /// Gets the PrinterSelection property.
        /// </summary>
        public static PrinterSelectionViewModel PrinterSelectionStatic
        {
            get
            {
                if (printerSelectionViewModel == null)
                {
                    CreatePrinterSelection();
                }

                return printerSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public PrinterSelectionViewModel PrinterSelection
        {
            get
            {
                return PrinterSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PrinterSelection property.
        /// </summary>
        public static void ClearPrinterSelection()
        {
            if (printerSelectionViewModel != null)
            {
                printerSelectionViewModel.Cleanup();
                printerSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the PrinterSelection property.
        /// </summary>
        public static void CreatePrinterSelection()
        {
            if (printerSelectionViewModel == null)
            {
                printerSelectionViewModel = new PrinterSelectionViewModel();
            }
        }

        #endregion        

        #region PricingMaster

        /// <summary>
        /// Gets the PricingMaster property.
        /// </summary>
        public static PricingMasterViewModel PricingMasterStatic
        {
            get
            {
                if (pricingMasterViewModel == null)
                {
                    CreatePricingMaster();
                }
              
                return pricingMasterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public PricingMasterViewModel PricingMaster
        {
            get
            {
                return PricingMasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PricingMaster property.
        /// </summary>
        public static void ClearPricingMaster()
        {
            if (pricingMasterViewModel != null)
            {
                pricingMasterViewModel.Cleanup();
                pricingMasterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the PricingMaster property.
        /// </summary>
        public static void CreatePricingMaster()
        {
            if (pricingMasterViewModel == null)
            {
                pricingMasterViewModel = new PricingMasterViewModel();
            }
        }

        #endregion        

        #region PriceListDetail

        /// <summary>
        /// Gets the PriceListDetail property.
        /// </summary>
        public static PriceListDetailViewModel PriceListDetailStatic
        {
            get
            {
                if (priceListDetailViewModel == null)
                {
                    CreatePriceListDetail();
                }
               
                return priceListDetailViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public PriceListDetailViewModel PriceListDetail
        {
            get
            {
                return PriceListDetailStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the PriceListDetail property.
        /// </summary>
        public static void ClearPriceListDetail()
        {
            if (priceListDetailViewModel != null)
            {
                priceListDetailViewModel.Cleanup();
                priceListDetailViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the PriceListDetail property.
        /// </summary>
        public static void CreatePriceListDetail()
        {
            if (priceListDetailViewModel == null)
            {
                priceListDetailViewModel = new PriceListDetailViewModel();
            }
        }

        #endregion        

        #region ProductionBatchSetUp

        /// <summary>
        /// Gets the ProductionBatchSetUp property.
        /// </summary>
        public static ProductionBatchSetUpViewModel ProductionBatchSetUpStatic
        {
            get
            {
                if (productionBatchSetUpViewModel == null)
                {
                    CreateProductionBatchSetUp();
                }
       
                return productionBatchSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ProductionBatchSetUpViewModel ProductionBatchSetUp
        {
            get
            {
                return ProductionBatchSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ProductionBatchSetUp property.
        /// </summary>
        public static void ClearProductionBatchSetUp()
        {
            if (productionBatchSetUpViewModel != null)
            {
                productionBatchSetUpViewModel.Cleanup();
                productionBatchSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ProductionBatchSetUp property.
        /// </summary>
        public static void CreateProductionBatchSetUp()
        {
            if (productionBatchSetUpViewModel == null)
            {
                productionBatchSetUpViewModel = new ProductionBatchSetUpViewModel();
            }
        }

        #endregion        

        #region Plant

        /// <summary>
        /// Gets the Plant property.
        /// </summary>
        public static PlantViewModel PlantStatic
        {
            get
            {
                if (plantViewModel == null)
                {
                    CreatePlant();
                }
       
                return plantViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public PlantViewModel Plant
        {
            get
            {
                return PlantStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Plant property.
        /// </summary>
        public static void ClearPlant()
        {
            if (plantViewModel != null)
            {
                plantViewModel.Cleanup();
                plantViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Plant property.
        /// </summary>
        public static void CreatePlant()
        {
            if (plantViewModel == null)
            {
                plantViewModel = new PlantViewModel();
            }
        }

        #endregion        

        #region Currency

        /// <summary>
        /// Gets the Currency property.
        /// </summary>
        public static CurrencyViewModel CurrencyStatic
        {
            get
            {
                if (currencyViewModel == null)
                {
                    CreateCurrency();
                }
                
                return currencyViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public CurrencyViewModel Currency
        {
            get
            {
                return CurrencyStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Currency property.
        /// </summary>
        public static void ClearCurrency()
        {
            if (currencyViewModel != null)
            {
                currencyViewModel.Cleanup();
                currencyViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Currency property.
        /// </summary>
        public static void CreateCurrency()
        {
            if (currencyViewModel == null)
            {
                currencyViewModel = new CurrencyViewModel();
            }
        }

        #endregion        

        #region Region

        /// <summary>
        /// Gets the Region property.
        /// </summary>
        public static RegionViewModel RegionStatic
        {
            get
            {
                if (regionViewModel == null)
                {
                    CreateRegion();
                }
                
                return regionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public RegionViewModel Region
        {
            get
            {
                return RegionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Region property.
        /// </summary>
        public static void ClearRegion()
        {
            if (regionViewModel != null)
            {
                regionViewModel.Cleanup();
                regionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Region property.
        /// </summary>
        public static void CreateRegion()
        {
            if (regionViewModel == null)
            {
                regionViewModel = new RegionViewModel();
            }
        }

        #endregion        

        #region Route

        /// <summary>
        /// Gets the Route property.
        /// </summary>
        public static RouteViewModel RouteStatic
        {
            get
            {
                if (routeViewModel == null)
                {
                    CreateRoute();
                }

                return routeViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public RouteViewModel Route
        {
            get
            {
                return RouteStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Route property.
        /// </summary>
        public static void ClearRoute()
        {
            if (routeViewModel != null)
            {
                routeViewModel.Cleanup();
                routeViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Route property.
        /// </summary>
        public static void CreateRoute()
        {
            if (routeViewModel == null)
            {
                routeViewModel = new RouteViewModel();
            }
        }

        #endregion        

        #region Quote

        /// <summary>
        /// Gets the Quote property.
        /// </summary>
        public static QuoteViewModel QuoteStatic
        {
            get
            {
                if (quoteViewModel == null)
                {
                    CreateQuote();
                }
          
                return quoteViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public QuoteViewModel Quote
        {
            get
            {
                return QuoteStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Quote property.
        /// </summary>
        public static void ClearQuote()
        {
            if (quoteViewModel != null)
            {
                quoteViewModel.Cleanup();
                quoteViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Quote property.
        /// </summary>
        public static void CreateQuote()
        {
            if (quoteViewModel == null)
            {
                quoteViewModel = new QuoteViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsQuoteNull()
        {
            return quoteViewModel == null;
        }

        #endregion        

        #region Order

        /// <summary>
        /// Gets the Order property.
        /// </summary>
        public static OrderViewModel OrderStatic
        {
            get
            {
                if (orderViewModel == null)
                {
                    CreateOrder();
                }
          
                return orderViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public OrderViewModel Order
        {
            get
            {
                return OrderStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Order property.
        /// </summary>
        public static void ClearOrder()
        {
            if (orderViewModel != null)
            {
                orderViewModel.Cleanup();
                orderViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Order property.
        /// </summary>
        public static void CreateOrder()
        {
            if (orderViewModel == null)
            {
                orderViewModel = new OrderViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsOrderNull()
        {
            return orderViewModel == null;
        }

        #endregion        

        #region Order2

        /// <summary>
        /// Gets the Order2 property.
        /// </summary>
        public static Order2ViewModel Order2Static
        {
            get
            {
                if (order2ViewModel == null)
                {
                    CreateOrder2();
                }
                
                return order2ViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public Order2ViewModel Order2
        {
            get
            {
                return Order2Static;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Order2 property.
        /// </summary>
        public static void ClearOrder2()
        {
            if (order2ViewModel != null)
            {
                order2ViewModel.Cleanup();
                order2ViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Order2 property.
        /// </summary>
        public static void CreateOrder2()
        {
            if (order2ViewModel == null)
            {
                order2ViewModel = new Order2ViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsOrder2Null()
        {
            return order2ViewModel == null;
        }

        #endregion

        #region APInvoice

        /// <summary>
        /// Gets the APInvoice property.
        /// </summary>
        public static APInvoiceViewModel APInvoiceStatic
        {
            get
            {
                if (apInvoiceViewModel == null)
                {
                    CreateAPInvoice();
                }

                return apInvoiceViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public APInvoiceViewModel APInvoice
        {
            get
            {
                return APInvoiceStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the APInvoice property.
        /// </summary>
        public static void ClearAPInvoice()
        {
            if (apInvoiceViewModel != null)
            {
                apInvoiceViewModel.Cleanup();
                apInvoiceViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the APInvoice property.
        /// </summary>
        public static void CreateAPInvoice()
        {
            if (apInvoiceViewModel == null)
            {
                apInvoiceViewModel = new APInvoiceViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAPInvoiceNull()
        {
            return apInvoiceViewModel == null;
        }

        #endregion        

        #region Invoice

        /// <summary>
        /// Gets the Invoice property.
        /// </summary>
        public static InvoiceViewModel InvoiceStatic
        {
            get
            {
                if (invoiceViewModel == null)
                {
                    CreateInvoice();
                }
               
                return invoiceViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public InvoiceViewModel Invoice
        {
            get
            {
                return InvoiceStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Invoice property.
        /// </summary>
        public static void ClearInvoice()
        {
            if (invoiceViewModel != null)
            {
                invoiceViewModel.Cleanup();
                invoiceViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Invoice property.
        /// </summary>
        public static void CreateInvoice()
        {
            if (invoiceViewModel == null)
            {
                invoiceViewModel = new InvoiceViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsInvoiceNull()
        {
            return invoiceViewModel == null;
        }

        #endregion

        #region APInvoiceCreation

        /// <summary>
        /// Gets the APInvoiceCreation property.
        /// </summary>
        public static APInvoiceCreationViewModel APInvoiceCreationStatic
        {
            get
            {
                if (apInvoiceCreationViewModel == null)
                {
                    CreateAPInvoiceCreation();
                }

                return apInvoiceCreationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public APInvoiceCreationViewModel APInvoiceCreation
        {
            get
            {
                return APInvoiceCreationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the APInvoiceCreation property.
        /// </summary>
        public static void ClearAPInvoiceCreation()
        {
            if (apInvoiceCreationViewModel != null)
            {
                apInvoiceCreationViewModel.Cleanup();
                apInvoiceCreationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the APInvoiceCreation property.
        /// </summary>
        public static void CreateAPInvoiceCreation()
        {
            if (apInvoiceCreationViewModel == null)
            {
                apInvoiceCreationViewModel = new APInvoiceCreationViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsAPInvoiceCreationNull()
        {
            return apInvoiceCreationViewModel == null;
        }

        #endregion        

        #region InvoiceCreation

        /// <summary>
        /// Gets the InvoiceCreation property.
        /// </summary>
        public static InvoiceCreationViewModel InvoiceCreationStatic
        {
            get
            {
                if (invoiceCreationViewModel == null)
                {
                    CreateInvoiceCreation();
                }
                
                return invoiceCreationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public InvoiceCreationViewModel InvoiceCreation
        {
            get
            {
                return InvoiceCreationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the InvoiceCreation property.
        /// </summary>
        public static void ClearInvoiceCreation()
        {
            if (invoiceCreationViewModel != null)
            {
                invoiceCreationViewModel.Cleanup();
                invoiceCreationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the InvoiceCreation property.
        /// </summary>
        public static void CreateInvoiceCreation()
        {
            if (invoiceCreationViewModel == null)
            {
                invoiceCreationViewModel = new InvoiceCreationViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsInvoiceCreationNull()
        {
            return invoiceCreationViewModel == null;
        }

        #endregion

        #region CreditNoteCreation

        /// <summary>
        /// Gets the CreditNoteCreation property.
        /// </summary>
        public static CreditNoteCreationViewModel CreditNoteCreationStatic
        {
            get
            {
                if (creditNoteCreationViewModel == null)
                {
                    CreateCreditNoteCreation();
                }

                return creditNoteCreationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public CreditNoteCreationViewModel CreditNoteCreation
        {
            get
            {
                return CreditNoteCreationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the CreditNoteCreation property.
        /// </summary>
        public static void ClearCreditNoteCreation()
        {
            if (creditNoteCreationViewModel != null)
            {
                creditNoteCreationViewModel.Cleanup();
                creditNoteCreationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the CreditNoteCreation property.
        /// </summary>
        public static void CreateCreditNoteCreation()
        {
            if (creditNoteCreationViewModel == null)
            {
                creditNoteCreationViewModel = new CreditNoteCreationViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsCreditNoteCreationNull()
        {
            return creditNoteCreationViewModel == null;
        }

        #endregion        

        #region LairageOrder

        /// <summary>
        /// Gets the LairageOrder property.
        /// </summary>
        public static LairageOrderViewModel LairageOrderStatic
        {
            get
            {
                if (lairageOrderViewModel == null)
                {
                    CreateLairageOrder();
                }
                
                return lairageOrderViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LairageOrderViewModel LairageOrder
        {
            get
            {
                return LairageOrderStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LairageOrder property.
        /// </summary>
        public static void ClearLairageOrder()
        {
            if (lairageOrderViewModel != null)
            {
                lairageOrderViewModel.Cleanup();
                lairageOrderViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LairageOrder property.
        /// </summary>
        public static void CreateLairageOrder()
        {
            if (lairageOrderViewModel == null)
            {
                lairageOrderViewModel = new LairageOrderViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsLairageOrderNull()
        {
            return lairageOrderViewModel == null;
        }

        #endregion        

        #region LairageAnimalsSearch

        /// <summary>
        /// Gets the LairageAnimalsSearch property.
        /// </summary>
        public static LairageAnimalsSearchViewModel LairageAnimalsSearchStatic
        {
            get
            {
                if (lairageAnimalsSearchViewModel == null)
                {
                    CreateLairageAnimalsSearch();
                }
        
                return lairageAnimalsSearchViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LairageAnimalsSearchViewModel LairageAnimalsSearch
        {
            get
            {
                return LairageAnimalsSearchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LairageAnimalsSearch property.
        /// </summary>
        public static void ClearLairageAnimalsSearch()
        {
            if (lairageAnimalsSearchViewModel != null)
            {
                lairageAnimalsSearchViewModel.Cleanup();
                lairageAnimalsSearchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LairageAnimalsSearch property.
        /// </summary>
        public static void CreateLairageAnimalsSearch()
        {
            if (lairageAnimalsSearchViewModel == null)
            {
                lairageAnimalsSearchViewModel = new LairageAnimalsSearchViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsLairageAnimalsSearchNull()
        {
            return lairageAnimalsSearchViewModel == null;
        }

        #endregion        

        #region Sequencer

        /// <summary>
        /// Gets the Sequencer property.
        /// </summary>
        public static SequencerViewModel SequencerStatic
        {
            get
            {
                if (sequencerViewModel == null)
                {
                    CreateSequencer();
                }

                return sequencerViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SequencerViewModel Sequencer
        {
            get
            {
                return SequencerStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Sequencer property.
        /// </summary>
        public static void ClearSequencer()
        {
            if (sequencerViewModel != null)
            {
                sequencerViewModel.Cleanup();
                sequencerViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Sequencer property.
        /// </summary>
        public static void CreateSequencer()
        {
            if (sequencerViewModel == null)
            {
                sequencerViewModel = new SequencerViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsSequencerNull()
        {
            return sequencerViewModel == null;
        }

        #endregion   
     
        #region SequencerSheepPanel

        /// <summary>
        /// Gets the SequencerSheepPanel property.
        /// </summary>
        public static SequencerSheepPanelViewModel SequencerSheepPanelStatic
        {
            get
            {
                if (sequencerSheepPanelViewModel == null)
                {
                    CreateSequencerSheepPanel();
                }
                
                return sequencerSheepPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SequencerSheepPanelViewModel SequencerSheepPanel
        {
            get
            {
                return SequencerSheepPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SequencerSheepPanel property.
        /// </summary>
        public static void ClearSequencerSheepPanel()
        {
            if (sequencerSheepPanelViewModel != null)
            {
                sequencerSheepPanelViewModel.Cleanup();
                sequencerSheepPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SequencerSheepPanel property.
        /// </summary>
        public static void CreateSequencerSheepPanel()
        {
            if (sequencerSheepPanelViewModel == null)
            {
                sequencerSheepPanelViewModel = new SequencerSheepPanelViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsSequencerSheepPanelNull()
        {
            return sequencerSheepPanelViewModel == null;
        }

        #endregion        

        #region SequencerPanel

        /// <summary>
        /// Gets the SequencerPanel property.
        /// </summary>
        public static SequencerPanelViewModel SequencerPanelStatic
        {
            get
            {
                if (sequencerPanelViewModel == null)
                {
                    CreateSequencerPanel();
                }
           
                return sequencerPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SequencerPanelViewModel SequencerPanel
        {
            get
            {
                return SequencerPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SequencerPanel property.
        /// </summary>
        public static void ClearSequencerPanel()
        {
            if (sequencerPanelViewModel != null)
            {
                sequencerPanelViewModel.Cleanup();
                sequencerPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SequencerPanel property.
        /// </summary>
        public static void CreateSequencerPanel()
        {
            if (sequencerPanelViewModel == null)
            {
                sequencerPanelViewModel = new SequencerPanelViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsSequencerPanelNull()
        {
            return sequencerPanelViewModel == null;
        }

        #endregion

        #region LairagePanel

        /// <summary>
        /// Gets the LairagePanel property.
        /// </summary>
        public static LairagePanelViewModel LairagePanelStatic
        {
            get
            {
                if (lairagePanelViewModel == null)
                {
                    CreateLairagePanel();
                }
              
                return lairagePanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public LairagePanelViewModel LairagePanel
        {
            get
            {
                return LairagePanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the LairagePanel property.
        /// </summary>
        public static void ClearLairagePanel()
        {
            if (lairagePanelViewModel != null)
            {
                lairagePanelViewModel.Cleanup();
                lairagePanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the LairagePanel property.
        /// </summary>
        public static void CreateLairagePanel()
        {
            if (lairagePanelViewModel == null)
            {
                lairagePanelViewModel = new LairagePanelViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsLairagePanelNull()
        {
            return lairagePanelViewModel == null;
        }

        #endregion        

        #region Grade

        /// <summary>
        /// Gets the Grade property.
        /// </summary>
        public static GradeViewModel GradeStatic
        {
            get
            {
                if (gradeViewModel == null)
                {
                    CreateGrade();
                }
        
                return gradeViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public GradeViewModel Grade
        {
            get
            {
                return GradeStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Grade property.
        /// </summary>
        public static void ClearGrade()
        {
            if (gradeViewModel != null)
            {
                gradeViewModel.Cleanup();
                gradeViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Grade property.
        /// </summary>
        public static void CreateGrade()
        {
            if (gradeViewModel == null)
            {
                gradeViewModel = new GradeViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsGradeNull()
        {
            return gradeViewModel == null;
        }

        #endregion

        #region GradeFivePoint

        /// <summary>
        /// Gets the GradeFivePoint property.
        /// </summary>
        public static GradeFivePointViewModel GradeFivePointStatic
        {
            get
            {
                if (gradeFivePointViewModel == null)
                {
                    CreateGradeFivePoint();
                }

                return gradeFivePointViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public GradeFivePointViewModel GradeFivePoint
        {
            get
            {
                return GradeFivePointStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the GradeFivePoint property.
        /// </summary>
        public static void ClearGradeFivePoint()
        {
            if (gradeFivePointViewModel != null)
            {
                gradeFivePointViewModel.Cleanup();
                gradeFivePointViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the GradeFivePoint property.
        /// </summary>
        public static void CreateGradeFivePoint()
        {
            if (gradeFivePointViewModel == null)
            {
                gradeFivePointViewModel = new GradeFivePointViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsGradeFivePointNull()
        {
            return gradeFivePointViewModel == null;
        }

        #endregion        

        #region GradeHistoric

        /// <summary>
        /// Gets the GradeHistoric property.
        /// </summary>
        public static GradeHistoricViewModel GradeHistoricStatic
        {
            get
            {
                if (gradeHistoricViewModel == null)
                {
                    CreateGradeHistoric();
                }
         
                return gradeHistoricViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public GradeHistoricViewModel GradeHistoric
        {
            get
            {
                return GradeHistoricStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the GradeHistoric property.
        /// </summary>
        public static void ClearGradeHistoric()
        {
            if (gradeHistoricViewModel != null)
            {
                gradeHistoricViewModel.Cleanup();
                gradeHistoricViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the GradeHistoric property.
        /// </summary>
        public static void CreateGradeHistoric()
        {
            if (gradeHistoricViewModel == null)
            {
                gradeHistoricViewModel = new GradeHistoricViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsGradeHistoricNull()
        {
            return gradeHistoricViewModel == null;
        }

        #endregion        

        #region Pig

        /// <summary>
        /// Gets the Pig property.
        /// </summary>
        public static PigViewModel PigStatic
        {
            get
            {
                if (pigViewModel == null)
                {
                    CreatePig();
                }
             
                return pigViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public PigViewModel Pig
        {
            get
            {
                return PigStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Pig property.
        /// </summary>
        public static void ClearPig()
        {
            if (pigViewModel != null)
            {
                pigViewModel.Cleanup();
                pigViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Pig property.
        /// </summary>
        public static void CreatePig()
        {
            if (pigViewModel == null)
            {
                pigViewModel = new PigViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsPigNull()
        {
            return pigViewModel == null;
        }

        #endregion        

        #region Grader

        /// <summary>
        /// Gets the Grader property.
        /// </summary>
        public static GraderViewModel GraderStatic
        {
            get
            {
                if (graderViewModel == null)
                {
                    CreateGrader();
                }
            
                return graderViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public GraderViewModel Grader
        {
            get
            {
                return GraderStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Grader property.
        /// </summary>
        public static void ClearGrader()
        {
            if (graderViewModel != null)
            {
                graderViewModel.Cleanup();
                graderViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Grader property.
        /// </summary>
        public static void CreateGrader()
        {
            if (graderViewModel == null)
            {
                graderViewModel = new GraderViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsGraderNull()
        {
            return graderViewModel == null;
        }

        #endregion        

        #region GraderPanel

        /// <summary>
        /// Gets the GraderPanel property.
        /// </summary>
        public static GraderPanelViewModel GraderPanelStatic
        {
            get
            {
                if (graderPanelViewModel == null)
                {
                    CreateGraderPanel();
                }
               
                return graderPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public GraderPanelViewModel GraderPanel
        {
            get
            {
                return GraderPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the GraderPanel property.
        /// </summary>
        public static void ClearGraderPanel()
        {
            if (graderPanelViewModel != null)
            {
                graderPanelViewModel.Cleanup();
                graderPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the GraderPanel property.
        /// </summary>
        public static void CreateGraderPanel()
        {
            if (graderPanelViewModel == null)
            {
                graderPanelViewModel = new GraderPanelViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsGraderPanelNull()
        {
            return graderPanelViewModel == null;
        }

        #endregion        

        #region GraderPigPanel

        /// <summary>
        /// Gets the GraderPigPanel property.
        /// </summary>
        public static GraderPigPanelViewModel GraderPigPanelStatic
        {
            get
            {
                if (graderPigPanelViewModel == null)
                {
                    CreateGraderPigPanel();
                }
          
                return graderPigPanelViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public GraderPigPanelViewModel GraderPigPanel
        {
            get
            {
                return GraderPigPanelStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the GraderPigPanel property.
        /// </summary>
        public static void ClearGraderPigPanel()
        {
            if (graderPigPanelViewModel != null)
            {
                graderPigPanelViewModel.Cleanup();
                graderPigPanelViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the GraderPigPanel property.
        /// </summary>
        public static void CreateGraderPigPanel()
        {
            if (graderPigPanelViewModel == null)
            {
                graderPigPanelViewModel = new GraderPigPanelViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsGraderPigPanelNull()
        {
            return graderPigPanelViewModel == null;
        }

        #endregion        

        #region SequencerMainData

        /// <summary>
        /// Gets the SequencerMainData property.
        /// </summary>
        public static SequencerMainDataViewModel SequencerMainDataStatic
        {
            get
            {
                if (sequencerMainDataViewModel == null)
                {
                    CreateSequencerMainData();
                }
           
                return sequencerMainDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public SequencerMainDataViewModel SequencerMainData
        {
            get
            {
                return SequencerMainDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SequencerMainData property.
        /// </summary>
        public static void ClearSequencerMainData()
        {
            if (sequencerMainDataViewModel != null)
            {
                sequencerMainDataViewModel.Cleanup();
                sequencerMainDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SequencerMainData property.
        /// </summary>
        public static void CreateSequencerMainData()
        {
            if (sequencerMainDataViewModel == null)
            {
                sequencerMainDataViewModel = new SequencerMainDataViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsSequencerMainDataNull()
        {
            return sequencerMainDataViewModel == null;
        }

        #endregion        

        #region TelesalesSearch

        /// <summary>
        /// Gets the TelesalesSearch property.
        /// </summary>
        public static TelesalesSearchViewModel TelesalesSearchStatic
        {
            get
            {
                if (telesalesSearchViewModel == null)
                {
                    CreateTelesalesSearch();
                }
   
                return telesalesSearchViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TelesalesSearchViewModel TelesalesSearch
        {
            get
            {
                return TelesalesSearchStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TelesalesSearch property.
        /// </summary>
        public static void ClearTelesalesSearch()
        {
            if (telesalesSearchViewModel != null)
            {
                telesalesSearchViewModel.Cleanup();
                telesalesSearchViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TelesalesSearch property.
        /// </summary>
        public static void CreateTelesalesSearch()
        {
            if (telesalesSearchViewModel == null)
            {
                telesalesSearchViewModel = new TelesalesSearchViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsTelesalesSearchNull()
        {
            return telesalesSearchViewModel == null;
        }

        #endregion        

        #region Indicator

        /// <summary>
        /// Gets the Indicator property.
        /// </summary>
        public static IndicatorViewModel IndicatorStatic
        {
            get
            {
                if (indicatorViewModel == null)
                {
                    CreateIndicator();
                }
    
                return indicatorViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public IndicatorViewModel Indicator
        {
            get
            {
                return IndicatorStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Indicator property.
        /// </summary>
        public static void ClearIndicator()
        {
            if (indicatorViewModel != null)
            {
                indicatorViewModel.Cleanup();
                indicatorViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Indicator property.
        /// </summary>
        public static void CreateIndicator()
        {
            if (indicatorViewModel == null)
            {
                indicatorViewModel = new IndicatorViewModel();
            }
        }

        #endregion        

        #region Warehouse

        /// <summary>
        /// Gets the Warehouse property.
        /// </summary>
        public static WarehouseViewModel WarehouseStatic
        {
            get
            {
                if (warehouseViewModel == null)
                {
                    CreateWarehouse();
                }
                
                return warehouseViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WarehouseViewModel Warehouse
        {
            get
            {
                return WarehouseStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Warehouse property.
        /// </summary>
        public static void ClearWarehouse()
        {
            if (warehouseViewModel != null)
            {
                warehouseViewModel.Cleanup();
                warehouseViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Warehouse property.
        /// </summary>
        public static void CreateWarehouse()
        {
            if (warehouseViewModel == null)
            {
                warehouseViewModel = new WarehouseViewModel();
            }
        }

        #endregion        

        #region WarehouseLocation

        /// <summary>
        /// Gets the WarehouseLocation property.
        /// </summary>
        public static WarehouseLocationViewModel WarehouseLocationStatic
        {
            get
            {
                if (warehouseLocationViewModel == null)
                {
                    CreateWarehouseLocation();
                }
                
                return warehouseLocationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public WarehouseLocationViewModel WarehouseLocation
        {
            get
            {
                return WarehouseLocationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the WarehouseLocation property.
        /// </summary>
        public static void ClearWarehouseLocation()
        {
            if (warehouseLocationViewModel != null)
            {
                warehouseLocationViewModel.Cleanup();
                warehouseLocationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the WarehouseLocation property.
        /// </summary>
        public static void CreateWarehouseLocation()
        {
            if (warehouseLocationViewModel == null)
            {
                warehouseLocationViewModel = new WarehouseLocationViewModel();
            }
        }

        #endregion        

        #region SalesSearchData

        /// <summary>
        /// Gets the SalesSearchData property.
        /// </summary>
        public static SalesSearchDataViewModel SalesSearchDataStatic
        {
            get
            {
                if (salesSearchDataViewModel == null)
                {
                    CreateSalesSearchData();
                }
                
                return salesSearchDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public SalesSearchDataViewModel SalesSearchData
        {
            get
            {
                return SalesSearchDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the SalesSearchData property.
        /// </summary>
        public static void ClearSalesSearchData()
        {
            if (salesSearchDataViewModel != null)
            {
                salesSearchDataViewModel.Cleanup();
                salesSearchDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the SalesSearchData property.
        /// </summary>
        public static void CreateSalesSearchData()
        {
            if (salesSearchDataViewModel == null)
            {
                salesSearchDataViewModel = new SalesSearchDataViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsSalesSearchDataNull()
        {
            return salesSearchDataViewModel == null;
        }

        #endregion        

        #region StockReconciliation

        /// <summary>
        /// Gets the StockReconciliation property.
        /// </summary>
        public static StockReconciliationViewModel StockReconciliationStatic
        {
            get
            {
                if (stockReconciliationViewModel == null)
                {
                    CreateStockReconciliation();
                }
              
                return stockReconciliationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public StockReconciliationViewModel StockReconciliation
        {
            get
            {
                return StockReconciliationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the StockReconciliation property.
        /// </summary>
        public static void ClearStockReconciliation()
        {
            if (stockReconciliationViewModel != null)
            {
                stockReconciliationViewModel.Cleanup();
                stockReconciliationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the StockReconciliation property.
        /// </summary>
        public static void CreateStockReconciliation()
        {
            if (stockReconciliationViewModel == null)
            {
                stockReconciliationViewModel = new StockReconciliationViewModel();
            }
        }

        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsStockReconciliationNull()
        {
            return stockReconciliationViewModel == null;
        }

        #endregion        

        #region ReportSalesSearchData

        /// <summary>
        /// Gets the ReportSalesSearchData property.
        /// </summary>
        public static ReportSalesSearchDataViewModel ReportSalesSearchDataStatic
        {
            get
            {
                if (reportSalesSearchDataViewModel == null)
                {
                    CreateReportSalesSearchData();
                }
             
                return reportSalesSearchDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ReportSalesSearchDataViewModel ReportSalesSearchData
        {
            get
            {
                return ReportSalesSearchDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ReportSalesSearchData property.
        /// </summary>
        public static void ClearReportSalesSearchData()
        {
            if (reportSalesSearchDataViewModel != null)
            {
                reportSalesSearchDataViewModel.Cleanup();
                reportSalesSearchDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ReportSalesSearchData property.
        /// </summary>
        public static void CreateReportSalesSearchData()
        {
            if (reportSalesSearchDataViewModel == null)
            {
                reportSalesSearchDataViewModel = new ReportSalesSearchDataViewModel();
            }
        }

        #endregion    
    
        #region Process Selection

        /// <summary>
        /// Gets the ProcessSelection property.
        /// </summary>
        public static ProcessSelectionViewModel ProcessSelectionStatic
        {
            get
            {
                if (processSelectionViewModel == null)
                {
                    CreateProcessSelection();
                }


                return processSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ProcessSelectionViewModel ProcessSelection
        {
            get
            {
                return ProcessSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ProcessSelection property.
        /// </summary>
        public static void ClearProcessSelection()
        {
            if (processSelectionViewModel != null)
            {
                processSelectionViewModel.Cleanup();
                processSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ProcessSelection property.
        /// </summary>
        public static void CreateProcessSelection()
        {
            if (processSelectionViewModel == null)
            {
                processSelectionViewModel = new ProcessSelectionViewModel();
            }
        }

        #endregion        

        #region TouchscreenReportSalesSearchData

        /// <summary>
        /// Gets the TouchscreenReportSalesSearchData property.
        /// </summary>
        public static TouchscreenReportSalesSearchDataViewModel TouchscreenReportSalesSearchDataStatic
        {
            get
            {
                if (touchscreenReportSalesSearchDataViewModel == null)
                {
                    CreateTouchscreenReportSalesSearchData();
                }
                
                return touchscreenReportSalesSearchDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public TouchscreenReportSalesSearchDataViewModel TouchscreenReportSalesSearchData
        {
            get
            {
                return TouchscreenReportSalesSearchDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TouchscreenReportSalesSearchData property.
        /// </summary>
        public static void ClearTouchscreenReportSalesSearchData()
        {
            if (touchscreenReportSalesSearchDataViewModel != null)
            {
                touchscreenReportSalesSearchDataViewModel.Cleanup();
                touchscreenReportSalesSearchDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TouchscreenReportSalesSearchData property.
        /// </summary>
        public static void CreateTouchscreenReportSalesSearchData()
        {
            if (touchscreenReportSalesSearchDataViewModel == null)
            {
                touchscreenReportSalesSearchDataViewModel = new TouchscreenReportSalesSearchDataViewModel();
            }
        }

        #endregion        

        #region ReportProductData

        /// <summary>
        /// Gets the ReportProductData property.
        /// </summary>
        public static ReportProductDataViewModel ReportProductDataStatic
        {
            get
            {
                if (reportProductDataViewModel == null)
                {
                    CreateReportProductData();
                }
         
                return reportProductDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ReportProductDataViewModel ReportProductData
        {
            get
            {
                return ReportProductDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ReportProductData property.
        /// </summary>
        public static void ClearReportProductData()
        {
            if (reportProductDataViewModel != null)
            {
                reportProductDataViewModel.Cleanup();
                reportProductDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ReportProductData property.
        /// </summary>
        public static void CreateReportProductData()
        {
            if (reportProductDataViewModel == null)
            {
                reportProductDataViewModel = new ReportProductDataViewModel();
            }
        }

        #endregion        

        #region ReportTransactionData

        /// <summary>
        /// Gets the ReportTransactionData property.
        /// </summary>
        public static ReportTransactionDataViewModel ReportTransactionDataStatic
        {
            get
            {
                if (reportTransactionDataViewModel == null)
                {
                    CreateReportTransactionData();
                }
              
                return reportTransactionDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ReportTransactionDataViewModel ReportTransactionData
        {
            get
            {
                return ReportTransactionDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ReportTransactionData property.
        /// </summary>
        public static void ClearReportTransactionData()
        {
            if (reportTransactionDataViewModel != null)
            {
                reportTransactionDataViewModel.Cleanup();
                reportTransactionDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ReportTransactionData property.
        /// </summary>
        public static void CreateReportTransactionData()
        {
            if (reportTransactionDataViewModel == null)
            {
                reportTransactionDataViewModel = new ReportTransactionDataViewModel();
            }
        }

        #endregion        

        #region ReportTransaction

        /// <summary>
        /// Gets the ReportTransaction property.
        /// </summary>
        public static ReportTransactionViewModel ReportTransactionStatic
        {
            get
            {
                if (reportTransactionViewModel == null)
                {
                    CreateReportTransaction();
                }
  
                return reportTransactionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ReportTransactionViewModel ReportTransaction
        {
            get
            {
                return ReportTransactionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ReportTransaction property.
        /// </summary>
        public static void ClearReportTransaction()
        {
            if (reportTransactionViewModel != null)
            {
                reportTransactionViewModel.Cleanup();
                reportTransactionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ReportTransaction property.
        /// </summary>
        public static void CreateReportTransaction()
        {
            if (reportTransactionViewModel == null)
            {
                reportTransactionViewModel = new ReportTransactionViewModel();
            }
        }

        #endregion        

        #region ProductSelection

        /// <summary>
        /// Gets the ProductSelection property.
        /// </summary>
        public static ProductSelectionViewModel ProductSelectionStatic
        {
            get
            {
                if (productSelectionViewModel == null)
                {
                    CreateProductSelection();
                }
            
                return productSelectionViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public ProductSelectionViewModel ProductSelection
        {
            get
            {
                return ProductSelectionStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ProductSelection property.
        /// </summary>
        public static void ClearProductSelection()
        {
            if (productSelectionViewModel != null)
            {
                productSelectionViewModel.Cleanup();
                productSelectionViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ProductSelection property.
        /// </summary>
        public static void CreateProductSelection()
        {
            if (productSelectionViewModel == null)
            {
                productSelectionViewModel = new ProductSelectionViewModel();
            }
        }

        #endregion        

        #region InventoryGroup

        /// <summary>
        /// Gets the InventoryGroup property.
        /// </summary>
        public static InventoryGroupViewModel InventoryGroupStatic
        {
            get
            {
                if (inventoryGroupViewModel == null)
                {
                    CreateInventoryGroup();
                }
             
                return inventoryGroupViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public InventoryGroupViewModel InventoryGroup
        {
            get
            {
                return InventoryGroupStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the InventoryGroup property.
        /// </summary>
        public static void ClearInventoryGroup()
        {
            if (inventoryGroupViewModel != null)
            {
                inventoryGroupViewModel.Cleanup();
                inventoryGroupViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the InventoryGroup property.
        /// </summary>
        public static void CreateInventoryGroup()
        {
            if (inventoryGroupViewModel == null)
            {
                inventoryGroupViewModel = new InventoryGroupViewModel();
            }
        }

        #endregion        

        #region InventorySearchData

        /// <summary>
        /// Gets the InventorySearchData property.
        /// </summary>
        public static InventorySearchDataViewModel InventorySearchDataStatic
        {
            get
            {
                if (inventorySearchDataViewModel == null)
                {
                    CreateInventorySearchData();
                }
            
                return inventorySearchDataViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public InventorySearchDataViewModel InventorySearchData
        {
            get
            {
                return InventorySearchDataStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the InventorySearchData property.
        /// </summary>
        public static void ClearInventorySearchData()
        {
            if (inventorySearchDataViewModel != null)
            {
                inventorySearchDataViewModel.Cleanup();
                inventorySearchDataViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the InventorySearchData property.
        /// </summary>
        public static void CreateInventorySearchData()
        {
            if (inventorySearchDataViewModel == null)
            {
                inventorySearchDataViewModel = new InventorySearchDataViewModel();
            }
        }

        #endregion        

        #region InventoryMaster

        /// <summary>
        /// Gets the InventoryMaster property.
        /// </summary>
        public static InventoryMasterViewModel InventoryMasterStatic
        {
            get
            {
                if (inventoryMasterViewModel == null)
                {
                    CreateInventoryMaster();
                }
         
                return inventoryMasterViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public InventoryMasterViewModel InventoryMaster
        {
            get
            {
                return InventoryMasterStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the InventoryMaster property.
        /// </summary>
        public static void ClearInventoryMaster()
        {
            if (inventoryMasterViewModel != null)
            {
                inventoryMasterViewModel.Cleanup();
                inventoryMasterViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the InventoryMaster property.
        /// </summary>
        public static void CreateInventoryMaster()
        {
            if (inventoryMasterViewModel == null)
            {
                inventoryMasterViewModel = new InventoryMasterViewModel();
            }
        }

        #endregion        

        #region InventoryGroupSetUp

        /// <summary>
        /// Gets the InventoryGroupSetUp property.
        /// </summary>
        public static InventoryGroupSetUpViewModel InventoryGroupSetUpStatic
        {
            get
            {
                if (inventoryGroupSetUpViewModel == null)
                {
                    CreateInventoryGroupSetUp();
                }
             
                return inventoryGroupSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public InventoryGroupSetUpViewModel InventoryGroupSetUp
        {
            get
            {
                return InventoryGroupSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the InventoryGroupSetUp property.
        /// </summary>
        public static void ClearInventoryGroupSetUp()
        {
            if (inventoryGroupSetUpViewModel != null)
            {
                inventoryGroupSetUpViewModel.Cleanup();
                inventoryGroupSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the InventoryGroupSetUp property.
        /// </summary>
        public static void CreateInventoryGroupSetUp()
        {
            if (inventoryGroupSetUpViewModel == null)
            {
                inventoryGroupSetUpViewModel = new InventoryGroupSetUpViewModel();
            }
        }

        #endregion        

        #region UOM

        /// <summary>
        /// Gets the UOM property.
        /// </summary>
        public static UOMViewModel UOMStatic
        {
            get
            {
                if (uomViewModel == null)
                {
                    CreateUOM();
                }
             
                return uomViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public UOMViewModel UOM
        {
            get
            {
                return UOMStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the UOM property.
        /// </summary>
        public static void ClearUOM()
        {
            if (uomViewModel != null)
            {
                uomViewModel.Cleanup();
                uomViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the UOM property.
        /// </summary>
        public static void CreateUOM()
        {
            if (uomViewModel == null)
            {
                uomViewModel = new UOMViewModel();
            }
        }

        #endregion        

        #region Epos

        /// <summary>
        /// Gets the Epos property.
        /// </summary>
        public static EposViewModel EposStatic
        {
            get
            {
                if (eposViewModel == null)
                {
                    CreateEpos();
                }
                
                return eposViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public EposViewModel Epos
        {
            get
            {
                return EposStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Epos property.
        /// </summary>
        public static void ClearEpos()
        {
            if (eposViewModel != null)
            {
                eposViewModel.Cleanup();
                eposViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Epos property.
        /// </summary>
        public static void CreateEpos()
        {
            if (eposViewModel == null)
            {
                eposViewModel = new EposViewModel();
            }
        }

        #endregion        

        #region EposSettingsSettings

        /// <summary>
        /// Gets the EposSettings property.
        /// </summary>
        public static EposSettingsViewModel EposSettingsStatic
        {
            get
            {
                if (eposSettingsViewModel == null)
                {
                    CreateEposSettings();
                }
                
                return eposSettingsViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public EposSettingsViewModel EposSettings
        {
            get
            {
                return EposSettingsStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the EposSettings property.
        /// </summary>
        public static void ClearEposSettings()
        {
            if (eposSettingsViewModel != null)
            {
                eposSettingsViewModel.Cleanup();
                eposSettingsViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the EposSettings property.
        /// </summary>
        public static void CreateEposSettings()
        {
            if (eposSettingsViewModel == null)
            {
                eposSettingsViewModel = new EposSettingsViewModel();
            }
        }

        #endregion        

        #region EposPasswordChangePasswordChange

        /// <summary>
        /// Gets the EposPasswordChange property.
        /// </summary>
        public static EposPasswordChangeViewModel EposPasswordChangeStatic
        {
            get
            {
                if (eposPasswordChangeViewModel == null)
                {
                    CreateEposPasswordChange();
                }
    
                return eposPasswordChangeViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public EposPasswordChangeViewModel EposPasswordChange
        {
            get
            {
                return EposPasswordChangeStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the EposPasswordChange property.
        /// </summary>
        public static void ClearEposPasswordChange()
        {
            if (eposPasswordChangeViewModel != null)
            {
                eposPasswordChangeViewModel.Cleanup();
                eposPasswordChangeViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the EposPasswordChange property.
        /// </summary>
        public static void CreateEposPasswordChange()
        {
            if (eposPasswordChangeViewModel == null)
            {
                eposPasswordChangeViewModel = new EposPasswordChangeViewModel();
            }
        }

        #endregion        

        #region EposReport

        /// <summary>
        /// Gets the Report property.
        /// </summary>
        public static EposReportViewModel EposReportStatic
        {
            get
            {
                if (eposReportViewModel == null)
                {
                    CreateEposReport();
                }
       
                return eposReportViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public EposReportViewModel EposReport
        {
            get
            {
                return EposReportStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Report property.
        /// </summary>
        public static void ClearEposReport()
        {
            if (eposReportViewModel != null)
            {
                eposReportViewModel.Cleanup();
                eposReportViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Report property.
        /// </summary>
        public static void CreateEposReport()
        {
            if (eposReportViewModel == null)
            {
                eposReportViewModel = new EposReportViewModel();
            }
        }

        #endregion

        //#region ReportSearch

        ///// <summary>
        ///// Gets the Report property.
        ///// </summary>
        //public static ReportSearchViewModel ReportSearchStatic
        //{
        //    get
        //    {
        //        if (reportSearchViewModel == null)
        //        {
        //            CreateReportSearch();
        //        }

        //        return reportSearchViewModel;
        //    }
        //}

        ///// <summary>
        ///// Gets the Master property.
        ///// </summary>
        //public ReportSearchViewModel ReportSearch
        //{
        //    get
        //    {
        //        return ReportSearchStatic;
        //    }
        //}

        ///// <summary>
        ///// Provides a deterministic way to delete the Report property.
        ///// </summary>
        //public static void ClearReportSearch()
        //{
        //    if (reportSearchViewModel != null)
        //    {
        //        reportSearchViewModel.Cleanup();
        //        reportSearchViewModel = null;
        //    }
        //}

        ///// <summary>
        ///// Provides a deterministic way to create the Report property.
        ///// </summary>
        //public static void CreateReportSearch()
        //{
        //    if (reportSearchViewModel == null)
        //    {
        //        reportSearchViewModel = new ReportSearchViewModel();
        //    }
        //}

        //#endregion

        #region ReportSetUp

        /// <summary>
        /// Gets the ReportSetUp property.
        /// </summary>
        public static ReportSetUpViewModel ReportSetUpStatic
        {
            get
            {
                if (reportSetUpViewModel == null)
                {
                    CreateReportSetUp();
                }

                return reportSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ReportSetUpViewModel ReportSetUp
        {
            get
            {
                return ReportSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ReportSetUp property.
        /// </summary>
        public static void ClearReportSetUp()
        {
            if (reportSetUpViewModel != null)
            {
                reportSetUpViewModel.Cleanup();
                reportSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ReportSetUp property.
        /// </summary>
        public static void CreateReportSetUp()
        {
            if (reportSetUpViewModel == null)
            {
                reportSetUpViewModel = new ReportSetUpViewModel();
            }
        }

        #endregion

        #region ReportFolders

        /// <summary>
        /// Gets the ReportFolders property.
        /// </summary>
        public static ReportFoldersViewModel ReportFoldersStatic
        {
            get
            {
                if (reportFoldersViewModel == null)
                {
                    CreateReportFolders();
                }

                return reportFoldersViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public ReportFoldersViewModel ReportFolders
        {
            get
            {
                return ReportFoldersStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the ReportFolders property.
        /// </summary>
        public static void ClearReportFolders()
        {
            if (reportFoldersViewModel != null)
            {
                reportFoldersViewModel.Cleanup();
                reportFoldersViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the ReportFolders property.
        /// </summary>
        public static void CreateReportFolders()
        {
            if (reportFoldersViewModel == null)
            {
                reportFoldersViewModel = new ReportFoldersViewModel();
            }
        }

        #endregion        

        #region EposLoggedOut

        /// <summary>
        /// Gets the EposLoggedOut property.
        /// </summary>
        public static EposLoggedOutViewModel EposLoggedOutStatic
        {
            get
            {
                if (eposLoggedOutViewModel == null)
                {
                    CreateEposLoggedOut();
                }
   
                return eposLoggedOutViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public EposLoggedOutViewModel EposLoggedOut
        {
            get
            {
                return EposLoggedOutStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the EposLoggedOut property.
        /// </summary>
        public static void ClearEposLoggedOut()
        {
            if (eposLoggedOutViewModel != null)
            {
                eposLoggedOutViewModel.Cleanup();
                eposLoggedOutViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the EposLoggedOut property.
        /// </summary>
        public static void CreateEposLoggedOut()
        {
            if (eposLoggedOutViewModel == null)
            {
                eposLoggedOutViewModel = new EposLoggedOutViewModel();
            }
        }

        #endregion        

        #region VatSetUp

        /// <summary>
        /// Gets the VatSetUp property.
        /// </summary>
        public static VatSetUpViewModel VatSetUpStatic
        {
            get
            {
                if (vatSetUpViewModel == null)
                {
                    CreateVatSetUp();
                }
                
                return vatSetUpViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        public VatSetUpViewModel VatSetUp
        {
            get
            {
                return VatSetUpStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the VatSetUp property.
        /// </summary>
        public static void ClearVatSetUp()
        {
            if (vatSetUpViewModel != null)
            {
                vatSetUpViewModel.Cleanup();
                vatSetUpViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the VatSetUp property.
        /// </summary>
        public static void CreateVatSetUp()
        {
            if (vatSetUpViewModel == null)
            {
                vatSetUpViewModel = new VatSetUpViewModel();
            }
        }

        #endregion        

        #region TemplateAllocation

        /// <summary>
        /// Gets the TemplateAllocation property.
        /// </summary>
        public static TemplateAllocationViewModel TemplateAllocationStatic
        {
            get
            {
                if (templateAllocationViewModel == null)
                {
                    CreateTemplateAllocation();
                }
             
                return templateAllocationViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public TemplateAllocationViewModel TemplateAllocation
        {
            get
            {
                return TemplateAllocationStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the TemplateAllocation property.
        /// </summary>
        public static void ClearTemplateAllocation()
        {
            if (templateAllocationViewModel != null)
            {
                templateAllocationViewModel.Cleanup();
                templateAllocationViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the TemplateAllocation property.
        /// </summary>
        public static void CreateTemplateAllocation()
        {
            if (templateAllocationViewModel == null)
            {
                templateAllocationViewModel = new TemplateAllocationViewModel();
            }
        }

        #endregion        

        #region Blank

        /// <summary>
        /// Gets the Blank property.
        /// </summary>
        public static BlankViewModel BlankStatic
        {
            get
            {
                if (blankViewModel == null)
                {
                    CreateBlank();
                }

                return blankViewModel;
            }
        }

        /// <summary>
        /// Gets the Master property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public BlankViewModel Blank
        {
            get
            {
                return BlankStatic;
            }
        }

        /// <summary>
        /// Provides a deterministic way to delete the Blank property.
        /// </summary>
        public static void ClearBlank()
        {
            if (blankViewModel != null)
            {
                blankViewModel.Cleanup();
                blankViewModel = null;
            }
        }

        /// <summary>
        /// Provides a deterministic way to create the Blank property.
        /// </summary>
        public static void CreateBlank()
        {
            if (blankViewModel == null)
            {
                blankViewModel = new BlankViewModel();
            }
        }

        #endregion        

        #endregion

        public static void CleanUpTouchscreens()
        {
            ClearARReturnTouchscreen();
            ClearAPReceiptTouchscreen();
            ClearIntoProduction();
            ClearOutOfProduction();
            ClearARDispatchTouchscreen();
            ClearAPReceiptDetails();
            ClearARReturn();
            ClearARDispatchDetails();
            ClearFactoryScreen();
            ClearTouchscreen();
        }

        public static void CleanUpTouchscreenChildModules()
        {
            ClearARReturnTouchscreen();
            ClearAPReceiptTouchscreen();
            ClearIntoProduction();
            ClearOutOfProduction();
            ClearARDispatchTouchscreen();
            ClearAPReceiptDetails();
            ClearARDispatchDetails();
            ClearTouchscreenSuppliers();
            ClearTouchscreenProducts();
            ClearProductionProductsCards();
        }

        public static void CleanUpPanels()
        {
            ClearTouchscreenReceiptPanel();
            ClearTouchscreenReceiptPalletPanel();
            ClearTouchscreenReceiptProductionPanel();
            ClearTouchscreenProductionPanel();
            ClearTouchscreenProductionRecallPanel();
            ClearTouchscreenOutOfProductionPanel();
            ClearTouchscreenOutOfProductionWithPiecesPanel();
            ClearTouchscreenDispatchPanel();
            ClearTouchscreenDispatchBatchPanel();
            ClearTouchscreenDispatchPanelRecordWeight();
            ClearTouchscreenDispatchPanelRecordWeightWithNotes();
            ClearTouchscreenDispatchPanelRecordWeightWithShipping();
            ClearSequencerPanel();
            ClearGraderPanel();
            ClearLairagePanel();
            ClearTouchscreenProductionPanel();
            ClearARReturnPanel();
        }

        public static void ClearTouchscreenModules(NouvemViewModelBase vm)
        {
            if (apReceiptTouchscreenViewModel != vm)
            {
                ClearAPReceiptTouchscreen();
            }

            if (ARReturnTouchscreenViewModel != vm)
            {
                ClearARReturnTouchscreen();
            }

            if (intoProductionViewModel != vm)
            {
                ClearIntoProduction();
            }

            if (outOfProductionViewModel != vm)
            {
                ClearOutOfProduction();
            }

            if (arDispatchTouchscreenViewModel != vm)
            {
                ClearARDispatch();
            }

            if (sequencerViewModel != vm)
            {
                ClearSequencer();
            }

            if (graderViewModel != vm)
            {
                ClearGrader();
            }

            //if (stockMovementTouchscreenViewModel != vm)
            //{
            //    ClearStockMovementTouchscreen();
            //}

            if (workflowViewModel != vm)
            {
                ClearWorkflow();
            }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }


        /// <summary>
        /// Determines if the vm is null.
        /// </summary>
        public static bool IsProductionOrdersLoaded()
        {
            return touchscreenProductionOrdersViewModel != null && touchscreenProductionOrdersViewModel.IsFormLoaded;
        }
    }
}