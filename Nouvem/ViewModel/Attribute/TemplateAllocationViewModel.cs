﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityTemplateAllocationViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Editors.Helpers;
using DevExpress.XtraSpreadsheet.Utils;
using Nouvem.Model.BusinessObject;

namespace Nouvem.ViewModel.Attribute
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class TemplateAllocationViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The attribute code.
        /// </summary>
        private bool isStandard;

        /// <summary>
        /// The attribute code.
        /// </summary>
        private CollectionData selectedAttributeType;

        /// <summary>
        /// The notes.
        /// </summary>
        private string notes;

        /// <summary>
        /// The notes.
        /// </summary>
        private bool attributesAssigned;

        /// <summary>
        /// The copy template name.
        /// </summary>
        private string copyTemplateName;

        /// <summary>
        /// The template inactive flag.
        /// </summary>
        private bool inactive;

        /// <summary>
        /// The template inactive flag.
        /// </summary>
        private bool showInactive;

        /// <summary>
        /// The template names reference.
        /// </summary>
        private ObservableCollection<AttributeTemplateData> traceabilityTemplateNames;

        /// <summary>
        /// The template names reference.
        /// </summary>
        private IList<AttributeTemplateData> allTraceabilityTemplateNames;

        /// <summary>
        /// The selected template name
        /// </summary>
        private AttributeTemplateData selectedTemplateName;

        /// <summary>
        /// The selected available traceability reference.
        /// </summary>
        private AttributeMasterData selectedAvailableTraceabilityMaster;

        /// <summary>
        /// The selected assigned traceability reference.
        /// </summary>
        private AttributeMasterData selectedAssignedTraceabilityMaster;

        /// <summary>
        /// Is it a workflow flag.
        /// </summary>
        private bool isWorkflow;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the TemplateAllocationViewModel class.
        /// </summary>
        public TemplateAllocationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Tuple<int?, bool>>(this, Token.AttributeTypeSelected, i =>
            {
                if (this.SelectedAssignedTraceabilityMaster != null)
                {
                    var resetId = i.Item1;
                    var isBatch = i.Item2;
                    var reset = this.AttributeResets.FirstOrDefault(x => x.NouAttributeResetID == resetId);
                    if (reset != null)
                    {
                        //if (isBatch
                        //    && (reset.Name.CompareIgnoringCase(AttributeReset.ResetAfterProductChange) || reset.Name.CompareIgnoringCase(AttributeReset.ResetAfterTransaction)))
                        //{
                        //    SystemMessage.Write(MessageType.Issue, string.Format(Message.InvalidAttributeReset, reset.Name, Strings.Batch));
                        //    this.SelectedAssignedTraceabilityMaster.NouAttributeResetId = null;
                        //}

                        //if (!isBatch && reset.Name.CompareIgnoringCase(AttributeReset.ResetOnBatchChange))
                        //{
                        //    SystemMessage.Write(MessageType.Issue, string.Format(Message.InvalidAttributeReset, reset.Name, Strings.Transaction));
                        //    this.SelectedAssignedTraceabilityMaster.NouAttributeResetId = null;
                        //}
                    }
                }
            });

            Messenger.Default.Register<string>(this, Token.ShowSqlWindow, s =>
            {
                if (this.SelectedAssignedTraceabilityMaster == null)
                {
                    return;
                }

                Messenger.Default.Send(this.SelectedAssignedTraceabilityMaster.DefaultSQL ?? string.Empty, Token.DisplaySql);
            });

            Messenger.Default.Register<string>(this, Token.SqlScript, s =>
            {
                if (this.SelectedAssignedTraceabilityMaster != null)
                {
                    this.SelectedAssignedTraceabilityMaster.DefaultSQL = s;
                }
            });

            // register fot a date master data change.
            Messenger.Default.Register<string>(this, Token.TraceabilityTemplateNamesUpdated, s =>
            {
                this.GetTraceabilityTemplateNames(true);

                if (this.TraceabilityMasters != null)
                {
                    foreach (var attributeMasterData in this.TraceabilityMasters)
                    {
                        attributeMasterData.VisibleProcessesForCombo = new List<object>();
                        attributeMasterData.EditProcessesForCombo = new List<object>();
                    }
                }
            });

            #endregion

            #region command registration

            this.CopyTemplateCommand = new RelayCommand(this.CopyTemplateCommandExecute);

            this.OnLoadingCommand = new RelayCommand(() =>
            {
                this.SelectedTemplateName = null;
                this.IsWorkflow = false;
                this.IsStandard = false;
                this.SelectedAttributeType = this.AttributeTypes.FirstOrDefault();
                this.IsFormLoaded = true;
                this.Inactive = false;
                this.ShowInactive = false;
                if (this.TraceabilityTemplateNames != null)
                {
                    this.TraceabilityTemplateNames.Clear();
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.SelectedTemplateType, NouvemMessageBoxButtons.StandardWorkflow);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Standard)
                    {
                        this.IsStandard = true;
                    }
                    else
                    {
                        this.IsWorkflow = true;
                    }
                }));
            });

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.attributesAssigned = true;
                this.SetControlMode(ControlMode.Update);
            });

            // Command to move the selected date.
            this.MoveTraceabilityCommand = new RelayCommand<string>(this.MoveTraceabilityCommandExecute);

            #endregion

            #region instantiation

            this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplateData>();
            this.TraceabilityMasters = new ObservableCollection<AttributeMasterData>();
            this.AvailableTraceabilityMasters = new ObservableCollection<AttributeMasterData>();
            this.AssignedTraceabilityMasters = new ObservableCollection<AttributeMasterData>();
            this.TraceabilityTemplateAllocations = new List<AttributeAllocationData>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessTraceability);
            this.GetTraceabilityMasters();
            this.TraceabilityTemplateAllocations = NouvemGlobal.AttributeAllocationData;
            this.GetTraceabilityTemplateNames();
            this.GetAttributeTabNames();
            this.GetAttributeResets();
            this.GetProcesses();
            this.GetAttributeProcesses();
            this.GetSPNames();
            this.IsStandard = true;
            this.GetAttributeTypes();
        }

        #endregion

        #region property

        /// <summary>
        /// The attribute code.
        /// </summary>
        public IList<CollectionData> AttributeTypes { get; set; }

        /// <summary>
        /// Gets or sets the copy template name.
        /// </summary>
        public string CopyTemplateName
        {
            get
            {
                return this.copyTemplateName;
            }

            set
            {
                this.copyTemplateName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the template is inactive or not.
        /// </summary>
        public bool Inactive
        {
            get
            {
                return this.inactive;
            }

            set
            {
                this.SetMode(value, this.inactive);
                this.inactive = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the template is inactive or not.
        /// </summary>
        public bool ShowInactive
        {
            get
            {
                return this.showInactive;
            }

            set
            {
                this.showInactive = value;
                this.RaisePropertyChanged();
                IList<AttributeTemplateData> templateData = new List<AttributeTemplateData>();
                if (value)
                {
                    if (this.IsWorkflow)
                    {
                        templateData = this.GetTemplates().Where(x => x.IsWorkflow && (x.Inactive == true)).OrderBy(x => x.Name).ToList();
                    }
                    else if (this.IsStandard)
                    {
                        templateData = this.GetTemplates().Where(x => !x.IsWorkflow && (x.Inactive == true)).OrderBy(x => x.Name).ToList();
                    }

                    this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplateData>(templateData);
                    this.TraceabilityTemplateNames.Add(new AttributeTemplateData { Name = Strings.DefineNew });
                }
                else
                {
                    if (this.IsWorkflow)
                    {
                        templateData = this.GetTemplates().Where(x => x.IsWorkflow && (x.Inactive == false || x.Inactive == null)).OrderBy(x => x.Name).ToList();
                    }
                    else if (this.IsStandard)
                    {
                        templateData = this.GetTemplates().Where(x => !x.IsWorkflow && (x.Inactive == false || x.Inactive == null)).OrderBy(x => x.Name).ToList();
                    }

                    this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplateData>(templateData);
                    this.TraceabilityTemplateNames.Add(new AttributeTemplateData { Name = Strings.DefineNew });
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is standard.
        /// </summary>
        public bool IsStandard
        {
            get
            {
                return this.isStandard;
            }

            set
            {
                if (value)
                {
                    var localTemplate = this.SelectedTemplateName;
                    IList<AttributeTemplateData> templateData;
                    if (this.ShowInactive)
                    {
                        templateData = this.GetTemplates().Where(x => x.Inactive == true && !x.IsWorkflow || x.IsNew).OrderBy(x => x.Name).ToList();
                    }
                    else
                    {
                        templateData = this.GetTemplates().Where(x => (x.Inactive == false || x.Inactive == null) && !x.IsWorkflow || x.IsNew).OrderBy(x => x.Name).ToList();
                    }

                    this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplateData>(templateData);
                    this.TraceabilityTemplateNames.Add(new AttributeTemplateData { Name = Strings.DefineNew });
                    if (localTemplate != null && localTemplate.IsNew)
                    {
                        this.EntitySelectionChange = true;
                        this.SelectedTemplateName =
                            this.TraceabilityTemplateNames.FirstOrDefault(x => x.IsNew && x.Name == localTemplate.Name);
                        this.EntitySelectionChange = false;
                    }

                    if (this.AssignedTraceabilityMasters.Any(x => x.UseAsWorkflow))
                    {
                        SystemMessage.Write(MessageType.Issue, string.Format(Message.InvalidTemplateType, Strings.Workflow));
                        return;
                    }

                    this.AvailableTraceabilityMasters.Clear();
                    this.AvailableTraceabilityMasters =
                        new ObservableCollection<AttributeMasterData>(
                            this.TraceabilityMasters.Where(x => !x.UseAsWorkflow && !this.AssignedTraceabilityMasters.Contains(x)).OrderBy(x => x.Code).ToList());
                }

                this.isStandard = value;
                this.RaisePropertyChanged();
                Messenger.Default.Send(value, Token.ShowStandardColumns);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is standard transaction.
        /// </summary>
        public CollectionData SelectedAttributeType
        {
            get
            {
                return this.selectedAttributeType;
            }

            set
            {
                this.selectedAttributeType = value;
                this.RaisePropertyChanged();

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(true, Token.ShowStandardColumns);
                }));
            }
        }

        /// <summary>
        /// Gets or sets the application sp names.
        /// </summary>
        public IList<string> SPNames { get; set; }

        /// <summary>
        /// Gets or sets the  processes.
        /// </summary>
        public IList<Process> Processes { get; set; }

        /// <summary>
        /// Gets or sets the edit processes.
        /// </summary>
        public IList<AttributeProcess> AttributeEditProcesses { get; set; }

        /// <summary>
        /// Gets or sets the visible processes.
        /// </summary>
        public IList<AttributeProcess> AttributeVisibleProcesses { get; set; }

        /// <summary>
        /// Gets or sets the resets.
        /// </summary>
        public IList<NouAttributeReset> AttributeResets { get; set; }

        /// <summary>
        /// Gets or sets the tab names.
        /// </summary>
        public IList<AttributeTabName> AttributeTabNames { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is a workflow template.
        /// </summary>
        public bool IsWorkflow
        {
            get
            {
                return this.isWorkflow;
            }

            set
            {
                if (this.IsFormLoaded)
                {
                    this.SetControlMode(ControlMode.Update);
                }

                if (value)
                {
                    var localTemplate = this.SelectedTemplateName;
                    IList<AttributeTemplateData> templateData;
                    if (this.ShowInactive)
                    {
                        templateData = this.GetTemplates().Where(x => x.Inactive == true && x.IsWorkflow || x.IsNew).OrderBy(x => x.Name).ToList();
                    }
                    else
                    {
                        templateData = this.GetTemplates().Where(x => (x.Inactive == false || x.Inactive == null) && x.IsWorkflow || x.IsNew).OrderBy(x => x.Name).ToList();
                    }

                    this.TraceabilityTemplateNames = new ObservableCollection<AttributeTemplateData>(templateData);
                    this.TraceabilityTemplateNames.Add(new AttributeTemplateData { Name = Strings.DefineNew });

                    if (localTemplate != null && localTemplate.IsNew)
                    {
                        this.EntitySelectionChange = true;
                        this.SelectedTemplateName =
                            this.TraceabilityTemplateNames.FirstOrDefault(x => x.IsNew && x.Name == localTemplate.Name);
                        this.EntitySelectionChange = false;
                    }

                    foreach (var trace in this.AssignedTraceabilityMasters)
                    {
                        trace.Batch = true;
                        trace.Transaction = false;
                    }

                    if (this.AssignedTraceabilityMasters.Any(x => !x.UseAsWorkflow))
                    {
                        SystemMessage.Write(MessageType.Issue, string.Format(Message.InvalidTemplateType, Strings.Standard));
                        return;
                    }

                    this.AvailableTraceabilityMasters.Clear();
                    this.AvailableTraceabilityMasters =
                        new ObservableCollection<AttributeMasterData>(
                            this.TraceabilityMasters.Where(x => x.UseAsWorkflow && !this.AssignedTraceabilityMasters.Contains(x)).OrderBy(x => x.Code).ToList());
                }

                this.isWorkflow = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get the Traceability template allocations.
        /// </summary>
        public IList<AttributeAllocationData> TraceabilityTemplateAllocations { get; private set; }

        /// <summary>
        /// Get the Traceability Masters.
        /// </summary>
        public ObservableCollection<AttributeMasterData> TraceabilityMasters { get; private set; }

        /// <summary>
        /// The available Traceability masters reference.
        /// </summary>
        private ObservableCollection<AttributeMasterData> availableTraceability;

        /// <summary>
        /// The assigned Traceability masters reference.
        /// </summary>
        private ObservableCollection<AttributeMasterData> assignedTraceability;

        /// <summary>
        /// Get or sets the available Traceability Masters.
        /// </summary>
        public ObservableCollection<AttributeMasterData> AvailableTraceabilityMasters
        {
            get
            {
                return this.availableTraceability;
            }

            set
            {
                this.availableTraceability = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        public string Notes
        {
            get
            {
                return this.notes;
            }

            set
            {
                this.SetMode(value, this.notes);
                this.notes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the assigned Traceability Masters.
        /// </summary>
        public ObservableCollection<AttributeMasterData> AssignedTraceabilityMasters
        {
            get
            {
                return this.assignedTraceability;
            }

            set
            {
                this.assignedTraceability = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the Traceability Masters.
        /// </summary>
        public ObservableCollection<AttributeTemplateData> TraceabilityTemplateNames
        {
            get
            {
                return this.traceabilityTemplateNames;
            }

            set
            {
                this.traceabilityTemplateNames = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the selected Traceability template name.
        /// </summary>
        public AttributeTemplateData SelectedTemplateName
        {
            get
            {
                return this.selectedTemplateName;
            }

            set
            {
                var handleTemplate = true;
                if (value != null && this.selectedTemplateName != null &&
                        this.selectedTemplateName.AttributeTemplateID == value.AttributeTemplateID)
                {
                    handleTemplate = false;
                }

                this.selectedTemplateName = value;
                this.RaisePropertyChanged();
                this.attributesAssigned = false;

                if (value != null && !this.EntitySelectionChange)
                {
                    if (handleTemplate)
                    {
                        this.HandleSelectedTemplateName();
                    }
                }
                else
                {
                    this.AvailableTraceabilityMasters.Clear();
                    this.AssignedTraceabilityMasters.Clear();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected available Traceabilitymaster.
        /// </summary>
        public AttributeMasterData SelectedAvailableTraceabilityMaster
        {
            get
            {
                return this.selectedAvailableTraceabilityMaster;
            }

            set
            {
                this.selectedAvailableTraceabilityMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected assigned Traceability master.
        /// </summary>
        public AttributeMasterData SelectedAssignedTraceabilityMaster
        {
            get
            {
                return this.selectedAssignedTraceabilityMaster;
            }

            set
            {
                this.selectedAssignedTraceabilityMaster = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand CopyTemplateCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; set; }

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to move the Traceability.
        /// </summary>
        public ICommand MoveTraceabilityCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Drill down to the template names.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            Messenger.Default.Send(Tuple.Create(ViewType.AttributeTemplateName, 0), Token.DrillDown);
            this.AvailableTraceabilityMasters.Clear();
            this.AssignedTraceabilityMasters.Clear();
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    //this.UpdateTraceabilityTemplateNames();
                    this.UpdateTemplate();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the application sp names. 
        /// </summary>
        private void GetSPNames()
        {
            this.SPNames = this.DataManager.GetSPNames().Where(x => x.StartsWithIgnoringCase(Constant.Macro)).ToList();
            this.SPNames.Insert(0, string.Empty);
        }

        /// <summary>
        /// Copies a template.
        /// </summary>
        private void CopyTemplateCommandExecute()
        {
            #region validation

            if (this.SelectedTemplateName == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCopyTemplateSelected);
                return;
            }

            if (string.IsNullOrWhiteSpace(this.CopyTemplateName))
            {
                SystemMessage.Write(MessageType.Issue, Message.CopyTemplateNameEmpty);
                return;
                return;
            }

            #endregion

            ProgressBar.SetUp(1, 3);
            ProgressBar.Run();

            if (ApplicationSettings.DisplayReportPrintMessageOnCopyTemplate)
            {
                NouvemMessageBox.Show(Message.CopyTemplateReportPrintPrompt);
            }

            try
            {
                var newTemplateId = this.DataManager.CopyTemplate(this.SelectedTemplateName.AttributeTemplateID,
               this.copyTemplateName);

                if (newTemplateId > 0)
                {
                    NouvemGlobal.GetAttributeAllocationData();
                    this.TraceabilityTemplateAllocations = NouvemGlobal.AttributeAllocationData;
                    ProgressBar.Run();
                    this.GetTraceabilityMasters();
                    this.GetTraceabilityTemplateNames();
                    ProgressBar.Run();
                    this.SelectedTemplateName =
                        this.TraceabilityTemplateNames.FirstOrDefault(x => x.AttributeTemplateID == newTemplateId);
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.NewTemplateCopied, this.CopyTemplateName));
                    this.CopyTemplateName = string.Empty;
                }
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Updates the current template.
        /// </summary>
        private void UpdateTemplate()
        {
            if (this.SelectedTemplateName == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoTemplateSelected);
                return;
            }

            if (this.AssignedTraceabilityMasters == null || !this.AssignedTraceabilityMasters.Any())
            {
                SystemMessage.Write(MessageType.Issue, Message.NoAllocationsOnTemplate);
                NouvemMessageBox.Show(Message.NoAllocationsOnTemplate);
                return;
            }

            if (!this.attributesAssigned)
            {
                // no attributes updating, so to speed things up we ignore them.
                if (this.DataManager.AddAttributesToTemplate(new List<AttributeMasterData>(),
               this.selectedTemplateName.AttributeTemplateID, this.IsWorkflow, this.notes, this.inactive, this.SelectedAttributeType.Data))
                {
                    SystemMessage.Write(MessageType.Priority, Message.TemplateUpdated);
                    this.SetControlMode(ControlMode.OK);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.TemplateNotUpdated);
                }

                this.attributesAssigned = false;
                return;
            }

            if (this.IsStandard && this.SelectedAttributeType.Data.CompareIgnoringCaseAndWhitespace(Strings.Standard))
            {
                foreach (var assignedTraceabilityMaster in this.AssignedTraceabilityMasters)
                {
                    if (assignedTraceabilityMaster.AttributeTabNameId == null)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoTabAllocated);
                        return;
                    }
                }
            }

            foreach (var assignedTraceabilityMaster in this.AssignedTraceabilityMasters)
            {
                assignedTraceabilityMaster.UseAsWorkflow = this.IsWorkflow;
            }

            try
            {
                var traceabilityMasters = this.AssignedTraceabilityMasters.DistinctBy(x => x.AttributeMasterId).ToList();
                foreach (var attributeMasterData in traceabilityMasters)
                {
                    attributeMasterData.VisibleProcessesForCombo = attributeMasterData.VisibleProcessesForCombo.Distinct().ToList();
                    attributeMasterData.EditProcessesForCombo = attributeMasterData.EditProcessesForCombo.Distinct().ToList();
                }

                ProgressBar.SetUp(0, traceabilityMasters.Count + 1);
                ProgressBar.Run();

                if (this.DataManager.AddAttributesToTemplate(traceabilityMasters,
               this.selectedTemplateName.AttributeTemplateID, this.IsWorkflow, this.notes, this.inactive, this.SelectedAttributeType.Data))
                {
                    SystemMessage.Write(MessageType.Priority, Message.TemplateUpdated);
                    this.Refresh();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.TemplateNotUpdated);
                }
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        ///// <summary>
        ///// Updates the Traceability template names group.
        ///// </summary>
        //private void UpdateTraceabilityTemplateNames()
        //{
        //    try
        //    {
        //        if (this.DataManager.AddOrUpdateTraceabilityTemplateNames(this.TraceabilityTemplateNames))
        //        {
        //            SystemMessage.Write(MessageType.Priority, Message.TraceabilityTemplateNamesUpdated);
        //        }
        //        else
        //        {
        //            SystemMessage.Write(MessageType.Issue, Message.TraceabilityTemplateNamesNotUpdated);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Log.LogError(this.GetType(), ex.Message);
        //        SystemMessage.Write(MessageType.Issue, Message.TraceabilityTemplateNamesNotUpdated);
        //    }

        //    this.SetControlMode(ControlMode.OK);
        //}

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearTemplateAllocation();
            Messenger.Default.Send(Token.Message, Token.CloseTraceabilityTemplateAllocationWindow);
        }

        /// <summary>
        /// Gets the application Traceability template names information.
        /// </summary>
        /// <param name="selectRecentlyAdded">Flag to determine if we are selecting the most recently added template.</param>
        private void GetTraceabilityTemplateNames(bool selectRecentlyAdded = false)
        {
            this.allTraceabilityTemplateNames = this.DataManager.GetTemplateNames().ToList();
            this.TraceabilityTemplateNames.Clear();
            foreach (var traceability in this.allTraceabilityTemplateNames)
            {
                this.TraceabilityTemplateNames.Add(traceability);
            }

            this.TraceabilityTemplateNames.Add(new AttributeTemplateData { Name = Strings.DefineNew });

            // Select the first, or most recently added template.
            if (selectRecentlyAdded)
            {
                this.EntitySelectionChange = true;
                this.SelectedTemplateName =
                    this.TraceabilityTemplateNames.ElementAt(this.TraceabilityTemplateNames.Count - 2);
                this.SelectedTemplateName.IsNew = true;
                this.EntitySelectionChange = false;

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.IsWorkflow = false;
                    this.IsStandard = false;
                }));
            }
        }

        /// <summary>
        /// Handles the selection of a Traceability template name.
        /// </summary>
        private void HandleSelectedTemplateName()
        {
            if (string.IsNullOrEmpty(this.SelectedTemplateName.AttributeType) || this.SelectedTemplateName.AttributeType.CompareIgnoringCaseAndWhitespace(Strings.Standard))
            {
                this.SelectedAttributeType = this.AttributeTypes.First();
            }
            else
            {
                this.SelectedAttributeType = this.AttributeTypes.FirstOrDefault(x => x.Data.CompareIgnoringCaseAndWhitespace(this.SelectedTemplateName.AttributeType));
            }

            this.IsWorkflow = false;

            // check if we are defining a new template name
            if (this.selectedTemplateName.Name.Equals(Strings.DefineNew))
            {
                // new..shortcut to the template name view.
                Messenger.Default.Send(ViewType.AttributeTemplateName);
                this.AvailableTraceabilityMasters.Clear();
                this.AssignedTraceabilityMasters.Clear();
                return;
            }


            this.Notes = this.selectedTemplateName.Notes;
            this.Inactive = this.selectedTemplateName.Inactive.ToBool();

            foreach (var attributeMasterData in this.TraceabilityMasters)
            {
                attributeMasterData.VisibleProcessesForCombo = new List<object>();
                attributeMasterData.EditProcessesForCombo = new List<object>();
            }

            this.AvailableTraceabilityMasters.Clear();
            this.AssignedTraceabilityMasters.Clear();
            this.AvailableTraceabilityMasters = new ObservableCollection<AttributeMasterData>(this.TraceabilityMasters.OrderBy(x => x.Code));

            var localUseAsWorkflow = false;
            var localAllocations =
                this.TraceabilityTemplateAllocations.Where(
                    x => x.AttributionAllocation.AttributeTemplateID == this.selectedTemplateName.AttributeTemplateID && x.AttributeEditProcesses == null).ToList();
            if (localAllocations.Any())
            {
                this.DataManager.GetAttributeAllocations(localAllocations);
            }

            this.TraceabilityTemplateAllocations.ToList().ForEach(x =>
            {
                if (x.AttributionAllocation.AttributeTemplateID == this.selectedTemplateName.AttributeTemplateID)
                {
                    var localTraceabilityMaster =
                        this.TraceabilityMasters.FirstOrDefault(traceabilityMaster => traceabilityMaster.AttributeMasterId == x.AttributionAllocation.AttributeMasterID);

                    if (localTraceabilityMaster != null)
                    {
                        localTraceabilityMaster.Batch = x.AttributionAllocation.Batch.ToBool();
                        localTraceabilityMaster.Transaction = x.AttributionAllocation.Transaction.ToBool();
                        localTraceabilityMaster.RequiredBeforeContinue = x.AttributionAllocation.RequiredBeforeContinue;
                        localTraceabilityMaster.RequiredBeforeCompletion = x.AttributionAllocation.RequiredBeforeCompletion;
                        localTraceabilityMaster.DefaultSQL = x.AttributionAllocation.DefaultValueSQL;
                        localTraceabilityMaster.Sequence = x.AttributionAllocation.Sequence.ToInt();
                        localTraceabilityMaster.NouAttributeResetId = x.AttributionAllocation.NouAttributeResetID;
                        localTraceabilityMaster.AttributeTabNameId = x.AttributionAllocation.AttributeTabNameID;
                        localTraceabilityMaster.LoadMacro = x.AttributionAllocation.LoadMacro;
                        localTraceabilityMaster.PostSelectionMacro = x.AttributionAllocation.PostSelectionMACRO;

                        this.AssignedTraceabilityMasters.Add(localTraceabilityMaster);
                        this.AvailableTraceabilityMasters.Remove(localTraceabilityMaster);

                        foreach (var attributeEditProcess in x.AttributeEditProcesses)
                        {
                            localTraceabilityMaster.EditProcessesForCombo.Add(attributeEditProcess.ProcessId);
                        }

                        foreach (var attributeVisibleProcess in x.AttributeVisibleProcesses)
                        {
                            localTraceabilityMaster.VisibleProcessesForCombo.Add(attributeVisibleProcess.ProcessId);
                        }
                    }

                    if (x.AttributionAllocation.AttributeMaster.IsWorkflow.ToBool())
                    {
                        localUseAsWorkflow = true;
                    }
                }
            });

            this.IsWorkflow = localUseAsWorkflow;
            this.IsStandard = !this.IsWorkflow;
        }

        /// <summary>
        /// Gets the tabs.
        /// </summary>
        private void GetAttributeTabNames()
        {
            this.AttributeTabNames = this.DataManager.GetAttributeTabNames();
        }

        /// <summary>
        /// Gets the tabs.
        /// </summary>
        private void GetAttributeResets()
        {
            this.AttributeResets = this.DataManager.GetAttributeResets();
        }

        /// <summary>
        /// Gets the attribute proceses.
        /// </summary>
        private void GetAttributeProcesses()
        {
            var processes = this.DataManager.GetAttributeProcesses();
            this.AttributeEditProcesses = new List<AttributeProcess>();
            this.AttributeVisibleProcesses = new List<AttributeProcess>();
            foreach (var attributeProcess in processes)
            {
                if (attributeProcess.Type == AttributeProcess.ProcessType.Editable)
                {
                    this.AttributeEditProcesses.Add(attributeProcess);
                }
                else
                {
                    this.AttributeVisibleProcesses.Add(attributeProcess);
                }
            }
        }

        /// <summary>
        /// Gets the attribute types.
        /// </summary>
        private void GetAttributeTypes()
        {
            this.AttributeTypes = new List<CollectionData>
            {
                new CollectionData {Data = Strings.Standard},
                new CollectionData {Data = Strings.Transaction},
                new CollectionData {Data = Strings.PostTransaction},
                new CollectionData {Data = Strings.Completion},
                new CollectionData {Data = Strings.Movement},
                new CollectionData {Data = Strings.Label}
            };
        }

        /// <summary>
        /// Gets the proceses.
        /// </summary>
        private void GetProcesses()
        {
            this.Processes = this.DataManager.GetProcesses();
        }

        /// <summary>
        /// Gets the application Traceability masters information.
        /// </summary>
        private void GetTraceabilityMasters()
        {
            this.TraceabilityMasters.Clear();
            foreach (var date in this.DataManager.GetAttributes())
            {
                this.TraceabilityMasters.Add(date);
            }
        }

        /// <summary>
        /// Gets the Traceability template allocations.
        /// </summary>
        private void GetTemplateAllocations()
        {
            var localDatas =
                this.DataManager.GetAttributeAllocationForTemplate(this.SelectedTemplateName.AttributeTemplateID);

            foreach (var traceabilityTemplateAllocation in this.TraceabilityTemplateAllocations.ToList())
            {
                if (traceabilityTemplateAllocation.AttributionAllocation.AttributeTemplateID ==
                    this.SelectedTemplateName.AttributeTemplateID)
                {
                    this.TraceabilityTemplateAllocations.Remove(traceabilityTemplateAllocation);
                }
            }

            foreach (var traceabilityTemplateAllocation in NouvemGlobal.AttributeAllocationData.ToList())
            {
                if (traceabilityTemplateAllocation.AttributionAllocation.AttributeTemplateID ==
                    this.SelectedTemplateName.AttributeTemplateID)
                {
                    NouvemGlobal.AttributeAllocationData.Remove(traceabilityTemplateAllocation);
                }
            }

            if (localDatas != null)
            {
                foreach (var localData in localDatas)
                {
                    this.TraceabilityTemplateAllocations.Add(localData);
                }
            }

            NouvemGlobal.AttributeAllocationData = this.TraceabilityTemplateAllocations;
            this.allTraceabilityTemplateNames = this.DataManager.GetTemplateNames().ToList();
            if (this.selectedTemplateName != null)
            {
                var localTemplateName =
                    this.allTraceabilityTemplateNames.FirstOrDefault(
                        x => x.AttributeTemplateID == this.selectedTemplateName.AttributeTemplateID);
                if (localTemplateName != null)
                {
                    this.selectedTemplateName.Notes = localTemplateName.Notes;
                    this.selectedTemplateName.Inactive = localTemplateName.Inactive;
                    this.selectedTemplateName.IsWorkflow = localTemplateName.IsWorkflow;
                }
            }
        }

        /// <summary>
        /// handles the allocation/deallocation of Traceabilitys to and from the selected template.
        /// </summary>
        /// <param name="direction">The allocation direction.</param>
        private void MoveTraceabilityCommandExecute(string direction)
        {
            #region validation

            if (this.SelectedTemplateName == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoTemplateSelected);
                return;
            }

            if (!this.CanMoveDate(direction))
            {
                return;
            }

            #endregion

            this.attributesAssigned = true;
            this.SetControlMode(ControlMode.Update);

            if (direction.Equals(Constant.MoveForward))
            {
                this.AssignedTraceabilityMasters.Add(this.selectedAvailableTraceabilityMaster);
                this.AvailableTraceabilityMasters.Remove(this.selectedAvailableTraceabilityMaster);

                return;
            }

            this.AvailableTraceabilityMasters.Add(this.selectedAssignedTraceabilityMaster);
            this.AssignedTraceabilityMasters.Remove(this.selectedAssignedTraceabilityMaster);
        }

        /// <summary>
        /// Determines if a move can be made.
        /// </summary>
        /// <returns>A flag, which indicates whether a move can be made.</returns>
        private bool CanMoveDate(string direction)
        {
            if (direction.Equals(Constant.MoveForward) && this.selectedAvailableTraceabilityMaster == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            if (direction.Equals(Constant.MoveBack) && this.selectedAssignedTraceabilityMaster == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void Refresh()
        {
            this.GetTemplateAllocations();
            this.SetControlMode(ControlMode.OK);
        }

        private IList<AttributeTemplateData> GetTemplates()
        {
            var list = new List<AttributeTemplateData>();
            foreach (var template in this.allTraceabilityTemplateNames)
            {
                list.Add(template);
            }

            return list;
        }

        #endregion
    }
}







