﻿// -----------------------------------------------------------------------
// <copyright file="AttributesViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;

namespace Nouvem.ViewModel.Attribute
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public abstract class AttributesViewModel : NouvemViewModelBase
    {
        #region field

        #region dynamic attribute

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute1;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute2;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute3;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute4;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute5;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute6;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute7;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute8;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute9;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute10;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute11;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute12;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute13;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute14;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute15;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute16;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute17;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute18;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute19;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute20;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute21;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute22;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute23;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute24;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute25;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute26;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute27;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute28;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute29;

        /// <summary>
        /// The dynamic attribute.
        /// </summary>
        private string attribute30;


        private string attribute31;
        private string attribute32;
        private string attribute33;
        private string attribute34;
        private string attribute35;
        private string attribute36;
        private string attribute37;
        private string attribute38;
        private string attribute39;
        private string attribute40;
        private string attribute41;
        private string attribute42;
        private string attribute43;
        private string attribute44;
        private string attribute45;
        private string attribute46;
        private string attribute47;
        private string attribute48;
        private string attribute49;
        private string attribute50;
        private string attribute51;
        private string attribute52;
        private string attribute53;
        private string attribute54;
        private string attribute55;
        private string attribute56;
        private string attribute57;
        private string attribute58;
        private string attribute59;
        private string attribute60;
        private string attribute61;
        private string attribute62;
        private string attribute63;
        private string attribute64;
        private string attribute65;
        private string attribute66;
        private string attribute67;
        private string attribute68;
        private string attribute69;
        private string attribute70;
        private string attribute71;
        private string attribute72;
        private string attribute73;
        private string attribute74;
        private string attribute75;
        private string attribute76;
        private string attribute77;
        private string attribute78;
        private string attribute79;
        private string attribute80;
        private string attribute81;
        private string attribute82;
        private string attribute83;
        private string attribute84;
        private string attribute85;
        private string attribute86;
        private string attribute87;
        private string attribute88;
        private string attribute89;
        private string attribute90;
        private string attribute91;
        private string attribute92;
        private string attribute93;
        private string attribute94;
        private string attribute95;
        private string attribute96;
        private string attribute97;
        private string attribute98;
        private string attribute99;
        private string attribute100;
        private string attribute101;
        private string attribute102;
        private string attribute103;
        private string attribute104;
        private string attribute105;
        private string attribute106;
        private string attribute107;
        private string attribute108;
        private string attribute109;
        private string attribute110;
        private string attribute111;
        private string attribute112;
        private string attribute113;
        private string attribute114;
        private string attribute115;
        private string attribute116;
        private string attribute117;
        private string attribute118;
        private string attribute119;
        private string attribute120;
        private string attribute121;
        private string attribute122;
        private string attribute123;
        private string attribute124;
        private string attribute125;
        private string attribute126;
        private string attribute127;
        private string attribute128;
        private string attribute129;
        private string attribute130;
        private string attribute131;
        private string attribute132;
        private string attribute133;
        private string attribute134;
        private string attribute135;
        private string attribute136;
        private string attribute137;
        private string attribute138;
        private string attribute139;
        private string attribute140;
        private string attribute141;
        private string attribute142;
        private string attribute143;
        private string attribute144;
        private string attribute145;
        private string attribute146;
        private string attribute147;
        private string attribute148;
        private string attribute149;
        private string attribute150;
        private string attribute151;
        private string attribute152;
        private string attribute153;
        private string attribute154;
        private string attribute155;
        private string attribute156;
        private string attribute157;
        private string attribute158;
        private string attribute159;
        private string attribute160;
        private string attribute161;
        private string attribute162;
        private string attribute163;
        private string attribute164;
        private string attribute165;
        private string attribute166;
        private string attribute167;
        private string attribute168;
        private string attribute169;
        private string attribute170;
        private string attribute171;
        private string attribute172;
        private string attribute173;
        private string attribute174;
        private string attribute175;
        private string attribute176;
        private string attribute177;
        private string attribute178;
        private string attribute179;
        private string attribute180;
        private string attribute181;
        private string attribute182;
        private string attribute183;
        private string attribute184;
        private string attribute185;
        private string attribute186;
        private string attribute187;
        private string attribute188;
        private string attribute189;
        private string attribute190;
        private string attribute191;
        private string attribute192;
        private string attribute193;
        private string attribute194;
        private string attribute195;
        private string attribute196;
        private string attribute197;
        private string attribute198;
        private string attribute199;
        private string attribute200;

        private string attribute201;
        private string attribute202;
        private string attribute203;
        private string attribute204;
        private string attribute205;
        private string attribute206;
        private string attribute207;
        private string attribute208;
        private string attribute209;
        private string attribute210;
        private string attribute211;
        private string attribute212;
        private string attribute213;
        private string attribute214;
        private string attribute215;
        private string attribute216;
        private string attribute217;
        private string attribute218;
        private string attribute219;
        private string attribute220;
        private string attribute221;
        private string attribute222;
        private string attribute223;
        private string attribute224;
        private string attribute225;
        private string attribute226;
        private string attribute227;
        private string attribute228;
        private string attribute229;
        private string attribute230;
        private string attribute231;
        private string attribute232;
        private string attribute233;
        private string attribute234;
        private string attribute235;
        private string attribute236;
        private string attribute237;
        private string attribute238;
        private string attribute239;
        private string attribute240;
        private string attribute241;
        private string attribute242;
        private string attribute243;
        private string attribute244;
        private string attribute245;
        private string attribute246;
        private string attribute247;
        private string attribute248;
        private string attribute249;
        private string attribute250;



        #endregion

        /// <summary>
        /// The third party kill flag.
        /// </summary>
        private string thirdParty;

        /// <summary>
        /// The current attributes.
        /// </summary>
        private List<AttributeAllocationData> attributes;

        /// <summary>
        /// The date days.
        /// </summary>
        private IList<DateDay> dateDays = new List<DateDay>();

        /// <summary>
        /// The application customers.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> customers;

        /// <summary>
        /// The intake details.
        /// </summary>
        private ObservableCollection<StockDetail> stockDetails;

        /// <summary>
        /// The application categories.
        /// </summary>
        private ObservableCollection<NouCategory> categories;

        /// <summary>
        /// The system kill types.
        /// </summary>
        private ObservableCollection<NouKillType> killTypes;

        /// <summary>
        /// The application categories.
        /// </summary>
        private ObservableCollection<CollectionData> freezerTypes = new ObservableCollection<CollectionData>();

        /// <summary>
        /// The selected detail.
        /// </summary>
        private StockDetail selectedStockDetail;

        /// <summary>
        /// The customer.
        /// </summary>
        private ViewBusinessPartner customer;

        /// <summary>
        /// The customer.
        /// </summary>
        private ViewBusinessPartner selectedLairageCustomer;

        /// <summary>
        /// The carcass supplier.
        /// </summary>
        private string carcassSupplier;

        /// <summary>
        /// The holding no.
        /// </summary>
        private int? intakeNumber;

        /// <summary>
        /// The holding no.
        /// </summary>
        private bool isWelsh;

        /// <summary>
        /// The holding no.
        /// </summary>
        private bool isContinental;

        /// <summary>
        /// The holding no.
        /// </summary>
        private bool isStandard;

        /// <summary>
        /// The holding no.
        /// </summary>
        private bool isOrganic;

        /// <summary>
        /// Is a select farm intake.
        /// </summary>
        private bool isSelectFarm;

        /// <summary>
        /// The holding no.
        /// </summary>
        private int? freezerIntakeNumber;

        /// <summary>
        /// The holding no.
        /// </summary>
        private string holdingNumber;

        /// <summary>
        /// The movement id.
        /// </summary>
        private string inwardMovementReferenceId;

        /// <summary>
        /// The identigen value.
        /// </summary>
        private string identigen;

        /// <summary>
        /// The carcass supplier.
        /// </summary>
        private string generic1;

        /// <summary>
        /// The animal eartag.
        /// </summary>
        private string farmAssured;

        /// <summary>
        /// The animal eartag.
        /// </summary>
        private string privateKill;

        /// <summary>
        /// The animal eartag.
        /// </summary>
        private string herdFarmAssured;

        /// <summary>
        /// The animal clipped value.
        /// </summary>
        private string clipped;

        /// <summary>
        /// The animal casualty value.
        /// </summary>
        private string casualty;

        /// <summary>
        /// The animal lame value.
        /// </summary>
        private string lame;

        /// <summary>
        /// The animal imported value.
        /// </summary>
        private string imported;

        /// <summary>
        /// The animal sex.
        /// </summary>
        private string sex;

        /// <summary>
        /// The animal category.
        /// </summary>
        private NouCategory selectedCategory;

        /// <summary>
        /// The animal age in months.
        /// </summary>
        private int ageInMonths;

        /// <summary>
        /// The animal age in days.
        /// </summary>
        private int ageInDays;

        /// <summary>
        /// The animal dob.
        /// </summary>
        private DateTime? dob;

        /// <summary>
        /// The animal dob.
        /// </summary>
        private DateTime? lastMoveDate;

        /// <summary>
        /// The animal breed.
        /// </summary>
        private NouBreed selectedBreed;

        /// <summary>
        /// The animal internal grade.
        /// </summary>
        private InternalGrade selectedInternalGrade;

        /// <summary>
        /// The animal cleanliness rating.
        /// </summary>
        private NouCleanliness selectedCleanliness;

        /// <summary>
        /// The animal cleanliness rating.
        /// </summary>
        private CollectionData selectedFreezerType;

        /// <summary>
        /// The animal eartag.
        /// </summary>
        private string eartag;

        /// <summary>
        /// The animal herd no.
        /// </summary>
        private string herdNo;

        /// <summary>
        /// The animal country of origin.
        /// </summary>
        private Model.DataLayer.Country countryOfOrigin;

        /// <summary>
        /// The animal country of origin.
        /// </summary>
        private Model.DataLayer.Country rearedIn;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool condemned;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool isCasualty;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool detained;

        /// <summary>
        /// The current farm residency.
        /// </summary>
        private int daysOfResidencyCurrent;

        /// <summary>
        /// The total farm residency.
        /// </summary>
        private int daysOfResidencyTotal;

        /// <summary>
        /// The animal moves.
        /// </summary>
        private int numberOfMoves;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool condemnedSide2;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool detainedSide2;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool notInsured;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool tbYes;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool burstBelly;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool legMissing;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private bool abscess;

        /// <summary>
        /// The animal halal flag.
        /// </summary>
        private string halal;

        /// <summary>
        /// The animal condemned flag.
        /// </summary>
        private string fatColour;

        /// <summary>
        /// The selected date.
        /// </summary>
        private DateTime selectedCalenderDate;

        /// <summary>
        /// The selected destination.
        /// </summary>
        private string selectedDestination;

        /// <summary>
        /// The selected destination.
        /// </summary>
        private CollectionData selectedKillingType;

        /// <summary>
        /// The selected destination.
        /// </summary>
        private CollectionData selectedDressSpec;

        /// <summary>
        /// The application breeds.
        /// </summary>
        private ObservableCollection<NouBreed> breeds;

        /// <summary>
        /// The application internal grades.
        /// </summary>
        private ObservableCollection<InternalGrade> internalGrades;

        /// <summary>
        /// The application dress specs.
        /// </summary>
        private ObservableCollection<CollectionData> dressSpecs;

        /// <summary>
        /// The application killing types.
        /// </summary>
        private ObservableCollection<CollectionData> killingTypes;

        /// <summary>
        /// Show calender flag.
        /// </summary>
        private bool showCalender;

        /// <summary>
        /// The application fat colours.
        /// </summary>
        private ObservableCollection<NouFatColour> fatColours;

        /// <summary>
        /// The selected fat colour.
        /// </summary>
        private NouFatColour selectedFatColour;

        /// <summary>
        /// Flag, as to whether a category check is to be ignored or not.
        /// </summary>
        protected bool IgnoreCategoryCheck;

        /// <summary>
        /// Flag, as to whether a category check is to be ignored or not.
        /// </summary>
        protected bool IgnoreStockDetailEdit;

        /// <summary>
        /// Are there unsaved attributes.
        /// </summary>
        protected bool UnsavedAttributes;

        /// <summary>
        /// The attribute allocation data.
        /// </summary>
        protected IList<AttributeAllocationData> traceabilityData = new List<AttributeAllocationData>();

        /// <summary>
        /// The attribute allocation data.
        /// </summary>
        protected IList<AttributeAllocationData> traceabilityDataMoves = new List<AttributeAllocationData>();

        /// <summary>
        /// The attribute allocation data.
        /// </summary>
        protected IList<AttributeAllocationData> traceabilityDataTransactions = new List<AttributeAllocationData>();

        /// <summary>
        /// The attribute allocation data.
        /// </summary>
        protected IList<AttributeAllocationData> traceabilityDataPostTransactions = new List<AttributeAllocationData>();

        /// <summary>
        /// The attribute allocation data.
        /// </summary>
        protected IList<AttributeAllocationData> traceabilityDataCompletion = new List<AttributeAllocationData>();

        /// <summary>
        /// The attribute allocation data.
        /// </summary>
        protected IList<AttributeAllocationData> traceabilityDataLabel = new List<AttributeAllocationData>();

        /// <summary>
        /// The attribute non response data.
        /// </summary>
        protected IList<AttributeAllocationData> NonStandardResponses = new List<AttributeAllocationData>();

        /// <summary>
        /// The attribute non response data.
        /// </summary>
        protected IList<AttributeAllocationData> NonStandardBatchResponses = new List<AttributeAllocationData>();

        /// <summary>
        /// The users to alert.
        /// </summary>
        protected AlertUsers AlertUsers = new AlertUsers();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AttributesViewModel"/> class.
        /// </summary>
        public AttributesViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Tuple<int,string>>(this, Token.DeleteTransaction, (t) => this.DeleteTransaction(t.Item1,t.Item2));
            Messenger.Default.Register<int>(this, Token.UndeleteTransaction, this.UndeleteTransaction);

            Messenger.Default.Register<string>(this, Token.SaveBatchAttributes, s =>
            {
                this.SaveBatchAttributes();
            });

            Messenger.Default.Register<AttributeAllocationData>(this, Token.ResponseGiven, a =>
            {
                if (a.AttributionAllocation.Batch == true)
                {
                    this.NonStandardBatchResponses.Add(a);
                }
                else
                {
                    this.NonStandardResponses.Add(a);
                }

                if (ApplicationSettings.SaveBatchAttributesOnEntry)
                {
                    this.SaveBatchAttributes(ignoreChecks: true);
                }
            });

            Messenger.Default.Register<AttributeAllocationData>(this, Token.CheckAttributeValue, this.CheckAttributeValue);

            Messenger.Default.Register<AttributeAllocationData>(this, Token.CheckDates, s =>
            {
                this.CheckBasedOnDates(s);
            });

            #endregion

            #region command


            #endregion
        }

        #endregion

        #region public interface

        #region property

        #region dynamic attribute

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute1
        {
            get
            {
                return this.attribute1;
            }

            set
            {
                this.attribute1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute2
        {
            get
            {
                return this.attribute2;
            }

            set
            {
                this.attribute2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute3
        {
            get
            {
                return this.attribute3;
            }

            set
            {
                this.attribute3 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute4
        {
            get
            {
                return this.attribute4;
            }

            set
            {
                this.attribute4 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute5
        {
            get
            {
                return this.attribute5;
            }

            set
            {
                this.attribute5 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute6
        {
            get
            {
                return this.attribute6;
            }

            set
            {
                this.attribute6 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute7
        {
            get
            {
                return this.attribute7;
            }

            set
            {
                this.attribute7 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute8
        {
            get
            {
                return this.attribute8;
            }

            set
            {
                this.attribute8 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute9
        {
            get
            {
                return this.attribute9;
            }

            set
            {
                this.attribute9 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute10
        {
            get
            {
                return this.attribute10;
            }

            set
            {
                this.attribute10 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute11
        {
            get
            {
                return this.attribute11;
            }

            set
            {
                this.attribute11 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute12
        {
            get
            {
                return this.attribute12;
            }

            set
            {
                this.attribute12 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute13
        {
            get
            {
                return this.attribute13;
            }

            set
            {
                this.attribute13 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute14
        {
            get
            {
                return this.attribute14;
            }

            set
            {
                this.attribute14 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute15
        {
            get
            {
                return this.attribute15;
            }

            set
            {
                this.attribute15 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute16
        {
            get
            {
                return this.attribute16;
            }

            set
            {
                this.attribute16 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute17
        {
            get
            {
                return this.attribute17;
            }

            set
            {
                this.attribute17 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute18
        {
            get
            {
                return this.attribute18;
            }

            set
            {
                this.attribute18 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute19
        {
            get
            {
                return this.attribute19;
            }

            set
            {
                this.attribute19 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute20
        {
            get
            {
                return this.attribute20;
            }

            set
            {
                this.attribute20 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute21
        {
            get
            {
                return this.attribute21;
            }

            set
            {
                this.attribute21 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute22
        {
            get
            {
                return this.attribute22;
            }

            set
            {
                this.attribute22 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute23
        {
            get
            {
                return this.attribute23;
            }

            set
            {
                this.attribute23 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute24
        {
            get
            {
                return this.attribute24;
            }

            set
            {
                this.attribute24 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute25
        {
            get
            {
                return this.attribute25;
            }

            set
            {
                this.attribute25 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute26
        {
            get
            {
                return this.attribute26;
            }

            set
            {
                this.attribute26 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute27
        {
            get
            {
                return this.attribute27;
            }

            set
            {
                this.attribute27 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute28
        {
            get
            {
                return this.attribute28;
            }

            set
            {
                this.attribute28 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute29
        {
            get
            {
                return this.attribute29;
            }

            set
            {
                this.attribute29 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dynamic attribute.
        /// </summary>
        public string Attribute30
        {
            get
            {
                return this.attribute30;
            }

            set
            {
                this.attribute30 = value;
                this.RaisePropertyChanged();
            }
        }

        public string Attribute31 { get { return this.attribute31; } set { this.attribute31 = value; this.RaisePropertyChanged(); } }
        public string Attribute32 { get { return this.attribute32; } set { this.attribute32 = value; this.RaisePropertyChanged(); } }
        public string Attribute33 { get { return this.attribute33; } set { this.attribute33 = value; this.RaisePropertyChanged(); } }
        public string Attribute34 { get { return this.attribute34; } set { this.attribute34 = value; this.RaisePropertyChanged(); } }
        public string Attribute35 { get { return this.attribute35; } set { this.attribute35 = value; this.RaisePropertyChanged(); } }
        public string Attribute36 { get { return this.attribute36; } set { this.attribute36 = value; this.RaisePropertyChanged(); } }
        public string Attribute37 { get { return this.attribute37; } set { this.attribute37 = value; this.RaisePropertyChanged(); } }
        public string Attribute38 { get { return this.attribute38; } set { this.attribute38 = value; this.RaisePropertyChanged(); } }
        public string Attribute39 { get { return this.attribute39; } set { this.attribute39 = value; this.RaisePropertyChanged(); } }
        public string Attribute40 { get { return this.attribute40; } set { this.attribute40 = value; this.RaisePropertyChanged(); } }
        public string Attribute41 { get { return this.attribute41; } set { this.attribute41 = value; this.RaisePropertyChanged(); } }
        public string Attribute42 { get { return this.attribute42; } set { this.attribute42 = value; this.RaisePropertyChanged(); } }
        public string Attribute43 { get { return this.attribute43; } set { this.attribute43 = value; this.RaisePropertyChanged(); } }
        public string Attribute44 { get { return this.attribute44; } set { this.attribute44 = value; this.RaisePropertyChanged(); } }
        public string Attribute45 { get { return this.attribute45; } set { this.attribute45 = value; this.RaisePropertyChanged(); } }
        public string Attribute46 { get { return this.attribute46; } set { this.attribute46 = value; this.RaisePropertyChanged(); } }
        public string Attribute47 { get { return this.attribute47; } set { this.attribute47 = value; this.RaisePropertyChanged(); } }
        public string Attribute48 { get { return this.attribute48; } set { this.attribute48 = value; this.RaisePropertyChanged(); } }
        public string Attribute49 { get { return this.attribute49; } set { this.attribute49 = value; this.RaisePropertyChanged(); } }
        public string Attribute50 { get { return this.attribute50; } set { this.attribute50 = value; this.RaisePropertyChanged(); } }
        public string Attribute51 { get { return this.attribute51; } set { this.attribute51 = value; this.RaisePropertyChanged(); } }
        public string Attribute52 { get { return this.attribute52; } set { this.attribute52 = value; this.RaisePropertyChanged(); } }
        public string Attribute53 { get { return this.attribute53; } set { this.attribute53 = value; this.RaisePropertyChanged(); } }
        public string Attribute54 { get { return this.attribute54; } set { this.attribute54 = value; this.RaisePropertyChanged(); } }
        public string Attribute55 { get { return this.attribute55; } set { this.attribute55 = value; this.RaisePropertyChanged(); } }
        public string Attribute56 { get { return this.attribute56; } set { this.attribute56 = value; this.RaisePropertyChanged(); } }
        public string Attribute57 { get { return this.attribute57; } set { this.attribute57 = value; this.RaisePropertyChanged(); } }
        public string Attribute58 { get { return this.attribute58; } set { this.attribute58 = value; this.RaisePropertyChanged(); } }
        public string Attribute59 { get { return this.attribute59; } set { this.attribute59 = value; this.RaisePropertyChanged(); } }
        public string Attribute60 { get { return this.attribute60; } set { this.attribute60 = value; this.RaisePropertyChanged(); } }
        public string Attribute61 { get { return this.attribute61; } set { this.attribute61 = value; this.RaisePropertyChanged(); } }
        public string Attribute62 { get { return this.attribute62; } set { this.attribute62 = value; this.RaisePropertyChanged(); } }
        public string Attribute63 { get { return this.attribute63; } set { this.attribute63 = value; this.RaisePropertyChanged(); } }
        public string Attribute64 { get { return this.attribute64; } set { this.attribute64 = value; this.RaisePropertyChanged(); } }
        public string Attribute65 { get { return this.attribute65; } set { this.attribute65 = value; this.RaisePropertyChanged(); } }
        public string Attribute66 { get { return this.attribute66; } set { this.attribute66 = value; this.RaisePropertyChanged(); } }
        public string Attribute67 { get { return this.attribute67; } set { this.attribute67 = value; this.RaisePropertyChanged(); } }
        public string Attribute68 { get { return this.attribute68; } set { this.attribute68 = value; this.RaisePropertyChanged(); } }
        public string Attribute69 { get { return this.attribute69; } set { this.attribute69 = value; this.RaisePropertyChanged(); } }
        public string Attribute70 { get { return this.attribute70; } set { this.attribute70 = value; this.RaisePropertyChanged(); } }
        public string Attribute71 { get { return this.attribute71; } set { this.attribute71 = value; this.RaisePropertyChanged(); } }
        public string Attribute72 { get { return this.attribute72; } set { this.attribute72 = value; this.RaisePropertyChanged(); } }
        public string Attribute73 { get { return this.attribute73; } set { this.attribute73 = value; this.RaisePropertyChanged(); } }
        public string Attribute74 { get { return this.attribute74; } set { this.attribute74 = value; this.RaisePropertyChanged(); } }
        public string Attribute75 { get { return this.attribute75; } set { this.attribute75 = value; this.RaisePropertyChanged(); } }
        public string Attribute76 { get { return this.attribute76; } set { this.attribute76 = value; this.RaisePropertyChanged(); } }
        public string Attribute77 { get { return this.attribute77; } set { this.attribute77 = value; this.RaisePropertyChanged(); } }
        public string Attribute78 { get { return this.attribute78; } set { this.attribute78 = value; this.RaisePropertyChanged(); } }
        public string Attribute79 { get { return this.attribute79; } set { this.attribute79 = value; this.RaisePropertyChanged(); } }
        public string Attribute80 { get { return this.attribute80; } set { this.attribute80 = value; this.RaisePropertyChanged(); } }
        public string Attribute81 { get { return this.attribute81; } set { this.attribute81 = value; this.RaisePropertyChanged(); } }
        public string Attribute82 { get { return this.attribute82; } set { this.attribute82 = value; this.RaisePropertyChanged(); } }
        public string Attribute83 { get { return this.attribute83; } set { this.attribute83 = value; this.RaisePropertyChanged(); } }
        public string Attribute84 { get { return this.attribute84; } set { this.attribute84 = value; this.RaisePropertyChanged(); } }
        public string Attribute85 { get { return this.attribute85; } set { this.attribute85 = value; this.RaisePropertyChanged(); } }
        public string Attribute86 { get { return this.attribute86; } set { this.attribute86 = value; this.RaisePropertyChanged(); } }
        public string Attribute87 { get { return this.attribute87; } set { this.attribute87 = value; this.RaisePropertyChanged(); } }
        public string Attribute88 { get { return this.attribute88; } set { this.attribute88 = value; this.RaisePropertyChanged(); } }
        public string Attribute89 { get { return this.attribute89; } set { this.attribute89 = value; this.RaisePropertyChanged(); } }
        public string Attribute90 { get { return this.attribute90; } set { this.attribute90 = value; this.RaisePropertyChanged(); } }
        public string Attribute91 { get { return this.attribute91; } set { this.attribute91 = value; this.RaisePropertyChanged(); } }
        public string Attribute92 { get { return this.attribute92; } set { this.attribute92 = value; this.RaisePropertyChanged(); } }
        public string Attribute93 { get { return this.attribute93; } set { this.attribute93 = value; this.RaisePropertyChanged(); } }
        public string Attribute94 { get { return this.attribute94; } set { this.attribute94 = value; this.RaisePropertyChanged(); } }
        public string Attribute95 { get { return this.attribute95; } set { this.attribute95 = value; this.RaisePropertyChanged(); } }
        public string Attribute96 { get { return this.attribute96; } set { this.attribute96 = value; this.RaisePropertyChanged(); } }
        public string Attribute97 { get { return this.attribute97; } set { this.attribute97 = value; this.RaisePropertyChanged(); } }
        public string Attribute98 { get { return this.attribute98; } set { this.attribute98 = value; this.RaisePropertyChanged(); } }
        public string Attribute99 { get { return this.attribute99; } set { this.attribute99 = value; this.RaisePropertyChanged(); } }
        public string Attribute100 { get { return this.attribute100; } set { this.attribute100 = value; this.RaisePropertyChanged(); } }
        public string Attribute101 { get { return this.attribute101; } set { this.attribute101 = value; this.RaisePropertyChanged(); } }
        public string Attribute102 { get { return this.attribute102; } set { this.attribute102 = value; this.RaisePropertyChanged(); } }
        public string Attribute103 { get { return this.attribute103; } set { this.attribute103 = value; this.RaisePropertyChanged(); } }
        public string Attribute104 { get { return this.attribute104; } set { this.attribute104 = value; this.RaisePropertyChanged(); } }
        public string Attribute105 { get { return this.attribute105; } set { this.attribute105 = value; this.RaisePropertyChanged(); } }
        public string Attribute106 { get { return this.attribute106; } set { this.attribute106 = value; this.RaisePropertyChanged(); } }
        public string Attribute107 { get { return this.attribute107; } set { this.attribute107 = value; this.RaisePropertyChanged(); } }
        public string Attribute108 { get { return this.attribute108; } set { this.attribute108 = value; this.RaisePropertyChanged(); } }
        public string Attribute109 { get { return this.attribute109; } set { this.attribute109 = value; this.RaisePropertyChanged(); } }
        public string Attribute110 { get { return this.attribute110; } set { this.attribute110 = value; this.RaisePropertyChanged(); } }
        public string Attribute111 { get { return this.attribute111; } set { this.attribute111 = value; this.RaisePropertyChanged(); } }
        public string Attribute112 { get { return this.attribute112; } set { this.attribute112 = value; this.RaisePropertyChanged(); } }
        public string Attribute113 { get { return this.attribute113; } set { this.attribute113 = value; this.RaisePropertyChanged(); } }
        public string Attribute114 { get { return this.attribute114; } set { this.attribute114 = value; this.RaisePropertyChanged(); } }
        public string Attribute115 { get { return this.attribute115; } set { this.attribute115 = value; this.RaisePropertyChanged(); } }
        public string Attribute116 { get { return this.attribute116; } set { this.attribute116 = value; this.RaisePropertyChanged(); } }
        public string Attribute117 { get { return this.attribute117; } set { this.attribute117 = value; this.RaisePropertyChanged(); } }
        public string Attribute118 { get { return this.attribute118; } set { this.attribute118 = value; this.RaisePropertyChanged(); } }
        public string Attribute119 { get { return this.attribute119; } set { this.attribute119 = value; this.RaisePropertyChanged(); } }
        public string Attribute120 { get { return this.attribute120; } set
            { this.attribute120 = value;
                this.RaisePropertyChanged(); } }
        public string Attribute121 { get { return this.attribute121; } set { this.attribute121 = value; this.RaisePropertyChanged(); } }
        public string Attribute122 { get { return this.attribute122; } set { this.attribute122 = value; this.RaisePropertyChanged(); } }
        public string Attribute123 { get { return this.attribute123; } set { this.attribute123 = value; this.RaisePropertyChanged(); } }
        public string Attribute124 { get { return this.attribute124; } set { this.attribute124 = value; this.RaisePropertyChanged(); } }
        public string Attribute125 { get { return this.attribute125; } set { this.attribute125 = value; this.RaisePropertyChanged(); } }
        public string Attribute126 { get { return this.attribute126; } set { this.attribute126 = value; this.RaisePropertyChanged(); } }
        public string Attribute127 { get { return this.attribute127; } set { this.attribute127 = value; this.RaisePropertyChanged(); } }
        public string Attribute128 { get { return this.attribute128; } set { this.attribute128 = value; this.RaisePropertyChanged(); } }
        public string Attribute129 { get { return this.attribute129; } set { this.attribute129 = value; this.RaisePropertyChanged(); } }
        public string Attribute130 { get { return this.attribute130; } set { this.attribute130 = value; this.RaisePropertyChanged(); } }
        public string Attribute131 { get { return this.attribute131; } set { this.attribute131 = value; this.RaisePropertyChanged(); } }
        public string Attribute132 { get { return this.attribute132; } set { this.attribute132 = value; this.RaisePropertyChanged(); } }
        public string Attribute133 { get { return this.attribute133; } set { this.attribute133 = value; this.RaisePropertyChanged(); } }
        public string Attribute134 { get { return this.attribute134; } set { this.attribute134 = value; this.RaisePropertyChanged(); } }
        public string Attribute135 { get { return this.attribute135; } set { this.attribute135 = value; this.RaisePropertyChanged(); } }
        public string Attribute136 { get { return this.attribute136; } set { this.attribute136 = value; this.RaisePropertyChanged(); } }
        public string Attribute137 { get { return this.attribute137; } set { this.attribute137 = value; this.RaisePropertyChanged(); } }
        public string Attribute138 { get { return this.attribute138; } set { this.attribute138 = value; this.RaisePropertyChanged(); } }
        public string Attribute139 { get { return this.attribute139; } set { this.attribute139 = value; this.RaisePropertyChanged(); } }
        public string Attribute140 { get { return this.attribute140; } set { this.attribute140 = value; this.RaisePropertyChanged(); } }
        public string Attribute141 { get { return this.attribute141; } set { this.attribute141 = value; this.RaisePropertyChanged(); } }
        public string Attribute142 { get { return this.attribute142; } set { this.attribute142 = value; this.RaisePropertyChanged(); } }
        public string Attribute143 { get { return this.attribute143; } set { this.attribute143 = value; this.RaisePropertyChanged(); } }
        public string Attribute144 { get { return this.attribute144; } set { this.attribute144 = value; this.RaisePropertyChanged(); } }
        public string Attribute145 { get { return this.attribute145; } set { this.attribute145 = value; this.RaisePropertyChanged(); } }
        public string Attribute146 { get { return this.attribute146; } set { this.attribute146 = value; this.RaisePropertyChanged(); } }
        public string Attribute147 { get { return this.attribute147; } set { this.attribute147 = value; this.RaisePropertyChanged(); } }
        public string Attribute148 { get { return this.attribute148; } set { this.attribute148 = value; this.RaisePropertyChanged(); } }
        public string Attribute149 { get { return this.attribute149; } set { this.attribute149 = value; this.RaisePropertyChanged(); } }
        public string Attribute150 { get { return this.attribute150; } set { this.attribute150 = value; this.RaisePropertyChanged(); } }
        public string Attribute151 { get { return this.attribute151; } set { this.attribute151 = value; this.RaisePropertyChanged(); } }
        public string Attribute152 { get { return this.attribute152; } set { this.attribute152 = value; this.RaisePropertyChanged(); } }
        public string Attribute153 { get { return this.attribute153; } set { this.attribute153 = value; this.RaisePropertyChanged(); } }
        public string Attribute154 { get { return this.attribute154; } set { this.attribute154 = value; this.RaisePropertyChanged(); } }
        public string Attribute155 { get { return this.attribute155; } set { this.attribute155 = value; this.RaisePropertyChanged(); } }
        public string Attribute156 { get { return this.attribute156; } set { this.attribute156 = value; this.RaisePropertyChanged(); } }
        public string Attribute157 { get { return this.attribute157; } set { this.attribute157 = value; this.RaisePropertyChanged(); } }
        public string Attribute158 { get { return this.attribute158; } set { this.attribute158 = value; this.RaisePropertyChanged(); } }
        public string Attribute159 { get { return this.attribute159; } set { this.attribute159 = value; this.RaisePropertyChanged(); } }
        public string Attribute160 { get { return this.attribute160; } set { this.attribute160 = value; this.RaisePropertyChanged(); } }
        public string Attribute161 { get { return this.attribute161; } set { this.attribute161 = value; this.RaisePropertyChanged(); } }
        public string Attribute162 { get { return this.attribute162; } set { this.attribute162 = value; this.RaisePropertyChanged(); } }
        public string Attribute163 { get { return this.attribute163; } set { this.attribute163 = value; this.RaisePropertyChanged(); } }
        public string Attribute164 { get { return this.attribute164; } set { this.attribute164 = value; this.RaisePropertyChanged(); } }
        public string Attribute165 { get { return this.attribute165; } set { this.attribute165 = value; this.RaisePropertyChanged(); } }
        public string Attribute166 { get { return this.attribute166; } set { this.attribute166 = value; this.RaisePropertyChanged(); } }
        public string Attribute167 { get { return this.attribute167; } set { this.attribute167 = value; this.RaisePropertyChanged(); } }
        public string Attribute168 { get { return this.attribute168; } set { this.attribute168 = value; this.RaisePropertyChanged(); } }
        public string Attribute169 { get { return this.attribute169; } set { this.attribute169 = value; this.RaisePropertyChanged(); } }
        public string Attribute170 { get { return this.attribute170; } set { this.attribute170 = value; this.RaisePropertyChanged(); } }
        public string Attribute171 { get { return this.attribute171; } set { this.attribute171 = value; this.RaisePropertyChanged(); } }
        public string Attribute172 { get { return this.attribute172; } set { this.attribute172 = value; this.RaisePropertyChanged(); } }
        public string Attribute173 { get { return this.attribute173; } set { this.attribute173 = value; this.RaisePropertyChanged(); } }
        public string Attribute174 { get { return this.attribute174; } set { this.attribute174 = value; this.RaisePropertyChanged(); } }
        public string Attribute175 { get { return this.attribute175; } set { this.attribute175 = value; this.RaisePropertyChanged(); } }
        public string Attribute176 { get { return this.attribute176; } set { this.attribute176 = value; this.RaisePropertyChanged(); } }
        public string Attribute177 { get { return this.attribute177; } set { this.attribute177 = value; this.RaisePropertyChanged(); } }
        public string Attribute178 { get { return this.attribute178; } set { this.attribute178 = value; this.RaisePropertyChanged(); } }
        public string Attribute179 { get { return this.attribute179; } set { this.attribute179 = value; this.RaisePropertyChanged(); } }
        public string Attribute180 { get { return this.attribute180; } set { this.attribute180 = value; this.RaisePropertyChanged(); } }
        public string Attribute181 { get { return this.attribute181; } set { this.attribute181 = value; this.RaisePropertyChanged(); } }
        public string Attribute182 { get { return this.attribute182; } set { this.attribute182 = value; this.RaisePropertyChanged(); } }
        public string Attribute183 { get { return this.attribute183; } set { this.attribute183 = value; this.RaisePropertyChanged(); } }
        public string Attribute184 { get { return this.attribute184; } set { this.attribute184 = value; this.RaisePropertyChanged(); } }
        public string Attribute185 { get { return this.attribute185; } set { this.attribute185 = value; this.RaisePropertyChanged(); } }
        public string Attribute186 { get { return this.attribute186; } set { this.attribute186 = value; this.RaisePropertyChanged(); } }
        public string Attribute187 { get { return this.attribute187; } set { this.attribute187 = value; this.RaisePropertyChanged(); } }
        public string Attribute188 { get { return this.attribute188; } set { this.attribute188 = value; this.RaisePropertyChanged(); } }
        public string Attribute189 { get { return this.attribute189; } set { this.attribute189 = value; this.RaisePropertyChanged(); } }
        public string Attribute190 { get { return this.attribute190; } set { this.attribute190 = value; this.RaisePropertyChanged(); } }
        public string Attribute191 { get { return this.attribute191; } set { this.attribute191 = value; this.RaisePropertyChanged(); } }
        public string Attribute192 { get { return this.attribute192; } set { this.attribute192 = value; this.RaisePropertyChanged(); } }
        public string Attribute193 { get { return this.attribute193; } set { this.attribute193 = value; this.RaisePropertyChanged(); } }
        public string Attribute194 { get { return this.attribute194; } set { this.attribute194 = value; this.RaisePropertyChanged(); } }
        public string Attribute195 { get { return this.attribute195; } set { this.attribute195 = value; this.RaisePropertyChanged(); } }
        public string Attribute196 { get { return this.attribute196; } set { this.attribute196 = value; this.RaisePropertyChanged(); } }
        public string Attribute197 { get { return this.attribute197; } set { this.attribute197 = value; this.RaisePropertyChanged(); } }
        public string Attribute198 { get { return this.attribute198; } set { this.attribute198 = value; this.RaisePropertyChanged(); } }
        public string Attribute199 { get { return this.attribute199; } set { this.attribute199 = value; this.RaisePropertyChanged(); } }
        public string Attribute200 { get { return this.attribute200; } set { this.attribute200 = value; this.RaisePropertyChanged(); } }
        public string Attribute201 { get { return this.attribute201; } set { this.attribute201 = value; this.RaisePropertyChanged(); } }
        public string Attribute202 { get { return this.attribute202; } set { this.attribute202 = value; this.RaisePropertyChanged(); } }
        public string Attribute203 { get { return this.attribute203; } set { this.attribute203 = value; this.RaisePropertyChanged(); } }
        public string Attribute204 { get { return this.attribute204; } set { this.attribute204 = value; this.RaisePropertyChanged(); } }
        public string Attribute205 { get { return this.attribute205; } set { this.attribute205 = value; this.RaisePropertyChanged(); } }
        public string Attribute206 { get { return this.attribute206; } set { this.attribute206 = value; this.RaisePropertyChanged(); } }
        public string Attribute207 { get { return this.attribute207; } set { this.attribute207 = value; this.RaisePropertyChanged(); } }
        public string Attribute208 { get { return this.attribute208; } set { this.attribute208 = value; this.RaisePropertyChanged(); } }
        public string Attribute209 { get { return this.attribute209; } set { this.attribute209 = value; this.RaisePropertyChanged(); } }
        public string Attribute210 { get { return this.attribute210; } set { this.attribute210 = value; this.RaisePropertyChanged(); } }
        public string Attribute211 { get { return this.attribute211; } set { this.attribute211 = value; this.RaisePropertyChanged(); } }
        public string Attribute212 { get { return this.attribute212; } set { this.attribute212 = value; this.RaisePropertyChanged(); } }
        public string Attribute213 { get { return this.attribute213; } set { this.attribute213 = value; this.RaisePropertyChanged(); } }
        public string Attribute214 { get { return this.attribute214; } set { this.attribute214 = value; this.RaisePropertyChanged(); } }
        public string Attribute215 { get { return this.attribute215; } set { this.attribute215 = value; this.RaisePropertyChanged(); } }
        public string Attribute216 { get { return this.attribute216; } set { this.attribute216 = value; this.RaisePropertyChanged(); } }
        public string Attribute217 { get { return this.attribute217; } set { this.attribute217 = value; this.RaisePropertyChanged(); } }
        public string Attribute218 { get { return this.attribute218; } set { this.attribute218 = value; this.RaisePropertyChanged(); } }
        public string Attribute219 { get { return this.attribute219; } set { this.attribute219 = value; this.RaisePropertyChanged(); } }
        public string Attribute220 { get { return this.attribute220; } set { this.attribute220 = value; this.RaisePropertyChanged(); } }
        public string Attribute221 { get { return this.attribute221; } set { this.attribute221 = value; this.RaisePropertyChanged(); } }
        public string Attribute222 { get { return this.attribute222; } set { this.attribute222 = value; this.RaisePropertyChanged(); } }
        public string Attribute223 { get { return this.attribute223; } set { this.attribute223 = value; this.RaisePropertyChanged(); } }
        public string Attribute224 { get { return this.attribute224; } set { this.attribute224 = value; this.RaisePropertyChanged(); } }
        public string Attribute225 { get { return this.attribute225; } set { this.attribute225 = value; this.RaisePropertyChanged(); } }
        public string Attribute226 { get { return this.attribute226; } set { this.attribute226 = value; this.RaisePropertyChanged(); } }
        public string Attribute227 { get { return this.attribute227; } set { this.attribute227 = value; this.RaisePropertyChanged(); } }
        public string Attribute228 { get { return this.attribute228; } set { this.attribute228 = value; this.RaisePropertyChanged(); } }
        public string Attribute229 { get { return this.attribute229; } set { this.attribute229 = value; this.RaisePropertyChanged(); } }
        public string Attribute230 { get { return this.attribute230; } set { this.attribute230 = value; this.RaisePropertyChanged(); } }
        public string Attribute231 { get { return this.attribute231; } set { this.attribute231 = value; this.RaisePropertyChanged(); } }
        public string Attribute232 { get { return this.attribute232; } set { this.attribute232 = value; this.RaisePropertyChanged(); } }
        public string Attribute233 { get { return this.attribute233; } set { this.attribute233 = value; this.RaisePropertyChanged(); } }
        public string Attribute234 { get { return this.attribute234; } set { this.attribute234 = value; this.RaisePropertyChanged(); } }
        public string Attribute235 { get { return this.attribute235; } set { this.attribute235 = value; this.RaisePropertyChanged(); } }
        public string Attribute236 { get { return this.attribute236; } set { this.attribute236 = value; this.RaisePropertyChanged(); } }
        public string Attribute237 { get { return this.attribute237; } set { this.attribute237 = value; this.RaisePropertyChanged(); } }
        public string Attribute238 { get { return this.attribute238; } set { this.attribute238 = value; this.RaisePropertyChanged(); } }
        public string Attribute239 { get { return this.attribute239; } set { this.attribute239 = value; this.RaisePropertyChanged(); } }
        public string Attribute240 { get { return this.attribute240; } set { this.attribute240 = value; this.RaisePropertyChanged(); } }
        public string Attribute241 { get { return this.attribute241; } set { this.attribute241 = value; this.RaisePropertyChanged(); } }
        public string Attribute242 { get { return this.attribute242; } set { this.attribute242 = value; this.RaisePropertyChanged(); } }
        public string Attribute243 { get { return this.attribute243; } set { this.attribute243 = value; this.RaisePropertyChanged(); } }
        public string Attribute244 { get { return this.attribute244; } set { this.attribute244 = value; this.RaisePropertyChanged(); } }
        public string Attribute245 { get { return this.attribute245; } set { this.attribute245 = value; this.RaisePropertyChanged(); } }
        public string Attribute246 { get { return this.attribute246; } set { this.attribute246 = value; this.RaisePropertyChanged(); } }
        public string Attribute247 { get { return this.attribute247; } set { this.attribute247 = value; this.RaisePropertyChanged(); } }
        public string Attribute248 { get { return this.attribute248; } set { this.attribute248 = value; this.RaisePropertyChanged(); } }
        public string Attribute249 { get { return this.attribute249; } set { this.attribute249 = value; this.RaisePropertyChanged(); } }
        public string Attribute250 { get { return this.attribute250; } set { this.attribute250 = value; this.RaisePropertyChanged(); } }


        #endregion


        /// <summary>
        /// Gets or sets the holding number.
        /// </summary>
        public string ThirdParty
        {
            get
            {
                return this.thirdParty;
            }

            set
            {
                this.thirdParty = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the calender is shown.
        /// </summary>
        public bool IsSelectFarm
        {
            get
            {
                return this.isSelectFarm;
            }

            set
            {
                this.SetMode(value, this.isSelectFarm);
                this.isSelectFarm = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the calender is shown.
        /// </summary>
        public bool IsWelsh
        {
            get
            {
                return this.isWelsh;
            }

            set
            {
                this.isWelsh = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the calender is shown.
        /// </summary>
        public bool IsContinental
        {
            get
            {
                return this.isContinental;
            }

            set
            {
                this.isContinental = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the calender is shown.
        /// </summary>
        public bool IsStandard
        {
            get
            {
                return this.isStandard;
            }

            set
            {
                this.isStandard = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the calender is shown.
        /// </summary>
        public bool IsOrganic
        {
            get
            {
                return this.isOrganic;
            }

            set
            {
                this.isOrganic = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected date.
        /// </summary>
        public DateTime SelectedCalenderDate
        {
            get
            {
                return this.selectedCalenderDate;
            }

            set
            {
                this.selectedCalenderDate = value;
                this.RaisePropertyChanged();
                this.HandleSelectedCalenderDate();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the calender is shown.
        /// </summary>
        public bool ShowCalender
        {
            get
            {
                return this.showCalender;
            }

            set
            {
                this.showCalender = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is condemned or not.
        /// </summary>
        public bool Condemned
        {
            get
            {
                return this.condemned;
            }

            set
            {
                this.condemned = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is detained or not.
        /// </summary>
        public bool Detained
        {
            get
            {
                return this.detained;
            }

            set
            {
                this.detained = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is condemned or not.
        /// </summary>
        public bool CondemnedSide2
        {
            get
            {
                return this.condemnedSide2;
            }

            set
            {
                this.condemnedSide2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is detained or not.
        /// </summary>
        public bool DetainedSide2
        {
            get
            {
                return this.detainedSide2;
            }

            set
            {
                this.detainedSide2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is isured or not.
        /// </summary>
        public bool NotInsured
        {
            get
            {
                return this.notInsured;
            }

            set
            {
                this.notInsured = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is tb yes or not.
        /// </summary>
        public bool TBYes
        {
            get
            {
                return this.tbYes;
            }

            set
            {
                this.tbYes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is condemned or not.
        /// </summary>
        public bool LegMissing
        {
            get
            {
                return this.legMissing;
            }

            set
            {
                this.legMissing = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is condemned or not.
        /// </summary>
        public bool Abscess
        {
            get
            {
                return this.abscess;
            }

            set
            {
                this.abscess = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is halal or not.
        /// </summary>
        public string Halal
        {
            get
            {
                return this.halal;
            }

            set
            {
                this.halal = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the carcass is condemned or not.
        /// </summary>
        public bool BurstBelly
        {
            get
            {
                return this.burstBelly;
            }

            set
            {
                this.burstBelly = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current days residency.
        /// </summary>
        public int DaysOfResidencyCurrent
        {
            get
            {
                return this.daysOfResidencyCurrent;
            }

            set
            {
                this.daysOfResidencyCurrent = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the total days residency.
        /// </summary>
        public int DaysOfResidencyTotal
        {
            get
            {
                return this.daysOfResidencyTotal;
            }

            set
            {
                this.daysOfResidencyTotal = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the number of valid animal moves.
        /// </summary>
        public int NumberOfMoves
        {
            get
            {
                return this.numberOfMoves;
            }

            set
            {
                this.numberOfMoves = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the carcass fat colour.
        /// </summary>
        public string FatColour
        {
            get
            {
                return this.fatColour;
            }

            set
            {
                this.fatColour = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the identigen bottle number.
        /// </summary>
        public string Identigen
        {
            get
            {
                return this.identigen;
            }

            set
            {
                this.identigen = value;
                this.RaisePropertyChanged();
            }
        }

        public string MoveToLairageReference { get; set; }

        /// <summary>
        /// Gets or sets the holding number.
        /// </summary>
        public string InwardMovementReferenceId
        {
            get
            {
                return this.inwardMovementReferenceId;
            }

            set
            {
                this.inwardMovementReferenceId = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the holding number.
        /// </summary>
        public string HoldingNumber
        {
            get
            {
                return this.holdingNumber;
            }

            set
            {
                this.holdingNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the holding number.
        /// </summary>
        public int? IntakeNumber
        {
            get
            {
                return this.intakeNumber;
            }

            set
            {
                this.intakeNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the holding number.
        /// </summary>
        public int? FreezerIntakeNumber
        {
            get
            {
                return this.freezerIntakeNumber;
            }

            set
            {
                this.freezerIntakeNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the carcass supplier.
        /// </summary>
        public string CarcassSupplier
        {
            get
            {
                return this.carcassSupplier;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = Strings.NoSupplier;
                }

                this.carcassSupplier = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected stock detail.
        /// </summary>
        public StockDetail SelectedStockDetail
        {
            get
            {
                return this.selectedStockDetail;
            }

            set
            {
                this.selectedStockDetail = value;
                this.RaisePropertyChanged();

                if (value != null && !this.IgnoreStockDetailEdit)
                {
                    this.HandleSelectedStockDetail(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the system kill types.
        /// </summary>
        public ObservableCollection<StockDetail> StockDetails
        {
            get
            {
                return this.stockDetails;
            }

            set
            {
                this.stockDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the cleanliness values.
        /// </summary>
        public IList<NouCleanliness> Cleanlinesses { get; set; }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        public ObservableCollection<CollectionData> FreezerTypes
        {
            get
            {
                return this.freezerTypes;
            }

            set
            {
                this.freezerTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        public CollectionData SelectedFreezerType
        {
            get
            {
                return this.selectedFreezerType;
            }

            set
            {
                this.selectedFreezerType = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the system kill types.
        /// </summary>
        public ObservableCollection<NouKillType> KillTypes
        {
            get
            {
                return this.killTypes;
            }

            set
            {
                this.killTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        public ObservableCollection<NouCategory> Categories
        {
            get
            {
                return this.categories;
            }

            set
            {
                this.categories = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the sexes.
        /// </summary>
        public IList<string> Sexes { get; set; }

        /// <summary>
        /// Gets or sets the yes/no values.
        /// </summary>
        public IList<string> YesNoValues { get; set; }

        /// <summary>
        /// Gets or sets the destinations.
        /// </summary>
        public IList<string> Destinations { get; set; }

        /// <summary>
        /// Gets or sets the look ups.
        /// </summary>
        public IList<AttributeLookUpData> AttributeLookUps { get; set; }

        /// <summary>
        /// Gets or sets the selected destination.
        /// </summary>
        public string SelectedDestination
        {
            get
            {
                return this.selectedDestination;
            }

            set
            {
                this.selectedDestination = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the fat colours.
        /// </summary>
        public ObservableCollection<NouFatColour> FatColours
        {
            get
            {
                return this.fatColours;
            }

            set
            {
                this.fatColours = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected fat colour.
        /// </summary>
        public NouFatColour SelectedFatColour
        {
            get
            {
                return this.selectedFatColour;
            }

            set
            {
                this.selectedFatColour = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the breeds.
        /// </summary>
        public ObservableCollection<NouBreed> Breeds
        {
            get
            {
                return this.breeds;
            }

            set
            {
                this.breeds = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the internal grades.
        /// </summary>
        public ObservableCollection<InternalGrade> InternalGrades
        {
            get
            {
                return this.internalGrades;
            }

            set
            {
                this.internalGrades = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dress specs.
        /// </summary>
        public CollectionData SelectedDressSpec
        {
            get
            {
                return this.selectedDressSpec;
            }

            set
            {
                this.selectedDressSpec = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dress specs.
        /// </summary>
        public CollectionData SelectedKillingType
        {
            get
            {
                return this.selectedKillingType;
            }

            set
            {
                this.selectedKillingType = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dress specs.
        /// </summary>
        public ObservableCollection<CollectionData> DressSpecs
        {
            get
            {
                return this.dressSpecs;
            }

            set
            {
                this.dressSpecs = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the breeds.
        /// </summary>
        public ObservableCollection<CollectionData> KillingTypes
        {
            get
            {
                return this.killingTypes;
            }

            set
            {
                this.killingTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public ObservableCollection<ViewBusinessPartner> Customers
        {
            get
            {
                return this.customers;
            }

            set
            {
                this.customers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Eartag
        {
            get
            {
                return this.eartag;
            }

            set
            {
                this.eartag = value;
                this.RaisePropertyChanged();
                Messenger.Default.Send(Token.Message, Token.FocusToHerdNo);
                this.HandleEartagEntry();
            }
        }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public Model.DataLayer.Country CountryOfOrigin
        {
            get
            {
                return this.countryOfOrigin;
            }

            set
            {
                this.countryOfOrigin = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application countries.
        /// </summary>
        public IList<Model.DataLayer.Country> Countries { get; set; }

        /// <summary>
        /// Gets or sets the application plants.
        /// </summary>
        public IList<Model.DataLayer.Plant> Plants { get; set; }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public Model.DataLayer.Country RearedIn
        {
            get
            {
                return this.rearedIn;
            }

            set
            {
                this.rearedIn = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string HerdNo
        {
            get
            {
                return this.herdNo;
            }

            set
            {
                this.herdNo = value;
                this.RaisePropertyChanged();
                if (!string.IsNullOrEmpty(value) && value.Length >= 10 && value.Substring(0, 8).IsNumericSequence())
                {
                    // scanned passport barcode data
                    this.HandleScannedPassportData(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the breed.
        /// </summary>
        public NouBreed Breed
        {
            get
            {
                return this.selectedBreed;
            }

            set
            {
                this.selectedBreed = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the internal grade.
        /// </summary>
        public InternalGrade SelectedInternalGrade
        {
            get
            {
                return this.selectedInternalGrade;
            }

            set
            {
                this.selectedInternalGrade = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the cleanliness rating.
        /// </summary>
        public NouCleanliness SelectedCleanliness
        {
            get
            {
                return this.selectedCleanliness;
            }

            set
            {
                this.selectedCleanliness = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dob.
        /// </summary>
        public DateTime? LastMoveDate
        {
            get
            {
                return this.lastMoveDate;
            }

            set
            {
                this.lastMoveDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dob.
        /// </summary>
        public DateTime? DOB
        {
            get
            {
                return this.dob;
            }

            set
            {
                this.dob = value;
                this.RaisePropertyChanged();
                this.AgeInMonths = value.ToDate().DateDifferenceInMonths(DateTime.Today);
                this.AgeInDays = value.ToDate().DateDifferenceInCurrentMonthDays(DateTime.Today);
            }
        }

        /// <summary>
        /// Gets or sets the age in months.
        /// </summary>
        public int AgeInMonths
        {
            get
            {
                return this.ageInMonths;
            }

            set
            {
                this.ageInMonths = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the age in days.
        /// </summary>
        public int AgeInDays
        {
            get
            {
                return this.ageInDays;
            }

            set
            {
                this.ageInDays = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the previously selected category.
        /// </summary>
        public NouCategory PreviousCategory { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public NouCategory SelectedCategory
        {
            get
            {
                return this.selectedCategory;
            }

            set
            {
                this.selectedCategory = value;
                if (value != null && !this.IgnoreCategoryCheck)
                {
                    this.HandleCategorySelection(value);
                }

                this.RaisePropertyChanged();
                this.PreviousCategory = value;
            }
        }

        /// <summary>
        /// Gets or sets the animal sex.
        /// </summary>
        public string Sex
        {
            get
            {
                return this.sex;
            }

            set
            {
                this.sex = value;
                this.RaisePropertyChanged();
                this.HandleSexSelection();
            }
        }

        /// <summary>
        /// Gets or sets the farm assured flag.
        /// </summary>
        public string HerdFarmAssured
        {
            get
            {
                return this.herdFarmAssured;
            }

            set
            {
                this.herdFarmAssured = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the destinations.
        /// </summary>
        public IList<Destination> DestinationsFull { get; set; }

        /// <summary>
        /// Gets or sets the generic 1 value.
        /// </summary>
        public string Generic1
        {
            get
            {
                return this.generic1;
            }

            set
            {
                var localValue = this.generic1;
                this.generic1 = value;
                this.RaisePropertyChanged();
                this.HandleGeneric1(localValue);
            }
        }

        /// <summary>
        /// Gets or sets the farm assured flag.
        /// </summary>
        public string FarmAssured
        {
            get
            {
                return this.farmAssured;
            }

            set
            {
                this.farmAssured = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the farm assured flag.
        /// </summary>
        public string PrivateKill
        {
            get
            {
                return this.privateKill;
            }

            set
            {
                this.privateKill = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the farm assured flag.
        /// </summary>
        public string Clipped
        {
            get
            {
                return this.clipped;
            }

            set
            {
                this.clipped = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the farm assured flag.
        /// </summary>
        public bool IsCasualty
        {
            get
            {
                return this.isCasualty;
            }

            set
            {
                this.isCasualty = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the farm assured flag.
        /// </summary>
        public string Casualty
        {
            get
            {
                return this.casualty;
            }

            set
            {
                this.casualty = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the farm assured flag.
        /// </summary>
        public string Lame
        {
            get
            {
                return this.lame;
            }

            set
            {
                this.lame = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the animal imported flag.
        /// </summary>
        public string Imported
        {
            get
            {
                return this.imported;
            }

            set
            {
                this.imported = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        public ViewBusinessPartner SelectedLairageCustomer
        {
            get
            {
                return this.selectedLairageCustomer;
            }

            set
            {
                this.selectedLairageCustomer = value;
                this.RaisePropertyChanged();
                this.HandleSelectedLairageCustomer();
            }
        }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        public ViewBusinessPartner SelectedCustomer
        {
            get
            {
                return this.customer;
            }

            set
            {
                this.customer = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to save the batch attributes.
        /// </summary>
        public ICommand SaveBatchAttributeCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Gets the destinations.
        /// </summary>
        protected void GetDestinationsFull()
        {
            this.DestinationsFull = this.DataManager.GetDestinations();
        }
        
        protected virtual void HandleGeneric1(string oldValue) { }
        protected virtual void DeleteTransaction (int serial, string barcode = "") { }
        protected virtual void UndeleteTransaction(int serial) { }
        protected virtual void HandleSelectedLairageCustomer() { }

        /// <summary>
        /// Saves the batch attributes.
        /// </summary>
        protected virtual void SaveBatchAttributes(int? parentId = null, bool ignoreChecks = false)
        {
            if (this.NonStandardBatchResponses != null && this.NonStandardBatchResponses.Any())
            {
                var nonStandardResponses = new List<Model.DataLayer.Attribute>();
                foreach (var attributeAllocationData in this.NonStandardBatchResponses)
                {
                    nonStandardResponses.Add(new Model.DataLayer.Attribute
                    {
                        Attribute1 = attributeAllocationData.TraceabilityValueNonStandard,
                        AttributeID_Parent = parentId,
                        AttributeMasterID = attributeAllocationData.AttributeMaster.AttributeMasterID
                    });
                }

                this.NonStandardBatchResponses.Clear();
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.DataManager.AddAttributes(nonStandardResponses);
                }));
            }
        }

        /// <summary>
        /// Gets the date days.
        /// </summary>
        protected void GetDressSpecs()
        {
            this.DressSpecs = new ObservableCollection<CollectionData>(this.DataManager.GetDressSpecs());
        }

        /// <summary>
        /// Gets the date days.
        /// </summary>
        protected void GetKillingTypes()
        {
            this.KillingTypes = new ObservableCollection<CollectionData>(this.DataManager.GetKillingTypes());
        }

        /// <summary>
        /// Gets the date days.
        /// </summary>
        protected void GetDateDays()
        {
            this.dateDays = this.DataManager.GetDateDays();
        }

        /// <summary>
        /// Gets the attribute data.
        /// </summary>
        protected void GetAttributeAllocationData()
        {
            this.traceabilityData = NouvemGlobal.AttributeAllocationData; // this.DataManager.GetAttributeAllocations();
        }

        /// <summary>
        /// Gets the attribute data.
        /// </summary>
        protected void CheckForUnsavedAttributes()
        {
            if (!ApplicationSettings.CheckForUnsavedBatchAttributes)
            {
                // no check required.
                return;
            }

            if (this.UnsavedAttributes)
            {
                NouvemMessageBox.Show(Message.UnsavedAttributes, touchScreen: true);
            }
        }

        /// <summary>
        /// Handle the incoming sale detail.
        /// </summary>
        protected virtual void HandleSaleDetail(SaleDetail saleDetail, NouvemViewModelBase bindingVM)
        {
            #region insurance

            if (saleDetail == null)
            {
                return;
            }

            if (saleDetail.InventoryItem == null)
            {
                saleDetail.InventoryItem =
                    NouvemGlobal.InventoryItems.FirstOrDefault(
                        x => x.Master.INMasterID == saleDetail.INMasterID);

                if (saleDetail.InventoryItem == null)
                {
                    // should never happen, but just in case.
                    this.Log.LogDebug(this.GetType(), string.Format("HandlesaleDetail():{0}", Message.InventoryItemNotFound));
                    //SystemMessage.Write(MessageType.Issue, Message.InventoryItemNotFound);
                    return;
                }
            }

            #endregion

            var item = saleDetail.InventoryItem.Master;
            this.attributes =
                this.traceabilityData.Where(x => x.AttributionAllocation.AttributeTemplateID == item.AttributeTemplateID).ToList();

            var localAttributeData = this.attributes.Where(x => x.AttributeEditProcesses == null).ToList();
            if (localAttributeData.Any())
            {
                this.DataManager.GetAttributeAllocations(localAttributeData);
            }

            var localAttributePermissionData = this.attributes.Where(x => !x.UserCanAdd.HasValue).ToList();
            if (localAttributePermissionData.Any())
            {
                this.DataManager.GetAttributeAllocationPermissions(localAttributePermissionData, NouvemGlobal.UserId.ToInt(), NouvemGlobal.LoggedInUser.UserMaster.UserGroupID);
            }

            foreach (var attributeAllocationData in this.attributes.OrderBy(x => x.AttributeMaster.AttributeMasterID_BasedOn))
            {
                attributeAllocationData.PropertyInfo = this.GetProperty(attributeAllocationData.AttributeMaster);
                attributeAllocationData.BindingViewModel = bindingVM;
                attributeAllocationData.INMasterID = item.INMasterID;
                var processData = this.SetProcessData(attributeAllocationData);
                attributeAllocationData.IsVisible = processData.Item1;
                attributeAllocationData.IsEditable = processData.Item2;

                #region dates

                if (attributeAllocationData.TraceabilityType.CompareIgnoringCase(Constant.Calender))
                {
                    this.SetDates(attributeAllocationData, this.attributes, item.INMasterID);
                }

                #endregion

                #region default value

                if (!string.IsNullOrEmpty(attributeAllocationData.AttributionAllocation.DefaultValueSQL))
                {
                    var scriptData = this.DataManager.GetData(attributeAllocationData.AttributionAllocation.DefaultValueSQL).ToList().Select(x => x.ToString()).ToList();
                    var defaultValue = scriptData.FirstOrDefault();
                    if (defaultValue != null)
                    {
                        this.SetAttributeValue(attributeAllocationData.AttributeMaster, defaultValue);
                    }
                }

                var loadSp = attributeAllocationData.AttributeMaster.LoadMACRO;
                var currentValue = this.GetAttributeValue(attributeAllocationData.AttributeMaster.AttributeMasterID);

                if (string.IsNullOrEmpty(currentValue) && !string.IsNullOrWhiteSpace(loadSp))
                {
                    int id;
                    var referenceId = this.GetReferenceId();
                    if (loadSp.EndsWithIgnoringCase(Constant.WithProduct))
                    {
                        id = this.GetWorkflowProductId();
                    }
                    else
                    {
                        id = this.GetWorkflowDocketId();
                    }

                    if (loadSp.StartsWithIgnoringCase(Constant.MacroReturn))
                    {
                        var productId = saleDetail.ProductId;
                        var result = this.DataManager.GetSPResultReturn(loadSp, string.Empty, id, referenceId, productId);
                        var resultValue = result.Item1 ?? string.Empty;
                        var resultMessage = result.Item2 ?? string.Empty;
                        var setOtherAttributeValue = result.Item3 ?? string.Empty;
                        if (!string.IsNullOrEmpty(setOtherAttributeValue))
                        {
                            this.SetAttributeValue(setOtherAttributeValue, resultValue);
                        }
                        else
                        {
                            this.SetAttributeValue(attributeAllocationData.AttributeMaster, resultValue);
                        }

                        this.HandleValidationResponse(resultMessage);
                    }
                }

                #endregion
            }

            if (ApplicationSettings.TouchScreenMode)
            {
                this.SetTypicalPieces(item.TypicalPieces);
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Messenger.Default.Send(this.attributes, Token.SetTraceability);
            }));
        }

        protected void SetTransactionAttributes(SaleDetail detail, StockDetail attributeData, NouvemViewModelBase vm)
        {
            this.SetAttributeValues(attributeData);
            this.HandleSaleDetailForTransaction(detail, vm);
        }

        /// <summary>
        /// Handle the incoming sale detail.
        /// </summary>
        protected virtual void HandleSaleDetailForTransaction(SaleDetail saleDetail, NouvemViewModelBase bindingVM)
        {
            #region insurance

            if (saleDetail == null)
            {
                return;
            }

            if (saleDetail.InventoryItem == null)
            {
                saleDetail.InventoryItem =
                    NouvemGlobal.InventoryItems.FirstOrDefault(
                        x => x.Master.INMasterID == saleDetail.INMasterID);

                if (saleDetail.InventoryItem == null)
                {
                    // should never happen, but just in case.
                    this.Log.LogDebug(this.GetType(), string.Format("HandlesaleDetail():{0}", Message.InventoryItemNotFound));
                    return;
                }
            }

            #endregion

            var item = saleDetail.InventoryItem.Master;
            this.attributes =
                this.traceabilityData.Where(x => x.AttributionAllocation.AttributeTemplateID == item.AttributeTemplateID).ToList();

            var localAttributes = this.attributes
                .Where(x => x.TraceabilityValueNonStandard.CompareIgnoringCase(Constant.Calender)
                            || (x.AttributeReset == null || x.AttributeReset.Name == AttributeReset.ResetAfterProductChange
                            || x.AttributeReset.Name == AttributeReset.ResetAfterTransaction));
            foreach (var attributeAllocationData in localAttributes)
            {
                attributeAllocationData.PropertyInfo = this.GetProperty(attributeAllocationData.AttributeMaster);
                attributeAllocationData.BindingViewModel = bindingVM;
                attributeAllocationData.INMasterID = item.INMasterID;
                var processData = this.SetProcessData(attributeAllocationData);
                attributeAllocationData.IsVisible = processData.Item1;
                attributeAllocationData.IsEditable = processData.Item2;

                #region dates

                if (attributeAllocationData.TraceabilityType.CompareIgnoringCase(Constant.Calender))
                {
                    this.SetDates(attributeAllocationData, this.attributes, item.INMasterID);
                }

                #endregion
            }

            if (ApplicationSettings.TouchScreenMode)
            {
                this.SetTypicalPieces(item.TypicalPieces);
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Messenger.Default.Send(this.attributes, Token.SetTraceability);
            }));
        }

        /// <summary>
        /// Sets the process flag (visibility/edit)
        /// </summary>
        /// <param name="data">The attribute allocation data.</param>
        protected virtual Tuple<bool, bool> SetProcessData(AttributeAllocationData data)
        {
            return Tuple.Create(true, true);
        }

        /// <summary>
        /// Sets the typical pieces.
        /// </summary>
        /// <param name="pieces">The typical pieces.</param>
        protected virtual void SetTypicalPieces(int? pieces)
        {
            this.Locator.Touchscreen.Pieces = pieces.ToInt();
        }

        /// <summary>
        /// Checks that all the required attributes have values.
        /// </summary>
        /// <returns>Empty string if all have values, otherwise the unfilled attribute name.</returns>
        protected string CheckAttributeData()
        {
            #region validation

            if (this.attributes == null)
            {
                return string.Empty;
            }

            #endregion

            var unfilledAttribute = string.Empty;
            foreach (var attributeAllocationData in this.attributes)
            {
                if (attributeAllocationData.AttributionAllocation.RequiredBeforeContinue
                    && attributeAllocationData.IsVisible
                    && attributeAllocationData.IsEditable)
                {
                    var value = this.GetAttributeValue(attributeAllocationData.AttributeMaster.AttributeMasterID);
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        Messenger.Default.Send(attributeAllocationData.AttributionAllocation.AttributeAllocationID, Token.EmptyRequiredAttribute);
                        return attributeAllocationData.AttributeMaster.Description;
                    }
                }
            }

            return unfilledAttribute;
        }

        /// <summary>
        /// Gets the attribute data for transaction checks.
        /// </summary>
        protected void GetAttributeAllocationApplicationData(int processId)
        {
            var localData =
                 this.traceabilityData
                    .Where(x => x.AttributeTemplate != null
                                && x.AttributeTemplate.AttributeType.CompareIgnoringCaseAndWhitespace(Strings.Transaction))
                     .OrderBy(s => s.AttributionAllocation.Sequence)
                     .ToList();

            var localPostData =
                this.traceabilityData
                    .Where(x => x.AttributeTemplate != null
                                && x.AttributeTemplate.AttributeType.CompareIgnoringCaseAndWhitespace(Strings.PostTransaction))
                    .OrderBy(s => s.AttributionAllocation.Sequence)
                    .ToList();

            var localCompleteData =
                this.traceabilityData
                    .Where(x => x.AttributeTemplate != null
                                && x.AttributeTemplate.AttributeType.CompareIgnoringCaseAndWhitespace(Strings.Completion))
                    .OrderBy(s => s.AttributionAllocation.Sequence)
                    .ToList();

            var localMoveData =
                this.traceabilityData
                    .Where(x => x.AttributeTemplate != null
                                && x.AttributeTemplate.AttributeType.CompareIgnoringCaseAndWhitespace(Strings.Movement))
                    .OrderBy(s => s.AttributionAllocation.Sequence)
                    .ToList();

            var localLabelData =
                this.traceabilityData
                    .Where(x => x.AttributeTemplate != null
                                && x.AttributeTemplate.AttributeType.CompareIgnoringCaseAndWhitespace(Strings.Label))
                    .OrderBy(s => s.AttributionAllocation.Sequence)
                    .ToList();

            var localAttributeData = localData.Union(localPostData).Union(localCompleteData).Union(localMoveData).Union(localLabelData).Where(x => x.AttributeVisibleProcesses == null).ToList();
            if (localAttributeData.Any())
            {
                this.DataManager.GetAttributeAllocations(localAttributeData);
            }

            this.traceabilityDataTransactions = localData
                .Where(x => x.AttributeVisibleProcesses != null &&
                            x.AttributeVisibleProcesses.Select(p => p.ProcessId).Contains(processId)).ToList();

            this.traceabilityDataPostTransactions = localPostData
                .Where(x => x.AttributeVisibleProcesses != null &&
                            x.AttributeVisibleProcesses.Select(p => p.ProcessId).Contains(processId)).ToList();

            this.traceabilityDataCompletion = localCompleteData
                .Where(x => x.AttributeVisibleProcesses != null &&
                            x.AttributeVisibleProcesses.Select(p => p.ProcessId).Contains(processId)).ToList();

            this.traceabilityDataMoves = localMoveData
                .Where(x => x.AttributeVisibleProcesses != null &&
                            x.AttributeVisibleProcesses.Select(p => p.ProcessId).Contains(processId)).ToList();

            this.traceabilityDataLabel = localLabelData
                .Where(x => x.AttributeVisibleProcesses != null &&
                            x.AttributeVisibleProcesses.Select(p => p.ProcessId).Contains(processId)).ToList();
        }

        /// <summary>
        /// Checks that all the required before completion attributes have values.
        /// </summary>
        /// <returns>Empty string if all have values, otherwise the unfilled attribute name.</returns>
        protected string CheckRequiredBeforeCompletionAttributeData()
        {
            #region validation

            if (this.attributes == null)
            {
                return string.Empty;
            }

            #endregion

            var unfilledAttribute = string.Empty;
            foreach (var attributeAllocationData in this.attributes)
            {
                if (attributeAllocationData.AttributionAllocation.RequiredBeforeCompletion
                    && attributeAllocationData.IsVisible
                    && attributeAllocationData.IsEditable)
                {
                    var value = this.GetAttributeValue(attributeAllocationData.AttributeMaster.AttributeMasterID);
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        return attributeAllocationData.AttributeMaster.Description;
                    }
                }
            }

            return unfilledAttribute;
        }

        /// <summary>
        /// Checks if any of the attributes are to be reset.
        /// </summary>
        /// <param name="saleDetail">The current detail.</param>
        protected void CheckAttributeTransactionReset(SaleDetail saleDetail)
        {
            if (saleDetail == null || saleDetail.InventoryItem == null)
            {
                return;
            }

            var item = saleDetail.InventoryItem.Master;
            var localAttributes =
                this.traceabilityData.Where(x => x.AttributionAllocation.AttributeTemplateID == item.AttributeTemplateID).ToList();

            foreach (var attributeAllocationData in localAttributes)
            {
                var reset = attributeAllocationData.AttributeReset;
                if (reset != null)
                {
                    if (reset.Name.CompareIgnoringCase(AttributeReset.ResetAfterTransaction))
                    {
                        this.SetAttributeValue(attributeAllocationData.AttributeMaster, string.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if any of the attributes are to be reset.
        /// </summary>
        /// <param name="saleDetail">The current detail.</param>
        protected void CheckAttributeProductChangeReset(SaleDetail saleDetail)
        {
            #region insurance

            if (saleDetail == null)
            {
                return;
            }

            if (saleDetail.InventoryItem == null)
            {
                saleDetail.InventoryItem =
                    NouvemGlobal.InventoryItems.FirstOrDefault(
                        x => x.Master.INMasterID == saleDetail.INMasterID);

                if (saleDetail.InventoryItem == null)
                {
                    // should never happen, but just in case.
                    this.Log.LogDebug(this.GetType(), string.Format("HandlesaleDetail():{0}", Message.InventoryItemNotFound));
                    return;
                }
            }

            #endregion

            var item = saleDetail.InventoryItem.Master;
            var localAttributes =
                this.traceabilityData.Where(x => x.AttributionAllocation.AttributeTemplateID == item.AttributeTemplateID).ToList();

            foreach (var attributeAllocationData in localAttributes)
            {
                var reset = attributeAllocationData.AttributeReset;
                if (reset != null)
                {
                    if (reset.Name.CompareIgnoringCase(AttributeReset.ResetAfterProductChange))
                    {
                        this.SetAttributeValue(attributeAllocationData.AttributeMaster, string.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if any of the attributes are to be reset on a batch change.
        /// </summary>
        protected void CheckAttributeBatchChangeReset()
        {
            var localAttributes =
                this.traceabilityData.ToList();

            foreach (var attributeAllocationData in localAttributes)
            {
                var reset = attributeAllocationData.AttributeReset;
                if (reset != null)
                {
                    if (reset.Name.CompareIgnoringCase(AttributeReset.ResetOnBatchChange)
                    || reset.Name.CompareIgnoringCase(AttributeReset.ResetOnBatchChangeOrScan))
                    {
                        this.SetAttributeValue(attributeAllocationData.AttributeMaster, string.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// Checks if any of the attributes are to be reset on a scan.
        /// </summary>
        protected void CheckAttributeScanChangeReset()
        {
            var localAttributes =
                this.traceabilityData.ToList();

            foreach (var attributeAllocationData in localAttributes)
            {
                var reset = attributeAllocationData.AttributeReset;
                if (reset != null)
                {
                    if (reset.Name.CompareIgnoringCase(AttributeReset.ResetOnBatchChangeOrScan))
                    {
                        this.SetAttributeValue(attributeAllocationData.AttributeMaster, string.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// Validates a transaction.
        /// </summary>
        /// <param name="checks">The checks to make.</param>
        /// <param name="id">The document id.</param>
        /// <param name="wgt">The transaction weight.</param>
        /// <param name="qty">The transaction qty.</param>
        /// <param name="inmasterid">The transaction product.</param>
        /// <param name="warehouseId">The transaction location.</param>
        /// <param name="processId">The transaction process.</param>
        /// <returns>Validation flag result.</returns>
        protected virtual bool ValidateData(IList<AttributeAllocationData> checks, int id, decimal wgt, decimal qty, int inmasterid, int warehouseId, int processId, string mode, string reference = "", string reference2 = "", string reference3 = "", string reference4 = "")
        {
            foreach (var check in checks)
            {
                int? intake = null;
                int? batch = null;
                int? workflow = null;
                int? dispatch = null;
                if (mode.CompareIgnoringCaseAndWhitespace(Constant.Intake))
                {
                    intake = id;
                }
                else if (mode.CompareIgnoringCaseAndWhitespace(Constant.Batch))
                {
                    batch = id;
                }
                else if (mode.CompareIgnoringCaseAndWhitespace(Constant.Workflow))
                {
                    workflow = id;
                }
                else
                {
                    dispatch = id;
                }

                var macro = check.AttributeMaster.StandardResponseChecker;
                if (macro == null)
                {
                    macro = check.AttributeMaster.LoadMACRO;
                }

                var result = this.DataManager.GetSPDataResultReturn(macro, id, wgt, qty, inmasterid, warehouseId, processId, reference, reference2,reference3,reference4);
                if (string.IsNullOrEmpty(result))
                {
                    continue;
                }

                var hardStop = check.AttributionAllocation.RequiredBeforeContinue;
                var messageButtons = hardStop ? NouvemMessageBoxButtons.OK : NouvemMessageBoxButtons.YesNo;
                NouvemMessageBox.Show(result, messageButtons, touchScreen: true, scanner: ApplicationSettings.ScannerMode);
                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                {
                    this.LogAlerts(check, workflow, intake, batch, processId, dispatch);
                    continue;
                }

                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected virtual int? GetReferenceId()
        {
            return 0;
        }

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected virtual int GetWorkflowDocketId()
        {
            return 0;
        }

        /// <summary>
        /// Gets the current attribute product id.
        /// </summary>
        /// <returns>THe corresponding product id.</returns>
        protected virtual int GetWorkflowProductId()
        {
            return 0;
        }

        /// <summary>
        /// Handle the selected stock detail parse/edit.
        /// </summary>
        /// <param name="detail">The detail to parse for edit.</param>
        protected virtual void HandleSelectedStockDetail(StockDetail detail)
        {
        }

        /// <summary>
        /// Handle the secondary barcode scan on the passport.
        /// </summary>
        /// <param name="category">The scanned passport data barcode.</param>
        protected virtual void HandleCategorySelection(NouCategory category)
        {
        }

        /// <summary>
        /// Handles a change od sex.
        /// </summary>
        protected virtual void HandleSexSelection()
        {
        }

        /// <summary>
        /// Handle the eartag entry.
        /// </summary>
        protected virtual void HandleEartagEntry()
        {
        }

        /// <summary>
        /// Handle the secondary barcode scan on the passport.
        /// </summary>
        /// <param name="data">The scanned passport data barcode.</param>
        protected virtual void HandleScannedPassportData(string data)
        {
        }

        /// <summary>
        /// Handles the selected date.
        /// </summary>
        protected virtual void HandleSelectedCalenderDate()
        {
        }

        /// <summary>
        /// Creates an attribute.
        /// </summary>
        /// <returns>The newly created attribute.</returns>
        protected virtual Nouvem.Model.DataLayer.Attribute CreateAttribute()
        {
            var cleanlinessId = this.SelectedCleanliness != null ? this.SelectedCleanliness.NouCleanlinessID : (int?)null;
            #region attributes

            var attribute = new Nouvem.Model.DataLayer.Attribute
            {
                Eartag = this.Eartag,
                Herd = this.HerdNo,
                DOB = this.DOB,
                DateOfLastMove = this.LastMoveDate,
                AgeInMonths = this.AgeInMonths,
                AgeInDays = this.AgeInDays,
                Category = this.SelectedCategory != null ? this.SelectedCategory.CategoryID : (int?)null,
                Breed = this.Breed != null ? this.Breed.NouBreedID : (int?)null,
                Sex = this.Sex,
                FarmAssured = this.FarmAssured.YesNoToBool(),
                Clipped = this.Clipped.YesNoToBool(),
                Casualty = this.Casualty.YesNoToBool(),
                Imported = this.Imported.YesNoToBool(),
                Lame = this.Lame.YesNoToBool(),
                Cleanliness = cleanlinessId,
                Generic1 = this.Generic1,
                CountryOfOrigin = this.CountryOfOrigin != null ? this.CountryOfOrigin.Name : string.Empty,
                RearedIn = this.RearedIn != null ? this.RearedIn.Name : string.Empty,
                KillType = this.KillType.ToString(),
                CurrentResidency = this.DaysOfResidencyCurrent,
                TotalResidency = this.DaysOfResidencyTotal,
                NoOfMoves = this.NumberOfMoves,
                HoldingNumber = this.HoldingNumber,
                RPA = this.SelectedStockDetail != null && this.SelectedStockDetail.RPA,
                Total = this.SelectedStockDetail != null && this.SelectedStockDetail.TotalExclVat.HasValue ? this.SelectedStockDetail.TotalExclVat : null,
                Attribute1 = this.Attribute1,
                Attribute2 = this.Attribute2,
                Attribute3 = this.Attribute3,
                Attribute4 = this.Attribute4,
                Attribute5 = this.Attribute5,
                Attribute6 = this.Attribute6,
                Attribute7 = this.Attribute7,
                Attribute8 = this.Attribute8,
                Attribute9 = this.Attribute9,
                Attribute10 = this.Attribute10,
                Attribute11 = this.Attribute11,
                Attribute12 = this.Attribute12,
                Attribute13 = this.Attribute13,
                Attribute14 = this.Attribute14,
                Attribute15 = this.Attribute15,
                Attribute16 = this.Attribute16,
                Attribute17 = this.Attribute17,
                Attribute18 = this.Attribute18,
                Attribute19 = this.Attribute19,
                Attribute20 = this.Attribute20,
                Attribute21 = this.Attribute21,
                Attribute22 = this.Attribute22,
                Attribute23 = this.Attribute23,
                Attribute24 = this.Attribute24,
                Attribute25 = this.Attribute25,
                Attribute26 = this.Attribute26,
                Attribute27 = this.Attribute27,
                Attribute28 = this.Attribute28,
                Attribute29 = this.Attribute29,
                Attribute30 = this.Attribute30,
                Attribute31 = this.Attribute31,
                Attribute32 = this.Attribute32,
                Attribute33 = this.Attribute33,
                Attribute34 = this.Attribute34,
                Attribute35 = this.Attribute35,
                Attribute36 = this.Attribute36,
                Attribute37 = this.Attribute37,
                Attribute38 = this.Attribute38,
                Attribute39 = this.Attribute39,
                Attribute40 = this.Attribute40,
                Attribute41 = this.Attribute41,
                Attribute42 = this.Attribute42,
                Attribute43 = this.Attribute43,
                Attribute44 = this.Attribute44,
                Attribute45 = this.Attribute45,
                Attribute46 = this.Attribute46,
                Attribute47 = this.Attribute47,
                Attribute48 = this.Attribute48,
                Attribute49 = this.Attribute49,
                Attribute50 = this.Attribute50,
                Attribute51 = this.Attribute51,
                Attribute52 = this.Attribute52,
                Attribute53 = this.Attribute53,
                Attribute54 = this.Attribute54,
                Attribute55 = this.Attribute55,
                Attribute56 = this.Attribute56,
                Attribute57 = this.Attribute57,
                Attribute58 = this.Attribute58,
                Attribute59 = this.Attribute59,
                Attribute60 = this.Attribute60,
                Attribute61 = this.Attribute61,
                Attribute62 = this.Attribute62,
                Attribute63 = this.Attribute63,
                Attribute64 = this.Attribute64,
                Attribute65 = this.Attribute65,
                Attribute66 = this.Attribute66,
                Attribute67 = this.Attribute67,
                Attribute68 = this.Attribute68,
                Attribute69 = this.Attribute69,
                Attribute70 = this.Attribute70,
                Attribute71 = this.Attribute71,
                Attribute72 = this.Attribute72,
                Attribute73 = this.Attribute73,
                Attribute74 = this.Attribute74,
                Attribute75 = this.Attribute75,
                Attribute76 = this.Attribute76,
                Attribute77 = this.Attribute77,
                Attribute78 = this.Attribute78,
                Attribute79 = this.Attribute79,
                Attribute80 = this.Attribute80,
                Attribute81 = this.Attribute81,
                Attribute82 = this.Attribute82,
                Attribute83 = this.Attribute83,
                Attribute84 = this.Attribute84,
                Attribute85 = this.Attribute85,
                Attribute86 = this.Attribute86,
                Attribute87 = this.Attribute87,
                Attribute88 = this.Attribute88,
                Attribute89 = this.Attribute89,
                Attribute90 = this.Attribute90,
                Attribute91 = this.Attribute91,
                Attribute92 = this.Attribute92,
                Attribute93 = this.Attribute93,
                Attribute94 = this.Attribute94,
                Attribute95 = this.Attribute95,
                Attribute96 = this.Attribute96,
                Attribute97 = this.Attribute97,
                Attribute98 = this.Attribute98,
                Attribute99 = this.Attribute99,
                Attribute100 = this.Attribute100,
                Attribute101 = this.Attribute101,
                Attribute102 = this.Attribute102,
                Attribute103 = this.Attribute103,
                Attribute104 = this.Attribute104,
                Attribute105 = this.Attribute105,
                Attribute106 = this.Attribute106,
                Attribute107 = this.Attribute107,
                Attribute108 = this.Attribute108,
                Attribute109 = this.Attribute109,
                Attribute110 = this.Attribute110,
                Attribute111 = this.Attribute111,
                Attribute112 = this.Attribute112,
                Attribute113 = this.Attribute113,
                Attribute114 = this.Attribute114,
                Attribute115 = this.Attribute115,
                Attribute116 = this.Attribute116,
                Attribute117 = this.Attribute117,
                Attribute118 = this.Attribute118,
                Attribute119 = this.Attribute119,
                Attribute120 = this.Attribute120,
                Attribute121 = this.Attribute121,
                Attribute122 = this.Attribute122,
                Attribute123 = this.Attribute123,
                Attribute124 = this.Attribute124,
                Attribute125 = this.Attribute125,
                Attribute126 = this.Attribute126,
                Attribute127 = this.Attribute127,
                Attribute128 = this.Attribute128,
                Attribute129 = this.Attribute129,
                Attribute130 = this.Attribute130,
                Attribute131 = this.Attribute131,
                Attribute132 = this.Attribute132,
                Attribute133 = this.Attribute133,
                Attribute134 = this.Attribute134,
                Attribute135 = this.Attribute135,
                Attribute136 = this.Attribute136,
                Attribute137 = this.Attribute137,
                Attribute138 = this.Attribute138,
                Attribute139 = this.Attribute139,
                Attribute140 = this.Attribute140,
                Attribute141 = this.Attribute141,
                Attribute142 = this.Attribute142,
                Attribute143 = this.Attribute143,
                Attribute144 = this.Attribute144,
                Attribute145 = this.Attribute145,
                Attribute146 = this.Attribute146,
                Attribute147 = this.Attribute147,
                Attribute148 = this.Attribute148,
                Attribute149 = this.Attribute149,
                Attribute150 = this.Attribute150,

                Attribute151 = this.Attribute151,
                Attribute152 = this.Attribute152,
                Attribute153 = this.Attribute153,
                Attribute154 = this.Attribute154,
                Attribute155 = this.Attribute155,
                Attribute156 = this.Attribute156,
                Attribute157 = this.Attribute157,
                Attribute158 = this.Attribute158,
                Attribute159 = this.Attribute159,
                Attribute160 = this.Attribute160,
                Attribute161 = this.Attribute161,
                Attribute162 = this.Attribute162,
                Attribute163 = this.Attribute163,
                Attribute164 = this.Attribute164,
                Attribute165 = this.Attribute165,
                Attribute166 = this.Attribute166,
                Attribute167 = this.Attribute167,
                Attribute168 = this.Attribute168,
                Attribute169 = this.Attribute169,
                Attribute170 = this.Attribute170,
                Attribute171 = this.Attribute171,
                Attribute172 = this.Attribute172,
                Attribute173 = this.Attribute173,
                Attribute174 = this.Attribute174,
                Attribute175 = this.Attribute175,
                Attribute176 = this.Attribute176,
                Attribute177 = this.Attribute177,
                Attribute178 = this.Attribute178,
                Attribute179 = this.Attribute179,
                Attribute180 = this.Attribute180,
                Attribute181 = this.Attribute181,
                Attribute182 = this.Attribute182,
                Attribute183 = this.Attribute183,
                Attribute184 = this.Attribute184,
                Attribute185 = this.Attribute185,
                Attribute186 = this.Attribute186,
                Attribute187 = this.Attribute187,
                Attribute188 = this.Attribute188,
                Attribute189 = this.Attribute189,
                Attribute190 = this.Attribute190,
                Attribute191 = this.Attribute191,
                Attribute192 = this.Attribute192,
                Attribute193 = this.Attribute193,
                Attribute194 = this.Attribute194,
                Attribute195 = this.Attribute195,
                Attribute196 = this.Attribute196,
                Attribute197 = this.Attribute197,
                Attribute198 = this.Attribute198,
                Attribute199 = this.Attribute199,
                Attribute200 = this.Attribute200,

                Attribute201 = this.Attribute201,
                Attribute202 = this.Attribute202,
                Attribute203 = this.Attribute203,
                Attribute204 = this.Attribute204,
                Attribute205 = this.Attribute205,
                Attribute206 = this.Attribute206,
                Attribute207 = this.Attribute207,
                Attribute208 = this.Attribute208,
                Attribute209 = this.Attribute209,
                Attribute210 = this.Attribute210,
                Attribute211 = this.Attribute211,
                Attribute212 = this.Attribute212,
                Attribute213 = this.Attribute213,
                Attribute214 = this.Attribute214,
                Attribute215 = this.Attribute215,
                Attribute216 = this.Attribute216,
                Attribute217 = this.Attribute217,
                Attribute218 = this.Attribute218,
                Attribute219 = this.Attribute219,
                Attribute220 = this.Attribute220,
                Attribute221 = this.Attribute221,
                Attribute222 = this.Attribute222,
                Attribute223 = this.Attribute223,
                Attribute224 = this.Attribute224,
                Attribute225 = this.Attribute225,
                Attribute226 = this.Attribute226,
                Attribute227 = this.Attribute227,
                Attribute228 = this.Attribute228,
                Attribute229 = this.Attribute229,
                Attribute230 = this.Attribute230,
                Attribute231 = this.Attribute231,
                Attribute232 = this.Attribute232,
                Attribute233 = this.Attribute233,
                Attribute234 = this.Attribute234,
                Attribute235 = this.Attribute235,
                Attribute236 = this.Attribute236,
                Attribute237 = this.Attribute237,
                Attribute238 = this.Attribute238,
                Attribute239 = this.Attribute239,
                Attribute240 = this.Attribute240,
                Attribute241 = this.Attribute241,
                Attribute242 = this.Attribute242,
                Attribute243 = this.Attribute243,
                Attribute244 = this.Attribute244,
                Attribute245 = this.Attribute245,
                Attribute246 = this.Attribute246,
                Attribute247 = this.Attribute247,
                Attribute248 = this.Attribute248,
                Attribute249 = this.Attribute249,
                Attribute250 = this.Attribute250,

            };

            #endregion

            return attribute;
        }

        /// <summary>
        /// Creates the attribute part of a transaction.
        /// </summary>
        protected virtual void CreateAttribute(StockDetail detail)
        {
            #region transaction

            detail.Attribute1 = this.Attribute1;
            detail.Attribute2 = this.Attribute2;
            detail.Attribute3 = this.Attribute3;
            detail.Attribute4 = this.Attribute4;
            detail.Attribute5 = this.Attribute5;
            detail.Attribute6 = this.Attribute6;
            detail.Attribute7 = this.Attribute7;
            detail.Attribute8 = this.Attribute8;
            detail.Attribute9 = this.Attribute9;
            detail.Attribute10 = this.Attribute10;
            detail.Attribute11 = this.Attribute11;
            detail.Attribute12 = this.Attribute12;
            detail.Attribute13 = this.Attribute13;
            detail.Attribute14 = this.Attribute14;
            detail.Attribute15 = this.Attribute15;
            detail.Attribute16 = this.Attribute16;
            detail.Attribute17 = this.Attribute17;
            detail.Attribute18 = this.Attribute18;
            detail.Attribute19 = this.Attribute19;
            detail.Attribute20 = this.Attribute20;
            detail.Attribute21 = this.Attribute21;
            detail.Attribute22 = this.Attribute22;
            detail.Attribute23 = this.Attribute23;
            detail.Attribute24 = this.Attribute24;
            detail.Attribute25 = this.Attribute25;
            detail.Attribute26 = this.Attribute26;
            detail.Attribute27 = this.Attribute27;
            detail.Attribute28 = this.Attribute28;
            detail.Attribute29 = this.Attribute29;
            detail.Attribute30 = this.Attribute30;
            detail.Attribute31 = this.Attribute31;
            detail.Attribute32 = this.Attribute32;
            detail.Attribute33 = this.Attribute33;
            detail.Attribute34 = this.Attribute34;
            detail.Attribute35 = this.Attribute35;
            detail.Attribute36 = this.Attribute36;
            detail.Attribute37 = this.Attribute37;
            detail.Attribute38 = this.Attribute38;
            detail.Attribute39 = this.Attribute39;
            detail.Attribute40 = this.Attribute40;
            detail.Attribute41 = this.Attribute41;
            detail.Attribute42 = this.Attribute42;
            detail.Attribute43 = this.Attribute43;
            detail.Attribute44 = this.Attribute44;
            detail.Attribute45 = this.Attribute45;
            detail.Attribute46 = this.Attribute46;
            detail.Attribute47 = this.Attribute47;
            detail.Attribute48 = this.Attribute48;
            detail.Attribute49 = this.Attribute49;
            detail.Attribute50 = this.Attribute50;
            detail.Attribute51 = this.Attribute51;
            detail.Attribute52 = this.Attribute52;
            detail.Attribute53 = this.Attribute53;
            detail.Attribute54 = this.Attribute54;
            detail.Attribute55 = this.Attribute55;
            detail.Attribute56 = this.Attribute56;
            detail.Attribute57 = this.Attribute57;
            detail.Attribute58 = this.Attribute58;
            detail.Attribute59 = this.Attribute59;
            detail.Attribute60 = this.Attribute60;
            detail.Attribute61 = this.Attribute61;
            detail.Attribute62 = this.Attribute62;
            detail.Attribute63 = this.Attribute63;
            detail.Attribute64 = this.Attribute64;
            detail.Attribute65 = this.Attribute65;
            detail.Attribute66 = this.Attribute66;
            detail.Attribute67 = this.Attribute67;
            detail.Attribute68 = this.Attribute68;
            detail.Attribute69 = this.Attribute69;
            detail.Attribute70 = this.Attribute70;
            detail.Attribute71 = this.Attribute71;
            detail.Attribute72 = this.Attribute72;
            detail.Attribute73 = this.Attribute73;
            detail.Attribute74 = this.Attribute74;
            detail.Attribute75 = this.Attribute75;
            detail.Attribute76 = this.Attribute76;
            detail.Attribute77 = this.Attribute77;
            detail.Attribute78 = this.Attribute78;
            detail.Attribute79 = this.Attribute79;
            detail.Attribute80 = this.Attribute80;
            detail.Attribute81 = this.Attribute81;
            detail.Attribute82 = this.Attribute82;
            detail.Attribute83 = this.Attribute83;
            detail.Attribute84 = this.Attribute84;
            detail.Attribute85 = this.Attribute85;
            detail.Attribute86 = this.Attribute86;
            detail.Attribute87 = this.Attribute87;
            detail.Attribute88 = this.Attribute88;
            detail.Attribute89 = this.Attribute89;
            detail.Attribute90 = this.Attribute90;
            detail.Attribute91 = this.Attribute91;
            detail.Attribute92 = this.Attribute92;
            detail.Attribute93 = this.Attribute93;
            detail.Attribute94 = this.Attribute94;
            detail.Attribute95 = this.Attribute95;
            detail.Attribute96 = this.Attribute96;
            detail.Attribute97 = this.Attribute97;
            detail.Attribute98 = this.Attribute98;
            detail.Attribute99 = this.Attribute99;
            detail.Attribute100 = this.Attribute100;
            detail.Attribute101 = this.Attribute101;
            detail.Attribute102 = this.Attribute102;
            detail.Attribute103 = this.Attribute103;
            detail.Attribute104 = this.Attribute104;
            detail.Attribute105 = this.Attribute105;
            detail.Attribute106 = this.Attribute106;
            detail.Attribute107 = this.Attribute107;
            detail.Attribute108 = this.Attribute108;
            detail.Attribute109 = this.Attribute109;
            detail.Attribute110 = this.Attribute110;
            detail.Attribute111 = this.Attribute111;
            detail.Attribute112 = this.Attribute112;
            detail.Attribute113 = this.Attribute113;
            detail.Attribute114 = this.Attribute114;
            detail.Attribute115 = this.Attribute115;
            detail.Attribute116 = this.Attribute116;
            detail.Attribute117 = this.Attribute117;
            detail.Attribute118 = this.Attribute118;
            detail.Attribute119 = this.Attribute119;
            detail.Attribute120 = this.Attribute120;
            detail.Attribute121 = this.Attribute121;
            detail.Attribute122 = this.Attribute122;
            detail.Attribute123 = this.Attribute123;
            detail.Attribute124 = this.Attribute124;
            detail.Attribute125 = this.Attribute125;
            detail.Attribute126 = this.Attribute126;
            detail.Attribute127 = this.Attribute127;
            detail.Attribute128 = this.Attribute128;
            detail.Attribute129 = this.Attribute129;
            detail.Attribute130 = this.Attribute130;
            detail.Attribute131 = this.Attribute131;
            detail.Attribute132 = this.Attribute132;
            detail.Attribute133 = this.Attribute133;
            detail.Attribute134 = this.Attribute134;
            detail.Attribute135 = this.Attribute135;
            detail.Attribute136 = this.Attribute136;
            detail.Attribute137 = this.Attribute137;
            detail.Attribute138 = this.Attribute138;
            detail.Attribute139 = this.Attribute139;
            detail.Attribute140 = this.Attribute140;
            detail.Attribute141 = this.Attribute141;
            detail.Attribute142 = this.Attribute142;
            detail.Attribute143 = this.Attribute143;
            detail.Attribute144 = this.Attribute144;
            detail.Attribute145 = this.Attribute145;
            detail.Attribute146 = this.Attribute146;
            detail.Attribute147 = this.Attribute147;
            detail.Attribute148 = this.Attribute148;
            detail.Attribute149 = this.Attribute149;
            detail.Attribute150 = this.Attribute150;
            detail.Attribute151 = this.Attribute151;
            detail.Attribute152 = this.Attribute152;
            detail.Attribute153 = this.Attribute153;
            detail.Attribute154 = this.Attribute154;
            detail.Attribute155 = this.Attribute155;
            detail.Attribute156 = this.Attribute156;
            detail.Attribute157 = this.Attribute157;
            detail.Attribute158 = this.Attribute158;
            detail.Attribute159 = this.Attribute159;
            detail.Attribute160 = this.Attribute160;
            detail.Attribute161 = this.Attribute161;
            detail.Attribute162 = this.Attribute162;
            detail.Attribute163 = this.Attribute163;
            detail.Attribute164 = this.Attribute164;
            detail.Attribute165 = this.Attribute165;
            detail.Attribute166 = this.Attribute166;
            detail.Attribute167 = this.Attribute167;
            detail.Attribute168 = this.Attribute168;
            detail.Attribute169 = this.Attribute169;
            detail.Attribute170 = this.Attribute170;
            detail.Attribute171 = this.Attribute171;
            detail.Attribute172 = this.Attribute172;
            detail.Attribute173 = this.Attribute173;
            detail.Attribute174 = this.Attribute174;
            detail.Attribute175 = this.Attribute175;
            detail.Attribute176 = this.Attribute176;
            detail.Attribute177 = this.Attribute177;
            detail.Attribute178 = this.Attribute178;
            detail.Attribute179 = this.Attribute179;
            detail.Attribute180 = this.Attribute180;
            detail.Attribute181 = this.Attribute181;
            detail.Attribute182 = this.Attribute182;
            detail.Attribute183 = this.Attribute183;
            detail.Attribute184 = this.Attribute184;
            detail.Attribute185 = this.Attribute185;
            detail.Attribute186 = this.Attribute186;
            detail.Attribute187 = this.Attribute187;
            detail.Attribute188 = this.Attribute188;
            detail.Attribute189 = this.Attribute189;
            detail.Attribute190 = this.Attribute190;
            detail.Attribute191 = this.Attribute191;
            detail.Attribute192 = this.Attribute192;
            detail.Attribute193 = this.Attribute193;
            detail.Attribute194 = this.Attribute194;
            detail.Attribute195 = this.Attribute195;
            detail.Attribute196 = this.Attribute196;
            detail.Attribute197 = this.Attribute197;
            detail.Attribute198 = this.Attribute198;
            detail.Attribute199 = this.Attribute199;
            detail.Attribute200 = this.Attribute200;
            detail.Attribute201 = this.Attribute201;
            detail.Attribute202 = this.Attribute202;
            detail.Attribute203 = this.Attribute203;
            detail.Attribute204 = this.Attribute204;
            detail.Attribute205 = this.Attribute205;
            detail.Attribute206 = this.Attribute206;
            detail.Attribute207 = this.Attribute207;
            detail.Attribute208 = this.Attribute208;
            detail.Attribute209 = this.Attribute209;
            detail.Attribute210 = this.Attribute210;
            detail.Attribute211 = this.Attribute211;
            detail.Attribute212 = this.Attribute212;
            detail.Attribute213 = this.Attribute213;
            detail.Attribute214 = this.Attribute214;
            detail.Attribute215 = this.Attribute215;
            detail.Attribute216 = this.Attribute216;
            detail.Attribute217 = this.Attribute217;
            detail.Attribute218 = this.Attribute218;
            detail.Attribute219 = this.Attribute219;
            detail.Attribute220 = this.Attribute220;
            detail.Attribute221 = this.Attribute221;
            detail.Attribute222 = this.Attribute222;
            detail.Attribute223 = this.Attribute223;
            detail.Attribute224 = this.Attribute224;
            detail.Attribute225 = this.Attribute225;
            detail.Attribute226 = this.Attribute226;
            detail.Attribute227 = this.Attribute227;
            detail.Attribute228 = this.Attribute228;
            detail.Attribute229 = this.Attribute229;
            detail.Attribute230 = this.Attribute230;
            detail.Attribute231 = this.Attribute231;
            detail.Attribute232 = this.Attribute232;
            detail.Attribute233 = this.Attribute233;
            detail.Attribute234 = this.Attribute234;
            detail.Attribute235 = this.Attribute235;
            detail.Attribute236 = this.Attribute236;
            detail.Attribute237 = this.Attribute237;
            detail.Attribute238 = this.Attribute238;
            detail.Attribute239 = this.Attribute239;
            detail.Attribute240 = this.Attribute240;
            detail.Attribute241 = this.Attribute241;
            detail.Attribute242 = this.Attribute242;
            detail.Attribute243 = this.Attribute243;
            detail.Attribute244 = this.Attribute244;
            detail.Attribute245 = this.Attribute245;
            detail.Attribute246 = this.Attribute246;
            detail.Attribute247 = this.Attribute247;
            detail.Attribute248 = this.Attribute248;
            detail.Attribute249 = this.Attribute249;
            detail.Attribute250 = this.Attribute250;


            #endregion

            #region batch

            detail.BatchAttribute = new StockDetail();

            #region validation

            if (this.attributes == null)
            {
                return;
            }

            #endregion

            foreach (var localAttribute in this.attributes)
            {
                if (localAttribute.PropertyInfo == null)
                {
                    this.Log.LogError(this.GetType(), string.Format("CreateAttribute: PropertyInfo Null: AttributeMasterID:{0}", localAttribute.AttributeMaster.AttributeMasterID));
                    continue;
                }

                if (localAttribute.IsBatch)
                {
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute1")) { detail.BatchAttribute.Attribute1 = this.Attribute1; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute2")) { detail.BatchAttribute.Attribute2 = this.Attribute2; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute3")) { detail.BatchAttribute.Attribute3 = this.Attribute3; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute4")) { detail.BatchAttribute.Attribute4 = this.Attribute4; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute5")) { detail.BatchAttribute.Attribute5 = this.Attribute5; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute6")) { detail.BatchAttribute.Attribute6 = this.Attribute6; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute7")) { detail.BatchAttribute.Attribute7 = this.Attribute7; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute8")) { detail.BatchAttribute.Attribute8 = this.Attribute8; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute9")) { detail.BatchAttribute.Attribute9 = this.Attribute9; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute10")) { detail.BatchAttribute.Attribute10 = this.Attribute10; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute11")) { detail.BatchAttribute.Attribute11 = this.Attribute11; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute12")) { detail.BatchAttribute.Attribute12 = this.Attribute12; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute13")) { detail.BatchAttribute.Attribute13 = this.Attribute13; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute14")) { detail.BatchAttribute.Attribute14 = this.Attribute14; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute15")) { detail.BatchAttribute.Attribute15 = this.Attribute15; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute16")) { detail.BatchAttribute.Attribute16 = this.Attribute16; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute17")) { detail.BatchAttribute.Attribute17 = this.Attribute17; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute18")) { detail.BatchAttribute.Attribute18 = this.Attribute18; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute19")) { detail.BatchAttribute.Attribute19 = this.Attribute19; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute20")) { detail.BatchAttribute.Attribute20 = this.Attribute20; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute21")) { detail.BatchAttribute.Attribute21 = this.Attribute21; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute22")) { detail.BatchAttribute.Attribute22 = this.Attribute22; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute23")) { detail.BatchAttribute.Attribute23 = this.Attribute23; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute24")) { detail.BatchAttribute.Attribute24 = this.Attribute24; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute25")) { detail.BatchAttribute.Attribute25 = this.Attribute25; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute26")) { detail.BatchAttribute.Attribute26 = this.Attribute26; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute27")) { detail.BatchAttribute.Attribute27 = this.Attribute27; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute28")) { detail.BatchAttribute.Attribute28 = this.Attribute28; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute29")) { detail.BatchAttribute.Attribute29 = this.Attribute29; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute30")) { detail.BatchAttribute.Attribute30 = this.Attribute30; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute31")) { detail.BatchAttribute.Attribute31 = this.Attribute31; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute32")) { detail.BatchAttribute.Attribute32 = this.Attribute32; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute33")) { detail.BatchAttribute.Attribute33 = this.Attribute33; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute34")) { detail.BatchAttribute.Attribute34 = this.Attribute34; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute35")) { detail.BatchAttribute.Attribute35 = this.Attribute35; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute36")) { detail.BatchAttribute.Attribute36 = this.Attribute36; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute37")) { detail.BatchAttribute.Attribute37 = this.Attribute37; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute38")) { detail.BatchAttribute.Attribute38 = this.Attribute38; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute39")) { detail.BatchAttribute.Attribute39 = this.Attribute39; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute40")) { detail.BatchAttribute.Attribute40 = this.Attribute40; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute41")) { detail.BatchAttribute.Attribute41 = this.Attribute41; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute42")) { detail.BatchAttribute.Attribute42 = this.Attribute42; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute43")) { detail.BatchAttribute.Attribute43 = this.Attribute43; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute44")) { detail.BatchAttribute.Attribute44 = this.Attribute44; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute45")) { detail.BatchAttribute.Attribute45 = this.Attribute45; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute46")) { detail.BatchAttribute.Attribute46 = this.Attribute46; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute47")) { detail.BatchAttribute.Attribute47 = this.Attribute47; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute48")) { detail.BatchAttribute.Attribute48 = this.Attribute48; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute49")) { detail.BatchAttribute.Attribute49 = this.Attribute49; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute50")) { detail.BatchAttribute.Attribute50 = this.Attribute50; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute51")) { detail.BatchAttribute.Attribute51 = this.Attribute51; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute52")) { detail.BatchAttribute.Attribute52 = this.Attribute52; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute53")) { detail.BatchAttribute.Attribute53 = this.Attribute53; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute54")) { detail.BatchAttribute.Attribute54 = this.Attribute54; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute55")) { detail.BatchAttribute.Attribute55 = this.Attribute55; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute56")) { detail.BatchAttribute.Attribute56 = this.Attribute56; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute57")) { detail.BatchAttribute.Attribute57 = this.Attribute57; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute58")) { detail.BatchAttribute.Attribute58 = this.Attribute58; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute59")) { detail.BatchAttribute.Attribute59 = this.Attribute59; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute60")) { detail.BatchAttribute.Attribute60 = this.Attribute60; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute61")) { detail.BatchAttribute.Attribute61 = this.Attribute61; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute62")) { detail.BatchAttribute.Attribute62 = this.Attribute62; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute63")) { detail.BatchAttribute.Attribute63 = this.Attribute63; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute64")) { detail.BatchAttribute.Attribute64 = this.Attribute64; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute65")) { detail.BatchAttribute.Attribute65 = this.Attribute65; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute66")) { detail.BatchAttribute.Attribute66 = this.Attribute66; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute67")) { detail.BatchAttribute.Attribute67 = this.Attribute67; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute68")) { detail.BatchAttribute.Attribute68 = this.Attribute68; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute69")) { detail.BatchAttribute.Attribute69 = this.Attribute69; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute70")) { detail.BatchAttribute.Attribute70 = this.Attribute70; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute71")) { detail.BatchAttribute.Attribute71 = this.Attribute71; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute72")) { detail.BatchAttribute.Attribute72 = this.Attribute72; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute73")) { detail.BatchAttribute.Attribute73 = this.Attribute73; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute74")) { detail.BatchAttribute.Attribute74 = this.Attribute74; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute75")) { detail.BatchAttribute.Attribute75 = this.Attribute75; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute76")) { detail.BatchAttribute.Attribute76 = this.Attribute76; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute77")) { detail.BatchAttribute.Attribute77 = this.Attribute77; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute78")) { detail.BatchAttribute.Attribute78 = this.Attribute78; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute79")) { detail.BatchAttribute.Attribute79 = this.Attribute79; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute80")) { detail.BatchAttribute.Attribute80 = this.Attribute80; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute81")) { detail.BatchAttribute.Attribute81 = this.Attribute81; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute82")) { detail.BatchAttribute.Attribute82 = this.Attribute82; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute83")) { detail.BatchAttribute.Attribute83 = this.Attribute83; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute84")) { detail.BatchAttribute.Attribute84 = this.Attribute84; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute85")) { detail.BatchAttribute.Attribute85 = this.Attribute85; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute86")) { detail.BatchAttribute.Attribute86 = this.Attribute86; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute87")) { detail.BatchAttribute.Attribute87 = this.Attribute87; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute88")) { detail.BatchAttribute.Attribute88 = this.Attribute88; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute89")) { detail.BatchAttribute.Attribute89 = this.Attribute89; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute90")) { detail.BatchAttribute.Attribute90 = this.Attribute90; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute91")) { detail.BatchAttribute.Attribute91 = this.Attribute91; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute92")) { detail.BatchAttribute.Attribute92 = this.Attribute92; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute93")) { detail.BatchAttribute.Attribute93 = this.Attribute93; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute94")) { detail.BatchAttribute.Attribute94 = this.Attribute94; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute95")) { detail.BatchAttribute.Attribute95 = this.Attribute95; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute96")) { detail.BatchAttribute.Attribute96 = this.Attribute96; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute97")) { detail.BatchAttribute.Attribute97 = this.Attribute97; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute98")) { detail.BatchAttribute.Attribute98 = this.Attribute98; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute99")) { detail.BatchAttribute.Attribute99 = this.Attribute99; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute100")) { detail.BatchAttribute.Attribute100 = this.Attribute100; continue; }

                    if (localAttribute.PropertyInfo.Name.Equals("Attribute101")) { detail.BatchAttribute.Attribute101 = this.Attribute101; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute102")) { detail.BatchAttribute.Attribute102 = this.Attribute102; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute103")) { detail.BatchAttribute.Attribute103 = this.Attribute103; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute104")) { detail.BatchAttribute.Attribute104 = this.Attribute104; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute105")) { detail.BatchAttribute.Attribute105 = this.Attribute105; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute106")) { detail.BatchAttribute.Attribute106 = this.Attribute106; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute107")) { detail.BatchAttribute.Attribute107 = this.Attribute107; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute108")) { detail.BatchAttribute.Attribute108 = this.Attribute108; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute109")) { detail.BatchAttribute.Attribute109 = this.Attribute109; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute110")) { detail.BatchAttribute.Attribute110 = this.Attribute110; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute111")) { detail.BatchAttribute.Attribute111 = this.Attribute111; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute112")) { detail.BatchAttribute.Attribute112 = this.Attribute112; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute113")) { detail.BatchAttribute.Attribute113 = this.Attribute113; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute114")) { detail.BatchAttribute.Attribute114 = this.Attribute114; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute115")) { detail.BatchAttribute.Attribute115 = this.Attribute115; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute116")) { detail.BatchAttribute.Attribute116 = this.Attribute116; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute117")) { detail.BatchAttribute.Attribute117 = this.Attribute117; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute118")) { detail.BatchAttribute.Attribute118 = this.Attribute118; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute119")) { detail.BatchAttribute.Attribute119 = this.Attribute119; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute120")) { detail.BatchAttribute.Attribute120 = this.Attribute120; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute121")) { detail.BatchAttribute.Attribute121 = this.Attribute121; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute122")) { detail.BatchAttribute.Attribute122 = this.Attribute122; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute123")) { detail.BatchAttribute.Attribute123 = this.Attribute123; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute124")) { detail.BatchAttribute.Attribute124 = this.Attribute124; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute125")) { detail.BatchAttribute.Attribute125 = this.Attribute125; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute126")) { detail.BatchAttribute.Attribute126 = this.Attribute126; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute127")) { detail.BatchAttribute.Attribute127 = this.Attribute127; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute128")) { detail.BatchAttribute.Attribute128 = this.Attribute128; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute129")) { detail.BatchAttribute.Attribute129 = this.Attribute129; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute130")) { detail.BatchAttribute.Attribute130 = this.Attribute130; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute131")) { detail.BatchAttribute.Attribute131 = this.Attribute131; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute132")) { detail.BatchAttribute.Attribute132 = this.Attribute132; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute133")) { detail.BatchAttribute.Attribute133 = this.Attribute133; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute134")) { detail.BatchAttribute.Attribute134 = this.Attribute134; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute135")) { detail.BatchAttribute.Attribute135 = this.Attribute135; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute136")) { detail.BatchAttribute.Attribute136 = this.Attribute136; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute137")) { detail.BatchAttribute.Attribute137 = this.Attribute137; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute138")) { detail.BatchAttribute.Attribute138 = this.Attribute138; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute139")) { detail.BatchAttribute.Attribute139 = this.Attribute139; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute140")) { detail.BatchAttribute.Attribute140 = this.Attribute140; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute141")) { detail.BatchAttribute.Attribute141 = this.Attribute141; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute142")) { detail.BatchAttribute.Attribute142 = this.Attribute142; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute143")) { detail.BatchAttribute.Attribute143 = this.Attribute143; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute144")) { detail.BatchAttribute.Attribute144 = this.Attribute144; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute145")) { detail.BatchAttribute.Attribute145 = this.Attribute145; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute146")) { detail.BatchAttribute.Attribute146 = this.Attribute146; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute147")) { detail.BatchAttribute.Attribute147 = this.Attribute147; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute148")) { detail.BatchAttribute.Attribute148 = this.Attribute148; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute149")) { detail.BatchAttribute.Attribute149 = this.Attribute149; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute150")) { detail.BatchAttribute.Attribute150 = this.Attribute150; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute151")) { detail.BatchAttribute.Attribute151 = this.Attribute151; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute152")) { detail.BatchAttribute.Attribute152 = this.Attribute152; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute153")) { detail.BatchAttribute.Attribute153 = this.Attribute153; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute154")) { detail.BatchAttribute.Attribute154 = this.Attribute154; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute155")) { detail.BatchAttribute.Attribute155 = this.Attribute155; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute156")) { detail.BatchAttribute.Attribute156 = this.Attribute156; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute157")) { detail.BatchAttribute.Attribute157 = this.Attribute157; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute158")) { detail.BatchAttribute.Attribute158 = this.Attribute158; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute159")) { detail.BatchAttribute.Attribute159 = this.Attribute159; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute160")) { detail.BatchAttribute.Attribute160 = this.Attribute160; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute161")) { detail.BatchAttribute.Attribute161 = this.Attribute161; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute162")) { detail.BatchAttribute.Attribute162 = this.Attribute162; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute163")) { detail.BatchAttribute.Attribute163 = this.Attribute163; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute164")) { detail.BatchAttribute.Attribute164 = this.Attribute164; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute165")) { detail.BatchAttribute.Attribute165 = this.Attribute165; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute166")) { detail.BatchAttribute.Attribute166 = this.Attribute166; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute167")) { detail.BatchAttribute.Attribute167 = this.Attribute167; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute168")) { detail.BatchAttribute.Attribute168 = this.Attribute168; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute169")) { detail.BatchAttribute.Attribute169 = this.Attribute169; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute170")) { detail.BatchAttribute.Attribute170 = this.Attribute170; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute171")) { detail.BatchAttribute.Attribute171 = this.Attribute171; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute172")) { detail.BatchAttribute.Attribute172 = this.Attribute172; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute173")) { detail.BatchAttribute.Attribute173 = this.Attribute173; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute174")) { detail.BatchAttribute.Attribute174 = this.Attribute174; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute175")) { detail.BatchAttribute.Attribute175 = this.Attribute175; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute176")) { detail.BatchAttribute.Attribute176 = this.Attribute176; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute177")) { detail.BatchAttribute.Attribute177 = this.Attribute177; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute178")) { detail.BatchAttribute.Attribute178 = this.Attribute178; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute179")) { detail.BatchAttribute.Attribute179 = this.Attribute179; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute180")) { detail.BatchAttribute.Attribute180 = this.Attribute180; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute181")) { detail.BatchAttribute.Attribute181 = this.Attribute181; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute182")) { detail.BatchAttribute.Attribute182 = this.Attribute182; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute183")) { detail.BatchAttribute.Attribute183 = this.Attribute183; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute184")) { detail.BatchAttribute.Attribute184 = this.Attribute184; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute185")) { detail.BatchAttribute.Attribute185 = this.Attribute185; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute186")) { detail.BatchAttribute.Attribute186 = this.Attribute186; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute187")) { detail.BatchAttribute.Attribute187 = this.Attribute187; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute188")) { detail.BatchAttribute.Attribute188 = this.Attribute188; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute189")) { detail.BatchAttribute.Attribute189 = this.Attribute189; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute190")) { detail.BatchAttribute.Attribute190 = this.Attribute190; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute191")) { detail.BatchAttribute.Attribute191 = this.Attribute191; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute192")) { detail.BatchAttribute.Attribute192 = this.Attribute192; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute193")) { detail.BatchAttribute.Attribute193 = this.Attribute193; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute194")) { detail.BatchAttribute.Attribute194 = this.Attribute194; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute195")) { detail.BatchAttribute.Attribute195 = this.Attribute195; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute196")) { detail.BatchAttribute.Attribute196 = this.Attribute196; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute197")) { detail.BatchAttribute.Attribute197 = this.Attribute197; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute198")) { detail.BatchAttribute.Attribute198 = this.Attribute198; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute199")) { detail.BatchAttribute.Attribute199 = this.Attribute199; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute200")) { detail.BatchAttribute.Attribute200 = this.Attribute200; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute201")) { detail.BatchAttribute.Attribute201 = this.Attribute201; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute202")) { detail.BatchAttribute.Attribute202 = this.Attribute202; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute203")) { detail.BatchAttribute.Attribute203 = this.Attribute203; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute204")) { detail.BatchAttribute.Attribute204 = this.Attribute204; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute205")) { detail.BatchAttribute.Attribute205 = this.Attribute205; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute206")) { detail.BatchAttribute.Attribute206 = this.Attribute206; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute207")) { detail.BatchAttribute.Attribute207 = this.Attribute207; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute208")) { detail.BatchAttribute.Attribute208 = this.Attribute208; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute209")) { detail.BatchAttribute.Attribute209 = this.Attribute209; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute210")) { detail.BatchAttribute.Attribute210 = this.Attribute210; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute211")) { detail.BatchAttribute.Attribute211 = this.Attribute211; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute212")) { detail.BatchAttribute.Attribute212 = this.Attribute212; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute213")) { detail.BatchAttribute.Attribute213 = this.Attribute213; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute214")) { detail.BatchAttribute.Attribute214 = this.Attribute214; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute215")) { detail.BatchAttribute.Attribute215 = this.Attribute215; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute216")) { detail.BatchAttribute.Attribute216 = this.Attribute216; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute217")) { detail.BatchAttribute.Attribute217 = this.Attribute217; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute218")) { detail.BatchAttribute.Attribute218 = this.Attribute218; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute219")) { detail.BatchAttribute.Attribute219 = this.Attribute219; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute220")) { detail.BatchAttribute.Attribute220 = this.Attribute220; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute221")) { detail.BatchAttribute.Attribute221 = this.Attribute221; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute222")) { detail.BatchAttribute.Attribute222 = this.Attribute222; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute223")) { detail.BatchAttribute.Attribute223 = this.Attribute223; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute224")) { detail.BatchAttribute.Attribute224 = this.Attribute224; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute225")) { detail.BatchAttribute.Attribute225 = this.Attribute225; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute226")) { detail.BatchAttribute.Attribute226 = this.Attribute226; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute227")) { detail.BatchAttribute.Attribute227 = this.Attribute227; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute228")) { detail.BatchAttribute.Attribute228 = this.Attribute228; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute229")) { detail.BatchAttribute.Attribute229 = this.Attribute229; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute230")) { detail.BatchAttribute.Attribute230 = this.Attribute230; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute231")) { detail.BatchAttribute.Attribute231 = this.Attribute231; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute232")) { detail.BatchAttribute.Attribute232 = this.Attribute232; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute233")) { detail.BatchAttribute.Attribute233 = this.Attribute233; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute234")) { detail.BatchAttribute.Attribute234 = this.Attribute234; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute235")) { detail.BatchAttribute.Attribute235 = this.Attribute235; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute236")) { detail.BatchAttribute.Attribute236 = this.Attribute236; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute237")) { detail.BatchAttribute.Attribute237 = this.Attribute237; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute238")) { detail.BatchAttribute.Attribute238 = this.Attribute238; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute239")) { detail.BatchAttribute.Attribute239 = this.Attribute239; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute240")) { detail.BatchAttribute.Attribute240 = this.Attribute240; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute241")) { detail.BatchAttribute.Attribute241 = this.Attribute241; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute242")) { detail.BatchAttribute.Attribute242 = this.Attribute242; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute243")) { detail.BatchAttribute.Attribute243 = this.Attribute243; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute244")) { detail.BatchAttribute.Attribute244 = this.Attribute244; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute245")) { detail.BatchAttribute.Attribute245 = this.Attribute245; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute246")) { detail.BatchAttribute.Attribute246 = this.Attribute246; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute247")) { detail.BatchAttribute.Attribute247 = this.Attribute247; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute248")) { detail.BatchAttribute.Attribute248 = this.Attribute248; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute249")) { detail.BatchAttribute.Attribute249 = this.Attribute249; continue; }
                    if (localAttribute.PropertyInfo.Name.Equals("Attribute250")) { detail.BatchAttribute.Attribute250 = this.Attribute250; continue; }

                }
            }

            #endregion
        }

        /// <summary>
        /// reset the attributes.
        /// </summary>
        protected virtual void ClearAttributes()
        {
            #region attributes

            this.Eartag = string.Empty;
            this.DOB = DateTime.Today;
            this.LastMoveDate = null;
            this.AgeInMonths = 0;
            this.SelectedCategory = null;
            this.SelectedCleanliness = null;
            this.SelectedCustomer = null;
            this.SelectedInternalGrade = null;
            //this.SelectedDressSpec = null;
            //this.SelectedKillingType = null;
            this.Breed = null;
            this.Sex = string.Empty;
            this.FarmAssured = Constant.No;
            this.Clipped = Constant.No;
            this.Casualty = Constant.No;
            this.Lame = Constant.No;
            this.Imported = Constant.No;
            this.ThirdParty = Constant.No;
            this.CarcassSupplier = string.Empty;
            this.TBYes = false;
            this.NotInsured = false;
            this.Condemned = false;
            this.Detained = false;
            this.CondemnedSide2 = false;
            this.DetainedSide2 = false;
            this.BurstBelly = false;
            this.LegMissing = false;
            this.Abscess = false;
            this.Halal = string.Empty;
            this.SelectedFatColour = null;
            this.HerdNo = string.Empty;
            this.HoldingNumber = string.Empty;
            this.Identigen = string.Empty;
            this.CountryOfOrigin = null;
            this.RearedIn = null;
            this.DaysOfResidencyCurrent = 0;
            this.DaysOfResidencyTotal = 0;
            this.NumberOfMoves = 0;
            this.SelectedFreezerType = this.FreezerTypes.FirstOrDefault();
            this.FreezerIntakeNumber = null;
            this.IntakeNumber = null;
            this.Attribute1 = string.Empty;
            this.Attribute2 = string.Empty;
            this.Attribute3 = string.Empty;
            this.Attribute4 = string.Empty;
            this.Attribute5 = string.Empty;
            this.Attribute6 = string.Empty;
            this.Attribute7 = string.Empty;
            this.Attribute8 = string.Empty;
            this.Attribute9 = string.Empty;
            this.Attribute10 = string.Empty;
            this.Attribute11 = string.Empty;
            this.Attribute12 = string.Empty;
            this.Attribute13 = string.Empty;
            this.Attribute14 = string.Empty;
            this.Attribute15 = string.Empty;
            this.Attribute16 = string.Empty;
            this.Attribute17 = string.Empty;
            this.Attribute18 = string.Empty;
            this.Attribute19 = string.Empty;
            this.Attribute20 = string.Empty;
            this.Attribute21 = string.Empty;
            this.Attribute22 = string.Empty;
            this.Attribute23 = string.Empty;
            this.Attribute24 = string.Empty;
            this.Attribute25 = string.Empty;
            this.Attribute26 = string.Empty;
            this.Attribute27 = string.Empty;
            this.Attribute28 = string.Empty;
            this.Attribute29 = string.Empty;
            this.Attribute30 = string.Empty;
            this.Attribute31 = string.Empty;
            this.Attribute32 = string.Empty;
            this.Attribute33 = string.Empty;
            this.Attribute34 = string.Empty;
            this.Attribute35 = string.Empty;
            this.Attribute36 = string.Empty;
            this.Attribute37 = string.Empty;
            this.Attribute38 = string.Empty;
            this.Attribute39 = string.Empty;
            this.Attribute40 = string.Empty;
            this.Attribute41 = string.Empty;
            this.Attribute42 = string.Empty;
            this.Attribute43 = string.Empty;
            this.Attribute44 = string.Empty;
            this.Attribute45 = string.Empty;
            this.Attribute46 = string.Empty;
            this.Attribute47 = string.Empty;
            this.Attribute48 = string.Empty;
            this.Attribute49 = string.Empty;
            this.Attribute50 = string.Empty;
            this.Attribute51 = string.Empty;
            this.Attribute52 = string.Empty;
            this.Attribute53 = string.Empty;
            this.Attribute54 = string.Empty;
            this.Attribute55 = string.Empty;
            this.Attribute56 = string.Empty;
            this.Attribute57 = string.Empty;
            this.Attribute58 = string.Empty;
            this.Attribute59 = string.Empty;
            this.Attribute60 = string.Empty;
            this.Attribute61 = string.Empty;
            this.Attribute62 = string.Empty;
            this.Attribute63 = string.Empty;
            this.Attribute64 = string.Empty;
            this.Attribute65 = string.Empty;
            this.Attribute66 = string.Empty;
            this.Attribute67 = string.Empty;
            this.Attribute68 = string.Empty;
            this.Attribute69 = string.Empty;
            this.Attribute70 = string.Empty;
            this.Attribute71 = string.Empty;
            this.Attribute72 = string.Empty;
            this.Attribute73 = string.Empty;
            this.Attribute74 = string.Empty;
            this.Attribute75 = string.Empty;
            this.Attribute76 = string.Empty;
            this.Attribute77 = string.Empty;
            this.Attribute78 = string.Empty;
            this.Attribute79 = string.Empty;
            this.Attribute80 = string.Empty;
            this.Attribute81 = string.Empty;
            this.Attribute82 = string.Empty;
            this.Attribute83 = string.Empty;
            this.Attribute84 = string.Empty;
            this.Attribute85 = string.Empty;
            this.Attribute86 = string.Empty;
            this.Attribute87 = string.Empty;
            this.Attribute88 = string.Empty;
            this.Attribute89 = string.Empty;
            this.Attribute90 = string.Empty;
            this.Attribute91 = string.Empty;
            this.Attribute92 = string.Empty;
            this.Attribute93 = string.Empty;
            this.Attribute94 = string.Empty;
            this.Attribute95 = string.Empty;
            this.Attribute96 = string.Empty;
            this.Attribute97 = string.Empty;
            this.Attribute98 = string.Empty;
            this.Attribute99 = string.Empty;
            this.Attribute100 = string.Empty;

            this.Attribute101 = string.Empty;
            this.Attribute102 = string.Empty;
            this.Attribute103 = string.Empty;
            this.Attribute104 = string.Empty;
            this.Attribute105 = string.Empty;
            this.Attribute106 = string.Empty;
            this.Attribute107 = string.Empty;
            this.Attribute108 = string.Empty;
            this.Attribute109 = string.Empty;
            this.Attribute110 = string.Empty;
            this.Attribute111 = string.Empty;
            this.Attribute112 = string.Empty;
            this.Attribute113 = string.Empty;
            this.Attribute114 = string.Empty;
            this.Attribute115 = string.Empty;
            this.Attribute116 = string.Empty;
            this.Attribute117 = string.Empty;
            this.Attribute118 = string.Empty;
            this.Attribute119 = string.Empty;
            this.Attribute120 = string.Empty;
            this.Attribute121 = string.Empty;
            this.Attribute122 = string.Empty;
            this.Attribute123 = string.Empty;
            this.Attribute124 = string.Empty;
            this.Attribute125 = string.Empty;
            this.Attribute126 = string.Empty;
            this.Attribute127 = string.Empty;
            this.Attribute128 = string.Empty;
            this.Attribute129 = string.Empty;
            this.Attribute130 = string.Empty;
            this.Attribute131 = string.Empty;
            this.Attribute132 = string.Empty;
            this.Attribute133 = string.Empty;
            this.Attribute134 = string.Empty;
            this.Attribute135 = string.Empty;
            this.Attribute136 = string.Empty;
            this.Attribute137 = string.Empty;
            this.Attribute138 = string.Empty;
            this.Attribute139 = string.Empty;
            this.Attribute140 = string.Empty;
            this.Attribute141 = string.Empty;
            this.Attribute142 = string.Empty;
            this.Attribute143 = string.Empty;
            this.Attribute144 = string.Empty;
            this.Attribute145 = string.Empty;
            this.Attribute146 = string.Empty;
            this.Attribute147 = string.Empty;
            this.Attribute148 = string.Empty;
            this.Attribute149 = string.Empty;
            this.Attribute150 = string.Empty;
            this.Attribute151 = string.Empty;
            this.Attribute152 = string.Empty;
            this.Attribute153 = string.Empty;
            this.Attribute154 = string.Empty;
            this.Attribute155 = string.Empty;
            this.Attribute156 = string.Empty;
            this.Attribute157 = string.Empty;
            this.Attribute158 = string.Empty;
            this.Attribute159 = string.Empty;
            this.Attribute160 = string.Empty;
            this.Attribute161 = string.Empty;
            this.Attribute162 = string.Empty;
            this.Attribute163 = string.Empty;
            this.Attribute164 = string.Empty;
            this.Attribute165 = string.Empty;
            this.Attribute166 = string.Empty;
            this.Attribute167 = string.Empty;
            this.Attribute168 = string.Empty;
            this.Attribute169 = string.Empty;
            this.Attribute170 = string.Empty;
            this.Attribute171 = string.Empty;
            this.Attribute172 = string.Empty;
            this.Attribute173 = string.Empty;
            this.Attribute174 = string.Empty;
            this.Attribute175 = string.Empty;
            this.Attribute176 = string.Empty;
            this.Attribute177 = string.Empty;
            this.Attribute178 = string.Empty;
            this.Attribute179 = string.Empty;
            this.Attribute180 = string.Empty;
            this.Attribute181 = string.Empty;
            this.Attribute182 = string.Empty;
            this.Attribute183 = string.Empty;
            this.Attribute184 = string.Empty;
            this.Attribute185 = string.Empty;
            this.Attribute186 = string.Empty;
            this.Attribute187 = string.Empty;
            this.Attribute188 = string.Empty;
            this.Attribute189 = string.Empty;
            this.Attribute190 = string.Empty;
            this.Attribute191 = string.Empty;
            this.Attribute192 = string.Empty;
            this.Attribute193 = string.Empty;
            this.Attribute194 = string.Empty;
            this.Attribute195 = string.Empty;
            this.Attribute196 = string.Empty;
            this.Attribute197 = string.Empty;
            this.Attribute198 = string.Empty;
            this.Attribute199 = string.Empty;
            this.Attribute200 = string.Empty;

            this.Attribute201 = string.Empty;
            this.Attribute202 = string.Empty;
            this.Attribute203 = string.Empty;
            this.Attribute204 = string.Empty;
            this.Attribute205 = string.Empty;
            this.Attribute206 = string.Empty;
            this.Attribute207 = string.Empty;
            this.Attribute208 = string.Empty;
            this.Attribute209 = string.Empty;
            this.Attribute210 = string.Empty;
            this.Attribute211 = string.Empty;
            this.Attribute212 = string.Empty;
            this.Attribute213 = string.Empty;
            this.Attribute214 = string.Empty;
            this.Attribute215 = string.Empty;
            this.Attribute216 = string.Empty;
            this.Attribute217 = string.Empty;
            this.Attribute218 = string.Empty;
            this.Attribute219 = string.Empty;
            this.Attribute220 = string.Empty;
            this.Attribute221 = string.Empty;
            this.Attribute222 = string.Empty;
            this.Attribute223 = string.Empty;
            this.Attribute224 = string.Empty;
            this.Attribute225 = string.Empty;
            this.Attribute226 = string.Empty;
            this.Attribute227 = string.Empty;
            this.Attribute228 = string.Empty;
            this.Attribute229 = string.Empty;
            this.Attribute230 = string.Empty;
            this.Attribute231 = string.Empty;
            this.Attribute232 = string.Empty;
            this.Attribute233 = string.Empty;
            this.Attribute234 = string.Empty;
            this.Attribute235 = string.Empty;
            this.Attribute236 = string.Empty;
            this.Attribute237 = string.Empty;
            this.Attribute238 = string.Empty;
            this.Attribute239 = string.Empty;
            this.Attribute240 = string.Empty;
            this.Attribute241 = string.Empty;
            this.Attribute242 = string.Empty;
            this.Attribute243 = string.Empty;
            this.Attribute244 = string.Empty;
            this.Attribute245 = string.Empty;
            this.Attribute246 = string.Empty;
            this.Attribute247 = string.Empty;
            this.Attribute248 = string.Empty;
            this.Attribute249 = string.Empty;
            this.Attribute250 = string.Empty;


            #endregion
        }

        /// <summary>
        /// Locates the assigned look up attribute at runtime, and gets it the attribute value.
        /// </summary>
        /// <param name="attributeMasterID">The attribute master id.</param>
        protected string GetAttributeValue(int attributeMasterID)
        {
            var lookup = this.AttributeLookUps.FirstOrDefault(x => x.AttributeMasterID == attributeMasterID);
            if (lookup == null)
            {
                return string.Empty;
            }

            var name = lookup.Attribute_Name;
            var typeSource = this.GetType();

            // Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var property in propertyInfo)
            {
                var fieldName = property.Name;
                var match = name.Equals(fieldName);
                if (match)
                {
                    var value = property.GetValue(this, null);

                    if (value == null)
                    {
                        return string.Empty;
                    }

                    return value.ToString();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Locates the assigned look up attribute at runtime, and gets it the attribute value.
        /// </summary>
        /// <param name="attributeName">The attribute name.</param>
        /// <param name="attribute">The value to search.</param>
        protected string GetAttributeValue(string attributeName, Nouvem.Model.DataLayer.Attribute attribute)
        {
            var lookup = this.AttributeLookUps.FirstOrDefault(x => x.AttributeDescription.CompareIgnoringCase(attributeName));
            if (lookup == null)
            {
                return string.Empty;
            }

            var name = lookup.Attribute_Name;
            var typeSource = attribute.GetType();

            // Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var property in propertyInfo)
            {
                var fieldName = property.Name;
                var match = name.Equals(fieldName);
                if (match)
                {
                    var value = property.GetValue(attribute, null);

                    if (value == null)
                    {
                        return string.Empty;
                    }

                    return value.ToString();
                }
            }

            return string.Empty;
        }

        protected void SetBatchAttributeValues(StockDetail batchAttribute)
        {
            #region attributes

            if (!string.IsNullOrEmpty(batchAttribute.Attribute1)) { this.Attribute1 = batchAttribute.Attribute1; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute2)) { this.Attribute2 = batchAttribute.Attribute2; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute3)) { this.Attribute3 = batchAttribute.Attribute3; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute4)) { this.Attribute4 = batchAttribute.Attribute4; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute5)) { this.Attribute5 = batchAttribute.Attribute5; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute6)) { this.Attribute6 = batchAttribute.Attribute6; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute7)) { this.Attribute7 = batchAttribute.Attribute7; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute8)) { this.Attribute8 = batchAttribute.Attribute8; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute9)) { this.Attribute9 = batchAttribute.Attribute9; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute10)) { this.Attribute10 = batchAttribute.Attribute10; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute11)) { this.Attribute11 = batchAttribute.Attribute11; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute12)) { this.Attribute12 = batchAttribute.Attribute12; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute13)) { this.Attribute13 = batchAttribute.Attribute13; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute14)) { this.Attribute14 = batchAttribute.Attribute14; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute15)) { this.Attribute15 = batchAttribute.Attribute15; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute16)) { this.Attribute16 = batchAttribute.Attribute16; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute17)) { this.Attribute17 = batchAttribute.Attribute17; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute18)) { this.Attribute18 = batchAttribute.Attribute18; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute19)) { this.Attribute19 = batchAttribute.Attribute19; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute20)) { this.Attribute20 = batchAttribute.Attribute20; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute21)) { this.Attribute21 = batchAttribute.Attribute21; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute22)) { this.Attribute22 = batchAttribute.Attribute22; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute23)) { this.Attribute23 = batchAttribute.Attribute23; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute24)) { this.Attribute24 = batchAttribute.Attribute24; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute25)) { this.Attribute25 = batchAttribute.Attribute25; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute26)) { this.Attribute26 = batchAttribute.Attribute26; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute27)) { this.Attribute27 = batchAttribute.Attribute27; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute28)) { this.Attribute28 = batchAttribute.Attribute28; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute29)) { this.Attribute29 = batchAttribute.Attribute29; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute30)) { this.Attribute30 = batchAttribute.Attribute30; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute31)) { this.Attribute31 = batchAttribute.Attribute31; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute32)) { this.Attribute32 = batchAttribute.Attribute32; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute33)) { this.Attribute33 = batchAttribute.Attribute33; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute34)) { this.Attribute34 = batchAttribute.Attribute34; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute35)) { this.Attribute35 = batchAttribute.Attribute35; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute36)) { this.Attribute36 = batchAttribute.Attribute36; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute37)) { this.Attribute37 = batchAttribute.Attribute37; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute38)) { this.Attribute38 = batchAttribute.Attribute38; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute39)) { this.Attribute39 = batchAttribute.Attribute39; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute40)) { this.Attribute40 = batchAttribute.Attribute40; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute41)) { this.Attribute41 = batchAttribute.Attribute41; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute42)) { this.Attribute42 = batchAttribute.Attribute42; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute43)) { this.Attribute43 = batchAttribute.Attribute43; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute44)) { this.Attribute44 = batchAttribute.Attribute44; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute45)) { this.Attribute45 = batchAttribute.Attribute45; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute46)) { this.Attribute46 = batchAttribute.Attribute46; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute47)) { this.Attribute47 = batchAttribute.Attribute47; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute48)) { this.Attribute48 = batchAttribute.Attribute48; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute49)) { this.Attribute49 = batchAttribute.Attribute49; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute50)) { this.Attribute50 = batchAttribute.Attribute50; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute51)) { this.Attribute51 = batchAttribute.Attribute51; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute52)) { this.Attribute52 = batchAttribute.Attribute52; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute53)) { this.Attribute53 = batchAttribute.Attribute53; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute54)) { this.Attribute54 = batchAttribute.Attribute54; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute55)) { this.Attribute55 = batchAttribute.Attribute55; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute56)) { this.Attribute56 = batchAttribute.Attribute56; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute57)) { this.Attribute57 = batchAttribute.Attribute57; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute58)) { this.Attribute58 = batchAttribute.Attribute58; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute59)) { this.Attribute59 = batchAttribute.Attribute59; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute60)) { this.Attribute60 = batchAttribute.Attribute60; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute61)) { this.Attribute61 = batchAttribute.Attribute61; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute62)) { this.Attribute62 = batchAttribute.Attribute62; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute63)) { this.Attribute63 = batchAttribute.Attribute63; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute64)) { this.Attribute64 = batchAttribute.Attribute64; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute65)) { this.Attribute65 = batchAttribute.Attribute65; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute66)) { this.Attribute66 = batchAttribute.Attribute66; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute67)) { this.Attribute67 = batchAttribute.Attribute67; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute68)) { this.Attribute68 = batchAttribute.Attribute68; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute69)) { this.Attribute69 = batchAttribute.Attribute69; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute70)) { this.Attribute70 = batchAttribute.Attribute70; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute71)) { this.Attribute71 = batchAttribute.Attribute71; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute72)) { this.Attribute72 = batchAttribute.Attribute72; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute73)) { this.Attribute73 = batchAttribute.Attribute73; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute74)) { this.Attribute74 = batchAttribute.Attribute74; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute75)) { this.Attribute75 = batchAttribute.Attribute75; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute76)) { this.Attribute76 = batchAttribute.Attribute76; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute77)) { this.Attribute77 = batchAttribute.Attribute77; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute78)) { this.Attribute78 = batchAttribute.Attribute78; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute79)) { this.Attribute79 = batchAttribute.Attribute79; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute80)) { this.Attribute80 = batchAttribute.Attribute80; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute81)) { this.Attribute81 = batchAttribute.Attribute81; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute82)) { this.Attribute82 = batchAttribute.Attribute82; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute83)) { this.Attribute83 = batchAttribute.Attribute83; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute84)) { this.Attribute84 = batchAttribute.Attribute84; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute85)) { this.Attribute85 = batchAttribute.Attribute85; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute86)) { this.Attribute86 = batchAttribute.Attribute86; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute87)) { this.Attribute87 = batchAttribute.Attribute87; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute88)) { this.Attribute88 = batchAttribute.Attribute88; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute89)) { this.Attribute89 = batchAttribute.Attribute89; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute90)) { this.Attribute90 = batchAttribute.Attribute90; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute91)) { this.Attribute91 = batchAttribute.Attribute91; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute92)) { this.Attribute92 = batchAttribute.Attribute92; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute93)) { this.Attribute93 = batchAttribute.Attribute93; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute94)) { this.Attribute94 = batchAttribute.Attribute94; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute95)) { this.Attribute95 = batchAttribute.Attribute95; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute96)) { this.Attribute96 = batchAttribute.Attribute96; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute97)) { this.Attribute97 = batchAttribute.Attribute97; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute98)) { this.Attribute98 = batchAttribute.Attribute98; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute99)) { this.Attribute99 = batchAttribute.Attribute99; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute100)) { this.Attribute100 = batchAttribute.Attribute100; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute101)) { this.Attribute101 = batchAttribute.Attribute101; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute102)) { this.Attribute102 = batchAttribute.Attribute102; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute103)) { this.Attribute103 = batchAttribute.Attribute103; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute104)) { this.Attribute104 = batchAttribute.Attribute104; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute105)) { this.Attribute105 = batchAttribute.Attribute105; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute106)) { this.Attribute106 = batchAttribute.Attribute106; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute107)) { this.Attribute107 = batchAttribute.Attribute107; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute108)) { this.Attribute108 = batchAttribute.Attribute108; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute109)) { this.Attribute109 = batchAttribute.Attribute109; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute110)) { this.Attribute110 = batchAttribute.Attribute110; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute111)) { this.Attribute111 = batchAttribute.Attribute111; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute112)) { this.Attribute112 = batchAttribute.Attribute112; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute113)) { this.Attribute113 = batchAttribute.Attribute113; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute114)) { this.Attribute114 = batchAttribute.Attribute114; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute115)) { this.Attribute115 = batchAttribute.Attribute115; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute116)) { this.Attribute116 = batchAttribute.Attribute116; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute117)) { this.Attribute117 = batchAttribute.Attribute117; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute118)) { this.Attribute118 = batchAttribute.Attribute118; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute119)) { this.Attribute119 = batchAttribute.Attribute119; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute120)) { this.Attribute120 = batchAttribute.Attribute120; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute121)) { this.Attribute121 = batchAttribute.Attribute121; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute122)) { this.Attribute122 = batchAttribute.Attribute122; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute123)) { this.Attribute123 = batchAttribute.Attribute123; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute124)) { this.Attribute124 = batchAttribute.Attribute124; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute125)) { this.Attribute125 = batchAttribute.Attribute125; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute126)) { this.Attribute126 = batchAttribute.Attribute126; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute127)) { this.Attribute127 = batchAttribute.Attribute127; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute128)) { this.Attribute128 = batchAttribute.Attribute128; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute129)) { this.Attribute129 = batchAttribute.Attribute129; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute130)) { this.Attribute130 = batchAttribute.Attribute130; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute131)) { this.Attribute131 = batchAttribute.Attribute131; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute132)) { this.Attribute132 = batchAttribute.Attribute132; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute133)) { this.Attribute133 = batchAttribute.Attribute133; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute134)) { this.Attribute134 = batchAttribute.Attribute134; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute135)) { this.Attribute135 = batchAttribute.Attribute135; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute136)) { this.Attribute136 = batchAttribute.Attribute136; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute137)) { this.Attribute137 = batchAttribute.Attribute137; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute138)) { this.Attribute138 = batchAttribute.Attribute138; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute139)) { this.Attribute139 = batchAttribute.Attribute139; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute140)) { this.Attribute140 = batchAttribute.Attribute140; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute141)) { this.Attribute141 = batchAttribute.Attribute141; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute142)) { this.Attribute142 = batchAttribute.Attribute142; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute143)) { this.Attribute143 = batchAttribute.Attribute143; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute144)) { this.Attribute144 = batchAttribute.Attribute144; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute145)) { this.Attribute145 = batchAttribute.Attribute145; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute146)) { this.Attribute146 = batchAttribute.Attribute146; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute147)) { this.Attribute147 = batchAttribute.Attribute147; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute148)) { this.Attribute148 = batchAttribute.Attribute148; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute149)) { this.Attribute149 = batchAttribute.Attribute149; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute150)) { this.Attribute150 = batchAttribute.Attribute150; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute151)) { this.Attribute151 = batchAttribute.Attribute151; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute152)) { this.Attribute152 = batchAttribute.Attribute152; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute153)) { this.Attribute153 = batchAttribute.Attribute153; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute154)) { this.Attribute154 = batchAttribute.Attribute154; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute155)) { this.Attribute155 = batchAttribute.Attribute155; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute156)) { this.Attribute156 = batchAttribute.Attribute156; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute157)) { this.Attribute157 = batchAttribute.Attribute157; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute158)) { this.Attribute158 = batchAttribute.Attribute158; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute159)) { this.Attribute159 = batchAttribute.Attribute159; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute160)) { this.Attribute160 = batchAttribute.Attribute160; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute161)) { this.Attribute161 = batchAttribute.Attribute161; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute162)) { this.Attribute162 = batchAttribute.Attribute162; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute163)) { this.Attribute163 = batchAttribute.Attribute163; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute164)) { this.Attribute164 = batchAttribute.Attribute164; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute165)) { this.Attribute165 = batchAttribute.Attribute165; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute166)) { this.Attribute166 = batchAttribute.Attribute166; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute167)) { this.Attribute167 = batchAttribute.Attribute167; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute168)) { this.Attribute168 = batchAttribute.Attribute168; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute169)) { this.Attribute169 = batchAttribute.Attribute169; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute170)) { this.Attribute170 = batchAttribute.Attribute170; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute171)) { this.Attribute171 = batchAttribute.Attribute171; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute172)) { this.Attribute172 = batchAttribute.Attribute172; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute173)) { this.Attribute173 = batchAttribute.Attribute173; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute174)) { this.Attribute174 = batchAttribute.Attribute174; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute175)) { this.Attribute175 = batchAttribute.Attribute175; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute176)) { this.Attribute176 = batchAttribute.Attribute176; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute177)) { this.Attribute177 = batchAttribute.Attribute177; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute178)) { this.Attribute178 = batchAttribute.Attribute178; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute179)) { this.Attribute179 = batchAttribute.Attribute179; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute180)) { this.Attribute180 = batchAttribute.Attribute180; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute181)) { this.Attribute181 = batchAttribute.Attribute181; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute182)) { this.Attribute182 = batchAttribute.Attribute182; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute183)) { this.Attribute183 = batchAttribute.Attribute183; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute184)) { this.Attribute184 = batchAttribute.Attribute184; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute185)) { this.Attribute185 = batchAttribute.Attribute185; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute186)) { this.Attribute186 = batchAttribute.Attribute186; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute187)) { this.Attribute187 = batchAttribute.Attribute187; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute188)) { this.Attribute188 = batchAttribute.Attribute188; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute189)) { this.Attribute189 = batchAttribute.Attribute189; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute190)) { this.Attribute190 = batchAttribute.Attribute190; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute191)) { this.Attribute191 = batchAttribute.Attribute191; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute192)) { this.Attribute192 = batchAttribute.Attribute192; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute193)) { this.Attribute193 = batchAttribute.Attribute193; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute194)) { this.Attribute194 = batchAttribute.Attribute194; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute195)) { this.Attribute195 = batchAttribute.Attribute195; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute196)) { this.Attribute196 = batchAttribute.Attribute196; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute197)) { this.Attribute197 = batchAttribute.Attribute197; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute198)) { this.Attribute198 = batchAttribute.Attribute198; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute199)) { this.Attribute199 = batchAttribute.Attribute199; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute200)) { this.Attribute200 = batchAttribute.Attribute200; }

            #endregion
        }

        /// <summary>
        /// Sets the attributes.
        /// </summary>
        /// <param name="details">The data to set the attributes to.</param>
        protected void SetAttributeValues(StockDetail details)
        {
            this.Attribute1 = details.Attribute1;
            this.Attribute2 = details.Attribute2;
            this.Attribute3 = details.Attribute3;
            this.Attribute4 = details.Attribute4;
            this.Attribute5 = details.Attribute5;
            this.Attribute6 = details.Attribute6;
            this.Attribute7 = details.Attribute7;
            this.Attribute8 = details.Attribute8;
            this.Attribute9 = details.Attribute9;
            this.Attribute10 = details.Attribute10;
            this.Attribute11 = details.Attribute11;
            this.Attribute12 = details.Attribute12;
            this.Attribute13 = details.Attribute13;
            this.Attribute14 = details.Attribute14;
            this.Attribute15 = details.Attribute15;
            this.Attribute16 = details.Attribute16;
            this.Attribute17 = details.Attribute17;
            this.Attribute18 = details.Attribute18;
            this.Attribute19 = details.Attribute19;
            this.Attribute20 = details.Attribute20;
            this.Attribute21 = details.Attribute21;
            this.Attribute22 = details.Attribute22;
            this.Attribute23 = details.Attribute23;
            this.Attribute24 = details.Attribute24;
            this.Attribute25 = details.Attribute25;
            this.Attribute26 = details.Attribute26;
            this.Attribute27 = details.Attribute27;
            this.Attribute28 = details.Attribute28;
            this.Attribute29 = details.Attribute29;
            this.Attribute30 = details.Attribute30;
            this.Attribute31 = details.Attribute31;
            this.Attribute32 = details.Attribute32;
            this.Attribute33 = details.Attribute33;
            this.Attribute34 = details.Attribute34;
            this.Attribute35 = details.Attribute35;
            this.Attribute36 = details.Attribute36;
            this.Attribute37 = details.Attribute37;
            this.Attribute38 = details.Attribute38;
            this.Attribute39 = details.Attribute39;
            this.Attribute40 = details.Attribute40;
            this.Attribute41 = details.Attribute41;
            this.Attribute42 = details.Attribute42;
            this.Attribute43 = details.Attribute43;
            this.Attribute44 = details.Attribute44;
            this.Attribute45 = details.Attribute45;
            this.Attribute46 = details.Attribute46;
            this.Attribute47 = details.Attribute47;
            this.Attribute48 = details.Attribute48;
            this.Attribute49 = details.Attribute49;
            this.Attribute50 = details.Attribute50;
            this.Attribute51 = details.Attribute51;
            this.Attribute52 = details.Attribute52;
            this.Attribute53 = details.Attribute53;
            this.Attribute54 = details.Attribute54;
            this.Attribute55 = details.Attribute55;
            this.Attribute56 = details.Attribute56;
            this.Attribute57 = details.Attribute57;
            this.Attribute58 = details.Attribute58;
            this.Attribute59 = details.Attribute59;
            this.Attribute60 = details.Attribute60;
            this.Attribute61 = details.Attribute61;
            this.Attribute62 = details.Attribute62;
            this.Attribute63 = details.Attribute63;
            this.Attribute64 = details.Attribute64;
            this.Attribute65 = details.Attribute65;
            this.Attribute66 = details.Attribute66;
            this.Attribute67 = details.Attribute67;
            this.Attribute68 = details.Attribute68;
            this.Attribute69 = details.Attribute69;
            this.Attribute70 = details.Attribute70;
            this.Attribute71 = details.Attribute71;
            this.Attribute72 = details.Attribute72;
            this.Attribute73 = details.Attribute73;
            this.Attribute74 = details.Attribute74;
            this.Attribute75 = details.Attribute75;
            this.Attribute76 = details.Attribute76;
            this.Attribute77 = details.Attribute77;
            this.Attribute78 = details.Attribute78;
            this.Attribute79 = details.Attribute79;
            this.Attribute80 = details.Attribute80;
            this.Attribute81 = details.Attribute81;
            this.Attribute82 = details.Attribute82;
            this.Attribute83 = details.Attribute83;
            this.Attribute84 = details.Attribute84;
            this.Attribute85 = details.Attribute85;
            this.Attribute86 = details.Attribute86;
            this.Attribute87 = details.Attribute87;
            this.Attribute88 = details.Attribute88;
            this.Attribute89 = details.Attribute89;
            this.Attribute90 = details.Attribute90;
            this.Attribute91 = details.Attribute91;
            this.Attribute92 = details.Attribute92;
            this.Attribute93 = details.Attribute93;
            this.Attribute94 = details.Attribute94;
            this.Attribute95 = details.Attribute95;
            this.Attribute96 = details.Attribute96;
            this.Attribute97 = details.Attribute97;
            this.Attribute98 = details.Attribute98;
            this.Attribute99 = details.Attribute99;
            this.Attribute100 = details.Attribute100;

            this.Attribute101 = details.Attribute101;
            this.Attribute102 = details.Attribute102;
            this.Attribute103 = details.Attribute103;
            this.Attribute104 = details.Attribute104;
            this.Attribute105 = details.Attribute105;
            this.Attribute106 = details.Attribute106;
            this.Attribute107 = details.Attribute107;
            this.Attribute108 = details.Attribute108;
            this.Attribute109 = details.Attribute109;
            this.Attribute110 = details.Attribute110;
            this.Attribute111 = details.Attribute111;
            this.Attribute112 = details.Attribute112;
            this.Attribute113 = details.Attribute113;
            this.Attribute114 = details.Attribute114;
            this.Attribute115 = details.Attribute115;
            this.Attribute116 = details.Attribute116;
            this.Attribute117 = details.Attribute117;
            this.Attribute118 = details.Attribute118;
            this.Attribute119 = details.Attribute119;
            this.Attribute120 = details.Attribute120;
            this.Attribute121 = details.Attribute121;
            this.Attribute122 = details.Attribute122;
            this.Attribute123 = details.Attribute123;
            this.Attribute124 = details.Attribute124;
            this.Attribute125 = details.Attribute125;
            this.Attribute126 = details.Attribute126;
            this.Attribute127 = details.Attribute127;
            this.Attribute128 = details.Attribute128;
            this.Attribute129 = details.Attribute129;
            this.Attribute130 = details.Attribute130;
            this.Attribute131 = details.Attribute131;
            this.Attribute132 = details.Attribute132;
            this.Attribute133 = details.Attribute133;
            this.Attribute134 = details.Attribute134;
            this.Attribute135 = details.Attribute135;
            this.Attribute136 = details.Attribute136;
            this.Attribute137 = details.Attribute137;
            this.Attribute138 = details.Attribute138;
            this.Attribute139 = details.Attribute139;
            this.Attribute140 = details.Attribute140;
            this.Attribute141 = details.Attribute141;
            this.Attribute142 = details.Attribute142;
            this.Attribute143 = details.Attribute143;
            this.Attribute144 = details.Attribute144;
            this.Attribute145 = details.Attribute145;
            this.Attribute146 = details.Attribute146;
            this.Attribute147 = details.Attribute147;
            this.Attribute148 = details.Attribute148;
            this.Attribute149 = details.Attribute149;
            this.Attribute150 = details.Attribute150;
            this.Attribute151 = details.Attribute151;
            this.Attribute152 = details.Attribute152;
            this.Attribute153 = details.Attribute153;
            this.Attribute154 = details.Attribute154;
            this.Attribute155 = details.Attribute155;
            this.Attribute156 = details.Attribute156;
            this.Attribute157 = details.Attribute157;
            this.Attribute158 = details.Attribute158;
            this.Attribute159 = details.Attribute159;
            this.Attribute160 = details.Attribute160;
            this.Attribute161 = details.Attribute161;
            this.Attribute162 = details.Attribute162;
            this.Attribute163 = details.Attribute163;
            this.Attribute164 = details.Attribute164;
            this.Attribute165 = details.Attribute165;
            this.Attribute166 = details.Attribute166;
            this.Attribute167 = details.Attribute167;
            this.Attribute168 = details.Attribute168;
            this.Attribute169 = details.Attribute169;
            this.Attribute170 = details.Attribute170;
            this.Attribute171 = details.Attribute171;
            this.Attribute172 = details.Attribute172;
            this.Attribute173 = details.Attribute173;
            this.Attribute174 = details.Attribute174;
            this.Attribute175 = details.Attribute175;
            this.Attribute176 = details.Attribute176;
            this.Attribute177 = details.Attribute177;
            this.Attribute178 = details.Attribute178;
            this.Attribute179 = details.Attribute179;
            this.Attribute180 = details.Attribute180;
            this.Attribute181 = details.Attribute181;
            this.Attribute182 = details.Attribute182;
            this.Attribute183 = details.Attribute183;
            this.Attribute184 = details.Attribute184;
            this.Attribute185 = details.Attribute185;
            this.Attribute186 = details.Attribute186;
            this.Attribute187 = details.Attribute187;
            this.Attribute188 = details.Attribute188;
            this.Attribute189 = details.Attribute189;
            this.Attribute190 = details.Attribute190;
            this.Attribute191 = details.Attribute191;
            this.Attribute192 = details.Attribute192;
            this.Attribute193 = details.Attribute193;
            this.Attribute194 = details.Attribute194;
            this.Attribute195 = details.Attribute195;
            this.Attribute196 = details.Attribute196;
            this.Attribute197 = details.Attribute197;
            this.Attribute198 = details.Attribute198;
            this.Attribute199 = details.Attribute199;
            this.Attribute200 = details.Attribute200;

            this.Attribute201 = details.Attribute201;
            this.Attribute202 = details.Attribute202;
            this.Attribute203 = details.Attribute203;
            this.Attribute204 = details.Attribute204;
            this.Attribute205 = details.Attribute205;
            this.Attribute206 = details.Attribute206;
            this.Attribute207 = details.Attribute207;
            this.Attribute208 = details.Attribute208;
            this.Attribute209 = details.Attribute209;
            this.Attribute210 = details.Attribute210;
            this.Attribute211 = details.Attribute211;
            this.Attribute212 = details.Attribute212;
            this.Attribute213 = details.Attribute213;
            this.Attribute214 = details.Attribute214;
            this.Attribute215 = details.Attribute215;
            this.Attribute216 = details.Attribute216;
            this.Attribute217 = details.Attribute217;
            this.Attribute218 = details.Attribute218;
            this.Attribute219 = details.Attribute219;
            this.Attribute220 = details.Attribute220;
            this.Attribute221 = details.Attribute221;
            this.Attribute222 = details.Attribute222;
            this.Attribute223 = details.Attribute223;
            this.Attribute224 = details.Attribute224;
            this.Attribute225 = details.Attribute225;
            this.Attribute226 = details.Attribute226;
            this.Attribute227 = details.Attribute227;
            this.Attribute228 = details.Attribute228;
            this.Attribute229 = details.Attribute229;
            this.Attribute230 = details.Attribute230;
            this.Attribute231 = details.Attribute231;
            this.Attribute232 = details.Attribute232;
            this.Attribute233 = details.Attribute233;
            this.Attribute234 = details.Attribute234;
            this.Attribute235 = details.Attribute235;
            this.Attribute236 = details.Attribute236;
            this.Attribute237 = details.Attribute237;
            this.Attribute238 = details.Attribute238;
            this.Attribute239 = details.Attribute239;
            this.Attribute240 = details.Attribute240;
            this.Attribute241 = details.Attribute241;
            this.Attribute242 = details.Attribute242;
            this.Attribute243 = details.Attribute243;
            this.Attribute244 = details.Attribute244;
            this.Attribute245 = details.Attribute245;
            this.Attribute246 = details.Attribute246;
            this.Attribute247 = details.Attribute247;
            this.Attribute248 = details.Attribute248;
            this.Attribute249 = details.Attribute249;
            this.Attribute250 = details.Attribute250;

        }

        /// <summary>
        /// Locates the input attributeat runtime, and assigns it the attribute value.
        /// </summary>
        /// <param name="name">The attribute to look for.</param>
        /// <param name="attributeValue">The value to assign.</param>
        protected void SetAttributeValue(string name, string attributeValue)
        {
            try
            {
                var typeSource = this.GetType();

                // Get all the properties of source object type
                var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                foreach (var property in propertyInfo)
                {
                    var fieldName = property.Name;
                    var match = name.Equals(fieldName);

                    if (match)
                    {
                        property.SetValue(this, attributeValue);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"SetAttributeValue:{e.Message}, value:{attributeValue}");
            }
        }

        /// <summary>
        /// Locates the assigned look up attribute at runtime, and assigns it the attribute value.
        /// </summary>
        /// <param name="master">The attribute master.</param>
        /// <param name="attributeValue">The value to assign.</param>
        protected void SetAttributeValue(AttributeMaster master, string attributeValue)
        {
            try
            {
                var lookup = this.AttributeLookUps.FirstOrDefault(x => x.AttributeMasterID == master.AttributeMasterID);
                if (lookup == null)
                {
                    return;
                }

                var name = lookup.Attribute_Name;
                var typeSource = this.GetType();

                // Get all the properties of source object type
                var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                foreach (var property in propertyInfo)
                {
                    var fieldName = property.Name;
                    var match = name.Equals(fieldName);

                    if (match)
                    {
                        property.SetValue(this, attributeValue);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(),$"SetAttributeValue:{e.Message}, Is attribute master null? {master == null}, value:{attributeValue}");
            }
        }

        /// <summary>
        /// Gets the animal freezer types.
        /// </summary>
        protected void GetFreezerTypes()
        {
            this.FreezerTypes = new ObservableCollection<CollectionData>();
            this.FreezerTypes.Add(new CollectionData { Data = Strings.NoneSelected });
            this.FreezerTypes.Add(new CollectionData { Data = Strings.Full });
            this.FreezerTypes.Add(new CollectionData { Data = Strings.Side });
            this.FreezerTypes.Add(new CollectionData { Data = Strings.ForeQtr });
            this.FreezerTypes.Add(new CollectionData { Data = Strings.HindQtr });
            this.SelectedFreezerType = this.FreezerTypes.First();
        }

        /// <summary>
        /// Save the batch attribute selection.
        /// </summary>
        /// <param name="batchId">The batch id.</param>
        /// <returns>Flag, as to successful save.</returns>
        protected int SaveBatchData(int batchId)
        {
            var stockDetail = new StockDetail { BatchNumberID = batchId };
            this.CreateAttribute(stockDetail);
            return this.DataManager.AddOrUpdateBatchAttributes(stockDetail);
        }

        /// <summary>
        /// Locates the assigned look up attribute at runtime, and assigns it the attribute value.
        /// </summary>
        /// <param name="master">The attribute master.</param>
        protected PropertyInfo GetProperty(AttributeMaster master)
        {
            var lookup = this.AttributeLookUps.FirstOrDefault(x => x.AttributeMasterID == master.AttributeMasterID);
            if (lookup == null)
            {
                return null;
            }

            var name = lookup.Attribute_Name;
            var typeSource = this.GetType();

            // Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var property in propertyInfo)
            {
                var fieldName = property.Name;
                var match = name.Equals(fieldName);

                if (match)
                {
                    return property;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the users to alert.
        /// </summary>
        protected void GetAlertUsers()
        {
            this.AlertUsers = this.DataManager.GetAlertUsers();
        }

        /// <summary>
        /// Log the alerts.
        /// </summary>
        protected void LogAlerts(AttributeAllocationData currentProcess, int? workflowId = null, int? apGoodsReceiptId = null, int? prOrderID = null, int? processId = null, int? arDispatchID = null)
        {
            // we need to log this as an alert.
            var userIds = new HashSet<int>();
            if (this.AlertUsers.UserAlerts != null)
            {
                var localUserAlerts =
                    this.AlertUsers.UserAlerts.Where(
                        x => x.AttributeMasterID == currentProcess.AttributeMaster.AttributeMasterID);
                foreach (var userAlert in localUserAlerts)
                {
                    userIds.Add(userAlert.UserMasterID);
                }
            }

            if (this.AlertUsers.UserGroupAlerts != null)
            {
                var localUserGroupAlerts =
                    this.AlertUsers.UserGroupAlerts.Where(
                        x => x.AttributeMasterID == currentProcess.AttributeMaster.AttributeMasterID);

                foreach (var localUserGroupAlert in localUserGroupAlerts)
                {
                    var userGroupId = localUserGroupAlert.UserGroupID;
                    var users = NouvemGlobal.Users.Where(x => x.UserMaster.UserGroupID == userGroupId);
                    foreach (var user in users)
                    {
                        userIds.Add(user.UserMaster.UserMasterID);
                    }
                }
            }

            if (userIds.Any())
            {
                var message = currentProcess.AttributeMaster.AlertMessage ?? string.Empty;
                var attributeMasterId = currentProcess.AttributeMaster.AttributeMasterID;
                currentProcess.AlertsRecorded = true;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.DataManager.AddUserAlerts(userIds, message, attributeMasterId, workflowId, apGoodsReceiptId, prOrderID, processId, arDispatchID);
                }));
            }
        }

        /// <summary>
        /// Clears the ui controls.
        /// </summary>
        protected void ClearAttributeControls()
        {
            Messenger.Default.Send(Token.Message, Token.ClearAttributeControls);
        }

        /// <summary>
        /// Gets the attribute look ups.
        /// </summary>
        protected void GetAttributeLookUps()
        {
            this.AttributeLookUps = this.DataManager.GetAttributeLookUps();
        }

        /// <summary>
        /// Gets the destinations.
        /// </summary>
        protected void GetDestinations()
        {
            this.Destinations = this.DataManager.GetDestinations().Select(x => x.Name).ToList();
        }

        /// <summary>
        /// Gets the animal fat colours.
        /// </summary>
        protected void GetFatColours()
        {
            this.FatColours = new ObservableCollection<NouFatColour>(this.DataManager.GetFatColours());
        }

        /// <summary>
        /// Gets the animal breeds.
        /// </summary>
        protected virtual void GetBreeds()
        {
            this.Breeds = new ObservableCollection<NouBreed>(this.DataManager.GetBreeds());
        }

        /// <summary>
        /// Gets the animal breeds.
        /// </summary>
        protected virtual void GetInternalGrades()
        {
            this.internalGrades = new ObservableCollection<InternalGrade>(this.DataManager.GetInternalGrades());
        }

        /// <summary>
        /// Gets the animal sexes.
        /// </summary>
        protected void GetSexes()
        {
            this.Sexes = new List<string> { Strings.Male, Strings.Female };
        }

        /// <summary>
        /// Gets the animal sexes.
        /// </summary>
        protected virtual void GetCountries()
        {
            this.Countries = this.DataManager.GetCountryMaster();
        }

        /// <summary>
        /// Gets the animal sexes.
        /// </summary>
        protected virtual void GetPlants()
        {
            this.Plants = this.DataManager.GetAllPlants();
        }

        /// <summary>
        /// Gets the farm assured values.
        /// </summary>
        protected void GetFAValues()
        {
            this.YesNoValues = new List<string> { Strings.Yes, Strings.No };
        }

        /// <summary>
        /// Gets the animal sexes.
        /// </summary>
        protected virtual void GetCategories()
        {
            this.Categories = new ObservableCollection<NouCategory>(this.DataManager.GetCategories());
        }

        /// <summary>
        /// Gets the kill types.
        /// </summary>
        protected void GetKillTypes()
        {
            this.KillTypes = new ObservableCollection<NouKillType>(NouvemGlobal.NouKillTypes);
        }

        /// <summary>
        /// Gets the cleanliness values.
        /// </summary>
        protected void GetCleanlinesses()
        {
            this.Cleanlinesses = this.DataManager.GetCleanlinesses();
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        protected virtual void GetCustomers()
        {
            this.Customers = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.CustomerPartners.Select(x => x.Details));
        }

        private void SetDates(AttributeAllocationData attributeAllocationData, IList<AttributeAllocationData> attributes, int inMasterId)
        {
            string localDate;

            if (attributeAllocationData.DateType.CompareIgnoringCase(DateType.EnteredDate))
            {
                // entered date, so nothing to do.
                return;
            }

            if (attributeAllocationData.DateType.CompareIgnoringCase(DateType.CurrentDate))
            {
                // current date, so apply today's date
                localDate = DateTime.Today.ToShortDateString();
                attributeAllocationData.PropertyInfo.SetValue(this, localDate);
                return;
            }

            // forward or back date, so get it's days forward
            var daysForward = this.dateDays.FirstOrDefault(x => x.INMasterID == inMasterId
                && x.AttributeMasterID == attributeAllocationData.AttributeMaster.AttributeMasterID);

            var days = 0;
            if (daysForward != null && !daysForward.Days.IsNullOrZero())
            {
                days = daysForward.Days.ToInt();
            }

            if (attributeAllocationData.AttributeMaster.AttributeMasterID_BasedOn != null)
            {
                // based on another date, so apply the days forward/back to that date
                var basedOnAttributeAllocationdata =
                  attributes.FirstOrDefault(
                      x =>
                          x.AttributeMaster.AttributeMasterID ==
                          attributeAllocationData.AttributeMaster.AttributeMasterID_BasedOn);

                if (basedOnAttributeAllocationdata != null)
                {
                    var basedOnValue =
                        this.GetAttributeValue(
                            basedOnAttributeAllocationdata.AttributeMaster.AttributeMasterID);

                    if (!string.IsNullOrWhiteSpace(basedOnValue))
                    {
                        var basedOnDate = basedOnValue.ToDate();

                        if (attributeAllocationData.DateType.CompareIgnoringCase(DateType.ForwardDate))
                        {
                            // forward date
                            localDate = basedOnDate.Add(TimeSpan.FromDays(days)).ToShortDateString();
                        }
                        else
                        {
                            // back date
                            localDate = basedOnDate.Add(TimeSpan.FromDays(-days)).ToShortDateString();
                        }

                        attributeAllocationData.PropertyInfo.SetValue(this, localDate);
                        return;
                    }
                }
            }

            // forward/back date not based on another date
            if (attributeAllocationData.DateType.CompareIgnoringCase(DateType.ForwardDate))
            {
                // forward date
                localDate = DateTime.Today.Add(TimeSpan.FromDays(days)).ToShortDateString();
            }
            else
            {
                // back date
                localDate = DateTime.Today.Add(TimeSpan.FromDays(-days)).ToShortDateString();
            }

            attributeAllocationData.PropertyInfo.SetValue(this, localDate);
        }

        /// <summary>
        /// Checks the input touchscreen atttribute value.
        /// </summary>
        /// <param name="data">THe associated attribute data.</param>
        private void CheckAttributeValue(AttributeAllocationData data)
        {
            this.UnsavedAttributes = true;
            var responseSp = data.AttributeMaster.StandardResponseChecker;
            if (!string.IsNullOrWhiteSpace(responseSp))
            {
                int id;
                var referenceId = this.GetReferenceId();
                if (responseSp.EndsWithIgnoringCase(Constant.WithProduct))
                {
                    id = this.GetWorkflowProductId();
                }
                else
                {
                    id = this.GetWorkflowDocketId();
                }

                if (!responseSp.StartsWithIgnoringCase(Constant.MacroReturn))
                {
                    // standard boolean validation result
                    var result = this.DataManager.GetSPResult(responseSp, data.TraceabilityValue, id);
                    this.HandleValidationResponse(result.Item2);
                    if (!result.Item1)
                    {
                        Messenger.Default.Send(Token.Message, Token.CreateAttributeResponse);
                        Messenger.Default.Send(data, Token.CreateAttributeResponse);
                    }
                }
                else
                {
                    // calculated value returned
                    var result = this.DataManager.GetSPResultReturn(responseSp, data.TraceabilityValue, id, referenceId);
                    var resultValue = result.Item1 ?? string.Empty;
                    var resultMessage = result.Item2 ?? string.Empty;
                    var setOtherAttributeValue = result.Item3 ?? string.Empty;
                    if (!string.IsNullOrEmpty(setOtherAttributeValue))
                    {
                        this.SetAttributeValue(setOtherAttributeValue, resultValue);
                    }
                    else
                    {
                        this.SetAttributeValue(data.AttributeMaster, resultValue);
                    }

                    this.HandleValidationResponse(resultMessage);
                }
            }

            var postSelectionSp = data.AttributionAllocation.PostSelectionMACRO;
            if (!string.IsNullOrWhiteSpace(postSelectionSp))
            {
                var result = this.DataManager.GetSPResult(postSelectionSp, data.TraceabilityValue, this.GetWorkflowDocketId());
                this.HandleValidationResponse(result.Item2);
                if (result.Item1)
                {
                    // result of true means refresh to observers
                    Messenger.Default.Send(data.TraceabilityValue, Token.PostSelectionMacroRefresh);
                }
            }

            if (ApplicationSettings.SaveBatchAttributesOnEntry)
            {
                // remove current control focus, to ensure binding
                Messenger.Default.Send(Token.Message, Token.FocusToAttributeButton);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.SaveBatchAttributes(ignoreChecks: true);
                }));
            }
        }

        protected virtual void HandleValidationResponse(string response)
        {
            if (string.IsNullOrWhiteSpace(response))
            {
                return;
            }

            NouvemMessageBox.Show(response, touchScreen: true);
        }

        /// <summary>
        /// Sets any attributes that are dependant on the based on date when the based on date's value is changed on the UI.
        /// </summary>
        /// <param name="data">The based on date data.</param>
        private void CheckBasedOnDates(AttributeAllocationData data)
        {
            #region validation

            if (data == null || this.attributes == null || data.AttributeMaster == null || string.IsNullOrWhiteSpace(data.TraceabilityValue))
            {
                return;
            }

            #endregion

            try
            {
                var basedOndate = data.TraceabilityValue.ToDate();

                var dependantDates =
                    this.attributes.Where(
                        x => x.AttributeMaster != null && x.AttributeMaster.AttributeMasterID_BasedOn == data.AttributeMaster.AttributeMasterID);

                foreach (var attributeAllocationData in dependantDates)
                {
                    if (!string.IsNullOrEmpty(attributeAllocationData.DateType) && attributeAllocationData.DateType.CompareIgnoringCase(DateType.ForwardDate))
                    {
                        var daysForward = this.dateDays.FirstOrDefault(x => x.INMasterID == attributeAllocationData.INMasterID
                           && x.AttributeMasterID == attributeAllocationData.AttributeMaster.AttributeMasterID);

                        if (daysForward != null)
                        {
                            var localDays = daysForward.Days.ToInt();
                            var date = basedOndate.Add(TimeSpan.FromDays(localDays)).ToShortDateString();
                            attributeAllocationData.PropertyInfo.SetValue(this, date);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("CheckBasedOnDates(): {0}", ex.Message));
            }
        }

        #endregion

        protected override void ControlSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            // throw new NotImplementedException();
        }
    }
}
