﻿
// -----------------------------------------------------------------------
// <copyright file="TouchscreenOrdersViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Purchases.APReceipt
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class TouchscreenOrdersViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The partner search box name.
        /// </summary>
        private string partnerSearchName;

        // All the orders.
        private ObservableCollection<Sale> allOrders = new ObservableCollection<Sale>();

        // The orders to display.
        private ObservableCollection<Sale> orders = new ObservableCollection<Sale>();

        /// <summary>
        /// The open order status id's.
        /// </summary>
        private IList<int?> orderStatuses = new List<int?>{NouvemGlobal.NouDocStatusActive.NouDocStatusID, NouvemGlobal.NouDocStatusFilled.NouDocStatusID, NouvemGlobal.NouDocStatusInProgress.NouDocStatusID, NouvemGlobal.NouDocStatusMoved.NouDocStatusID }; 

        /// <summary>
        /// The selected sale.
        /// </summary>
        private Sale sale;

        /// <summary>
        /// Multi select text.
        /// </summary>
        private string multiSelectText;

        /// <summary>
        /// Multi select text.
        /// </summary>
        private bool multiSelectMode;

        /// <summary>
        /// The search/filter text.
        /// </summary>
        private string ordersSearchText;

        /// <summary>
        /// The delivery/shipping name.
        /// </summary>
        private string deliveryDateName;

        /// <summary>
        /// Flag, as to whether the data is to be refreshed.
        /// </summary>
        private bool dontRefreshData;

        /// <summary>
        /// The sort by delivery date text.
        /// </summary>
        private string deliveryDateSort;

        /// <summary>
        /// The sort by route text.
        /// </summary>
        private string routeSort;

        /// <summary>
        /// The sort by order received text.
        /// </summary>
        private string orderReceivedSort;

        /// <summary>
        /// The sort by supplier text.
        /// </summary>
        private string supplierSort;

        /// <summary>
        /// Show the grid group panel flag.
        /// </summary>
        private bool showGroupPanel;

        /// <summary>
        /// The view we navigated from.
        /// </summary>
        private ViewType navigatedFromView;

        /// <summary>
        /// The incoming partner.
        /// </summary>
        private ViewBusinessPartner partner;

        /// <summary>
        /// The date the order was created.
        /// </summary>
        private DateTime? orderReceivedDate;

        /// <summary>
        /// The display date.
        /// </summary>
        private string displayOrderDate;

        /// <summary>
        /// The all partners reference.
        /// </summary>
        private ViewBusinessPartner allPartners = new ViewBusinessPartner{ Name = Strings.AllPartners };

        /// <summary>
        /// The master module.
        /// </summary>
        private ViewType module;

        /// <summary>
        /// Flag, to signify an incoming list of orders.
        /// </summary>
        private bool incomingSearchSales;

      
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenOrdersViewModel"/> class.
        /// </summary>
        public TouchscreenOrdersViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<List<Sale>>(this, Token.SendOrders, this.HandleMultipleOrderSelection);

            // Register to assign the incoming supplier sales.
            Messenger.Default.Register<Tuple<ViewBusinessPartner, ViewType>>(this, Token.DisplayPartnerSales, o =>
            {
                if (!this.TouchscreenModuleLoaded)
                {
                    this.TouchscreenModuleLoaded = true;
                    this.HandleIncomingSales(o);
                    return;
                }

                this.HandleIncomingSales(o);
                //Task.Factory.StartNew(() => Application.Current.Dispatcher.BeginInvoke(
                //   DispatcherPriority.Background,
                //      new Action(() => this.HandleIncomingSales(o))));
            });


            // Register to assign the incoming supplier sales.
            Messenger.Default.Register<Tuple<ViewBusinessPartner, ViewType>>(this, Token.DisplaySupplierSales, o =>
            {
                if (!this.TouchscreenModuleLoaded)
                {
                    this.TouchscreenModuleLoaded = true;
                    this.HandleIncomingSales(o);
                    return;
                }

                this.HandleIncomingSales(o);
                //Task.Factory.StartNew(() => Application.Current.Dispatcher.BeginInvoke(
                //   DispatcherPriority.Background,
                //      new Action(() => this.HandleIncomingSales(o))));
            });

            // Register to assign the incoming supplier sales.
            Messenger.Default.Register<Tuple<ViewBusinessPartner, ViewType, List<Sale>>>(this, Token.DisplaySupplierSales, o =>
            {
                this.dontRefreshData = true;
                if (ApplicationSettings.ScannerMode)
                {
                    this.TouchscreenModuleLoaded = true;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.SetIncomingSales(o);
                    }));

                    return;
                }

                if (!this.TouchscreenModuleLoaded)
                {
                    this.TouchscreenModuleLoaded = true;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.SetIncomingSales(o);
                    }));

                    return;
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.SetIncomingSales(o);
                }));
            });

            #endregion

            #region command handler

            this.RefreshCommand = new RelayCommand(() =>
            {
                this.GetAllOrders();
                this.RefreshFiltersCommandExecute();
                SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
            });

            // Search for an order.
            this.FindOrderCommand = new RelayCommand(() =>
            {
                this.clearingSalesSearch = true;
                this.OrdersSearchText = string.Empty;
                this.clearingSalesSearch = false;
                Messenger.Default.Send(Token.Message, Token.DisplayOrderKeyboard);
            });

            // Handle the orders load event.
            this.OnOrdersLoadingCommand = new RelayCommand(this.OnOrdersLoadingCommandExecute);

            // Handle the orders unload event.
            this.OnOrdersUnloadedCommand = new RelayCommand(this.OnOrdersUnloadedCommandExecute);

            // Handler to turn the goup panel display on/off.
            this.ShowGroupPanelCommand = new RelayCommand(() => this.ShowGroupPanel = !this.ShowGroupPanel);

            // Scroll the suppliers grid.
            this.ScrollCommand = new RelayCommand<string>(s => Messenger.Default.Send(s, Token.ScrollOrders));

            // Handle the selection of a supplier sale.
            this.SaleSelectedCommand = new RelayCommand(this.SaleSelectedCommandExecute, () => this.Sale != null);

            // Handle the command to sort the grid.
            this.SortByCommand = new RelayCommand<string>(this.SortByCommandExecute);

            // Move back to main ui or suppliers screen.
            this.MoveBackCommand = new RelayCommand(this.MoveBackCommandExecute);

            // Create a new order for the selected supplier.
            this.CreateOrderCommand = new RelayCommand(this.CreateOrderCommandExecute);

            // Refreshes the filters.
            this.RefreshFiltersCommand = new RelayCommand(this.RefreshFiltersCommandExecute);

            #endregion

            this.OrderReceivedDate = null;
            this.DeliveryDateSort = ApplicationSettings.DispatchOrdersDateSort;
            this.DeliveryDateName = ApplicationSettings.DispatchOrdersDateType;
            this.MultiSelectMode = false;
            this.RefreshFilters();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the route id.
        /// </summary>
        public int? RouteId { get; set; }

        /// <summary>
        /// Gets or set a value indicating whether we are searching by an attribute or not.
        /// </summary>
        public bool SearchByAttribute { get; set; }

        /// <summary>
        /// Gets or sets the text for the multi select button.
        /// </summary>
        public bool MultiSelectMode
        {
            get
            {
                return this.multiSelectMode;
            }

            set
            {
                this.multiSelectMode = value;
                this.RaisePropertyChanged();
                if (value)
                {
                    this.MultiSelectText = Strings.Complete;
                }
                else
                {
                    Messenger.Default.Send(Token.Message, Token.GetOrders);
                    this.MultiSelectText = Strings.MultiSelect;
                }
            }
        }

        /// <summary>
        /// Gets or sets the text for the multi select button.
        /// </summary>
        public string MultiSelectText
        {
            get
            {
                return this.multiSelectText;
            }

            set
            {
                this.multiSelectText = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the del/shipping name.
        /// </summary>
        public string DeliveryDateName
        {
            get
            {
                return this.deliveryDateName;
            }

            set
            {
                this.deliveryDateName = value;
                this.RaisePropertyChanged();
                ApplicationSettings.DispatchOrdersDateType = value;
                this.SortByDates(false);
            }
        }
      
        /// <summary>
        /// Gets or sets the partner search name.
        /// </summary>
        public string PartnerSearchName
        {
            get
            {
                return this.partnerSearchName;
            }

            set
            {
                this.partnerSearchName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the orders serch/filter text.
        /// </summary>
        public string OrdersSearchText
        {
            get
            {
                return this.ordersSearchText;
            }

            set
            {
                this.ordersSearchText = value;
                this.RaisePropertyChanged();

                if (!this.IsFormLoaded)
                {
                    return;
                }

                if (string.IsNullOrEmpty(value))
                {
                    if (!this.clearingSalesSearch)
                    {
                        this.DisplayOrders();
                    }
                }
                else
                {
                    this.DisplayOrders();
                }
            }
        }

        /// <summary>
        /// Gets all the orders.
        /// </summary>
        public ObservableCollection<Sale> AllOrders
        {
            get
            {
                return this.allOrders;
            }
        }

        /// <summary>
        /// Gets or sets the order creation date to search for.
        /// </summary>
        public DateTime? OrderReceivedDate
        {
            get
            {
                return this.orderReceivedDate;
            }

            set
            {
                this.orderReceivedDate = value;
                this.RaisePropertyChanged();
                this.DisplayOrderDate = value.HasValue ? value.ToDate().ToShortDateString() : Strings.AllDates; 
                Messenger.Default.Send(false, Token.DisplayCalender);

                if (value.HasValue)
                {
                    this.SortByDates(false);
                }
            }
        }

        /// <summary>
        /// Gets or sets the display order creation date.
        /// </summary>
        public string DisplayOrderDate
        {
            get
            {
                return this.displayOrderDate;
            }

            set
            {
                this.displayOrderDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the incoming partner.
        /// </summary>
        public ViewBusinessPartner Partner
        {
            get
            {
                return this.partner;
            }

            set
            {
                this.partner = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the grid couup panel is displayed.
        /// </summary>
        public bool ShowGroupPanel
        {
            get
            {
                return this.showGroupPanel;
            }

            set
            {
                this.showGroupPanel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the delivery date sort text.
        /// </summary>
        public string DeliveryDateSort
        {
            get
            {
                return this.deliveryDateSort;
            }

            set
            {
                this.deliveryDateSort = value;
                this.RaisePropertyChanged();
                ApplicationSettings.DispatchOrdersDateSort = value;
            }
        }

        /// <summary>
        /// Gets or set the route sort text.
        /// </summary>
        public string RouteSort
        {
            get
            {
                return this.routeSort;
            }

            set
            {
                this.routeSort = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the order received sort text.
        /// </summary>
        public string OrderReceivedSort
        {
            get
            {
                return this.orderReceivedSort;
            }

            set
            {
                this.orderReceivedSort = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the supplier sort text.
        /// </summary>
        public string SupplierSort
        {
            get
            {
                return this.supplierSort;
            }

            set
            {
                this.supplierSort = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the selected sale.
        /// </summary>
        public Sale Sale
        {
            get
            {
                return this.sale;
            }

            set
            {
                this.sale = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded && ApplicationSettings.ScannerMode)
                {
                    if (value != null)
                    {
                        this.HandleSelectedOrder();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the orders.
        /// </summary>
        public ObservableCollection<Sale> Orders
        {
            get
            {
                return this.orders;
            }

            set
            {
                this.orders = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Sets the master module.
        /// </summary>
        /// <param name="view">The master module type.</param>
        public void SetModule(ViewType view)
        {
            this.module = view;

            if (this.module == ViewType.APReceipt || this.module == ViewType.Sequencer || this.module == ViewType.LairageIntake)
            {
                this.PartnerSearchName = Strings.Supplier;
            }
            else
            {
                this.PartnerSearchName = Strings.Customer;
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the order search.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the order search.
        /// </summary>
        public ICommand FindOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to refresh the filters.
        /// </summary>
        public ICommand RefreshFiltersCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnOrdersLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event.
        /// </summary>
        public ICommand OnOrdersUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to create an order.
        /// </summary>
        public ICommand CreateOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to show the group panel.
        /// </summary>
        public ICommand ShowGroupPanelCommand { get; private set; }

        /// <summary>
        /// Gets the command to scroll the ui.
        /// </summary>
        public ICommand ScrollCommand { get; set; }

        /// <summary>
        /// Gets the command to sort the grid.
        /// </summary>
        public ICommand SortByCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a supplier sale selection.
        /// </summary>
        public ICommand SaleSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command tomove back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Creates an order for the selected supplier.
        /// </summary>
        protected virtual void CreateOrderCommandExecute()
        {
            if (this.Partner == null || this.Partner.Name.Equals(Strings.AllPartners))
            {
                NouvemMessageBox.Show(Message.NoPartnerSelected, touchScreen:true);
                return;
            }

            // move to the main screen, and send the supplier.
            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
            Messenger.Default.Send(true, Token.CloseTouchscreenOrdersWindow);

            // instruct the apgoodsreceipttouchscreen to update it's batch no.
            Messenger.Default.Send(Token.Message, Token.UpdateBatchNo);

            Messenger.Default.Send(this.Partner, Token.CreateOrder);
        }

        /// <summary>
        /// Move back to the previous module.
        /// </summary>
        private void MoveBackCommandExecute()
        {
            if (ApplicationSettings.ScannerMode)
            {
                this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ScannerTouchscreen;
                return;
            }

            Messenger.Default.Send(true, Token.CloseTouchscreenOrdersWindow);
            if (this.navigatedFromView == ViewType.TouchscreenSuppliers)
            {
                Messenger.Default.Send(false, Token.CloseTouchscreenPartnersWindow);
                return;
            }
        }

        /// <summary>
        /// Handles the sorting of the grid.
        /// </summary>
        /// <param name="field">The sort parameter.</param>
        private void SortByCommandExecute(string field)
        {
            IEnumerable<Sale> localOrders;
            if (field.Equals(Constant.DeliveryDate))
            {
                this.SortByDates(true);
            }

            if (field.Equals(Constant.Route))
            {
                if (this.RouteSort.Equals(Strings.Descending))
                {
                    localOrders = this.allOrders.OrderBy(x => x.RouteName);
                    this.Orders = new ObservableCollection<Sale>(localOrders);
                    this.RouteSort = Strings.Ascending;
                }
                else
                {
                    localOrders = this.Orders.OrderByDescending(x => x.RouteName);
                    this.Orders = new ObservableCollection<Sale>(localOrders);
                    this.RouteSort = Strings.Descending;
                }

                return;
            }

            if (field.Equals("ChangeDateType"))
            {
                if (this.DeliveryDateName == Strings.DocumentDate)
                {
                    this.DeliveryDateName = Strings.ShippingDate;
                }
                else if (this.DeliveryDateName == Strings.ShippingDate)
                {
                    this.DeliveryDateName = Strings.DeliveryDate;
                }
                else
                {
                    this.DeliveryDateName = Strings.DocumentDate;
                }
             
                return;
            }

            if (field.Equals(Constant.OrderReceived))
            {
                Messenger.Default.Send(true, Token.DisplayCalender);
                return;
            }

            if (field.Equals(Constant.Supplier))
            {
                this.Locator.TouchscreenSuppliers.MasterModule = ViewType.TouchscreenOrders;
                if (!this.PartnersScreenCreated)
                {
                    this.PartnersScreenCreated = true;
                    Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPartners);
                    return;
                }

                Messenger.Default.Send(false, Token.CloseTouchscreenPartnersWindow);
                this.Locator.TouchscreenSuppliers.OnEntry();
            }
        }

        /// <summary>
        /// Sets the order dates sorting.
        /// </summary>
        /// <param name="switchSort">Are we toggling between ascending and descending flag.</param>
        /// <param name="date">Are we searching by a specific date (null = no).</param>
        private void SortByDates(bool switchSort)
        {
            IEnumerable<Sale> localOrders;
            if (switchSort)
            {
                if (this.DeliveryDateSort.Equals(Strings.Descending))
                {
                    if (this.DeliveryDateName == Strings.DocumentDate)
                    {
                        localOrders = this.allOrders.OrderBy(x => x.DocumentDate);
                    }
                    else if (this.DeliveryDateName == Strings.ShippingDate)
                    {
                        localOrders = this.allOrders.OrderBy(x => x.ShippingDate);
                    }
                    else
                    {
                        localOrders = this.allOrders.OrderBy(x => x.DeliveryDate);
                    }

                    this.DeliveryDateSort = Strings.Ascending;
                }
                else
                {
                    if (this.DeliveryDateName == Strings.DocumentDate)
                    {
                        localOrders = this.allOrders.OrderByDescending(x => x.DocumentDate);
                    }
                    else if (this.DeliveryDateName == Strings.ShippingDate)
                    {
                        localOrders = this.allOrders.OrderByDescending(x => x.ShippingDate);
                    }
                    else
                    {
                        localOrders = this.allOrders.OrderByDescending(x => x.DeliveryDate);
                    }

                    this.DeliveryDateSort = Strings.Descending;
                }
            }
            else if (this.OrderReceivedDate.HasValue)
            {
                if (this.DeliveryDateSort.Equals(Strings.Descending))
                {
                    if (this.DeliveryDateName == Strings.DocumentDate)
                    {
                        localOrders = this.allOrders.Where(x => x.DocumentDate == this.OrderReceivedDate).OrderByDescending(x => x.DocumentDate);
                    }
                    else if (this.DeliveryDateName == Strings.ShippingDate)
                    {
                        localOrders = this.allOrders.Where(x => x.ShippingDate == this.OrderReceivedDate).OrderByDescending(x => x.ShippingDate);
                    }
                    else
                    {
                        localOrders = this.allOrders.Where(x => x.DeliveryDate == this.OrderReceivedDate).OrderByDescending(x => x.DeliveryDate);
                    }
                }
                else
                {
                    if (this.DeliveryDateName == Strings.DocumentDate)
                    {
                        localOrders = this.allOrders.Where(x => x.DocumentDate == this.OrderReceivedDate).OrderBy(x => x.DocumentDate);
                    }
                    else if (this.DeliveryDateName == Strings.ShippingDate)
                    {
                        localOrders = this.allOrders.Where(x => x.ShippingDate == this.OrderReceivedDate).OrderBy(x => x.ShippingDate);
                    }
                    else
                    {
                        localOrders = this.allOrders.Where(x => x.DeliveryDate == this.OrderReceivedDate).OrderBy(x => x.DeliveryDate);
                    }
                }
            }
            else
            {
                if (this.DeliveryDateSort.Equals(Strings.Descending))
                {
                    if (this.DeliveryDateName == Strings.DocumentDate)
                    {
                        localOrders = this.allOrders.OrderByDescending(x => x.DocumentDate);
                    }
                    else if (this.DeliveryDateName == Strings.ShippingDate)
                    {
                        localOrders = this.allOrders.OrderByDescending(x => x.ShippingDate);
                    }
                    else
                    {
                        localOrders = this.allOrders.OrderByDescending(x => x.DeliveryDate);
                    }
                }
                else
                {
                    if (this.DeliveryDateName == Strings.DocumentDate)
                    {
                        localOrders = this.allOrders.OrderBy(x => x.DocumentDate);
                    }
                    else if (this.DeliveryDateName == Strings.ShippingDate)
                    {
                        localOrders = this.allOrders.OrderBy(x => x.ShippingDate);
                    }
                    else
                    {
                        localOrders = this.allOrders.OrderBy(x => x.DeliveryDate);
                    }
                }
            }

            this.SetDynamicDate(localOrders);
            this.Orders = new ObservableCollection<Sale>(localOrders);
            return;
        }

        private void SetDynamicDate(IEnumerable<Sale> localOrders)
        {
            foreach (var localOrder in localOrders)
            {
                if (this.DeliveryDateName == Strings.DocumentDate)
                {
                    localOrder.DynamicDate = localOrder.DocumentDateTruncateTime;
                }
                else if (this.DeliveryDateName == Strings.ShippingDate)
                {
                    localOrder.DynamicDate = localOrder.ShippingDateTruncateTime;
                }
                else
                {
                    localOrder.DynamicDate = localOrder.DeliveryDateTruncateTime;
                }
            }
        }

        /// <summary>
        /// Sends the selected sale to the main vm.
        /// </summary>
        private void SaleSelectedCommandExecute()
        {
            this.HandleSelectedOrder();
        }

        public void OnEntry()
        {
            this.OnOrdersLoadingCommandExecute();
        }

        /// <summary>
        /// handle the load event.
        /// </summary>
        private void OnOrdersLoadingCommandExecute()
        {
            if (!ApplicationSettings.RetainTouchscreenOrdersFilter)
            {
                if (!this.incomingSearchSales)
                {
                    this.OrdersSearchText = string.Empty;
                }

                this.incomingSearchSales = false;

                if (!this.dontRefreshData)
                {
                    this.RefreshFilters();
                }

                this.dontRefreshData = false;
            }
        
            if (ApplicationSettings.ScannerMode)
            {
                this.Orders = new ObservableCollection<Sale>(this.DataManager.GetAllTouchscreenARDispatches(this.orderStatuses, this.RouteId).OrderBy(x => x.DeliveryDate));
            }

            this.Sale = null;
            this.IsFormLoaded = true;
            this.SearchByAttribute = false;
            this.Locator.FactoryScreen.ModuleName = Strings.Orders;
        }

        /// <summary>
        /// handle the load event.
        /// </summary>
        private void OnOrdersUnloadedCommandExecute()
        {
            this.IsFormLoaded = false;
        }

        /// <summary>
        /// Refresh the filters.
        /// </summary>
        private void RefreshFiltersCommandExecute()
        {
            this.OrderReceivedDate = null;
            this.RefreshFilters();
        }

        #endregion

        #region helper

        /// <summary>
        /// Handles the selected order.
        /// </summary>
        public void HandleSelectedOrder()
        {
            if (this.multiSelectMode)
            {
                return;
            }

            if (this.Sale != null && this.Sale.SaleDetails != null)
            {
                foreach (var detail in this.Sale.SaleDetails)
                {
                    detail.SetPriceMethod();
                }
            }

            switch (this.module)
            {
                case ViewType.APReceipt:
                case ViewType.Sequencer:
                case ViewType.Grader:
                    this.HandleSelectedSupplierOrder();
                    break;

                case ViewType.ARDispatch:
                    this.HandleSelectedCustomerOrder();
                    break;

                case ViewType.LairageIntake:
                    this.HandleSelectedLairageOrder();
                    break;

                case ViewType.ARReturns:
                    this.HandleSelectedReturnsOrder();
                    break;
            }
        }

        /// <summary>
        /// Handles the selected suppier order.
        /// </summary>
        private void HandleSelectedLairageOrder()
        {
            Messenger.Default.Send(this.Sale, Token.SearchSaleSelected);
            Messenger.Default.Send(true, Token.CloseTouchscreenOrdersWindow);
        }

        /// <summary>
        /// Handles the selected suppier order.
        /// </summary>
        private void HandleSelectedSupplierOrder()
        {
            //this.DataManager.GetTransactionData(this.Sale.SaleDetails);
            ViewBusinessPartner localSupplier = null;

            if (this.Sale.BPCustomer != null)
            {
                var localPartner =
                    NouvemGlobal.SupplierPartners.FirstOrDefault(
                        x => x.Details.BPMasterID == this.Sale.BPCustomer.BPMasterID);

                if (localPartner != null)
                {
                    localSupplier = localPartner.Details;
                }
            }

            this.Sale = this.DataManager.GetTouchscreenReceiptById(this.Sale.SaleID);
            Messenger.Default.Send(Tuple.Create(this.Sale, localSupplier), Token.SaleSelected);
            Messenger.Default.Send(true, Token.CloseTouchscreenOrdersWindow);
        }

        /// <summary>
        /// Handles the selected returns order.
        /// </summary>
        private void HandleSelectedReturnsOrder()
        {
            //this.DataManager.GetTransactionData(this.Sale.SaleDetails);
            ViewBusinessPartner localSupplier = null;

            if (this.Sale.BPCustomer != null)
            {
                var localPartner =
                    NouvemGlobal.CustomerPartners.FirstOrDefault(
                        x => x.Details.BPMasterID == this.Sale.BPCustomer.BPMasterID);

                if (localPartner != null)
                {
                    localSupplier = localPartner.Details;
                }
            }

            this.Sale = this.DataManager.GetARReturnById(this.Sale.SaleID);
            Messenger.Default.Send(Tuple.Create(this.Sale, localSupplier), Token.SaleSelected);
            Messenger.Default.Send(true, Token.CloseTouchscreenOrdersWindow);
        }

        /// <summary>
        /// Handles the selected customer order.
        /// </summary>
        private void HandleSelectedCustomerOrder()
        {
            if (ApplicationSettings.ScannerMode)
            {
                this.IsFormLoaded = false;
                this.Sale = this.DataManager.GetTouchscreenARDispatch(this.Sale.SaleID);
                this.IsFormLoaded = true;

                //this.DataManager.GetDispatchTransactionData(this.Sale.SaleDetails);
                ViewBusinessPartner localCustomer;
                var localExistingCustomer =
                    NouvemGlobal.CustomerPartners.FirstOrDefault(
                        x => x.Details.BPMasterID == this.Sale.BPMasterID);
                if (localExistingCustomer == null)
                {
                    localCustomer = this.DataManager.GetBusinessPartnerShort(this.Sale.BPMasterID.ToInt());
                }
                else
                {
                    localCustomer = localExistingCustomer.Details;
                }

                this.Locator.ScannerMainDispatch.HandleSelectedOrder(Tuple.Create(this.Sale, localCustomer));
                this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ScannerTouchscreen;
            }
            else
            {
                this.Sale = this.DataManager.GetTouchscreenARDispatch(this.Sale.SaleID);
                //this.DataManager.GetDispatchTransactionData(this.Sale.SaleDetails);

                ViewBusinessPartner localCustomer;
                var localExistingCustomer =
                    NouvemGlobal.CustomerPartners.FirstOrDefault(
                        x => x.Details.BPMasterID == this.Sale.BPMasterID);
                if (localExistingCustomer == null)
                {
                    localCustomer = this.DataManager.GetBusinessPartnerShort(this.Sale.BPMasterID.ToInt());
                }
                else
                {
                    localCustomer = localExistingCustomer.Details;
                }

                Messenger.Default.Send(Tuple.Create(this.Sale, localCustomer), Token.SaleSelected);
                Messenger.Default.Send(true, Token.CloseTouchscreenOrdersWindow);
            }
        }

        /// <summary>
        /// Handles the incoming orders.
        /// </summary>
        /// <param name="data">The incoming order data (orders and view navigated from).</param>
        private void HandleIncomingSales(Tuple<ViewBusinessPartner, ViewType> data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("HandleIncomingSales(): Coming from {0}", data.Item2));

            this.GetAllOrders();
            this.Orders.Clear();
            this.SortByDates(false);

            var localPartner = data.Item1;
            this.navigatedFromView = data.Item2;

            if (localPartner == null || localPartner.Name.Equals(Strings.AllPartners))
            {
                this.Log.LogDebug(this.GetType(), "Displaying all orders");
                
                //this.Orders = new ObservableCollection<Sale>(this.allOrders);
                return;
            }

            if (localPartner.BPMasterID != 0)
            {
                // Show supplier orders only.
                this.Log.LogDebug(this.GetType(), string.Format("Displaying orders for partner:{0}", localPartner.BPMasterID));
                this.Partner = localPartner;
                var partnerOrders = this.allOrders.Where(x => x.BPCustomer.BPMasterID == this.Partner.BPMasterID).ToList();
                if (partnerOrders.Any() || ApplicationSettings.ScannerMode)
                {
                    this.SetDefaultSortParameters(false);
                    this.Orders = new ObservableCollection<Sale>(partnerOrders);
                }
                else if (this.navigatedFromView == ViewType.TouchscreenSuppliers)
                {
                    // No orders, so back to main screen
                    Messenger.Default.Send(this.Partner, Token.SupplierSelected);
                    this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
                }
            }
        }

        /// <summary>
        /// Handles the incoming orders.
        /// </summary>
        /// <param name="data">The incoming order data (orders and view navigated from).</param>
        private void SetIncomingSales(Tuple<ViewBusinessPartner, ViewType, List<Sale>> data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("SetIncomingSales(): Coming from {0}", data.Item2));

            //this.GetAllOrders();
            this.Orders.Clear();

            var localPartner = data.Item1;
            this.navigatedFromView = data.Item2;
            var partnerOrders = data.Item3;

            if (localPartner.BPMasterID != 0)
            {
                // Show supplier orders only.
                this.Log.LogDebug(this.GetType(), string.Format("Displaying orders for partner:{0}", localPartner.BPMasterID));
                this.Partner = localPartner;
                if (partnerOrders.Any() || ApplicationSettings.ScannerMode)
                {
                    this.SetDefaultSortParameters(false);
                    this.Orders = new ObservableCollection<Sale>(partnerOrders);
                    this.incomingSearchSales = true;
                }
            }
        }

        /// <summary>
        /// Gets all the orders.
        /// </summary>
        public void GetAllOrders(string type = "")
        {
            switch (this.module)
            {
                case ViewType.APReceipt:
                case ViewType.LairageIntake:
                    this.allOrders = new ObservableCollection<Sale>(this.DataManager.GetAllAPReceipts(this.orderStatuses));              
                    break;

                case ViewType.ARDispatch:
                    this.allOrders = new ObservableCollection<Sale>(
                        this.DataManager.GetAllTouchscreenARDispatches(this.orderStatuses, this.RouteId).OrderBy(x => x.DeliveryDate).ThenBy(x => x.Partner));
                    break;

                case ViewType.Sequencer:
                    this.allOrders = new ObservableCollection<Sale>(this.DataManager.GetAllAPReceiptsByType(this.orderStatuses, Constant.Sheep));
                    break;

                case ViewType.Grader:
                    this.allOrders = new ObservableCollection<Sale>(this.DataManager.GetAllAPReceiptsByType(this.orderStatuses, type));
                    break;

                case ViewType.ARReturns:
                    this.allOrders = new ObservableCollection<Sale>(this.DataManager.GetARReturns(this.orderStatuses));
                    break;
            }
        }

        /// <summary>
        /// Sets the UI orders.
        /// </summary>
        public void SetOrders()
        {
            this.Orders.Clear();
            foreach (var allOrder in this.allOrders)
            {
                this.Orders.Add(allOrder);
            }
        }

        /// <summary>
        /// Sets the default sorting values.
        /// </summary>
        /// <param name="setPartner">Flag, as to whether the partners is to be (re)set.</param>
        private void SetDefaultSortParameters(bool setPartner = true)
        {
            //this.DeliveryDateSort = Strings.Ascending;
            this.RouteSort = Strings.Ascending;
            //this.OrderReceivedDate = null;

            if (setPartner)
            {
                this.Partner = this.allPartners;
            }
        }

        /// <summary>
        /// Handles the completion of 1 or more orders when in multi select mode.
        /// </summary>
        /// <param name="orders">The orders to process.</param>
        private void HandleMultipleOrderSelection(IList<Sale> orders)
        {
            if (!orders.Any())
            {
                return;
            }

            NouvemMessageBox.Show(Message.MultiOrdersCompletePrompt, NouvemMessageBoxButtons.YesNo, touchScreen:true);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            try
            {
                Messenger.Default.Send(orders.ToList(), Token.CompleteMultiOrder);
            }
            finally
            {
                this.GetAllOrders();
                this.RefreshFilters();
            }
        }

        /// <summary>
        /// Refreshes the filters.
        /// </summary>
        private void RefreshFilters()
        {
            this.SetDefaultSortParameters();
           
            if (this.module == ViewType.APReceipt || this.module == ViewType.Sequencer)
            {
                this.Orders.Clear();
                var localOrders = this.allOrders; 

                if (localOrders.Count() > 100)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Orders.Clear();
                        foreach (var order in localOrders.Take(100))
                        {
                            this.Orders.Add(order);
                        }
                    }));
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Orders.Clear();
                        foreach (var order in localOrders)
                        {
                            this.Orders.Add(order);
                        }
                    }));
                }
            }
            else
            {
                this.SortByDates(false);
            }
        }

        /// <summary>
        /// Display the orders.
        /// </summary>
        private void DisplayOrders()
        {
            if (this.module == ViewType.APReceipt)
            {
                if (this.SearchByAttribute)
                {
                    var searchData = this.DataManager.GetAllAPReceiptsByAttribute(this.OrdersSearchText);
                    this.Orders = new ObservableCollection<Sale>(searchData);
                    return;
                }

                this.Orders.Clear();
                IList<Sale> localOrders;
                if (ApplicationSettings.SearchIntakeByReference)
                {
                    localOrders = this.allOrders.Where(x => !string.IsNullOrEmpty(x.Reference) && x.Reference.ContainsIgnoringCase(this.OrdersSearchText)).ToList();
                }
                else
                {
                    localOrders = this.allOrders.Where(x => !string.IsNullOrEmpty(x.OtherReference) && x.OtherReference.ContainsIgnoringCase(this.OrdersSearchText)).ToList();
                }

                if (localOrders.Count() > 100)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        foreach (var order in localOrders.Take(100))
                        {
                            this.Orders.Add(order);
                        }
                    }));
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        foreach (var order in localOrders)
                        {
                            this.Orders.Add(order);
                        }
                    }));
                }
            }
            else
            {
                var localOrders = this.allOrders.Where(x => !string.IsNullOrEmpty(x.Partner) && x.Partner.ContainsIgnoringCase(this.OrdersSearchText));
                this.Orders = new ObservableCollection<Sale>(localOrders);
            }
        }

        /// <summary>
        /// Removes the completed order from the orders collection.
        /// </summary>
        /// <param name="completedOrderId">The completed order id.</param>
        [Obsolete]
        private void RefreshOrders(int completedOrderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RefreshOrders(): Attempting to remove completed order id:{0} from the orders collection", completedOrderId));
            var completedOrder = this.Orders.FirstOrDefault(x => x.SaleID == completedOrderId);
            if (completedOrder != null)
            {
                this.Orders.Remove(completedOrder);
                this.allOrders.Remove(completedOrder);
                this.Log.LogDebug(this.GetType(), "Completed order removed");
                return;
            }

            this.Log.LogError(this.GetType(), "Completed order not removed");
        }

        #endregion

        #endregion
    }
}
