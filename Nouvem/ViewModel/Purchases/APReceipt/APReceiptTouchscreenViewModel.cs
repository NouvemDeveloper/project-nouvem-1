﻿// -----------------------------------------------------------------------
// <copyright file="APReceiptTouchscreenViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using System.Windows.Forms;
using Nouvem.View.Production.IntoProduction;
using Application = System.Windows.Application;

namespace Nouvem.ViewModel.Purchases.APReceipt
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class APReceiptTouchscreenViewModel : APReceiptViewModel
    {
        #region field

        /// <summary>
        /// The touch screen selected sale details.
        /// </summary>
        private ObservableCollection<SaleDetail> selectedSaleDetails;

        /// <summary>
        /// The barcodes parse data.
        /// </summary>
        private IList<BarcodeParse> barcodeParses;

        /// <summary>
        /// The batch id.
        /// </summary>
        private string batchId;

        /// <summary>
        /// The incoming clean read flag.
        /// </summary>
        private bool cleanRead;

        /// <summary>
        /// The incoming manually set weight flag.
        /// </summary>
        private bool manualWeight;

        /// <summary>
        /// The incoming tare weight.
        /// </summary>
        private decimal tare;

        /// <summary>
        /// The incoming pieces.
        /// </summary>
        private int pieces;

        /// <summary>
        /// Flag, as to whether the current product is highlighted on ui load.
        /// </summary>
        private bool highlightSelectedProduct;

        /// <summary>
        /// Flag, as to whether a batch number is to be generated.
        /// </summary>
        private bool generateBatchNo;

        /// <summary>
        /// The batch traceability data.
        /// </summary>
        private readonly List<BatchTraceability> traceabilityData = new List<BatchTraceability>();

        /// <summary>
        /// All the order details transactions.
        /// </summary>
        private IList<SaleDetail> allOrderDetails;

        /// <summary>
        /// Timer used to determine a product removal operation.
        /// </summary>
        private readonly DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// Carcass split hind.
        /// </summary>
        private InventoryItem HindProduct;

        /// <summary>
        /// Carcass split fore.
        /// </summary>
        private InventoryItem ForeProduct;

        /// <summary>
        /// The product warehouse location.
        /// </summary>
        protected Warehouse selectedWarehouse;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="APReceiptTouchscreenViewModel"/> class.
        /// </summary>
        public APReceiptTouchscreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, Token.KeyboardValueEnteredForPalletNo, s =>
            {
                this.PalletNumber = s;
            });

            Messenger.Default.Register<string>(this, Token.DisplayGenericScreenSelection, s => Messenger.Default.Send(Token.Message, Token.DisplayGenericSelection));

            // Register for the incoming warehouse selection.
            Messenger.Default.Register<Warehouse>(this, Token.WarehouseSelected, o =>
            {
                this.selectedWarehouse = o;
            });

            // Register for the incoming combo collection selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, c =>
            {
                if (c.Identifier.Equals(Constant.Batch))
                {
                    this.EntitySelectionChange = true;
                    this.BatchNumber = this.DataManager.GetBatchNumber(c.ID);
                    this.EntitySelectionChange = false;
                }
            });

            // Register for the label change name.
            Messenger.Default.Register<Model.DataLayer.Process>(this, Token.ProcessSelected, i =>
            {
                var checkNotMade = this.DataManager.GetProcessStartUpChecks(i.ProcessID);
                if (checkNotMade.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.MissingProcessStartUpCheckMessage = string.Format(Message.HACCPNotCompleted, i.Name.Trim(), checkNotMade.First());
                        SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                        NouvemMessageBox.Show(this.MissingProcessStartUpCheckMessage, touchScreen: true);
                    }));

                    this.DisableModule = true;
                    return;
                }

                this.DisableModule = false;
                this.SetIntakeMode(i);
                if (this.Locator.ProcessSelection.SaveAsDefaultProcess)
                {
                    this.DataManager.SetDefaultModuleProcess(ViewType.APReceiptTouchscreen, ApplicationSettings.IntakeMode.ToString(), NouvemGlobal.DeviceId.ToInt());
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, Token.PrintBarcode, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintBarcode();
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.ManualSerial, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.IntakeScan, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.HandleScannerData(s);
                }
            });

            Messenger.Default.Register<bool>(this, Token.UpdateShowAttributes, b => ApplicationSettings.ShowIntakeAttributes = b);

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                if (this.ScannerStockMode == ScannerMode.Reprint)
                {
                    var transaction = this.DataManager.GetStockTransaction(s.ToInt());

                    if (transaction != null)
                    {
                        try
                        {
                            this.PrintManager.ReprintLabel(transaction.StockTransactionID);
                        }
                        catch (Exception ex)
                        {
                            SystemMessage.Write(MessageType.Issue, ex.Message);
                            this.Log.LogError(this.GetType(), ex.Message);
                        }
                    }
                }
            });

            // Register for the incoming keyboard selection.
            Messenger.Default.Register<string>(this, Token.KeyboardValueEnteredForReceiptBatch, s => this.Reference = s);

            //Messenger.Default.Register<string>(this, Token.UpdateBatchNo, s =>
            //{
            //    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //    {
            //        this.AutoGenerateNewBatchCommand();
            //    }));
            //});

            // Register for an intake labels number change.
            Messenger.Default.Register<int>(this, Token.LabelsToPrintUpdate, i =>
                {
                    ApplicationSettings.GoodsInLabelsToPrint = i;
                });

            // Register for the touchscreen ui load completion notification.
            Messenger.Default.Register<string>(this, Token.TouchscreenUiLoaded, s => this.HandleTouchscreenLoad());

            // Register for the incoming supplier.
            Messenger.Default.Register<ViewBusinessPartner>(this, Token.SupplierSelected, s => this.SelectedPartner = s);

            // Register for the incoming supplier, creating a new order.
            Messenger.Default.Register<string>(this, Token.UpdateBatchNo, s =>
            {
                this.AutoGenerateNewBatchCommand();
            });

            // Register for the incoming supplier, creating a new order.
            Messenger.Default.Register<ViewBusinessPartner>(this, Token.CreateOrder, this.CreateNewOrder);

            // Register for the incoming supplier order.
            Messenger.Default.Register<Tuple<Sale, ViewBusinessPartner>>(this, Token.SaleSelected, this.HandleSelectedOrder);

            // Register for the incoming product selection.
            Messenger.Default.Register<InventoryItem>(this, Token.ProductSelected, this.HandleProductSelection);

            // Register for the indicator weight data.
            Messenger.Default.Register<string>(this, Token.IndicatorWeight, x =>
            {
                this.manualWeight = false;
                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.DontPrintLabel = false;
                }

                this.RecordWeight();
            });

            // Register for the manual weight data.
            Messenger.Default.Register<Tuple<decimal, bool, bool>>(this, Token.IndicatorWeight, weightData =>
            {
                this.indicatorWeight = weightData.Item1;
                this.manualWeight = true;
                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.DontPrintLabel = false;
                }

                this.RecordWeight();
            });

            // Register for the indicator tare weight.
            Messenger.Default.Register<double>(this, Token.TareSet, tare => this.tare = tare.ToDecimal());

            // Register for the touchscreen record weight action.
            //Messenger.Default.Register<string>(this, Token.TouchscreenRecordWeight, action => this.RecordWeight());

            // Register for the touchscreen complete order action.
            Messenger.Default.Register<string>(this, Token.TouchscreenCompleteOrder, action => this.CompleteOrder());

            // Register for the touchscreen new batch action.
            //Messenger.Default.Register<string>(this, Token.TouchscreenNewBatch, action => this.CreateNewBatch());

            // register for the incoming traceability results.
            Messenger.Default.Register<Tuple<string, List<TraceabilityResult>>>(this, Token.TraceabilityValues, this.RecordWeightComplete);

            // Register for an incoming keypad value for the qty.
            //Messenger.Default.Register<string>(this, KeypadTarget.Quantity, this.HandleQtySelection);

            // Register for the incoming batch number.
            Messenger.Default.Register<BatchNumber>(this, Token.BatchNumber, batchNo => this.BatchNumber = batchNo);

            // Register for the incoming qty.
            Messenger.Default.Register<decimal>(this, Token.QuantityUpdate, i =>
            {
                this.ProductQuantity = i;
                this.SetTypicalWeight();
            });

            #endregion

            #region command

            this.PalletNumberCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Tuple.Create(ViewType.Keyboard, ViewType.APReceipt));
            });

            this.GenerateNewBatchCommand = new RelayCommand(this.GenerateNewBatchCommandExecute);

            this.InputReferenceCommand = new RelayCommand(this.InputReferenceCommandExecute);

            // Handle the ui load event.
            this.OnAPReceiptLoadingCommand = new RelayCommand(this.OnAPReceiptLoadingCommandExecute);

            // Handle the ui load event.
            this.OnAPReceiptUnloadingCommand = new RelayCommand(this.OnAPReceiptUnloadingCommandExecute);

            // Handle the product removal check, by starting/stopping the timer
            this.CheckForProductRemovalCommand = new RelayCommand<string>((s) =>
            {
                if (s.ToBool())
                {
                    this.timer.Start();
                }
                else
                {
                    this.timer.Stop();
                }
            });

            // Handle the view switch.
            this.SwitchViewCommand = new RelayCommand<string>(this.SwitchViewCommandExecute);

            #endregion

            ApplicationSettings.IntakeMode = ApplicationSettings.DefaultIntakeMode;
            this.SetTouchscreen();
            this.StockLabelsToPrint = 1;
            this.SelectedSaleDetails = new ObservableCollection<SaleDetail>();
            this.allOrderDetails = new List<SaleDetail>();
            SystemMessage.Write(MessageType.Priority, Message.GoodsInLoaded);
            this.SetUpTimer();
            NouvemGlobal.CheckBatchValuesSet = true;
            this.PartnerName = Strings.SelectSupplier;
         
            this.GetBarcodeParses();
            this.SetUpBarcodeTimer();
            this.GetDateDays();
            this.GetAlertUsers();
            this.ClearAttributes();
            this.ClearAttributeControls();
            this.GetProcesses();
            this.SetDefaultProcesses();
            this.CloseHiddenWindows();
            this.OpenScanner();
            this.ForeProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == ApplicationSettings.ForeProduct);
            this.HindProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == ApplicationSettings.HindProduct);
            this.Locator.Touchscreen.UIText = Strings.TerminalLocation;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or set the pieces.
        /// </summary>
        public string BatchId
        {
            get
            {
                return this.batchId;
            }

            set
            {
                this.batchId = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the pieces.
        /// </summary>
        public int Pieces
        {
            get
            {
                return this.pieces;
            }

            set
            {
                this.pieces = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the touchscreen sale details.
        /// </summary>
        public ObservableCollection<SaleDetail> SelectedSaleDetails
        {
            get
            {
                return this.selectedSaleDetails;
            }

            set
            {
                this.selectedSaleDetails = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command torecord the pieces.
        /// </summary>
        public ICommand PalletNumberCommand { get; private set; }

        /// <summary>
        /// Gets the command torecord the pieces.
        /// </summary>
        public ICommand RecordPiecesCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event
        /// </summary>
        public ICommand OnAPReceiptUnloadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event
        /// </summary>
        public ICommand OnAPReceiptLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a supplier group selection.
        /// </summary>
        public ICommand SupplierGroupSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to check for a grid product removal.
        /// </summary>
        public ICommand CheckForProductRemovalCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a keyboard call to input a batch reference.
        /// </summary>
        public ICommand InputReferenceCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to generate a new batch number.
        /// </summary>
        public ICommand GenerateNewBatchCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Handle the carcass split selection.
        /// </summary>
        /// <param name="isHind">Is it a hind selection flag.</param>
        public void CarcassSplitProductSelected(bool isHind, bool fullCarcass)
        {
            var product = isHind ? this.HindProduct : this.ForeProduct;

            var localProduct = this.SaleDetails.FirstOrDefault(x => x.INMasterID == product.Master.INMasterID);
            if (localProduct != null)
            {
                var localManualWgt = this.Locator.Indicator.ManualWeight;
                var localWgt = this.Locator.Indicator.Weight;
                this.SelectedSaleDetail = localProduct;
                this.Locator.Indicator.ManualWeight = localManualWgt;
                this.Locator.Indicator.Weight = localWgt;
            }
            else
            {
                var productToAdd = product;
                if (productToAdd == null)
                {
                    return;
                }

                var localStockId = this.StocktransactionId;
                this.HandleProductSelection(productToAdd);
                this.StocktransactionId = localStockId;
            }

            System.Threading.Thread.Sleep(200);
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.manualWeight = false;
                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.DontPrintLabel = true;
                }

                try
                {
                    this.scanProcessing = true;
                    this.Locator.Indicator.RecordWeight();
                    Messenger.Default.Send(Token.Message, Token.CloseCarcassSplitWindow);
                }
                finally
                {
                    this.scanProcessing = false;
                }
            }));
        }

        /// <summary>
        /// Removes a deleted label from the line.
        /// </summary>
        /// <param name="trans">The label to delete.</param>
        public bool RemoveTransaction(int intakeid, StockTransaction trans)
        {
            var sale = this.DataManager.GetAPReceiptFullById(intakeid);
            var localIntake = sale.SaleDetails.FirstOrDefault(x => x.INMasterID == trans.INMasterID && x.SaleDetailID == trans.MasterTableID);

            if (localIntake != null)
            {
                localIntake.WeightReceived -= trans.TransactionWeight.ToDecimal();
                localIntake.QuantityReceived -= trans.TransactionQTY.ToDecimal();
                localIntake.SetPriceMethodOnly();
                localIntake.UpdateTotalLocallyOnly(goodsIn: true);

                localIntake.IsSelectedSaleDetail = true;

                this.UpdateSaleOrderTotals(sale);
                if (this.DataManager.RemoveIntakeStockFromOrder(sale))
                {
                    this.DataManager.DeleteTransactionOnly(new StockTransactionData { Transaction = trans });
                    if (this.Sale != null && this.Sale.SaleID == sale.SaleID)
                    {
                        // update the ui sale.
                        var localSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == trans.INMasterID && x.SaleDetailID == trans.MasterTableID);
                        if (localSaleDetail != null)
                        {
                            localSaleDetail.WeightReceived -= trans.TransactionWeight.ToDecimal();
                            localSaleDetail.QuantityReceived -= trans.TransactionQTY.ToDecimal();
                            localSaleDetail.TransactionCount =
                            this.DataManager.GetTransactionCount(localSaleDetail.SaleDetailID,
                                NouvemGlobal.TransactionTypeGoodsReceiptId);
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes a deleted label from the line.
        /// </summary>
        /// <param name="trans">The label to delete.</param>
        public void RemoveLabel(StockTransaction trans)
        {
            var localReceipt = this.SaleDetails.FirstOrDefault(x => x.INMasterID == trans.INMasterID);
            if (localReceipt != null)
            {
                localReceipt.WeightReceived -= trans.TransactionWeight.ToDecimal();
                localReceipt.QuantityReceived -= trans.TransactionQTY.ToDecimal();
            }
        }

        #endregion

        #endregion

        #region protected

        #region override
        protected override bool ResetQtyToZero()
        {
            return ApplicationSettings.ResetQtyToZeroAtIntake;
        }

        protected virtual void SetTouchscreen()
        {
            if (ApplicationSettings.ResetQtyToZeroAtIntake)
            {
                this.Locator.Touchscreen.Quantity = 0;
            }
            else
            {
                this.Locator.Touchscreen.Quantity = 1;
            }

            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowIntakeAttributes;
            this.Locator.Touchscreen.UIText = Strings.TerminalLocation;
            ViewModelLocator.ClearTouchscreenSuppliers();
            ViewModelLocator.ClearStockMovementTouchscreen();
            this.Locator.Touchscreen.OutOfProductionExtensionWidth = 0;
            this.Locator.Indicator.WeighMode = WeighMode.Manual;
            this.Locator.Touchscreen.UIText = string.Empty;
        }

        /// <summary>
        /// Gets the local module warehouse.
        /// </summary>
        /// <returns></returns>
        protected virtual int GetWarehouseId()
        {
            return this.selectedWarehouse != null && this.selectedWarehouse.WarehouseID != 0
                ? this.selectedWarehouse.WarehouseID
                : NouvemGlobal.WarehouseIntakeId;
        }

        /// <summary>
        /// Sets the module default process.
        /// </summary>
        protected virtual void SetDefaultProcesses()
        {
            this.Locator.ProcessSelection.SetDefaultProcesses(ApplicationSettings.DefaultIntakeModes, ApplicationSettings.IntakeMode.ToString());
        }

        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        protected StockDetail CreateTransaction()
        {
            this.Log.LogDebug(this.GetType(), "CreatingTransaction(): Creating transaction");

            var localWarehouseId = this.GetWarehouseId();

            return new StockDetail
            {
                StockTransactionID = this.StocktransactionId,
                TransactionWeight = this.indicatorWeight,
                Tare = this.tare,
                Pieces = this.pieces == 0 ? 1 : this.pieces,
                ManualWeight = this.manualWeight,
                TransactionQty = this.ProductQuantity.ToDecimal() == 0 ? 1 : this.ProductQuantity.ToDecimal(),
                INMasterID = this.SelectedSaleDetail.INMasterID,
                TransactionDate = DateTime.Now,
                NouTransactionTypeID = this.GetTransactionId(),
                WarehouseID = localWarehouseId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                Comments = this.PalletNumber,
                ProcessID = this.Locator.ProcessSelection.SelectedProcess != null ? this.Locator.ProcessSelection.SelectedProcess.ProcessID : (int?)null,
                BatchNumberID = this.SelectedSaleDetail != null ? this.SelectedSaleDetail.BatchNumberId : null
            };
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void DeleteTransaction(int stockId, string barcode = "")
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to delete intake label:{stockId}");
            var error = this.DataManager.DeleteIntakeStock(stockId, this.Sale.SaleID, NouvemGlobal.UserId.ToInt(),
                NouvemGlobal.DeviceId.ToInt());
            if (string.IsNullOrEmpty(error))
            {
                this.Log.LogInfo(this.GetType(), "Label deleted");
                this.GetLiveStatus();
                this.SetOrderLinesStatus();
                SystemMessage.Write(MessageType.Priority, Message.StockRemoved);
            }
            else
            {
                this.Log.LogError(this.GetType(), $"Label deletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
        }

        /// <summary>
        /// Deletes a transaction.
        /// </summary>
        /// <param name="stockId">The transaction to delete.</param>
        protected override void UndeleteTransaction(int stockId)
        {
            this.Log.LogInfo(this.GetType(), $"Attempting to undelete into prod label:{stockId}");
            var error = this.DataManager.UndeleteStock(stockId, NouvemGlobal.TransactionTypeGoodsReceiptId, NouvemGlobal.UserId.ToInt(), NouvemGlobal.DeviceId.ToInt());

            if (string.IsNullOrEmpty(error))
            {
                this.Log.LogInfo(this.GetType(), "Label undeleted");
                this.GetLiveStatus();
                SystemMessage.Write(MessageType.Priority, Message.StockUndeleted);
                this.Log.LogInfo(this.GetType(), "Label deleted");
            }
            else
            {
                this.Log.LogError(this.GetType(), $"Label undeletion failure:{error}");
                SystemMessage.Write(MessageType.Issue, error);
            }
        }

        /// <summary>
        /// Sets the ui batch(s) id to display.
        /// </summary>
        /// <param name="value">The current batch.</param>
        protected override void SetUIBatches(BatchNumber value)
        {
            if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch && !this.EntitySelectionChange)
            {
                if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.SaleDetailID > 0)
                {
                    var lineBatches =
                        this.DataManager.GetIntakeLineBatchReferences(this.SelectedSaleDetail.SaleDetailID).ToList();
                    if (lineBatches.Count > 1)
                    {
                        this.BatchId = string.Join(",", lineBatches.Select(x => x));
                        return;
                    }
                }
            }

            if (value != null)
            {
                this.BatchId = value.Number;
            }
            else
            {
                this.BatchId = string.Empty;
            }
        }

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleStockLabels(string data)
        {
            this.StockLabelsToPrint = data.ToInt();
        }

        /// <summary>
        /// handles the barcode scan.
        /// </summary>
        /// <param name="data">The scanned barcode.</param>
        protected override void HandleScannerData(string data)
        {
            if (this.scanProcessing)
            {
                return;
            }
           
            try
            {
                this.scanProcessing = true;
                this.manualWeight = false;
                this.Log.LogInfo(this.GetType(), string.Format("HandleScannerData(): data:{0}, length:{1}", data, data.Length));

#if DEBUG
                //data = "(20)HQ(21)18059"; 
#endif
                //if (ApplicationSettings.IntakeMode == IntakeMode.Intake)
                //{
                //    this.HandlePallet(data.Trim().ToInt());
                //    return;
                //}

                if (ViewModelLocator.IsTransactionManagerLoaded())
                {
                    Messenger.Default.Send(data.Trim(), Token.LabelSearch);
                    return;
                }

                if (ApplicationSettings.IntakeMode == IntakeMode.IntakeImport)
                {
                    if (this.SelectedPartner != null)
                    {
                        var useParseBarcode = this.barcodeParses
                            .Any(x => x.BPMasterID == this.SelectedPartner.BPMasterID);
                        if (useParseBarcode)
                        {
                            this.ParseBarcode(data);
                            return;
                        }
                    }

                    var trans = this.DataManager.GetStockTransactionByComments(data.Trim(), ApplicationSettings.ImportWarehouseId);
                    if (!trans.StockTransactionID.HasValue)
                    {
                        var msg = Message.StockNotFound;
                        if (!string.IsNullOrEmpty(trans.Error))
                        {
                            msg = string.Format(Message.LabelAlreadyScanned, data, trans.Error);
                        }

                        SystemMessage.Write(MessageType.Issue, msg);
                        NouvemMessageBox.Show(msg, touchScreen: true);
                        return;
                    }

                    this.SetAttributeValues(trans);
                    this.StocktransactionId = trans.StockTransactionID;

                    if (trans.INMasterID == ApplicationSettings.ForeProduct ||
                        trans.INMasterID == ApplicationSettings.HindProduct)
                    {
                        this.Locator.Touchscreen.DisableWeigh(true);
                        this.CarcassSplitProductSelected(trans.INMasterID == ApplicationSettings.HindProduct, false);
                        return;
                    }

                    var scannedData = this.DataManager.GetScannedCarcassSplitStock(data.Trim(), true);

                    var scannedProducts = string.Empty;
                    foreach (var i in scannedData)
                    {
                        scannedProducts = scannedProducts + "," + i.ToString();
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("2. Sending products:{0} for UI de-selection", scannedProducts));
                    var split = new CarcassSplitView(scannedData, ViewType.APReceipt);
                    split.Show();
                    split.Focus();

                    //SystemMessage.Write(MessageType.Priority, string.Format("Carcass with barcode {0} scanned and ready for weighing", data));
                    return;
                }

                if (ApplicationSettings.RecallProductAtScan)
                {
                    var localProduct = this.RetrieveProductFromEan13Barcode(data, ApplicationSettings.RecallProductAtScanStartPos, ApplicationSettings.RecallProductAtScanLength);
                    if (localProduct == null)
                    {
                        var productCode = data.Substring(ApplicationSettings.RecallProductAtScanStartPos, ApplicationSettings.RecallProductAtScanLength);
                        var msg = string.Format(Message.ProductMatchingCodeNotFound, productCode);
                        this.Log.LogError(this.GetType(), msg);
                        SystemMessage.Write(MessageType.Issue, msg);
                        NouvemMessageBox.Show(msg, touchScreen: true);
                        return;
                    }

                    if (this.SaleDetails == null)
                    {
                        this.SaleDetails = new ObservableCollection<SaleDetail>();
                    }

                    var orderProduct = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localProduct.Master.INMasterID);
                    if (orderProduct != null)
                    {
                        this.SelectedSaleDetail = orderProduct;
                    }
                    else
                    {
                        this.HandleProductSelection(localProduct);
                        if (this.SelectedSaleDetail != null)
                        {
                            this.SelectedSaleDetail.DontPrintLabel = true;
                        }
                    }

                    return;
                }

                if (ApplicationSettings.RecallBatchAtIntakeScan)
                {
                    var localData = data.RemoveNonIntegers().ToInt();
                    if (localData == 123456789)
                    {
                        // record weight barcode
                        this.RecordWeight();
                        return;
                    }

                    var trans = this.DataManager.GetStockTransactionIncIntakeId(localData);
                    if (trans == null || !trans.Comments.CompareIgnoringCase(Constant.Standard))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NonIntakeBarcodeScan);
                        NouvemMessageBox.Show(Message.NonIntakeBarcodeScan, touchScreen: true);
                        return;
                    }

                    var localSale = this.DataManager.GetTouchscreenReceiptById(trans.LabelID.ToInt());
                    if (localSale != null)
                    {
                        BatchNumber localBatch = null;
                        if (localSale.SaleDetails.Any())
                        {
                            localBatch = localSale.SaleDetails.First().Batch;
                            this.BatchNumber = localBatch;
                        }

                        var localPartner =
                            NouvemGlobal.BusinessPartners.FirstOrDefault(
                                x => x.Details.BPMasterID == localSale.BPCustomer.BPMasterID);

                        if (localPartner != null)
                        {

                            this.HandleSelectedOrder(Tuple.Create(localSale, localPartner.Details));
                            this.BatchNumber = localBatch;
                        }
                    }

                    return;
                }

                if (this.SelectedPartner == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                    return;
                }

                if (this.SelectedSaleDetail == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                    return;
                }

                //this.ParseBarcode(data);
            }
            finally
            {
                this.scanProcessing = false;
            }
        }

        /// <summary>
        /// Completes the current order.
        /// </summary>
        protected override void CompleteOrder()
        {
            #region validation

            var error = string.Empty;

            var unCompleted = this.CheckRequiredBeforeCompletionAttributeData();
            if (unCompleted != string.Empty)
            {
                var message = string.Format(Message.RequiredBeforeCompletionAttributeValueMissing, unCompleted);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoPartnerSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.EmptyOrder;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error, touchScreen: true);
                return;
            }

            try
            {
                if (!this.ValidateData(
                    this.traceabilityDataCompletion,
                    this.Sale.SaleID,
                    0, 0, 0, 0, this.Locator.ProcessSelection.SelectedProcess.ProcessID,
                    Constant.Intake))
                {
                    return;
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion

            NouvemMessageBox.Show(Message.VerifyOrderCompletion, NouvemMessageBoxButtons.YesNo, touchScreen: true, scanner: ApplicationSettings.ScannerMode);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            var partnerId = this.SelectedPartner?.BPMasterID;
            var docketId = this.Sale.SaleID;
            this.DataManager.PriceIntakeDocket(this.Sale.SaleID);
            if (this.DataManager.CompleteOrCancelAPReceipt(this.Sale))
            {
                SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.Sale.Number));
                if (ApplicationSettings.PrintBatchLabelOnItakeCompletion)
                {
                    this.PrintBarcode();
                }
            
                this.ClearForm();
                this.PartnerName = Strings.SelectSupplier;
                this.Reference = string.Empty;
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                this.RefreshDocStatusItems();
                Messenger.Default.Send(Token.Message, Token.ClearData);
                this.ReportManager.EmailModuleReports(Properties.Module.Intake, partnerId.ToInt(), docketId.ToString());
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderNotCompleted);
            }
        }

        /// <summary>
        /// Gets the current attribute docket id (SaleID,PROrderID).
        /// </summary>
        /// <returns>THe corresponding docket id.</returns>
        protected override int GetWorkflowDocketId()
        {
            return this.Sale != null ? this.Sale.SaleID : 0;
        }

        /// <summary>
        /// Gets the current attribute product id.
        /// </summary>
        /// <returns>THe corresponding product id.</returns>
        protected override int GetWorkflowProductId()
        {
            return this.SelectedSaleDetail != null ? this.SelectedSaleDetail.INMasterID : 0;
        }

        /// <summary>
        /// Saves the batch attributes.
        /// </summary>
        protected override void SaveBatchAttributes(int? parentId = null, bool ignoreChecks = false)
        {
            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            if (this.BatchNumber == null)
            {
                this.AutoGenerateNewBatchCommand();
            }

            if (!ignoreChecks)
            {
                var unfilled = this.CheckAttributeData();
                if (unfilled != string.Empty)
                {
                    var message = string.Format(Message.AttributeValueMissing, unfilled);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message, touchScreen: true);
                    return;
                }
            }

            #endregion

            this.UnsavedAttributes = false;
            var attributeId = this.SaveBatchData(this.BatchNumber.BatchNumberID);
            if (attributeId > 0)
            {
                if (this.NonStandardBatchResponses != null && this.NonStandardBatchResponses.Any() && this.Sale.SaleID > 0)
                {
                    foreach (var response in this.NonStandardBatchResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: this.Sale.SaleID);
                    }
                }

                base.SaveBatchAttributes(attributeId);
                SystemMessage.Write(MessageType.Priority, Message.BatchAttributesSaved);

                if (!ignoreChecks)
                {
                    NouvemMessageBox.Show(Message.BatchAttributesSaved, touchScreen: true, flashMessage: true);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.BatchAttributesNotSaved);
            }
        }

        /// <summary>
        /// Sets the process flag (visibility/edit)
        /// </summary>
        /// <param name="data">The attribute allocation data.</param>
        protected override Tuple<bool, bool> SetProcessData(AttributeAllocationData data)
        {
            var visible = false;
            var editable = false;
            if (ApplicationSettings.IntakeMode == IntakeMode.Intake)
            {
                visible = data.AttributeVisibleProcesses != null &&
                              data.AttributeVisibleProcesses.Any(
                                  x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.Intake));

                editable = data.AttributeEditProcesses != null &&
                               data.AttributeEditProcesses.Any(
                                   x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.Intake));
            }

            if (ApplicationSettings.IntakeMode == IntakeMode.IntakeImport)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntakeImport));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntakeImport));
            }

            if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntakeMultiBatch));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntakeMultiBatch));
            }

            if (ApplicationSettings.IntakeMode == IntakeMode.IntakeWithPallet)
            {
                visible = data.AttributeVisibleProcesses != null &&
                          data.AttributeVisibleProcesses.Any(
                              x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntakeWithPallet));

                editable = data.AttributeEditProcesses != null &&
                           data.AttributeEditProcesses.Any(
                               x => x.Process != null && x.Process.Name.CompareIgnoringCaseAndWhitespace(ProcessType.IntakeWithPallet));
            }

            return Tuple.Create(visible, editable);
        }

        /// <summary>
        /// Sets the typical pieces.
        /// </summary>
        /// <param name="typicalPieces">The typical pieces.</param>
        protected override void SetTypicalPieces(int? typicalPieces)
        {
            var localPieces = typicalPieces.ToInt();
            this.Locator.Touchscreen.Pieces = localPieces;
            this.pieces = localPieces;

            if (ApplicationSettings.IntakeItemByThePiece)
            {
                this.ProductQuantity = localPieces;
            }
        }

        /// <summary>
        /// Resets the ui.
        /// </summary>
        /// <param name="clearSelectedCustomer">Flag, indicating whether the selected customer data is cleared.</param>
        protected override void ClearForm(bool clearSelectedCustomer = true)
        {
            base.ClearForm(clearSelectedCustomer);
            this.PartnerName = Strings.SelectSupplier;
            this.Reference = string.Empty;
            this.BatchNumber = null;
        }

        /// <summary>
        /// Attempts to remove the selected product.
        /// </summary>
        protected override void RemoveProductCommandExecute()
        {
            if (this.SelectedSaleDetail != null)
            {
                // If there are no transactions recorded against this product, remove.
                if (this.SelectedSaleDetail.QuantityReceived.ToDouble() > 0 || this.SelectedSaleDetail.WeightDelivered.ToDouble() > 0)
                {
                    var authorised = this.AuthorisationsManager.AllowUserToDeleteLabel;
                    if (authorised)
                    {
                        NouvemMessageBox.Show(Message.ViewProductLineTransactions, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TransactionManager);
                            this.Locator.TransactionManager.SetTransactions(this.DataManager.GetTransactionsForIntakeLine(this.SelectedSaleDetail.SaleDetailID));
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                    }
                }
                else
                {
                    // Deleting product
                    if (this.RemoveItem(ViewType.APReceipt))
                    {
                        SystemMessage.Write(MessageType.Priority, Message.ProductRemoved);
                    }
                }

                this.CheckForEmptyOrder();
            }
        }

        /// <summary>
        /// Override the sale selection.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            #region parse sale

            this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == this.Sale.NouDocStatusID);
            this.ValidUntilDate = this.Sale.QuoteValidDate;
            this.DeliveryDate = this.Sale.DeliveryDate;
            this.DocumentDate = this.Sale.DocumentDate;
            this.DiscountPercentage = this.Sale.DiscountPercentage.ToDecimal();
            this.Remarks = this.Sale.Remarks;
            this.SelectedSalesEmployee = this.Sale.SalesEmployeeID != null ?
                this.SalesEmployees.FirstOrDefault(x => x.UserMaster.UserMasterID == this.Sale.SalesEmployeeID) : null;
            this.SubTotal = this.Sale.SubTotalExVAT.ToDecimal();
            this.Tax = this.Sale.VAT.ToDecimal();
            this.Discount = this.Sale.DiscountIncVAT.ToDecimal();
            this.Total = this.Sale.GrandTotalIncVAT.ToDecimal();
            this.DocketNote = this.Sale.DocketNote;
            this.InvoiceNote = this.Sale.InvoiceNote;
            this.SelectedDeliveryAddress = this.Sale.DeliveryAddress != null && this.Sale.DeliveryAddress.Details != null ?
                this.Addresses.FirstOrDefault(
                x => x.Details.BPAddressID == this.Sale.DeliveryAddress.Details.BPAddressID) : null;
            this.SelectedInvoiceAddress = this.Sale.InvoiceAddress != null && this.Sale.InvoiceAddress.Details != null ?
                this.Addresses.FirstOrDefault(
                x => x.Details.BPAddressID == this.Sale.InvoiceAddress.Details.BPAddressID) : null;
            this.Reference = this.Sale.OtherReference;

            #endregion

            #region parse sale details

            this.Locator.Touchscreen.DisableWeigh(false);

            // Add the Sale details.
            this.DoNotHandleSelectedSaleDetail = true;
            this.SaleDetails = new ObservableCollection<SaleDetail>(this.Sale.SaleDetails);
            this.DoNotHandleSelectedSaleDetail = false;
            if (this.SaleDetails.Any())
            {
                this.SelectedSaleDetail = this.SaleDetails.Last();
                this.BatchNumber = this.SelectedSaleDetail.Batch;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                if (saleDetail.WeightOrdered == null)
                {
                    saleDetail.WeightOrdered = 0;
                }

                if (saleDetail.QuantityOrdered == null)
                {
                    saleDetail.QuantityOrdered = 0;
                }

                if (saleDetail.WeightReceived == null)
                {
                    saleDetail.WeightReceived = 0;
                }

                if (saleDetail.QuantityReceived == null)
                {
                    saleDetail.QuantityReceived = 0;
                }
            }

            #endregion

            if (this.BatchNumber == null)
            {
                this.AutoGenerateNewBatchCommand();
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.SetOrderLinesStatus();
            }));

            if (!string.IsNullOrWhiteSpace(this.Sale.PopUpNote))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(this.Sale.PopUpNote, isPopUp: true);
                }));
            }

            if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp: true);
                    this.CustomerPopUpNote = string.Empty;
                }));
            }
        }

        /// <summary>
        /// Sets the ui panel.
        /// </summary>
        protected virtual void SetPanel()
        {
            if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
            {
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenReceiptPanelWithBarcodePrint;
                return;
            }

            if (ApplicationSettings.IntakeMode == IntakeMode.IntakeWithPallet)
            {
                this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenReceiptPalletPanel;
                return;
            }

            this.Locator.Touchscreen.PanelViewModel = this.Locator.TouchscreenReceiptPanel;
        }

        /// <summary>
        /// Override, to send the order item to the traceability module, and the container to the indicator vm.
        /// </summary>
        protected override void HandleSelectedSaleDetail()
        {
            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;

            //Messenger.Default.Send(this.SelectedSaleDetail, Token.SaleDetailSelected);

            if (this.SelectedSaleDetail.InventoryItem != null && this.SelectedSaleDetail.InventoryItem.BoxTareContainer != null)
            {
                Messenger.Default.Send(this.SelectedSaleDetail.InventoryItem.BoxTareContainer, Token.ContainerSelected);
            }

            if (this.SelectedSaleDetail.Batch == null)
            {
                if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
                {
                    this.BatchNumber = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
                    if (this.BatchNumber != null)
                    {
                        this.DataManager.UpdateAPReceiptBatchNumber(this.SelectedSaleDetail.SaleDetailID,
                            this.BatchNumber.BatchNumberID);
                    }
                   
                    this.SelectedSaleDetail.Batch = this.BatchNumber;
                }
                else
                {
                    this.SelectedSaleDetail.Batch = this.BatchNumber;
                }
            }

            if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch && !this.EntitySelectionChange)
            {
                this.BatchNumber = this.SelectedSaleDetail.Batch;
            }

            this.CheckAttributeProductChangeReset(this.SelectedSaleDetail);
            this.HandleSaleDetail(this.SelectedSaleDetail, this);
            
            //if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
            //{
            //    if (!this.SelectedSaleDetail.StockDetails.Any() && this.SelectedSaleDetail.Batch != null)
            //    {
            //        var lastBatchAttribute = this.DataManager.GetLastBatchAttributeData(this.SelectedSaleDetail.Batch.BatchNumberID,
            //            NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.TransactionTypeGoodsReceiptId);

            //        if (lastBatchAttribute != null)
            //        {
            //            this.SetAttributeValues(lastBatchAttribute);
            //            this.HandleSaleDetailForTransaction(this.SelectedSaleDetail, this);
            //        }
            //    }
            //}
            //else
            //{
            //    //if (!this.SelectedSaleDetail.StockDetails.Any() && this.Sale.LastBatchAttribute != null)
            //    //{
            //    //    this.SetAttributeValues(this.Sale.LastBatchAttribute);
            //    //    this.HandleSaleDetailForTransaction(this.SelectedSaleDetail, this);
            //    //}
            //}

            this.SetTypicalWeight();
            this.SetTare(this.SelectedSaleDetail?.InventoryItem);

            if (this.SelectedSaleDetail != null && !string.IsNullOrWhiteSpace(this.SelectedSaleDetail.Notes))
            {
                var notes = this.SelectedSaleDetail.Notes;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(notes, isPopUp: true);
                }));
            }
        }

        #endregion

        #endregion

        #region private 

        #region command execution

        /// <summary>
        /// Handle the ui load event.
        /// </summary>
        private void OnAPReceiptLoadingCommandExecute()
        {
            Messenger.Default.Send(false, Token.DisableLegacyAttributesView);
            this.IsBusy = false;
            this.SetActiveDocument(this);
            ViewModelLocator.ClearARDispatchTouchscreen();
            this.generateBatchNo = true;
            this.ScannerStockMode = ScannerMode.Scan;
            this.Locator.FactoryScreen.ModuleName = Strings.Intake;
            ApplicationSettings.UsingWorkflowForStandardAttributes = true;

            this.CheckForEmptyOrder();
            this.IsFormLoaded = true;
            if (ApplicationSettings.AlwaysShowAttributes)
            {
                this.Locator.Touchscreen.ShowAttributes = true;
            }
        }

        /// <summary>
        /// Handle the parent touchscreen load.
        /// </summary>
        public void TouchscreenLoaded()
        {
            //this.RefreshLocalDocNumberings();
            this.SetActiveDocument(this);
            this.GetPriceLists();
        }

        /// <summary>
        /// Handle the ui load event.
        /// </summary>
        private void OnAPReceiptUnloadingCommandExecute()
        {
            this.IsFormLoaded = false;
            this.Locator.Touchscreen.DisableWeigh(false);
        }

        /// <summary>
        /// Generates a new batch number.
        /// </summary>
        private void GenerateNewBatchCommandExecute()
        {
            NouvemMessageBox.Show(Message.NewBatchNumberConfirmation, NouvemMessageBoxButtons.YesNo, ApplicationSettings.TouchScreenMode);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            this.AutoGenerateNewBatchCommand();
        }

        /// <summary>
        /// Handle the batch refrence input.
        /// </summary>
        private void InputReferenceCommandExecute()
        {
            Messenger.Default.Send(Tuple.Create(ViewType.Keyboard, ViewType.APReceiptTouchscreen));
        }

        /// <summary>
        /// Switch the view (supplier or orders)
        /// </summary>
        /// <param name="view">The view to switch to.</param>
        private void SwitchViewCommandExecute(string view)
        {
            Messenger.Default.Send(Token.Message, Token.CloseMessageBoxWindow);
            this.IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (view.Equals(Constant.Suppliers))
                    {
                        this.Locator.TouchscreenSuppliers.MasterModule = ViewType.APReceipt;
                        if (!this.PartnersScreenCreated)
                        {
                            this.PartnersScreenCreated = true;
                            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPartners);
                            //this.Locator.TouchscreenOrders.OnEntry();
                            return;
                        }

                        //Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen), Token.DisplayPartnerSales);
                        Messenger.Default.Send(false, Token.CloseTouchscreenPartnersWindow);
                        this.Locator.TouchscreenSuppliers.OnEntry();
                        return;
                    }

                    if (view.Equals(Constant.Products))
                    {
                        if (ApplicationSettings.OnlyShowSupplierProductsAtIntake)
                        {
                            this.Locator.TouchscreenProducts.PartnerId = this.SelectedPartner?.BPMasterID;
                        }

                        this.Locator.TouchscreenProducts.Module = ViewType.APReceipt;
                        this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.TouchscreenProducts;
                        return;
                    }

                    if (view.Equals("Batch Orders"))
                    {
                        if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
                        {
                            if (this.SelectedSaleDetail == null || this.SelectedSaleDetail.SaleDetailID == 0)
                            {
                                return;
                            }

                            var lineBatches =
                                this.DataManager.GetIntakeLineBatchNumbers(this.SelectedSaleDetail.SaleDetailID);
                            if (lineBatches.Count > 1)
                            {
                                var collectionData = (from batch in lineBatches.Where(x => x != null)
                                    select new CollectionData {Data = batch.Number, ID = batch.BatchNumberID, Identifier = Constant.Batch}).ToList();

                                Messenger.Default.Send(collectionData, Token.DisplayTouchscreenCollection);
                                Messenger.Default.Send(ViewType.CollectionDisplay);
                            }

                            return;
                        }

                        if (!this.OrdersScreenCreated)
                        {
                            this.OrdersScreenCreated = true;
                            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenOrder);
                            return;
                        }

                        Messenger.Default.Send(false, Token.CloseTouchscreenOrdersWindow);
                        this.Locator.TouchscreenOrders.OnEntry();

                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            this.Locator.TouchscreenOrders.SearchByAttribute = true;
                        }));

                        return;
                    }
                   
                    Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen), Token.DisplayPartnerSales);
                    if (!this.OrdersScreenCreated)
                    {
                        this.OrdersScreenCreated = true;
                        Messenger.Default.Send(Token.Message, Token.CreateTouchscreenOrder);
                        return;
                    }

                    Messenger.Default.Send(false, Token.CloseTouchscreenOrdersWindow);
                    this.Locator.TouchscreenOrders.OnEntry();
                });
            }).ContinueWith(task => this.IsBusy = false);
        }

        #endregion

        #region helper

        /// <summary>
        /// Sets the product tare.
        /// </summary>
        /// <param name="product">The product to set.</param>
        private void SetTare(InventoryItem product)
        {
            if (product == null)
            {
                return;
            }

            if (ApplicationSettings.ZeroTareOutsideOfDispatch)
            {
                this.Locator.Indicator.Tare = 0;
            }
            else
            {
                this.Locator.Indicator.Tare = product.PiecesTareWeight;
            }
        }

        /// <summary>
        /// Parses the supplier barcode.
        /// </summary>
        /// <param name="data">The barcode data to parse.</param>
        private void ParseBarcode(string data)
        {
            try
            {
                var localItems = this.barcodeParses
                    .Where(x => x.BPMasterID == this.SelectedPartner.BPMasterID
                                && x.INMasterID == this.SelectedSaleDetail.INMasterID).ToList();
     
                foreach (var barcodeParse in localItems)
                {
                    var startPos = barcodeParse.StartPos;
                    var endPos = barcodeParse.EndPos;
                    var value = data.Substring(startPos - 1, endPos);
                    this.SetAttributeValue(barcodeParse.Attribute, value);
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), $"Barcode Parse:{e.Message}");
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
        }

        /// <summary>
        /// Sets the typical wgt.
        /// </summary>
        private void SetTypicalWeight()
        {
            if (!this.TypicalWeight.IsNullOrZero())
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Indicator.SetManualWeight(this.TypicalWeight.ToDecimal() * this.ProductQuantity.ToDecimal());
                }));

                return;
            }

            this.Locator.Indicator.SetManualWeight(0);
        }

        /// <summary>
        /// Auto generates a new batch number.
        /// </summary>
        public void AutoGenerateNewBatchCommand()
        {
            if (ApplicationSettings.BatchCreationMode == BatchCreationMode.OnEveryProduct)
            {
                return;
            }

            if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
            {
                this.BatchNumber = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.Batch = this.BatchNumber;
                }
        
                return;
            }

            if (ApplicationSettings.ManuallyEnterBatchNumber)
            {
                Messenger.Default.Send(Tuple.Create(ViewType.Keyboard, ViewType.APReceiptDetails));
                return;
            }

            this.BatchNumber = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
            this.SelectedSaleDetail = null;
            this.CheckAttributeBatchChangeReset();
        }

        /// <summary>
        /// Gets the barcode parses.
        /// </summary>
        private void GetBarcodeParses()
        {
            this.barcodeParses = this.DataManager.GetBarcodeParses();
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpBarcodeTimer()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                if (ApplicationSettings.TouchScreenMode)
                {
                    Keypad.Show(KeypadTarget.ManualSerial);
                }
            };
        }

        /// <summary>
        /// Handles a pallet scan.
        /// </summary>
        /// <param name="palletId">The pallet id.</param>
        public void HandlePallet(int palletId)
        {
            var scanOn = this.ScannerStockMode != ScannerMode.MinusScan;
            if (this.Sale.SaleID == 0)
            {
                this.UpdateDocNumbering();
                this.CreateSale();
                var receipt = this.DataManager.AddAPReceipt(this.Sale);
                this.Sale.SaleID = receipt.Item1;
            }

            if (this.DataManager.AddOrRemoveIntakePalletStock(palletId, this.Sale.SaleID, true))
            {
                this.Locator.TouchscreenOrders.SetModule(ViewType.APReceipt);
                this.Locator.TouchscreenOrders.Sale = this.Sale;
                this.Locator.TouchscreenOrders.HandleSelectedOrder();
            }
        }

        /// <summary>
        /// Checks for an empty order, cancelling the associated batch if empty.
        /// </summary>
        private void CheckForEmptyOrder()
        {
            if (ApplicationSettings.IntakeMode != IntakeMode.WithProductionOrder || this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            var empty = false;
            if (this.SaleDetails != null)
            {
                if (!this.SaleDetails.Any())
                {
                    empty = true;
                }
                else
                {
                    empty = this.SaleDetails.All(x => x.WeightReceived <= 0);
                }
            }

            if (empty)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.EmptyBatchPrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes && this.BatchNumber != null)
                    {
                        if (this.DataManager.CancelBatch(this.BatchNumber.BatchNumberID))
                        {
                            this.Sale.NouDocStatusID = NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
                            this.CancelOrder();
                            this.ClearForm();
                            SystemMessage.Write(MessageType.Priority, string.Format(Message.BatchCancelled, this.Reference));
                        }
                    }
                }));
            }
        }

        /// <summary>
        /// Handles an incoming selected order.
        /// </summary>
        /// <param name="orderDetails">The order and supplier.</param>
        public void HandleSelectedOrder(Tuple<Sale, ViewBusinessPartner> orderDetails)
        {
            this.ClearAttributes();
            this.ClearAttributeControls();
            //StockDetail lastBatchAttribute = null;
            //if (!orderDetails.Item1.BatchNumberId.IsNullOrZero())
            //{
            //    lastBatchAttribute = this.DataManager.GetLastBatchAttributeData(orderDetails.Item1.BatchNumberId.ToInt(),
            //        NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.TransactionTypeGoodsReceiptId);
            //}

            this.highlightSelectedProduct = false;
            this.generateBatchNo = false;
            this.SelectedPartner = orderDetails.Item2;
            this.Sale = orderDetails.Item1;
            //this.Sale.LastBatchAttribute = lastBatchAttribute;
            if (this.SaleDetails != null && this.SaleDetails.Count == 1)
            {
                this.SelectedSaleDetail = this.SaleDetails.First();
                this.highlightSelectedProduct = true;
            }
            else
            {
                this.SelectedSaleDetail = null;
            }
        }

        /// <summary>
        /// Creates a new order.
        /// </summary>
        /// <param name="partner">The supplier we are creating an order for.</param>
        private void CreateNewOrder(ViewBusinessPartner partner)
        {
            this.Reference = string.Empty;
            this.SelectedPartner = partner;
            this.SaleDetails.Clear();
            this.AutoGenerateNewBatchCommand();
        }

        /// <summary>
        /// Handle the product selection.
        /// </summary>
        private void HandleProductSelection(InventoryItem product)
        {
            this.selectedWarehouse = null;
            this.StocktransactionId = null;
            var localSaleDetail = new SaleDetail
            {
                InventoryItem = product,
                NouStockMode = product.StockMode,
                INMasterID = product.Master.INMasterID,
                WeightOrdered = 0.00m,
                WeightReceived = 0.00m,
                QuantityOrdered = 0.00m,
                QuantityReceived = 0.00m,
                WeightOutstanding = 0.00m,
                QuantityOutstanding = 0.00m
            };
            
            this.SetTare(product);

            if (product.Master.WarehouseID == null)
            {
                this.Locator.WarehouseSelection.SetWarehouse(null);
            }
            else
            {
                this.selectedWarehouse =
                    NouvemGlobal.WarehouseLocations.FirstOrDefault(x => x.WarehouseID == product.Master.WarehouseID);
                if (this.selectedWarehouse != null)
                {
                    this.Locator.WarehouseSelection.SetWarehouse(this.selectedWarehouse);
                }
                else
                {
                    this.Locator.WarehouseSelection.SetWarehouse(null);
                }
            }

            var existingProduct = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localSaleDetail.INMasterID);
            if (existingProduct == null)
            {
                if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
                {
                    this.BatchNumber = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
                    localSaleDetail.Batch = this.BatchNumber;
                }

                this.SaleDetails.Add(localSaleDetail);
                this.SelectedSaleDetail = localSaleDetail;
            }
            else
            {
                if (ApplicationSettings.IntakeMode == IntakeMode.IntakeMultiBatch)
                {
                    var name = string.Empty;
                    var appProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x =>
                        x.Master.INMasterID == existingProduct.INMasterID);
                    if (appProduct != null)
                    {
                        name = appProduct.Master.Name;
                    }

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(string.Format(Message.CreateNewBatchAtIntake, name), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.EntitySelectionChange = true;
                            this.BatchNumber = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
                            existingProduct.Batch = this.BatchNumber;
                        }

                        this.SelectedSaleDetail = existingProduct;
                        this.EntitySelectionChange = false;
                        this.highlightSelectedProduct = true;
                        //this.CheckAttributeProductChangeReset(this.SelectedSaleDetail);
                    }));

                    return;
                }
                else
                {
                    this.SelectedSaleDetail = existingProduct;
                }
            }
           
            this.highlightSelectedProduct = true;
            //this.CheckAttributeProductChangeReset(this.SelectedSaleDetail);
        }

        private bool CurrentBatchTraceabilityMatch(InventoryItem product)
        {
            var localTraceabilityData = NouvemGlobal.TraceabilityData;
            var item = product.Master;

            var traceabilityTemplateNameId = item.TraceabilityTemplateNameID;
            var qualityTemplateNameId = item.QualityTemplateNameID;
            var dateTemplateNameId = item.DateTemplateNameID;

            var traceability = localTraceabilityData.FirstOrDefault(x => x.Key.Item1 == traceabilityTemplateNameId && x.Key.Item2.Equals(Constant.Traceability)).Value;
            var quality = localTraceabilityData.FirstOrDefault(x => x.Key.Item1 == qualityTemplateNameId && x.Key.Item2.Equals(Constant.Quality)).Value;
            var date = localTraceabilityData.FirstOrDefault(x => x.Key.Item1 == dateTemplateNameId && x.Key.Item2.Equals(Constant.Date)).Value;

            return true;

        }

        /// <summary>
        /// Handle the recording of the weight.
        /// </summary>
        private void RecordWeight()
        {
            if (!this.TypicalWeight.HasValue && !this.manualWeight)
            {
                this.IndicatorManager.OpenIndicatorPort();
            }

            #region validation

            if (this.DisableModule)
            {
                SystemMessage.Write(MessageType.Issue, this.MissingProcessStartUpCheckMessage);
                return;
            }

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            if (this.SelectedSaleDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoProductSelected);
                NouvemMessageBox.Show(Message.NoProductSelected, touchScreen: true);
                return;
            }

            if (ApplicationSettings.ManuallyEnterBatchNumber && (this.BatchNumber == null ||
               string.IsNullOrEmpty(this.BatchNumber.Number)))
            {
                NouvemMessageBox.Show(Message.NoBatchNoEntered, touchScreen: true);
                return;
            }

            if (this.Locator.Touchscreen.Quantity <= 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.QtyZeroStop);
                NouvemMessageBox.Show(Message.QtyZeroStop, touchScreen: true);
                return;
            }

            if (ApplicationSettings.MustEnterPalletNumberAtIntake &&
                string.IsNullOrWhiteSpace(this.PalletNumber))
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPalletNumberEntered);
                NouvemMessageBox.Show(Message.NoPalletNumberEntered, touchScreen: true);
                return;
            }

            if (ApplicationSettings.DisableManualWeighingAtIntake == this.SelectedPartner.BPMasterID &&
                !this.scanProcessing)
            {
                return;
            }

            #endregion

            #region attributes validation

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            this.CheckForUnsavedAttributes();

            #endregion

            #region weight validation

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.cleanRead = true;
                    this.manualWeight = false;
                }
                else 
                {
                    if (this.SelectedSaleDetail != null &&
                        this.SelectedSaleDetail.StockMode != StockMode.BatchQty &&
                        this.SelectedSaleDetail.StockMode != StockMode.ProductQty)
                    {
                        if (saveWeightResult.Equals(Message.ScalesInMotion))
                        {
                            this.scalesMotionTimer.Start();
                            return;
                        }
                        else
                        {
                            Messenger.Default.Send(Token.Message, Token.WeightError);
                            SystemMessage.Write(MessageType.Issue, saveWeightResult);
                            NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                            return;
                        }
                    }
                    else
                    {
                        this.cleanRead = true;
                        this.manualWeight = false;
                    }
                }
            }

            if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null)
            {
                var minWgt = this.SelectedSaleDetail.InventoryItem.MinWeight.ToDecimal();
                var maxWgt = this.SelectedSaleDetail.InventoryItem.MaxWeight.ToDecimal();
                if ((minWgt != 0 && this.Locator.Indicator.Weight < minWgt)
                    || (maxWgt != 0 && this.Locator.Indicator.Weight > maxWgt))
                {
                    var message = string.Format(Message.OutOfBoundsWeight, minWgt, maxWgt);
                    SystemMessage.Write(MessageType.Issue, message);
                    NouvemMessageBox.Show(message, touchScreen: true);
                    return;
                }
            }

            #endregion

            this.SelectedSaleDetail.Batch = this.BatchNumber;
            var stockDetail = this.CreateTransaction();
            this.CreateAttribute(stockDetail);
            this.SelectedSaleDetail.StockDetails.Add(stockDetail);
            this.SelectedSaleDetail.StockDetailToProcess = stockDetail;
            this.SelectedSaleDetail.Batch = this.BatchNumber;

            var localSaleDetail = this.SelectedSaleDetail;
            var stockTransactions = this.StockLabelsToPrint > 1 ? this.StockLabelsToPrint : 1;
            for (int i = 0; i < stockTransactions; i++)
            {
                this.CompleteTransaction(this.indicatorWeight);
            }

            Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);
            Messenger.Default.Send(Token.Message, Token.ResetSerialData);
            this.StockLabelsToPrint = 1;
            if (ApplicationSettings.TouchScreenMode)
            {
                this.CheckAttributeTransactionReset(localSaleDetail);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {
                    this.SelectedSaleDetail.TransactionCount =
                        this.DataManager.GetTransactionCount(this.SelectedSaleDetail.SaleDetailID,
                            NouvemGlobal.TransactionTypeGoodsReceiptId);
                }));
            }
        }

        /// <summary>
        /// Handle the item traceability data, completing the weight recording process.
        /// </summary>
        /// <param name="dataResults">The incoming traceability data.</param>
        private void RecordWeightComplete(Tuple<string, List<TraceabilityResult>> dataResults)
        {
            this.Log.LogDebug(this.GetType(), "CompleteTouchscreenGoodsInProcessing: Complete processing started");
            var error = dataResults.Item1;
            var data = dataResults.Item2;

            #region validation

            if (error != string.Empty)
            {
                // value hasn't been selected/entered by the user, so exit here.
                this.Log.LogDebug(this.GetType(), "Missing value. Not selected by user");
                SystemMessage.Write(MessageType.Issue, Message.TraceabilityValuesNotSet);
                NouvemMessageBox.Show(Message.TraceabilityValuesNotSet, touchScreen: true);
                return;
            }

            #endregion

            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.cleanRead = true;
                    this.manualWeight = false;
                }
                else if (saveWeightResult.Equals(Message.ScalesInMotion))
                {
                    // motion on scales, so move processing to the tick event and wait for scales stability.
                    this.waitForStabilityTraceabilityData = data;
                    this.scalesMotionTimer.Start();
                    return;
                }
                else
                {
                    Messenger.Default.Send(Token.Message, Token.WeightError);
                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                    NouvemMessageBox.Show(saveWeightResult, touchScreen: true);
                    return;
                }
            }

            if (this.BatchNumber != null && this.BatchNumber.Number != null && NouvemGlobal.BatchValuesSet.ContainsKey(this.BatchNumber.Number))
            {
                NouvemGlobal.BatchValuesSet[this.BatchNumber.Number] = true;
            }

            if (this.SelectedSaleDetail.GoodsReceiptDatas == null)
            {
                this.Log.LogDebug(this.GetType(), string.Format("Creating goods receipt data list for product:{0}", this.SelectedSaleDetail.INMasterID));
                this.SelectedSaleDetail.GoodsReceiptDatas = new List<GoodsReceiptData>();
            }

            var localGoodsReceiptData =
               this.SelectedSaleDetail.GoodsReceiptDatas.FirstOrDefault(
                   x => x.BatchNumber.Number == this.BatchNumber.Number);

            if (localGoodsReceiptData == null)
            {
                // new batch.
                this.Log.LogDebug(this.GetType(), string.Format("Creating new goods receipt data for batch number:{0}", this.BatchNumber.Number));
                localGoodsReceiptData = new GoodsReceiptData { BatchNumber = new BatchNumber { BatchNumberID = this.BatchNumber.BatchNumberID, Number = this.BatchNumber.Number } };
                localGoodsReceiptData.TransactionData = new List<StockTransactionData>();
                localGoodsReceiptData.BatchTraceabilities = new List<BatchTraceability>();
                this.SelectedSaleDetail.GoodsReceiptDatas.Add(localGoodsReceiptData);
            }

            var transactions = data.Where(x => !x.BatchAttribute).ToList();
            //if (transactions.Any())
            //{
            localGoodsReceiptData.TransactionData.Add(this.ProcessTransactions(transactions));
            //}

            var batchTraceabilityData = data.Where(x => x.BatchAttribute).ToList();
            if (batchTraceabilityData.Any())
            {
                if (localGoodsReceiptData.BatchTraceabilities == null ||
                    localGoodsReceiptData.BatchTraceabilities.TrueForAll(x => x.BatchTraceabilityID == 0))
                {
                    localGoodsReceiptData.BatchTraceabilities = this.ProcessBatchTraceabilities(batchTraceabilityData);
                }
            }

            this.Log.LogDebug(this.GetType(), string.Format("Current Wgt:{0}. Adding weight:{1}", this.SelectedSaleDetail.WeightReceived, this.indicatorWeight));

            if (ApplicationSettings.IntakeMode == IntakeMode.WithProductionOrder)
            {
                this.Sale.ProductionOrder = this.CreateProductionOrder();
            }

            this.CompleteTransaction(this.indicatorWeight);
        }

        /// <summary>
        /// Process, and add the transaction, and it's transaction traceability data to the sale detail.
        /// </summary>
        /// <param name="transactions">The transaction trceability data.</param>
        private StockTransactionData ProcessTransactions(IList<TraceabilityResult> transactions)
        {
            this.Log.LogDebug(this.GetType(), "ProcessTransactions(): Processing transactions");

            var stockTransaction = new StockTransaction();
            var transTraceabilities = new List<TransactionTraceability>();

            // add the transaction weight to our stock transaction, and other relevant data.
            stockTransaction.TransactionWeight = this.indicatorWeight;
            stockTransaction.GrossWeight = this.indicatorWeight + this.tare;
            stockTransaction.Tare = this.tare;
            stockTransaction.Pieces = this.pieces == 0 ? 1 : this.pieces;
            stockTransaction.ManualWeight = this.manualWeight;
            stockTransaction.TransactionQTY = this.ProductQuantity.ToDecimal();
            stockTransaction.INMasterID = this.SelectedSaleDetail.INMasterID;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
            stockTransaction.WarehouseID = NouvemGlobal.WarehouseIntakeId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;

            foreach (var transaction in transactions)
            {
                // add to our transaction traceability result
                var traceability = this.ParseToTransactionTraceability(transaction);
                transTraceabilities.Add(traceability);
            }

            var transactionData = new StockTransactionData
            {
                Transaction = stockTransaction,
                TransactionTraceabilities = transTraceabilities
            };

            return transactionData;
        }
       
        /// <summary>
        /// Handle the new batch creation request.
        /// </summary>
        [Obsolete]
        private void CreateNewBatch()
        {
            this.Log.LogDebug(this.GetType(), "CreateNewBatch(): Generating new batch number");
            var localBatchNo = this.BatchManager.GenerateBatchNumber(false, Strings.Intake);
            this.BatchNumber = localBatchNo;
            SystemMessage.Write(MessageType.Priority, string.Format("{0}: {1}", Message.NewBatchNumberGenerated, localBatchNo.Number));
            Messenger.Default.Send(localBatchNo, Token.NewTouchscreenBatchNumberGenerated);
        }

        /// <summary>
        /// Handle the keypad quantity selection.
        /// </summary>
        /// <param name="qty">The quantity amount.</param>
        private void HandleQtySelection(string qty)
        {
            if (this.SelectedSaleDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoOrderItemSelected);
                NouvemMessageBox.Show(Message.NoOrderItemSelected, touchScreen: true);
                return;
            }

            if (this.SelectedSaleDetail.StockMode == StockMode.Serial)
            {
                // serial stock always has a qty of 1.
                return;
            }

            this.ProductQuantity = qty.ToDecimal();

            if (!Settings.Default.AllowIntakeOverAmount)
            {
                if (this.SelectedSaleDetail.QuantityOutstanding - this.SelectedSaleDetail.TouchscreenQtyOrdered < 0)
                {
                    SystemMessage.Write(MessageType.Issue, Message.IntakeQtyAmountOver);
                    NouvemMessageBox.Show(Message.IntakeWgtAmountOver, touchScreen: true);
                    this.ProductQuantity = 0;
                }
            }
        }

        /// <summary>
        /// Prints a batch barcode.
        /// </summary>
        private void PrintBarcode()
        {
            #region validation

            if (this.scanProcessing)
            {
                return;
            }

            if (this.Sale == null)
            {
                return;
            }

            if (this.Sale.SaleDetails == null)
            {
                this.Sale.SaleDetails = new List<SaleDetail>();
            }

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                NouvemMessageBox.Show(Message.NoPartnerSelected, touchScreen: true);
                return;
            }

            var unfilled = this.CheckAttributeData();
            if (unfilled != string.Empty)
            {
                var message = string.Format(Message.AttributeValueMissing, unfilled);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            try
            {
                if (!this.ValidateData(this.traceabilityDataLabel, this.Sale.SaleID,
                    0, 0, 0,
                    NouvemGlobal.TransactionTypeGoodsReceiptId, this.Locator.ProcessSelection.SelectedProcess.ProcessID, Constant.Intake))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion

            // use the first sale line as the master table id in order to pull back the dispatch data for the label.
            int? orderId = null;
            var productId = NouvemGlobal.InventoryItems.First().Master.INMasterID;
            if (this.SaleDetails != null && this.SaleDetails.Any())
            {
                var firstId = this.SaleDetails.First().SaleDetailID;
                if (firstId > 0)
                {
                    orderId = firstId;
                }

                productId = this.SaleDetails.Last().INMasterID;
            }

            if (this.BatchNumber == null)
            {
                this.AutoGenerateNewBatchCommand();
            }

            // fetch the traceability values
            var stockTransaction = new StockTransaction();
            stockTransaction.TransactionWeight = 0;
            stockTransaction.GrossWeight = 0;
            stockTransaction.Tare = 0;
            stockTransaction.BatchNumberID = this.BatchNumber.BatchNumberID;
            stockTransaction.MasterTableID = orderId;
            stockTransaction.ManualWeight = this.manualWeight;
            stockTransaction.TransactionQTY = 0;
            stockTransaction.INMasterID = productId;
            stockTransaction.TransactionDate = DateTime.Now;
            stockTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
            stockTransaction.WarehouseID = NouvemGlobal.WarehouseIntakeId;
            stockTransaction.DeviceMasterID = NouvemGlobal.DeviceId;
            stockTransaction.UserMasterID = NouvemGlobal.UserId;
            stockTransaction.BPMasterID = this.SelectedPartner.BPMasterID;
            stockTransaction.Deleted = DateTime.Now;
            stockTransaction.Consumed = DateTime.Now;
            stockTransaction.Reference = -1;
            stockTransaction.Comments = ApplicationSettings.IntakeMode.ToString();

            var id = this.DataManager.AddTransaction(stockTransaction);
            if (id > 0)
            {
                this.Print(id, LabelType.Shipping, true, process: this.Locator.ProcessSelection.SelectedProcess,
                    batchLabel:true);
                SystemMessage.Write(MessageType.Priority, Message.BatchBarcodePrinted);
            }
        }

        /// <summary>
        /// Handle the touchscreen load completion notification.
        /// </summary>
        private void HandleTouchscreenLoad()
        {
            if (!this.highlightSelectedProduct)
            {
                this.SelectedSaleDetail = null;
            }
            else
            {
                // Need to regenerate the ui traceability data
                this.SelectedSaleDetail = this.SelectedSaleDetail;
            }

            this.highlightSelectedProduct = false;
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.timer.Interval = TimeSpan.FromSeconds(1.5);
            this.timer.Tick += (sender, args) =>
            {
                this.timer.Stop();
                if (this.SelectedSaleDetail != null)
                {
                    // If there are no transactions recorded against this product, remove.
                    if (this.SelectedSaleDetail.GoodsReceiptDatas != null &&
                        this.SelectedSaleDetail.GoodsReceiptDatas.All(
                            x => x.TransactionData == null || !x.TransactionData.Any()))
                    {
                        // Deleting product
                        if (this.RemoveItem(ViewType.APReceipt))
                        {
                            SystemMessage.Write(MessageType.Priority, Message.ProductRemoved);
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                    }
                }
            };

            this.scalesMotionTimer.Interval = TimeSpan.FromMilliseconds(5);
            this.scalesMotionTimer.Tick += (sender, args) =>
            {
                if (this.scalesStabilityWaitTicks == ApplicationSettings.WaitForScalesToStable)
                {
                    this.scalesStabilityWaitTicks = 0;
                    this.scalesMotionTimer.Stop();
                    NouvemMessageBox.Show(Message.ScalesInMotion, touchScreen: true);
                    return;
                }

                if (!this.Locator.Indicator.ScalesInMotion)
                {
                    this.scalesMotionTimer.Stop();
                    this.scalesStabilityWaitTicks = 0;
                    this.RecordWeight();
                    return;
                }
                else
                {
                    this.scalesStabilityWaitTicks++;
                }
            };
        }

        /// <summary>
        /// Creates a production batch.
        /// </summary>
        private ProductionData CreateProductionOrder()
        {
            // TODO need to implement production order set ups.
            //this.GetDocNumbering();

            var data = new ProductionData();

            data.BatchNumber = this.BatchNumber;
            data.Order = new PROrder
            {
                DocumentNumberingID = this.SelectedDocNumbering.DocumentNumberingID,
                Number = this.NextNumber,
                NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                ScheduledDate = DateTime.Today,
                Reference = this.Reference,
                CreationDate = DateTime.Now,
                UserID = NouvemGlobal.UserId.ToInt(),
                DeviceID = NouvemGlobal.DeviceId.ToInt()
            };

            data.Inputs = new List<PROrderInput>();
            foreach (var product in this.SaleDetails)
            {
                var input = new PROrderInput
                {
                    INMasterID = product.INMasterID,
                    Selected = false
                };

                data.Inputs.Add(input);
            }

            return data;
        }

        /// <summary>
        /// Sets the mode.
        /// </summary>
        /// <param name="process">The incoming process selection.</param>
        private void SetIntakeMode(Nouvem.Model.DataLayer.Process process)
        {
            try
            {
                var name = process.Name;
                //if (process.Name.CompareIgnoringCase("Intake"))
                //{
                //    name = "Standard";
                //}

                ApplicationSettings.IntakeMode =
                    (IntakeMode)Enum.Parse(typeof(IntakeMode), name.Replace(" ",""));
                this.SetPanel();
                this.GetAttributeAllocationApplicationData(process.ProcessID);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("SetIntoProductionMode: {0}", ex.Message));
            }
        }

        #endregion

        #endregion
    }
}

