﻿// -----------------------------------------------------------------------
// <copyright file="RegionViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Region
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class RegionViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The regions collection.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.Region> regions;

        /// <summary>
        /// The selected region.
        /// </summary>
        private Model.BusinessObject.Region selectedRegion;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RegionViewModel"/> class.
        /// </summary>
        public RegionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region message registration



            #endregion

            #region command handler

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            this.GetRegions();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected region.
        /// </summary>
        public Model.BusinessObject.Region SelectedRegion
        {
            get
            {
                return this.selectedRegion;
            }

            set
            {
                this.selectedRegion = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the regions.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.Region> Regions
        {
            get
            {
                return this.regions;
            }

            set
            {
                this.regions = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the regions.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region method



        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedRegion != null)
            {
                this.selectedRegion.Deleted = DateTime.Now;
                this.UpdateRegions();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.Regions.Remove(this.selectedRegion);
            }
        }

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateRegions();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion

        #region helper

        /// <summary>
        /// Updates the regions.
        /// </summary>
        private void UpdateRegions()
        {
            #region validation

            foreach (var region in this.regions)
            {
                if (string.IsNullOrWhiteSpace(region.Name))
                {
                    SystemMessage.Write(MessageType.Issue, Message.RegionsNameBlank);
                    return;
                }
            }

            #endregion

            try
            {
                if (this.DataManager.AddOrUpdateRegions(this.Regions))
                {
                    SystemMessage.Write(MessageType.Priority, Message.RegionsUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.RegionsNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.RegionsNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearRegion();
            Messenger.Default.Send(Token.Message, Token.CloseRegionWindow);
        }

        /// <summary>
        /// Gets the regions.
        /// </summary>
        private void GetRegions()
        {
            this.Regions = new ObservableCollection<Model.BusinessObject.Region>(this.DataManager.GetRegions());
        }

        #endregion

        #endregion
    }
}

