﻿// -----------------------------------------------------------------------
// <copyright file="ScannerTouchscreenViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Properties;

namespace Nouvem.ViewModel.Scanner
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class ScannerTouchscreenViewModel : Touchscreen.TouchscreenViewModel
    {
        #region field
       

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScannerTouchscreenViewModel"/> class.
        /// </summary>
        public ScannerTouchscreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            // Handle a products products ui swap.
            this.SwapViewCommand = new RelayCommand(() =>
            {
                if (this.CoreDisplayViewModel == this.Locator.ScannerMainDispatch)
                {
                    this.CoreDisplayViewModel = this.Locator.DispatchCount;
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    return;
                }

                if (this.CoreDisplayViewModel == this.Locator.DispatchCount)
                {
                    this.CoreDisplayViewModel = this.Locator.ScannerMainDispatch;
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    return;
                }
            });

            this.OnLoadingCommand = new RelayCommand(ViewModelLocator.ClearARDispatchTouchscreen);
            this.MoveBackCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
                ViewModelLocator.ClearScannerMainDispatch();
            });

            this.ManualScanCommand = new RelayCommand(() =>
            {
                Keypad.Show(KeypadTarget.ManualSerial, ApplicationSettings.ScannerMode, ApplicationSettings.ScannerEmulatorMode, useKeypadSides: ApplicationSettings.UseKeypadSidesAtDispatch);
            });

            #endregion
        }

        #endregion

        #region public interface

        #region property

        #endregion

        #region command

        public ICommand SwapViewCommand { get; set; }

        /// <summary>
        /// Gets the ui load event handler.
        /// </summary>
        public ICommand OnLoadingCommand { get; set; }

        /// <summary>
        /// Gets the move back command.
        /// </summary>
        public ICommand MoveBackCommand { get; set; }

        /// <summary>
        /// Gets the command to manually scan.
        /// </summary>
        public ICommand ManualScanCommand { get; set; }

        #endregion

        #endregion

        #region protected



        #endregion

        #region private



        #endregion
            
    }
}
