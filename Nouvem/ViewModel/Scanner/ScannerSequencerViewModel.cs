﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.KillLine.Sequencer;

namespace Nouvem.ViewModel.Scanner
{
    public class ScannerSequencerViewModel : SequencerViewModel
    {

        #region field

        private ViewModelBase centerViewModel;

        /// <summary>
        /// The eartag to check;
        /// </summary>
        private string eartagToVerify;

        /// <summary>
        /// The verification response.
        /// </summary>
        private string verification;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ScannerSequencerViewModel"/> class.
        /// </summary>
        public ScannerSequencerViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.MoveBackCommand = new RelayCommand<string>(s =>
            {
                if (s.Equals(Strings.Menu))
                {
                    ViewModelLocator.ClearSequencer();
                    ViewModelLocator.ClearScannerSequencer();
                    ViewModelLocator.ClearScannerSequencerMain();
                    ViewModelLocator.ClearScannerSequencerVerification();
                    Messenger.Default.Send(Token.Message, Token.CloseWindow);
                    Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                    Messenger.Default.Send(Token.Message, Token.CreateScannerModuleSelection);
                    return;
                }

                // back to main
                Messenger.Default.Send(Token.Message, Token.CloseWindow);
            });

            #endregion

            this.CenterViewModel = this.Locator.ScannerSequencerMain;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public ViewModelBase CenterViewModel
        {
            get
            {
                return this.centerViewModel;
            }

            set
            {
                this.centerViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public string Verification
        {
            get
            {
                return this.verification;
            }

            set
            {
                this.verification = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public string EartagToVerify
        {
            get
            {
                return this.eartagToVerify;
            }

            set
            {
                this.eartagToVerify = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a manual scan.
        /// </summary>
        public ICommand MoveBackCommand { get; set; }

        #endregion

        #endregion

        #region protected
        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            #region validation

            if (this.DisableScannning)
            {
                return;
            }

            #endregion

            if (data.EndsWithIgnoringCase(ApplicationSettings.KeyboardWedgeTerminator))
            {
                data = data.Substring(0, data.Length - 1);
            }

            this.Verification = string.Empty;
            if (this.CenterViewModel == this.Locator.ScannerSequencerMain)
            {
                base.HandleScannerData(data);
                return;
            }

            data = data.Replace("\r", "");

            if (data.Length > 12 && data.StartsWith("372"))
            {
                data = data.Substring(3, data.Length - 3);
            }

            this.EartagToVerify = data;
            if (this.EartagToVerify.Length < ApplicationSettings.MinSequencerEartagLength)
            {
                this.Verification = string.Format(Message.SequencerEartaglengthTooShort, ApplicationSettings.MinSequencerEartagLength);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(this.Verification, scanner: true);
                }));

                return;
            }

            var carcass = this.DataManager.VerifyEartag(data);
            if (!string.IsNullOrEmpty(carcass.Error))
            {
                this.Verification = carcass.Error;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(this.Verification, scanner: true);
                }));

                return;
            }

            if (carcass.SequencedDate.HasValue)
            {
                this.Verification = $"This animal has been sequenced on {carcass.SequencedDate.ToDate().ToString("g")}";
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(this.Verification, scanner: true);
                }));

                return;
            }

            if (!carcass.LotAuthorisedDate.HasValue)
            {
                this.Verification = $"You cannot sequence this animal as it's from an unauthorised lot: Lot No. {carcass.Number}";
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(this.Verification, scanner: true);
                }));

                return;
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                new Action(() =>
                {
                    System.Threading.Thread.Sleep(1000);
                    this.Verification = $"{carcass.Eartag} verified and in the system, waiting to be sequenced.";
                }));
        }

        /// <summary>
        /// Switch the view.
        /// </summary>
        protected override void SwitchViewCommandExecute()
        {
            if (this.CenterViewModel == this.Locator.ScannerSequencerMain)
            {
                this.Verification = string.Empty;
                this.EartagToVerify = string.Empty;
                this.CenterViewModel = this.Locator.ScannerSequencerVerification;
            }
            else
            {
                this.CenterViewModel = this.Locator.ScannerSequencerMain;
            }
        }

        #endregion

        #region private



        #endregion

    }
}
