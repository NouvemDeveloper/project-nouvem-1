﻿// -----------------------------------------------------------------------
// <copyright file="MapViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Map
{
    using System;
    using System.Text;
    using System.Windows.Input;
    using DevExpress.Xpf.Map;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;

    public class MapViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The search keywords.
        /// </summary>
        private string keywords;

        /// <summary>
        /// The search location.
        /// </summary>
        private string location;

        /// <summary>
        /// The starting index.
        /// </summary>
        private int startingIndex;

        /// <summary>
        /// The map longitude.
        /// </summary>
        private double longitude;

        /// <summary>
        /// The map latitude.
        /// </summary>
        private double latitude;

        /// <summary>
        /// The serach results.
        /// </summary>
        private string searchResults;

        /// <summary>
        // The zoom level.
        /// </summary>
        private int zoomLevel;

        /// <summary>
        /// The center point.
        /// </summary>
        private GeoPoint centerPoint;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MapViewModel"/> class.
        /// </summary>
        public MapViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Command to handle the location serach.
            this.SearchCommand = new RelayCommand(this.SearchCommandExecute);

            // Command to handle the location serach.
            this.OnSearchCompletedCommand = new RelayCommand<BingSearchCompletedEventArgs>(this.OnSearchCompletedCommandExecute);

            // Command to handle the load event.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Command to handle the unload event.
            this.OnUnloadedCommand = new RelayCommand(this.OnUnloadedCommandExecute);

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the zoom level.
        /// </summary>
        public int ZoomLevel
        {
            get
            {
                return this.zoomLevel;
            }

            set
            {
                this.zoomLevel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the center point.
        /// </summary>
        public GeoPoint CenterPoint
        {
            get
            {
                return this.centerPoint;
            }

            set
            {
                this.centerPoint = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the search results.
        /// </summary>
        public string SearchResults
        {
            get
            {
                return this.searchResults;
            }

            set
            {
                this.searchResults = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the keywords.
        /// </summary>
        public string Keywords
        {
            get
            {
                return this.keywords;
            }

            set
            {
                this.keywords = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search location.
        /// </summary>
        public string Location
        {
            get
            {
                return this.location;
            }

            set
            {
                this.location = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the keywords.
        /// </summary>
        public int StartingIndex
        {
            get
            {
                return this.startingIndex;
            }

            set
            {
                this.startingIndex = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        public double Longitude
        {
            get
            {
                return this.longitude;
            }

            set
            {
                this.longitude = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        public double Latitude
        {
            get
            {
                return this.latitude;
            }

            set
            {
                this.latitude = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to search the map.
        /// </summary>
        public ICommand SearchCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the search completion.
        /// </summary>
        public ICommand OnSearchCompletedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        #endregion

        #endregion

        #region proteced

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // mot implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the location search.
        /// </summary>
        private void SearchCommandExecute()
        {
            var searchCoordinates = Tuple.Create(this.keywords, this.location, new GeoPoint(this.longitude, this.latitude), this.startingIndex);
            Messenger.Default.Send(searchCoordinates);
        }

        /// <summary>
        /// Handles the search completion event.
        /// </summary>
        /// <param name="e">The event arg.</param>
        private void OnSearchCompletedCommandExecute(BingSearchCompletedEventArgs e)
        {
            if (e.Error != null || e.Cancelled)
            {
                return;
            }

            var result = e.RequestResult;
            if (result.ResultCode == RequestResultCode.Success)
            {
                var region = result.SearchRegion;

                if (region != null)
                {
                    this.NavigateTo(region.Location);
                }
                else if (result.SearchResults.Count > 0)
                {
                    this.NavigateTo(result.SearchResults[0].Location);
                }

                this.DisplayResults(e.RequestResult);
            }
        }

        /// <summary>
        /// Handle the load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.CenterPoint = new GeoPoint(55, -5);
            this.ZoomLevel = 6;
        }

        /// <summary>
        /// Handle the unload event.
        /// </summary>
        private void OnUnloadedCommandExecute()
        {
            ViewModelLocator.ClearMap();
        }
       
        #endregion

        #region helper

        /// <summary>
        /// Navigates the map using the navigation data.
        /// </summary>
        /// <param name="searchLocation">The search parameters.</param>
        private void NavigateTo(GeoPoint searchLocation)
        {
            this.CenterPoint = searchLocation;
            this.ZoomLevel = 8;
        }

        /// <summary>
        /// Creates the search results string.
        /// </summary>
        /// <param name="requestResult">The search result value.</param>
        private void DisplayResults(SearchRequestResult requestResult)
        {
            var resultList = new StringBuilder();
            resultList.Append(String.Format("Result Code: {0}\n", requestResult.ResultCode));
            resultList.Append(String.Format("Fault Reason: {0}\n", requestResult.FaultReason));
            resultList.Append(String.Format("Estimated Matches: {0}\n", requestResult.EstimatedMatches));
            resultList.Append(String.Format("Keyword: {0}\n", requestResult.Keyword));
            resultList.Append(String.Format("Location: {0}\n", requestResult.Location));
            resultList.Append(String.Format("\n"));

            if (requestResult.ResultCode == RequestResultCode.Success)
            {
                var resCounter = 1;
                foreach (LocationInformation resultInfo in requestResult.SearchResults)
                {
                    resultList.Append(String.Format("Result {0}:\n", resCounter));
                    resultList.Append(String.Format("Display Name: {0}\n", resultInfo.DisplayName));
                    resultList.Append(String.Format("Entity Type: {0}\n", resultInfo.EntityType));
                    resultList.Append(String.Format("Address: {0}\n", resultInfo.Address));
                    resultList.Append(String.Format("Location: {0}\n", resultInfo.Location));
                    resultList.Append(String.Format("______________________________\n"));

                    resCounter++;
                }

                if (requestResult.SearchRegion != null)
                {
                    resultList.Append(String.Format("\n===================================\n"));
                    resultList.Append(String.Format("Search region:\n"));
                    resultList.Append(String.Format("Display Name: {0}\n", requestResult.SearchRegion.DisplayName));
                    resultList.Append(String.Format("Entity Type: {0}\n", requestResult.SearchRegion.EntityType));
                    resultList.Append(String.Format("Address: {0}\n", requestResult.SearchRegion.Address));
                    resultList.Append(String.Format("Location: {0}\n", requestResult.SearchRegion.Location));
                }

                resultList.Append(String.Format("\n===================================\n"));
                resultList.Append(String.Format("Alternate search regions:\n\n"));
                resCounter = 1;
                foreach (LocationInformation locationInfo in requestResult.AlternateSearchRegions)
                {
                    resultList.Append(String.Format("Region {0}:\n", resCounter));
                    resultList.Append(String.Format("Display Name: {0}\n", locationInfo.DisplayName));
                    resultList.Append(String.Format("Entity Type: {0}\n", locationInfo.EntityType));
                    resultList.Append(String.Format("Address: {0}\n", locationInfo.Address));
                    resultList.Append(String.Format("Location: {0}\n", locationInfo.Location));
                    resultList.Append(String.Format("______________________________\n"));
                    resCounter++;
                }
            }

            this.SearchResults = resultList.ToString();
        }

        #endregion

        #endregion
    }
}
