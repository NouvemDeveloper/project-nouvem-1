﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.KillLine.Grader
{
    public class PrinterSelectionViewModel : NouvemViewModelBase
    {
       #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PrinterSelectionViewModel"/> class.
        /// </summary>
        public PrinterSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.PrinterSelectedCommand = new RelayCommand<string>(s =>
            {
                var localPrinter = s == "1" ? this.Printer1 : this.Printer2;
                Messenger.Default.Send(localPrinter, Token.PrinterSelected);
                this.Close();
            });

            this.CloseCommand = new RelayCommand(this.Close);

            #endregion

            this.GetPrinters();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets printer 1.
        /// </summary>
        public Printer Printer1 { get; set; }

        /// <summary>
        /// Gets or sets printer 2.
        /// </summary>
        public Printer Printer2 { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the printer selection command.
        /// </summary>
        public ICommand CloseCommand { get; private set; }

        /// <summary>
        /// Gets the printer selection command.
        /// </summary>
        public ICommand PrinterSelectedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the printers.
        /// </summary>
        private void GetPrinters()
        {
            var printers = this.DataManager.GetPrinters();
            this.Printer1 = printers.FirstOrDefault(x => x.PrinterNumber == 1);
            this.Printer2 = printers.FirstOrDefault(x => x.PrinterNumber == 2);
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.ClosePrinterSelectionWindow);
        }

        #endregion
    }
}
