﻿// -----------------------------------------------------------------------
// <copyright file="LairageOrderViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.KillLine.Lairage
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class LairageOrderViewModel : KillLineViewModelBase
    {
        #region field

        /// <summary>
        /// Auto copy flag.
        /// </summary>
        private bool autoCopyToLairage;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LairageOrderViewModel"/> class.
        /// </summary>
        public LairageOrderViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Tuple<ViewType, InventoryItem>>(this, Token.ProductSelected,
                t =>
                {
                    var product = t.Item2;
                    if (t.Item1 == ViewType.APOrder)
                    {
                        if (product != null)
                        {
                            var detailToAdd = new SaleDetail { INMasterID = product.Master.INMasterID };
                            this.SaleDetails.Add(detailToAdd);
                            this.SelectedSaleDetail = detailToAdd;
                        }
                    }
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(
                this,
                Token.BusinessPartnerSelected,
                partner =>
                {
                    if (this.Locator.BPSearchData.CurrentSearchType == ViewType.APOrder)
                    {
                        this.SelectedPartner =
                            this.Suppliers.FirstOrDefault(x => x.BPMasterID == partner.Details.BPMasterID);
                    }
                });

            #endregion

            #region command handler

            // Handler to display the search screen
            this.DisplaySearchScreenCommand = new RelayCommand<KeyEventArgs>(e => this.DisplaySearchScreenCommandExecute(Tuple.Create(true, ViewType.APOrder)),
                e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"));

            // Handler to search for a sale using the user input search text.
            this.FindSaleCommand = new RelayCommand(this.FindSaleCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(() => this.RemoveItem(ViewType.APOrder));

            #endregion

            this.GetLocalDocNumberings();
            this.AutoCopyToLairage = ApplicationSettings.AutoCopyToLairage;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether we are auto copying order to a lairage intake.
        /// </summary>
        public bool AutoCopyToLairage
        {
            get
            {
                return this.autoCopyToLairage;
            }

            set
            {
                this.autoCopyToLairage = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand FindSaleCommand { get; private set; }

        #endregion

        #region method



        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (searchSale != null)
            {
                this.Sale = this.DataManager.GetLairageOrderByID(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetLairageOrderByLastEdit();
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetLairageOrderByFirstLast(true);
                return;
            }

            var localSale = this.DataManager.GetLairageOrderByID(this.Sale.SaleID - 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetLairageOrderByFirstLast(false);
                return;
            }

            var localSale = this.DataManager.GetLairageOrderByID(this.Sale.SaleID + 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetLairageOrderByFirstLast(true);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetLairageOrderByFirstLast(false);
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            base.OnLoadingCommandExecute();
            this.ClearForm();
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            ApplicationSettings.AutoCopyToLairage = this.autoCopyToLairage;
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        protected override void DirectSearchCommandExecute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                this.ShowSearchGridCommandExecute(Constant.APOrder);
            }
            else if (this.CurrentMode == ControlMode.Find)
            {
                this.FindSaleCommandExecute();
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetAllLairageOrdersByDate(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date);
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllLairageOrdersByDate(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date);
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales,ViewType.LairageOrder), Token.SearchForSale);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.LairageOrder);
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateSale();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Overide the selected sale, to reinstate the sale id.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            var localSaleId = this.Sale.SaleID;
            base.HandleSelectedSale();
            this.Sale.SaleID = localSaleId;
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            #region validation

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (this.SelectedKillType == null || this.SelectedKillType.NouKillTypeID == 0)
            {
                error = Message.NoKillTypeSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }
            else if (!string.IsNullOrEmpty(this.DeliveryTime) && !this.DeliveryTime.IsValidTime())
            {
                error = Message.InvalidTime;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            try
            {
                this.CreateSale();
                this.UpdateDocNumbering();
                ProgressBar.SetUp(0, 3);
                ProgressBar.Run();
                var newSaleId = this.DataManager.AddLairageOrder(this.Sale);
                if (newSaleId)
                {
                    if (this.AutoCopyToLairage)
                    {
                        try
                        {
                            this.Locator.Lairage.CreateLairgeFromOrder(this.Sale.SaleID);
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), ex.Message);
                        }
                    }

                    SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCreated, this.NextNumber));
                    this.ClearForm();
                    this.NextNumber = this.SelectedDocNumbering.NextNumber;
                    this.RefreshDocStatusItems();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.OrderNotCreated);
                }
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Finds a sale quote.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            string error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (this.SelectedKillType == null || this.SelectedKillType.NouKillTypeID == 0)
            {
                error = Message.NoKillTypeSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            this.CreateSale();

            if (this.DataManager.UpdateLairageOrder(this.Sale))
            {
                SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderUpdated, this.NextNumber));
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.OrderNotUpdated, this.NextNumber));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.LairageOrder)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearLairageOrder();
            Messenger.Default.Send(Token.Message, Token.CloseLairageOrderWindow);
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            // not implemented
        }

        /// <summary>
        /// Removes an item from the grid.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            this.RemoveItem(ViewType.LairageOrder);
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
           // not implemented
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution

        #endregion

        #region helper

        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public void RefreshSales()
        {
            if (this.Locator.SalesSearchData.AssociatedView != ViewType.LairageOrder)
            {
                return;
            }

            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = this.DataManager.GetAllLairageOrdersByDate(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date);
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllLairageOrdersByDate(orderStatuses,
                    ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date);
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.LairageOrder), Token.SearchForSale);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.LairageOrder);
            }
        }

        #endregion

        #endregion
    }
}

