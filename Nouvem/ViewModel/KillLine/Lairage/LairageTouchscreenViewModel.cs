﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.KillLine.Sequencer;

namespace Nouvem.ViewModel.KillLine.Lairage
{
    public class LairageTouchscreenViewModel : LairageViewModel
    {
        #region field
        

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LairageTouchscreenViewModel"/> class.
        /// </summary>
        public LairageTouchscreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<bool>(this, Token.UpdateShowAttributes, b => ApplicationSettings.ShowLairageAttributes = b);

            // Register for the incoming supplier.
            Messenger.Default.Register<string>(this, Token.ManualTagEnteredAtSequencer, s =>
            {
                if (string.IsNullOrWhiteSpace(s))
                {
                    return;
                }

                this.Eartag = s;
                this.AddTransaction();
            });

            // Register for the incoming supplier.
            Messenger.Default.Register<ViewBusinessPartner>(this, Token.SupplierSelected, s =>
            {
                this.StockDetails.Clear();
                this.SelectedPartner = s;
            });

            // Register for an incoming keypad value for the qty.
            Messenger.Default.Register<string>(this, KeypadTarget.ManualSerial, s =>
            {
                if (!string.IsNullOrEmpty(s))
                {
                    this.HandleScannerData(s);
                }
            });

            #endregion

            #region command handler

            this.SwitchViewCommand = new RelayCommand<string>(this.SwitchViewCommandExecute);
            this.ShowKeyboardCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(true, Token.DisplayLairageKeyboard);
                Messenger.Default.Send(Token.Message, Token.FocusToEartag);
            });

            #endregion

            this.IsFocusedSaleModule = true;
            this.IsFormLoaded = true;
            this.SetUpTimer();
            this.Locator.Sequencer.DisableScannning = true;
            this.Locator.Sequencer.LairageSequencerCombo = true;
            this.HandlePartner();
            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowSequencerAttributes;
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command

        /// <summary>
        /// Gets the command to switch the view.
        /// </summary>
        public ICommand ShowKeyboardCommand { get; }

        /// <summary>
        /// Gets the command to switch the view.
        /// </summary>
        public ICommand SwitchViewCommand { get; }

        #endregion

        #endregion

        #region protected


        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddTransaction()
        {
            this.Log.LogDebug(this.GetType(), "AddTransaction(): Adding a transaction");

            #region validation

            var error = string.Empty;

            if (!this.ValidateTime(this.TimeInTransit))
            {
                return;
            }

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoSupplierSelected;
            }
            else if (this.ProposedKillDate == null)
            {
                error = Message.NoProposedKillDateEntered;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (ApplicationSettings.MustSelectAgentAtLairage && this.SelectedAgent == null)
            {
                error = Message.NoAgentSelected;
            }

            if (this.KillType == KillType.Beef)
            {
                if (ApplicationSettings.MaxLairageBeefIntake > 0 &&
                         this.StockDetails.Count == ApplicationSettings.MaxLairageBeefIntake)
                {
                    error = string.Format(Message.LairageIntakeMaxReached, ApplicationSettings.MaxLairageBeefIntake);
                    var localError = error;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(localError);
                    }));
                }
                else if (string.IsNullOrWhiteSpace(this.Eartag))
                {
                    error = Message.NoEartagEntered;
                }
            }

            if (this.KillType == KillType.Sheep)
            {
                if (this.SelectedCategory == null)
                {
                    error = Message.NoCategoryEntered;
                }

                if (!ApplicationSettings.ManuallySequenceSheep)
                {
                    if (string.IsNullOrWhiteSpace(this.Eartag))
                    {
                        error = Message.NoEartagEntered;
                    }
                }

                if (ApplicationSettings.ManuallySequenceSheep)
                {
                    if (this.IntakeNumber.IsNullOrZero())
                    {
                        error = Message.NoSheepIntakeNumberEntered;
                    }
                }
            }

            if (this.KillType == KillType.Pig)
            {
                if (this.SelectedCategory == null)
                {
                    error = Message.NoCategoryEntered;
                }

                if (this.IntakeNumber.IsNullOrZero() && this.CurrentMode != ControlMode.Update)
                {
                    error = Message.NoSheepIntakeNumberEntered;
                }
            }

            if (error != string.Empty)
            {
                if (ApplicationSettings.TouchScreenMode)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    NouvemMessageBox.Show(error, touchScreen: true);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, error);
                }

                return;
            }

            #endregion

            var transactionCount = 1;
            if (this.KillType == KillType.Sheep && ApplicationSettings.ManuallySequenceSheep)
            {
                transactionCount = this.IntakeNumber.ToInt();
            }

            if (this.KillType == KillType.Pig && this.CurrentMode != ControlMode.Update)
            {
                transactionCount = this.IntakeNumber.ToInt();
            }

            if (transactionCount > 1)
            {
                ProgressBar.SetUp(1, transactionCount);
            }

            for (int i = 0; i < transactionCount; i++)
            {
                ProgressBar.Run();
                var tag = this.Eartag;
                this.CreateSale();
                if (this.Sale.SaleID == 0)
                {
                    this.AddSale();
                }
                else
                {
                    this.UpdateSale();
                }

                if (this.FreezerIntakeNumber.HasValue)
                {
                    this.FreezerIntakeNumber--;
                }

                this.HandleTouchscreenEntry(tag);
            }

            ProgressBar.Reset();

            if (transactionCount > 1)
            {
                SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                this.ClearAttributes();
            }

            this.SelectedStockDetail = null;
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        /// <summary>
        /// Handle the selected stock detail parse/edit.
        /// </summary>
        /// <param name="detail">The detail to parse for edit.</param>
        protected override void HandleSelectedStockDetail(StockDetail detail)
        {
            base.HandleSelectedStockDetail(detail);
            this.HandleTouchscreenEntry(detail.Eartag);
            //this.Locator.Sequencer.ScannedEartagStockDetails.Clear();
            //this.Locator.Sequencer.ScannedEartagStockDetails.Add(this.SelectedStockDetail);
            //this.Locator.Sequencer.SelectedScannedEartagStockDetail =
            //    this.Locator.Sequencer.ScannedEartagStockDetails.FirstOrDefault();
        }

        /// <summary>
        /// Handles an attribute value selectionn.
        /// </summary>
        /// <param name="c">The attribute selection data.</param>
        protected override void HandleAttributeValueSelection(CollectionData c)
        {
            //if (c.Identifier.Equals(Constant.TraceabilityAttribute))
            //{
            //    var category = this.Categories.FirstOrDefault(x => x.CategoryID == c.ID);
            //    if (category != null)
            //    {
            //        this.SelectedCategory = category;
            //    }
            //}
        }

        /// <summary>
        /// Handles a partner change, setting the ui name.
        /// </summary>
        protected override void HandlePartner()
        {
            this.PartnerName = this.SelectedPartner != null ? this.SelectedPartner.Name : Strings.NoSupplier;
        }

        /// <summary>
        /// Handles a newly entered animal, auto sequencing it.
        /// </summary>
        /// <param name="eartag"></param>
        protected override void HandleTouchscreenEntry(string eartag)
        {
            this.Locator.Sequencer.HandleEartagInput(eartag);
        }

        /// <summary>
        /// Gets the animal breeds.
        /// </summary>
        protected override void GetBreeds()
        {
            this.Breeds = new ObservableCollection<NouBreed>(this.DataManager.GetBreeds());
            this.Locator.Sequencer.Breeds = new ObservableCollection<NouBreed>(this.Breeds);
        }

        /// <summary>
        /// Closes the other kill modules.
        /// </summary>
        protected override void CloseOtherKillModules(ViewType currentModule)
        {
            base.CloseOtherKillModules(ViewType.LairageIntakeTouchscreen);
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            Messenger.Default.Send(ViewType.LairageIntake, Token.SetDataContext);
            Messenger.Default.Send(true, Token.DisableLegacyAttributesView);

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Messenger.Default.Send(true, Token.DisableSequencerHerdNo);
            }));
        }

        #endregion

        #region private

        #region command

        /// <summary>
        /// Switch the view (supplier or orders)
        /// </summary>
        /// <param name="view">The view to switch to.</param>
        private void SwitchViewCommandExecute(string view)
        {
            this.IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (view.Equals(Constant.Suppliers))
                    {
                        this.Locator.TouchscreenSuppliers.MasterModule = ViewType.LairageIntakeTouchscreen;
                        if (!this.PartnersScreenCreated)
                        {
                            this.PartnersScreenCreated = true;
                            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPartners);
                            //this.Locator.TouchscreenOrders.OnEntry();
                            return;
                        }

                        //Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen), Token.DisplayPartnerSales);
                        Messenger.Default.Send(false, Token.CloseTouchscreenPartnersWindow);
                        this.Locator.TouchscreenSuppliers.OnEntry();
                        return;
                    }

                    this.Locator.TouchscreenOrders.SetModule(ViewType.LairageIntake);
                    Messenger.Default.Send(Tuple.Create(this.SelectedPartner, ViewType.APReceiptTouchscreen), Token.DisplayPartnerSales);
                    if (!this.OrdersScreenCreated)
                    {
                        this.OrdersScreenCreated = true;
                        Messenger.Default.Send(Token.Message, Token.CreateTouchscreenOrder);
                        return;
                    }

                    Messenger.Default.Send(false, Token.CloseTouchscreenOrdersWindow);
                    this.Locator.TouchscreenOrders.OnEntry();
                });
            }).ContinueWith(task => this.IsBusy = false);
        }

        #endregion

        #region helper

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.ScannerStockMode = ScannerMode.Scan;
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                Global.Keypad.Show(KeypadTarget.ManualSerial);
            };
        }

        #endregion

        #endregion
    }
}
