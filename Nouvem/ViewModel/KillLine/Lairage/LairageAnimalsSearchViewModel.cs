﻿// -----------------------------------------------------------------------
// <copyright file="SalesSearchDataViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Entity.Migrations.History;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Grid;
using Microsoft.Reporting.WinForms;

namespace Nouvem.ViewModel.KillLine.Lairage
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Class that handles the search for the sale orders.
    /// </summary>
    public class LairageAnimalsSearchViewModel : KillLineViewModelBase
    {
        #region field

        /// <summary>
        /// The selected grid sale.
        /// </summary>
        private Sale selectedSale;

        /// <summary>
        /// The ui mode.
        /// </summary>
        private ViewType mode;

        /// <summary>
        /// The stock data to update.
        /// </summary>
        private IList<StockDetail> stockToUpdate = new List<StockDetail>();

        /// <summary>
        /// The selected sales.
        /// </summary>
        private ObservableCollection<StockDetail> selectedAnimals = new ObservableCollection<StockDetail>();

        /// <summary>
        /// The filtered search sales.
        /// </summary>
        private IList<Sale> searchSales;

        /// <summary>
        /// The selected grid sale.
        /// </summary>
        private Sale currentSale;

        /// <summary>
        /// The search from date.
        /// </summary>
        private DateTime fromDate;

        /// <summary>
        /// The searcht to date.
        /// </summary>
        private DateTime toDate;

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        protected bool keepVisible;

        /// <summary>
        /// The show all orders flag..
        /// </summary>
        protected bool showAllOrders;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LairageAnimalsSearchViewModel"/> class.
        /// </summary>
        public LairageAnimalsSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            #endregion

            #region command registration

            // The handler for the grid selection changed event.
            this.OnSelectionChangedCommand = new RelayCommand(this.OnSelectionChangedCommandExecute);

            // Handle the ui partner selection.
            this.SaleSelectedCommand = new RelayCommand(this.SaleSelectedCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the form closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            // Handle the form closing.
            this.RefreshCommand = new RelayCommand(() =>
            {
                this.Refresh();
                SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
            });

            #endregion

            #region instantiation

            this.SelectedAnimals = new ObservableCollection<StockDetail>();

            #endregion

            this.GetDestinationsFull();
            this.GetCategories();
            this.GetFAValues();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        public ViewType Mode
        {
            get
            {
                return this.mode;
            }

            set
            {
                this.mode = value;

                if (this.Mode == ViewType.LairageKillInformation)
                {
                    this.FormName = Strings.LairageKillInformation;
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.LairageKillInformationSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.LairageKillInformationSearchType;
                }
                else if (this.Mode == ViewType.BeefReport)
                {
                    this.FormName = Strings.ScotBeefExport;
                    this.SearchTypes = new List<string> { Strings.KillDate };
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.BeefReportSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.BeefReportSearchType;
                    this.SetControlMode(ControlMode.Export);
                }
                else if (this.Mode == ViewType.Identigen)
                {
                    this.FormName = Strings.Identigen;
                    this.SearchTypes = new List<string> { Strings.KillDate };
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.IdentigenSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.IdentigenSearchType;
                }
                else if (this.Mode == ViewType.KillDetails)
                {
                    this.FormName = Strings.KillDetails;
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.KillDetailsSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.KillDetailsSearchType;
                }
                else if (this.Mode == ViewType.RPAReport)
                {
                    this.FormName = Strings.RPA;
                    this.SearchTypes = new List<string> { Strings.KillDate };
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.RPAReportSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.RPAReportSearchType;
                }
                else if (this.Mode == ViewType.AnimalMovements)
                {
                    this.FormName = Strings.AnimalMovements;
                    this.SearchTypes = new List<string> { Strings.KillDate };
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.AnimalMovermentsSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.AnimalMovermentsSearchType;
                }
                else if (this.Mode == ViewType.AnimalPricing)
                {
                    this.FormName = Strings.AnimalPricing;
                    this.SearchTypes = new List<string> { Strings.PaymentDate };
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.AnimalPricingSearchType)
                        ? Strings.PaymentDate
                        : ApplicationSettings.AnimalPricingSearchType;
                }
                else
                {
                    this.FormName = Strings.Payments;
                    this.SearchTypes = new List<string> { Strings.KillDate };
                    this.SelectedSearchType = string.IsNullOrEmpty(ApplicationSettings.PaymentsSearchType)
                        ? Strings.KillDate
                        : ApplicationSettings.PaymentsSearchType;
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected sales.
        /// </summary>
        public StockDetail SelectedAnimal { get; set; }

        /// <summary>
        /// Gets or sets the selected sales.
        /// </summary>
        public ObservableCollection<StockDetail> SelectedAnimals
        {
            get
            {
                return this.selectedAnimals;
            }

            set
            {
                if (value != null)
                {
                    if (value.Any())
                    {
                        this.SetControlMode(ControlMode.Add);
                    }
                    else
                    {
                        this.SetControlMode(ControlMode.OK);
                    }
                }

                this.selectedAnimals = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime FromDate
        {
            get
            {
                return this.fromDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.fromDate = DateTime.Today;
                }
                else
                {
                    this.fromDate = value;
                }

                this.RaisePropertyChanged();

                if (value > this.toDate)
                {
                    this.ToDate = value;
                    return;
                }

                if (this.IsFormLoaded)
                {
                    ApplicationSettings.SalesSearchFromDate = value;
                    //Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime ToDate
        {
            get
            {
                return this.toDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.toDate = DateTime.Today;
                }
                else
                {
                    this.toDate = value;
                }

                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    ApplicationSettings.SalesSearchToDate = value;
                    //Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected search sale.
        /// </summary>
        public Sale SelectedSale
        {
            get
            {
                return this.selectedSale;
            }

            set
            {
                if (value != null && this.selectedAnimals.Count < 2)
                {
                    this.selectedSale = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether all the orders are to be dispalyed.
        /// </summary>
        public bool ShowAllOrders
        {
            get
            {
                return this.showAllOrders;
            }

            set
            {
                this.showAllOrders = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded)
                {
                    ApplicationSettings.AnimalsSearchShowAllOrders = value;
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search is emanating from the master vm i.e. report display search.
        /// </summary>
        public bool ReportMode { get; set; }

        /// <summary>
        /// Gets or sets the associated view.
        /// </summary>
        public ViewType AssociatedView { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the grid selection changed command handler.
        /// </summary>
        public ICommand OnSelectionChangedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the sale selection.
        /// </summary>
        public ICommand SaleSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected override void GetSearchTypes()
        {
            this.SearchTypes = new List<string> { Strings.KillDate, Strings.ProposedKilldate, };
        }

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected override void HandleSelectedSearchType()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Refresh();
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            #endregion

            if (mode == Constant.Email)
            {
                if (this.Mode == ViewType.AnimalMovements)
                {
                    try
                    {
                        var file = Path.Combine(Settings.Default.ToAimsPath, string.Format("animalMovements{0}.txt", DateTime.Today.ToString("ddMMyyyy")));
                        this.DepartmentBodiesManager.SendEmail(file);
                    }
                    catch (Exception ex)
                    {
                        SystemMessage.Write(MessageType.Issue, ex.Message);
                    }

                    return;
                }

                if (this.Mode == ViewType.BeefReport)
                {
                    try
                    {
                        var file = Path.Combine(Settings.Default.KillDataPath, string.Format("ScotBeef{0}.csv", DateTime.Today.ToString("ddMMyyyy")));
                        this.DepartmentBodiesManager.SendEmail(file);
                    }
                    catch (Exception ex)
                    {
                        SystemMessage.Write(MessageType.Issue, ex.Message);
                    }
                }

                return;
            }

            if (this.Mode == ViewType.RPAReport)
            {
                var start = this.FromDate;
                var end = this.ToDate;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter {Name = "Start", Values = {start.ToString()}},
                     new ReportParameter {Name = "End", Values = {end.ToString()}}
                };

                var reportData = new ReportData { Name = ReportName.RPAExportReport, ReportParameters = reportParam };

                try
                {
                    if (preview)
                    {
                        this.ReportManager.PreviewReport(reportData);
                    }
                    else
                    {
                        this.ReportManager.PrintReport(reportData);
                    }

                    this.Locator.ReportViewer.AttachmentType = "CSV";
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                return;
            }

            if (this.Mode == ViewType.Identigen)
            {
                var start = this.FromDate;
                var end = this.ToDate;

                var reportParam = new List<ReportParameter>
                {
                    new ReportParameter {Name = "Start", Values = {start.ToString()}},
                     new ReportParameter {Name = "End", Values = {end.ToString()}}
                };

                var reportData = new ReportData { Name = ReportName.IdentigenReport, ReportParameters = reportParam };

                try
                {
                    if (preview)
                    {
                        this.ReportManager.PreviewReport(reportData);
                    }
                    else
                    {
                        this.ReportManager.PrintReport(reportData);
                    }

                    this.Locator.ReportViewer.AttachmentType = "CSV";
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                return;
            }
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected virtual void SaleSelectedCommandExecute()
        {
            if (this.Mode == ViewType.LairageKillInformation)
            {
                if (this.SelectedAnimal != null)
                {
                    var localSale = new Sale { SaleID = this.SelectedAnimal.Serial.ToInt() };
                    Messenger.Default.Send(localSale, Token.SearchSaleSelectedForReport);
                    this.Close();
                }

                return;
            }

            if (this.Mode == ViewType.Payment)
            {
                this.HandleSelectedAnimals();
            }
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.FromDate = ApplicationSettings.AnimalsSearchFromDate;
            if (this.FromDate > DateTime.Today)
            {
                this.ToDate = this.FromDate;
            }
            else
            {
                this.ToDate = DateTime.Today;
            }

            this.KeepVisible = ApplicationSettings.AnimalsSearchShowKeepVisible;
            this.ShowAllOrders = ApplicationSettings.AnimalsSearchShowAllOrders;
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
            this.GetStockDetails();
            if (this.Mode == ViewType.BeefReport)
            {
                this.SetControlMode(ControlMode.Export);
            }
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected virtual void OnClosingCommandExecute()
        {
            #region message deregistration

            Messenger.Default.Unregister<string>(this);

            #endregion

            this.IsFormLoaded = false;
            this.Close();
        }

        /// <summary>
        /// Handler for the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    if (this.Mode == ViewType.Payment)
                    {
                        this.HandleSelectedAnimals();
                    }

                    break;

                case ControlMode.Export:
                    if (this.Mode == ViewType.AnimalMovements)
                    {
                        this.HandleSelectedAnimalMovements();
                        return;
                    }

                    if (this.Mode == ViewType.AnimalPricing)
                    {
                        this.HandleSelectedAnimalPricing();
                        return;
                    }

                    if (this.Mode == ViewType.BeefReport)
                    {
                        this.HandleSelectedBeefReportAnimals();
                        return;
                    }

                    this.HandleSelectedIdentigenAnimals();
                    break;

                case ControlMode.Update:
                    this.Update();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            ApplicationSettings.AnimalsSearchFromDate = this.fromDate;
            ApplicationSettings.AnimalsSearchToDate = this.ToDate;
            ApplicationSettings.AnimalsSearchShowKeepVisible = this.KeepVisible;
            ApplicationSettings.AnimalsSearchShowAllOrders = this.ShowAllOrders;

            Messenger.Default.Send(Token.Message, Token.CloseLairageAnimalsSearchWindow);
            ViewModelLocator.ClearLairageAnimalsSearch();
        }

        /// <summary>
        /// Override, to set the focus to here.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            try
            {
                if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
                {
                    return;
                }

                var authorised = this.AuthorisationsManager.AllowUserToEditCarcass;
                if (!authorised)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToEditCarcass);
                    this.SetControlMode(ControlMode.OK);
                    return;
                }

                if (e.Cell.Property.Equals("RPA") && this.Mode == ViewType.RPAReport && this.SelectedAnimal != null)
                {
                    var animalToAdd =
                            this.stockToUpdate.FirstOrDefault(
                                x => x.StockTransactionID == this.SelectedAnimal.StockTransactionID);
                    if (animalToAdd == null)
                    {
                        this.stockToUpdate.Add(this.SelectedAnimal);
                    }

                    this.SetControlMode(ControlMode.Update);
                    return;
                }

                if (e.Cell.Property.Equals("Generic2") && this.SelectedAnimal != null)
                {
                    var animalToAdd =
                        this.stockToUpdate.FirstOrDefault(
                            x => x.StockTransactionID == this.SelectedAnimal.StockTransactionID);
                    if (animalToAdd == null)
                    {
                        this.stockToUpdate.Add(this.SelectedAnimal);
                    }

                    this.SelectedAnimal.Generic2 = e.Value.ToString();
                    this.SetControlMode(ControlMode.Update);
                    return;
                }

                if (e.Cell.Property.Equals("Grade") && this.SelectedAnimal != null)
                {
                    var animalToAdd =
                        this.stockToUpdate.FirstOrDefault(
                            x => x.StockTransactionID == this.SelectedAnimal.StockTransactionID);
                    if (animalToAdd == null)
                    {
                        this.stockToUpdate.Add(this.SelectedAnimal);
                    }

                    this.SelectedAnimal.Grade = e.Value.ToString();
                    this.SetControlMode(ControlMode.Update);
                    return;
                }

                //if (e.Cell.Property.Equals("ClippedYesNo") && this.SelectedAnimal != null)
                //{
                //    var animalToAdd =
                //        this.stockToUpdate.FirstOrDefault(
                //            x => x.StockTransactionID == this.SelectedAnimal.StockTransactionID);
                //    if (animalToAdd == null)
                //    {
                //        this.stockToUpdate.Add(this.SelectedAnimal);
                //    }

                //    this.SelectedAnimal.Clipped = e.Value.ToString().YesNoToBool();
                //    this.SetControlMode(ControlMode.Update);
                //    return;
                //}

                if (e.Cell.Property.Equals("FarmAssuredString") && this.SelectedAnimal != null)
                {
                    var animalToAdd =
                        this.stockToUpdate.FirstOrDefault(
                            x => x.StockTransactionID == this.SelectedAnimal.StockTransactionID);
                    if (animalToAdd == null)
                    {
                        this.stockToUpdate.Add(this.SelectedAnimal);
                    }

                    //this.SelectedAnimal.Casualty = e.Value.ToString().YesNoToBool();
                    this.SetControlMode(ControlMode.Update);
                    return;
                }

                if (e.Cell.Property.Equals("Category.CategoryID") && this.SelectedAnimal != null)
                {
                    var animalToAdd =
                        this.stockToUpdate.FirstOrDefault(
                            x => x.StockTransactionID == this.SelectedAnimal.StockTransactionID);
                    if (animalToAdd == null)
                    {
                        this.stockToUpdate.Add(this.SelectedAnimal);
                    }

                    //this.SelectedAnimal.Casualty = e.Value.ToString().YesNoToBool();
                    this.SetControlMode(ControlMode.Update);
                    return;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Overrides the control selection, ensuring OK is always the control mode.
        /// </summary>
        /// <param name="mode">The mode to change to.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected override void Close()
        {
            this.SaveSearchType();

            if (!this.keepVisible)
            {
                ApplicationSettings.AnimalsSearchFromDate = this.fromDate;
                ApplicationSettings.AnimalsSearchToDate = this.ToDate;
                ApplicationSettings.AnimalsSearchShowKeepVisible = this.KeepVisible;
                ApplicationSettings.AnimalsSearchShowAllOrders = this.ShowAllOrders;

                Messenger.Default.Send(Token.Message, Token.CloseLairageAnimalsSearchWindow);
                ViewModelLocator.ClearLairageAnimalsSearch();
            }
        }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        private void OnSelectionChangedCommandExecute()
        {
            if (this.Mode == ViewType.LairageKillInformation)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            if (this.Mode == ViewType.KillDetails)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            if (this.Mode == ViewType.Identigen || this.Mode == ViewType.AnimalMovements || this.Mode == ViewType.BeefReport || this.Mode == ViewType.AnimalPricing)
            {
                if (this.SelectedAnimals == null || !this.SelectedAnimals.Any() || !this.IsFormLoaded)
                {
                    this.SetControlMode(ControlMode.OK);
                    return;
                }

                this.SetControlMode(ControlMode.Export);
                return;
            }

            if (this.SelectedAnimals == null || !this.SelectedAnimals.Any() || !this.IsFormLoaded)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.SetControlMode(ControlMode.Add);
        }

        #endregion

        #region helper

        /// <summary>
        /// Updates the data.
        /// </summary>
        private void Update()
        {
            if (!this.stockToUpdate.Any())
            {
                return;
            }

            var authorised = this.AuthorisationsManager.AllowUserToEditCarcass;
            if (!authorised)
            {
                SystemMessage.Write(MessageType.Issue, Message.NotAuthorisedToEditCarcass);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            foreach (var stockDetail in this.stockToUpdate)
            {
                if (!this.DataManager.EditCarcass(
                    NouvemGlobal.DeviceId.ToInt(),
                    NouvemGlobal.UserId.ToInt(),
                    null,
                    stockDetail.CarcassNumber,
                    stockDetail.WeightSide1,
                    stockDetail.WeightSide2,
                    stockDetail.Grade,
                    stockDetail.GradingDate.ToDate().Date,
                    stockDetail.CategoryID,
                    stockDetail.FarmAssuredString.YesNoToBool(),
                    stockDetail.RPA,
                    stockDetail.Generic2,
                    stockDetail.KillType))
                {
                    SystemMessage.Write(MessageType.Issue, Message.Error);
                };
            }

            this.stockToUpdate.Clear();
            this.Refresh();
            SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
            this.SetControlMode(ControlMode.OK);

            //if (this.DataManager.UpdateKillAttribute(this.stockToUpdate))
            //{
            //    this.stockToUpdate.Clear();
            //    this.Refresh();
            //    SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
            //    this.SetControlMode(ControlMode.OK);
            //}
            //else
            //{
            //    SystemMessage.Write(MessageType.Issue, Message.Error);
            //}
        }

        /// <summary>
        /// Save the search type.
        /// </summary>
        private void SaveSearchType()
        {
            if (this.Mode == ViewType.LairageKillInformation)
            {
                ApplicationSettings.LairageKillInformationSearchType = this.SelectedSearchType;
            }
            else if (this.Mode == ViewType.BeefReport)
            {
                ApplicationSettings.BeefReportSearchType = this.SelectedSearchType;
            }
            else if (this.Mode == ViewType.Identigen)
            {
                ApplicationSettings.IdentigenSearchType = this.SelectedSearchType;
            }
            else if (this.Mode == ViewType.KillDetails)
            {
                ApplicationSettings.KillDetailsSearchType = this.SelectedSearchType;
            }
            else if (this.Mode == ViewType.RPAReport)
            {
                ApplicationSettings.RPAReportSearchType = this.SelectedSearchType;
            }
            else if (this.Mode == ViewType.AnimalMovements)
            {
                ApplicationSettings.AnimalMovermentsSearchType = this.SelectedSearchType;
            }
            else if (this.Mode == ViewType.AnimalPricing)
            {
                ApplicationSettings.AnimalPricingSearchType = this.SelectedSearchType;
            }
            else
            {
                ApplicationSettings.PaymentsSearchType = this.SelectedSearchType;
            }
        }

        /// <summary>
        /// Sends the selected animals to all interestyed observers.
        /// </summary>
        private void HandleSelectedBeefReportAnimals()
        {
            if (this.StockDetails != null && this.StockDetails.Any())
            {
                try
                {
                    this.DepartmentBodiesManager.ExportBeefData(this.StockDetails);
                    SystemMessage.Write(MessageType.Priority, Message.RecordsExportedToIdentigen);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Refresh();
                    }));
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    NouvemMessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Sends the selected animals to all interestyed observers.
        /// </summary>
        private void HandleSelectedIdentigenAnimals()
        {
            if (this.SelectedAnimals != null && this.SelectedAnimals.Any())
            {
                try
                {
                    var localDate = this.SelectedAnimals.Last().GradingDate.ToDate();
                    this.DepartmentBodiesManager.UploadSamples(this.selectedAnimals,
                    ApplicationSettings.IdentigenUserName,
                    ApplicationSettings.IdentigenPassword,
                    localDate,
                    string.Format("{0} {1}", ApplicationSettings.PlantCode, ApplicationSettings.Customer));

                    SystemMessage.Write(MessageType.Priority, Message.RecordsExportedToIdentigen);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Refresh();
                    }));
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    NouvemMessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Sends the selected animals to all interestyed observers.
        /// </summary>
        private void HandleSelectedAnimalMovements()
        {
            if (this.SelectedAnimals != null && this.SelectedAnimals.Any())
            {
                try
                {
                    this.DepartmentBodiesManager.MoveAnimals(this.SelectedAnimals);
                    SystemMessage.Write(MessageType.Priority, Message.AnimalsMoved);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Refresh();
                    }));
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    NouvemMessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Sends the selected animals to all interestyed observers.
        /// </summary>
        private void HandleSelectedAnimalPricing()
        {
            if (this.SelectedAnimals != null && this.SelectedAnimals.Any())
            {
                try
                {
                    this.DepartmentBodiesManager.SendAnimalPrices(this.SelectedAnimals);
                    SystemMessage.Write(MessageType.Priority, Message.AnimalsMoved);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.Refresh();
                    }));
                }
                catch (Exception ex)
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                    NouvemMessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Sends the selected animals to all interested observers.
        /// </summary>
        private void HandleSelectedAnimals()
        {
            if (this.SelectedAnimals != null && this.SelectedAnimals.Any())
            {
                var unpaidAnimals = this.selectedAnimals.Where(x => !x.Paid);
                if (unpaidAnimals.Any())
                {
                    Messenger.Default.Send(this.SelectedAnimals.ToList(), Token.CarcassesSelected);
                    if (this.KeepVisible)
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            this.Refresh();
                        }));
                    }
                }
            }

            this.Close();
        }

        /// <summary>
        /// Refresh the calling orders.
        /// </summary>
        private void Refresh()
        {
            this.GetStockDetails();
        }

        /// <summary>
        /// Gets the lairage animals.
        /// </summary>
        private void GetStockDetails()
        {
            IList<StockDetail> details;

            if (this.Mode == ViewType.LairageKillInformation)
            {
                details = this.DataManager.GetAllCarcassSides(this.FromDate, this.ToDate, this.SelectedSearchType);
            }
            else if (this.Mode == ViewType.BeefReport)
            {
                if (this.ShowAllOrders)
                {
                    details = this.DataManager.GetAllBeefKilledAnimalDetailsByStatus(this.FromDate, this.ToDate);
                }
                else
                {
                    details = this.DataManager.GetBeefKilledAnimalDetailsByStatus(this.FromDate, this.ToDate);
                }
            }
            else if (this.Mode == ViewType.Identigen)
            {
                if (this.ShowAllOrders)
                {
                    details = this.DataManager.GetAllIdentigenKilledAnimalDetailsByStatus(this.FromDate, this.ToDate, NouvemGlobal.NouDocStatusExported.NouDocStatusID);
                }
                else
                {
                    details = this.DataManager.GetIdentigenKilledAnimalDetailsByStatus(this.FromDate, this.ToDate, NouvemGlobal.NouDocStatusExported.NouDocStatusID);
                }
            }
            else if (this.Mode == ViewType.KillDetails)
            {
                details = this.DataManager.GetAllKilledLairageAnimalDetails(this.FromDate, this.ToDate, this.SelectedSearchType);
            }
            else if (this.Mode == ViewType.RPAReport)
            {
                if (this.ShowAllOrders)
                {
                    details = this.DataManager.GetAllRPAAnimalDetails(this.FromDate, this.ToDate);
                }
                else
                {
                    details = this.DataManager.GetRPAAnimalDetails(this.FromDate, this.ToDate);
                }
            }
            else if (this.Mode == ViewType.AnimalMovements)
            {
                if (this.ShowAllOrders)
                {
                    details = this.DataManager.GetAllKilledAnimalDetailsByStatus(this.FromDate, this.ToDate, NouvemGlobal.NouDocStatusMoved.NouDocStatusID);
                }
                else
                {
                    details = this.DataManager.GetKilledAnimalDetailsByStatus(this.FromDate, this.ToDate, NouvemGlobal.NouDocStatusMoved.NouDocStatusID);
                }
            }
            else if (this.Mode == ViewType.AnimalPricing)
            {
                if (this.ShowAllOrders)
                {
                    details = this.DataManager.GetAllKilledAnimalDetailsByPrice(this.FromDate, this.ToDate);
                }
                else
                {
                    details = this.DataManager.GetAllKilledAnimalDetailsByPriceUnsent(this.FromDate, this.ToDate);
                }
            }
            else
            {
                if (this.ShowAllOrders)
                {
                    details = this.DataManager.GetAllLairageAnimalDetails(this.FromDate, this.ToDate);
                }
                else
                {
                    details = this.DataManager.GetLairageAnimalDetails(this.FromDate, this.ToDate);
                }
            }

            foreach (var detail in details)
            {
                detail.FarmAssuredString = detail.FarmAssuredYesNo;
            }

            this.StockDetails = new ObservableCollection<StockDetail>(details);
            this.SelectedAnimal = this.StockDetails.FirstOrDefault();
        }

        #endregion

        #endregion
    }
}




