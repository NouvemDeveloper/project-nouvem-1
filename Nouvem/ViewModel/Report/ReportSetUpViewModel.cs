﻿// -----------------------------------------------------------------------
// <copyright file="ReportSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.ObjectModel;

namespace Nouvem.ViewModel.Report
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Interface;

    public class ReportSetUpViewModel : ReportViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// The report user settings.
        /// </summary>
        private IList<UserReport> usersToUpdate = new List<UserReport>();

        /// <summary>
        /// The report user settings.
        /// </summary>
        private ObservableCollection<UserReport> users = new ObservableCollection<UserReport>();

        /// <summary>
        /// The report user settings.
        /// </summary>
        private UserReport selectedUser;

        /// <summary>
        /// The report alias.
        /// </summary>
        private string alias;

        private string selectedMacroColumn1;
        private string selectedMacroColumn2;
        private string selectedMacroColumn3;
        private string selectedMacroColumn4;
        private string selectedMacroColumn5;
        private string selectedMacroColumn6;
        private string selectedMacroColumn7;
        private string selectedMacroColumn8;
        private string selectedMacroColumn9;
        private string selectedMacroColumn10;
        private string reportParameter1;
        private string reportParameter2;
        private string reportParameter3;
        private string reportParameter4;
        private string reportParameter5;
        private string reportParameter6;
        private string reportParameter7;
        private string reportParameter8;
        private string reportParameter9;
        private string reportParameter10;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportSetUpViewModel"/> class.
        /// </summary>
        public ReportSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Tuple<int,bool>>(this, Token.SearchDateSelected, this.HandleSearchDate);

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            #endregion

            #region command handler

            #endregion

            this.SetDefaultData();
            this.GetMacros();
            this.GetReports();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public ObservableCollection<UserReport> Users
        {
            get
            {
                return this.users;
            }

            set
            {
                this.users = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public UserReport SelectedUser
        {
            get
            {
                return this.selectedUser;
            }

            set
            {
                this.selectedUser = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn1
        {
            get
            {
                return this.selectedMacroColumn1;
            }

            set
            {
                this.selectedMacroColumn1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn2
        {
            get
            {
                return this.selectedMacroColumn2;
            }

            set
            {
                this.selectedMacroColumn2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn3
        {
            get
            {
                return this.selectedMacroColumn3;
            }

            set
            {
                this.selectedMacroColumn3 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn4
        {
            get
            {
                return this.selectedMacroColumn4;
            }

            set
            {
                this.selectedMacroColumn4 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn5
        {
            get
            {
                return this.selectedMacroColumn5;
            }

            set
            {
                this.selectedMacroColumn5 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn6
        {
            get
            {
                return this.selectedMacroColumn6;
            }

            set
            {
                this.selectedMacroColumn6 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn7
        {
            get
            {
                return this.selectedMacroColumn7;
            }

            set
            {
                this.selectedMacroColumn7 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn8
        {
            get
            {
                return this.selectedMacroColumn8;
            }

            set
            {
                this.selectedMacroColumn8 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn9
        {
            get
            {
                return this.selectedMacroColumn9;
            }

            set
            {
                this.selectedMacroColumn9 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter macro column.
        /// </summary>
        public string SelectedMacroColumn10
        {
            get
            {
                return this.selectedMacroColumn10;
            }

            set
            {
                this.selectedMacroColumn10 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter1
        {
            get
            {
                return this.reportParameter1;
            }

            set
            {
                this.SetMode(this.reportParameter1, value);
                this.reportParameter1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter2
        {
            get
            {
                return this.reportParameter2;
            }

            set
            {
                this.SetMode(this.reportParameter2, value);
                this.reportParameter2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter3
        {
            get
            {
                return this.reportParameter3;
            }

            set
            {
                this.SetMode(this.reportParameter3, value);
                this.reportParameter3 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter4
        {
            get
            {
                return this.reportParameter4;
            }

            set
            {
                this.SetMode(this.reportParameter4, value);
                this.reportParameter4 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter5
        {
            get
            {
                return this.reportParameter5;
            }

            set
            {
                this.SetMode(this.reportParameter5, value);
                this.reportParameter5 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter6
        {
            get
            {
                return this.reportParameter6;
            }

            set
            {
                this.SetMode(this.reportParameter6, value);
                this.reportParameter6 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter7
        {
            get
            {
                return this.reportParameter7;
            }

            set
            {
                this.SetMode(this.reportParameter7, value);
                this.reportParameter7 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter8
        {
            get
            {
                return this.reportParameter8;
            }

            set
            {
                this.SetMode(this.reportParameter8, value);
                this.reportParameter8 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter9
        {
            get
            {
                return this.reportParameter9;
            }

            set
            {
                this.SetMode(this.reportParameter9, value);
                this.reportParameter9 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the match up report parameter.
        /// </summary>
        public string ReportParameter10
        {
            get
            {
                return this.reportParameter10;
            }

            set
            {
                this.SetMode(this.reportParameter10, value);
                this.reportParameter10 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether multi select is used.
        /// </summary>
        public string Alias
        {
            get
            {
                return this.alias;
            }

            set
            {
                this.SetMode(value, this.alias);
                this.alias = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application search sps.
        /// </summary>
        public IList<string> ReportMacros { get; set; }

        /// <summary>
        /// Gets or sets the application search sps.
        /// </summary>
        public IList<string> SearchScreenMacros { get; set; }

        #endregion

        #region command
        

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.SelectedReport == null)
            {
                this.SelectedReport = this.Reports.FirstOrDefault();
                return;
            }

            var previousItem =
                this.Reports.TakeWhile(
                    item => item.ReportID != this.SelectedReport.ReportID)
                    .LastOrDefault();

            if (previousItem != null)
            {
                this.SelectedReport = previousItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.SelectedReport == null)
            {
                this.SelectedReport = this.Reports.FirstOrDefault();
                return;
            }

            var nextItem =
                this.Reports.SkipWhile(
                    item => item.ReportID != this.SelectedReport.ReportID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextItem != null)
            {
                this.SelectedReport = nextItem;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (this.SelectedReport == null)
            {
                this.SelectedReport = this.Reports.FirstOrDefault();
                return;
            }

            var firstItem = this.Reports.FirstOrDefault();

            if (firstItem != null)
            {
                this.SelectedReport = firstItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (this.SelectedReport == null)
            {
                this.SelectedReport = this.Reports.LastOrDefault();
                return;
            }

            var lastItem = this.Reports.LastOrDefault();

            if (lastItem != null)
            {
                this.SelectedReport = lastItem;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            this.SelectedReport = this.DataManager.GetReportByLastEdit();
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles an update.
        /// </summary>
        protected override void UpdateCommandExecute()
        {
            if (this.selectedUser == null)
            {
                return;
            }

            if (this.usersToUpdate.Contains(this.selectedUser))
            {
                this.usersToUpdate.Remove(this.selectedUser);
            }

            this.usersToUpdate.Add(this.selectedUser);
            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Override the cancellation to close locally.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            this.SaveReportDateType();
            this.Close();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.Close();
                    break;

                case ControlMode.Update:
                    this.UpdateReport();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            this.ReportManager.GetReports();
            this.IsFormLoaded = false;
            ViewModelLocator.ClearReportSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseReportSetUpWindow);
            Messenger.Default.Unregister(this);
        }

        /// <summary>
        /// Handles the report macro selection.
        /// </summary>
        protected override void HandleSelectedReportMacro()
        {
            if (string.IsNullOrEmpty(this.SelectedReportMacro))
            {
                return;
            }

        }

        /// <summary>
        /// Handles a report selection.
        /// </summary>
        protected override void HandleSelectedReport()
        {
            this.ClearForm(false);
            if (this.SelectedReport.IsNull() || this.SelectedReport.ReportID == 0)
            {
                return;
            }

            this.SetReportUsers();
            Messenger.Default.Send(this.SelectedReport, Token.ReportSelected);
            this.SelectedSearchScreenMacro = this.SelectedReport.SearchGridMacro;
            this.SelectedReportMacro = this.SelectedReport.ReportMacro;
            this.Alias = this.SelectedReport.Alias;
            this.DisplayKeepVisible = this.SelectedReport.KeepVisible;
            this.MultiSelect = this.SelectedReport.MultiSelect;
            this.ViewMultiple = this.SelectedReport.ViewMultiple;
            this.DisplayRefresh = this.SelectedReport.Refresh;
            this.DisplayDates = this.SelectedReport.ShowDates;
            this.ShowCreateFile = this.SelectedReport.ShowCreateFile;
            this.DisplaySearchID = this.SelectedReport.ShowSearchID;
            this.DisplayShowAll = this.SelectedReport.ShowAll;
            this.DisplayDateTypes = this.SelectedReport.ShowDateTypes;
            this.DateTypes.Clear();
            foreach (var reportDateType in this.ReportDateTypes)
            {
                reportDateType.IsSelected = this.SelectedReport.ReportDateLookUps.Any(x => x.NouSearchDateTypeID == reportDateType.NouSearchDateTypeID);
            }

            if (this.SelectedReport.ReportUserDateType != null)
            {
                this.SelectedDateType = this.DateTypes.FirstOrDefault(x =>
                    x.NouSearchDateTypeID == this.SelectedReport.ReportUserDateType.ReportDateTypeID);
            }
            else
            {
                this.SelectedDateType = this.DateTypes.FirstOrDefault();
            }

            for (int i = 0; i < this.SelectedReport.ReportParams.Count; i++)
            {
                var param = this.SelectedReport.ReportParams.ElementAt(i);
                if (i == 0)
                {
                    this.ReportParameter1 = param.Name;
                    this.SelectedMacroColumn1 = param.Property;
                    continue;
                }

                if (i == 1)
                {
                    this.ReportParameter2 = param.Name;
                    this.SelectedMacroColumn2 = param.Property;
                    continue;
                }

                if (i == 2)
                {
                    this.ReportParameter3 = param.Name;
                    this.SelectedMacroColumn3 = param.Property;
                    continue;
                }

                if (i == 3)
                {
                    this.ReportParameter4 = param.Name;
                    this.SelectedMacroColumn4 = param.Property;
                    continue;
                }

                if (i == 4)
                {
                    this.ReportParameter5 = param.Name;
                    this.SelectedMacroColumn5 = param.Property;
                    continue;
                }

                if (i == 5)
                {
                    this.ReportParameter6 = param.Name;
                    this.SelectedMacroColumn6 = param.Property;
                    continue;
                }

                if (i == 6)
                {
                    this.ReportParameter7 = param.Name;
                    this.SelectedMacroColumn7 = param.Property;
                    continue;
                }

                if (i == 7)
                {
                    this.ReportParameter8 = param.Name;
                    this.SelectedMacroColumn8 = param.Property;
                    continue;
                }

                if (i == 8)
                {
                    this.ReportParameter9 = param.Name;
                    this.SelectedMacroColumn9 = param.Property;
                    continue;
                }

                if (i == 9)
                {
                    this.ReportParameter10 = param.Name;
                    this.SelectedMacroColumn10 = param.Property;
                    continue;
                }
            }
        }

        /// <summary>
        /// Handles the search screen macro selection.
        /// </summary>
        protected override void HandleSelectedSearchScreenMacro()
        {
            this.PopulateSearchGrid();
        }

        #endregion

        #region private

        /// <summary>
        /// Sets the report users.
        /// </summary>
        private void SetReportUsers()
        {
            var reportUsers = this.DataManager.GetReportSettings(this.SelectedReport.ReportID);
            this.Users.Clear();
            foreach (var localUser in NouvemGlobal.Users.OrderBy(x => x.UserMaster.FullName))
            {
                var userReport = new UserReport {UserMaster = localUser.UserMaster, UserMasterID = NouvemGlobal.UserId, ReportName = this.SelectedReport.Name, ReportID = this.SelectedReport.ReportID};
                var reportUser = reportUsers.FirstOrDefault(x => x.UserMasterID == localUser.UserMaster.UserMasterID);
                if (reportUser != null)
                {
                    this.Users.Add(new UserReport { UserMaster = localUser.UserMaster, PrinterName = reportUser.PrinterName, UserReportsID = reportUser.UserReportsID});
                    continue;
                }

                this.Users.Add(userReport);
            }
        }

        /// <summary>
        /// Clears the ui.
        /// </summary>
        private void ClearForm(bool clearReport = true)
        {
            this.ReportParameter1 = string.Empty;
            this.ReportParameter2 = string.Empty;
            this.ReportParameter3 = string.Empty;
            this.ReportParameter4 = string.Empty;
            this.ReportParameter5 = string.Empty;
            this.ReportParameter6 = string.Empty;
            this.ReportParameter7 = string.Empty;
            this.ReportParameter8 = string.Empty;
            this.ReportParameter9 = string.Empty;
            this.ReportParameter10 = string.Empty;
            this.SelectedMacroColumn1 = string.Empty;
            this.SelectedMacroColumn2 = string.Empty;
            this.SelectedMacroColumn3 = string.Empty;
            this.SelectedMacroColumn4 = string.Empty;
            this.SelectedMacroColumn5 = string.Empty;
            this.SelectedMacroColumn6 = string.Empty;
            this.SelectedMacroColumn7 = string.Empty;
            this.SelectedMacroColumn8 = string.Empty;
            this.SelectedMacroColumn9 = string.Empty;
            this.SelectedMacroColumn10 = string.Empty;
            this.MacroColumns.Clear();
            this.Alias = string.Empty;

            foreach (var reportDateType in this.ReportDateTypes)
            {
                reportDateType.IsSelected = true;
            }

            if (this.SelectedReport != null && clearReport)
            {
                this.SelectedReport = null;
            }

            this.DisplayShowAll = false;
            this.DisplayDates = false;
            this.DisplayDateTypes = false;
            this.DisplaySearchID = false;
            this.MultiSelect = false;
            this.ViewMultiple = false;
            this.DisplayKeepVisible = false;
            this.DisplayRefresh = false;
            this.ShowCreateFile = false;
            this.SelectedReportMacro = string.Empty;
            this.SelectedSearchScreenMacro = string.Empty;
            this.Users.Clear();
            this.usersToUpdate.Clear();
        }

        private void UpdateReport()
        {
            if (this.SelectedReport == null)
            {
                return;
            }

            this.SelectedReport.Alias = this.Alias;
            this.SelectedReport.KeepVisible = this.DisplayKeepVisible;
            this.SelectedReport.MultiSelect = this.MultiSelect;
            this.SelectedReport.ViewMultiple = this.ViewMultiple;
            this.SelectedReport.Refresh = this.DisplayRefresh;
            this.SelectedReport.ShowDates = this.DisplayDates;
            this.SelectedReport.ShowDateTypes = this.DisplayDateTypes;
            this.SelectedReport.ShowSearchID = this.DisplaySearchID;
            this.SelectedReport.ShowCreateFile = this.ShowCreateFile;
            this.SelectedReport.ShowAll = this.DisplayShowAll;
            this.SelectedReport.ReportMacro = this.SelectedReportMacro ?? string.Empty;
            this.SelectedReport.SearchGridMacro = this.SelectedSearchScreenMacro ?? string.Empty;
            this.SelectedReport.ReportParams = new List<ReportParam>();
            this.SelectedReport.UserReports = this.usersToUpdate;
            var dateTypeIds = this.DateTypes.Where(x => x.IsSelected).Select(x => x.NouSearchDateTypeID);
            this.SelectedReport.ReportDatesTypeIds = new List<int>(dateTypeIds);

            if (!string.IsNullOrWhiteSpace(this.ReportParameter1))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter1, Property = this.SelectedMacroColumn1, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter2))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter2, Property = this.SelectedMacroColumn2, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter3))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter3, Property = this.SelectedMacroColumn3, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter4))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter4, Property = this.SelectedMacroColumn4, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter5))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter5, Property = this.SelectedMacroColumn5, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter6))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter6, Property = this.SelectedMacroColumn6, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter7))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter7, Property = this.SelectedMacroColumn7, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter8))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter8, Property = this.SelectedMacroColumn8, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter9))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter9, Property = this.SelectedMacroColumn9, ReportID = this.SelectedReport.ReportID });
            }

            if (!string.IsNullOrWhiteSpace(this.ReportParameter10))
            {
                this.SelectedReport.ReportParams.Add(new ReportParam { Name = this.ReportParameter10, Property = this.SelectedMacroColumn10, ReportID = this.SelectedReport.ReportID });
            }

            if (this.DataManager.AddOrUpdateReport(this.SelectedReport))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                this.ClearForm();
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }
        }

        /// <summary>
        /// Gets the application report macros. 
        /// </summary>
        private void GetMacros()
        {
            var macros = this.DataManager.GetSPNames();

            this.ReportMacros = macros.Where(x => x.StartsWithIgnoringCase(Constant.ReportSP)).ToList();
            this.ReportMacros.Insert(0, string.Empty);

            this.SearchScreenMacros = macros.Where(x => x.StartsWithIgnoringCase(Constant.ReportSearch)).ToList();
            this.SearchScreenMacros.Insert(0, string.Empty);
        }

        /// <summary>
        /// Sets the default dates.
        /// </summary>
        private void SetDefaultData()
        {
            this.FromDate = DateTime.Today;
            this.ToDate = DateTime.Today;
        }

        private void HandleSearchDate(Tuple<int,bool> searchDate)
        {
            if (this.ReportDateTypes == null || !this.ReportDateTypes.Any())
            {
                return;
            }

            var localSearchDate = this.DateTypes.FirstOrDefault(x => x.NouSearchDateTypeID == searchDate.Item1);
            if (searchDate.Item2)
            {
                if (localSearchDate == null)
                {
                    var localReportSearchDate = this.ReportDateTypes.FirstOrDefault(x => x.NouSearchDateTypeID == searchDate.Item1);
                    this.DateTypes.Add(localReportSearchDate);
                }
            }
            else
            {
                if (localSearchDate != null)
                {
                    this.DateTypes.Remove(localSearchDate);
                }
            }
        }

        #endregion
    }
}

