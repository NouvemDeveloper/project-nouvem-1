﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenReportSalesSearchDataViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Report
{
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class TouchscreenReportSalesSearchDataViewModel : ReportSalesSearchDataViewModel
    {
        public TouchscreenReportSalesSearchDataViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }
        }

        /// <summary>
        /// Override, to display the touchscreen report viewer.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.SelectedSales != null && this.SelectedSales.Any())
            {
                Messenger.Default.Send(this.SelectedSales.First(), Token.SearchSaleSelectedForTouchscreenReport);
            }

            this.Close();
        }
    }
}
