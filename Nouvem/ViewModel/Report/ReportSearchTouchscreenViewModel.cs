﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Report
{
    public class ReportSearchTouchscreenViewModel : ReportSearchViewModel
    {

        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportSearchTouchscreenViewModel"/> class.
        /// </summary>
        public ReportSearchTouchscreenViewModel(ReportData report)
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            this.SetDefaultData(report);
            this.GetReports();
            this.SelectedReport = report;
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handle a row selection.
        /// </summary>
        protected override void RowSelectedCommandExecute()
        {
            #region validation

            if (this.SelectedSearchGridRow.IsNull())
            {
                return;
            }

            #endregion

            if (!this.KeepVisible)
            {
                this.Close();
            }

            this.ReportManager.ShowReport(this.SelectedSearchGridRow, this.SelectedReport, isTouchscreen:true);
        }

        #endregion

        #region private



        #endregion

    }
}
