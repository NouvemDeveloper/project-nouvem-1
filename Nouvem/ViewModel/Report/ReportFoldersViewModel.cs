﻿// -----------------------------------------------------------------------
// <copyright file="ReportFoldersViewModel .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Report
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class ReportFoldersViewModel : ReportViewModelBase
    {
        #region field

        /// <summary>
        /// The unassigned report data.
        /// </summary>
        private ObservableCollection<ReportData> unassignedReports = new ObservableCollection<ReportData>();

        /// <summary>
        /// The folder assigned report data.
        /// </summary>
        private ObservableCollection<ReportData> assignedReports = new ObservableCollection<ReportData>();

        /// <summary>
        /// The report folders.
        /// </summary>
        private ObservableCollection<ReportsFolder> reportFolders = new ObservableCollection<ReportsFolder>();

        /// <summary>
        /// The report folders.
        /// </summary>
        private ReportsFolder selectedReportFolder;

        /// <summary>
        /// The selected assigned report data.
        /// </summary>
        private ReportData selectedAssignedReport;

        /// <summary>
        /// The ui assigned reports grid content.
        /// </summary>
        private string gridFolderContent;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFoldersViewModel"/> class.
        /// </summary>
        public ReportFoldersViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler


            this.MoveReportCommand = new RelayCommand<string>(this.MoveReportCommandExecute);

            #endregion

            this.GridFolderContent = Strings.FolderReports;
            this.HasWriteAuthorisation = true;
            this.Refresh();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the assigned reports grid content.
        /// </summary>
        public string GridFolderContent
        {
            get
            {
                return this.gridFolderContent;
            }

            set
            {
                this.gridFolderContent = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the report data.
        /// </summary>
        public ReportData SelectedAssignedReport
        {
            get
            {
                return this.selectedAssignedReport;
            }

            set
            {
                this.selectedAssignedReport = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the unassigned report data.
        /// </summary>
        public ObservableCollection<ReportData> UnassignedReports
        {
            get
            {
                return this.unassignedReports;
            }

            set
            {
                this.unassignedReports = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the assigned report data.
        /// </summary>
        public ObservableCollection<ReportData> AssignedReports
        {
            get
            {
                return this.assignedReports;
            }

            set
            {
                this.assignedReports = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the report folders.
        /// </summary>
        public ObservableCollection<ReportsFolder> ReportFolders
        {
            get
            {
                return this.reportFolders;
            }

            set
            {
                this.reportFolders = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the report folders.
        /// </summary>
        public ReportsFolder SelectedReportFolder
        {
            get
            {
                return this.selectedReportFolder;
            }

            set
            {
                this.selectedReportFolder = value;
                this.RaisePropertyChanged();
                this.HandleSelectedReportFolder();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to move the report.
        /// </summary>
        public ICommand MoveReportCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Override the cancellation to close locally.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.Close();
                    break;

                case ControlMode.Update:
                    this.UpdateReportFolders();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handles an update.
        /// </summary>
        protected override void UpdateCommandExecute()
        {
            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            this.IsFormLoaded = false;
            ViewModelLocator.ClearReportFolders();
            Messenger.Default.Send(Token.Message, Token.CloseReportFoldersWindow);
            Messenger.Default.Unregister(this);
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// handles the allocation/deallocation of reports to and from the selected folder.
        /// </summary>
        /// <param name="direction">The allocation direction.</param>
        private void MoveReportCommandExecute(string direction)
        {
            #region validation

            if (this.SelectedReportFolder == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoReportFolderSelected);
                return;
            }

            if (this.SelectedReportFolder.ReportFolderId == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.FolderNotCreated);
                return;
            }

            if (!this.CanMove(direction))
            {
                return;
            }

            #endregion
 
            this.SetControlMode(ControlMode.Update);

            if (direction.Equals(Constant.MoveForward))
            {
                this.AssignedReports.Add(this.SelectedReport);
                this.UnassignedReports.Remove(this.SelectedReport);
                this.SetReportsFolder();
                return;
            }

            this.UnassignedReports.Add(this.SelectedAssignedReport);
            this.AssignedReports.Remove(this.SelectedAssignedReport);
            this.SetReportsFolder();
        }

        #endregion

        #region helper

        /// <summary>
        /// Sets the reports folder ids.
        /// </summary>
        private void SetReportsFolder()
        {
            foreach (var reportData in this.Reports.Where(x => x.ReportFolderID == this.selectedReportFolder.ReportFolderId))
            {
                reportData.ReportFolderID = null;
            }

            foreach (var reportData in this.AssignedReports)
            {
                var localReport = this.Reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(reportData.Name));
                if (localReport != null)
                {
                    localReport.ReportFolderID = this.selectedReportFolder.ReportFolderId;
                }
            }
        }

        private void HandleSelectedReportFolder()
        {
            if (this.SelectedReportFolder == null)
            {
                this.GridFolderContent = Strings.FolderReports;
                return;
            }

            this.GridFolderContent = string.Format("{0} {1}", this.SelectedReportFolder.Name, Strings.FolderReports);
            this.UnassignedReports = new ObservableCollection<ReportData>(this.Reports.Where(x => x.ReportFolderID.IsNull()));
            this.AssignedReports = new ObservableCollection<ReportData>(this.Reports.Where(x => x.ReportFolderID == this.SelectedReportFolder.ReportFolderId));
        }

        /// <summary>
        /// Determines if a move can be made.
        /// </summary>
        /// <returns>A flag, which indicates whether a move can be made.</returns>
        private bool CanMove(string direction)
        {
            if (direction.Equals(Constant.MoveForward) && this.SelectedReport == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            if (direction.Equals(Constant.MoveBack) && this.selectedAssignedReport == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NothingSelected);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Update the data.
        /// </summary>
        private void UpdateReportFolders()
        {
            if (this.DataManager.AddOrUpdateReportFolders(this.reportFolders))
            {
                this.DataManager.AddOrUpdateReports(this.Reports);
                this.Refresh();
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }
        }

        /// <summary>
        /// Gets the application folders.
        /// </summary>
        private void GetFolders()
        {
            this.ReportFolders.Clear();
            foreach (var folder in this.DataManager.GetReportFolders())
            {
                this.ReportFolders.Add(new ReportsFolder
                {
                    ReportFolderId = folder.ReportFolderID,
                    Name = folder.Name,
                    ParentId = folder.ParentID
                });
            }

            this.SelectedReportFolder = null;
        }

        /// <summary>
        /// Refreshes the ui.
        /// </summary>
        private void Refresh()
        {
            this.GetReports();
            this.GetFolders();
        }

        #endregion

        #endregion

    }
}
