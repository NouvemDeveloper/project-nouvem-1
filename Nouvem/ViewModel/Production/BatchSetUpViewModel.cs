﻿// -----------------------------------------------------------------------
// <copyright file="BatchSetUpViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Grid;
using DevExpress.XtraPrinting.Native;
using Nouvem.ViewModel.Interface;
using Nouvem.ViewModel.Production.IntoProduction;

namespace Nouvem.ViewModel.Production
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.Shared;

    public class BatchSetUpViewModel : ProductionBatchSetUpViewModel
    {
        #region field

        /// <summary>
        /// The reared in countries.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.Plant> plants;

        /// <summary>
        /// Show all the orders flag.
        /// </summary>
        private bool showAllOrders;

        /// <summary>
        /// Show all the orders flag.
        /// </summary>
        private bool? multipleReceipts;

        /// <summary>
        /// The auto complete a mix flag.
        /// </summary>
        private bool autoCompleteMix;

        /// <summary>
        /// Show all the orders flag.
        /// </summary>
        private bool? reworkStockNonAdjust;

        /// <summary>
        /// The search from date.
        /// </summary>
        private DateTime fromDate;

        /// <summary>
        /// The searcht to date.
        /// </summary>
        private DateTime toDate;

        private decimal? typicalMixQty;

        /// <summary>
        /// The spec name.
        /// </summary>
        private string specName;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool searchMode;

        /// <summary>
        /// The batches.
        /// </summary>
        private ObservableCollection<ProductionData> batches = new ObservableCollection<ProductionData>();

        /// <summary>
        /// The specs.
        /// </summary>
        private ObservableCollection<ProductionData> specifications = new ObservableCollection<ProductionData>();

        /// <summary>
        /// The selected spec.
        /// </summary>
        private ProductionData selectedSpecification = new ProductionData();

        /// <summary>
        /// The selected batch.
        /// </summary>
        private ProductionData selectedBatch = new ProductionData();

        /// <summary>
        /// The typical batch size.
        /// </summary>
        private string mixNotes;

        /// <summary>
        /// The selected spec products.
        /// </summary>
        private ObservableCollection<ProductSelection> specificationProducts = new ObservableCollection<ProductSelection>();

        /// <summary>
        /// The batch details.
        /// </summary>
        private ObservableCollection<SaleDetail> ingredients = new ObservableCollection<SaleDetail>();

        /// <summary>
        /// The batch details.
        /// </summary>
        private SaleDetail selectedIngredient;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the SpecificationsViewModel class.
        /// </summary>
        public BatchSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessPricing);

            #region message registration

            Messenger.Default.Register<CellValueChangedEventArgs>(
                this,
                Token.RecipeCellChange,
                x =>
                {
                    this.OnCellValueChangingCommandExecute(x);
                });

            Messenger.Default.Register<Tuple<int, bool>>(
                this,
                Token.PlantItemSelected,
                x =>
                {
                    this.UpdateCommandExecute();
                    if (x.Item1 == 0)
                    {
                        this.SelectAllPlants(x.Item2);
                    }
                });


            Messenger.Default.Register<Tuple<int, bool>>(
                this,
                Token.ItemSelected,
                x =>
                {
                    this.UpdateCommandExecute();
                    if (x.Item1 == 0)
                    {
                        this.SelectAllCategories(x.Item2);
                    }
                });

            Messenger.Default.Register<Tuple<int, bool, bool>>(
                this,
                Token.CountryItemSelected,
                x =>
                {
                    this.UpdateCommandExecute();
                    if (x.Item1 == 0)
                    {
                        this.SelectAllCountries(x.Item2, x.Item3);
                    }
                });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<int>(this, Token.ProductionSelected, s =>
            {
                var batch = this.Batches.FirstOrDefault(x => x.Order.PROrderID == s);
                if (batch == null)
                {
                    var localBatch = this.DataManager.GetProductionBatchOrder(s);
                    if (localBatch != null)
                    {
                        this.Batches.Add(localBatch);
                        this.SetBatch(localBatch);
                    }
                }
                else
                {
                    batch = this.DataManager.GetProductionBatchOrder(s);
                    this.SetBatch(batch);
                }
            });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<Sale>(this, Token.ProductionSelected, s =>
            {
                var batch = this.Batches.FirstOrDefault(x => x.Order.PROrderID == s.SaleID);
                if (batch == null)
                {
                    var localBatch = this.DataManager.GetProductionBatchOrder(s.SaleID);
                    if (localBatch != null)
                    {
                        this.Batches.Add(localBatch);
                        this.SetBatch(localBatch);
                    }
                }
                else
                {
                    batch = this.DataManager.GetProductionBatchOrder(s.SaleID);
                    this.SetBatch(batch);
                }
            });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    this.ControlChange(x);
                });

            Messenger.Default.Register<Tuple<bool, int?>>(this, Token.UpdateProductSelections, tuple => this.UpdateSelections(tuple.Item1, tuple.Item2));

            #endregion

            #region command registration

            this.RemoveProductCommand = new RelayCommand(() =>
            {
                if (this.SelectedIngredient != null)
                {
                    if (!this.AuthorisationsManager.AllowUserToRemoveProductFromProductionOrder)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                        return;
                    }

                    if (this.SelectedBatch != null && this.SelectedBatch.Order.PROrderID > 0)
                    {
                        if (!this.DataManager.DoesProductionProductLineContainTransactions(this.SelectedBatch.Order.PROrderID, this.SelectedIngredient.INMasterID))
                        {
                            NouvemMessageBox.Show(string.Format(Message.RemoveProductPrompt, this.GetProductName(this.SelectedIngredient.INMasterID)), NouvemMessageBoxButtons.YesNo);
                            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                            {
                                return;
                            }

                            this.Ingredients.Remove(this.SelectedIngredient);
                            this.UpdateProduction();
                            this.MoveLastEdit();
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                        }
                    }
                    else
                    {
                        NouvemMessageBox.Show(string.Format(Message.RemoveProductPrompt, this.GetProductName(this.SelectedIngredient.INMasterID)), NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }

                        this.Ingredients.Remove(this.SelectedIngredient);
                    }
                }
            });

            //this.OnCellValueChangingCommand =
            //    new RelayCommand<CellValueChangedEventArgs>(this.OnCellValueChangingCommandExecute);

            this.OnClosingCommand = new RelayCommand(() =>
                {
                    ApplicationSettings.NouProrderTypeID = this.SelectedProductionType?.NouPROrderTypeID;
                });

            #endregion

            #region instantiation

            this.Products = new ObservableCollection<ProductSelection>();

            #endregion

            this.GetProductionTypes();
            this.GetPlants();
            this.GetCountries();
            this.GetCategories();
            this.GetProducts();
            this.GetSpecifications();
            this.ToDate = DateTime.Today;
            this.FromDate = ApplicationSettings.BatchSetUpFromDate < DateTime.Today.AddYears(-100)
                ? DateTime.Today
                : ApplicationSettings.BatchSetUpFromDate;
            this.ShowAllOrders = ApplicationSettings.BatchSetupShowAllOrders;
            this.Refresh();
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.Add);
            this.GetDocNumbering();
            this.GetIssueMethods();
            this.GetUOMs();
            this.OrderMethods = this.DataManager.GetOrderMethods()
                .Where(x => x.Name.CompareIgnoringCase(Strings.Quantity)
                            || x.Name.CompareIgnoringCase(Strings.Weight)).ToList();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the auto completion of a mix flag.
        /// </summary>
        public bool AutoCompleteMix
        {
            get
            {
                return this.autoCompleteMix;
            }

            set
            {
                this.SetMode(value, this.autoCompleteMix);
                this.autoCompleteMix = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the mix completion pop up notes.
        /// </summary>
        public string MixNotes
        {
            get
            {
                return this.mixNotes;
            }

            set
            {
                this.SetMode(value, this.mixNotes);
                this.mixNotes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public SaleDetail SelectedIngredient
        {
            get
            {
                return this.selectedIngredient;
            }

            set
            {
                this.selectedIngredient = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether qa is required.
        /// </summary>
        public ObservableCollection<SaleDetail> Ingredients
        {
            get
            {
                return this.ingredients;
            }

            set
            {
                this.ingredients = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public bool ShowAllOrders
        {
            get
            {
                return this.showAllOrders;
            }

            set
            {
                this.showAllOrders = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded)
                {
                    this.GetBatches();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime FromDate
        {
            get
            {
                return this.fromDate;
            }

            set
            {
                this.fromDate = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.GetBatches();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime ToDate
        {
            get
            {
                return this.toDate;
            }

            set
            {
                this.toDate = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.GetBatches();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in find mode.
        /// </summary>
        public bool SearchMode
        {
            get
            {
                return this.searchMode;
            }

            set
            {
                this.searchMode = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    // Find sale order mode, so set focus to inventory search combo box.
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedInventoryControl);
                }
            }
        }

        /// <summary>
        /// Gets or sets the batches.
        /// </summary>
        public ObservableCollection<ProductionData> Batches
        {
            get
            {
                return this.batches;
            }

            set
            {
                this.batches = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        public ICommand RemoveProductCommand { get; set; }
        public ICommand OnClosingCommand { get; set; }

        public ICommand OnCellValueChangingCommand { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Updates all the selected nodes, sub nodes 'add to list' property'.
        /// </summary>
        /// <param name="selected">The add to list selection value.</param>
        /// <param name="nodeID">The node selected.</param>
        public void UpdateSelections(bool selected, int? nodeID)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            // Set the nodes 'sub nodes' add to list property to that of the selected value.
            foreach (var product in this.Products.Where(x => x.ParentID == nodeID))
            {
                product.AddToList = selected;
            }

            this.SetSpecificationProducts();
        }

        #endregion

        #endregion

        #region protected override

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.SetBatch(this.DataManager.GetProductionBatchOrderLastEdit());
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedBatch == null || this.SelectedBatch.Order.PROrderID == 0)
            {
                this.SetBatch(this.DataManager.GetProductionBatchOrderFirstLast(true));
                return;
            }

            var localSelectedBatch = this.DataManager.GetProductionBatchOrder(this.SelectedBatch.Order.PROrderID - 1);
            if (localSelectedBatch != null)
            {
                this.SetBatch(localSelectedBatch);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedBatch == null || this.SelectedBatch.Order.PROrderID == 0)
            {
                this.SetBatch(this.DataManager.GetProductionBatchOrderFirstLast(false));
                return;
            }

            var localSelectedBatch = this.DataManager.GetProductionBatchOrder(this.SelectedBatch.Order.PROrderID + 1);
            if (localSelectedBatch != null)
            {
                this.SetBatch(localSelectedBatch);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.SetBatch(this.DataManager.GetProductionBatchOrderFirstLast(true));
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.SetBatch(this.DataManager.GetProductionBatchOrderFirstLast(false));
        }

        #endregion

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedBatch == null || this.SelectedBatch.Order.PROrderID == 0)
            {
                //SystemMessage.Write(MessageType.Issue, Message.InvalidDispatchDocket);
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(103, this.SelectedBatch.Order.PROrderID.ToString(), !preview);
            return;
        }

        /// <summary>
        /// Gets the production types.
        /// </summary>
        protected override void GetProductionTypes()
        {
            var types = this.DataManager.GetPrOrderTypes();
            if (!types.Any())
            {
                this.ProductionTypes = new List<NouPROrderType> { new NouPROrderType { NouPROrderTypeID = 1, Description = Strings.Standard } };
            }
            else
            {
                this.ProductionTypes = types;
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                //if (ApplicationSettings.NouProrderTypeID.HasValue)
                //{
                //    this.SelectedProductionType = this.ProductionTypes.FirstOrDefault(x => x.NouPROrderTypeID == ApplicationSettings.NouProrderTypeID);
                //    return;
                //}

                this.SelectedProductionType = this.ProductionTypes.First();
            }));
        }

        /// <summary>
        /// Creates a production batch.
        /// </summary>
        protected override void AddProduction()
        {
            if (this.ProcessingData)
            {
                return;
            }


            #region validation

            if (this.SelectedProductionType != null
                && this.SelectedProductionType.Description.CompareIgnoringCase(Strings.Receipe)
                && (this.SaleDetails == null || !this.SaleDetails.Any()))
            {
                SystemMessage.Write(MessageType.Issue, Message.EmptyRecipeProduction);
                if (ApplicationSettings.TouchScreenMode)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.EmptyRecipeProduction, touchScreen:true);
                    }));
                }

                return;
            }

            #endregion

            this.ProcessingData = true;
            var batchId = 0;
            try
            {
                ProgressBar.SetUp(0, 3);
                ProgressBar.Run();
                System.Threading.Thread.Sleep(100);
                // TODO need to implement production order set ups.
                this.GetDocNumbering();

                var data = new ProductionData();

                this.Log.LogDebug(this.GetType(), "Creating new batch: Add production");
                data.BatchNumber =
                this.BatchManager.GenerateBatchNumber(false, this.BatchReference, referenceOnly:true);
                int? specId = null;
                if (this.SelectedSpecification != null && this.SelectedSpecification.Specification.PRSpecID > 0)
                {
                    specId = this.SelectedSpecification.Specification.PRSpecID;
                }

                decimal? recipeWgt = null;
                if (this.SaleDetails != null)
                {
                    var headerProduct = this.SaleDetails.FirstOrDefault();
                    if (headerProduct != null)
                    {
                        if (!headerProduct.QuantityOrdered.IsNullOrZero())
                        {
                            recipeWgt = headerProduct.QuantityOrdered;
                        }
                        else
                        {
                            recipeWgt = headerProduct.WeightOrdered;
                        }
                    }
                }

                if (this.SelectedProductionType.Description.CompareIgnoringCase(Strings.Receipe) && recipeWgt <= 0)
                {
                    NouvemMessageBox.Show(string.Format(Message.ZeroQuantityRecipeStop, recipeWgt));
                    return;
                }

                data.Order = new PROrder
                {
                    DocumentNumberingID = this.SelectedDocNumbering.DocumentNumberingID,
                    Number = this.NextNumber,
                    NouDocStatusID = this.SelectedDocStatus.NouDocStatusID,
                    ScheduledDate = this.ScheduledDate,
                    Reference = this.BatchReference,
                    CreationDate = DateTime.Now,
                    EditDate = DateTime.Now,
                    PRSpecID = specId,
                    MixNote = this.mixNotes,
                    BPMasterID = this.SelectedCustomer != null ? this.SelectedCustomer.BPMasterID : (int?)null,
                    BPContactID = this.SelectedContact != null ? this.SelectedContact.Details.BPContactID : (int?)null,
                    UserID = NouvemGlobal.UserId.ToInt(),
                    DeviceID = NouvemGlobal.DeviceId.ToInt(),
                    BatchNumberID = data.BatchNumber.BatchNumberID,
                    TypicalMixQty = this.typicalMixQty,
                    MultipleReceipts = this.multipleReceipts,
                    ReworkNonAdjust = this.reworkStockNonAdjust,
                    AutoCompleteMix = this.autoCompleteMix,
                    BatchWeight = recipeWgt
                };

                if (this.SelectedProductionType != null &&
                    this.SelectedProductionType.Description.CompareIgnoringCase(Constant.Receipe)
                    && this.SaleDetails != null && this.SaleDetails.Any())
                {
                    data.Order.INMasterID = this.SaleDetails.First().INMasterID;
                }

                batchId = this.DataManager.AddProductionOrder(data);
                ProgressBar.Run();
                System.Threading.Thread.Sleep(50);
                if (batchId > 0)
                {
                    Messenger.Default.Send(Token.Message, Token.ProductionBatchCreated);
                    this.DataManager.SetDocumentNumber(this.SelectedDocNumbering.DocumentNumberingID);
                    this.GetBatches();
                    this.EntitySelectionChange = true;
                    this.SelectedBatch = this.Batches.FirstOrDefault(x => x.Order.PROrderID == batchId);
                    this.EntitySelectionChange = false;
                    this.UpdateProduction();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.ProductionOrderNotCreated);
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.ProcessingData = false;

                if (ApplicationSettings.TouchScreenMode)
                {
                    this.MoveBackCommandExecute();
                    this.SelectedBatch.Order.PROrderID = batchId;
                    this.Locator.TouchscreenProductionOrders.SelectedOrder = this.SelectedBatch;
                    this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();
                }
            }
        }

        /// <summary>
        /// Moves back to main screen.
        /// </summary>
        protected override void MoveBackCommandExecute()
        {
            if (ApplicationSettings.ScannerMode)
            {
                ViewModelLocator.ClearBatchSetUp();
                Messenger.Default.Send(Token.Message, Token.CloseScannerBatchSetUp);
                return;
            }

            this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
        }

        /// <summary>
        /// Handler for a doc status change.
        /// </summary>
        protected override void HandleDocStatusChange()
        {
            if (this.SelectedBatch == null || this.SelectedBatch.Order.PROrderID == 0)
            {
                this.SetControlMode(ControlMode.Add);
                return;
            }

            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Adds a new spec.
        /// </summary>
        protected override void UpdateProduction()
        {
            try
            {
                // get the id's of the products to be added
                var productsToAdd = new HashSet<int>();
                foreach (var specificationProduct in this.specificationProducts)
                {
                    productsToAdd.Add(-specificationProduct.NodeID.ToInt());
                }

                #region validation

                if (this.selectedBatch == null || this.selectedBatch.Order.Reference == Strings.NoneSelected)
                {
                    return;
                }

                #endregion

                if (this.SelectedSpecification == null)
                {
                    this.SelectedSpecification = new ProductionData();
                }

                this.SelectedSpecification.Order = this.SelectedBatch.Order;
                foreach (var i in productsToAdd)
                {
                    this.SelectedSpecification.Outputs.Add(new PROrderOutput { INMasterID = i, PROrderID = this.SelectedBatch.Order.PROrderID });
                }

                this.SelectedSpecification.CarcassTypes.Clear();
                foreach (var category in this.Categories.Where(x => x.IsSelected && x.CategoryID > 0))
                {
                    this.SelectedSpecification.CarcassTypes.Add(new PRInputCarcassTypeOrder { CarcassTypeID = category.CategoryID, PROrderID = this.SelectedBatch.Order.PROrderID });
                }

                this.SelectedSpecification.Countries.Clear();
                foreach (var country in this.CountriesBornIn.Where(x => x.IsSelected && x.CountryID > 0))
                {
                    this.SelectedSpecification.Countries.Add(new PRInputCountryOrder { CountryID = country.CountryID, BornIn = true, PROrderID = this.SelectedBatch.Order.PROrderID });
                }

                foreach (var country in this.CountriesRearedIn.Where(x => x.IsSelected && x.CountryID > 0))
                {
                    this.SelectedSpecification.Countries.Add(new PRInputCountryOrder { CountryID = country.CountryID, BornIn = false, PROrderID = this.SelectedBatch.Order.PROrderID });
                }

                this.SelectedSpecification.Plants.Clear();
                foreach (var plant in this.Plants.Where(x => x.IsSelected && x.PlantID > 0))
                {
                    this.SelectedSpecification.Plants.Add(new PRInputPlantOrder { PlantID = plant.PlantID, PROrderID = this.SelectedBatch.Order.PROrderID });
                }

                if ((this.QualityAssured && this.NonQualityAssured) || (!this.QualityAssured && !this.NonQualityAssured))
                {
                    this.SelectedSpecification.Order.QA = null;
                }
                else
                {
                    this.SelectedSpecification.Order.QA = this.QualityAssured;
                }

                if ((this.AllowOverAge && this.AllowUnderAge) || (!this.AllowOverAge && !this.AllowUnderAge))
                {
                    this.SelectedSpecification.Order.OverAge = null;
                }
                else
                {
                    this.SelectedSpecification.Order.OverAge = this.AllowOverAge;
                }

                this.SelectedSpecification.Order.NouPROrderTypeID = this.SelectedProductionType.NouPROrderTypeID;
                this.SelectedSpecification.Order.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;

                decimal? recipeWgt = null;
                foreach (var detail in this.SaleDetails)
                {
                    if (detail.IsHeaderProduct.IsNull())
                    {
                        detail.IsHeaderProduct = detail.InternalSaleDetails != null && detail.InternalSaleDetails.Any();
                    }

                    if (detail.IsHeaderProduct.IsTrue())
                    {
                        var localProduct = this.DataManager.GetRecipeOrderMethodByProductId(detail.INMasterID);
                        if (localProduct != null && localProduct.NouOrderMethodID == 1)
                        {
                            detail.QuantityOrdered = detail.WeightOrdered;
                            detail.WeightOrdered = 0;
                        }

                        if (!detail.QuantityOrdered.IsNullOrZero())
                        {
                            recipeWgt = detail.QuantityOrdered;
                        }
                        else
                        {
                            recipeWgt = detail.WeightOrdered;
                        }
                    }

                    this.SelectedSpecification.Order.INMasterID = detail.INMasterID;
                }

                if (this.SelectedProductionType.Description.CompareIgnoringCase(Strings.Receipe) && recipeWgt <= 0)
                {
                    NouvemMessageBox.Show(string.Format(Message.ZeroQuantityRecipeStop, recipeWgt));
                    return;
                }

                foreach (var ingredient in this.Ingredients)
                {
                    //ingredient.QuantityOrdered = ingredient.Qty;
                    var localuom = this.NouUOMs.FirstOrDefault(x => x.UOMMasterID == ingredient.NouUOMID);
                    if (localuom != null)
                    {
                        var localOrderMethodId = localuom.NouOrderMethodID;
                        var localOrderMethod =
                            this.OrderMethods.FirstOrDefault(x => x.NouOrderMethodID == localOrderMethodId);
                        if (localOrderMethod != null &&
                            localOrderMethod.Name.CompareIgnoringCase(Strings.Weight))
                        {
                            ingredient.WeightOrdered = ingredient.QuantityOrdered;
                            ingredient.QuantityOrdered = 0;
                        }
                    }
                }

                this.SelectedSpecification.Order.ScheduledDate = this.ScheduledDate;
                this.SelectedSpecification.Order.BatchWeight = recipeWgt;
                this.SelectedSpecification.ProductionDetails = this.Ingredients.Union(this.SaleDetails).ToList();
                if (this.DataManager.UpdateOrderSpecification(this.SelectedSpecification))
                {
                    ProgressBar.Run();
                    System.Threading.Thread.Sleep(50);
                    SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
                    this.Refresh();
                    this.SetControlMode(ControlMode.OK);

                    ProgressBar.Run();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
                }

                this.SetControlMode(ControlMode.Add);
            }
            finally
            {
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Override, to set the focus to here.
        /// </summary>
        private void OnCellValueChangingCommandExecute(CellValueChangedEventArgs e)
        {
            this.Log.LogInfo(this.GetType(), "OnCellValueChangingCommandExecute");

            #region validation

            if (e == null || e.Cell == null || e.Value == null || string.IsNullOrEmpty(e.Cell.Property))
            {
                return;
            }

            #endregion

            this.HandleRecipe(e.Cell.Property, e.Value);
        }

        /// <summary>
        /// Handles a recipe selection.
        /// </summary>
        /// <param name="property"></param>
        /// <param name="value"></param>
        private void HandleRecipe(string property, object value)
        {
            try
            {
                if (property.Equals("INMasterID"))
                {
                    var inMasterId = (int)value;
                    var localIngredients = this.DataManager.GetRecipeByProductId(inMasterId);

                    if (localIngredients != null && localIngredients.SaleDetails != null)
                    {
                        this.AutoCompleteMix = localIngredients.AutoCompleteMix.ToBool();
                        this.MixNotes = localIngredients.TechnicalNotes;
                        this.multipleReceipts = localIngredients.MultipleReceipts;
                        this.reworkStockNonAdjust = localIngredients.ReworkStockNonAdjust;
                        if (this.SelectedSaleDetail != null)
                        {
                            this.typicalMixQty = localIngredients.TypicalMixQty;
                            var receipePriceListId = localIngredients.PriceListIDValue;

                            if (receipePriceListId.HasValue)
                            {
                                NouvemGlobal.ReceipePriceList =
                                    this.PriceLists.FirstOrDefault(x => x.PriceListID == receipePriceListId);
                                this.SelectedSaleDetail.PriceListID = receipePriceListId.ToInt();
                            }
                            else
                            {
                                NouvemGlobal.ReceipePriceList = null;
                            }

                            this.SelectedSaleDetail.BaseQuantity = localIngredients.TypicalBatchSize;
                            this.SelectedSaleDetail.RunningBaseQuantity = localIngredients.TypicalBatchSize;
                            this.SelectedSaleDetail.WeightOrdered = localIngredients.TypicalBatchSize;
                            this.SelectedSaleDetail.Mixes =
                                this.SelectedSaleDetail.WeightOrdered.ToDouble().ToMixCount(this.typicalMixQty.ToDouble());

                            this.SelectedSaleDetail.InternalSaleDetails = localIngredients.SaleDetails;
                        }

                        this.Ingredients.Clear();
                        foreach (var localIngredientsSaleDetail in localIngredients.SaleDetails)
                        {
                            localIngredientsSaleDetail.Qty = localIngredientsSaleDetail.QuantityOrdered;
                            localIngredientsSaleDetail.Wgt = localIngredientsSaleDetail.WeightOrdered;
                            this.Ingredients.Add(localIngredientsSaleDetail);
                        }
                    }
                    else
                    {
                        this.Ingredients.Clear();
                    }
                }

                if (this.SelectedSaleDetail != null && this.Ingredients.Any() && property.Equals("WeightOrdered"))
                {
                    if (this.SelectedSaleDetail.IsHeaderProduct.IsTrue() && this.typicalMixQty == null)
                    {
                        var localIngredients = this.DataManager.GetRecipeByProductId(this.SelectedSaleDetail.INMasterID);
                        if (localIngredients != null)
                        {
                            this.typicalMixQty = localIngredients.TypicalMixQty;
                        }
                    }

                    var baseQty = this.SelectedSaleDetail.RunningBaseQuantity;
                    var localQty = value.ToString();
                    var qty = localQty.ToDecimal();

                    this.SelectedSaleDetail.Mixes =
                        qty.ToMixCount(this.typicalMixQty.ToDecimal());

                    var percentage = Math.Round(((qty / baseQty) * 100).ToDecimal(), 2);
                    foreach (var ingredient in this.Ingredients)
                    {
                        ingredient.QuantityOrdered = Math.Round((ingredient.Qty.ToDecimal() / 100) * percentage, 2);
                        ingredient.WeightOrdered = Math.Round((ingredient.Wgt.ToDecimal() / 100) * percentage, 2);

                        // regenerate the tolerance uom's
                        ingredient.PlusTolerance = ingredient.PlusTolerance;
                        ingredient.MinusTolerance = ingredient.MinusTolerance;
                    }

                    if (this.SelectedBatch != null)
                    {
                        this.SelectedBatch.Order.ActualBatchSize = qty;
                    }
                }

                if (this.SelectedSaleDetail != null && this.Ingredients.Any() && property.Equals("QuantityOrdered"))
                {
                    var baseQty = this.SelectedSaleDetail.RunningBaseQuantity;
                    var localQty = value.ToString();
                    var qty = localQty.ToDecimal();

                    this.SelectedSaleDetail.Mixes =
                        qty.ToMixCount(this.typicalMixQty.ToDecimal());

                    var percentage = Math.Round(((qty / baseQty) * 100).ToDecimal(), 2);
                    foreach (var ingredient in this.Ingredients)
                    {
                        ingredient.QuantityOrdered = Math.Round((ingredient.Qty.ToDecimal() / 100) * percentage, 0);
                        ingredient.WeightOrdered = Math.Round((ingredient.Wgt.ToDecimal() / 100) * percentage, 2);

                        // regenerate the tolerance uom's
                        ingredient.PlusTolerance = ingredient.PlusTolerance;
                        ingredient.MinusTolerance = ingredient.MinusTolerance;
                    }

                    if (this.SelectedBatch != null)
                    {
                        this.SelectedBatch.Order.ActualBatchSize = qty;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogDebug(this.GetType(), ex.Message);
            }

            if (this.SelectedBatch == null || this.SelectedBatch.Order.PROrderID == 0)
            {
                this.SetControlMode(ControlMode.Add);
                return;
            }

            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Handles a sale detail selection.
        /// </summary>
        protected override void HandleSelectedSaleDetail()
        {
            if (this.SelectedSaleDetail == null || this.SelectedSaleDetail.INMasterID == 0)
            {
                this.Ingredients.Clear();
                return;
            }

            if (this.SelectedSaleDetail.InternalSaleDetails != null)
            {
                this.Ingredients.Clear();
                foreach (var internalSaleDetail in this.SelectedSaleDetail.InternalSaleDetails)
                {
                    this.Ingredients.Add(internalSaleDetail);
                }
            }

            if (this.SelectedBatch != null)
            {
                this.SelectedSaleDetail.QuantityOrdered = this.SelectedBatch.Order.ActualBatchSize;
            }
        }

        /// <summary>
        /// Handles a touchscreen recipe selection.
        /// </summary>
        protected override void HandleSelectedRecipeItem()
        {
            if (this.SelectedRecipeItem == null)
            {
                return;
            }

            this.SelectedSaleDetail = new SaleDetail { INMasterID = this.SelectedRecipeItem.Master.INMasterID, IsHeaderProduct = true };
            if (this.SaleDetails == null)
            {
                this.SaleDetails = new ObservableCollection<SaleDetail>();
            }
           
            this.SaleDetails.Clear();
            this.SaleDetails.Add(this.SelectedSaleDetail);

            this.HandleRecipe("INMasterID", this.SelectedRecipeItem.Master.INMasterID);
            this.RecipeBatchQty = this.SelectedSaleDetail.BaseQuantity.ToDecimal();
        }

        /// <summary>
        /// Handles a touchscreen recipe selection.
        /// </summary>
        protected override void HandleSelectedRecipeQty()
        {
            if (this.SelectedRecipeItem == null)
            {
                return;
            }

            if (this.SelectedSaleDetail == null)
            {
                this.SelectedSaleDetail = new SaleDetail { INMasterID = this.SelectedRecipeItem.Master.INMasterID, IsHeaderProduct = true };
                this.SaleDetails.Add(this.SelectedSaleDetail);
            }

            this.SelectedSaleDetail.WeightOrdered = this.RecipeBatchQty;
            this.HandleRecipe("WeightOrdered", this.RecipeBatchQty);
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                // Moving into add new partner mode, so clear the partner fields.
                this.Refresh();
                this.SetControlMode(ControlMode.Add);
                this.SearchMode = false;
                Messenger.Default.Send(Token.Message, Token.FocusToDataUpdate);
            }

            if (this.CurrentMode == ControlMode.Find)
            {
                // Moving into find partner mode, so clear the partner fields.
                this.Refresh();
                this.SetControlMode(ControlMode.Find);
                this.SearchMode = true;
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedInventoryControl);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddProduction();
                    break;

                case ControlMode.Find:
                    Messenger.Default.Send(ViewType.BatchEdit);
                    this.Locator.BatchEdit.SearchMode = true;
                    break;

                case ControlMode.Update:
                    this.UpdateProduction();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }
      
        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the application plants.
        /// </summary>
        protected override void GetPlants()
        {
            var localPlants = this.DataManager.GetAllPlants();
            localPlants.Insert(0, new Model.DataLayer.Plant());

            this.Plants = new ObservableCollection<Nouvem.Model.BusinessObject.Plant>(
                (from x in localPlants
                 select new Nouvem.Model.BusinessObject.Plant
                 {
                     PlantID = x.PlantID,
                     CountryID = x.CountryID,
                     Code = x.Code,
                     SiteName = x.SiteName
                 }).ToList());
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        protected override void GetCategories()
        {
            var localCategories = (from cat in this.DataManager.GetCategories()
                select new Model.BusinessObject.Category
                {
                    CategoryID = cat.CategoryID,
                    Name = cat.Name,
                    Description = cat.Description,
                    CatType = cat.CatType
                }).ToList();

            localCategories.Insert(0, new Category());
            this.Categories = new ObservableCollection<Model.BusinessObject.Category>(localCategories);
        }

        /// <summary>
        /// Gets the application countries.
        /// </summary>
        protected override void GetCountries()
        {
            var localCountries = this.DataManager.GetCountryMaster();
            localCountries.Insert(0, new Country());
            this.CountriesBornIn = new ObservableCollection<NouCountry>(
                (from x in localCountries
                 select new NouCountry
                 {
                     CountryID = x.CountryID,
                     Name = x.Name,
                     Code = x.Code,
                     BornIn = true
                 }).ToList());

            this.CountriesRearedIn = new ObservableCollection<NouCountry>(
                (from x in localCountries
                 select new NouCountry
                 {
                     CountryID = x.CountryID,
                     Name = x.Name,
                     Code = x.Code
                 }).ToList());
        }

        /// <summary>
        /// Selects/deselects all plants.
        /// </summary>
        /// <param name="select">The select flag.</param>
        private void SelectAllPlants(bool select)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            foreach (var plant in this.Plants.Where(x => x.PlantID > 0))
            {
                plant.IsSelected = select;
            }
        }

        /// <summary>
        /// Selects/deselects all categories.
        /// </summary>
        /// <param name="select">The select flag.</param>
        private void SelectAllCategories(bool select)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            foreach (var category in this.Categories.Where(x => x.CategoryID > 0))
            {
                category.IsSelected = select;
            }
        }

        /// <summary>
        /// Selects/deselects all countries.
        /// </summary>
        /// <param name="select">The select flag.</param>
        private void SelectAllCountries(bool select, bool bornIn)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            if (bornIn)
            {
                foreach (var country in this.CountriesBornIn.Where(x => x.CountryID > 0))
                {
                    country.IsSelected = select;
                }
            }
            else
            {
                foreach (var country in this.CountriesRearedIn.Where(x => x.CountryID > 0))
                {
                    country.IsSelected = select;
                }
            }
        }

        /// <summary>
        /// Handles a selected spec.
        /// </summary>
        protected override void HandleSelectedSpecification()
        {
            this.EntitySelectionChange = true;
            this.SpecificationProducts.Clear();
            var specProducts = this.SelectedSpecification.OutputsSpec.Select(x => (int?)x.INMasterID).ToList();
   
            foreach (var productSelection in this.Products)
            {
                //productSelection.AddToList = specProducts.Contains(productSelection.NodeID);
                if (specProducts.Contains(Math.Abs(productSelection.NodeID.ToInt())))
                {
                    productSelection.AddToList = true;
                }
                else
                {
                    productSelection.AddToList = false;
                }
            }

            var specCategories = this.SelectedSpecification.CarcassTypesSpec.Select(x => (int?)x.CarcassTypeID).ToList();
            foreach (var categorySelection in this.Categories)
            {
                categorySelection.IsSelected = specCategories.Contains(categorySelection.CategoryID);
            }

            var specCountriesBornIn = this.SelectedSpecification.CountriesSpec.Where(c => c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesBornIn)
            {
                country.IsSelected = specCountriesBornIn.Contains(country.CountryID);
            }

            var specCountriesRearedIn = this.SelectedSpecification.CountriesSpec.Where(c => !c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesRearedIn)
            {
                country.IsSelected = specCountriesRearedIn.Contains(country.CountryID);
            }

            var specPlants = this.SelectedSpecification.PlantsSpec.Select(x => (int?)x.PlantID).ToList();
            foreach (var plant in this.Plants)
            {
                plant.IsSelected = specPlants.Contains(plant.PlantID);
            }

            if (!this.SelectedSpecification.Specification.QA.HasValue)
            {
                this.QualityAssured = true;
                this.NonQualityAssured = true;
            }
            else
            {
                this.QualityAssured = this.SelectedSpecification.Specification.QA.ToBool();
                this.NonQualityAssured = !this.QualityAssured;
            }

            if (!this.SelectedSpecification.Specification.OverAge.HasValue)
            {
                this.AllowOverAge = true;
                this.AllowUnderAge = true;
            }
            else
            {
                this.AllowOverAge = this.SelectedSpecification.Specification.OverAge.ToBool();
                this.AllowUnderAge = !this.AllowOverAge;
            }

            this.EntitySelectionChange = false;
            this.SetSpecificationProducts();

            if (this.SelectedBatch == null || this.SelectedBatch.Order.PROrderID == 0)
            {
                this.SetControlMode(ControlMode.Add);
            }
            else
            {
                this.SetControlMode(ControlMode.Update);
            }
        }

        protected override void HandleActualBatchValue()
        {
            this.CalculateMargin();
        }

        protected override void CalculateMargin()
        {
            try
            {
                if (this.ActualBatchValue > 0)
                {
                    this.OrderMargin = Math.Round(((this.ActualBatchValue.ToDecimal() - this.Total) / this.ActualBatchValue.ToDecimal() * 100).ToDecimal(), 2);
                    this.OrderValue = this.ActualBatchValue.ToDecimal() - this.Total.ToDecimal();
                }
                else
                {
                    this.OrderMargin = 0;
                    this.OrderValue = 0;
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Handles a selected spec.
        /// </summary>
        protected override void HandleSelectedBatch()
        {
            this.EntitySelectionChange = true;
            if (this.SelectedBatch.Number == 0)
            {
                this.SelectedBatch = this.DataManager.GetProductionBatchOrder(this.SelectedBatch.Order.PROrderID);
            }
          
            this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x =>
                x.NouDocStatusID == this.SelectedBatch.Order.NouDocStatusID);

            var specProducts = this.SelectedBatch.Outputs.Select(x => (int?)x.INMasterID).ToList();
            foreach (var productSelection in this.Products)
            {
                if (specProducts.Contains(Math.Abs(productSelection.NodeID.ToInt())))
                {
                    productSelection.AddToList = true;
                }
                else
                {
                    productSelection.AddToList = false;
                }
            }
          
            this.SelectedSpecification =
                this.Specifications.FirstOrDefault(x => x.Specification.PRSpecID == this.SelectedBatch.Order.PRSpecID);
            if (this.SelectedSpecification == null)
            {
                this.SelectedSpecification = this.Specifications.FirstOrDefault();
            }

            var specCategories = this.SelectedBatch.CarcassTypes.Select(x => (int?)x.CarcassTypeID).ToList();
            foreach (var categorySelection in this.Categories)
            {
                categorySelection.IsSelected = specCategories.Contains(categorySelection.CategoryID);
            }

            var specCountriesBornIn = this.SelectedBatch.Countries.Where(c => c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesBornIn)
            {
                country.IsSelected = specCountriesBornIn.Contains(country.CountryID);
            }

            var specCountriesRearedIn = this.SelectedBatch.Countries.Where(c => !c.BornIn).Select(x => (int?)x.CountryID).ToList();
            foreach (var country in this.CountriesRearedIn)
            {
                country.IsSelected = specCountriesRearedIn.Contains(country.CountryID);
            }

            var specPlants = this.SelectedBatch.Plants.Select(x => (int?)x.PlantID).ToList();
            foreach (var plant in this.Plants)
            {
                plant.IsSelected = specPlants.Contains(plant.PlantID);
            }

            if (!this.SelectedBatch.Order.QA.HasValue)
            {
                this.QualityAssured = true;
                this.NonQualityAssured = true;
            }
            else
            {
                this.QualityAssured = this.SelectedBatch.Order.QA.ToBool();
                this.NonQualityAssured = !this.QualityAssured;
            }

            if (!this.SelectedBatch.Order.OverAge.HasValue)
            {
                this.AllowOverAge = true;
                this.AllowUnderAge = true;
            }
            else
            {
                this.AllowOverAge = this.SelectedBatch.Order.OverAge.ToBool();
                this.AllowUnderAge = !this.AllowOverAge;
            }

            if (this.SelectedBatch.Order.INMasterID.HasValue)
            {
                this.SelectedCustomer =
                    this.Customers.FirstOrDefault(x => x.BPMasterID == this.SelectedBatch.Order.INMasterID);
            }
            else
            {
                this.SelectedCustomer = null;
            }

            this.SelectedProductionType = this.ProductionTypes.FirstOrDefault();
            if (this.SelectedBatch.Order.NouPROrderTypeID.HasValue)
            {
                this.SelectedProductionType = this.ProductionTypes.FirstOrDefault(x =>
                    x.NouPROrderTypeID == this.SelectedBatch.Order.NouPROrderTypeID);
            }

            this.ScheduledDate = this.SelectedBatch.Order.ScheduledDate.ToDate();
            this.BatchReference = this.SelectedBatch.Order.Reference;
            this.MixNotes = this.SelectedBatch.Order.MixNote;
            this.Ingredients.Clear();
            this.SaleDetails.Clear();
            if (this.SelectedBatch.ProductionDetails != null)
            {
                this.Ingredients = new ObservableCollection<SaleDetail>(this.SelectedBatch.ProductionDetails.Where(x => x.IsHeaderProduct == null));
                this.SaleDetails = new ObservableCollection<SaleDetail>(this.SelectedBatch.ProductionDetails.Where(x => x.IsHeaderProduct.HasValue));
            }
           
            //this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault();

            this.EntitySelectionChange = false;
            this.SetSpecificationProducts();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            NouvemGlobal.ReceipePriceList = null;
            ViewModelLocator.ClearBatchSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseBatchSetUpWindow);
        }

        /// <summary>
        /// Gets the application products.
        /// </summary>
        private void GetProducts()
        {
            this.Products.Clear();
            foreach (var product in this.DataManager.GetGroupedProducts().OrderBy(x => x.ProductName))
            {
                this.Products.Add(product);
            }
        }

        /// <summary>
        /// Gets the application specs.
        /// </summary>
        protected override void GetSpecifications()
        {
            this.Specifications.Clear();
            var specs = this.DataManager.GetSpecifications();
            foreach (var productionData in specs)
            {
                this.Specifications.Add(productionData);
            }

            var defaultSpec = new ProductionData { Specification = new PRSpec { Name = Strings.NoneSelected } };
            this.Specifications.Insert(0, defaultSpec);
            this.SelectedSpecification = this.Specifications.First();
        }

        /// <summary>
        /// Gets the application batches.
        /// </summary>
        private void GetBatches()
        {
            this.Batches.Clear();
            var statuses = new List<int> {NouvemGlobal.NouDocStatusActive.NouDocStatusID};
            if (this.ShowAllOrders)
            {
                statuses = new List<int> { NouvemGlobal.NouDocStatusActive.NouDocStatusID, NouvemGlobal.NouDocStatusComplete.NouDocStatusID };
            }

            var localBatches = this.DataManager.GetProductionBatchOrdersForDesktop(statuses, this.FromDate, this.ToDate);
            foreach (var productionData in localBatches)
            {
                this.Batches.Add(productionData);
            }

            var defaultBatch = new ProductionData { Order = new PROrder { Reference = Strings.NoneSelected } };
            this.Batches.Insert(0, defaultBatch);
            this.SelectedBatch = this.Batches.First();
        }

        /// <summary>
        /// Refreshes the ui.
        /// </summary>
        private void Refresh()
        {
            this.ClearForm();
            this.RefreshProducts();
            this.GetBatches();
        }

        /// <summary>
        /// Sets the retrieved batch data.
        /// </summary>
        /// <param name="batch">The retieved batch.</param>
        private void SetBatch(ProductionData batch)
        {
            if (batch == null)
            {
                return;
            }

            this.AutoCompleteMix = batch.Order.AutoCompleteMix.ToBool();
            var localDetails =
                this.DataManager.GetProductionOrderTransactionDataNotSplit(batch.Order.PROrderID);
            if (batch.ProductionType.CompareIgnoringCaseAndWhitespace(Constant.Receipe))
            {
                foreach (var detail in batch.ProductionDetails.Where(x => x.IsHeaderProduct.IsNullOrFalse()))
                {
                    detail.QuantityOrdered = !detail.QuantityOrdered.IsNullOrZero() ? detail.QuantityOrdered : detail.WeightOrdered;
                    detail.Qty = detail.QuantityOrdered;
                    var match = localDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                    if (match == null)
                    {
                        continue;
                    }

                    detail.WeightIntoProduction = match.WeightIntoProduction;
                    detail.QuantityIntoProduction = match.QuantityIntoProduction;
                    detail.InventoryItem = match.InventoryItem;
                }

                foreach (var detail in batch.ProductionDetails.Where(x => x.IsHeaderProduct.IsTrue()))
                {
                    var localDetail =
                        this.DataManager.GetProductionOrderTransactionDataForProduct(batch.Order.PROrderID, detail.INMasterID, NouvemGlobal.TransactionTypeProductionReceiptId);

                    var localProduct = this.DataManager.GetRecipeOrderMethodByProductId(detail.INMasterID);
                    if (localProduct != null && localProduct.NouOrderMethodID == 1)
                    {
                        detail.WeightOrdered = detail.QuantityOrdered;
                        detail.QuantityOrdered = 0;
                    }

                    detail.WeightIntoProduction = localDetail.WeightIntoProduction;
                    detail.QuantityIntoProduction = localDetail.QuantityIntoProduction;
                    detail.InventoryItem = localDetail.InventoryItem;
                    detail.RunningBaseQuantity = detail.QuantityOrdered.IsNullOrZero()
                        ? detail.BaseQuantity
                        : detail.QuantityOrdered;
                }
            }
            else
            {
                foreach (var detail in batch.ProductionDetails)
                {
                    var match = localDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                    if (match == null)
                    {
                        continue;
                    }

                    detail.WeightIntoProduction = match.WeightIntoProduction;
                    detail.QuantityIntoProduction = match.QuantityIntoProduction;
                    detail.InventoryItem = match.InventoryItem;
                }
            }
      
            this.SelectedBatch = batch;

            this.SubTotal = 0;
            this.Tax = 0;
            this.Total = 0;
            this.TypicalBatchValue = 0;
            this.ActualBatchValue = 0;

            foreach (var localDetail in this.Ingredients)
            {
                localDetail.TotalExVAT =
                    Math.Round(localDetail.QuantityIntoProduction.ToDecimal() * localDetail.UnitPrice.ToDecimal(), 2);

                localDetail.TotalIncVAT =
                    Math.Round(localDetail.QuantityIntoProduction.ToDecimal() * localDetail.UnitPrice.ToDecimal(), 2);

                localDetail.VAT = localDetail.TotalIncVAT - localDetail.TotalExVAT;
            }

            foreach (var localDetail in this.SaleDetails)
            {
                localDetail.TotalExVAT =
                    Math.Round(localDetail.QuantityIntoProduction.ToDecimal() * localDetail.UnitPrice.ToDecimal(), 2);

                localDetail.TotalIncVAT =
                    Math.Round(localDetail.QuantityIntoProduction.ToDecimal() * localDetail.UnitPrice.ToDecimal(), 2);

                localDetail.VAT = localDetail.TotalIncVAT - localDetail.TotalExVAT;
            }

            if (this.Ingredients.Any())
            {
                this.SubTotal = this.Ingredients.Sum(x => x.TotalExVAT.ToDecimal());
                this.Total = this.Ingredients.Sum(x => x.TotalIncVAT.ToDecimal());
                this.Tax = this.Total - this.SubTotal;
            }

            var headerProduct = this.SaleDetails.FirstOrDefault();
            if (headerProduct != null)
            {
                this.ActualBatchValue = headerProduct.TotalIncVAT.ToDecimal();
                this.TypicalBatchValue = headerProduct.BaseQuantity.ToDecimal() * headerProduct.UnitPrice;
            }
        }

        /// <summary>
        /// Sets the spec products.
        /// </summary>
        private void SetSpecificationProducts()
        {
            this.SpecificationProducts.Clear();
            var localProducts = this.Products
                .Where(x => x.AddToList && x.IsLeafNode)
                .OrderBy(prod => prod.GroupName).ThenBy(prod => prod.ProductName).ToList();
         
            foreach (var productSelection in localProducts)
            {
                this.SpecificationProducts.Add(productSelection);
            }

            this.EntitySelectionChange = true;
            var specGroups = this.SpecificationProducts.Select(x => x.ParentID).Distinct().ToList();
            var groups = this.Products.Where(x => !x.IsLeafNode && specGroups.Contains(x.NodeID)).ToList();
            foreach (var group in groups)
            {
                var count = this.Products.Count(x => x.IsLeafNode && x.ParentID == group.NodeID);
                var specProductsCount = this.SpecificationProducts.Count(x => x.ParentID == group.NodeID);
                if (count == specProductsCount)
                {
                    group.AddToList = true;
                }
            }

            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Refreshes the product selections.
        /// </summary>
        private void RefreshProducts()
        {
            this.EntitySelectionChange = true;
            foreach (var productSelection in this.Products)
            {
                productSelection.AddToList = false;
            }

            Messenger.Default.Send(Token.Message, Token.CollapseGroups);
            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Clears the form.
        /// </summary>
        private void ClearForm()
        {
            this.SpecName = string.Empty;
            this.EntitySelectionChange = true;
            this.SelectedSpecification = this.Specifications.FirstOrDefault();
            this.SelectedBatch = this.Batches.FirstOrDefault();
            this.typicalMixQty = null;
            this.multipleReceipts = null;
            this.reworkStockNonAdjust = null;
            this.MixNotes = string.Empty;
            foreach (var category in this.Categories)
            {
                category.IsSelected = false;
            }

            foreach (var country in this.CountriesBornIn)
            {
                country.IsSelected = false;
            }

            foreach (var country in this.CountriesRearedIn)
            {
                country.IsSelected = false;
            }

            foreach (var plant in this.Plants)
            {
                plant.IsSelected = false;
            }

            this.QualityAssured = false;
            this.NonQualityAssured = false;
            this.AllowOverAge = false;
            this.AllowUnderAge = false;
            this.ScheduledDate = DateTime.Today;
            if (this.SaleDetails != null)
            {
                this.SaleDetails.Clear();
            }

            if (this.Ingredients != null)
            {
                this.Ingredients.Clear();
            }

            this.BatchReference =
                this.DataManager.GetProductionBatchReference(NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.UserId.ToInt(), 0, this.ScheduledDate);
            this.SelectedProductionType = this.ProductionTypes.FirstOrDefault();
            this.EntitySelectionChange = false;
            this.SpecificationProducts.Clear();
        }

        #endregion
    }
}

