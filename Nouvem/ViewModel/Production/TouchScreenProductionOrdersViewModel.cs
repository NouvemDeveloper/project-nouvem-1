﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenOrdersViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;

namespace Nouvem.ViewModel.Production
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class TouchscreenProductionOrdersViewModel : NouvemViewModelBase
    {
        #region field

        // The orders to display.
        private ObservableCollection<ProductionData> orders = new ObservableCollection<ProductionData>();

        /// <summary>
        /// The selected order.
        /// </summary>
        private ProductionData selectedOrder;

        /// <summary>
        /// The sort by scheduled date text.
        /// </summary>
        private string schedulesDateSort;

        /// <summary>
        /// The orders filter/search text.
        /// </summary>
        private string ordersSearchText;

        /// <summary>
        /// The sort by route text.
        /// </summary>
        private string routeSort;

        /// <summary>
        /// The sort by route text.
        /// </summary>
        private bool editOrder;

        private bool completeOrder;

        /// <summary>
        /// The sort by order received text.
        /// </summary>
        private string orderReceivedSort;

        /// <summary>
        /// The sort by supplier text.
        /// </summary>
        private string supplierSort;

        /// <summary>
        /// Show the grid group panel flag.
        /// </summary>
        private bool showGroupPanel;

        /// <summary>
        /// The view we navigated from.
        /// </summary>
        private ViewType navigatedFromView;

        /// <summary>
        /// The incoming partner.
        /// </summary>
        private ViewBusinessPartner partner;

        /// <summary>
        /// The date the order was created.
        /// </summary>
        private DateTime? orderReceivedDate;

        /// <summary>
        /// The display date.
        /// </summary>
        private string displayOrderDate;

        /// <summary>
        /// The all partners reference.
        /// </summary>
        private ViewBusinessPartner allPartners = new ViewBusinessPartner { Name = Strings.AllPartners };

        /// <summary>
        /// The all dates reference.
        /// </summary>
        private string allDates = Strings.AllDates;

        /// <summary>
        /// Flag, as to whether we are handling an incoming supplier selection.
        /// </summary>
        private bool handlingIncomingSales;

        /// <summary>
        /// The master module.
        /// </summary>
        private ViewType module;

        // All the orders.
        protected ObservableCollection<ProductionData> allOrders = new ObservableCollection<ProductionData>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenProductionOrdersViewModel "/> class.
        /// </summary>
        public TouchscreenProductionOrdersViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.ProductionOrderSearch, this.HandleOrderSearch);

            Messenger.Default.Register<string>(this, Token.ProductionBatchCreated, s => this.GetAllOrders());

            // Register to assign the incoming supplier sales.
            Messenger.Default.Register<Tuple<ViewBusinessPartner, ViewType>>(this, Token.DisplaySupplierSales, o =>
            {
                this.HandleIncomingSales(o);
                //Task.Factory.StartNew(() => Application.Current.Dispatcher.BeginInvoke(
                //   DispatcherPriority.Background,
                //      new Action(() => this.HandleIncomingSales(o))));
            });

            #endregion

            #region command handler

            // Handle the order selection.
            this.OrderSelectedCommand = new RelayCommand(this.OrderSelectedCommandExecute);

            // Handle the orders load event.
            this.OnOrdersLoadingCommand = new RelayCommand(this.OnOrdersLoadingCommandExecute);

            // Handle the orders unload event.
            this.OnOrdersUnloadedCommand = new RelayCommand(this.OnOrdersUnloadedCommandExecute);

            // Handler to turn the goup panel display on/off.
            this.ShowGroupPanelCommand = new RelayCommand(() => this.ShowGroupPanel = !this.ShowGroupPanel);

            // Scroll the suppliers grid.
            this.ScrollCommand = new RelayCommand<string>(s => Messenger.Default.Send(s, Token.ScrollOrders));

            // Handle the command to sort the grid.
            this.SortByCommand = new RelayCommand<string>(this.SortByCommandExecute);

            // Move back to main ui or suppliers screen.
            this.MoveBackCommand = new RelayCommand(this.MoveBackCommandExecute);

            // Create a new order for the selected supplier.
            this.CreateOrderCommand = new RelayCommand(this.CreateOrderCommandExecute);

            // Refreshes the filters.
            this.RefreshFiltersCommand = new RelayCommand(this.RefreshFiltersCommandExecute);

            // Search for an order.
            this.FindOrderCommand = new RelayCommand(this.FindOrderCommandExecute);

            #endregion
            
            this.RefreshFilters();

            if (ApplicationSettings.ScannerMode)
            {
                this.GetAllOrders();
            }
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the orders.
        /// </summary>
        public ObservableCollection<ProductionData> AllOrders
        {
            get
            {
                return this.allOrders;
            }
        }

        /// <summary>
        /// Gets or sets the orders search/filter text.
        /// </summary>
        public string OrdersSearchText
        {
            get
            {
                return this.ordersSearchText;
            }

            set
            {
                this.ordersSearchText = value;
                this.RaisePropertyChanged();
                this.HandleOrdersSearch();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a selected order is to be edited.
        /// </summary>
        public bool CompleteOrder
        {
            get { return this.completeOrder; }
            set
            {
                this.completeOrder = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a selected order is to be edited.
        /// </summary>
        public bool EditOrder
        {
            get { return this.editOrder;}
            set
            {
                this.editOrder = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the order creation date to search for.
        /// </summary>
        public DateTime? OrderReceivedDate
        {
            get
            {
                return this.orderReceivedDate;
            }

            set
            {
                this.orderReceivedDate = value;
                this.RaisePropertyChanged();
                this.DisplayOrderDate = value.ToDate().ToShortDateString();
                Messenger.Default.Send(false, Token.DisplayCalender);

                if (value.HasValue)
                {
                    var localOrders
                        = this.AllOrders.Where(x => x.CreationDate.Date == value.ToDate().Date);

                    if (this.Partner != this.allPartners)
                    {
                        localOrders = localOrders.Where(x => x.BPCustomer.BPMasterID == this.Partner.BPMasterID);
                    }

                    if (this.DisplayOrderDate != this.allDates)
                    {
                        localOrders = localOrders.Where(x => x.CreationDate.Date == this.OrderReceivedDate.ToDate().Date);
                    }

                    this.Orders = new ObservableCollection<ProductionData>(localOrders);
                }
            }
        }

        /// <summary>
        /// Gets or sets the display order creation date.
        /// </summary>
        public string DisplayOrderDate
        {
            get
            {
                return this.displayOrderDate;
            }

            set
            {
                this.displayOrderDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the incoming partner.
        /// </summary>
        public ViewBusinessPartner Partner
        {
            get
            {
                return this.partner;
            }

            set
            {
                this.partner = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the grid couup panel is displayed.
        /// </summary>
        public bool ShowGroupPanel
        {
            get
            {
                return this.showGroupPanel;
            }

            set
            {
                this.showGroupPanel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the scheduled date sort text.
        /// </summary>
        public string ScheduledDateSort
        {
            get
            {
                return this.schedulesDateSort;
            }

            set
            {
                this.schedulesDateSort = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the route sort text.
        /// </summary>
        public string RouteSort
        {
            get
            {
                return this.routeSort;
            }

            set
            {
                this.routeSort = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the order received sort text.
        /// </summary>
        public string OrderReceivedSort
        {
            get
            {
                return this.orderReceivedSort;
            }

            set
            {
                this.orderReceivedSort = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the supplier sort text.
        /// </summary>
        public string SupplierSort
        {
            get
            {
                return this.supplierSort;
            }

            set
            {
                this.supplierSort = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or set the selected order.
        /// </summary>
        public ProductionData SelectedOrder
        {
            get
            {
                return this.selectedOrder;
            }

            set
            {
                this.selectedOrder = value;
                this.RaisePropertyChanged();

                if (ApplicationSettings.ScannerMode)
                {
                    if (value != null)
                    {
                        this.HandleSelectedOrder();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the orders.
        /// </summary>
        public ObservableCollection<ProductionData> Orders
        {
            get
            {
                return this.orders;
            }

            set
            {
                this.orders = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Gets all the orders by location.
        /// </summary>
        public void GetAllOrdersByLocation(int locationId)
        {
            this.allOrders = new ObservableCollection<ProductionData>(this.DataManager.GetProductionOrdersByLocation(locationId));
        }

        /// <summary>
        /// Create the new batch screen.
        /// </summary>
        public void CreateOrder()
        {
            this.CreateOrderCommandExecute();
        }

        /// <summary>
        /// Finds and sets the order matching the incoming order id.
        /// </summary>
        /// <param name="prOrderid"></param>
        public void SetOrder(int prOrderid)
        {
            this.SelectedOrder = this.AllOrders.FirstOrDefault(x => x.Order.PROrderID == prOrderid);
            this.Log.LogInfo(this.GetType(), $"Set Order: PROrder ID:{prOrderid}, All Orders Count:{this.AllOrders.Count}, Selected Order:{this.SelectedOrder?.OrderID}");
        }

        /// <summary>
        /// Handles the selected customer order.
        /// </summary>
        public void HandleSelectedStockOrder(bool isProduction = false)
        {
            if (this.SelectedOrder == null)
            {
                return;
            }

            this.SelectedOrder.ProductionDetails =
                this.DataManager.GetStockMovementTransactionData(this.SelectedOrder.Order.PROrderID);

            if (this.SelectedOrder.ProductionDetails != null)
            {
                if (this.SelectedOrder.ProductionDetails.Any())
                {
                    this.SelectedOrder.Order = this.SelectedOrder.ProductionDetails.First().PROrder;
                    var localBatch = this.SelectedOrder.ProductionDetails.First().Batch;
                    Messenger.Default.Send(localBatch, Token.BatchNumberSelected);
                }

            }

            if (this.SelectedOrder.Order != null)
            {
                this.Log.LogDebug(this.GetType(), string.Format("HandleSelectedCustomerOrder(): Order Id:{0} selected", this.SelectedOrder.Order.PROrderID));
            }

            if (isProduction)
            {
                Messenger.Default.Send(this.SelectedOrder, Token.ProductionOrderSelected);
            }
            else
            {
                Messenger.Default.Send(this.SelectedOrder, Token.ProductionBatchSelected);
            }

            Messenger.Default.Send(Token.Message, Token.CloseTouchscreenProductionOrders);
            Messenger.Default.Send(true, Token.CloseTouchscreenWarehouseWindow);
            Messenger.Default.Send(false, Token.DisplayWarehouseKeyboard);
        }

        /// <summary>
        /// Only get the orders.
        /// </summary>
        public void GetAllOrdersOnly()
        {
            this.allOrders = new ObservableCollection<ProductionData>(this.DataManager.GetProductionOrders(this.AllOrders));
        }

        /// <summary>
        /// Gets all the orders.
        /// </summary>
        public virtual void GetAllOrders(int? inMasteriD = null, bool useIntakeBatches = false, int? orderBatchDataID = null)
        {
            if (ApplicationSettings.ClearProductionOrdersOnEntry)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.FindOrderCommandExecute();
                }));

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.allOrders = new ObservableCollection<ProductionData>(this.DataManager.GetProductionOrders(this.AllOrders));
                }));
            }
            else
            {
                this.allOrders = new ObservableCollection<ProductionData>(this.DataManager.GetProductionOrders(this.AllOrders, useIntakeBatches));
                //this.Orders = new ObservableCollection<ProductionData>(this.allOrders.OrderByDescending(x => x.CreationDate));
            }

            if (ApplicationSettings.ScannerMode)
            {
                this.OrdersSearchText = string.Empty;
            }
        }

        /// <summary>
        /// Gets the orders.
        /// </summary>
        public void GetOrders(bool multiBatch = false)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.allOrders = new ObservableCollection<ProductionData>(this.DataManager.GetProductionOrders(this.allOrders, multiBatch));
            }));
        }

        /// <summary>
        /// Sets the master module.
        /// </summary>
        /// <param name="view">The master module type.</param>
        public void SetModule(ViewType view)
        {
            this.module = view;
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the order search.
        /// </summary>
        public ICommand FindOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the order selection.
        /// </summary>
        public ICommand OrderSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to refresh the filters.
        /// </summary>
        public ICommand RefreshFiltersCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnOrdersLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event.
        /// </summary>
        public ICommand OnOrdersUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to create an order.
        /// </summary>
        public ICommand CreateOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to show the group panel.
        /// </summary>
        public ICommand ShowGroupPanelCommand { get; private set; }

        /// <summary>
        /// Gets the command to scroll the ui.
        /// </summary>
        public ICommand ScrollCommand { get; set; }

        /// <summary>
        /// Gets the command to sort the grid.
        /// </summary>
        public ICommand SortByCommand { get; private set; }

        /// <summary>
        /// Gets the command tomove back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Move back to the previous module.
        /// </summary>
        protected virtual void MoveBackCommandExecute()
        {
            this.IsFormLoaded = false;
            Messenger.Default.Send(true, Token.CloseTouchscreenProductionOrders);
        }

        /// <summary>
        /// Handles an orders search.
        /// </summary>
        protected virtual void HandleOrdersSearch()
        {
            var value = this.ordersSearchText;
            if (string.IsNullOrEmpty(value))
            {
                if (!this.clearingSalesSearch)
                {
                    if (ApplicationSettings.ClearProductionOrdersOnEntry)
                    {
                        this.Orders.Clear();
                    }
                    else
                    {
                        if (ApplicationSettings.OrderAmountToTake == 0)
                        {
                            ApplicationSettings.OrderAmountToTake = 100;
                        }

                        var localOrders = this.allOrders.OrderByDescending(x => x.CreationDate);
                        if (localOrders.Count() > ApplicationSettings.OrderAmountToTake)
                        {
                            this.Orders = new ObservableCollection<ProductionData>(localOrders.Take(ApplicationSettings.OrderAmountToTake));
                        }
                        else
                        {
                            this.Orders = new ObservableCollection<ProductionData>(localOrders);
                        }
                    }
                }
            }
            else
            {
                if (ApplicationSettings.DontAutoFilterProductionBatchSearch)
                {
                    var localOrders = this.allOrders.Where(x => !string.IsNullOrWhiteSpace(x.Reference)
                                                                && (x.Reference.Length >= 7 && value.Length >= 7
                                                                && (x.Reference.Length >= value.Length)
                                                                && x.Reference.ToLower().Contains(value.ToLower()))
                                                                || x.Reference.CompareIgnoringCase(value)).ToList();
                    if (localOrders.Count == 1)
                    {
                        localOrders.First().INMasterID =
                            this.DataManager.GetProductionOrderProduct(localOrders.First().Order.PROrderID);
                    }

                    this.Orders = new ObservableCollection<ProductionData>(localOrders);

                    if (this.Orders.Count > 1)
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(Message.SimiliarBatchesContactTechnical, touchScreen: true);
                        }));
                    }
                }
                else
                {
                    var localOrders = this.allOrders.Where(x => !string.IsNullOrWhiteSpace(x.Reference) && x.Reference.StartsWithIgnoringCase(value));
                    this.Orders = new ObservableCollection<ProductionData>(localOrders);
                }
            }

            // TODO quick fix for woolleys - need to handle this more gracefully
            if (ApplicationSettings.AlwaysShowButcheryBatch)
            {
                var butcheryBatch = this.allOrders.FirstOrDefault(x => x.Reference.CompareIgnoringCase("Butchery"));
                if (butcheryBatch != null)
                {
                    this.Orders.Insert(0, butcheryBatch);
                }
            }
        }

        protected override void ControlSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Creates an order for the selected supplier.
        /// </summary>
        private void CreateOrderCommandExecute()
        {
            if (!ApplicationSettings.CreatingTouchscreenRecipes)
            {
                this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ProductionBatchSetUp;
            }
            else
            {

                this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.BatchSetUp;
            }
  
            Messenger.Default.Send(true, Token.CloseTouchscreenProductionOrders);
        }

        /// <summary>
        /// Handles the sorting of the grid.
        /// </summary>
        /// <param name="field">The sort parameter.</param>
        private void SortByCommandExecute(string field)
        {
            IEnumerable<ProductionData> localOrders;
            if (field.Equals(Constant.ScheduledDate))
            {
                if (this.ScheduledDateSort.Equals(Constant.Ascending))
                {
                    localOrders = this.allOrders.OrderBy(x => x.ScheduledDate);
                    this.ScheduledDateSort = Constant.Descending;
                }
                else
                {
                    localOrders = this.allOrders.OrderByDescending(x => x.ScheduledDate);
                    this.ScheduledDateSort = Constant.Ascending;
                }

                if (this.Partner != this.allPartners)
                {
                    localOrders = localOrders.Where(x => x.BPCustomer != null && x.BPCustomer.BPMasterID == this.Partner.BPMasterID);
                }

                if (this.DisplayOrderDate != this.allDates)
                {
                    localOrders = localOrders.Where(x => x.ReleaseDate.ToDate().Date == this.OrderReceivedDate.ToDate().Date);
                }

                this.Orders = new ObservableCollection<ProductionData>(localOrders);

                return;
            }

            if (field.Equals(Constant.OrderReceived))
            {
                Messenger.Default.Send(true, Token.DisplayCalender);
                return;
            }

            if (field.Equals(Constant.Customer))
            {
                this.Locator.TouchscreenSuppliers.MasterModule = ViewType.TouchscreenProductionOrders;
                if (!this.PartnersScreenCreated)
                {
                    this.PartnersScreenCreated = true;
                    Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPartners);
                    return;
                }

                Messenger.Default.Send(false, Token.CloseTouchscreenPartnersWindow);
                this.Locator.TouchscreenSuppliers.OnEntry();
                this.Locator.TouchscreenSuppliers.SetPartners(ViewType.ARDispatch);
            }
        }

        /// <summary>
        /// Sends the selected order to the main vm.
        /// </summary>
        private void OrderSelectedCommandExecute()
        {
            this.HandleSelectedOrder();
        }

        public void OnEntry()
        {
            this.OnOrdersLoadingCommandExecute();
        }

        /// <summary>
        /// handle the load event.
        /// </summary>
        private void OnOrdersLoadingCommandExecute()
        {
            this.OrdersSearchText = string.Empty;
            this.EditOrder = false;
            this.CompleteOrder = false;
            this.Locator.FactoryScreen.ModuleName = Strings.Orders;
            if (this.handlingIncomingSales)
            {
                this.handlingIncomingSales = false;
                this.IsFormLoaded = true;
                return;
            }

            /* Dont reset the order for standard production selection/weighing. 
             * (TSBloor for example will always reset) */
            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
            {
                this.SelectedOrder = null;
            }
           
            this.IsFormLoaded = true;
        }

        /// <summary>
        /// handle the load event.
        /// </summary>
        private void OnOrdersUnloadedCommandExecute()
        {
            this.IsFormLoaded = false;
            this.EditOrder = false;
            this.CompleteOrder = false;
        }

        /// <summary>
        /// Refresh the filters.
        /// </summary>
        private void RefreshFiltersCommandExecute()
        {
            this.RefreshFilters();
        }

        #endregion

        #region helper

        private void CompleteSelectedOrder()
        {
            if (this.SelectedOrder == null)
            {
                return;
            }

            try
            {
                NouvemMessageBox.Show("You are about to complete this batch. Do you wish to proceed?", NouvemMessageBoxButtons.YesNo, touchScreen: true, scanner: ApplicationSettings.ScannerMode);
                if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                {
                    return;
                }

                this.DataManager.ChangeOrderStatus(this.SelectedOrder.Order.PROrderID, 3);
                var localOrder =
                    this.allOrders.FirstOrDefault(x => x.Order.PROrderID == this.SelectedOrder.Order.PROrderID);
                if (localOrder != null)
                {
                    this.allOrders.Remove(localOrder);
                }

                this.Orders.Remove(this.SelectedOrder);
                SystemMessage.Write(MessageType.Priority, "Batch completed");
            }
            finally
            {
                this.CompleteOrder = false;
            }
        }

        /// <summary>
        /// Shows/hides the keyboard.
        /// </summary>
        private void FindOrderCommandExecute()
        {
            this.clearingSalesSearch = true;
            this.OrdersSearchText = string.Empty;
            this.clearingSalesSearch = false;
            Messenger.Default.Send(Token.Message, Token.DisplayOrdersKeyboard);
        }

        /// <summary>
        /// Edits an order for the selected supplier.
        /// </summary>
        private void EdidSelectedOrder()
        {
            try
            {
                Messenger.Default.Send(true, Token.CloseTouchscreenProductionOrders);
                this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.ProductionBatchSetUp;
                this.Locator.ProductionBatchSetUp.EditOrder(this.SelectedOrder);
            }
            finally
            {
                this.EditOrder = false;
            }
        }

        /// <summary>
        /// Handles the selected customer order.
        /// </summary>
        public virtual void HandleSelectedOrder()
        {
            this.IsFormLoaded = false;
            if (this.SelectedOrder == null)
            {
                return;
            }

            if (this.CompleteOrder)
            {
                this.CompleteSelectedOrder();
                return;
            }

            if (this.EditOrder)
            {
                this.EdidSelectedOrder();
                return;
            }

            if (this.module == ViewType.StockMovement)
            {
                this.HandleSelectedStockOrder();
                return;
            }

            if (this.module == ViewType.OutOfProduction)
            {
                Messenger.Default.Send(this.selectedOrder.BatchNumber, Token.BatchNumberSelected);

                if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfButchery
                    || ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfInjection)
                {
                    this.HandleBatchOrder();
                    return;
                }

                this.SelectedOrder = this.DataManager.GetProductionBatchOrder(this.SelectedOrder.Order.PROrderID);
                Messenger.Default.Send(this.SelectedOrder, Token.ProductionOrderSelected);
                Messenger.Default.Send(true, Token.CloseTouchscreenProductionOrders);
                return;
            }
            
            if (this.module == ViewType.IntoProduction)
            {
                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.Injection
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.Butchery)
                {
                    this.HandleBatchOrder();
                    return;
                }

                if (ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoJointing
                    || ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoSlicing)
                {
                    this.HandleBatchOrderPreDispatch();
                    return;
                }

                var localScannerMode = ApplicationSettings.ScannerMode;
                ApplicationSettings.ScannerMode = false;
                this.SelectedOrder = this.DataManager.GetProductionBatchOrder(this.SelectedOrder.Order.PROrderID);
                ApplicationSettings.ScannerMode = localScannerMode;
                var localDetails =
                    this.DataManager.GetProductionOrderTransactionData(this.SelectedOrder.Order.PROrderID);
                if (this.SelectedOrder.ProductionType.CompareIgnoringCaseAndWhitespace(Constant.Receipe))
                {
                    // we only want the current receipe mix data
                    localDetails = localDetails.Where(x => x.SplitStockID == this.SelectedOrder.MixCount).ToList();
                }

                foreach (var detail in this.SelectedOrder.ProductionDetails.Where(x => x.IsHeaderProduct.IsNullOrFalse()))
                {
                    var match = localDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                    if (match == null)
                    {
                        continue;
                    }

                    detail.WeightIntoProduction = match.WeightIntoProduction;
                    detail.QuantityIntoProduction = match.QuantityIntoProduction;
                    detail.InventoryItem = match.InventoryItem;
                }

                if (this.SelectedOrder.ProductionDetails == null || !this.SelectedOrder.ProductionDetails.Any())
                {
                    this.SelectedOrder.ProductionDetails = localDetails;
                }

                if (this.SelectedOrder.ProductionDetails != null && this.SelectedOrder.ProductionDetails.Any())
                {
                    var localBatch = this.SelectedOrder.ProductionDetails.First().Batch;
                    Messenger.Default.Send(localBatch, Token.BatchNumberSelected);
                }

                Messenger.Default.Send(this.SelectedOrder, Token.ProductionOrderSelected);
                Messenger.Default.Send(true, Token.CloseTouchscreenProductionOrders);
            }

            if (this.module == ViewType.ARDispatch)
            {
                Messenger.Default.Send(this.selectedOrder.BatchNumber, Token.BatchNumberSelected);
                Messenger.Default.Send(this.SelectedOrder, Token.ProductionOrderSelected);
                var goodsData = this.DataManager.GetProductionOrderBatchData(this.selectedOrder);
                Messenger.Default.Send(goodsData, Token.BatchNumberSelected);
                Messenger.Default.Send(this.selectedOrder.Spec, Token.SpecSelected);
                Messenger.Default.Send(true, Token.HighlightSelectedProduct);

                if (!ApplicationSettings.ScannerMode)
                {
                    //this.Locator.FactoryScreen.MainDisplayViewModel = this.Locator.Touchscreen;
                    Messenger.Default.Send(true, Token.CloseTouchscreenProductionOrders);
                }

                return;
            }
        }

        /// <summary>
        /// Handles the selected customer order.
        /// </summary>
        public void HandleBatchOrder()
        {
            if (this.SelectedOrder == null)
            {
                return;
            }


            this.DataManager.GetBatchOrderData(this.SelectedOrder);

            if (this.SelectedOrder.ProductionDetails != null && this.SelectedOrder.ProductionDetails.Any())
            {
                var localBatch = this.SelectedOrder.ProductionDetails.First().Batch;
                Messenger.Default.Send(localBatch, Token.BatchNumberSelected);
            }

            Messenger.Default.Send(this.SelectedOrder, Token.ProductionOrderSelected);
            Messenger.Default.Send(Token.Message, Token.CloseTouchscreenProductionOrders);
            Messenger.Default.Send(true, Token.CloseTouchscreenWarehouseWindow);
        }

        /// <summary>
        /// Handles the selected customer order.
        /// </summary>
        public void HandleBatchOrderPreDispatch()
        {
            if (this.SelectedOrder == null)
            {
                return;
            }


            this.DataManager.GetBatchOrderDataPreDispatch(this.SelectedOrder);

            if (this.SelectedOrder.ProductionDetails != null && this.SelectedOrder.ProductionDetails.Any())
            {
                var localBatch = this.SelectedOrder.ProductionDetails.First().Batch;
                Messenger.Default.Send(localBatch, Token.BatchNumberSelected);
            }

            Messenger.Default.Send(this.SelectedOrder, Token.ProductionOrderSelected);
            Messenger.Default.Send(Token.Message, Token.CloseTouchscreenProductionOrders);
            Messenger.Default.Send(true, Token.CloseTouchscreenWarehouseWindow);
        }

        /// <summary>
        /// Handles the incoming orders.
        /// </summary>
        /// <param name="data">The incoming order data (orders and view navigated from).</param>
        private void HandleIncomingSales(Tuple<ViewBusinessPartner, ViewType> data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("HandleIncomingSales(): Coming from {0}", data.Item2));
            this.GetAllOrders();
            this.Orders.Clear();
            this.handlingIncomingSales = true;

            var localPartner = data.Item1;
            this.navigatedFromView = data.Item2;

            if (localPartner == null || localPartner.Name.Equals(Strings.AllPartners))
            {
                this.Log.LogDebug(this.GetType(), "Displaying all orders");
                this.Orders = new ObservableCollection<ProductionData>(this.allOrders);
                return;
            }

            if (localPartner.BPMasterID != 0)
            {
                // Show supplier orders only.
                this.Log.LogDebug(this.GetType(), string.Format("Displaying orders for partner:{0}", localPartner.BPMasterID));
                this.Partner = localPartner;
                var partnerOrders = this.allOrders.Where(x => x.BPCustomer != null && x.BPCustomer.BPMasterID == this.Partner.BPMasterID).ToList();
                if (partnerOrders.Any())
                {
                    this.SetDefaultSortParameters(false);
                    this.Orders = new ObservableCollection<ProductionData>(partnerOrders);
                }
            }
        }

        /// <summary>
        /// Sets the default sorting values.
        /// </summary>
        /// <param name="setPartner">Flag, as to whether the partners is to be (re)set.</param>
        private void SetDefaultSortParameters(bool setPartner = true)
        {
            this.ScheduledDateSort = Constant.Ascending;
            this.DisplayOrderDate = this.allDates;

            if (setPartner)
            {
                this.Partner = this.allPartners;
            }
        }

        /// <summary>
        /// Refreshes the filters.
        /// </summary>
        private void RefreshFilters()
        {
            this.SetDefaultSortParameters();
            this.Orders = new ObservableCollection<ProductionData>(this.allOrders);
        }

        private void HandleOrderSearch(string data)
        {
            this.Log.LogInfo(this.GetType(), $"HandleOrderSearch - Data:{data}");
            var trans = this.DataManager.GetStockTransactionIncDeleted(data.ToInt());
            if (trans == null)
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.BatchNotFound, data));
                NouvemMessageBox.Show(string.Format(Message.BatchNotFound, data), NouvemMessageBoxButtons.OK, touchScreen: true);
                return;
            }

            var localBatch = this.allOrders.FirstOrDefault(
                x => x.Order.PROrderID == trans.MasterTableID);

            if (localBatch == null)
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.BatchNotFound, data));
                NouvemMessageBox.Show(string.Format(Message.BatchNotFound, data), touchScreen: true);
                return;
            }

            this.Orders.Clear();
            this.Orders.Add(localBatch);
            this.SelectedOrder = this.Orders.First();
        }

        #endregion

        #endregion
    }
}
