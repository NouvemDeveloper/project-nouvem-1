﻿// -----------------------------------------------------------------------
// <copyright file="ProductionProductsCardsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Model.Enum;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Production.OutOfProduction
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Shared;
    using Nouvem.ViewModel.Purchases.APReceipt;

    public class ProductionProductsCardsViewModel : TouchscreenProductsViewModel
    {
        #region field

        /// <summary>
        /// The allowable out of production products.
        /// </summary>
        private IList<InventoryItem> allowableProducts = new List<InventoryItem>();

        /// <summary>
        /// All the groups.
        /// </summary>
        private IList<INGroup> productionGroups = new List<INGroup>();

        /// <summary>
        /// The hierarchical groups have allowable products flag.
        /// </summary>
        private bool groupsContainAllowableProducts;

        /// <summary>
        /// The display production details collection.
        /// </summary>
        private ObservableCollection<InventoryItem> displayProductionDetails = new ObservableCollection<InventoryItem>();

        /// <summary>
        /// The display production details collection.
        /// </summary>
        private InventoryItem selectedDisplayProductionDetail;

        private bool swapProduct;

        private bool sortMode;
        private bool sortGroupMode;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductionProductsCardsViewModel"/> class.
        /// </summary>
        public ProductionProductsCardsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.ClearSearch, t =>
            {
                if (!string.IsNullOrEmpty(this.ProductsSearchText))
                {
                    this.ProductsSearchText = string.Empty;
                }
            });

            Messenger.Default.Register<List<INGroup>>(this, Token.SendSortGroupProducts, t =>
            {
                if (t.Count == 0)
                {
                    foreach (var inventoryItem in this.ProductGroups)
                    {
                        inventoryItem.SwapProduct = false;
                    }

                    return;
                }

                foreach (var selectedProduct in t)
                {
                    selectedProduct.SwapProduct = true;
                }

                if (t.Count == 2)
                {
                    this.HandleGroupSorting(t);
                }
            });

            Messenger.Default.Register<List<InventoryItem>>(this, Token.SendSortProducts, t =>
            {
                if (t.Count == 0)
                {
                    foreach (var inventoryItem in this.Products)
                    {
                        inventoryItem.SwapProduct = false;
                    }

                    return;
                }

                foreach (var selectedProduct in t)
                {
                    selectedProduct.SwapProduct = true;
                }

                if (t.Count == 2)
                {
                    this.HandleSorting(t);
                }
            });

            Messenger.Default.Register<Tuple<decimal,decimal>>(this, Token.UpdateWeights, t =>
            {
                if (this.SelectedDisplayProductionDetail != null)
                {
                    this.SelectedDisplayProductionDetail.StockWgt = this.SelectedDisplayProductionDetail.StockWgt + t.Item1;
                    this.SelectedDisplayProductionDetail.StockQty = this.SelectedDisplayProductionDetail.StockQty + t.Item2;
                }
            });

            #endregion

            #region command handler

            this.TouchUpGroupCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.GetSortGroupProducts);
            }, () => this.SortGroupMode);

            this.TouchUpCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.GetSortProducts);
            }, () => this.SortMode);

            this.OnGridViewLoadedCommand = new RelayCommand(() => { this.UpdateGridData(this.PROrderID); });

            this.OnUnloadedCommand = new RelayCommand(() =>
            {
                if (this.Products != null)
                {
                    foreach (var localProduct in this.Products)
                    {
                        localProduct.IsSelected = false;
                    }

                    this.SelectedProduct = null;
                }
            });

            // Scroll the suppliers grid.
            this.GroupSelectedCommand = new RelayCommand(() =>
            {
                this.HandleProductSelection();
            });

            #endregion

            this.productionGroups = this.DataManager.GetInventoryGroups();
        }

        #endregion

        #region public interface

        #region property

        public bool SwapProduct
        {
            get
            {
                return this.swapProduct;
            }

            set
            {
                this.swapProduct = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether sorting mode is on.
        /// </summary>
        public bool SortGroupMode
        {
            get { return this.sortGroupMode; }

            set
            {
                this.sortGroupMode = value;
                this.RaisePropertyChanged();
                this.SelectedProductGroup = null;
                ApplicationSettings.SortGroupMode = value;

                if (value)
                {
                    this.Products.Clear();
                    this.Locator.OutOfProduction.Spec = Strings.SortingGroups;
                }
                else
                {
                    this.Locator.OutOfProduction.Spec = this.Locator.OutOfProduction.SelectedOrder != null
                        ? this.Locator.OutOfProduction.SelectedOrder.Reference
                        : Message.SelectProductionOrder;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether sorting mode is on.
        /// </summary>
        public bool SortMode
        {
            get { return this.sortMode; }

            set
            {
                this.sortMode = value;
                this.RaisePropertyChanged();
                this.SelectedProduct = null;
                ApplicationSettings.SortMode = value;
                if (value)
                {
                    this.Locator.OutOfProduction.Spec = Strings.SortingProducts;
                }
                else
                {
                    this.Locator.OutOfProduction.Spec = this.Locator.OutOfProduction.SelectedOrder != null 
                        ? this.Locator.OutOfProduction.SelectedOrder.Reference 
                        : Message.SelectProductionOrder;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the grid view is loaded.
        /// </summary>
        public int? PROrderID { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the grid view is loaded.
        /// </summary>
        public bool GridViewLoaded { get; set; }

        /// <summary>
        /// Gets or sets the production details for the display grid.
        /// </summary>
        public ObservableCollection<InventoryItem> DisplayProductionDetails
        {
            get
            {
                return this.displayProductionDetails;
            }

            set
            {
                this.displayProductionDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the production details for the display grid.
        /// </summary>
        public InventoryItem SelectedDisplayProductionDetail
        {
            get
            {
                return this.selectedDisplayProductionDetail;
            }

            set
            {
                this.selectedDisplayProductionDetail = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.SelectedProduct =
                        this.allowableProducts.FirstOrDefault(x => x.INMasterID == value.ProductID);
                }
                else
                {
                    this.SelectedProduct = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if the selected product has been processed.
        /// </summary>
        public bool ProductProcessingComplete { get; set; }

        /// <summary>
        /// Sets the products allowed out of production.
        /// </summary>
        /// <param name="allowableProducts">The incoming allowable products.</param>
        public void SetAllowableProducts(IList<InventoryItem> localAllowableProducts)
        {
            this.Products.Clear();
            this.allowableProducts = localAllowableProducts.Where(x => (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                         || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)).ToList();
       

            this.allProducts = this.allowableProducts.GroupBy(x => x.Group).ToDictionary(key => key.Key, value => value.ToList());
            this.GetProductGroups();
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a product touch down in sort mode.
        /// </summary>
        public ICommand TouchDownCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a product touch up in sort mode.
        /// </summary>
        public ICommand TouchUpCommand { get; set; }

        /// <summary>
        /// Gets the command to handle a product touch up in sort mode.
        /// </summary>
        public ICommand TouchUpGroupCommand { get; set; }

        /// <summary>
        /// Gets the command to scroll the ui.
        /// </summary>
        public ICommand GroupSelectedCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the ui load event.
        /// </summary>
        public ICommand OnLoadedCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the ui unload event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; set; }

        /// <summary>
        /// Gets the command to scroll the ui.
        /// </summary>
        public ICommand OnGridViewLoadedCommand { get; set; }

        #endregion

        #region method

        public void UpdateGridData(int? orderId)
        {
            this.DisplayProductionDetails.Clear();
            if (this.PROrderID.HasValue)
            {
                var details = this.DataManager.GetProductionBatchDetails(orderId.ToInt(), NouvemGlobal.TransactionTypeProductionReceiptId);

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    foreach (var detail in details)
                    {
                        this.DisplayProductionDetails.Add(detail);
                    }
                }));
            }
        }

        public void SetProduct(int productId, int groupId)
        {
            this.SelectedProductGroup = this.productionGroups.FirstOrDefault(x => x.INGroupID == groupId);
            if (this.SelectedProductGroup != null)
            {
                this.HandleProductionProductGroupSelection();
                this.SelectedProduct = this.Products.FirstOrDefault(x => x.Master.INMasterID == productId);
            }
        }

        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Shows/hides the keyboard.
        /// </summary>
        protected override void FindProductCommandExecute()
        {
            //if (!string.IsNullOrEmpty(this.ProductsSearchText))
            //{
                this.ProductsSearchText = string.Empty;
           // }
        
            base.FindProductCommandExecute();
        }

        /// <summary>
        /// Update the products to that of the selected group.
        /// </summary>
        protected override void HandleProductionProductGroupSelection()
        {
            if (this.SortGroupMode)
            {
                return;
            }

            this.DeselectProduct();
            this.Products.Clear();
           
            var localProductGroup =
                this.allProducts.FirstOrDefault(x => x.Key.INGroupID == this.SelectedProductGroup.INGroupID);

            if (localProductGroup.Key != null)
            {
                const int newProductIndex = 100000;
                var count = localProductGroup.Value.Count;
                var unsortedProducts = localProductGroup.Value.Where(x => !x.SortIndex.HasValue).ToList();
                var newProducts = localProductGroup.Value.Where(x => x.SortIndex.HasValue && x.SortIndex == newProductIndex).OrderBy(x => x.Master.CreationDate).ToList();
                var indexedProducts  = localProductGroup.Value.Where(x => x.SortIndex.HasValue && x.SortIndex != newProductIndex).ToList();
                var outOfRangeIndexProducts = indexedProducts.Where(x => x.Master.SortIndex > count);
                var inRangeIndexProducts = indexedProducts.Where(x => x.Master.SortIndex <= count).OrderBy(x => x.SortIndex);
                var unionProducts = outOfRangeIndexProducts.Union(unsortedProducts).OrderBy(x => x.Name).Union(newProducts).ToList();

                foreach (var product in inRangeIndexProducts)
                {
                    unionProducts.Insert(product.SortIndex.ToInt() - 1, product);
                }
           
                this.Products = new ObservableCollection<InventoryItem>(unionProducts);
                //this.AllSearchProducts = unionProducts;
                this.DeselectProduct();
            }

            this.DisplayProductGroups();
        }

        protected override void HandleProductGroupSelection()
        {
            //base.HandleProductGroupSelection();
        }

        /// <summary>
        /// Handles a product selection.
        /// </summary>
        protected override void HandleProductSelection()
        {
            if (this.SortMode)
            {
                return;
            }

            #region validation

            if (this.SelectedProduct == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(this.ProductsSearchText))
            {
                this.EntitySelectionChange = true;
                this.ProductsSearchText = string.Empty;
                this.EntitySelectionChange = false;
                //Messenger.Default.Send(false, Token.DisplayKeyboard);
            }

            #endregion

            try
            {
                this.ProductProcessingComplete = false;
                this.Log.LogDebug(this.GetType(), string.Format("HandleProductSelection: Selected product:{0}", this.SelectedProduct.Master));
                this.Locator.OutOfProduction.HandleProductSelection(this.SelectedProduct);
            }
            finally
            {
                this.ProductProcessingComplete = true;
            }

            try
            {
                var customerId = this.Locator.OutOfProduction.SelectedCustomer != null
                    ? this.Locator.OutOfProduction.SelectedCustomer.BPMasterID
                    : 0;
                var customerGroupId = this.Locator.OutOfProduction.SelectedCustomer != null
                    ? this.Locator.OutOfProduction.SelectedCustomer.BPGroupID
                    : 0;
                var warehouseId =
                    this.Locator.OutOfProduction.SelectedWarehouse == null || this.Locator.OutOfProduction.SelectedWarehouse.Name == Strings.TerminalLocation
                        ? NouvemGlobal.OutOfProductionId
                        : this.Locator.OutOfProduction.SelectedWarehouse.WarehouseID;
                this.LabelManager.GetLabelAndDisplay(customerId, customerGroupId, this.SelectedProduct.Master.INMasterID, this.SelectedProduct.Master.INGroupID, LabelType.Item, warehouseId:warehouseId);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Gets the product groups.
        /// </summary>
        protected override void GetProductGroups()
        {
            if (!this.allowableProducts.Any())
            {
                return;
            }

            const int newProductGroupIndex = 100000;
            var count = this.productionGroups.Count;
            this.allProductGroups.Clear();
           
            var unSortedGroups = this.productionGroups.Where(x => !x.SortIndex.HasValue);
            var newGroups = this.productionGroups.Where(x => x.SortIndex.HasValue && x.SortIndex == newProductGroupIndex);
            var indexedGroups = this.productionGroups.Where(x => x.SortIndex.HasValue && x.SortIndex != newProductGroupIndex).ToList();
            var outOfRangeGroups = indexedGroups.Where(x => x.SortIndex > count);
            var inRangeGroups = indexedGroups.Where(x => x.SortIndex <= count).OrderBy(x => x.SortIndex); 
            var unionGroups = outOfRangeGroups.Union(unSortedGroups).OrderBy(x => x.Name).Union(newGroups).ToList();

            foreach (var inRangeGroup in inRangeGroups)
            {
                unionGroups.Insert(inRangeGroup.SortIndex.ToInt() -1, inRangeGroup);
            }

            foreach (var localGroup in unionGroups)
            {
                this.groupsContainAllowableProducts = false;
                if (this.DoesGroupHierarchyHaveProducts(localGroup))
                {
                    this.allProductGroups.Add(localGroup);
                }
            }

            // Set the base groduct groups for initial display.
            //this.ProductGroups = new ObservableCollection<INGroup>(this.allProductGroups.Where(x => x.ParentInGroupID.IsNullOrZero()));
            this.ProductGroups = new ObservableCollection<INGroup>();
            foreach (var localGroup in this.allProductGroups)
            {
                if (localGroup.ParentInGroupID.IsNullOrZero())
                {
                    this.ProductGroups.Add(localGroup);
                }
            }

            this.homeProductGroups = this.ProductGroups.ToList();
        }
        
        /// <summary>
        /// Determines is any group or any of it's sub groups have allowable products.
        /// </summary>
        /// <param name="group">The group to check.</param>
        /// <returns>Flag, as to whether there are hierarchical group allowable products.</returns>
        private bool DoesGroupHierarchyHaveProducts(INGroup group)
        {
            if (group == null)
            {
                return false;
            }

            if (this.allowableProducts.Any(x => x.Group != null && x.Group.INGroupID == group.INGroupID))
            {
                this.groupsContainAllowableProducts = true;
                return true;
            }

            if (this.productionGroups.All(x => x.ParentInGroupID != group.INGroupID))
            {
                // group doesn't contain any allowable products, and it has no sub groups.
                return false;
            }

            // recursively check the sub groups
            var subGroups = this.productionGroups.Where(x => x != null && x.ParentInGroupID == group.INGroupID);
            foreach (var subGroup in subGroups)
            {
                this.DoesGroupHierarchyHaveProducts(subGroup);
            }

            return this.groupsContainAllowableProducts;
        }


        /// <summary>
        /// Gets all the products.
        /// </summary>
        protected override void GetAllProducts()
        {
        }

        #endregion

        #endregion

        #region private

        /// <summary>
        /// Swaps the sort index of the 2 selected products.
        /// </summary>
        /// <param name="sortProducts">The products to swap.</param>
        private void HandleSorting(List<InventoryItem> sortProducts)
        {
            var productA = sortProducts.ElementAt(0);
            var productB = sortProducts.ElementAt(1);

            if (!productA.SortIndex.HasValue)
            {
                productA.Master.SortIndex = this.Products.IndexOf(productA) + 1;
            }

            if (!productB.SortIndex.HasValue)
            {
                productB.Master.SortIndex = this.Products.IndexOf(productB) + 1;
            }

            Application.Current.Dispatcher.Invoke(() =>
            {
                NouvemMessageBox.Show(string.Format(Message.SortProductsPrompt, productA.Name, productB.Name), NouvemMessageBoxButtons.YesNo, touchScreen:true);
            });

            if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
            {
                var productAIndex = productA.SortIndex;
                var productBIndex = productB.SortIndex;
                productA.Master.SortIndex = productBIndex;
                productB.Master.SortIndex = productAIndex;
                if (this.DataManager.SwapSortIndexes(productA, productB))
                {
                    this.SelectedProductGroup = this.SelectedProductGroup;
                    this.HandleProductionProductGroupSelection();
                    SystemMessage.Write(MessageType.Priority, Message.SortProductsSwapped);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.SortProductsSwapError);
                }
            }

            this.SelectedProduct = null;
            foreach (var inventoryItem in this.Products)
            {
                inventoryItem.SwapProduct = false;
            }
        }

        /// <summary>
        /// Swaps the sort index of the 2 selected products.
        /// </summary>
        /// <param name="sortProducts">The products to swap.</param>
        private void HandleGroupSorting(List<INGroup> sortGroups)
        {
            var productGroupA = sortGroups.ElementAt(0);
            var productGroupB = sortGroups.ElementAt(1);

            if (!productGroupA.SortIndex.HasValue)
            {
                productGroupA.SortIndex = this.ProductGroups.IndexOf(productGroupA) + 1;
            }

            if (!productGroupB.SortIndex.HasValue)
            {
                productGroupB.SortIndex = this.ProductGroups.IndexOf(productGroupB) + 1;
            }

            Application.Current.Dispatcher.Invoke(() =>
            {
                NouvemMessageBox.Show(string.Format(Message.SortProductsPrompt, productGroupA.Name, productGroupB.Name), NouvemMessageBoxButtons.YesNo, touchScreen: true);
            });

            if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
            {
                var productAIndex = productGroupA.SortIndex;
                var productBIndex = productGroupB.SortIndex;
                productGroupA.SortIndex = productBIndex;
                productGroupB.SortIndex = productAIndex;
                if (this.DataManager.SwapGroupSortIndexes(productGroupA, productGroupB))
                {
                    this.GetProductGroups();
                    SystemMessage.Write(MessageType.Priority, Message.SortGroupsSwapped);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.SortGroupsSwapIssue);
                }
            }

            this.SelectedProductGroup = null;
            foreach (var group in this.ProductGroups)
            {
                group.SwapProduct = false;
            }
        }

        #endregion
    }
}
