﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using DevExpress.Xpf.Grid;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Production.Receipe
{
    public class ReceipeViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// The selected method.
        /// </summary>
        private NouOrderMethod selectedOrderMethod;

        /// <summary>
        /// Multi weight on recipe completion flag.
        /// </summary>
        private bool multipleReceipts;

        /// <summary>
        /// Multi weight on recipe completion flag.
        /// </summary>
        private decimal? recipePrice;

        /// <summary>
        /// Multi weight on recipe completion flag.
        /// </summary>
        private bool reworkStockNonAdjust;

        /// <summary>
        /// The receipe product.
        /// </summary>
        private InventoryItem selectedProduct;

        /// <summary>
        /// The valuation price book.
        /// </summary>
        private PriceMaster selectedValuePriceList;

        /// <summary>
        /// The valuation price book.
        /// </summary>
        private PriceMaster selectedCostPriceList;

        /// <summary>
        /// The typical batch size.
        /// </summary>
        private decimal? typicalBatchSize;

        /// <summary>
        /// The typical batch size.
        /// </summary>
        private string typicalMixQty;

        /// <summary>
        /// The auto update recipe price book flag.
        /// </summary>
        private bool autoUpdate;

        /// <summary>
        /// The auto complete a mix flag.
        /// </summary>
        private bool autoCompleteMix;

        /// <summary>
        /// The typical batch size.
        /// </summary>
        private string mixNotes;

        private string closeToken = Token.CloseReceipeWindow;

        /// <summary>
        /// The typical batch value.
        /// </summary>
        private decimal? valuationPrice;

        /// <summary>
        /// The receipe alias name.
        /// </summary>
        private string alias;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceipeViewModel"/> class.
        /// </summary>
        public ReceipeViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            Messenger.Default.Register<Sale>(this,Token.RecipeSelected, s =>
            {
                this.SelectedProduct = this.ReceipeItems.FirstOrDefault(x => x.INMasterID == s.INMasterID);
            });

            #endregion

            #region command handler

            this.DrillDownToINMasterCommand = new RelayCommand(this.DrillDownToINMasterItemCommandExecute);

            #endregion

            this.GetOrderMethods();
            this.HasWriteAuthorisation = true;
            this.GetIssueMethods();
            this.GetUOMs();
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
            this.SetRecipePriceList();
            this.AutoUpdate = ApplicationSettings.AutoUpdateRecipePrice;
            this.AutoCompleteMix = ApplicationSettings.AutoCompleteRecipeMix;
            this.IsFormLoaded = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceipeViewModel"/> class.
        /// </summary>
        public ReceipeViewModel(int inMasterId)
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

        

            #endregion

            #region command handler

            this.DrillDownToINMasterCommand = new RelayCommand(this.DrillDownToINMasterItemCommandExecute);

            #endregion

            this.closeToken = Token.CloseReceipeWithProductWindow;
            this.GetIssueMethods();
            this.GetUOMs();
            this.IsFormLoaded = true;
            this.SelectedProduct = this.ReceipeItems.FirstOrDefault(x => x.Master.INMasterID == inMasterId);
            this.SetRecipePriceList();
            this.AutoUpdate = ApplicationSettings.AutoUpdateRecipePrice;
            this.AutoCompleteMix = ApplicationSettings.AutoCompleteRecipeMix;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// The selected order method.
        /// </summary>
        public NouOrderMethod SelectedOrderMethod
        {
            get
            {
                return this.selectedOrderMethod;
            }

            set
            {
                this.SetMode(value, this.selectedOrderMethod);
                this.selectedOrderMethod = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public decimal? RecipePrice
        {
            get
            {
                return this.recipePrice;
            }

            set
            {
                this.recipePrice = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public string Alias
        {
            get
            {
                return this.alias;
            }

            set
            {
                this.SetMode(value, this.alias);
                this.alias = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the auto completion of a mix flag.
        /// </summary>
        public bool AutoCompleteMix
        {
            get
            {
                return this.autoCompleteMix;
            }

            set
            {
                this.SetMode(value, this.autoCompleteMix);
                this.autoCompleteMix = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recipe price book auto update value.
        /// </summary>
        public bool AutoUpdate
        {
            get
            {
                return this.autoUpdate;
            }

            set
            {
                this.SetMode(value, this.autoUpdate);
                this.autoUpdate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the mix completion pop up notes.
        /// </summary>
        public string MixNotes
        {
            get
            {
                return this.mixNotes;
            }

            set
            {
                this.SetMode(value, this.mixNotes);
                this.mixNotes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public decimal? TypicalBatchSize
        {
            get
            {
                return this.typicalBatchSize;
            }

            set
            {
                this.SetMode(value, this.typicalBatchSize);
                this.typicalBatchSize = value;
                this.RaisePropertyChanged();
                this.TypicalBatchValue = value.ToDecimal() * this.valuationPrice.ToDecimal();
            }
        }

        /// <summary>
        /// Gets or sets the stock labels to print.
        /// </summary>
        public bool MultiReceipts
        {
            get
            {
                return this.multipleReceipts;
            }

            set
            {
                this.SetMode(value, this.multipleReceipts);
                this.multipleReceipts = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock labels to print.
        /// </summary>
        public bool ReworkStockNonAdjust
        {
            get
            {
                return this.reworkStockNonAdjust;
            }

            set
            {
                this.SetMode(value, this.reworkStockNonAdjust);
                this.reworkStockNonAdjust = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public string TypicalMixQty
        {
            get
            {
                return this.typicalMixQty;
            }

            set
            {
                this.SetMode(value, this.typicalMixQty);
                this.typicalMixQty = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public decimal? ValuationPrice
        {
            get
            {
                return this.valuationPrice;
            }

            set
            {
                this.valuationPrice = value;
                this.RaisePropertyChanged();
                this.TypicalBatchValue = this.TypicalBatchSize.ToDecimal() * value.ToDecimal();
            }
        }

        /// <summary>
        /// Gets or sets the selected valuation price book.
        /// </summary>
        public PriceMaster SelectedValuePriceList
        {
            get
            {
                return this.selectedValuePriceList;
            }

            set
            {
                this.SetMode(value, this.selectedValuePriceList);
                this.selectedValuePriceList = value;
                this.RaisePropertyChanged();

                this.ValuationPrice = null;
                if (value != null && this.selectedProduct != null)
                {
                    var detail = this.DataManager.GetPriceListDetail(value.PriceListID, this.SelectedProduct.Master.INMasterID);
                    if (detail != null)
                    {
                        this.ValuationPrice = detail.Price;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected cost price book.
        /// </summary>
        public PriceMaster SelectedCostPriceList
        {
            get
            {
                return this.selectedCostPriceList;
            }

            set
            {
                this.SetMode(value, this.selectedCostPriceList);
                this.selectedCostPriceList = value;
                this.RaisePropertyChanged();
                // NouvemGlobal.ReceipePriceList = value;
                if (this.SelectedProduct != null && value != null)
                {
                    var priceData =
                        this.DataManager.GetPriceListDetail(value.PriceListID, this.selectedProduct.Master.INMasterID);
                    if (priceData != null)
                    {
                        this.RecipePrice = priceData.Price;
                    }
                    else
                    {
                        this.RecipePrice = null;
                    }
                }

                //if (this.SaleDetails != null)
                //{
                //    foreach (var saleDetail in this.SaleDetails)
                //    {
                //        saleDetail.HandleSelectedReceipeItem(saleDetail.INMasterID);
                //    }
                //}
            }
        }

        /// <summary>
        /// Gets or sets the selected partner.
        /// </summary>
        public InventoryItem SelectedProduct
        {
            get
            {
                return this.selectedProduct;
            }

            set
            {
                this.SetMode(value, this.selectedProduct);
                this.selectedProduct = value;
                this.RaisePropertyChanged();
                if (value != null && !this.EntitySelectionChange)
                {
                    var localReceipe = this.DataManager.GetRecipeByProductId(value.Master.INMasterID);
                    if (this.CurrentMode == ControlMode.Copy)
                    {
                        if (localReceipe != null)
                        {
                            SystemMessage.Write(MessageType.Issue, string.Format(Message.RecipeForProductExists, this.GetProductName(localReceipe.INMasterID.ToInt())));

                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                NouvemMessageBox.Show(string.Format(Message.RecipeForProductExists, this.GetProductName(localReceipe.INMasterID.ToInt())));
                            }));
                        }
                        else
                        {
                            if (this.Sale != null)
                            {
                                this.Sale.AllowCopy = true;
                            }

                            this.SetControlMode(ControlMode.Add);
                        }

                        return;
                    }

                    this.SaleDetails.Clear();
                    if (localReceipe != null)
                    {
                        this.Sale = localReceipe;
                        this.SetControlMode(ControlMode.Update);
                    }
                    else
                    {
                        this.Sale = new Sale();
                        this.SetControlMode(ControlMode.Add);
                    }
                }

                if (this.SelectedCostPriceList != null && value != null)
                {
                    var priceData =
                        this.DataManager.GetPriceListDetail(this.SelectedCostPriceList.PriceListID, value.Master.INMasterID);
                    if (priceData != null)
                    {
                        this.RecipePrice = priceData.Price;
                    }
                    else
                    {
                        this.RecipePrice = null;
                    }
                }
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetRecipeByLastEdit();
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetRecipeByFirstLast(true);
                return;
            }

            var localSale = this.DataManager.GetRecipeById(this.Sale.SaleID - 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetRecipeByFirstLast(false);
                return;
            }

            var localSale = this.DataManager.GetRecipeById(this.Sale.SaleID + 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetRecipeByFirstLast(true);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetRecipeByFirstLast(false);
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to show the in master screen.
        /// </summary>
        public ICommand DrillDownToINMasterCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                //SystemMessage.Write(MessageType.Issue, Message.InvalidDispatchDocket);
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(102, this.Sale.SaleID.ToString(), !preview);
            return;
        }

        protected override void HandleBatchValue()
        {
            this.CalculateMargin();
        }

        /// <summary>
        /// Handles a grid field change.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            if (this.Sale == null)
            {
                return;
            }

            if (this.Sale.SaleID > 0)
            {
                this.SetControlMode(ControlMode.Update);
            }
            else
            {
                this.SetControlMode(ControlMode.Add);
            }
        }

        protected override void CalculateMargin()
        {
            try
            {
                if (this.TypicalBatchValue > 0)
                {
                    this.OrderMargin = Math.Round(((this.TypicalBatchValue.ToDecimal() - this.Total) / this.TypicalBatchValue.ToDecimal() * 100).ToDecimal(), 2);
                    this.OrderValue = this.TypicalBatchValue.ToDecimal() - this.Total.ToDecimal();
                }
                else
                {
                    this.OrderMargin = 0;
                    this.OrderValue = 0;
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Drills down to the selected item.
        /// </summary>
        protected override void DrillDownToMasterItemCommandExecute()
        {
            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            var isReipe = this.DataManager.GetRecipeByProductId(this.SelectedSaleDetail.INMasterID);
            if (isReipe == null)
            {
                Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedSaleDetail.INMasterID), Token.DrillDown);
            }
            else
            {
                Messenger.Default.Send(this.SelectedSaleDetail.INMasterID, Token.ShowReceipe);
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
        }

        /// <summary>
        /// Gets the inventory items.
        /// </summary>
        protected override void GetInventoryItems()
        {
            this.SaleItems = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems
                .Where(x => x.Master.Deleted == null && ((x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                                         || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                .OrderBy(x => x.Master.Name));

            this.ReceipeItems = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems
                .Where(x => x.Master.Deleted == null && (x.Master.ReceipeProduct!= null && x.Master.ReceipeProduct == true && (x.Master.InActiveFrom == null && x.Master.InActiveTo == null)
                                                         || (DateTime.Today < x.Master.InActiveFrom || DateTime.Today > x.Master.InActiveTo)))
                .OrderBy(x => x.Master.Name));
        }

        protected override void CopyDocumentTo()
        {
            
        }

        protected override void CopyDocumentFrom()
        {
            
        }

        protected override void FindSale()
        {
            Messenger.Default.Send(ViewType.RecipeSearch);
        }

        protected override void FindSaleCommandExecute()
        {
        }

        protected override void DirectSearchCommandExecute()
        {
         
        }

        protected override void AddSale()
        {
            if (this.OrderUpdateInProgress)
            {
                return;
            }

            this.OrderUpdateInProgress = true;

            #region validation

            var error = string.Empty;

            if (this.SelectedProduct == null || this.SelectedProduct.Master.INMasterID == 0)
            {
                error = Message.NoReceipeProductSelected;
            }

            if (error != string.Empty)
            {
                this.OrderUpdateInProgress = false;
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error);
                return;
            }

            #endregion

            try
            {
                this.CreateSale();
                ProgressBar.SetUp(0, 3);
                ProgressBar.Run();
                var newSaleId = this.DataManager.AddRecipe(this.Sale);
                if (newSaleId > 0)
                {
                    ProgressBar.Run();
                    this.Sale.SaleID = newSaleId;
                    SystemMessage.Write(MessageType.Priority, Message.ReceipeCreated);
                    if (this.AutoUpdate)
                    {
                        this.UpdateRecipePriceList();
                    }

                    this.ClearForm();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.ReceipeNotCreated);
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.ClearForm();
                this.SetControlMode(ControlMode.OK);
                this.OrderUpdateInProgress = false;
            }
        }

        protected override void UpdateSale()
        {
            if (this.OrderUpdateInProgress)
            {
                return;
            }

            this.OrderUpdateInProgress = true;

            #region validation

            var error = string.Empty;

            if (this.SelectedProduct== null || this.SelectedProduct.Master.INMasterID == 0)
            {
                error = Message.NoReceipeProductSelected;
            }

            if (error != string.Empty)
            {
                this.OrderUpdateInProgress = false;
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error);
                return;
            }

            #endregion

            try
            {
                this.CreateSale();
                ProgressBar.SetUp(0, 3);
                ProgressBar.Run();
      
                if (this.DataManager.UpdateRecipe(this.Sale))
                {
                    SystemMessage.Write(MessageType.Priority, Message.ReceipeUpdated);
                    if (this.AutoUpdate)
                    {
                        this.UpdateRecipePriceList();
                        return;
                    }

                    this.ClearForm();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.ReceipeNotUpdated);
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.ClearForm();
                this.SetControlMode(ControlMode.OK);
                this.OrderUpdateInProgress = false;
            }
        }

        /// <summary>
        /// Handles a sale selection.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            try
            {
                this.EntitySelectionChange = true;
                this.SelectedProduct =
                    this.ReceipeItems.FirstOrDefault(x => x.Master.INMasterID == this.Sale.INMasterID);
                this.Remarks = this.Sale.Remarks;
                this.SubTotal = this.Sale.SubTotalExVAT.ToDecimal();
                this.Tax = this.Sale.VAT.ToDecimal(); 
                this.Discount = this.Sale.DiscountIncVAT.ToDecimal();
                this.Total = this.Sale.GrandTotalIncVAT.ToDecimal(); 
                this.Alias = this.Sale.Alias;
                this.MultiReceipts = this.Sale.MultipleReceipts.ToBool();
                this.ReworkStockNonAdjust = this.Sale.ReworkStockNonAdjust.ToBool();
                this.MixNotes = this.Sale.TechnicalNotes;
                this.AutoCompleteMix = this.Sale.AutoCompleteMix.ToBool();

                if (this.SelectedCostPriceList != null)
                {
                    this.SelectedCostPriceList = null;
                }

                if (this.SelectedValuePriceList != null)
                {
                    this.SelectedValuePriceList = null;
                }

                if (this.Sale.PriceListIDValue.HasValue)
                {
                    this.SelectedValuePriceList =
                        this.PriceLists.FirstOrDefault(x => x.PriceListID == this.Sale.PriceListIDValue);
                }
                
                this.TypicalBatchSize = this.Sale.TypicalBatchSize;
                this.TypicalMixQty = this.Sale.TypicalMixQty.ToString();

                // Add the sale details.
                this.SaleDetails = new ObservableCollection<SaleDetail>(this.Sale.SaleDetails);

                if (this.Sale.PriceListID.HasValue)
                {
                    this.SelectedCostPriceList = this.PriceLists.First(x => x.PriceListID == this.Sale.PriceListID);
                }

                if (this.Sale.NouOrderMethodID == null)
                {
                    this.SelectedOrderMethod = this.OrderMethods.FirstOrDefault();
                }
                else
                {
                    this.SelectedOrderMethod =
                        this.OrderMethods.FirstOrDefault(
                            x => x.NouOrderMethodID == this.Sale.NouOrderMethodID);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
            finally
            {
                this.EntitySelectionChange = false;
                this.UpdateSaleOrderTotals();
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Creates a sale order object.
        /// </summary>
        protected override void CreateSale()
        {
            base.CreateSale();
            this.Sale.INMasterID = this.SelectedProduct.Master.INMasterID;
            this.Sale.Alias = this.Alias;
            this.Sale.PriceListID = this.SelectedCostPriceList != null ? this.SelectedCostPriceList.PriceListID : (int?)null;
            this.Sale.PriceListIDValue = this.SelectedValuePriceList != null ? this.SelectedValuePriceList.PriceListID : (int?)null;
            this.Sale.TypicalBatchSize = this.typicalBatchSize;
            this.Sale.TypicalMixQty = this.typicalMixQty.ToDecimal();
            this.Sale.MultipleReceipts = this.MultiReceipts;
            this.Sale.ReworkStockNonAdjust = this.ReworkStockNonAdjust;
            this.Sale.TechnicalNotes = this.mixNotes;
            this.Sale.AutoCompleteMix = this.autoCompleteMix;
            if (this.SelectedOrderMethod == null || this.SelectedOrderMethod.Name.Equals(Strings.NoneSelected))
            {
                this.Sale.NouOrderMethodID = null;
            }
            else
            {
                this.Sale.NouOrderMethodID = this.SelectedOrderMethod.NouOrderMethodID;
            }
        }

        /// <summary>
        /// Override, to disable any writes when docket is part of a parent document.
        /// </summary>
        /// <param name="mode">The control mode.</param>
        protected override void SetControlMode(ControlMode mode)
        {
            if (this.CurrentMode == ControlMode.Copy && this.Sale != null && !this.Sale.AllowCopy)
            {
                return;
            }

            base.SetControlMode(mode);
            if (mode == ControlMode.Copy)
            {
                this.SelectedProduct = null;
                if (this.Sale != null)
                {
                    this.Sale.SaleID = 0;
                }
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateSale();
                    Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to clean up.
        /// </summary>
        protected override void ClearForm(bool clearSelectedCustomer = true)
        {
            base.ClearForm(clearSelectedCustomer);
            this.SelectedProduct = null;
            this.SelectedCostPriceList = null;
            this.SelectedValuePriceList = null;
            this.TypicalBatchSize = null;
            this.TypicalMixQty = null;
            this.TypicalBatchValue = null;
            this.ValuationPrice = null;
            this.Alias = string.Empty;
            this.MultiReceipts = false;
            this.ReworkStockNonAdjust = false;
            this.MixNotes = string.Empty;
            this.RecipePrice = null;
            this.SelectedOrderMethod = this.OrderMethods.FirstOrDefault();
            this.SaleDetails?.Clear();
            this.SaleDetails = null;
            this.Sale = null;
            this.Sale = new Sale();
            this.SaleDetails = new ObservableCollection<SaleDetail>();
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Overridden, to delete the child grid item.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.SelectedSaleDetail != null)
            {
                this.SaleDetails.Remove(this.SelectedSaleDetail);
                if (this.Sale.SaleID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Clean up, and close.
        /// </summary>
        protected override void Close()
        {
            ApplicationSettings.RecipeScreenOffset = 20;
            ApplicationSettings.AutoUpdateRecipePrice = this.AutoUpdate;
            NouvemGlobal.ReceipePriceList = null;
            this.ClearForm();
            Messenger.Default.Unregister(this);
           // ViewModelLocator.ClearReceipe();
            Messenger.Default.Send(Token.Message, this.closeToken);
        }

        #endregion

        #region private

        #region command implementation

        /// <summary>
        /// Handler for the ui unload event.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region helper

        /// <summary>
        /// Sets the recipe price list.
        /// </summary>
        private void SetRecipePriceList()
        {
            this.SelectedCostPriceList =
                this.PriceLists.FirstOrDefault(x => x.CurrentPriceListName.CompareIgnoringCase(Strings.RecipeCostPriceList));
            NouvemGlobal.ReceipePriceList = this.SelectedCostPriceList;
        }

        /// <summary>
        /// Gets the application order methods.
        /// </summary>
        private void GetOrderMethods()
        {
            this.OrderMethods = this.DataManager.GetOrderMethods().Where(x => x.Name.CompareIgnoringCase(Constant.Quantity) 
            || x.Name.CompareIgnoringCase(Constant.Weight)).ToList();
            this.OrderMethods.Insert(0, new NouOrderMethod { Name = Strings.NoneSelected });
        }

        /// <summary>
        /// Drills down to the selected item.
        /// </summary>
        private void DrillDownToINMasterItemCommandExecute()
        {
            if (this.SelectedProduct == null)
            {
                return;
            }

            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedProduct.Master.INMasterID), Token.DrillDown);
        }

        private void UpdateRecipePriceList()
        {
            #region validation

            if (this.SelectedCostPriceList == null || this.SelectedProduct == null || !this.AutoUpdate || this.TypicalBatchSize.IsNullOrZero() || this.SaleDetails == null)
            {
                return;
            }

            #endregion

            try
            {
                var price = 0m;
                foreach (var detail in this.SaleDetails)
                {
                    price += detail.TotalExVAT.ToDecimal();
                }

                var divider = this.TypicalBatchSize.ToDecimal();
                //if (this.SelectedOrderMethod.Name.CompareIgnoringCase(Strings.Quantity))
                //{
                //    divider = this.SelectedProduct.Master.NominalWeight.ToDecimal() * divider;
                //}

                if (divider == 0)
                {
                    return;
                }

                price = Math.Round(price / divider, 5);

                if (!this.DataManager.IsProductOnPriceBook(this.SelectedCostPriceList.PriceListID,
                    this.SelectedProduct.Master.INMasterID))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        Messenger.Default.Send(ViewType.PriceListDetail);
                        this.Locator.PriceListDetail.AddProductToPriceListDetail(this.SelectedCostPriceList.PriceListID,
                            this.SelectedProduct.Master, price);
                        this.ClearForm();
                    }));

                    return;
                }

                this.DataManager.UpdatePrice(price, this.SelectedCostPriceList.PriceListID,
                    this.SelectedProduct.Master.INMasterID);

                this.ClearForm();
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }
        }

        #endregion

        #endregion
    }
}
