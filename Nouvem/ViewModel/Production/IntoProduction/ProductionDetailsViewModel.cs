﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Purchases.APReceipt;
using Nouvem.ViewModel.Sales.ARDispatch;

namespace Nouvem.ViewModel.Production.IntoProduction
{
    [Obsolete("Deprecated", true)]
    public class ProductionDetailsViewModel : APReceiptDetailsViewModel
    {
       // #region field

       // #endregion

       // #region constructor

       // /// <summary>
       // /// Initializes a new instance of the <see cref="ProductionDetailsViewModel"/> class.
       // /// </summary>
       // private ProductionDetailsViewModel()
       // {
       //     if (this.IsInDesignMode)
       //     {
       //         return;
       //     }

       //     #region message registration

       //     #endregion

       //     #region command handler

       //     #endregion
       // }

       // #endregion

       // #region public interface

       // #region property



       // #endregion

       // #region command



       // #endregion

       // #endregion

       // #region protected

       // /// <summary>
       // /// Direct the incoming sale detail.
       // /// </summary>
       // protected override void DirectSaleDetail()
       // {
       //     this.HandleDisplaySaleDetail();
       // }
        
       ///// <summary>
       // /// Handles the incoming sale detail which is being used to dispatch serial stock.
       // /// </summary>
       // public void HandleProductionDetailForSerialProduction(bool disableScanning = false)
       // {
       //     if (this.SelectedSaleDetail.INMasterID == 0 && (this.SelectedSaleDetail.GoodsReceiptDatas != null && !this.SelectedSaleDetail.GoodsReceiptDatas.Any()))
       //     {
       //         if (this.GoodsInDetails != null)
       //         {
       //             this.GoodsInDetails.Clear();
       //             Messenger.Default.Send(Token.Message, Token.ClearBatchControls);
       //         }

       //         return;
       //     }

           
       //     int? traceabilityTemplateNameId = 0;
       //     int? qualityTemplateNameId = 0;
       //     int? dateTemplateNameId = 0;

       //     #region insurance

       //     if (this.SelectedSaleDetail.InventoryItem == null)
       //     {
       //         this.SelectedSaleDetail.InventoryItem =
       //             NouvemGlobal.InventoryItems.FirstOrDefault(
       //                 x => x.Master.INMasterID == this.SelectedSaleDetail.INMasterID);

       //         if (this.SelectedSaleDetail.InventoryItem == null)
       //         {
       //             // should never happen, but just in case.
       //             this.Log.LogError(this.GetType(), string.Format("HandleProductionDetailForSerialProduction():{0}", Message.InventoryItemNotFound));
       //             SystemMessage.Write(MessageType.Issue, Message.InventoryItemNotFound);
       //             return;
       //         }
       //     }

       //     #endregion

       //     var item = this.SelectedSaleDetail.InventoryItem.Master;

       //     this.ItemNo = item.Code;
       //     this.ItemDescription = item.Name;
       //     traceabilityTemplateNameId = item.TraceabilityTemplateNameID;
       //     qualityTemplateNameId = item.QualityTemplateNameID;
       //     dateTemplateNameId = item.DateTemplateNameID;

       //     var traceability = this.traceabilityData.FirstOrDefault(x => x.Key.Item1 == traceabilityTemplateNameId && x.Key.Item2.Equals(Constant.Traceability)).Value;
       //     var quality = this.traceabilityData.FirstOrDefault(x => x.Key.Item1 == qualityTemplateNameId && x.Key.Item2.Equals(Constant.Quality)).Value;
       //     var date = this.traceabilityData.FirstOrDefault(x => x.Key.Item1 == dateTemplateNameId && x.Key.Item2.Equals(Constant.Date)).Value;

       //     if (traceability == null)
       //     {
       //         traceability = new List<TraceabilityData>();
       //     }

       //     if (quality == null)
       //     {
       //         quality = new List<TraceabilityData>();
       //     }

       //     if (date == null)
       //     {
       //         date = new List<TraceabilityData>();
       //     }

       //     var data = traceability.Concat(quality).Concat(date);

       //     var localBatchNo = new BatchNumber();
       //     if (this.SelectedSaleDetail.GoodsReceiptDatas != null && this.SelectedSaleDetail.GoodsReceiptDatas.Any())
       //     {
       //         localBatchNo = this.SelectedSaleDetail.GoodsReceiptDatas.Last().BatchNumber;
       //     }

       //     // add the generated batch number to our attributes.
       //     foreach (var localTraceabilityData in data)
       //     {
       //         localTraceabilityData.BatchNumber = localBatchNo;
       //     }

       //     this.BatchNumber = localBatchNo;

       //     var batchAttributes = new List<TraceabilityData>();
       //     var serialAttributes = new List<TraceabilityData>();

       //     StockTransactionData.TransactionData = null;

       //     // Divide the traceability attributes into batch and serial (can be both)
       //     foreach (var localData in data)
       //     {
       //         if (localData.Batch == true)
       //         {
          
       //             localData.TraceabilityValue = value.Item1;
       //             localData.TraceabilityId = value.Item2;
       //             batchAttributes.Add(localData);
       //         }

       //         if (localData.Transaction == true)
       //         {
           
       //             localSerialData.TraceabilityValue = value.Item1;
       //             localSerialData.TraceabilityId = value.Item2;
       //             serialAttributes.Add(localSerialData);
       //         }
       //     }

       //     Messenger.Default.Send(Token.Message, Token.ClearData);

       //     // send the batch attributes
       //     //if (batchAttributes.Any())
       //     //{
       //         if (ApplicationSettings.TouchScreenMode)
       //         {
       //             Messenger.Default.Send(batchAttributes, Token.CreateTouchscreenDynamicControls);
       //         }
       //         else
       //         {
       //             Messenger.Default.Send(batchAttributes, Token.CreateDynamicControlsForSerial);
       //         }
       //     //}

       //     // send the serial attributes
       //     //if (serialAttributes.Any())
       //     //{
       //         if (ApplicationSettings.TouchScreenMode)
       //         {
       //             Messenger.Default.Send(serialAttributes, Token.CreateTouchscreenDynamicSerialColumns);
       //         }
       //         else
       //         {
       //             Messenger.Default.Send(serialAttributes, Token.CreateDynamicSerialColumnsForSerial);
       //         }
       //     //}

       //     if (!ApplicationSettings.TouchScreenMode)
       //     {
       //         //// Create goods in records from the stored stock transaction/transaction traceability values.
       //         //var storedSerialData = this.CreateGoodsInDataForDispatch();
       //         //if (storedSerialData.Any())
       //         //{
       //         //    this.GoodsInDetails = new ObservableCollection<GoodsIn>(storedSerialData);
       //         //}

       //         //if (this.Locator.ARDispatch.ScannerStockMode == ScannerMode.Scan)
       //         //{
       //         //    if (this.GoodsInDetails != null && this.GoodsInDetails.Any())
       //         //    {
       //         //        this.SelectedGoodsInDetail = this.GoodsInDetails.Last();
       //         //        this.ProcessDispatch();
       //         //    }
       //         //}
       //     }
       //     else
       //     {
       //         if (this.Locator.IntoProduction.ScannerStockMode == ScannerMode.Scan && !disableScanning && ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard)
       //         {
       //             this.Locator.IntoProduction.RecordSerialProduction();
       //         }
       //     }
       // }

       // #endregion

       // #region private

       // #endregion
            
    }
}
