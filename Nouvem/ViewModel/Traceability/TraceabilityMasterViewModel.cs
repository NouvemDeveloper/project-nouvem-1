﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Traceability
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class TraceabilityMasterViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected traceability master.
        /// </summary>
        private ViewTraceabilityMaster selectedTraceabilityMaster;

        /// <summary>
        /// The traceability collection.
        /// </summary>
        private ObservableCollection<ViewTraceabilityMaster> traceabilityMasters;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the TraceabilityMasterViewModel class.
        /// </summary>
        public TraceabilityMasterViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessTraceability);

            #region message registration

            Messenger.Default.Register<string>(this, Token.SqlScript, this.HandleIncomingSql);

            Messenger.Default.Register<string>(this, Token.DataGenerated, this.HandleIncomingData);

            Messenger.Default.Register<string>(this, Token.ShowSqlWindow, s => this.ShowSqlWindow());

            Messenger.Default.Register<string>(this, Token.ShowColWindow, s => this.ShowColWindow());

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand<CellValueChangedEventArgs>(this.UpdateCommandExecute);

            #endregion

            #region instantiation

            this.TraceabilityMasters = new ObservableCollection<ViewTraceabilityMaster>();

            #endregion

            this.GetTraceabilityMasters();
            this.GetGs1Masters();
            this.GetTraceabilityTypes();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the selected traceability master.
        /// </summary>
        public ViewTraceabilityMaster SelectedTraceabilityMaster
        {
            get
            {
                return this.selectedTraceabilityMaster;
            }

            set
            {
                this.selectedTraceabilityMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the traceability Masters.
        /// </summary>
        public ObservableCollection<ViewTraceabilityMaster> TraceabilityMasters
        {
            get
            {
                return this.traceabilityMasters;
            }

            set
            {
                this.traceabilityMasters = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get the gs1 Masters.
        /// </summary>
        public ObservableCollection<ViewGs1Master> Gs1AiMasters { get; private set; }

        /// <summary>
        /// Get the system traceability types.
        /// </summary>
        public ObservableCollection<NouTraceabilityType> TraceabilityTypes { get; private set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        /// <summary>
        /// Gets the command to display the sql input window.
        /// </summary>
        public ICommand ShowSqlWindowCommand { get; set; }
        
        #endregion

        #region protected override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedTraceabilityMaster != null)
            {
                this.selectedTraceabilityMaster.Deleted = true;
                this.UpdateTraceabilityMaster();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.TraceabilityMasters.Remove(this.selectedTraceabilityMaster);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateTraceabilityMaster();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle a cell value change.
        /// </summary>
        /// <param name="e">The event arg.</param>
        private void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            #region validation

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property))
            {
                return;
            }

            #endregion

            if (!this.CanUpdate())
            {
                return;
            }

            this.SetControlMode(ControlMode.Update);

            if (e.Cell.Property.Equals("NouTraceabilityTypeID"))
            {
                this.ClearData((int)e.Value);
            }
        }

        #endregion

        #region helper

        /// <summary>
        /// Handles the incoming sql generation.
        /// </summary>
        /// <param name="sql">The incoming sql.</param>
        private void HandleIncomingSql(string sql)
        {
            /* ViewTraceabilityMaster is an entity object, so we can't implement the INotifyPropertyChanged interface to update immediately.
             * This more long winded approach works, but is not ideal */
            var pos = 0;
            var localItem = this.selectedTraceabilityMaster;
            for (int i = 0; i < this.TraceabilityMasters.Count; i++)
            {
                var item = this.TraceabilityMasters.ElementAt(i);
                if (item.TraceabilityTemplatesMasterID ==
                    this.selectedTraceabilityMaster.TraceabilityTemplatesMasterID)
                {
                    localItem.SQL = sql;
                    pos = i;
                    break;
                }
            }

            if (pos > 0)
            {
                this.TraceabilityMasters.Remove(this.selectedTraceabilityMaster);
                this.TraceabilityMasters.Insert(pos, localItem);
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Clears the sql and collection data on traceability type change.
        /// </summary>
        private void ClearData(int newValueId)
        {
            /* ViewTraceabilityMaster is an entity object, so we can't implement the INotifyPropertyChanged interface to update immediately.
             * This more long winded approach works, but is not ideal */
            var pos = 0;
            var localItem = this.selectedTraceabilityMaster;
            for (int i = 0; i < this.TraceabilityMasters.Count; i++)
            {
                var item = this.TraceabilityMasters.ElementAt(i);
                if (item.TraceabilityTemplatesMasterID ==
                    this.selectedTraceabilityMaster.TraceabilityTemplatesMasterID)
                {
                    localItem.SQL = string.Empty;
                    localItem.Collection = string.Empty;
                    localItem.NouTraceabilityTypeID = newValueId;
                    pos = i;
                    break;
                }
            }

            if (pos > 0)
            {
                this.TraceabilityMasters.Remove(this.selectedTraceabilityMaster);
                this.TraceabilityMasters.Insert(pos, localItem);
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Handles the incoming data generation.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        private void HandleIncomingData(string data)
        {
            /* ViewTraceabilityMaster is an entity object, so we can't implement the INotifyPropertyChanged interface to update immediately.
             * This more long winded approach works, but is not ideal */
            var pos = 0;
            var localItem = this.selectedTraceabilityMaster;
            for (int i = 0; i < this.TraceabilityMasters.Count; i++)
            {
                var item = this.TraceabilityMasters.ElementAt(i);
                if (item.TraceabilityTemplatesMasterID ==
                    this.selectedTraceabilityMaster.TraceabilityTemplatesMasterID)
                {
                    localItem.Collection = data;
                    pos = i;
                    break;
                }
            }

            if (pos > 0)
            {
                this.TraceabilityMasters.Remove(this.selectedTraceabilityMaster);
                this.TraceabilityMasters.Insert(pos, localItem);
                this.SetControlMode(ControlMode.Update);
            }
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        private void ShowSqlWindow()
        {
            if (this.SelectedTraceabilityMaster == null || this.SelectedTraceabilityMaster.NouTraceabilityTypeID == 0)
            {
                return;
            }

            var traceability = this.SelectedTraceabilityMaster;
            if (traceability.NouTraceabilityTypeID !=
                    NouvemGlobal.TraceabilityTypes.First(x => x.TraceabilityType.Equals(Constant.SQL)).NouTraceabilityTypeID)
            {
                // Non sql type. Exit.
                SystemMessage.Write(MessageType.Issue, Message.NonSqlTraceabilityType);
                return;
            }

            var sql = this.selectedTraceabilityMaster.SQL ?? string.Empty;
            Messenger.Default.Send(sql, Token.DisplaySql);
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        private void ShowColWindow()
        {
            if (this.SelectedTraceabilityMaster == null || this.SelectedTraceabilityMaster.NouTraceabilityTypeID == 0)
            {
                return;
            }

            var traceability = this.SelectedTraceabilityMaster;
            if (traceability.NouTraceabilityTypeID !=
                    NouvemGlobal.TraceabilityTypes.First(x => x.TraceabilityType.Equals(Constant.Collection)).NouTraceabilityTypeID)
            {
                // Non collection type. Exit.
                SystemMessage.Write(MessageType.Issue, Message.NonCollectionDataType);
                return;
            }

            var data = this.selectedTraceabilityMaster.Collection ?? string.Empty;
            Messenger.Default.Send(data, Token.DisplayCol);
        }

        /// <summary>
        /// Updates the traceability master group.
        /// </summary>
        private void UpdateTraceabilityMaster()
        {
            #region validation

            foreach (var traceability in this.TraceabilityMasters)
            {
                if (traceability.NouTraceabilityTypeID == 0)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoTypeSelected);
                    return;
                }

                if (string.IsNullOrWhiteSpace(traceability.TraceabilityCode))
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoCodeSelected);
                    return;
                }

                if (traceability.NouTraceabilityTypeID ==
                    NouvemGlobal.TraceabilityTypes.First(x => x.TraceabilityType.Equals(Constant.SQL)).NouTraceabilityTypeID)
                {
                    // If it's a sql type, ensure that a valid sql statement has been entered.
                    if (string.IsNullOrWhiteSpace(traceability.SQL))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.SqlNotEntered);
                        return;
                    }
                }

                if (traceability.NouTraceabilityTypeID ==
                    NouvemGlobal.TraceabilityTypes.First(x => x.TraceabilityType.Equals(Constant.Collection)).NouTraceabilityTypeID)
                {
                    // If it's a sql type, ensure that a valid sql statement has been entered.
                    if (string.IsNullOrWhiteSpace(traceability.Collection))
                    {
                        SystemMessage.Write(MessageType.Issue, Message.CollectionDataNotEntered);
                        return;
                    }
                }
            }

            #endregion

            try
            {
                if (this.DataManager.AddOrUpdateTraceabilityMasters(this.TraceabilityMasters.ToList()))
                {
                    SystemMessage.Write(MessageType.Priority, Message.TraceabilityMasterUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.TraceabilityMasterNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.TraceabilityMasterNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearTraceabilityMaster();
            Messenger.Default.Send(Token.Message, Token.CloseTraceabilityMasterWindow);
        }

        /// <summary>
        /// Gets the application traceability masters information.
        /// </summary>
        private void GetTraceabilityMasters()
        {
            this.TraceabilityMasters = new ObservableCollection<ViewTraceabilityMaster>(this.DataManager.GetTraceabilityMasters());
        }

        /// <summary>
        /// Gets the application Gs1 information.
        /// </summary>
        private void GetGs1Masters()
        {
            this.Gs1AiMasters = new ObservableCollection<ViewGs1Master>(this.DataManager.GetGs1MasterView());
        }

        /// <summary>
        /// Gets the application traceability types information.
        /// </summary>
        private void GetTraceabilityTypes()
        {
            this.TraceabilityTypes = new ObservableCollection<NouTraceabilityType>(this.DataManager.GetTraceabilityTypes());
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion

        #endregion
    }
}

