﻿// -----------------------------------------------------------------------
// <copyright file="DateViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Date
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class DateViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The dates collection.
        /// </summary>
        private ObservableCollection<ViewDatesMaster> datesMasters;

        /// <summary>
        /// The selected date.
        /// </summary>
        private ViewDatesMaster selectedDate;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the DateViewModel class.
        /// </summary>
        public DateViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }
 
            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessGS1);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            #region instantiation

            this.DatesMasters = new ObservableCollection<ViewDatesMaster>();

            #endregion

            this.GetDatesMasters();
            this.GetGs1Masters();
            this.GetDateTypes();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the selected date.
        /// </summary>
        public ViewDatesMaster SelectedDate
        {
            get
            {
                return this.selectedDate;
            }

            set
            {
                this.selectedDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the dates Masters.
        /// </summary>
        public ObservableCollection<ViewDatesMaster> DatesMasters
        {
            get
            {
                return this.datesMasters;
            }

            set
            {
                this.datesMasters = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get the gs1 Masters.
        /// </summary>
        public ObservableCollection<ViewGs1Master> Gs1AiMasters { get; private set; }

        /// <summary>
        /// Get the system date types.
        /// </summary>
        public ObservableCollection<NouDateType> DatesTypes { get; private set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the dates masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateDateMaster();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the date master group.
        /// </summary>
        private void UpdateDateMaster()
        {
            #region validation

            foreach (var date in this.DatesMasters)
            {
                if (date.NouDateTypeID == 0)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoTypeSelected);
                    return;
                }

                if (string.IsNullOrWhiteSpace(date.DateCode))
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoCodeSelected);
                    return;
                }

                if (string.IsNullOrWhiteSpace(date.DateDescription))
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoDescriptionSelected);
                    return;
                }
            }

            #endregion

            try
            {
                if (this.DataManager.AddOrUpdateDateMasters(this.DatesMasters))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DateMasterUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DateMasterNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.DateMasterNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearDate();
            Messenger.Default.Send(Token.Message, Token.CloseDateWindow);
        }

        /// <summary>
        /// Gets the application dates masters information.
        /// </summary>
        private void GetDatesMasters()
        {
            this.DatesMasters.Clear();
            foreach (var date in this.DataManager.GetDateMasters())
            {
                this.DatesMasters.Add(date);
            }
        }

        /// <summary>
        /// Gets the application Gs1 information.
        /// </summary>
        private void GetGs1Masters()
        {
           this.Gs1AiMasters = new ObservableCollection<ViewGs1Master>(this.DataManager.GetGs1MasterView());
        }

        /// <summary>
        /// Gets the application date types information.
        /// </summary>
        private void GetDateTypes()
        {
            this.DatesTypes = new ObservableCollection<NouDateType>(this.DataManager.GetDateTypes());
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}
