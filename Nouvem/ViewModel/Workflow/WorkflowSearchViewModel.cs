﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowAlertsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.ViewModel.Sales;

    public class WorkflowSearchViewModel : SalesSearchDataViewModel
    {
        #region field

        /// <summary>
        /// The workflows to update.
        /// </summary>
        private HashSet<Sale> workFlowsToUpdate = new HashSet<Sale>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowSearchViewModel"/> class.
        /// </summary>
        public WorkflowSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command registration

            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                if (this.SelectedSale != null && !string.IsNullOrWhiteSpace(s))
                {
                    this.SelectedSale.PopUpNote = s;
                    this.SetControlMode(ControlMode.Update);

                    var localWorkFlow = this.workFlowsToUpdate.FirstOrDefault(x => x.SaleID == this.SelectedSale.SaleID);
                    if (localWorkFlow != null)
                    {
                        this.workFlowsToUpdate.Remove(localWorkFlow);
                    }

                    this.workFlowsToUpdate.Add(this.SelectedSale);
                }
            });

            Messenger.Default.Register<string>(this, Token.ShowDesktopNotesWindow, s => this.ShowNoteWindow());

            this.UpdateCommmand = new RelayCommand(() =>
            {
                if (this.SelectedSale != null && this.SelectedSale.NouDocStatusID.HasValue)
                {
                    var localWorkFlow = this.workFlowsToUpdate.FirstOrDefault(x => x.SaleID == this.SelectedSale.SaleID);
                    if (localWorkFlow != null)
                    {
                        this.workFlowsToUpdate.Remove(localWorkFlow);
                    }

                    this.workFlowsToUpdate.Add(this.SelectedSale);
                }

                this.SetControlMode(ControlMode.Update);
            });

            #endregion

            #region instantiation

            #endregion

            if (ApplicationSettings.WorkflowSearchFromDate < DateTime.Today.AddYears(-100))
            {
                this.FromDate = DateTime.Today;
            }
            else
            {
                this.FromDate = ApplicationSettings.WorkflowSearchFromDate;
            }

            this.ShowAllOrders = ApplicationSettings.WorkflowSearchShowAllOrders;

            this.ToDate = DateTime.Today;
            this.NouDocStatus = new List<NouDocStatu>
            {
                NouvemGlobal.NouDocStatusActive, 
                NouvemGlobal.NouDocStatusInProgress,
                NouvemGlobal.NouDocStatusComplete, 
                NouvemGlobal.NouDocStatusCancelled
            };

            this.GetWorkflows();
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the doc statuses.
        /// </summary>
        public IList<NouDocStatu> NouDocStatus { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a cell value change.
        /// </summary>
        public ICommand UpdateCommmand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.OK:
                    this.Close();
                    break;

                case ControlMode.Update:
                    if (this.workFlowsToUpdate.Any())
                    {
                        if (this.DataManager.UpdateWorkflowStatus(this.workFlowsToUpdate, this.SelectedSale.NouDocStatusID.ToInt()))
                        {
                            SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                            this.workFlowsToUpdate.Clear();
                            this.Refresh();
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Abstract cancel selection command.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.SelectedSale == null)
            {
                return;
            }

            if (this.ReportMode)
            {
                Messenger.Default.Send(this.SelectedSale, Token.SearchSaleSelectedForReport);
                return;
            }

            var workflow = this.DataManager.GetWorkflowById(this.SelectedSale.SaleID);
            this.Locator.Workflow.HandleSelectedWorkflow(workflow, true);
            if (!this.KeepVisible)
            {
                this.Close();
            }

            Messenger.Default.Send(Token.Message, Token.CreateWorkflow);
        }

        /// <summary>
        /// Refreshes the lairages.
        /// </summary>
        protected override void Refresh()
        {
            if (this.IsFormLoaded)
            {
                this.GetWorkflows();
            }
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            this.ReportMode = false;
            ApplicationSettings.WorkflowSearchFromDate = this.FromDate;
            ApplicationSettings.WorkflowSearchShowAllOrders = this.ShowAllOrders;
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected override void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
            ViewModelLocator.ClearWorkflowSearch();
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        protected override void ShowNotesWindow()
        {
        }

        #endregion

        #region private

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        private void ShowNoteWindow()
        {
            if (this.SelectedSale == null || this.SelectedSale.SaleID == 0)
            {
                return;
            }

            var notes = string.Empty;
            Messenger.Default.Send(notes, Token.DisplayNotes);
        }

        /// <summary>
        /// Retrieves the workflows.
        /// </summary>
        private void GetWorkflows()
        {
            var docStatusesAll = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID,
                NouvemGlobal.NouDocStatusCancelled.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                NouvemGlobal.NouDocStatusExported.NouDocStatusID,
            };

            var docStatusesActive = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
            };

            if (this.ShowAllOrders)
            {
                this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetWorkflowsByDate(docStatusesAll, this.FromDate, this.ToDate));
            }
            else
            {
                this.FilteredSales = new ObservableCollection<Sale>(this.DataManager.GetWorkflowsByDate(docStatusesActive, this.FromDate, this.ToDate));
            }
        }

        #endregion
    }
}



