﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using GalaSoft.MvvmLight;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class WorkflowViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// The current template name.
        /// </summary>
        private string templateName;

        /// <summary>
        /// The response vm.
        /// </summary>
        private ViewModelBase responseViewModel;

        /// <summary>
        /// The current process we are working on.
        /// </summary>
        private AttributeAllocationData currentProcess;

        /// <summary>
        /// Flag, as to whether the ui back arrow is disabled or not.
        /// </summary>
        private bool isBackArrowEnabled;

        /// <summary>
        /// Flag, as to whether the ui back arrow is disabled or not.
        /// </summary>
        private bool isForwardArrowEnabled;

        /// <summary>
        /// The question asked.
        /// </summary>
        private string question;

        /// <summary>
        /// The completion enabled flag.
        /// </summary>
        private bool enableWorkflowCompletion;

        /// <summary>
        /// The current answer.
        /// </summary>
        private string answer;

        /// <summary>
        /// The possible responses.
        /// </summary>
        private ObservableCollection<CollectionData> responses = new ObservableCollection<CollectionData>();

        /// <summary>
        /// The non standard response authorised user groups.
        /// </summary>
        private IList<UserGroupNonStandard> nonStandardUserGroups;

        /// <summary>
        /// The selected response.
        /// </summary>
        private CollectionData selectedResponse;

        /// <summary>
        /// The selected response.
        /// </summary>
        private CollectionData selectedSummaryResponse;

        /// <summary>
        /// The current template.
        /// </summary>
        private AttributeTemplateData currentTemplate;

        /// <summary>
        /// The current question number.
        /// </summary>
        private int questionNumber;

        /// <summary>
        /// The manual weight flag.
        /// </summary>
        private bool manualWeight;

        /// <summary>
        /// The tare weight.
        /// </summary>
        private decimal tare;

        /// <summary>
        /// The linked module id.
        /// </summary>
        private int? linkedModuleId;

        /// <summary>
        /// The workflow number.
        /// </summary>
        private int workflowNo;

        /// <summary>
        /// The workflow number.
        /// </summary>
        private string startDate;

        /// <summary>
        /// Are we processing a nopn standard response.
        /// </summary>
        private bool nonStandardReponseMode;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowViewModel"/> class.
        /// </summary>
        public WorkflowViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register to handle the authorisation result.
            Messenger.Default.Register<string>(this, Token.EnterSelected, this.MoveCommandExecute);

            // Register to handle the authorisation result.
            Messenger.Default.Register<AttributeAllocationData>(this, Token.CreateAttributeResponse, this.HandleNonStandardAttributeResponse);

            // Register to handle the authorisation result.
            Messenger.Default.Register<string>(this, Token.AuthorisationResult, this.HandleNonStandardResponseAuthorisation);

            Messenger.Default.Register<string>(this, Token.TouchscreenKeyPadEntry, s =>
            {
                this.HandleKeyPadEntry(s);
            });

            Messenger.Default.Register<Sale>(this, Token.WorkflowSelected, o =>
            {
                this.HandleSelectedWorkflow(o);
            });

            Messenger.Default.Register<Tuple<AttributeTemplateData, List<AttributeAllocationData>>>(this, Token.TemplateAllocationsSelected, o =>
            {
                this.CloseSelectionScreens();
                this.currentTemplate = o.Item1;
                this.TemplateName = o.Item1.Name;

                var previousWorkflow = this.DataManager.GetLastWorkflowDate(this.currentTemplate.AttributeTemplateID);
                if (previousWorkflow != null)
                {
                    var formattedTime = string.Format("{0:d/M/yyyy HH:mm}", previousWorkflow.ToDate());
                    NouvemMessageBox.Show(string.Format(Message.WorkflowCompletedRecently, formattedTime), NouvemMessageBoxButtons.YesNo, touchScreen: true);
                    if (NouvemMessageBox.UserSelection == UserDialogue.No)
                    {
                        this.TemplateName = string.Empty;
                        return;
                    }
                }

                var data = o.Item2.OrderBy(x => x.AttributionAllocation.Sequence).ToList();
                this.Sale = new Sale
                {
                    TemplateID = o.Item1.AttributeTemplateID,
                    WorkflowData = data
                };

                this.WorkflowProceed(true);
            });

            // Register for the indicator weight data.
            Messenger.Default.Register<string>(this, Token.IndicatorWeight, x =>
            {
                this.manualWeight = false;
                this.RecordWeight();
            });

            // Register for the manual weight data.
            Messenger.Default.Register<Tuple<decimal, bool, bool>>(this, Token.IndicatorWeight, weightData =>
            {
                this.indicatorWeight = weightData.Item1;
                this.manualWeight = true;
                this.RecordWeight();
            });

            // Register for the indicator tare weight.
            Messenger.Default.Register<double>(this, Token.TareSet, tare => this.tare = tare.ToDecimal());

            #endregion

            #region command handler

            this.SelectedResponseCommand = new RelayCommand(this.SelectedResponseCommandExecute);

            this.DisplayFileCommand = new RelayCommand(this.DisplayFileCommandExecute);

            this.OKCommand = new RelayCommand(() =>
            {
                if (string.IsNullOrWhiteSpace(this.Answer))
                {
                    Messenger.Default.Send(Token.Message, Token.NoResponseGiven);
                }
                else
                {
                    Messenger.Default.Unregister(this);
                    this.currentProcess.TraceabilityValueNonStandard = this.Answer;
                    Messenger.Default.Send(this.currentProcess, Token.ResponseGiven);
                }

                this.Close();
            });

            this.MenuCommand = new RelayCommand(() =>
            {
                this.Locator.Menu.AddMenuItems(ViewType.Workflow);
                Messenger.Default.Send(Token.Message, Token.CreateMenu);
            });

            // Handle the view switch.
            this.SwitchViewCommand = new RelayCommand<string>(this.SwitchViewCommandExecute);

            // Handle the view switch.
            this.MoveCommand = new RelayCommand<string>(this.MoveCommandExecute);

            this.MoveBackCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.NoResponseGiven);
                this.Close();
            });

            // Handle the view switch.
            this.CompleteWorkflowCommand = new RelayCommand(this.CompleteWorkflowCommandExecute);

            this.RecordWeightCommand = new RelayCommand(() => this.Locator.Indicator.RecordWeight());

            this.OnClosingCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = false;
                ViewModelLocator.ClearWorkflow();
            });

            #endregion

            this.EnableWorkflowCompletion = true;
            this.GetAttributeLookUps();
            this.GetAlertUsers();
            this.GetNonStandardUserGroups();
            this.TemplateName = Message.SelectNewQC;
            this.IsBackArrowEnabled = !ApplicationSettings.WorkflowDisableBackArrow;
            this.IsForwardArrowEnabled = !ApplicationSettings.WorkflowDisableForwardArrow;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether the ui back arrow is disabled or not.
        /// </summary>
        public bool IsBackArrowEnabled
        {
            get
            {
                return this.isBackArrowEnabled;
            }

            set
            {
                this.isBackArrowEnabled = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui back arrow is disabled or not.
        /// </summary>
        public bool IsForwardArrowEnabled
        {
            get
            {
                return this.isForwardArrowEnabled;
            }

            set
            {
                this.isForwardArrowEnabled = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the workflow no.
        /// </summary>
        public string StartDate
        {
            get
            {
                return this.startDate;
            }

            set
            {
                this.startDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the workflow no.
        /// </summary>
        public int WorkflowNo
        {
            get
            {
                return this.workflowNo;
            }

            set
            {
                this.workflowNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the completion button is enabled.
        /// </summary>
        public bool EnableWorkflowCompletion
        {
            get
            {
                return this.enableWorkflowCompletion;
            }

            set
            {
                this.enableWorkflowCompletion = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the response vm.
        /// </summary>
        public ViewModelBase ResponseViewModel
        {
            get
            {
                return this.responseViewModel;
            }

            set
            {
                this.responseViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the possible responses.
        /// </summary>
        public ObservableCollection<CollectionData> Responses
        {
            get
            {
                return this.responses;
            }

            set
            {
                this.responses = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected response.
        /// </summary>
        public CollectionData SelectedSummaryResponse
        {
            get
            {
                return this.selectedSummaryResponse;
            }

            set
            {
                this.selectedSummaryResponse = value;
                this.RaisePropertyChanged();

                if (value != null && !this.EntitySelectionChange && this.IsBackArrowEnabled)
                {
                    foreach (var attributeAllocationData in this.Sale.WorkflowData)
                    {
                        attributeAllocationData.IsCurrentProcess = false;
                    }

                    var current =
                        this.Sale.WorkflowData.FirstOrDefault(x => x.AttributeMaster.AttributeMasterID == value.ID);
                    if (current != null)
                    {
                        this.WorkflowProceed(true, current);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected response.
        /// </summary>
        public CollectionData SelectedResponse
        {
            get
            {
                return this.selectedResponse;
            }

            set
            {
                this.selectedResponse = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current question.
        /// </summary>
        public string Question
        {
            get
            {
                return this.question;
            }

            set
            {
                this.question = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current question.
        /// </summary>
        public string Answer
        {
            get
            {
                return this.answer;
            }

            set
            {
                this.answer = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current template name.
        /// </summary>
        public string TemplateName
        {
            get
            {
                return this.templateName;
            }

            set
            {
                this.templateName = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Sets a template to select.
        /// </summary>
        /// <param name="id">The template id.</param>
        public void SetTemplate(int id, int moduleId)
        {
            this.linkedModuleId = moduleId;
            this.Locator.WorkflowTemplateSelection.SetTemplate(id);
        }

        /// <summary>
        /// Handles a selected workflow.
        /// </summary>
        /// <param name="o">The workflow to display.</param>
        public void HandleSelectedWorkflow(Sale o, bool displaySummaryScreen = false)
        {
            this.CloseSelectionScreens();
            this.currentTemplate = o.TemplateData;
            this.TemplateName = o.TemplateData.Name;
            this.Sale = o;
            this.WorkflowNo = this.Sale.Number;
            this.StartDate = this.Sale.CreationDate.ToDate().ToString();
            this.EnableWorkflowCompletion = this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
            foreach (var attributeAllocationData in this.Sale.WorkflowData)
            {
                if (attributeAllocationData.Attribute != null)
                {
                    attributeAllocationData.TraceabilityValue = attributeAllocationData.Attribute.Answer;
                }

                if (attributeAllocationData.AttributeNonStandard != null)
                {
                    attributeAllocationData.TraceabilityValueNonStandard =
                   attributeAllocationData.AttributeNonStandard.Answer;
                }
            }

            if (displaySummaryScreen)
            {
                this.ShowSummary();
            }
            else
            {
                this.WorkflowProceed(true);
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a call to display the menu.
        /// </summary>
        public ICommand SelectedResponseCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a call to display the menu.
        /// </summary>
        public ICommand DisplayFileCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a call to display the menu.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a call to display the menu.
        /// </summary>
        public ICommand MenuCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a view change.
        /// </summary>
        public ICommand SwitchViewCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a call to display the menu.
        /// </summary>
        public ICommand OKCommand { get; private set; }

        /// <summary>
        /// Gets the command to move processes.
        /// </summary>
        public ICommand MoveCommand { get; set; }

        /// <summary>
        /// Gets the command to complete the workflow.
        /// </summary>
        public ICommand CompleteWorkflowCommand { get; set; }

        /// <summary>
        /// Gets the command to handle the close event.
        /// </summary>
        public ICommand OnClosingCommand { get; set; }

        /// <summary>
        /// Gets the command to capture a weight.
        /// </summary>
        public ICommand RecordWeightCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Clears the attributes.
        /// </summary>
        protected override void ClearAttributes()
        {
            base.ClearAttributes();
            this.Answer = string.Empty;
            this.Question = string.Empty;
            this.Responses.Clear();
            this.ResponseViewModel = this.Locator.WorkflowCollection;
        }

        /// <summary>
        /// Creates the workflow.
        /// </summary>
        protected void AddWorkflow()
        {
            this.GetLocalDocNumberings();
            this.Sale.TemplateID = this.currentTemplate.AttributeTemplateID;
            this.Sale.Number = this.NextNumber;
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.DocumentNumberingID = this.SelectedDocNumbering.DocumentNumberingID;
            var newWorkFlowId = this.DataManager.AddWorkflow(this.Sale);
            if (newWorkFlowId > 0)
            {
                this.UpdateDocNumbering();
                this.WorkflowNo = this.Sale.Number;
                this.StartDate = string.Format("{0:g}", this.Sale.CreationDate.ToDate());

                SystemMessage.Write(MessageType.Priority, string.Format(Message.WorkflowCreated, this.Sale.Number));
                this.Sale.SaleID = newWorkFlowId;

                // check if the answer is non standard
                this.nonStandardReponseMode = this.IsResponseNonStandard();
                if (this.nonStandardReponseMode)
                {
                    this.NonStandardResponseHandler();
                }
                else
                {
                    this.WorkflowProceed(true);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.WorkflowNotCreated);
            }
        }

        /// <summary>
        /// Updates the workflow.
        /// </summary>
        protected void UpdateWorkflow()
        {
            var attribute = this.Sale.CurrentWorkflowData.Attribute;
            if (this.nonStandardReponseMode && this.Sale.CurrentWorkflowData.AttributeNonStandard != null && !this.currentProcess.AlertsRecorded)
            {
                attribute = this.Sale.CurrentWorkflowData.AttributeNonStandard;
                if (this.currentProcess.AttributeMaster.LogAlertNonStd == true)
                {
                    this.LogAlerts(this.currentProcess, workflowId: this.Sale.SaleID);
                }
            }
            else if (!this.nonStandardReponseMode && this.currentProcess.AlertsRecorded)
            {
                // a correction has been made, so remove the alerts.
                this.DataManager.RemoveUserAlerts(this.Sale.SaleID,
                    this.currentProcess.AttributeMaster.AttributeMasterID);
                this.currentProcess.AlertsRecorded = false;
            }

            if (this.DataManager.UpdateWorkflow(attribute))
            {
                SystemMessage.Write(MessageType.Priority, Message.WorkflowUpdated);

                if (!this.nonStandardReponseMode)
                {
                    // we are in standard mode, so we check that it's a standard answer
                    this.nonStandardReponseMode = this.IsResponseNonStandard();
                }
                else
                {
                    this.nonStandardReponseMode = false;
                }

                if (this.nonStandardReponseMode)
                {
                    // it's not a standard answer
                    this.NonStandardResponseHandler();
                }
                else
                {
                    this.WorkflowProceed(true);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.WorkflowNotUpdated);
            }
        }

        /// <summary>
        /// Close, and cleanup.
        /// </summary>
        protected override void Close()
        {
            Messenger.Default.Unregister(this);
            ViewModelLocator.ClearWorkflow();
            Messenger.Default.Send(Token.Message, Token.CloseWorkflow);
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.Workflow)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Handles the selected date.
        /// </summary>
        protected override void HandleSelectedCalenderDate()
        {
            this.Answer = this.SelectedCalenderDate.ToShortDateString();

            if (this.AutoMoveForward() && !this.EntitySelectionChange)
            {
                this.MoveForward();
            }
        }

        protected override void CopyDocumentTo()
        {
            // not implemented
        }

        protected override void CopyDocumentFrom()
        {
            // not implemented
        }

        protected override void FindSale()
        {
            // not implemented
        }

        protected override void FindSaleCommandExecute()
        {
            // not implemented
        }

        protected override void DirectSearchCommandExecute()
        {
            // not implemented
        }

        protected override void AddSale()
        {
            // not implemented
        }

        protected override void UpdateSale()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            ApplicationSettings.UsingWorkflowForStandardAttributes = false;
            if (this.linkedModuleId == null)
            {
                ViewModelLocator.ClearTouchscreenModules(this);
            }
    
            this.IsFormLoaded = true;
        }

        /// <summary>
        /// Handles the collection screen selection.
        /// </summary>
        private void SelectedResponseCommandExecute()
        {
            if (this.selectedResponse != null)
            {
                this.Answer = this.selectedResponse.Data;

                if (this.AutoMoveForward())
                {
                    this.MoveForward();
                }
            }
        }

        /// <summary>
        /// Handler for the command to display a new file to the db.
        /// </summary>
        private void DisplayFileCommandExecute()
        {
            #region validation

            if (this.SelectedAttachment == null)
            {
                return;
            }

            #endregion

            try
            {
                var file = this.SelectedAttachment.File;
                var fileName = this.SelectedAttachment.FileName;

                var path = Path.Combine(Settings.Default.AtachmentsPath, fileName);
                File.WriteAllBytes(path, file);
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Complete the current workflow.
        /// </summary>
        private void CompleteWorkflowCommandExecute()
        {
            this.CompleteWorkflow(false);
        }

        /// <summary>
        /// Moves processes.
        /// </summary>
        /// <param name="direction">The direction to move.</param>
        private void MoveCommandExecute(string direction)
        {
            if (this.currentProcess == null)
            {
                return;
            }

            if (direction.CompareIgnoringCase("Back"))
            {
                this.WorkflowProceed(false);
                return;
            }

            if (!ApplicationSettings.UsingWorkflowForStandardAttributes)
            {
                this.MoveForward();
            }
        }

        /// <summary>
        /// Switch the view (supplier or orders)
        /// </summary>
        /// <param name="view">The view to switch to.</param>
        private void SwitchViewCommandExecute(string view)
        {
            this.IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (view.Equals("New"))
                    {
                        Messenger.Default.Send(Token.Message, Token.CreateWorkflowTemplateSelection);
                        return;
                    }

                    Messenger.Default.Send(Token.Message, Token.CreateWorkflowSelection);
                });
            }).ContinueWith(task => this.IsBusy = false);
        }

        #endregion

        #region helper

        /// <summary>
        /// Completes the workflow.
        /// </summary>
        private void CompleteWorkflow(bool autoCompleting)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            if (this.Sale.WorkflowData == null)
            {
                return;
            }

            try
            {
                if (!this.ValidateData(
                    this.traceabilityDataCompletion,
                    this.Sale.SaleID,
                    0, 0, 0, 0, this.Locator.ProcessSelection.SelectedProcess.ProcessID,
                    Constant.Workflow))
                {
                    return;
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion

            var workflowId = this.Sale.SaleID;
            foreach (var data in this.Sale.WorkflowData)
            {
                if (string.IsNullOrWhiteSpace(data.TraceabilityValue) && data.AttributionAllocation.RequiredBeforeCompletion)
                {
                    SystemMessage.Write(MessageType.Issue, Message.WorkflowRequiredAnswersMissing);
                    NouvemMessageBox.Show(Message.WorkflowRequiredAnswersMissing, touchScreen: true);
                    return;
                }

                if (data.AttributeNonStandard != null && string.IsNullOrWhiteSpace(data.TraceabilityValueNonStandard) && data.AttributionAllocation.RequiredBeforeCompletion)
                {
                    SystemMessage.Write(MessageType.Issue, Message.WorkflowRequiredAnswersMissing);
                    NouvemMessageBox.Show(Message.WorkflowRequiredAnswersMissing, touchScreen: true);
                    return;
                }
            }

            try
            {
                if (!autoCompleting)
                {
                    NouvemMessageBox.Show(Message.WorkflowCompletionPrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                    if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                    {
                        this.DataManager.CompleteWorkflow(this.Sale);
                        this.ClearAttributes();
                        this.TemplateName = Message.SelectNewQC;
                        SystemMessage.Write(MessageType.Priority, Message.WorkflowCompleted);
                    }
                }
                else
                {
                    this.DataManager.CompleteWorkflow(this.Sale);
                    this.ClearAttributes();
                    this.TemplateName = Message.SelectNewQC;
                    SystemMessage.Write(MessageType.Priority, Message.WorkflowCompleted);
                }
            }
            finally
            {
                try
                {
                    if (this.linkedModuleId.HasValue)
                    {
                        this.DataManager.SetWorkflowModule(this.linkedModuleId.ToInt(), workflowId);
                        Messenger.Default.Send(workflowId, Token.HaccpComplete);
                        this.Close();
                    }
                }
                finally
                {
                    this.linkedModuleId = null;
                }
            }
        }

        /// <summary>
        /// Ensures all selection screen are closed.
        /// </summary>
        private void CloseSelectionScreens()
        {
            Messenger.Default.Send(Token.Message, Token.CloseWorkflowSelection);
        }

        /// <summary>
        /// Records a weight.
        /// </summary>
        private void RecordWeight()
        {
            if (!this.manualWeight)
            {
                var saveWeightResult = this.IndicatorManager.SaveWeight();
                if (saveWeightResult == string.Empty)
                {
                    this.indicatorWeight = this.IndicatorManager.RecordedIndicatorWeight.ToDecimal();
                    this.manualWeight = false;
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, saveWeightResult);
                    NouvemMessageBox.Show(saveWeightResult, touchScreen: true, flashMessage: true, isLeft: true);
                    return;
                }
            }

            this.Answer = this.indicatorWeight.ToString();

            if (this.AutoMoveForward())
            {
                this.MoveForward();
            }
        }

        /// <summary>
        /// Moves to the next process, saving the current data.
        /// </summary>
        private void MoveForward()
        {
            if (this.currentProcess == null)
            {
                return;
            }

            if (this.ResponseViewModel == this.Locator.WorkflowSummary)
            {
                // on summary screen
                return;
            }

            if (string.IsNullOrWhiteSpace(this.Answer) && this.currentProcess.AttributionAllocation.RequiredBeforeContinue)
            {
                SystemMessage.Write(MessageType.Issue, Message.WorkFlowAnswerRequired);
                NouvemMessageBox.Show(Message.WorkFlowAnswerRequired, touchScreen: true);
                return;
            }

            var attribute = new AttributeWorkflow { Answer = this.Answer, AttributeMasterID = this.currentProcess.AttributeMaster.AttributeMasterID };

            if (this.nonStandardReponseMode)
            {
                if (this.currentProcess.AttributeNonStandard != null &&
                    this.currentProcess.AttributeNonStandard.AttributeWorkflowID > 0)
                {
                    attribute.AttributeWorkflowID = this.currentProcess.AttributeNonStandard.AttributeWorkflowID;
                }

                attribute.AttributeID_Parent = this.currentProcess.Attribute.AttributeWorkflowID;
                attribute.AttributeTemplateRecordsID = this.currentProcess.Attribute.AttributeTemplateRecordsID;
                this.currentProcess.AttributeNonStandard = attribute;
                this.currentProcess.TraceabilityValueNonStandard = this.Answer;
            }
            else
            {
                if (this.currentProcess.Attribute != null &&
                    this.currentProcess.Attribute.AttributeWorkflowID > 0)
                {
                    attribute.AttributeWorkflowID = this.currentProcess.Attribute.AttributeWorkflowID;
                }

                this.currentProcess.Attribute = attribute;
                this.currentProcess.TraceabilityValue = this.Answer;
            }

            this.Sale.CurrentWorkflowData = this.currentProcess;
            if (this.Sale.SaleID == 0)
            {
                this.AddWorkflow();
            }
            else
            {
                this.Sale.CurrentWorkflowData.Attribute.AttributeTemplateRecordsID = this.Sale.SaleID;
                this.UpdateWorkflow();
            }
        }

        /// <summary>
        /// Verifies that the answer is a standard response.
        /// </summary>
        private bool IsResponseNonStandard()
        {
            var responseSp = this.currentProcess.AttributeMaster.StandardResponseChecker;
            if (!string.IsNullOrWhiteSpace(responseSp))
            {
                var result = this.DataManager.GetSPResult(responseSp, this.Answer, this.Sale.SaleID);
                this.HandleValidationResponse(result.Item2);
                return !result.Item1;
            }

            return false;
        }

        /// <summary>
        /// Checks that valid authorisation credentials have been supplied before moving on.
        /// </summary>
        /// <param name="result">The authorisation result.</param>
        private void HandleNonStandardResponseAuthorisation(string result)
        {
            if (result.Equals(Constant.Cancel))
            {
                this.nonStandardReponseMode = false;
                NouvemMessageBox.Show(Message.AuthorisedUserGroupCheckCancelled, touchScreen: true);
                return;
            }

            if (result.Equals(Constant.NoAuthorisation))
            {
                this.nonStandardReponseMode = false;
                NouvemMessageBox.Show(Message.InvalidCredentialsForNonStandardResponse, touchScreen: true);
                return;
            }

            this.NonStandardResponseHandler(true);
        }

        /// <summary>
        /// Handler for any non standard responses.
        /// </summary>
        private void NonStandardResponseHandler(bool ignoreAuthorisation = false)
        {
            if (!ignoreAuthorisation)
            {
                var authorisedGroups =
               this.nonStandardUserGroups.Where(
                   x => x.AttributeMasterID == this.currentProcess.AttributeMaster.AttributeMasterID).ToList();
                if (authorisedGroups.Any())
                {
                    // need to check if the user is part of a non standard response authorised group
                    var userGroupId = NouvemGlobal.LoggedInUser.UserMaster.UserGroupID;
                    var groupIds = authorisedGroups.Select(x => x.UserGroupID).ToList();
                    if (!groupIds.Contains(userGroupId))
                    {
                        // need authorisation
                        NouvemMessageBox.Show(Message.AuthorisedUserCredentialsRequired, touchScreen: true);
                        this.Locator.LoginUser.VerifyUser = true;
                        this.Locator.LoginUser.AuthorisedUserGroups = groupIds;
                        Messenger.Default.Send(Token.Message, Token.CreateTouchscreenUsers);
                        return;
                    }
                }
            }

            this.Question = string.Empty;
            this.Answer = string.Empty;

            this.Question = this.currentProcess.AttributeMaster.MessageText_NonStd;
            this.Answer = this.currentProcess.TraceabilityValueNonStandard ?? string.Empty;

            // get the action
            if (this.currentProcess.TraceabilityTypeNonStandard.CompareIgnoringCase(Constant.Collection))
            {
                this.ResponseViewModel = this.Locator.WorkflowCollection;
                var collectionData = this.currentProcess.AttributeMaster.Collection_NonStd.Split(',')
                        .Select(x => x.ToString())
                        .ToList();

                this.Responses.Clear();
                foreach (var response in collectionData)
                {
                    this.Responses.Add(new CollectionData { Data = response });
                }

                return;
            }

            if (this.currentProcess.TraceabilityTypeNonStandard.CompareIgnoringCase(Constant.SQL))
            {
                this.ResponseViewModel = this.Locator.WorkflowCollection;
                var script = this.currentProcess.AttributeMaster.SQL_NonStd;

                this.Responses.Clear();
                if (script != null)
                {
                    var scriptData = this.DataManager.GetData(script).ToList().Select(x => x.ToString()).ToList();
                    if (scriptData.Any())
                    {
                        foreach (var response in scriptData)
                        {
                            this.Responses.Add(new CollectionData { Data = response });
                        }
                    }
                }

                return;
            }

            if (this.currentProcess.TraceabilityTypeNonStandard.CompareIgnoringCase(Constant.ManualEntry))
            {
                this.ResponseViewModel = this.Locator.WorkflowKeyboard;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(this.Answer ?? string.Empty, Token.DisplayWorkflowKeyboard);
                }));

                return;
            }

            if (this.currentProcess.TraceabilityTypeNonStandard.CompareIgnoringCase(Constant.Calender))
            {
                this.ResponseViewModel = this.Locator.WorkflowCalender;
                this.EntitySelectionChange = true;
                var defaultDate = DateTime.Today;
                if (!string.IsNullOrWhiteSpace(this.Answer))
                {
                    var localDate = this.Answer.ToDate();
                    if (localDate.DateDifferenceInMonths(defaultDate) < 1000)
                    {
                        defaultDate = localDate;
                    }
                }

                this.SelectedCalenderDate = defaultDate;
                this.EntitySelectionChange = false;
                this.ShowCalender = true;
                return;
            }

            if (this.currentProcess.TraceabilityTypeNonStandard.CompareIgnoringCase(Constant.Weight))
            {
                this.ResponseViewModel = this.Locator.WorkflowIndicator;
                return;
            }

            if (this.currentProcess.TraceabilityTypeNonStandard.CompareIgnoringCase(Constant.ManualNumericEntry))
            {
                this.ResponseViewModel = this.Locator.WorkflowKeypad;
            }
        }

        /// <summary>
        /// Displays the summary screen.
        /// </summary>
        private void ShowSummary()
        {
            this.ResponseViewModel = this.Locator.WorkflowSummary;
            this.Question = Strings.WorkflowSummary;
            this.Answer = this.Sale.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID ? Message.WorkflowHasBeenCompleted : Strings.WorkflowComplete;
            this.Responses.Clear();
            this.EntitySelectionChange = true;
            foreach (var workflow in this.Sale.WorkflowData)
            {
                var complete = !string.IsNullOrWhiteSpace(workflow.TraceabilityValue);
                var nonStandard = false;
                if (workflow.AttributeNonStandard != null)
                {
                    complete = !string.IsNullOrWhiteSpace(workflow.TraceabilityValueNonStandard);
                    nonStandard = true;
                }

                if (!complete)
                {
                    // not completed -- takes precedence over non standard
                    this.Answer = Strings.SupplyAllAnswers;
                    nonStandard = false;
                }

                this.Responses.Add(new CollectionData
                {
                    Data = string.Format("{0}{1}{2}", workflow.Description, Environment.NewLine, workflow.TraceabilityValue),
                    ID = workflow.AttributeMaster.AttributeMasterID,
                    NonStandard = nonStandard,
                    IsComplete = complete
                });
            }

            this.SelectedSummaryResponse = null;
            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Moves back or forward through the workflow.
        /// </summary>
        /// <param name="forward">The direction to move.</param>
        private void WorkflowProceed(bool forward, AttributeAllocationData summaryProcess = null)
        {
            this.nonStandardReponseMode = false;
            this.ShowCalender = false;
            if (this.Sale.WorkflowData != null && this.Sale.WorkflowData.Any())
            {
                var lastProcess = this.Sale.WorkflowData.FirstOrDefault(x => x.IsCurrentProcess);
                if (lastProcess == null)
                {
                    if (summaryProcess != null)
                    {
                        this.currentProcess = summaryProcess;
                    }
                    else
                    {
                        this.currentProcess = this.Sale.WorkflowData.First();
                    }
                }
                else
                {
                    var pos = this.Sale.WorkflowData.IndexOf(this.currentProcess);
                    if (forward)
                    {
                        if (pos + 1 == this.Sale.WorkflowData.Count)
                        {
                            // end of workflow
                            SystemMessage.Write(MessageType.Priority, Message.EndOfHACCPChecks);
                            this.ShowSummary();

                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                NouvemMessageBox.Show(Message.EndOfHACCPChecks, flashMessage:true);
                            }));


                            //NouvemMessageBox.Show(Message.EndOfWorkflow, NouvemMessageBoxButtons.YesNo, touchScreen:true);
                            //if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                            //{
                            //    this.CompleteWorkflow(true);
                            //}

                            return;
                        }

                        lastProcess.IsCurrentProcess = false;
                        this.currentProcess = this.Sale.WorkflowData.ElementAt(pos + 1);
                    }
                    else
                    {
                        if (pos == 0)
                        {
                            // at the start of workflow
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                SystemMessage.Write(MessageType.Priority, Message.StartOfWorkflow);
                                NouvemMessageBox.Show(Message.StartOfWorkflow, touchScreen: true);
                            }));
                        }
                        else
                        {
                            lastProcess.IsCurrentProcess = false;
                            this.currentProcess = this.Sale.WorkflowData.ElementAt(pos - 1);
                        }
                    }
                }

                this.currentProcess.IsCurrentProcess = true;
                this.ClearAttributes();

                // Set the question
                this.Question = this.currentProcess.AttributeMaster.MessageText;

                var loadMacro = this.currentProcess.AttributeMaster.LoadMACRO;
                if (!string.IsNullOrEmpty(loadMacro))
                {
                    var result = this.DataManager.GetSPResultReturn(loadMacro, string.Empty, this.currentProcess.AttributeMaster.AttributeMasterID);
                    this.Answer = result.Item1 ?? string.Empty;
                }
                else
                {
                    // check for a default answer
                    var defaultValueScript = this.currentProcess.AttributionAllocation.DefaultValueSQL;

                    if (defaultValueScript != null)
                    {
                        var commandData = this.DataManager.GetData(defaultValueScript).ToList().Select(x => x.ToString()).ToList();
                        if (commandData.Any())
                        {
                            this.Answer = commandData.First();
                        }
                    }
                }

                // now check for overriding existing answer
                if (!string.IsNullOrWhiteSpace(this.currentProcess.TraceabilityValue))
                {
                    this.Answer = this.currentProcess.TraceabilityValue;
                }

                // get the action
                if (this.currentProcess.TraceabilityType.CompareIgnoringCase(Constant.Collection))
                {
                    this.ResponseViewModel = this.Locator.WorkflowCollection;
                    var collectionData = this.currentProcess.AttributeMaster.Collection.Split(',')
                            .Select(x => x.ToString())
                            .ToList();

                    foreach (var response in collectionData)
                    {
                        this.Responses.Add(new CollectionData { Data = response });
                    }

                    this.SelectedResponse = null;

                    return;
                }

                if (this.currentProcess.TraceabilityType.CompareIgnoringCase(Constant.SQL))
                {
                    this.ResponseViewModel = this.Locator.WorkflowCollection;
                    var script = this.currentProcess.AttributeMaster.SQL;

                    if (script != null)
                    {
                        var scriptData = this.DataManager.GetData(script).ToList().Select(x => x.ToString()).ToList();
                        if (scriptData.Any())
                        {
                            foreach (var response in scriptData)
                            {
                                this.Responses.Add(new CollectionData { Data = response });
                            }
                        }

                        this.SelectedResponse = null;
                    }

                    return;
                }

                if (this.currentProcess.TraceabilityType.CompareIgnoringCase(Constant.ManualEntry))
                {
                    this.ResponseViewModel = this.Locator.WorkflowKeyboard;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        Messenger.Default.Send(this.Answer ?? string.Empty, Token.DisplayWorkflowKeyboard);
                    }));

                    return;
                }

                if (this.currentProcess.TraceabilityType.CompareIgnoringCase(Constant.Calender))
                {
                    this.ResponseViewModel = this.Locator.WorkflowCalender;
                    this.EntitySelectionChange = true;
                    var defaultDate = DateTime.Today;
                    if (!string.IsNullOrWhiteSpace(this.Answer))
                    {
                        var localDate = this.Answer.ToDate();
                        if (localDate.DateDifferenceInMonths(defaultDate) < 1000)
                        {
                            defaultDate = localDate;
                        }
                    }

                    this.SelectedCalenderDate = defaultDate;
                    this.EntitySelectionChange = false;
                    this.ShowCalender = true;
                    return;
                }

                if (this.currentProcess.TraceabilityType.CompareIgnoringCase(Constant.Weight))
                {
                    this.ResponseViewModel = this.Locator.WorkflowIndicator;
                    return;
                }

                if (this.currentProcess.TraceabilityType.CompareIgnoringCase(Constant.ManualNumericEntry))
                {
                    this.ResponseViewModel = this.Locator.WorkflowKeypad;
                }
            }
        }

        /// <summary>
        /// Handler for the key button selection.
        /// </summary>
        /// <param name="entry">The keypad selection.</param>
        private void HandleKeyPadEntry(string entry)
        {
            if (entry.CompareIgnoringCase("Clear"))
            {
                this.Answer = string.Empty;
                return;
            }

            if (entry.CompareIgnoringCase("Back"))
            {
                if (!string.IsNullOrEmpty(this.Answer) && this.Answer.Length > 0)
                {
                    this.Answer = this.Answer.Substring(0, this.Answer.Length - 1);
                }

                return;
            }

            if (entry.CompareIgnoringCase("-"))
            {
                this.Answer = string.Format("-{0}", this.Answer); return;
            }

            if (entry.CompareIgnoringCase("Enter"))
            {
                if (this.AutoMoveForward())
                {
                    this.MoveForward();
                }

                return;
            }

            this.Answer += entry;
        }

        /// <summary>
        /// Gets the non standard response user groups.
        /// </summary>
        private void GetNonStandardUserGroups()
        {
            this.nonStandardUserGroups = this.DataManager.GetUserGroupsNonStandard();
        }

        private void HandleNonStandardAttributeResponse(AttributeAllocationData data)
        {
            this.currentProcess = data;
            this.NonStandardResponseHandler();
        }

        /// <summary>
        /// Determines if we are to auto move forward on answer selection.
        /// </summary>
        /// <returns>Flag, as to whether we are to auto move forward on answer selection.</returns>
        private bool AutoMoveForward()
        {
            return ApplicationSettings.WorkflowProceedOnSelection &&
                   !ApplicationSettings.UsingWorkflowForStandardAttributes;
        }

        #endregion

        #endregion
    }
}

