﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowAlertsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.ViewModel.Sales;

    public class WorkflowAlertsViewModel : SalesSearchDataViewModel
    {
        #region field

        /// <summary>
        /// The users to check for alerts.
        /// </summary>
        private HashSet<int?> usersToCheck;

        /// <summary>
        /// The alerts to update.
        /// </summary>
        private HashSet<int> alertsToUpdate = new HashSet<int>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowAlertsViewModel"/> class.
        /// </summary>
        public WorkflowAlertsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command registration

            this.ShowUsersCommand = new RelayCommand(() => Messenger.Default.Send(ViewType.AlertUsersSetUp));
            this.MarkAllViewedCommand = new RelayCommand(() =>
            {
                if (this.DataManager.MarkAllUserAlertAsViewed(NouvemGlobal.UserId.ToInt()))
                {
                    NouvemMessageBox.Show(Message.MarkAllAlertsViewedPrompt, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }

                    SystemMessage.Write(MessageType.Priority, Message.AllAlertsMarkedAsViewed);
                    this.GetAlerts();
                    var alerts = this.DataManager.AreThereNewAlerts(this.usersToCheck, ApplicationSettings.DisplayNewAlertsMessageUntilAlertOpened);
                    Messenger.Default.Send(alerts, Token.NewAlerts);
                    return;
                }

                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            });

            this.MarkAllCompleteCommand = new RelayCommand(() =>
            {
                if (this.DataManager.MarkAllUserAlertAsComplete(NouvemGlobal.UserId.ToInt()))
                {
                    NouvemMessageBox.Show(Message.MarkAllAlertsCompletePrompt, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }

                    SystemMessage.Write(MessageType.Priority, Message.AllAlertsMarkedAsCompleted);
                    this.GetAlerts();
                    return;
                }

                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            });

            this.UpdateCommmand = new RelayCommand(() =>
            {
                if (this.SelectedSale != null && this.SelectedSale.NouDocStatusID.HasValue)
                {
                    this.alertsToUpdate.Add(this.SelectedSale.SaleID);
                }

                this.SetControlMode(ControlMode.Update);
            });

            #endregion

            if (ApplicationSettings.AlertsFromDate < DateTime.Today.AddYears(-100))
            {
                this.FromDate = DateTime.Today;
            }
            else
            {
                this.FromDate = ApplicationSettings.AlertsFromDate;
            }

            this.NouDocStatus = new List<NouDocStatu>
            {
                NouvemGlobal.NouDocStatusActive, 
                NouvemGlobal.NouDocStatusComplete, 
                NouvemGlobal.NouDocStatusCancelled
            };

            this.ShowAllOrders = ApplicationSettings.AlertsShowAllOrders;
            this.KeepVisible = ApplicationSettings.AlertsKeepVisible;
            this.ToDate = DateTime.Today;
            this.MarkAlertsRead();
            this.GetAlerts();
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region public

        #region property

        /// <summary>
        /// Gets or sets the doc statuses.
        /// </summary>
        public IList<NouDocStatu> NouDocStatus { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a cell value change.
        /// </summary>
        public ICommand UpdateCommmand { get; private set; }

        /// <summary>
        /// Gets the command to display the users screen.
        /// </summary>
        public ICommand ShowUsersCommand { get; private set; }

        /// <summary>
        /// Gets the command to mark all alerts as viewed.
        /// </summary>
        public ICommand MarkAllViewedCommand { get; private set; }

        /// <summary>
        /// Gets the command to mark all alerts as complete.
        /// </summary>
        public ICommand MarkAllCompleteCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.OK:
                    this.Close();
                    break;

                case ControlMode.Update:
                    if (this.alertsToUpdate.Any())
                    {
                        if (this.DataManager.UpdateAlertsStatus(this.alertsToUpdate, this.SelectedSale.NouDocStatusID.ToInt()))
                        {
                            SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
                            this.alertsToUpdate.Clear();
                            this.Refresh();
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
                        }
                    }

                    break;
            }
        }

        /// <summary>
        /// Abstract cancel selection command.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void SaleSelectedCommandExecute()
        {
            if (this.SelectedSale == null)
            {
                return;
            }

            if (!this.KeepVisible)
            {
                this.Close();
            }

            var alertId = this.SelectedSale.SaleID;
            this.DataManager.MarkAlertAsViewed(alertId);

            if (ApplicationSettings.DisplayNewAlertsMessageUntilAlertOpened)
            {
                var alerts = this.DataManager.AreThereNewAlerts(this.usersToCheck, ApplicationSettings.DisplayNewAlertsMessageUntilAlertOpened);
                Messenger.Default.Send(alerts, Token.NewAlerts);

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.GetAlerts();
                }));
            }

            if (this.SelectedSale.Description == Strings.HACCP)
            {
                var workflow = this.DataManager.GetWorkflowById(this.SelectedSale.BaseDocumentReferenceID.ToInt());
                this.Locator.Workflow.HandleSelectedWorkflow(workflow, true);
                Messenger.Default.Send(Token.Message, Token.CreateWorkflow);
            }
            else if (this.SelectedSale.Description == Strings.GoodsIn)
            {
                Messenger.Default.Send(ViewType.APReceipt);
                this.Locator.APReceipt.Sale = this.DataManager.GetAPReceiptFullById(this.SelectedSale.BaseDocumentReferenceID.ToInt());
                return;

                this.Locator.Touchscreen.SetModule(ViewType.APReceipt);
                this.Locator.TouchscreenOrders.SetModule(ViewType.APReceipt);
                ViewModelLocator.TouchscreenStatic.Module = Strings.Intake;
                SystemMessage.Write(MessageType.Background, Message.ReceiptScreenLoaded);

                var sale = this.DataManager.GetTouchscreenReceiptById(this.SelectedSale.BaseDocumentReferenceID.ToInt());
                ViewBusinessPartner localSupplier = null;
                var localPartner =
                        NouvemGlobal.SupplierPartners.FirstOrDefault(
                            x => x.Details.BPMasterID == sale.BPCustomer.BPMasterID);

                if (localPartner != null)
                {
                    localSupplier = localPartner.Details;
                }

                this.Locator.APReceiptTouchscreen.HandleSelectedOrder(Tuple.Create(sale, localSupplier));
                Messenger.Default.Send(ViewType.FactoryTouchScreen);
            }
            else if (this.SelectedSale.Description == Strings.Production)
            {
                this.Locator.Touchscreen.SetModule(ViewType.IntoProduction);
                this.Locator.TouchscreenProductionOrders.SetModule(ViewType.IntoProduction);
                this.Locator.TouchscreenProductionOrders.SelectedOrder =
                    this.Locator.TouchscreenProductionOrders.AllOrders.FirstOrDefault(x =>
                        x.Order.PROrderID == this.SelectedSale.BaseDocumentReferenceID);
                this.Locator.TouchscreenProductionOrders.HandleSelectedOrder();

                ViewModelLocator.TouchscreenStatic.Module = Strings.IntoProduction;
                Messenger.Default.Send(ViewType.FactoryTouchScreen);
            }
            else if (this.SelectedSale.Description == Strings.Dispatch)
            {
                Messenger.Default.Send(ViewType.ARDispatch);
                this.Locator.ARDispatch.Sale = this.DataManager.GetARDispatchById(this.SelectedSale.BaseDocumentReferenceID.ToInt());
                return;

                this.Locator.Touchscreen.SetModule(ViewType.ARDispatch);
                var sale = this.DataManager.GetARDispatchById(this.SelectedSale.BaseDocumentReferenceID.ToInt());
                ViewBusinessPartner localCustomer = null;
                var localPartner =
                    NouvemGlobal.CustomerPartners.FirstOrDefault(
                        x => x.Details.BPMasterID == sale.BPCustomer.BPMasterID);

                if (localPartner != null)
                {
                    localCustomer = localPartner.Details;
                }

                this.Locator.ARDispatchTouchscreen.HandleSelectedOrder(Tuple.Create(sale, localCustomer));
                Messenger.Default.Send(ViewType.FactoryTouchScreen);
                SystemMessage.Write(MessageType.Priority, Message.DispatchScreenLoaded);
            }
        }

        /// <summary>
        /// Refreshes the lairages.
        /// </summary>
        protected override void Refresh()
        {
            if (this.IsFormLoaded)
            {
                this.GetAlerts();
            }
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            ApplicationSettings.AlertsFromDate = this.FromDate;
            ApplicationSettings.AlertsShowAllOrders = this.ShowAllOrders;
            ApplicationSettings.AlertsKeepVisible = this.keepVisible;
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected override void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
            ViewModelLocator.ClearWorkflowAlerts();
        }

        #endregion

        #region private

        /// <summary>
        /// Marks the alerts are read.
        /// </summary>
        private void MarkAlertsRead()
        {
            this.usersToCheck = new HashSet<int?>();
            this.usersToCheck.Add(NouvemGlobal.UserId);
            var userAlerts = this.DataManager.GetUserLookUps(NouvemGlobal.UserId.ToInt());
            foreach (var userAlertsLookUp in userAlerts)
            {
                if (!userAlertsLookUp.IsGroup)
                {
                    usersToCheck.Add(userAlertsLookUp.UserOrGroupID);
                }
                else
                {
                    var localUsers =
                        NouvemGlobal.Users.Where(x => x.UserMaster.UserGroupID == userAlertsLookUp.UserOrGroupID);
                    foreach (var localUser in localUsers)
                    {
                        this.usersToCheck.Add(localUser.UserMaster.UserMasterID);
                    }
                }
            }

            var markAlertsAsRead = this.DataManager.MarkAlertsAsRead(this.usersToCheck);
            if (!ApplicationSettings.DisplayNewAlertsMessageUntilAlertOpened && markAlertsAsRead)
            {
                Messenger.Default.Send(false, Token.NewAlerts);
            }
        }

        /// <summary>
        /// Retrieves the workflows.
        /// </summary>
        private void GetAlerts()
        {
            var docStatusesAll = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                NouvemGlobal.NouDocStatusCancelled.NouDocStatusID,
                NouvemGlobal.NouDocStatusComplete.NouDocStatusID
            };

            var docStatusesActive = new List<int?>
            {
                NouvemGlobal.NouDocStatusActive.NouDocStatusID
            };

            IList<Sale> alerts = null;
            if (this.ShowAllOrders)
            {
                alerts = this.DataManager.GetAlerts(docStatusesAll, this.FromDate, this.ToDate);
            }
            else
            {
                alerts = this.DataManager.GetAlerts(docStatusesActive, this.FromDate, this.ToDate);
            }

            if (ApplicationSettings.ShowAlertsForAllUsers)
            {
                this.FilteredSales = new ObservableCollection<Sale>(alerts);
            }
            else
            {
                this.FilteredSales = new ObservableCollection<Sale>(alerts.Where(x => this.usersToCheck.Contains(x.UserIDCreation)));
            }
        }

        #endregion
    }
}
