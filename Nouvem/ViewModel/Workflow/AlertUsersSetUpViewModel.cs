﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Workflow
{
    public class AlertUsersSetUpViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The users.
        /// </summary>
        private ObservableCollection<AllUser> users = new ObservableCollection<AllUser>();

        /// <summary>
        /// The user groups.
        /// </summary>
        private ObservableCollection<AllUser> userGroups = new ObservableCollection<AllUser>();

        /// <summary>
        /// The all user alerts flag.
        /// </summary>
        private bool showAlertsForAllUsers;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AlertUsersSetUpViewModel"/> class.
        /// </summary>
        public AlertUsersSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.UserOrGroupSelected, s =>
            {
                if (!this.EntitySelectionChange)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            });

            #endregion

            #region command handler

            this.OnClosingCommand = new RelayCommand(() =>
            {
                ApplicationSettings.ShowAlertsForAllUsers = this.ShowAlertsForAllUsers;
            });

            #endregion

            this.HasWriteAuthorisation = true;
            this.GetUsersAndGroups();
            this.ShowAlertsForAllUsers = ApplicationSettings.ShowAlertsForAllUsers;
        }

        #endregion

        #region public

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether alerts are diplayed locally for all users.
        /// </summary>
        public bool ShowAlertsForAllUsers
        {
            get
            {
                return this.showAlertsForAllUsers;
            }

            set
            {
                this.showAlertsForAllUsers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public ObservableCollection<AllUser> Users
        {
            get
            {
                return this.users;
            }

            set
            {
                this.users = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user groups.
        /// </summary>
        public ObservableCollection<AllUser> UserGroups
        {
            get
            {
                return this.userGroups;
            }

            set
            {
                this.userGroups = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the closing event.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            if (this.CurrentMode == ControlMode.Find || this.CurrentMode == ControlMode.Add)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Handles the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateUsers();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// handles the control cancel process.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the users/groups.
        /// </summary>
        private void GetUsersAndGroups()
        {
            this.EntitySelectionChange = true;
            this.Users.Clear();
            this.UserGroups.Clear();
            var lookUps = this.DataManager.GetUserLookUps(NouvemGlobal.UserId.ToInt());
            var userLookUps = lookUps.Where(x => !x.IsGroup).ToList();
            var userGroupLookUps = lookUps.Where(x => x.IsGroup).ToList();
            var dbGroups = this.DataManager.GetUserGroups().OrderBy(x => x.Description).ToList();
            foreach (var group in dbGroups)
            {
                var userGroup = new AllUser
                {
                    Id = group.UserGroupID,
                    Name = group.Description,
                    IsSelected = userGroupLookUps.Any(x => x.UserOrGroupID == group.UserGroupID)
                };

                this.UserGroups.Add(userGroup);
            }

            var localUsers = NouvemGlobal.Users.OrderBy(x => x.UserMaster.FullName);
            foreach (var user in localUsers)
            {
                var localUser = new AllUser
                {
                    Id = user.UserMaster.UserMasterID,
                    Name = user.UserMaster.FullName,
                    IsSelected = userLookUps.Any(x => x.UserOrGroupID == user.UserMaster.UserMasterID) || user.UserMaster.UserMasterID == NouvemGlobal.UserId
                };

                this.Users.Add(localUser);
            }

            this.EntitySelectionChange = false;
        }

        /// <summary>
        /// Updates the users.
        /// </summary>
        private void UpdateUsers()
        {
            var updates = new List<UserAlertsLookUp>();
            var localUserGroups = this.UserGroups.Where(x => x.IsSelected);
            var localUsers = this.Users.Where(x => x.IsSelected);
            foreach (var userGroup in localUserGroups)
            {
                updates.Add(new UserAlertsLookUp
                {
                    UserMasterID = NouvemGlobal.UserId.ToInt(),
                    UserOrGroupID = userGroup.Id,
                    IsGroup = true
                });
            }

            foreach (var user in localUsers)
            {
                updates.Add(new UserAlertsLookUp
                {
                    UserMasterID = NouvemGlobal.UserId.ToInt(),
                    UserOrGroupID = user.Id,
                    IsGroup = false
                });
            }

            if (this.DataManager.UpdateUserAlertsSetUp(updates, NouvemGlobal.UserId.ToInt()))
            {
                SystemMessage.Write(MessageType.Priority, Message.DataUpdateSuccess);
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.DataUpdateError);
            }
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseAlertUserSetUpWindow);
            ViewModelLocator.ClearAlertUsersSetUp();
        }

        #endregion
    }
}

