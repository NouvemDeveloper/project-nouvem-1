﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowSelectionViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;

    public class WorkflowSelectionViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected template name.
        /// </summary>
        private Sale selectedWorkflow;

        /// <summary>
        /// The workflows.
        /// </summary>
        private ObservableCollection<Sale> workflows = new ObservableCollection<Sale>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowSelectionViewModel"/> class.
        /// </summary>
        public WorkflowSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.WorkflowSelectedCommand = new RelayCommand(() =>
            {
                if (this.selectedWorkflow != null)
                {
                    this.HandleSelectedWorkflow();
                }
            });

            // Handle the view switch.
            this.MoveBackCommand = new RelayCommand(this.Close);

            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the ui group templates.
        /// </summary>
        public ObservableCollection<Sale> Workflows
        {
            get
            {
                return this.workflows;
            }

            set
            {
                this.workflows = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected group.
        /// </summary>
        public Sale SelectedWorkflow
        {
            get
            {
                return this.selectedWorkflow;
            }

            set
            {
                this.selectedWorkflow = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a selection.
        /// </summary>
        public ICommand WorkflowSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main ui.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main ui.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected void OnLoadingCommandExecute()
        {
            this.GetWorkflows();
        }

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution



        #endregion

        #region helper

        /// <summary>
        /// Handles a workflow selection.
        /// </summary>
        private void HandleSelectedWorkflow()
        {
            if (this.SelectedWorkflow == null)
            {
                return;
            }

            var workflow = this.DataManager.GetWorkflowById(this.SelectedWorkflow.SaleID);
            this.Close();
            Messenger.Default.Send(workflow, Token.WorkflowSelected);
        }

        /// <summary>
        /// Gets the workflows.
        /// </summary>
        private void GetWorkflows()
        {
            this.Workflows = new ObservableCollection<Sale>(
                this.DataManager.GetWorkflows(new List<int?> { 
                    NouvemGlobal.NouDocStatusActive.NouDocStatusID, 
                    NouvemGlobal.NouDocStatusInProgress.NouDocStatusID }).OrderByDescending(x => x.CreationDate));
        }

        /// <summary>
        /// Closes the screen.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseWorkflowSelection);
        }

        #endregion

        #endregion
    }
}



