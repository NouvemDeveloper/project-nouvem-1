﻿// -----------------------------------------------------------------------
// <copyright file="WorkflowTemplateSelectionViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;

    public class WorkflowTemplateSelectionViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected group.
        /// </summary>
        private AttributeTemplateGroup selectedTemplateGroup;

        /// <summary>
        /// The selected template name.
        /// </summary>
        private AttributeTemplateData selectedTemplateName;

        /// <summary>
        /// The templates.
        /// </summary>
        private IList<AttributeTemplateData> templates;

        /// <summary>
        /// The ui group templates.
        /// </summary>
        private ObservableCollection<AttributeTemplateData> templateNames = new ObservableCollection<AttributeTemplateData>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkflowTemplateSelectionViewModel"/> class.
        /// </summary>
        public WorkflowTemplateSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.TemplateSelectedCommand = new RelayCommand(() =>
            {
                if (this.selectedTemplateName != null)
                {
                    this.HandleSelectedTemplate();
                }
            });

            // Handle the view switch.
            this.MoveBackCommand = new RelayCommand(this.Close);

            this.OnLoadingCommand = new RelayCommand(() =>
            {
                this.SelectedTemplateGroup = null;
                this.TemplateNames.Clear();
            });

            #endregion

            this.GetTemplates();
            this.GetGroups();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the ui group templates.
        /// </summary>
        public ObservableCollection<AttributeTemplateData> TemplateNames
        {
            get
            {
                return this.templateNames;
            }

            set
            {
                this.templateNames = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected group.
        /// </summary>
        public AttributeTemplateData SelectedTemplateName
        {
            get
            {
                return this.selectedTemplateName;
            }

            set
            {
                this.selectedTemplateName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected group.
        /// </summary>
        public AttributeTemplateGroup SelectedTemplateGroup
        {
            get
            {
                return this.selectedTemplateGroup;
            }

            set
            {
                this.selectedTemplateGroup = value;
                this.RaisePropertyChanged();
                this.HandleSelectedGroup();
            }
        }

        /// <summary>
        /// Gets or sets the template groups.
        /// </summary>
        public IList<AttributeTemplateGroup> TemplateGroups { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a selection.
        /// </summary>
        public ICommand TemplateSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main ui.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main ui.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Sets a template to select.
        /// </summary>
        /// <param name="id">The template id.</param>
        public void SetTemplate(int id)
        {
            this.SelectedTemplateName =
                this.templates.FirstOrDefault(x =>
                    x.AttributeTemplateID == id);
            if (this.SelectedTemplateName != null)
            {
                this.HandleSelectedTemplate();
            }
        }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution



        #endregion

        #region helper

        /// <summary>
        /// Handles the template selection.
        /// </summary>
        private void HandleSelectedTemplate()
        {
            var data = this.DataManager.GetAttributeAllocationData(this.selectedTemplateName.AttributeTemplateID);
            var localTemplate = this.SelectedTemplateName;
            var dataToSend = Tuple.Create(localTemplate, data.ToList());
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Messenger.Default.Send(dataToSend, Token.TemplateAllocationsSelected);
            }));

            this.Close();
        }

        /// <summary>
        /// Gets the groups.
        /// </summary>
        private void GetGroups()
        {
            this.TemplateGroups = this.DataManager.GetTemplateGroups();
            this.SelectedTemplateGroup = null;
        }

        /// <summary>
        /// Gets the templates.
        /// </summary>
        private void GetTemplates()
        {
            this.templates = this.DataManager.GetTemplateNames().Where(x => x.IsWorkflow && x.Inactive != true).ToList();
        }

        /// <summary>
        /// Handles the selected group template.
        /// </summary>
        private void HandleSelectedGroup()
        {
            this.TemplateNames.Clear();
            if (this.SelectedTemplateGroup == null)
            {
                return;
            }

            var localTemplates =
                this.templates.Where(
                    x => x.AttributeTemplateGroupID == this.SelectedTemplateGroup.AttributeTemplateGroupID);
            foreach (var attributeTemplateData in localTemplates)
            {
                this.TemplateNames.Add(attributeTemplateData);
            }
        }

        /// <summary>
        /// Closes the screen.
        /// </summary>
        private void Close()
        {
            Messenger.Default.Send(Token.Message, Token.CloseWorkflowSelection);
        }

        #endregion

        #endregion
    }
}

