﻿// -----------------------------------------------------------------------
// <copyright file="BPViewModelBase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using ThicknessConverter = Xceed.Wpf.DataGrid.Converters.ThicknessConverter;

namespace Nouvem.ViewModel.BusinessPartner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Shared;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public abstract class BPViewModelBase : NouvemViewModelBase
    {
        #region field

        #region master business partner

        /// <summary>
        /// The selected partner reference.
        /// </summary>
        private BusinessPartner selectedBusinessPartner;

        /// <summary>
        /// The business partners reference.
        /// </summary>
        private ObservableCollection<BusinessPartner> businessPartners;

        /// <summary>
        /// The partner groups collection.
        /// </summary>
        private ObservableCollection<BPGroup> businessPartnerGroups;

        /// <summary>
        /// The partner types collection.
        /// </summary>
        private ObservableCollection<NouBPType> businessPartnerTypes;

        /// <summary>
        /// The partner currencies collection.
        /// </summary>
        private ObservableCollection<BPCurrency> businessPartnerCurrencies;

        /// <summary>
        /// The routes collection.
        /// </summary>
        private ObservableCollection<Route> routes;

        /// <summary>
        /// active box checked flag.
        /// </summary>
        private bool checkBoxActive;

        /// <summary>
        /// inactive box checked flag.
        /// </summary>
        private bool checkBoxInactive;

        /// <summary>
        /// The selected route collection.
        /// </summary>
        private Route selectedRoute;

        /// <summary>
        /// The partner code.
        /// </summary>
        private string partnerCode;

        /// <summary>
        /// The partner name.
        /// </summary>
        private string partnerName;

        /// <summary>
        /// The partner vat no.
        /// </summary>
        private string partnerVATNo;

        /// <summary>
        /// The partner company no.
        /// </summary>
        private string partnerCompanyNo;

        /// <summary>
        /// The partner remarks.
        /// </summary>
        private string partnerRemarks;

        /// <summary>
        /// The partner tel.
        /// </summary>
        private string partnerTel;

        /// <summary>
        /// The partner fax.
        /// </summary>
        private string partnerFax;

        /// <summary>
        /// The partner mobile.
        /// </summary>
        private string partnerMobile;

        /// <summary>
        /// The partner website.
        /// </summary>
        private string partnerWeb;

        /// <summary>
        /// the partner notes.
        /// </summary>
        private string partnerNotes;

        /// <summary>
        /// the partner invoice notes.
        /// </summary>
        private string invoiceNote;

        /// <summary>
        /// the partner notes.
        /// </summary>
        private string popUpNote;

        /// <summary>
        /// The partner main contact name.
        /// </summary>
        private string partnerMainContactName;

        /// <summary>
        /// The partner main contact mobile.
        /// </summary>
        private string partnerMainContactMobile;

        /// <summary>
        /// The partner main contact email.
        /// </summary>
        private string partnerMainContactEmail;

        /// <summary>
        /// The partner main contact direct line.
        /// </summary>
        private string partnerMainContactDirectLine;

        /// <summary>
        /// The partner credit limit.
        /// </summary>
        private int partnerCreditLimit;

        /// <summary>
        /// The partner account balance.
        /// </summary>
        private decimal partnerAccountBalance;

        /// <summary>
        /// The partner limit.
        /// </summary>
        private decimal? partnerOrderLimit;

        /// <summary>
        /// The partner uplift.
        /// </summary>
        private int partnerUplift;

        /// <summary>
        /// The partner po required value.
        /// </summary>
        private bool partnerPORequired;

        /// <summary>
        /// The partner on hold value.
        /// </summary>
        private bool partnerOnHold;

        /// <summary>
        /// The partner on hold value.
        /// </summary>
        private bool insurance;

        /// <summary>
        /// The partner on hold value.
        /// </summary>
        private bool ifaLevy;

        /// <summary>
        /// The partner on hold value.
        /// </summary>
        private bool scrapie;

        /// <summary>
        /// The partner active from date.
        /// </summary>
        private DateTime? partnerActiveFromDate;

        /// <summary>
        /// The partner active to date.
        /// </summary>
        private DateTime? partnerActiveToDate;

        /// <summary>
        /// The partner inactive from date.
        /// </summary>
        private DateTime? partnerInactiveFromDate;

        /// <summary>
        /// The partner inactive from date.
        /// </summary>
        private DateTime? partnerInactiveToDate;

        /// <summary>
        /// The partner type.
        /// </summary>
        private NouBPType partnerType;

        /// <summary>
        /// The partner currency.
        /// </summary>
        private BPCurrency partnerCurrency;

        /// <summary>
        /// The display currency.
        /// </summary>
        private BPCurrency displayCurrency;

        /// <summary>
        /// The partner type.
        /// </summary>
        private BPGroup partnerGroup;

        /// <summary>
        /// The selected tab item view in focus.
        /// </summary>
        private SelectedView selectedPartnerView;

        /// <summary>
        /// A snapshot of the selected partner reference.
        /// </summary>
        protected BusinessPartner selectedBusinessPartnerSnapshot;

        /// <summary>
        /// The searching mode flag.
        /// </summary>
        private bool searchMode;

        #endregion

        #region selected contact

        /// <summary>
        /// The selected partner contact.
        /// </summary>
        private BusinessPartnerContact selectedContact;

        /// <summary>
        /// The selected contact id.
        /// </summary>
        private int selectedContactId;

        /// <summary>
        /// The selected contact title.
        /// </summary>
        private string selectedContactTitle;

        /// <summary>
        /// The selected contact first name.
        /// </summary>
        private string selectedContactFirstName;

        /// <summary>
        /// The selected contact last name.
        /// </summary>
        private string selectedContactLastName;

        /// <summary>
        /// The selected contact job title.
        /// </summary>
        private string selectedContactJobTitle;

        /// <summary>
        /// The selected contact mobile.
        /// </summary>
        private string selectedContactMobile;

        /// <summary>
        /// The selected contact email.
        /// </summary>
        private string selectedContactEmail;

        /// <summary>
        /// The selected contact tel.
        /// </summary>
        private string selectedContactDirectLine;

        /// <summary>
        /// The contact primary value.
        /// </summary>
        private bool selectedContactIsPrimary;

        #endregion

        #region selected address

        /// <summary>
        /// The partner selected address.
        /// </summary>
        private BusinessPartnerAddress selectedAddress;

        /// <summary>
        /// The selected address id.
        /// </summary>
        private int selectedAddressId;

        /// <summary>
        /// The selected address line 1.
        /// </summary>
        private string selectedAddressLine1;

        /// <summary>
        /// The selected address line 1.
        /// </summary>
        private string gln;

        /// <summary>
        /// The selected address line 2.
        /// </summary>
        private string selectedAddressLine2;

        /// <summary>
        /// The selected address line 3.
        /// </summary>
        private string selectedAddressLine3;

        /// <summary>
        /// The selected address line 4.
        /// </summary>
        private string selectedAddressLine4;

        /// <summary>
        /// The selected address line 5.
        /// </summary>
        private string selectedAddressLine5;

        /// <summary>
        /// The selected address postcode.
        /// </summary>
        private string selectedAddressPostcode;

        /// <summary>
        /// The selected address is shipping value.
        /// </summary>
        private bool selectedAddressIsShipping;

        /// <summary>
        /// The selected address is billing value.
        /// </summary>
        private bool selectedAddressIsBilling;

        /// <summary>
        /// The selected address plant name.
        /// </summary>
        private Model.DataLayer.Plant selectedPlant;

        /// <summary>
        /// The selected address plant code.
        /// </summary>
        private string selectedAddressPlantCode;

        /// <summary>
        /// The selected address plant country.
        /// </summary>
        private string selectedAddressPlantCountry;

        #endregion

        #region herds

        /// <summary>
        /// The selected herd.
        /// </summary>
        private SupplierHerd selectedPartnerHerd;

        /// <summary>
        /// The selected herd.
        /// </summary>
        private ObservableCollection<SupplierHerd> selectedPartnerHerds;

        /// <summary>
        /// Is the herd beef flag.
        /// </summary>
        private bool isBeefHerd;

        /// <summary>
        /// Is the herd sheep flag.
        /// </summary>
        private bool isSheepHerd;

        /// <summary>
        /// The herd no.
        /// </summary>
        private string herdNumber;

        /// <summary>
        /// Is a farmer flag.
        /// </summary>
        private bool isLivestockSupplier;

        #endregion

        #region selected attachment

        /// <summary>
        /// The partner selected attachment
        /// </summary>
        private BPAttachment selectedAttachment;

        #endregion

        #region property

        /// <summary>
        /// The property selected field.
        /// </summary>
        private BusinessPartnerProperty selectedProperty;

        #endregion

        #region payment

        /// <summary>
        /// The partner limit.
        /// </summary>
        private decimal? partnerDeliveryChargeThreshold;

        /// <summary>
        /// The selected partner price list.
        /// </summary>
        private PriceList selectedPriceList;

        /// <summary>
        /// The price lists.
        /// </summary>
        private ObservableCollection<PriceList> priceLists;

        #endregion

        #region label detail

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelTextName;

        /// <summary>
        /// The multi label text.
        /// </summary>
        private string labelMultiText1;

        /// <summary>
        /// The multi label text.
        /// </summary>
        private string labelMultiText2;

        /// <summary>
        /// The barcode text.
        /// </summary>
        private string barcode1;

        /// <summary>
        /// The barcode text.
        /// </summary>
        private string barcode2;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText1;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText2;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText3;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText4;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText5;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText6;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText7;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText8;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText9;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText10;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText11;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText12;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText13;

        /// <summary>
        /// The label text.
        /// </summary>
        private string labelText14;

        /// <summary>
        /// The label text name.
        /// </summary>
        private string labelText15;

        #endregion

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BPViewModelBase"/> class.
        /// </summary>
        protected BPViewModelBase()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration


           
            #endregion

            #region command registration


            #endregion

            #region instantiation

            this.BusinessPartnerCurrencies = new ObservableCollection<BPCurrency>();
            this.BusinessPartnerGroups = new ObservableCollection<BPGroup>();
            this.BusinessPartnerTypes = new ObservableCollection<NouBPType>();
            this.BusinessPartnerContacts = new ObservableCollection<BusinessPartnerContact>();
            this.BusinessPartnerAddresses = new ObservableCollection<BusinessPartnerAddress>();
            this.BusinessPartnerAttachments = new ObservableCollection<BPAttachment>();
            this.BPProperties = new ObservableCollection<BusinessPartnerProperty>();
            this.SelectedProperties = new ObservableCollection<BPProperty>();
            this.selectedContact = new BusinessPartnerContact();
            this.selectedAddress = new BusinessPartnerAddress();
            this.Plants = new ObservableCollection<Model.DataLayer.Plant>();
            this.PriceLists = new ObservableCollection<PriceList>();
        
            #endregion

            NouvemGlobal.BusinessPartnerProperties = this.DataManager.GetProperties();
            NouvemGlobal.BusinessPartnerPropertySelections = this.DataManager.GetPropertySelection();
            this.CheckForNewEntities(EntityType.BusinessPartner);
            this.GetBusinessPartners();
            this.GetPlants();
            this.GetPriceLists();
            this.GetRoutes();
            this.BusinessPartnerContacts.Add(new BusinessPartnerContact { Details = new BPContact{ FirstName = Strings.DefineNew }});
            this.BusinessPartnerAddresses.Add(new BusinessPartnerAddress
            {
                Details = new BPAddress { AddressLine1 = Strings.DefineNew }, 
                PlantDetails = new Model.DataLayer.Plant(),
                PlantCountry = new Country()
            });

            this.SetLabelNames();
            this.GetProcesses();
        }

        #endregion

        #region enum

        /// <summary>
        /// Enumeration that models the selected tab item view.
        /// </summary>
        public enum SelectedView
        {
            /// <summary>
            /// The general tab item.
            /// </summary>
            General,

            /// <summary>
            /// The contacts tab item.
            /// </summary>
            Contacts,

            /// <summary>
            /// The addresses tab item.
            /// </summary>
            Addresses,

            /// <summary>
            /// The payment terms tab item.
            /// </summary>
            PaymentTerms,

            /// <summary>
            /// The properties tab item.
            /// </summary>
            Properties,

            /// <summary>
            /// The remarks tab item.
            /// </summary>
            Remarks,

            /// <summary>
            /// The attachments tab item.
            /// </summary>
            Attachments
        }

        #endregion

        #region public interface

        #region property

        #region master business partner

        /// <summary>
        /// Gets or sets the partner code.
        /// </summary>
        public string PartnerCode
        {
            get
            {
                return this.partnerCode;
            }

            set
            {
                this.SetMode(value, this.partnerCode);
                this.partnerCode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner name.
        /// </summary>
        public string PartnerName
        {
            get
            {
                return this.partnerName;
            }

            set
            {
                this.SetMode(value, this.partnerName);
                this.partnerName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the inactive chaeck box is checked.
        /// </summary>
        public bool CheckBoxInactive
        {
            get
            {
                return this.checkBoxInactive;
            }

            set
            {
                this.SetMode(value, this.checkBoxInactive);
                this.checkBoxInactive = value;
                this.RaisePropertyChanged();

                if (value && this.SelectedBusinessPartner != null && !this.PartnerInactiveFromDate.HasValue &&
                    !this.PartnerInactiveToDate.HasValue)
                {
                    this.PartnerInactiveFromDate = DateTime.Today;
                    this.PartnerInactiveToDate = DateTime.Today.AddYears(100);
                }
                else if (!value)
                {
                    this.PartnerInactiveFromDate = null;
                    this.PartnerInactiveToDate = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the active chaeck box is checked.
        /// </summary>
        public bool CheckBoxActive
        {
            get
            {
                return this.checkBoxActive;
            }

            set
            {
                this.SetMode(value, this.checkBoxActive);
                this.checkBoxActive = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner company no.
        /// </summary>
        public string PartnerCompanyNo
        {
            get
            {
                return this.partnerCompanyNo;
            }

            set
            {
                this.SetMode(value, this.partnerCompanyNo);
                this.partnerCompanyNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner vat no.
        /// </summary>
        public string PartnerVATNo
        {
            get
            {
                return this.partnerVATNo;
            }

            set
            {
                this.SetMode(value, this.partnerVATNo);
                this.partnerVATNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner phone.
        /// </summary>
        public string PartnerTel
        {
            get
            {
                return this.partnerTel;
            }

            set
            {
                this.SetMode(value, this.partnerTel);
                this.partnerTel = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the partner remarks.
        /// </summary>
        public string PartnerRemarks
        {
            get
            {
                return this.partnerRemarks;
            }

            set
            {
                this.SetMode(value, this.partnerRemarks);
                this.partnerRemarks = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner notes.
        /// </summary>
        public string PartnerNotes
        {
            get
            {
                return this.partnerNotes;
            }

            set
            {
                this.SetMode(value, this.partnerNotes);
                this.partnerNotes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner invoice notes.
        /// </summary>
        public string InvoiceNote
        {
            get
            {
                return this.invoiceNote;
            }

            set
            {
                this.SetMode(value, this.invoiceNote);
                this.invoiceNote = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner pop up notes.
        /// </summary>
        public string PopUpNote
        {
            get
            {
                return this.popUpNote;
            }

            set
            {
                this.SetMode(value, this.popUpNote);
                this.popUpNote = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner fax.
        /// </summary>
        public string PartnerFax
        {
            get
            {
                return this.partnerFax;
            }

            set
            {
                this.SetMode(value, this.partnerFax);
                this.partnerFax = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner mobile.
        /// </summary>
        public string PartnerMobile
        {
            get
            {
                return this.partnerMobile;
            }

            set
            {
                this.SetMode(value, this.partnerMobile);
                this.partnerMobile = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner web.
        /// </summary>
        public string PartnerWeb
        {
            get
            {
                return this.partnerWeb;
            }

            set
            {
                this.SetMode(value, this.partnerWeb);
                this.partnerWeb = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner credit limit.
        /// </summary>
        public int PartnerCreditLimit
        {
            get
            {
                return this.partnerCreditLimit;
            }

            set
            {
                this.partnerCreditLimit = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner account balance.
        /// </summary>
        public decimal PartnerAccountBalance
        {
            get
            {
                return this.partnerAccountBalance;
            }

            set
            {
                this.partnerAccountBalance = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner limit.
        /// </summary>
        public decimal? PartnerOrderLimit
        {
            get
            {
                return this.partnerOrderLimit;
            }

            set
            {
                this.SetMode(value, this.partnerOrderLimit);
                this.partnerOrderLimit= value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner uplift.
        /// </summary>
        public int PartnerUplift
        {
            get
            {
                return this.partnerUplift;
            }

            set
            {
                this.SetMode(value, this.partnerUplift);
                this.partnerUplift = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets indicating whether a po is required.
        /// </summary>
        public bool PartnerPORequired
        {
            get
            {
                return this.partnerPORequired;
            }

            set
            {
                this.SetMode(value, this.partnerPORequired);
                this.partnerPORequired = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets indicating whether the partner is on hold.
        /// </summary>
        public bool PartnerOnHold
        {
            get
            {
                return this.partnerOnHold;
            }

            set
            {
                this.SetMode(value, this.partnerOnHold);
                this.partnerOnHold = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets indicating whether the partner is on hold.
        /// </summary>
        public bool Insurance
        {
            get
            {
                return this.insurance;
            }

            set
            {
                this.SetMode(value, this.insurance);
                this.insurance = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets indicating whether the partner is on hold.
        /// </summary>
        public bool IFALevy
        {
            get
            {
                return this.ifaLevy;
            }

            set
            {
                this.SetMode(value, this.ifaLevy);
                this.ifaLevy = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets indicating whether the partner is on hold.
        /// </summary>
        public bool Scrapie
        {
            get
            {
                return this.scrapie;
            }

            set
            {
                this.SetMode(value, this.scrapie);
                this.scrapie = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner currency.
        /// </summary>
        public BPCurrency PartnerCurrency
        {
            get
            {
                return this.partnerCurrency;
            }

            set
            {
                this.SetMode(value, this.partnerCurrency);
                this.partnerCurrency = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner group.
        /// </summary>
        public BPGroup PartnerGroup
        {
            get
            {
                return this.partnerGroup;
            }

            set
            {
                this.SetMode(value, this.partnerGroup);
                this.partnerGroup = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner type.
        /// </summary>
        public NouBPType PartnerType
        {
            get
            {
                return this.partnerType;
            }

            set
            {
                this.SetMode(value, this.partnerType);
                this.partnerType = value;
                this.RaisePropertyChanged();

                this.IsLivestockSupplier = value != null 
                    && (value.Type == BusinessPartnerType.LivestockSupplier || value.Type == BusinessPartnerType.CustomerAndLivestock);
            }
        }

        /// <summary>
        /// Gets or sets the display currency.
        /// </summary>
        public BPCurrency DisplayCurrency
        {
            get
            {
                return this.displayCurrency;
            }

            set
            {
                this.SetMode(value, this.displayCurrency);
                this.displayCurrency = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner main contact name.
        /// </summary>
        public string PartnerMainContactName
        {
            get
            {
                return this.partnerMainContactName;
            }

            set
            {
                this.partnerMainContactName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner main contact mobile.
        /// </summary>
        public string PartnerMainContactMobile
        {
            get
            {
                return this.partnerMainContactMobile;
            }

            set
            {
                this.partnerMainContactMobile = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner main contact tel.
        /// </summary>
        public string PartnerMainContactDirectLine
        {
            get
            {
                return this.partnerMainContactDirectLine;
            }

            set
            {
                this.partnerMainContactDirectLine = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner main contact email.
        /// </summary>
        public string PartnerMainContactEmail
        {
            get
            {
                return this.partnerMainContactEmail;
            }

            set
            {
                this.partnerMainContactEmail = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner active from date.
        /// </summary>
        public DateTime? PartnerActiveFromDate
        {
            get
            {
                return this.partnerActiveFromDate;
            }

            set
            {
                this.SetMode(value, this.partnerActiveFromDate);
                this.partnerActiveFromDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner active to date.
        /// </summary>
        public DateTime? PartnerActiveToDate
        {
            get
            {
                return this.partnerActiveToDate;
            }

            set
            {
                this.SetMode(value, this.partnerActiveToDate);
                this.partnerActiveToDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner inactive from date.
        /// </summary>
        public DateTime? PartnerInactiveFromDate
        {
            get
            {
                return this.partnerInactiveFromDate;
            }

            set
            {
                this.SetMode(value, this.partnerInactiveFromDate);
                this.partnerInactiveFromDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner inactive to date.
        /// </summary>
        public DateTime? PartnerInactiveToDate
        {
            get
            {
                return this.partnerInactiveToDate;
            }

            set
            {
                this.SetMode(value, this.partnerInactiveToDate);
                this.partnerInactiveToDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the partner route
        /// </summary>
        public Route SelectedRoute
        {
            get
            {
                return this.selectedRoute;
            }

            set
            {
                this.SetMode(value, this.selectedRoute);
                this.selectedRoute = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the routes.
        /// </summary>
        public ObservableCollection<Route> Routes
        {
            get
            {
                return this.routes;
            }

            set
            {
                this.routes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the business partner groups.
        /// </summary>
        public ObservableCollection<BPGroup> BusinessPartnerGroups
        {
            get
            {
                return this.businessPartnerGroups;
            }

            set
            {
                this.businessPartnerGroups = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the business partner currencies.
        /// </summary>
        public ObservableCollection<BPCurrency> BusinessPartnerCurrencies
        {
            get
            {
                return this.businessPartnerCurrencies;
            }

            set
            {
                this.businessPartnerCurrencies = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the business partner currencies.
        /// </summary>
        public ObservableCollection<NouBPType> BusinessPartnerTypes
        {
            get
            {
                return this.businessPartnerTypes;
            }

            set
            {
                this.businessPartnerTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets the business partner contacts.
        /// </summary>
        public ObservableCollection<BusinessPartnerContact> BusinessPartnerContacts { get; private set; }

        /// <summary>
        /// Gets the business partner addresses.
        /// </summary>
        public ObservableCollection<BusinessPartnerAddress> BusinessPartnerAddresses { get; private set; }

        /// <summary>
        /// Gets the business partner attachments.
        /// </summary>
        public ObservableCollection<BPAttachment> BusinessPartnerAttachments { get; private set; }
     
        /// <summary>
        /// Gets the business partner addresses.
        /// </summary>
        public ObservableCollection<BusinessPartnerProperty> BPProperties { get; set; }

        /// <summary>
        /// Gets or sets the business partner selected properties.
        /// </summary>
        public ObservableCollection<BPProperty> SelectedProperties { get; set; }

        /// <summary>
        /// Gets or sets the business partner plants.
        /// </summary>
        public ObservableCollection<Model.DataLayer.Plant> Plants { get; set; }
    
        /// <summary>
        /// Gets or sets a value indicating whether we are in find mode.
        /// </summary>
        public bool SearchMode
        {
            get
            {
                return this.searchMode;
            }

            set
            {
                this.searchMode = value;
                this.RaisePropertyChanged();

                if (value)
                {
                    // Find sale order mode, so set focus to partner search combo box.
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedPartnerControl);
                }
            }
        }

        /// <summary>
        /// Gets or sets the partners.
        /// </summary>
        public ObservableCollection<BusinessPartner> BusinessPartners
        {
            get
            {
                return this.businessPartners;
            }

            set
            {
                this.businessPartners = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the selected partner.
        /// </summary>
        public BusinessPartner SelectedBusinessPartner
        {
            get
            {
                return this.selectedBusinessPartner;     
            }

            set
            {
                if (value == null)
                {
                    return;
                }

                this.selectedBusinessPartner = value;
                if (value.Details != null && value.Details.BPMasterID > 0)
                {
                    value = this.DataManager.GetBusinessPartner(value.Details.BPMasterID);
                    this.selectedBusinessPartner = value;
                }
        
                this.RaisePropertyChanged();
                this.HandleSelectedPartner();
              
                // we need to create a deep copy snapshot, for auditing comparison.
                //this.selectedBusinessPartnerSnapshot = value.Copy();
            }
        }

        #endregion

        #region selected contact

        /// <summary>
        /// Gets or sets the selected partner contact.
        /// </summary>
        public BusinessPartnerContact SelectedContact
        {
            get
            {
                return this.selectedContact;
            }

            set
            {
                if (value != null)
                {
                    if (value.Details == null)
                    {
                        value.Details = new BPContact
                                            {
                                                FirstName = string.Empty, 
                                                LastName = string.Empty, 
                                                Email = string.Empty, 
                                                JobTitle = string.Empty, 
                                                Phone = string.Empty
                                            };
                    }

                    this.selectedContact = value;
                    this.RaisePropertyChanged();

                    this.HandleSelectedContact();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected contact id.
        /// </summary>
        public int SelectedContactID
        {
            get
            {
                return this.selectedContactId;
            }

            set
            {
                this.selectedContactId = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected contact title.
        /// </summary>
        public string SelectedContactTitle
        {
            get
            {
                return this.selectedContactTitle;
            }

            set
            {
                this.SetMode(value, this.selectedContactTitle);
                this.selectedContactTitle = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected contact first name.
        /// </summary>
        public string SelectedContactFirstName
        {
            get
            {
                return this.selectedContactFirstName;
            }

            set
            {
                this.SetMode(value, this.selectedContactFirstName);
                this.selectedContactFirstName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected contact last name.
        /// </summary>
        public string SelectedContactLastName
        {
            get
            {
                return this.selectedContactLastName;
            }

            set
            {
                this.SetMode(value, this.selectedContactLastName);
                this.selectedContactLastName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected contact job title.
        /// </summary>
        public string SelectedContactJobTitle
        {
            get
            {
                return this.selectedContactJobTitle;
            }

            set
            {
                this.SetMode(value, this.selectedContactJobTitle);
                this.selectedContactJobTitle = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected contact mobile.
        /// </summary>
        public string SelectedContactMobile
        {
            get
            {
                return this.selectedContactMobile;
            }

            set
            {
                this.SetMode(value, this.selectedContactMobile);
                this.selectedContactMobile = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected contact direct line.
        /// </summary>
        public string SelectedContactDirectLine
        {
            get
            {
                return this.selectedContactDirectLine;
            }

            set
            {
                this.SetMode(value, this.selectedContactDirectLine);
                this.selectedContactDirectLine = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected contact email.
        /// </summary>
        public string SelectedContactEmail
        {
            get
            {
                return this.selectedContactEmail;
            }

            set
            {
                this.SetMode(value, this.selectedContactEmail);
                this.selectedContactEmail = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected view in focus.
        /// </summary>
        public SelectedView SelectedPartnerView
        {
            get
            {
                return this.selectedPartnerView;
            }

            set
            {
                this.selectedPartnerView = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the contact is the primary contact.
        /// </summary>
        public bool SelectedContactIsPrimary
        {
            get
            {
                return this.selectedContactIsPrimary;
            }

            set
            {
                this.SetMode(value, this.selectedContactIsPrimary);

                if (value && this.selectedBusinessPartner != null)
                {
                    // Ensure the other contacts primary field is nullified.
                    this.selectedBusinessPartner.Contacts.ToList().ForEach(x => x.Details.PrimaryContact = false);
                }

                this.selectedContactIsPrimary = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region selected address

        /// <summary>
        /// Gets or sets the selected partner address.
        /// </summary>
        public BusinessPartnerAddress SelectedAddress
        {
            get
            {
                return this.selectedAddress;
            }

            set
            {
                if (value == null)
                {
                    return;
                }

                if (value.Details == null)
                {
                    value.Details = new BPAddress
                    {
                        AddressLine1 = string.Empty,
                        AddressLine2 = string.Empty,
                        AddressLine3 = string.Empty,
                        AddressLine4 = string.Empty
                    };
                }

                if (value.PlantDetails == null)
                {
                    value.PlantDetails = new Model.DataLayer.Plant
                    {
                        Code = string.Empty,
                        SiteName = string.Empty
                    };
                }

                if (value.PlantCountry == null)
                {
                    value.PlantCountry = new Country();
                }

                this.selectedAddress = value;
                this.RaisePropertyChanged();

                this.HandleSelectedAddress();
            }
        }

        /// <summary>
        /// Gets or sets the selected partner attachment.
        /// </summary>
        public BPAttachment SelectedAttachment
        {
            get
            {
                return this.selectedAttachment;
            }

            set
            {
                this.selectedAttachment = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address plant code.
        /// </summary>
        public string SelectedAddressPlantCode
        {
            get
            {
                return this.selectedAddressPlantCode;
            }

            set
            {
                this.SetMode(value, this.selectedAddressPlantCode);
                this.selectedAddressPlantCode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address plant country.
        /// </summary>
        public string SelectedAddressCountry
        {
            get
            {
                return this.selectedAddressPlantCountry;
            }

            set
            {
                this.SetMode(value, this.selectedAddressPlantCountry);
                this.selectedAddressPlantCountry = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address plant.
        /// </summary>
        public Model.DataLayer.Plant SelectedPlant
        {
            get
            {
                return this.selectedPlant;
            }

            set
            {
                this.SetMode(value, this.selectedPlant);
                this.selectedPlant = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address id.
        /// </summary>
        public int SelectedAddressID
        {
            get
            {
                return this.selectedAddressId;
            }

            set
            {
                this.selectedAddressId = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address line 1.
        /// </summary>
        public string GLN
        {
            get
            {
                return this.gln;
            }

            set
            {
                this.SetMode(value, this.gln);
                this.gln = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address line 1.
        /// </summary>
        public string SelectedAddressLine1
        {
            get
            {
                return this.selectedAddressLine1;
            }

            set
            {
                this.SetMode(value, this.selectedAddressLine1);
                this.selectedAddressLine1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address line 2.
        /// </summary>
        public string SelectedAddressLine2
        {
            get
            {
                return this.selectedAddressLine2;
            }

            set
            {
                this.SetMode(value, this.selectedAddressLine2);
                this.selectedAddressLine2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address line 3.
        /// </summary>
        public string SelectedAddressLine3
        {
            get
            {
                return this.selectedAddressLine3;
            }

            set
            {
                this.SetMode(value, this.selectedAddressLine3);
                this.selectedAddressLine3 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address line 4.
        /// </summary>
        public string SelectedAddressLine4
        {
            get
            {
                return this.selectedAddressLine4;
            }

            set
            {
                this.SetMode(value, this.selectedAddressLine4);
                this.selectedAddressLine4 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address line 5.
        /// </summary>
        public string SelectedAddressLine5
        {
            get
            {
                return this.selectedAddressLine5;
            }

            set
            {
                this.SetMode(value, this.selectedAddressLine5);
                this.selectedAddressLine5 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected address postcode.
        /// </summary>
        public string SelectedAddressPostcode
        {
            get
            {
                return this.selectedAddressPostcode;
            }

            set
            {
                this.SetMode(value, this.selectedAddressPostcode);
                this.selectedAddressPostcode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the selected address is a ship to address.
        /// </summary>
        public bool SelectedAddressIsShipping
        {
            get
            {
                return this.selectedAddressIsShipping;
            }

            set
            {
                this.SetMode(value, this.selectedAddressIsShipping);

                if (this.selectedBusinessPartner != null)
                {
                    if (this.SelectedAddress != null)
                    {
                        this.SelectedAddress.Details.Shipping = value;
                    }

                    //// Ensure the other addresses shipping field is nullified.
                    //foreach (var address in this.selectedBusinessPartner.Addresses)
                    //{
                    //    if (address.Details != null && this.selectedAddress != null && this.selectedAddress.Details != null
                    //        && address.Details.BPAddressID != this.selectedAddress.Details.BPAddressID && address.Details.Shipping == true)
                    //    {
                    //        address.Details.Shipping = false;
                    //    }
                    //}
                }

                this.selectedAddressIsShipping = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the selected address is a bill to address.
        /// </summary>
        public bool SelectedAddressIsBilling
        {
            get
            {
                return this.selectedAddressIsBilling;
            }

            set
            {
                this.SetMode(value, this.selectedAddressIsBilling);

                if (this.selectedBusinessPartner != null)
                {
                    if (this.SelectedAddress != null)
                    {
                        this.SelectedAddress.Details.Billing = value;
                    }

                    //foreach (var address in this.selectedBusinessPartner.Addresses)
                    //{
                    //    if (address.Details != null && this.selectedAddress != null && this.selectedAddress.Details != null
                    //        && address.Details.BPAddressID != this.selectedAddress.Details.BPAddressID && address.Details.Billing == true)
                    //    {
                    //        address.Details.Billing = false;
                    //    }
                    //}
                }
               
                this.selectedAddressIsBilling = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region herds

        /// <summary>
        /// Gets or sets the selected herd.
        /// </summary>
        public SupplierHerd SelectedPartnerHerd
        {
            get
            {
                return this.selectedPartnerHerd;
            }

            set
            {
                this.selectedPartnerHerd = value;
                this.RaisePropertyChanged();
                if (value != null && value.HerdNumber != Strings.DefineNew)
                {
                    this.HerdNumber = value.HerdNumber;
                    this.IsBeefHerd = value.Beef.ToBool();
                    this.IsSheepHerd = value.Sheep.ToBool();
                }
                else
                {
                    this.HerdNumber = string.Empty;
                    this.IsBeefHerd = false;
                    this.IsSheepHerd = false;

                    if (value != null && value.HerdNumber == Strings.DefineNew)
                    {
                        Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected herd.
        /// </summary>
        public ObservableCollection<SupplierHerd> SelectedPartnerHerds
        {
            get
            {
                return this.selectedPartnerHerds;
            }

            set
            {
                if (value != null)
                {
                    value.Add(new SupplierHerd { HerdNumber = Strings.DefineNew });
                }

                this.selectedPartnerHerds = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the herd no.
        /// </summary>
        public string HerdNumber
        {
            get
            {
                return this.herdNumber;
            }

            set
            {
                this.SetMode(value, this.herdNumber);
                this.herdNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the herd is beef.
        /// </summary>
        public bool IsBeefHerd
        {
            get
            {
                return this.isBeefHerd;
            }

            set
            {
                this.SetMode(value, this.isBeefHerd);
                this.isBeefHerd = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the herd is sheep.
        /// </summary>
        public bool IsSheepHerd
        {
            get
            {
                return this.isSheepHerd;
            }

            set
            {
                this.SetMode(value, this.isSheepHerd);
                this.isSheepHerd = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the partner is a farmer.
        /// </summary>
        public bool IsLivestockSupplier
        {
            get
            {
                return this.isLivestockSupplier;
            }

            set
            {
                this.isLivestockSupplier = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the selected partner property.
        /// </summary>
        public BusinessPartnerProperty SelectedProperty
        {
            get
            {
                return this.selectedProperty;
            }

            set
            {
                if (value == null)
                {
                    return;
                }

                this.selectedProperty = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region payment

        /// <summary>
        /// Gets or sets the partner del charge threshold.
        /// </summary>
        public decimal? PartnerDeliveryChargeThreshold
        {
            get
            {
                return this.partnerDeliveryChargeThreshold;
            }

            set
            {
                this.SetMode(value, this.partnerDeliveryChargeThreshold);
                this.partnerDeliveryChargeThreshold = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the price masters.
        /// </summary>
        public ObservableCollection<PriceList> PriceLists
        {
            get
            {
                return this.priceLists;
            }

            set
            {
                this.priceLists = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected price list.
        /// </summary>
        public PriceList SelectedPriceList
        {
            get
            {
                return this.selectedPriceList;
            }

            set
            {
                this.SetMode(value, this.selectedPriceList);
                this.selectedPriceList = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region label detail

        /// <summary>
        /// Gets or sets the static label text name.
        /// </summary>
        public string LabelTextName
        {
            get
            {
                return this.labelTextName;
            }

            set
            {
                this.labelTextName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the bacode text.
        /// </summary>
        public string Barcode1
        {
            get
            {
                return this.barcode1;
            }

            set
            {
                this.SetMode(value, this.barcode1);
                this.barcode1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the bacode text.
        /// </summary>
        public string Barcode2
        {
            get
            {
                return this.barcode2;
            }

            set
            {
                this.SetMode(value, this.barcode2);
                this.barcode2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelMultiText1
        {
            get
            {
                return this.labelMultiText1;
            }

            set
            {
                this.SetMode(value, this.labelMultiText1);
                this.labelMultiText1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelMultiText2
        {
            get
            {
                return this.labelMultiText2;
            }

            set
            {
                this.SetMode(value, this.labelMultiText2);
                this.labelMultiText2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText1
        {
            get
            {
                return this.labelText1;
            }

            set
            {
                this.SetMode(value, this.labelText1);
                this.labelText1 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText2
        {
            get
            {
                return this.labelText2;
            }

            set
            {
                this.SetMode(value, this.labelText2);
                this.labelText2 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText3
        {
            get
            {
                return this.labelText3;
            }

            set
            {
                this.SetMode(value, this.labelText3);
                this.labelText3 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText4
        {
            get
            {
                return this.labelText4;
            }

            set
            {
                this.SetMode(value, this.labelText4);
                this.labelText4 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText5
        {
            get
            {
                return this.labelText5;
            }

            set
            {
                this.SetMode(value, this.labelText5);
                this.labelText5 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText6
        {
            get
            {
                return this.labelText6;
            }

            set
            {
                this.SetMode(value, this.labelText6);
                this.labelText6 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText7
        {
            get
            {
                return this.labelText7;
            }

            set
            {
                this.SetMode(value, this.labelText7);
                this.labelText7 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText8
        {
            get
            {
                return this.labelText8;
            }

            set
            {
                this.SetMode(value, this.labelText8);
                this.labelText8 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText9
        {
            get
            {
                return this.labelText9;
            }

            set
            {
                this.SetMode(value, this.labelText9);
                this.labelText9 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText10
        {
            get
            {
                return this.labelText10;
            }

            set
            {
                this.SetMode(value, this.labelText10);
                this.labelText10 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText11
        {
            get
            {
                return this.labelText11;
            }

            set
            {
                this.SetMode(value, this.labelText11);
                this.labelText11 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText12
        {
            get
            {
                return this.labelText12;
            }

            set
            {
                this.SetMode(value, this.labelText12);
                this.labelText12 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText13
        {
            get
            {
                return this.labelText13;
            }

            set
            {
                this.SetMode(value, this.labelText13);
                this.labelText13 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText14
        {
            get
            {
                return this.labelText14;
            }

            set
            {
                this.SetMode(value, this.labelText14);
                this.labelText14 = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the label text.
        /// </summary>
        public string LabelText15
        {
            get
            {
                return this.labelText15;
            }

            set
            {
                this.SetMode(value, this.labelText15);
                this.labelText15 = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #endregion

        #endregion

        #region protected

        protected override void GetProcesses()
        {
            this.Processes = new ObservableCollection<Model.DataLayer.Process>();
            this.AllProcesses = this.DataManager.GetProcesses().ToList();
            var displayProcesses = ApplicationSettings.DefaultOutOfProductionModes;
            if (!string.IsNullOrWhiteSpace(displayProcesses))
            {
                var names = displayProcesses.Split(',').Select(x => x.Replace(" ", "").Trim());
                var localProcesses = this.AllProcesses.Where(x => names.Contains(x.Name.Replace(" ", "").Trim()));
                this.Processes = new ObservableCollection<Model.DataLayer.Process>(localProcesses);
            }

            this.Processes.Insert(0, new Model.DataLayer.Process { Name = Strings.NoneSelected });
        }

        protected override void CheckForNewEntities(string entityType = "")
        {
            try
            {
                var newEntities = this.DataManager.CheckForNewEntities(NouvemGlobal.DeviceId.ToInt(), entityType);
                var partnerEntities = newEntities.Where(x => x.Name.CompareIgnoringCase(EntityType.BusinessPartner));
                foreach (var partnerEntity in partnerEntities)
                {
                    if (NouvemGlobal.BusinessPartners.Any(x => x.Details.BPMasterID == partnerEntity.EntityID))
                    {
                        continue;
                    }

                    var localPartner = this.DataManager.GetBusinessPartner(partnerEntity.EntityID);
                    if (localPartner != null)
                    {
                        NouvemGlobal.AddNewPartner(localPartner);
                        this.BusinessPartners.Add(localPartner);
                    }
                }
            }
            catch (Exception e)
            {
                
            }
           
        }

        /// <summary>
        /// Method that creates a blank business partner.
        /// </summary>
        protected void CreateBlankBusinessPartner()
        {
            this.SelectedBusinessPartner = new BusinessPartner
            {
                Addresses = new List<BusinessPartnerAddress>(),
                Attachments = new List<BPAttachment>(),
                Details = new ViewBusinessPartner(),
                Contacts = new List<BusinessPartnerContact>(),
                PartnerProperties = new List<BusinessPartnerProperty>(),
                Herds = new List<SupplierHerd>()
            };

            this.PartnerType = this.BusinessPartnerTypes.FirstOrDefault(x => x.Type.Equals(Strings.NoneSelected));
            this.SelectedRoute = this.Routes.FirstOrDefault(x => x.Name.Equals(Strings.NoneSelected));
            this.PartnerGroup = this.BusinessPartnerGroups.FirstOrDefault(x => x.BPGroupName.Equals(Strings.NoneSelected));
            this.PartnerCurrency = this.BusinessPartnerCurrencies.FirstOrDefault(x => x.Name.Equals(Strings.NoneSelected));
            this.SelectedPriceList = this.PriceLists.FirstOrDefault(x => x.CurrentPriceListName.Equals(Strings.NoneSelected));
            this.CheckBoxActive = false;
            this.CheckBoxInactive = false;
            this.HerdNumber = string.Empty;
            this.IsBeefHerd = false;
            this.IsSheepHerd = false;
        }

        /// <summary>
        /// Override the control mode set, to only apply when a selected entity is not being changed.
        /// </summary>
        /// <typeparam name="T">The generic type.</typeparam>
        /// <param name="newValue">The old value to be changed from.</param>
        /// <param name="oldValue">The value to change to.</param>
        protected override void SetMode<T>(T newValue, T oldValue)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            base.SetMode<T>(newValue, oldValue);
        }

        /// <summary>
        /// Method that adds a new address.
        /// </summary>
        protected void AddNewAddressCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

            if (this.AddressCreated())
            {
                // There's a current address selected, so clear it.
                this.ClearAddress();
            }
        }

        /// <summary>
        /// Method that adds a new contact.
        /// </summary>
        protected void AddNewContactCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

            if (this.ContactCreated())
            {
                // There's a current contact selected, so clear it.
                this.ClearContact();
            }
        }

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        protected void UpdateBusinessPartner()
        {
            #region Validation

            var error = this.ValidateBusinessPartner();
            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            try
            {
                var partnerToUpdate = this.CreateOrUpdateBusinessPartner();

                if (this.DataManager.AddOrUpdateBusinessPartner(partnerToUpdate, this.selectedBusinessPartnerSnapshot))
                {
                    SystemMessage.Write(MessageType.Priority, Message.BusinessPartnerUpdated);

                    this.UpdatePartnerContacts();
                    this.UpdatePartnerAddresses();
                    this.UpdatePartnerAttachments();
                    //NouvemGlobal.UpdatePartner(partnerToUpdate);
                    var localPartner = NouvemGlobal.BusinessPartners.FirstOrDefault(x =>
                        x.Details.BPMasterID == partnerToUpdate.Details.BPMasterID);
                    if (localPartner != null)
                    {
                        NouvemGlobal.BusinessPartners.Remove(localPartner);
                        NouvemGlobal.BusinessPartners.Add(partnerToUpdate);
                    }

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemGlobal.RefreshPartners();
                        Messenger.Default.Send(Token.Message, Token.RefreshPartners); 
                        this.GetBusinessPartners();
                    }));

                    this.selectedBusinessPartnerSnapshot = this.selectedBusinessPartner.Copy();
                    this.CreateBlankBusinessPartner();
                    this.SetControlMode(ControlMode.OK);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.BusinessPartnerNotUpdated);
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Method that creates a new business partner.
        /// </summary>
        protected BusinessPartner CreateOrUpdateBusinessPartner()
        {
            if (this.CurrentMode != ControlMode.Update)
            {
                this.SelectedBusinessPartner.Details.Code = this.PartnerCode;
            }
            
            this.SelectedBusinessPartner.Details.Name = this.PartnerName ?? string.Empty;
            this.SelectedBusinessPartner.Details.CompanyNo = this.PartnerCompanyNo ?? string.Empty;
            this.SelectedBusinessPartner.Details.Remarks = this.PartnerRemarks ?? string.Empty;
            this.SelectedBusinessPartner.Details.Notes = this.PartnerNotes ?? string.Empty;
            this.SelectedBusinessPartner.Details.PopUpNotes = this.PopUpNote ?? string.Empty;
            this.SelectedBusinessPartner.Details.InvoiceNote = this.InvoiceNote ?? string.Empty;
            this.SelectedBusinessPartner.Details.FAX = this.PartnerFax ?? string.Empty;
            this.SelectedBusinessPartner.Details.Tel = this.PartnerTel ?? string.Empty;
            this.SelectedBusinessPartner.Details.Web = this.PartnerWeb ?? string.Empty;
            this.SelectedBusinessPartner.Details.DeliveryChargeThreshold = this.PartnerDeliveryChargeThreshold;
            this.SelectedBusinessPartner.Details.VATNo = this.PartnerVATNo ?? string.Empty;
            this.SelectedBusinessPartner.Details.ActiveFrom = this.PartnerActiveFromDate;
            this.SelectedBusinessPartner.Details.ActiveTo = this.PartnerActiveToDate;
            this.SelectedBusinessPartner.Details.InActiveFrom = this.PartnerInactiveFromDate;
            this.SelectedBusinessPartner.Details.InActiveTo = this.PartnerInactiveToDate;
            this.SelectedBusinessPartner.Details.Balance = this.PartnerAccountBalance;
            this.SelectedBusinessPartner.Details.OrderLimit = this.PartnerOrderLimit;
            this.SelectedBusinessPartner.Details.Uplift = this.PartnerUplift;
            this.SelectedBusinessPartner.Details.CreditLimit = this.PartnerCreditLimit;
            this.SelectedBusinessPartner.Details.PORequired = this.PartnerPORequired;
            this.SelectedBusinessPartner.Details.OnHold = this.PartnerOnHold;
            this.SelectedBusinessPartner.Details.Insurance = this.Insurance;
            this.SelectedBusinessPartner.Details.IFALevy = this.IFALevy;
            this.SelectedBusinessPartner.Details.Scrapie = this.Scrapie;
            this.SelectedBusinessPartner.Details.DeviceID = NouvemGlobal.DeviceId;
            this.SelectedBusinessPartner.Details.EditDate = DateTime.Now;
            this.SelectedBusinessPartner.Route = this.SelectedRoute != null && this.SelectedRoute.Name != Strings.NoneSelected ? this.SelectedRoute : null;
            this.SelectedBusinessPartner.Details.PriceListID = this.SelectedPriceList.PriceListID > 0 ? (int?)this.SelectedPriceList.PriceListID : null;
            this.SelectedBusinessPartner.Details.BPMasterID_Agent = this.SelectedAgent != null && this.SelectedAgent.BPMasterID > 0 ? this.SelectedAgent.BPMasterID : (int?)null;

            this.SelectedBusinessPartner.PartnerProperties =
                this.BPProperties.Where(x => x.IsSelected).ToList();

            if (this.AddressCreated())
            {
                if (this.SelectedAddress != null)
                {
                    this.SelectedAddress.Details.AddressLine1 = this.SelectedAddressLine1 ?? string.Empty;
                    this.SelectedAddress.Details.AddressLine2 = this.SelectedAddressLine2 ?? string.Empty;
                    this.SelectedAddress.Details.AddressLine3 = this.SelectedAddressLine3 ?? string.Empty;
                    this.SelectedAddress.Details.AddressLine4 = this.SelectedAddressLine4 ?? string.Empty;
                    this.SelectedAddress.Details.AddressLine5 = this.SelectedAddressLine5 ?? string.Empty;
                    this.SelectedAddress.Details.PostCode = this.SelectedAddressPostcode ?? string.Empty;
                    this.SelectedAddress.Details.GLN = this.GLN ?? string.Empty;
                    this.SelectedAddress.Details.Billing = this.SelectedAddressIsBilling;
                    this.SelectedAddress.Details.Shipping = this.SelectedAddressIsShipping;
                    this.SelectedAddress.PlantDetails.PlantID = this.SelectedPlant.PlantID;

                    if (this.SelectedAddress.Details.BPAddressID == 0)
                    {
                        this.SelectedBusinessPartner.Addresses.Add(this.selectedAddress);
                    }

                    if (this.CurrentMode == ControlMode.Add)
                    {
                        var invoiceAddressEntered =
                       this.SelectedBusinessPartner.Addresses.Any(x => x.Details.Billing == true);
                        if (!invoiceAddressEntered)
                        {
                            this.SelectedBusinessPartner.Addresses.First().Details.Billing = true;
                        }

                        var delAddressEntered =
                           this.SelectedBusinessPartner.Addresses.Any(x => x.Details.Shipping == true);
                        if (!delAddressEntered)
                        {
                            this.SelectedBusinessPartner.Addresses.First().Details.Shipping = true;
                        }
                    }
                }
            }

            if (this.SelectedProcess == null || this.SelectedProcess.Name.Equals(Strings.NoneSelected))
            {
                this.selectedBusinessPartner.Details.ProcessID = null;
            }
            else
            {
                this.selectedBusinessPartner.Details.ProcessID = this.SelectedProcess.ProcessID;
            }

            if (!string.IsNullOrWhiteSpace(this.HerdNumber))
            {
                if (this.CurrentMode == ControlMode.Add)
                {
                    this.selectedBusinessPartner.Herds.Add(new SupplierHerd { HerdNumber = this.herdNumber, Beef = this.isBeefHerd, Sheep = this.isSheepHerd });
                }
                else if (this.SelectedPartnerHerd == null || this.SelectedPartnerHerd.HerdNumber == Strings.DefineNew)
                {
                    this.SelectedBusinessPartner.Herds = new List<SupplierHerd>();
                    this.selectedBusinessPartner.Herds.Add(new SupplierHerd
                    {
                        HerdNumber = this.herdNumber, Beef = this.isBeefHerd, Sheep = this.isSheepHerd
                    });
                }
                else if (this.SelectedPartnerHerd != null && this.SelectedPartnerHerd.HerdNumber != Strings.DefineNew)
                {
                    this.SelectedBusinessPartner.Herds = new List<SupplierHerd>();
                    this.SelectedPartnerHerd.HerdNumber = this.HerdNumber;
                    this.SelectedPartnerHerd.Beef = this.IsBeefHerd;
                    this.SelectedPartnerHerd.Sheep = this.IsSheepHerd;
                    this.selectedBusinessPartner.Herds.Add(this.SelectedPartnerHerd);
                }
            }

            if (this.PartnerCurrency != null)
            {
                this.SelectedBusinessPartner.Details.BPCurrencyID = this.PartnerCurrency.BPCurrencyID;
                this.SelectedBusinessPartner.PartnerCurrency = this.PartnerCurrency;
            }
            else
            {
                this.SelectedBusinessPartner.Details.BPCurrencyID = 0;
            }

            if (this.PartnerType != null)
            {
                this.SelectedBusinessPartner.Details.NouBPTypeID = this.PartnerType.NouBPTypeID;
                this.SelectedBusinessPartner.PartnerType = this.PartnerType;
            }
            else
            {
                this.SelectedBusinessPartner.Details.NouBPTypeID = 0;
            }

            if (this.PartnerGroup != null)
            {
                this.SelectedBusinessPartner.Details.BPGroupID = this.PartnerGroup.BPGroupID;
                this.SelectedBusinessPartner.PartnerGroup = this.PartnerGroup;
            }
            else
            {
                this.SelectedBusinessPartner.Details.BPGroupID = 0;
            }

            if (this.ContactCreated())
            {
                this.SelectedContact.Details.Title = this.SelectedContactTitle ?? string.Empty;
                this.SelectedContact.Details.FirstName = this.SelectedContactFirstName ?? string.Empty;
                this.SelectedContact.Details.LastName = this.SelectedContactLastName ?? string.Empty;
                this.SelectedContact.Details.JobTitle = this.SelectedContactJobTitle ?? string.Empty;
                this.SelectedContact.Details.Phone = this.SelectedContactDirectLine ?? string.Empty;
                this.SelectedContact.Details.Email = this.SelectedContactEmail ?? string.Empty;
                this.SelectedContact.Details.Mobile = this.SelectedContactMobile ?? string.Empty;
                this.SelectedContact.Details.PrimaryContact = this.SelectedContactIsPrimary;

                if (this.SelectedContact.Details.BPContactID == 0)
                {
                    // new contact, so add to our partner contacts list.
                    this.SelectedBusinessPartner.Contacts.Add(this.selectedContact);
                }
            }

            if (this.SelectedBusinessPartner.Details.BPMasterID > 0)
            {
                var localPartner = this.BusinessPartners.FirstOrDefault(x =>
                    x.Details.BPMasterID == this.SelectedBusinessPartner.Details.BPMasterID);
                if (localPartner != null)
                {
                    localPartner.Details.EditDate = DateTime.Now;
                }
            }

            #region label items

            if (
                !string.IsNullOrEmpty(this.LabelText1)
                || !string.IsNullOrEmpty(this.LabelText1)
                || !string.IsNullOrEmpty(this.LabelText2)
                || !string.IsNullOrEmpty(this.LabelText3)
                || !string.IsNullOrEmpty(this.LabelText4)
                || !string.IsNullOrEmpty(this.LabelText5)
                || !string.IsNullOrEmpty(this.LabelText6)
                || !string.IsNullOrEmpty(this.LabelText7)
                || !string.IsNullOrEmpty(this.LabelText8)
                || !string.IsNullOrEmpty(this.LabelText9)
                || !string.IsNullOrEmpty(this.LabelText10)
                || !string.IsNullOrEmpty(this.LabelText11)
                || !string.IsNullOrEmpty(this.LabelText12)
                || !string.IsNullOrEmpty(this.LabelText13)
                || !string.IsNullOrEmpty(this.LabelText14)
                || !string.IsNullOrEmpty(this.LabelText15)
                )
            {
                if (this.selectedBusinessPartner.LabelField == null)
                {
                    this.selectedBusinessPartner.LabelField = new BPLabelField();
                }

                this.selectedBusinessPartner.LabelField.Field1 = this.LabelText1;
                this.selectedBusinessPartner.LabelField.Field2 = this.LabelText2;
                this.selectedBusinessPartner.LabelField.Field3 = this.LabelText3;
                this.selectedBusinessPartner.LabelField.Field4 = this.LabelText4;
                this.selectedBusinessPartner.LabelField.Field5 = this.LabelText5;
                this.selectedBusinessPartner.LabelField.Field6 = this.LabelText6;
                this.selectedBusinessPartner.LabelField.Field7 = this.LabelText7;
                this.selectedBusinessPartner.LabelField.Field8 = this.LabelText8;
                this.selectedBusinessPartner.LabelField.Field9 = this.LabelText9;
                this.selectedBusinessPartner.LabelField.Field10 = this.LabelText10;
                this.selectedBusinessPartner.LabelField.Field11 = this.LabelText11;
                this.selectedBusinessPartner.LabelField.Field12 = this.LabelText12;
                this.selectedBusinessPartner.LabelField.Field13 = this.LabelText13;
                this.selectedBusinessPartner.LabelField.Field14 = this.LabelText14;
                this.selectedBusinessPartner.LabelField.Field15 = this.LabelText15;
            }
            else
            {
                // no label field data.
                this.selectedBusinessPartner.LabelField = null;
            }

            #endregion

            return this.SelectedBusinessPartner;
        }

        /// <summary>
        /// Gets the partners.
        /// </summary>
        protected void GetBusinessPartners()
        {
            this.BusinessPartners = new ObservableCollection<BusinessPartner>(NouvemGlobal.BusinessPartners
                .Where(x => (x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                            || (DateTime.Today < x.Details.InActiveFrom || DateTime.Today > x.Details.InActiveTo)).OrderBy(x => x.Details.Name).ToList());

            if (ApplicationSettings.OnlyShowLivestockSuppliersAtBusinessPartner)
            {
                this.BusinessPartners = new ObservableCollection<BusinessPartner>(this.BusinessPartners.Where(x => x.Details.NouBPTypeID > 3000));
            }
        }

        /// <summary>
        /// Method that validates a business partner for adds or updates.
        /// </summary>
        /// <returns>A flag, indicating validation or not.</returns>
        protected string ValidateBusinessPartner()
        {
            var error = string.Empty;

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateBusinessPartners)
            {
                error = Message.AccessDenied;
                return error;
            }

            if (!string.IsNullOrEmpty(this.PartnerCode) && this.CurrentMode != ControlMode.Update && this.DoesPartnerCodeExist())
            {
                error = string.Format(Message.PartnerCodeAlreadyExists, this.PartnerCode);
                return error;
            }

            if (this.ContactCreated())
            {
                if (string.IsNullOrWhiteSpace(this.SelectedContactFirstName))
                {
                    error = Message.FirstNameEmpty;
                    return error;
                }
            }

            if (this.AddressCreated())
            {
                if (string.IsNullOrWhiteSpace(this.SelectedAddressLine1))
                {
                    error = Message.AddressLine1Empty;
                    return error;
                }
            }

            if (!this.AreThereAddresses())
            {
                error = Message.NoAddressEntered;
                return error;
            }

            if (string.IsNullOrWhiteSpace(this.PartnerCode))
            {
                error = Message.CodeEmpty;
                return error;
            }

            if (this.PartnerCode.Length > 30)
            {
                error = Message.CodeTooLong;
                return error;
            }

            if (this.PartnerType == null || this.PartnerType.Type.Equals(Strings.NoneSelected))
            {
                error = Message.SelectPartnerType;
                return error;
            }

            if (this.PartnerGroup == null || this.PartnerGroup.BPGroupName.Equals(Strings.NoneSelected))
            {
                this.PartnerGroup = this.DataManager.GetBusinessPartnerGroups().FirstOrDefault();
            }

            if (this.PartnerCurrency == null || this.PartnerCurrency.Name.Equals(Strings.NoneSelected))
            {
                error = Message.SelectCurrencyType;
                return error;
            }

            if (this.CurrentMode == ControlMode.Update && this.SelectedBusinessPartner.Details.BPMasterID == 0)
            {
                error = Message.CannotUpdatePartner;
                return error;
            }

            return error;
        }

        #endregion

        #region private

        #region master

        /// <summary>
        /// Method that parses the selected partners details.
        /// </summary>
        private void HandleSelectedPartner()
        {
            if (this.selectedBusinessPartner == null)
            {
                return;
            }

            try
            {
                // Set the flag that we are changing a business partner, so that control changes are to be ignored.
                this.EntitySelectionChange = true;

                this.CheckBoxInactive = false;
                this.CheckBoxActive = false;

                this.PartnerName = this.selectedBusinessPartner.Details.Name;
                this.PartnerCode = this.selectedBusinessPartner.Details.Code;
                this.PartnerVATNo = this.selectedBusinessPartner.Details.VATNo;
                this.PartnerFax = this.selectedBusinessPartner.Details.FAX;
                this.PartnerWeb = this.selectedBusinessPartner.Details.Web;
                this.PartnerTel = this.selectedBusinessPartner.Details.Tel;
                this.PartnerDeliveryChargeThreshold = this.selectedBusinessPartner.Details.DeliveryChargeThreshold;
                this.PartnerRemarks = this.selectedBusinessPartner.Details.Remarks;
                this.PartnerNotes = this.selectedBusinessPartner.Details.Notes;
                this.PopUpNote = this.selectedBusinessPartner.Details.PopUpNotes;
                this.InvoiceNote = this.selectedBusinessPartner.Details.InvoiceNote;
                this.PartnerCompanyNo = this.selectedBusinessPartner.Details.CompanyNo;
                this.PartnerCreditLimit = this.selectedBusinessPartner.Details.CreditLimit.ToInt();
                this.PartnerUplift = this.selectedBusinessPartner.Details.Uplift.ToInt();
                this.PartnerPORequired = this.selectedBusinessPartner.Details.PORequired.ToBool();
                this.PartnerOnHold = this.selectedBusinessPartner.Details.OnHold.ToBool();
                this.Insurance = this.selectedBusinessPartner.Details.Insurance.ToBool();
                this.IFALevy = this.selectedBusinessPartner.Details.IFALevy.ToBool();
                this.Scrapie = this.selectedBusinessPartner.Details.Scrapie.ToBool();
                this.PartnerAccountBalance = this.selectedBusinessPartner.Details.Balance.ToDecimal();
                this.PartnerOrderLimit = this.selectedBusinessPartner.Details.OrderLimit;
                this.PartnerActiveFromDate = this.selectedBusinessPartner.Details.ActiveFrom;
                this.PartnerActiveToDate = this.selectedBusinessPartner.Details.ActiveTo;
                this.PartnerInactiveFromDate = this.selectedBusinessPartner.Details.InActiveFrom;
                this.PartnerInactiveToDate = this.selectedBusinessPartner.Details.InActiveTo;
                this.PartnerMainContactName = string.Empty;
                this.PartnerMainContactEmail = string.Empty;
                this.PartnerMainContactDirectLine = string.Empty;
                this.PartnerMainContactMobile = string.Empty;
                this.SelectedAgent = null;
                if (this.selectedBusinessPartner.Details.BPMasterID_Agent.HasValue)
                {
                    this.SelectedAgent = this.Agents.FirstOrDefault(x =>
                        x.BPMasterID == this.selectedBusinessPartner.Details.BPMasterID_Agent);
                }

                this.SelectedPriceList = this.PriceLists.FirstOrDefault(x => x.PriceListID == this.selectedBusinessPartner.Details.PriceListID);
                if (this.SelectedPriceList == null)
                {
                    this.SelectedPriceList =
                        this.PriceLists.FirstOrDefault(x => x.CurrentPriceListName.Equals(Strings.NoneSelected));
                }

                if (this.PartnerInactiveFromDate != null && this.PartnerInactiveToDate != null &&
                       DateTime.Today >= this.PartnerInactiveFromDate && DateTime.Today <= this.PartnerInactiveToDate)
                {
                    this.CheckBoxInactive = true;
                }

                if (this.selectedBusinessPartner.Route != null)
                {
                    this.SelectedRoute = this.selectedBusinessPartner.Route;
                }

                if (!this.selectedBusinessPartner.Details.ProcessID.HasValue)
                {
                    this.SelectedProcess = this.Processes.FirstOrDefault();
                }
                else
                {
                    this.SelectedProcess = this.Processes.FirstOrDefault(x =>
                        x.ProcessID == this.selectedBusinessPartner.Details.ProcessID);
                }

                if (!this.selectedBusinessPartner.Details.RouteMasterID.HasValue)
                {
                    this.SelectedRoute = this.Routes.FirstOrDefault();
                }
                else
                {
                    this.SelectedRoute = this.Routes.FirstOrDefault(x =>
                        x.RouteID == this.selectedBusinessPartner.Details.RouteMasterID);
                }

                if (!this.selectedBusinessPartner.Details.BPCurrencyID.IsNullOrZero())
                {
                    this.PartnerCurrency = this.BusinessPartnerCurrencies.FirstOrDefault(x =>
                        x.BPCurrencyID == this.selectedBusinessPartner.Details.BPCurrencyID);
                }

                if (this.PartnerCurrency == null)
                {
                    this.PartnerCurrency = this.BusinessPartnerCurrencies.FirstOrDefault();
                }

                if (this.selectedBusinessPartner.Details.NouBPTypeID > 0)
                {
                    this.PartnerType = this.BusinessPartnerTypes.FirstOrDefault(x =>
                        x.NouBPTypeID == this.selectedBusinessPartner.Details.NouBPTypeID);
                }

                if (this.PartnerType == null)
                {
                    this.PartnerType = this.BusinessPartnerTypes.FirstOrDefault();
                }

                if (!this.selectedBusinessPartner.Details.BPGroupID.IsNullOrZero())
                {
                    this.PartnerGroup = this.BusinessPartnerGroups.FirstOrDefault(x =>
                        x.BPGroupID == this.selectedBusinessPartner.Details.BPGroupID);
                }

                if (this.PartnerGroup == null)
                {
                    this.PartnerGroup = this.BusinessPartnerGroups.FirstOrDefault();
                }

                this.UpdatePartnerContacts();
                this.UpdatePartnerAddresses();
                this.UpdatePartnerProperties();
                this.UpdatePartnerAttachments();
                this.SelectedPartnerHerds = new ObservableCollection<SupplierHerd>(
                    this.DataManager.GetSupplierHerds(this.selectedBusinessPartner.Details.BPMasterID));

                if (this.CurrentMode == ControlMode.Add && this.SelectedBusinessPartner.Details.BPMasterID == 0 && ApplicationSettings.AutoTickScrapie)
                {
                    this.Scrapie = true;
                    this.Insurance = true;
                }

                #region label

                this.LabelText1 = string.Empty;
                this.LabelText2 = string.Empty;
                this.LabelText3 = string.Empty;
                this.LabelText4 = string.Empty;
                this.LabelText5 = string.Empty;
                this.LabelText6 = string.Empty;
                this.LabelText7 = string.Empty;
                this.LabelText8 = string.Empty;
                this.LabelText9 = string.Empty;
                this.LabelText10 = string.Empty;
                this.LabelText11 = string.Empty;
                this.LabelText12 = string.Empty;
                this.LabelText13 = string.Empty;
                this.LabelText14 = string.Empty;
                this.LabelText15 = string.Empty;

                if (this.selectedBusinessPartner.LabelField != null)
                {
                    this.LabelText1 = this.selectedBusinessPartner.LabelField.Field1;
                    this.LabelText2 = this.selectedBusinessPartner.LabelField.Field2;
                    this.LabelText3 = this.selectedBusinessPartner.LabelField.Field3;
                    this.LabelText4 = this.selectedBusinessPartner.LabelField.Field4;
                    this.LabelText5 = this.selectedBusinessPartner.LabelField.Field5;
                    this.LabelText6 = this.selectedBusinessPartner.LabelField.Field6;
                    this.LabelText7 = this.selectedBusinessPartner.LabelField.Field7;
                    this.LabelText8 = this.selectedBusinessPartner.LabelField.Field8;
                    this.LabelText9 = this.selectedBusinessPartner.LabelField.Field9;
                    this.LabelText10 = this.selectedBusinessPartner.LabelField.Field10;
                    this.LabelText11 = this.selectedBusinessPartner.LabelField.Field11;
                    this.LabelText12 = this.selectedBusinessPartner.LabelField.Field12;
                    this.LabelText13 = this.selectedBusinessPartner.LabelField.Field13;
                    this.LabelText14 = this.selectedBusinessPartner.LabelField.Field14;
                    this.LabelText15 = this.selectedBusinessPartner.LabelField.Field15;
                }

                #endregion
            }
            finally
            {
                this.EntitySelectionChange = false;
                this.SetControlMode(ControlMode.OK);
                this.SearchMode = false;
            }
        }

        /// <summary>
        /// Gets the application routes.
        /// </summary>
        private void GetRoutes()
        {
            this.Routes = new ObservableCollection<Route>(NouvemGlobal.Routes);
            this.Routes.Insert(0, new Route {Name = Strings.NoneSelected});
        }

        #endregion

        #region contact

        /// <summary>
        /// Method that clears the contact screen data.
        /// </summary>
        protected void ClearContact()
        {
            this.SelectedContact = new BusinessPartnerContact();
            this.SelectedContactID = 0;
            this.SelectedContactTitle = string.Empty;
            this.SelectedContactFirstName = string.Empty;
            this.SelectedContactLastName = string.Empty;
            this.SelectedContactJobTitle = string.Empty;
            this.SelectedContactEmail = string.Empty;
            this.SelectedContactDirectLine = string.Empty;
            this.SelectedContactMobile = string.Empty;
            this.SelectedContactIsPrimary = false;
        }

        /// <summary>
        /// Method that updates the business partner contacts.
        /// </summary>
        protected void UpdatePartnerContacts()
        {
            this.BusinessPartnerContacts.Clear();
            var localContacts = this.selectedBusinessPartner.Contacts.ToList();
            
            if (!localContacts.Any(contact => contact.Details.PrimaryContact.ToBool()))
            {
                // no primary contact set up, so enure the main contact details are cleared.
                this.PartnerMainContactName = string.Empty;
                this.PartnerMainContactEmail = string.Empty;
                this.PartnerMainContactDirectLine = string.Empty;
                this.PartnerMainContactMobile = string.Empty;
            }
            
            localContacts.ForEach(
                contact =>
                {
                    if (contact.Details.PrimaryContact.ToBool())
                    {
                        this.PartnerMainContactName = string.Format("{0} {1}", contact.Details.FirstName, contact.Details.LastName);
                        this.PartnerMainContactEmail = contact.Details.Email;
                        this.PartnerMainContactDirectLine = contact.Details.Phone;
                        this.PartnerMainContactMobile = contact.Details.Mobile;
                    }

                    this.BusinessPartnerContacts.Add(contact);
                });

            this.BusinessPartnerContacts.Add(new BusinessPartnerContact { Details = new BPContact { FirstName = Strings.DefineNew } });

            this.ClearContact();
        }

        /// <summary>
        /// Method that parses the selected contacts details.
        /// </summary>
        private void HandleSelectedContact()
        {
            if (this.selectedContact == null)
            {
                return;
            }

            if (this.selectedContact.Details.FirstName.Equals(Strings.DefineNew))
            {
                this.AddNewContactCommandExecute();
                return;
            }

            var localMode = this.CurrentMode;
            this.SelectedContactID = this.selectedContact.Details.BPContactID;
            this.SelectedContactTitle = this.selectedContact.Details.Title;
            this.SelectedContactFirstName = this.selectedContact.Details.FirstName;
            this.SelectedContactLastName = this.selectedContact.Details.LastName;
            this.SelectedContactJobTitle = this.selectedContact.Details.JobTitle;
            this.SelectedContactDirectLine = this.selectedContact.Details.Phone;
            this.SelectedContactEmail = this.selectedContact.Details.Email;
            this.SelectedContactMobile = this.selectedContact.Details.Mobile;
            this.SelectedContactIsPrimary = this.selectedContact.Details.PrimaryContact.ToBool();

            // ensure we are in the same control mode as pre-selection
            this.SetControlMode(localMode);
        }

        #endregion

        #region address

        /// <summary>
        /// Method that clears the address screen data.
        /// </summary>
        protected void ClearAddress()
        {
            this.SelectedAddress = new BusinessPartnerAddress();
            this.SelectedAddressID = 0;
            this.SelectedAddressLine1 = string.Empty;
            this.SelectedAddressLine2 = string.Empty;
            this.SelectedAddressLine3 = string.Empty;
            this.SelectedAddressLine4 = string.Empty;
            this.SelectedAddressLine5 = string.Empty;
            this.GLN = string.Empty;
            this.SelectedAddressPostcode = string.Empty;
            this.SelectedAddressIsBilling = false;
            this.SelectedAddressIsShipping = false;
            this.SelectedAddressPlantCode = string.Empty;
            this.SelectedAddressCountry = string.Empty;
            this.SelectedPlant = this.Plants.FirstOrDefault(x => x.SiteName.Equals(Strings.NoneSelected));
        }

        /// <summary>
        /// Method that updates the business partner addresses.
        /// </summary>
        protected void UpdatePartnerAddresses()
        {
            this.BusinessPartnerAddresses.Clear();
            this.selectedBusinessPartner.Addresses.ToList().ForEach(address => this.BusinessPartnerAddresses.Add(address));
            this.BusinessPartnerAddresses.Add(new BusinessPartnerAddress { Details = new BPAddress { AddressLine1 = Strings.DefineNew } });
            this.ClearAddress();
        }

        /// <summary>
        /// Method that updates the business partner attachments.
        /// </summary>
        protected void UpdatePartnerAttachments()
        {
            if (this.selectedBusinessPartner.Attachments == null)
            {
                return;
            }

            this.BusinessPartnerAttachments.Clear();
            var localAttachments = new List<BPAttachment>(this.selectedBusinessPartner.Attachments);

            foreach (var localAttachment in localAttachments)
            {
                if (localAttachment.Deleted)
                {
                    this.selectedBusinessPartner.Attachments.Remove(localAttachment);
                }
            }

            this.selectedBusinessPartner.Attachments.ToList().ForEach(attachment => this.BusinessPartnerAttachments.Add(attachment));
        }

        /// <summary>
        /// Method that updates the business partner properties.
        /// </summary>
        protected void UpdatePartnerProperties()
        {
            if (this.selectedBusinessPartner.PartnerProperties == null)
            {
                return;
            }

            this.BPProperties.Clear();
            var partnerProperties = this.selectedBusinessPartner.PartnerProperties.Select(x => x.Details.BPPropertyID).ToList();

            if (NouvemGlobal.BusinessPartnerProperties != null)
            {
                foreach (var businessPartnerProperty in NouvemGlobal.BusinessPartnerProperties)
                {
                    var prop = new BusinessPartnerProperty
                    {
                        Details = businessPartnerProperty,
                        IsSelected = partnerProperties.Contains(businessPartnerProperty.BPPropertyID)
                    };

                    var message =
                        NouvemGlobal.BusinessPartnerPropertySelections.FirstOrDefault(
                            x =>
                                x.BPMasterID == this.selectedBusinessPartner.Details.BPMasterID &&
                                x.BPPropertyID == businessPartnerProperty.BPPropertyID);

                    if (message != null)
                    {
                        prop.DisplayMessage = message.DisplayMessage;
                    }

                    this.BPProperties.Add(prop);
                }
            }
        }

        /// <summary>
        /// Determine if a business partner has been selected.
        /// </summary>
        /// <returns></returns>
        protected override bool CheckForEntity()
        {
            return this.SelectedBusinessPartner != null && this.SelectedBusinessPartner.Details.BPMasterID > 0;
        }

        /// <summary>
        /// Method that parses the selected address details.
        /// </summary>
        private void HandleSelectedAddress()
        {
            if (this.selectedAddress == null)
            {
                return;
            }

            if (this.selectedAddress.Details.AddressLine1.Equals(Strings.DefineNew))
            {
                this.AddNewAddressCommandExecute();
                return;
            }

            var localMode = this.CurrentMode;

            this.SelectedAddressID = this.selectedAddress.Details.BPAddressID;
            this.SelectedAddressLine1 = this.selectedAddress.Details.AddressLine1;
            this.SelectedAddressLine2 = this.selectedAddress.Details.AddressLine2;
            this.SelectedAddressLine3 = this.selectedAddress.Details.AddressLine3;
            this.SelectedAddressLine4 = this.selectedAddress.Details.AddressLine4;
            this.SelectedAddressLine5 = this.selectedAddress.Details.AddressLine5;
            this.GLN = this.selectedAddress.Details.GLN;
            this.SelectedAddressPostcode = this.selectedAddress.Details.PostCode;
            this.SelectedAddressIsBilling = this.selectedAddress.Details.Billing.ToBool();
            this.SelectedAddressIsShipping = this.selectedAddress.Details.Shipping.ToBool();

            if (this.selectedAddress.PlantDetails != null)
            {
                this.SelectedAddressPlantCode = this.selectedAddress.PlantDetails.Code ?? string.Empty;
                this.SelectedAddressCountry = this.selectedAddress.PlantCountry.Name ?? string.Empty;
                this.SelectedPlant = this.Plants.FirstOrDefault(x => x.PlantID == this.selectedAddress.PlantDetails.PlantID);
            }

            // ensure we are in the same control mode as pre-selection.
            this.SetControlMode(localMode);
        }

        #endregion

        #region payment

        /// <summary>
        /// Gets the application price lists.
        /// </summary>
        private void GetPriceLists()
        {
            this.PriceLists = new ObservableCollection<PriceList>(this.DataManager.GetAllPriceLists().OrderBy(x => x.CurrentPriceListName));
            if (NouvemGlobal.SpecialPriceList != null)
            {
                var specPrice = this.PriceLists.FirstOrDefault(x => x.PriceListID == NouvemGlobal.SpecialPriceList.PriceListID);
                if (specPrice != null)
                {
                    this.PriceLists.Remove(specPrice);
                }
            }

            if (NouvemGlobal.CostPriceList != null)
            {
                var costPriceList =
                    this.PriceLists.FirstOrDefault(x => x.PriceListID == NouvemGlobal.CostPriceList.PriceListID);
                if (costPriceList != null)
                {
                    this.PriceLists.Remove(costPriceList);
                    this.PriceLists.Insert(0, costPriceList);
                }
            }

            if (NouvemGlobal.BasePriceList != null)
            {
                var basePriceList =
                   this.PriceLists.FirstOrDefault(x => x.PriceListID == NouvemGlobal.BasePriceList.PriceListID);
                if (basePriceList != null)
                {
                    this.PriceLists.Remove(basePriceList);
                    this.PriceLists.Insert(0, basePriceList);
                }
            }
           
            this.PriceLists.Insert(0, new PriceList { CurrentPriceListName = Strings.NoneSelected });
        }

        #endregion

        #region helper

        /// <summary>
        /// Sets the localised label names.
        /// </summary>
        private void SetLabelNames()
        {
            this.LabelTextName = Strings.Text;
        }

        /// <summary>
        /// Gets the application plants
        /// </summary>
        private void GetPlants()
        {
            this.Plants.Clear();
            this.Plants.Add(new Model.DataLayer.Plant { SiteName = Strings.NoneSelected});
            foreach (var plant in this.DataManager.GetAllPlants())
            {
                this.Plants.Add(plant);
            }

            this.SelectedPlant = this.Plants.FirstOrDefault(x => x.SiteName.Equals(Strings.NoneSelected));
        }

        /// <summary>
        /// Determine if the partner code already exists.
        /// </summary>
        /// <returns>Flag, as to whether the partner code exists.</returns>
        private bool DoesPartnerCodeExist()
        {
            return this.DataManager.DoesPartnerCodeExist(this.PartnerCode);
        }

        /// <summary>
        /// Determines if a contact is being added.
        /// </summary>
        /// <returns>A flag, that determines if a contact is to be added.</returns>
        private bool ContactCreated()
        {
            return !string.IsNullOrWhiteSpace(this.SelectedContactTitle)
                   || !string.IsNullOrWhiteSpace(this.SelectedContactFirstName)
                   || !string.IsNullOrWhiteSpace(this.SelectedContactLastName)
                   || !string.IsNullOrWhiteSpace(this.SelectedContactJobTitle)
                   || !string.IsNullOrWhiteSpace(this.SelectedContactDirectLine)
                   || !string.IsNullOrWhiteSpace(this.SelectedContactEmail)
                   || !string.IsNullOrWhiteSpace(this.SelectedContactMobile);
        }

        /// <summary>
        /// Determines if an address is being added.
        /// </summary>
        /// <returns>A flag, that determines if an address is to be added.</returns>
        private bool AddressCreated()
        {
            return !string.IsNullOrWhiteSpace(this.SelectedAddressLine1)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine2)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine3)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine4)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine4)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine5);
        }

        /// <summary>
        /// Determines if an address is being added.
        /// </summary>
        /// <returns>A flag, that determines if an address is to be added.</returns>
        private bool AreThereAddresses()
        {
            // check if any addresses exist (there is already a 'define new' address)
            if (this.BusinessPartnerAddresses != null && this.BusinessPartnerAddresses.Count > 1)
            {
                return true;
            }

            return !string.IsNullOrWhiteSpace(this.SelectedAddressLine1)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine2)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine3)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine4)
                   || !string.IsNullOrWhiteSpace(this.SelectedAddressLine5);
        }

        #endregion

        #endregion
    }
}
