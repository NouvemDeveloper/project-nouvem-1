﻿// -----------------------------------------------------------------------
// <copyright file="BPSearchDataViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.BusinessPartner
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    /// <summary>
    /// Class that handles the search for business partners.
    /// </summary>
    public class BPSearchDataViewModel : BPViewModelBase
    {
        #region field

        /// <summary>
        /// Show the inactve partners flag.
        /// </summary>
        private bool showInactivePartners;

        /// <summary>
        /// The search term to filter by.
        /// </summary>
        private string searchTerm;

        /// <summary>
        /// The selectyed grid partner.
        /// </summary>
        private BusinessPartner selectedSearchPartner;

        /// <summary>
        /// The filtered search partners.
        /// </summary>
        private IList<BusinessPartner> searchPartners;

        /// <summary>
        /// The filtered data collection.
        /// </summary>
        private ObservableCollection<BusinessPartner> filteredBusinessPartners;

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        private bool keepVisible;
      
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="BPSearchDataViewModel"/> class.
        /// </summary>
        public BPSearchDataViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the search type.
            Messenger.Default.Register<ViewType>(this, Token.SearchTerm, type => this.CurrentSearchType = type);

            #endregion

            #region command registration

            // Handle the ui partner selection.
            this.BusinessPartnerSelectedCommand = new RelayCommand(this.PartnerSelectedCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the form closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion

            #region instantiation

            this.FilteredBusinessPartners = new ObservableCollection<BusinessPartner>();

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether the inactive products are to be displayed
        /// </summary>
        public bool ShowInactivePartners
        {
            get
            {
                return this.showInactivePartners;
            }

            set
            {
                this.showInactivePartners = value;
                this.RaisePropertyChanged();
                this.HandlePartnersSearch();
            }
        }

        /// <summary>
        /// Gets or sets the current search type screen.
        /// </summary>
        public ViewType CurrentSearchType { get; set; }
     
        /// <summary>
        /// Gets or sets the item to search for.
        /// </summary>
        public string SearchTerm
        {
            get
            {
                return this.searchTerm;
            }

            set
            {
                this.searchTerm = value;
                this.RaisePropertyChanged();
                this.GetFilteredPartnersLocal();
            }
        }

        /// <summary>
        /// Gets or sets the selected search business partner.
        /// </summary>
        public BusinessPartner SelectedSearchPartner
        {
            get
            {
                return this.selectedSearchPartner;
            }

            set
            {
                if (value != null)
                {
                    this.selectedSearchPartner = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the filtered business partners.
        /// </summary>
        public ObservableCollection<BusinessPartner> FilteredBusinessPartners
        {
            get
            {
                return this.filteredBusinessPartners;
            }

            set
            {
                this.filteredBusinessPartners = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the partner selection.
        /// </summary>
        public ICommand BusinessPartnerSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Method that sets the partners to display.
        /// </summary>
        /// <param name="data">The partners to display.</param>
        public void SetFilteredPartners(IList<BusinessPartner> data)
        {
            this.FilteredBusinessPartners = new ObservableCollection<BusinessPartner>(data);
        }

        /// <summary>
        /// Method that sets the partners to display.
        /// </summary>
        public void SetPartners()
        {
            this.HandlePartnersSearch();
        }

        #endregion

        #endregion

        #region protected 

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected virtual void OnLoadingCommandExecute()
        {
            #region message registration

            //Messenger.Default.Register<string>(this, Token.ReportCommand, HandleReportSelection);

            #endregion

            this.ShowInactivePartners = false;
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
            this.KeepVisible = false;
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected virtual void OnClosingCommandExecute()
        {
            #region message deregistration

            Messenger.Default.Unregister<string>(this);

            #endregion

            this.KeepVisible = false;
            this.IsFormLoaded = false;
            this.CloseWindow();
        }

        /// <summary>
        /// Handles partner search.
        /// </summary>
        protected virtual void HandlePartnersSearch()
        {
            if (this.ShowInactivePartners)
            {
                var acticeItems =
                 NouvemGlobal.BusinessPartners.Where(x => (x.Details.InActiveFrom != null && x.Details.InActiveTo != null)
                                     && (DateTime.Today >= x.Details.InActiveFrom && DateTime.Today <= x.Details.InActiveTo)).OrderBy(x => x.Details.Name);
                this.FilteredBusinessPartners = new ObservableCollection<BusinessPartner>(acticeItems);
            }
            else
            {
                this.FilteredBusinessPartners = new ObservableCollection<BusinessPartner>(NouvemGlobal.BusinessPartners.Where(x => (x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                                     || (DateTime.Today < x.Details.InActiveFrom || DateTime.Today > x.Details.InActiveTo)).OrderBy(x => x.Details.Name));
            }
        }

        /// <summary>
        /// Handler for the selection of a business partner.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.PartnerSelectedCommandExecute();
        }

        /// <summary>
        /// Handler for the cancelling of the business partner search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.CloseWindow();
        }

        /// <summary>
        /// Overrides the control selection, ensuring OK is always the control mode.
        /// </summary>
        /// <param name="mode">The mode to change to.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region private 

        #region command execution

        /// <summary>
        /// Handles the selection of a partner.
        /// </summary>
        private void PartnerSelectedCommandExecute()
        {
            if (this.selectedSearchPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.SelectPartner);
                return;
            }

            if (this.CurrentSearchType == ViewType.BPSearchData)
            {
                if (!this.Locator.BPMaster.IsFormLoaded)
                {
                    Messenger.Default.Send(ViewType.BPMaster);
                }
            }

            Messenger.Default.Send(Token.Message, Token.SetTopMost);
            Messenger.Default.Send(this.selectedSearchPartner, Token.BusinessPartnerSelected);
            this.CloseWindow();
        }

        #endregion

        #region helper
        
        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        private void CloseWindow()
        {
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseWindow);
                this.FilteredBusinessPartners.Clear();
            }
        }

        /// <summary>
        /// Method that filters the business partners by the search term.
        /// </summary>
        private void GetFilteredPartners()
        {
            this.FilteredBusinessPartners.Clear();

            // Get the filtered by field(s) collection.
            this.searchPartners.ToList().ForEach(x => this.FilteredBusinessPartners.Add(x));
        }

        /// <summary>
        /// Method that filters the local filter partner list by the search term.
        /// </summary>
        private void GetFilteredPartnersLocal()
        {
            this.FilteredBusinessPartners.Clear();
            var localPartners = this.searchPartners.Where(x => x.Details.Code.StartsWithIgnoringCase(this.searchTerm));

            localPartners.ToList().ForEach(x => this.FilteredBusinessPartners.Add(x));
        }
        
        #endregion

        #endregion
    }
}
