﻿// -----------------------------------------------------------------------
// <copyright file="DeviceSearchViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace Nouvem.ViewModel.License
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    /// <summary>
    /// User search data
    /// </summary>
    public class DeviceSearchViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected grid device.
        /// </summary>
        private DeviceMaster selectedSearchDevice;

        /// <summary>
        /// The filtered data collection.
        /// </summary>
        private ObservableCollection<DeviceMaster> filteredDevices;

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        private bool keepVisible;

        /// <summary>
        /// Extra flag needed to counteract touchscreen flag when in desktop office mode.
        /// </summary>
        private bool officeMode;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceSearchViewModel"/> class.
        /// </summary>
        public DeviceSearchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration

            // Handle the ui partner selection.
            this.DeviceSelectedCommand = new RelayCommand(this.DeviceSelectedCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the form closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            // Handle the touchscreen move back command.
            this.MoveBackCommand = new RelayCommand(() => this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TransactionManager));

            #endregion

            #region instantiation

            this.FilteredDevices = new ObservableCollection<DeviceMaster>();

            #endregion

            this.GetDevices();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected search device.
        /// </summary>
        public DeviceMaster SelectedSearchDevice
        {
            get
            {
                return this.selectedSearchDevice;
            }

            set
            {
                if (value != null)
                {
                    this.selectedSearchDevice = value;
                    this.RaisePropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the filtered devices.
        /// </summary>
        public ObservableCollection<DeviceMaster> FilteredDevices
        {
            get
            {
                return this.filteredDevices;
            }

            set
            {
                this.filteredDevices = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the user selection.
        /// </summary>
        public ICommand DeviceSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the transactions careen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Method that sets the devices to display.
        /// </summary>
        /// <param name="data">The devices to display.</param>
        public void SetFilteredUsers(IList<DeviceMaster> data)
        {
            this.FilteredDevices = new ObservableCollection<DeviceMaster>(data);
            this.officeMode = true;
        }

        #endregion

        #endregion

        #region protected override command execute

        /// <summary>
        /// Handler for the selection of a device.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.DeviceSelectedCommandExecute();
        }

        /// <summary>
        /// Handler for the cancelling of the device search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.CloseWindow();
        }

        /// <summary>
        /// Overrides the control selection, ensuring OK is always the control mode.
        /// </summary>
        /// <param name="mode">The mode to change to.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handles the selection of a device.
        /// </summary>
        private void DeviceSelectedCommandExecute()
        {
            if (this.selectedSearchDevice == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.SelectDevice);
                return;
            }

            if (ApplicationSettings.TouchScreenMode && !this.officeMode)
            {
                this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TransactionManager);

                var localDevice = this.selectedSearchDevice;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(localDevice, Token.DeviceSelected);
                }));
            }
            else
            {
                if (!this.Locator.DeviceSearch.IsFormLoaded)
                {
                    Messenger.Default.Send(ViewType.DeviceSetUp);
                }

                this.Locator.DeviceSetUp.SelectedDevice = this.selectedSearchDevice;

                this.CloseWindow();
            }
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.KeepVisible = false;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            this.KeepVisible = false;
            this.IsFormLoaded = false;
            this.CloseWindow();
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets the application devices.
        /// </summary>
        private void GetDevices()
        {
            if (ApplicationSettings.TouchScreenMode)
            {
                this.FilteredDevices = new ObservableCollection<DeviceMaster>(this.DataManager.GetDevices().OrderBy(x => x.DeviceName));
                this.SelectedSearchDevice = null;
            }
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        private void CloseWindow()
        {
            if (!this.keepVisible)
            {
                Messenger.Default.Send(Token.Message, Token.CloseDeviceSearchWindow);
                this.FilteredDevices.Clear();
            }
        }

        #endregion

        #endregion
    }
}
