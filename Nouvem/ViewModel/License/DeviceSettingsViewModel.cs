﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Interface;

namespace Nouvem.ViewModel.License
{
    public class DeviceSettingsViewModel : NouvemViewModelBase
    {

        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceSettingsViewModel"/> class.
        /// </summary>
        public DeviceSettingsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.SwitchControl, this.ControlChange);

            #endregion

            #region command handler

            #endregion
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    break;

                case ControlMode.Update:
                    break;
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        private void Close()
        {
            Messenger.Default.Send(Token.Message,Token.CloseDeviceSettingsWindow);
        }

        #endregion


    }
}
