﻿// -----------------------------------------------------------------------
// <copyright file="UserChangePasswordViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Nouvem.ViewModel.User
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class UserChangePasswordViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The user password.
        /// </summary>
        private string password;

        /// <summary>
        /// The user confirmed password.
        /// </summary>
        private string confirmedPassword;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// The focused ui control.
        /// </summary>
        private string focusedControl = "Password";

        /// <summary>
        /// The application users.
        /// </summary>
        private IList<Model.BusinessObject.User> users;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the UserChangePasswordViewModel class.
        /// </summary>
        public UserChangePasswordViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.HasWriteAuthorisation = true;

            #region message registration

            // When in epos mode, the touchpad selection is sent. 
            Messenger.Default.Register<string>(this, Token.SetEposSelection, s =>
            {
                if (this.focusedControl.Equals(Constant.Password))
                {
                    this.Password = s;
                }
                else
                {
                    this.ConfirmedPassword = s;
                }
            });

            #endregion

            #region command registration

            // Change the control mode to update
            this.UpdatePasswordCommand = new RelayCommand(() => this.UpdatePassword());

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update));

            // handle the epos password change load event.
            this.OnLoadingCommand = new RelayCommand(() => this.Password = string.Empty);

            // handle the epos password change unload event.
            this.OnUnLoadedCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.ResetTouchPad));

            this.OnGotFocusCommand = new RelayCommand<string>(s =>
            {
                this.focusedControl = s;
                Messenger.Default.Send(Token.Message, Token.ResetTouchPad);
            });

            #endregion

            this.SetControlMode(ControlMode.Update);
            
            if (Settings.Default.EposMode)
            {
                this.users = this.DataManager.GetUsers();
            }
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the user password.
        /// </summary>
        public string Password
        {
            get
            {
                return this.password;
            }

            set
            {
                this.password = value;
                this.RaisePropertyChanged();
                this.SetControlMode(ControlMode.Update);
                this.ErrorMessage = string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the user confirmed password.
        /// </summary>
        public string ConfirmedPassword
        {
            get
            {
                return this.confirmedPassword;
            }

            set
            {
                this.confirmedPassword = value;
                this.RaisePropertyChanged();
                this.SetControlMode(ControlMode.Update);
                this.ErrorMessage = string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                this.errorMessage = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdatePasswordCommand { get; private set; }

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event.
        /// </summary>
        public ICommand OnUnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the got focus event.
        /// </summary>
        public ICommand OnGotFocusCommand { get; private set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdatePassword();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the password.
        /// </summary>
        private void UpdatePassword()
        {
            #region validation

            if (string.IsNullOrWhiteSpace(this.password))
            {
                this.ErrorMessage = Message.PasswordEmpty;
                return;
            }

            if (!this.password.Equals(this.confirmedPassword))
            {
                this.ErrorMessage = Message.PasswordsNotMatch;
                return;
            }

            if (ApplicationSettings.UserPasswordMustBeUnique)
            {
                // ensure the password is unique
                if (this.users.Any(x => Security.Security.DecryptString(x.UserMaster.Password).Equals(this.password)))
                {
                    NouvemMessageBox.Show(Message.PasswordExists, touchScreen:true);
                    return;
                }
            }

            #endregion
            
            try
            {
                if (this.DataManager.UpdatePassword(ApplicationSettings.LoggedInUserIdentifier, this.password))
                {
                    SystemMessage.Write(MessageType.Priority, Message.PasswordUpdated);
                    Messenger.Default.Send(Message.PasswordUpdated, Token.PasswordChangeConfirmation);
                    this.Close();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PasswordNotUpdated);
                    Messenger.Default.Send(Message.PasswordNotUpdated, Token.PasswordChangeConfirmation);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.PasswordNotUpdated);
                Messenger.Default.Send(Message.PasswordNotUpdated, Token.PasswordChangeConfirmation);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close, clean up, and open the main application (or log in to the epos system, if we are in epos mode)
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearUserChangePassword();
            Messenger.Default.Send(Token.Message,Token.CloseUserChangePasswordWindow);
      
            if (ApplicationSettings.TouchScreenModeOnly)
            {
                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenView);
            }
            else
            {
                Messenger.Default.Send(Token.Message, Token.CreateMasterView);
            }
        }

        #endregion
    }
}