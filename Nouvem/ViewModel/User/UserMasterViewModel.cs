﻿// -----------------------------------------------------------------------
// <copyright file="UserMasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.User
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Win32;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
    using Nouvem.ViewModel.Interface;
    using Nouvem.Security;

    public class UserMasterViewModel : NouvemViewModelBase, IRibbonBarCommand
    {
        #region field

        /// <summary>
        /// active box checked flag.
        /// </summary>
        private bool checkBoxActive;

        /// <summary>
        /// inactive box checked flag.
        /// </summary>
        private bool checkBoxInactive;

        /// <summary>
        /// The set up user name.
        /// </summary>
        private string setUpUserName;

        /// <summary>
        /// The set up full name.
        /// </summary>
        private string setUpUserFullName;

        /// <summary>
        /// The reference.
        /// </summary>
        private string reference;

        /// <summary>
        /// The set up user email.
        /// </summary>
        private string setUpUserEmail;

        /// <summary>
        /// The set up user mobile.
        /// </summary>
        private string setUpUserMobile;

        /// <summary>
        /// The set up user password.
        /// </summary>
        private string setUpUserPassword;

        /// <summary>
        /// The set up user confirmation password.
        /// </summary>
        private bool passwordConfirmed;

        /// <summary>
        /// The password doesn't expire flag.
        /// </summary>
        private bool setUpPasswordNeverExpires;

        /// <summary>
        /// The change password flag.
        /// </summary>
        private bool setUpChangePasswordAtNextLogin;

        /// <summary>
        /// The suspend user flag.
        /// </summary>
        private bool setUpSuspendUser;

        /// <summary>
        /// The set up selected group.
        /// </summary>
        private UserGroup_ setUpSelectedUserGroup;

        /// <summary>
        /// The user active to date.
        /// </summary>
        private DateTime? userActiveToDate;

        /// <summary>
        /// The user active from date.
        /// </summary>
        private DateTime? userActiveFromDate;

        /// <summary>
        /// The user inactive to date.
        /// </summary>
        private DateTime? userInactiveToDate;

        /// <summary>
        /// The user inactive from date.
        /// </summary>
        private DateTime? userInactiveFromDate;

        /// <summary>
        /// The user photo image.
        /// </summary>
        private byte[] userPhoto;

        /// <summary>
        /// The selected user.
        /// </summary>
        private User selectedUser;

        /// <summary>
        /// The users.
        /// </summary>
        private ObservableCollection<User> users;

        /// <summary>
        /// The selected user snapshot.
        /// </summary>
        private User selectedUserSnapshot;

        /// <summary>
        /// The search mode flag.
        /// </summary>
        private bool searchMode;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the UserMasterViewModel class.
        /// </summary>
        public UserMasterViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register to update the user groups.
            Messenger.Default.Register<string>(this, Token.UpdateUserGroups, s => this.GetUserGroups());

            // receives the incoming password.
            Messenger.Default.Register<string>(this, Token.UserPassword, x => this.setUpUserPassword = x);

            // receives the incoming password.
            Messenger.Default.Register<bool>(this, Token.UserPassword, b =>
            {
                if (this.SelectedUser != null && this.SelectedUser.UserMaster.UserMasterID > 0)
                {
                    this.SetControlMode(ControlMode.Update);
                }
                else
                {
                    this.SetControlMode(ControlMode.Add);
                }
            });

            // Register for a master window ribbon navigation command.
            Messenger.Default.Register<RibbonCommand>(this, Token.NavigationCommand, this.HandleRibbonNavigation);

            // Register for a master window change control command.
            Messenger.Default.Register<string>(
                this,
                Token.SwitchControl,
                x =>
                {
                    this.ControlChange(x);
                    //// Only change control if the BPMasterWindowView is the active window.
                    //if (this.IsFormActive)
                    //{
                    //    this.ControlChange(x);
                    //}
                });

            Messenger.Default.Register<string>(
                this,
                Token.UserPasswordConfirm,
                confirmationPassword =>
                {
                    if (confirmationPassword != this.setUpUserPassword)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.PasswordsNotMatch);
                        Messenger.Default.Send(Token.Message, Token.ResetConfirmationPassword);
                        this.passwordConfirmed = false;
                        return;
                    }

                    this.passwordConfirmed = true;
                });

            // Register for an incoming user selection.
            Messenger.Default.Register<int>(this, Token.UserSelected, i => this.SelectedUser = this.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == i));

            #endregion

            #region command registration

            // Handle the user search
            this.FindUserCommand = new RelayCommand<KeyEventArgs>(e => this.FindUserCommandExecute(), e => e.Key == Key.F1);

            // Handle the user photo uploading.
            this.UploadImageCommand = new RelayCommand(() =>
            {
                if (this.selectedUser.UserMaster.UserMasterID == 0)
                {
                    this.SetControlMode(ControlMode.Add);
                }
                else
                {
                    this.SetControlMode(ControlMode.Update);
                }

                this.UploadImageCommandExecute();
            });

            // Handle the window load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the window close.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion

            #region instantiation

            this.UserGroups = new ObservableCollection<UserGroup_>();

            #endregion

            this.GetUsers();
            this.GetUserGroups();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether the inactive chaeck box is checked.
        /// </summary>
        public bool CheckBoxInactive
        {
            get
            {
                return this.checkBoxInactive;
            }

            set
            {
                this.SetMode(value, this.checkBoxInactive);
                this.checkBoxInactive = value;
                this.RaisePropertyChanged();

                if (value && this.SelectedUser != null && !this.UserInactiveFromDate.HasValue &&
                    !this.UserInactiveToDate.HasValue)
                {
                    this.UserInactiveFromDate = DateTime.Today;
                    this.UserInactiveToDate = DateTime.Today.AddYears(100);
                }
                else if (!value)
                {
                    this.UserInactiveFromDate = null;
                    this.UserInactiveToDate = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the active chaeck box is checked.
        /// </summary>
        public bool CheckBoxActive
        {
            get
            {
                return this.checkBoxActive;
            }

            set
            {
                this.SetMode(value, this.checkBoxActive);
                this.checkBoxActive = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the active chaeck box is checked.
        /// </summary>
        public string SetUpUserPassword
        {
            get
            {
                return this.setUpUserPassword;
            }

            set
            {
                this.setUpUserPassword = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected user.
        /// </summary>
        public User SelectedUser
        {
            get
            {
                return this.selectedUser;
            }

            set
            {
                if (value == null)
                {
                    return;
                }

                if (!this.Users.Any(x => x.UserMaster.UserMasterID == value.UserMaster.UserMasterID))
                {
                    this.Users.Add(value);
                }

                this.selectedUser = value;
                this.RaisePropertyChanged();

                this.selectedUserSnapshot = value.Copy;
                this.HandleSelectedUser();
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public ObservableCollection<User> Users
        {
            get
            {
                return this.users;
            }

            set
            {
                this.users = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the puser active from date.
        /// </summary>
        public DateTime? UserActiveFromDate
        {
            get
            {
                return this.userActiveFromDate;
            }

            set
            {
                this.SetMode(value, this.userActiveFromDate);
                this.userActiveFromDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the User active to date.
        /// </summary>
        public DateTime? UserActiveToDate
        {
            get
            {
                return this.userActiveToDate;
            }

            set
            {
                this.SetMode(value, this.userActiveToDate);
                this.userActiveToDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the User inactive from date.
        /// </summary>
        public DateTime? UserInactiveFromDate
        {
            get
            {
                return this.userInactiveFromDate;
            }

            set
            {
                this.SetMode(value, this.userInactiveFromDate);
                this.userInactiveFromDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the User inactive to date.
        /// </summary>
        public DateTime? UserInactiveToDate
        {
            get
            {
                return this.userInactiveToDate;
            }

            set
            {
                this.SetMode(value, this.userInactiveToDate);
                this.userInactiveToDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user reference.
        /// </summary>
        public string Reference
        {
            get
            {
                return this.reference;
            }

            set
            {
                this.SetMode(value, this.reference);
                this.reference = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the set up user full name.
        /// </summary>
        public string SetUpUserFullName
        {
            get
            {
                return this.setUpUserFullName;
            }

            set
            {
                this.SetMode(value, this.setUpUserFullName);
                this.setUpUserFullName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the set up user photo image.
        /// </summary>
        public byte[] UserPhoto
        {
            get
            {
                return this.userPhoto;
            }

            set
            {
                this.userPhoto = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the set up name.
        /// </summary>
        public string SetUpUserName
        {
            get
            {
                return this.setUpUserName;
            }

            set
            {
                this.SetMode(value, this.setUpUserName);
                this.setUpUserName = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are in search mode.
        /// </summary>
        public bool SearchMode
        {
            get
            {
                return this.searchMode;
            }

            set
            {
                this.searchMode = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the set up user email.
        /// </summary>
        public string SetUpUserEmail
        {
            get
            {
                return this.setUpUserEmail;
            }

            set
            {
                this.SetMode(value, this.setUpUserEmail);
                this.setUpUserEmail = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the set up user mobile.
        /// </summary>
        public string SetUpUserMobile
        {
            get
            {
                return this.setUpUserMobile;
            }

            set
            {
                this.SetMode(value, this.setUpUserMobile);
                this.setUpUserMobile = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the set up selected group.
        /// </summary>
        public UserGroup_ SetUpSelectedUserGroup
        {
            get
            {
                return this.setUpSelectedUserGroup;
            }

            set
            {
                this.SetMode(value, this.setUpSelectedUserGroup);
                this.setUpSelectedUserGroup = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a password never expires.
        /// </summary>
        public bool SetUpPasswordNeverExpires
        {
            get
            {
                return this.setUpPasswordNeverExpires;
            }

            set
            {
                this.SetMode(value, this.setUpPasswordNeverExpires);
                this.setUpPasswordNeverExpires = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a password is to be cahnged at next login.
        /// </summary>
        public bool SetUpChangePasswordAtNextLogin
        {
            get
            {
                return this.setUpChangePasswordAtNextLogin;
            }

            set
            {
                this.SetMode(value, this.setUpChangePasswordAtNextLogin);
                this.setUpChangePasswordAtNextLogin = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a user is suspended.
        /// </summary>
        public bool SetUpSuspendUser
        {
            get
            {
                return this.setUpSuspendUser;
            }

            set
            {
                this.SetMode(value, this.setUpSuspendUser);
                this.setUpSuspendUser = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the user groups.
        /// </summary>
        public ObservableCollection<UserGroup_> UserGroups { get; private set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the find user command.
        /// </summary>
        public ICommand FindUserCommand { get; private set; }

        /// <summary>
        /// Gets the command to upload a user photo image.
        /// </summary>
        public ICommand UploadImageCommand { get; private set; }

        /// <summary>
        /// Gets the loading event command.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the closing event command.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region IRibbonBarCommand implementation

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public void MoveBack()
        {
            if (this.SelectedUser == null)
            {
                return;
            }

            // Remove the nouvem user from the search
            var localUsers = this.Users;
            var nouvemUser =
                localUsers.FirstOrDefault(
                    user =>
                    (user.UserMaster.UserName == Constant.NouvemLogin
                     && Security.DecryptString(user.UserMaster.Password) == ApplicationSettings.NouvemPassword));

            if (nouvemUser != null)
            {
                localUsers.Remove(nouvemUser);
            }

            var previousUser =
                localUsers.TakeWhile(
                    user => user.UserMaster.UserMasterID != this.SelectedUser.UserMaster.UserMasterID)
                    .LastOrDefault();

            if (previousUser != null)
            {
                this.SelectedUser = previousUser;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateBack);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public void MoveNext()
        {
            if (this.SelectedUser == null)
            {
                return;
            }

            // Remove the nouvem user from the search
            var localUsers = this.Users;
            var nouvemUser =
                localUsers.FirstOrDefault(
                    user =>
                    (user.UserMaster.UserName == Constant.NouvemLogin
                     && Security.DecryptString(user.UserMaster.Password) == ApplicationSettings.NouvemPassword));

            if (nouvemUser != null)
            {
                localUsers.Remove(nouvemUser);
            }

            var nextUser =
                localUsers.SkipWhile(
                    user => user.UserMaster.UserMasterID != this.SelectedUser.UserMaster.UserMasterID)
                    .Skip(1)
                    .FirstOrDefault();

            if (nextUser != null)
            {
                this.SelectedUser = nextUser;
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.CannotNavigateFurther);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public void MoveFirst()
        {
            if (this.SelectedUser == null)
            {
                return;
            }

            // Remove the nouvem user from the search
            var localUsers = this.Users;
            var nouvemUser =
                localUsers.FirstOrDefault(
                    user =>
                    (user.UserMaster.UserName == Constant.NouvemLogin
                     && Security.DecryptString(user.UserMaster.Password) == ApplicationSettings.NouvemPassword));

            if (nouvemUser != null)
            {
                localUsers.Remove(nouvemUser);
            }

            var firstUser = localUsers.FirstOrDefault();

            if (firstUser != null)
            {
                this.SelectedUser = firstUser;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public void MoveLast()
        {
            if (this.SelectedUser == null)
            {
                return;
            }

            // Remove the nouvem user from the search
            var localUsers = this.Users;
            var nouvemUser =
                localUsers.FirstOrDefault(
                    user =>
                    (user.UserMaster.UserName == Constant.NouvemLogin
                     && Security.DecryptString(user.UserMaster.Password) == ApplicationSettings.NouvemPassword));

            if (nouvemUser != null)
            {
                localUsers.Remove(nouvemUser);
            }

            var lastUser = localUsers.LastOrDefault();

            if (lastUser != null)
            {
                this.SelectedUser = lastUser;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public void MoveLastEdit()
        {
            if (this.Users == null || !this.Users.Any())
            {
                return;
            }

            this.SelectedUser = this.Users.OrderByDescending(x => x.UserMaster.EditDate).First();
        }

        /// <summary>
        /// Handler for the ribbon bar navigation selection.
        /// </summary>
        /// <param name="command">The ribbon bar navigation command</param>
        public void HandleRibbonNavigation(RibbonCommand command)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            switch (command)
            {
                case RibbonCommand.Back:
                    this.MoveBack();
                    break;

                case RibbonCommand.Next:
                    this.MoveNext();
                    break;

                case RibbonCommand.First:
                    this.MoveFirst();
                    break;

                case RibbonCommand.Last:
                    this.MoveLast();
                    break;

                case RibbonCommand.LastEdit:
                    this.MoveLastEdit();
                    break;
            }
        }

        /// <summary>
        /// Handler for the master window control command change notification.
        /// </summary>
        /// <param name="mode">The control mode to change to.</param>
        public void ControlChange(string mode)
        {
            this.ControlCommandExecute(mode);
        }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Drill down to user group.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            Messenger.Default.Send(Tuple.Create(ViewType.UserGroup, 0), Token.DrillDown);
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new partner creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var localMode = this.CurrentMode;
            base.ControlCommandExecute(mode);
            this.SearchMode = false;

            // check the add/update permissions
            if (this.ControlModeWrite)
            {
                if (!this.HasWriteAuthorisation)
                {
                    // no write permissions, so revert to original mode.
                    this.SetControlMode(localMode);
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }
            }

            if (this.CurrentMode == ControlMode.Add)
            {
                // Moving into add new user mode, so clear the partner fields.
                this.ClearForm();
                this.SetControlMode(ControlMode.Add);
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                return;
            }

            if (this.CurrentMode == ControlMode.Find)
            {
                // Moving into add new user mode, so clear the partner fields.
                this.ClearForm();
                this.SetControlMode(ControlMode.Find);
                this.SearchMode = true;
                Messenger.Default.Send(Token.Message, Token.FocusToDataUpdate);
            }
        }

        /// <summary>
        /// Method that sets up the ui to add a new user.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddNewUser();
                    break;

                case ControlMode.Find:
                    this.FindUser();
                    break;

                case ControlMode.Update:
                    this.UpdateUser();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Method that clears the form if in emdedded user control mode,
        /// or closes the form if in pop up window mode.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.ClearForm();
            this.SetControlMode(ControlMode.Add);
            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);

            // If we are in pop up window mode, close it.
            this.Close();
        }

        /// <summary>
        /// Checks for the existance of a current selected user.
        /// </summary>
        /// <returns>A flag, indicating the existence of a selected user.</returns>
        protected override bool CheckForEntity()
        {
            return this.selectedUser != null && this.selectedUser.UserMaster.UserMasterID > 0;
        }

        /// <summary>
        /// Overrides the command to display the audit screen.
        /// </summary>
        /// <param name="s">The command parameter.</param>
        protected override void AuditDisplayCommandExecute(string s)
        {
            base.AuditDisplayCommandExecute(s);

            if (s.Equals(Constant.UserMaster) && this.selectedUser.UserMaster.UserMasterID > 0)
            {
                Messenger.Default.Send(Tuple.Create(Constant.UserMaster, this.selectedUser.UserMaster.UserMasterID), Token.AuditData);
            }
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handler to upload an item image
        /// </summary>
        private void UploadImageCommandExecute()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "PNG|*.png|BMP|*.bmp|GIF|*.gif|JPG|*.jpg;*.jpeg|TIFF|*.tif;*.tiff";

            if (openFileDialog.ShowDialog() == true)
            {
                this.UserPhoto = File.ReadAllBytes(openFileDialog.FileName);
            }
        }

        /// <summary>
        /// Handler for the user search.
        /// </summary>
        private void FindUserCommandExecute()
        {
            this.FindUser();
        }

        /// <summary>
        /// Handler for the form load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.CreateBlankUser();
            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessUsers);
            var localMode = this.HasWriteAuthorisation ? ControlMode.Add : ControlMode.OK;
            this.SetControlMode(localMode);
            this.IsFormLoaded = true;
        }

        /// <summary>
        /// Handler for the form load event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            this.CancelSelectionCommandExecute();
            this.IsFormLoaded = false;
            this.UserGroups?.Clear();
            this.Users?.Clear();
            this.UserGroups = null;
            this.Users = null;
        }

        #endregion

        #region helper

        /// <summary>
        /// Method that closes the window.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearUserMaster();
            Messenger.Default.Send(Token.Message, Token.CloseUserSetUpWindow);
        }

        /// <summary>
        /// Method that adds a new user.
        /// </summary>
        private void AddNewUser()
        {
            #region validation

            string error;
            if (!this.ValidateUser(out error))
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            var localUser = this.CreateOrUpdateUser();

            try
            {
                if (this.DataManager.AddOrUpdateUser(localUser, this.selectedUserSnapshot))
                {
                    SystemMessage.Write(MessageType.Priority, Message.UserAdded);
                    this.Users.Add(localUser);
                    this.ClearForm();
                    this.GetUsers();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.UserNotAdded);
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Search for the user matching the search parameters.
        /// </summary>
        private void FindUser()
        {
            // Remove the nouvem user, if it's there.
            var nouvemUser =
                this.Users.FirstOrDefault(
                    x => x.UserMaster.UserName != null &&
                    x.UserMaster.UserName.CompareIgnoringCase("Nouvem"));

            if (nouvemUser != null)
            {
                this.Users.Remove(nouvemUser);
            }
           
            Messenger.Default.Send(ViewType.UserSearchData);
        }

        /// <summary>
        /// Update the selected user.
        /// </summary>
        private void UpdateUser()
        {
            #region validation

            var error = string.Empty;
            if (this.SelectedUser == null)
            {
                error = Message.NoUserSelected;
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            if (!this.ValidateUser(out error))
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            var localUser = this.CreateOrUpdateUser();

            try
            {
                if (this.DataManager.AddOrUpdateUser(localUser, this.selectedUserSnapshot))
                {
                    SystemMessage.Write(MessageType.Priority, Message.UserUpdated);
                    this.Users.Add(localUser);
                    this.selectedUserSnapshot = this.selectedUser;
                    this.ClearForm();
                    this.GetUsers();
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.UserNotAdded);
                }
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Creates, or updates a user.
        /// </summary>
        /// <returns>The newly created, or updated user.</returns>
        private User CreateOrUpdateUser()
        {
            if (this.SelectedUser == null)
            {
                this.SelectedUser = new User { UserMaster = new UserMaster() };
            }

            this.SelectedUser.UserMaster.UserName = this.setUpUserName;
            this.SelectedUser.UserMaster.FullName = this.setUpUserFullName;
            this.SelectedUser.UserMaster.DeviceID = NouvemGlobal.DeviceId;
            this.SelectedUser.UserMaster.EditDate = DateTime.Now;
            this.SelectedUser.UserMaster.Reference = this.reference;
            this.SelectedUser.UserMaster.Password = Security.EncryptString(this.setUpUserPassword);
            this.SelectedUser.UserMaster.Mobile = this.setUpUserMobile ?? string.Empty;
            this.SelectedUser.UserMaster.Email = this.setUpUserEmail ?? string.Empty;
            this.SelectedUser.UserMaster.ChangeAtNextLogin = this.SetUpChangePasswordAtNextLogin;
            this.SelectedUser.UserMaster.PasswordNeverExpires = this.setUpPasswordNeverExpires;
            this.SelectedUser.UserMaster.SuspendUser = this.setUpSuspendUser;
            this.SelectedUser.UserMaster.InActiveFrom = this.UserInactiveFromDate;
            this.SelectedUser.UserMaster.InActiveTo = this.UserInactiveToDate;
            this.SelectedUser.UserMaster.ActiveFrom = this.UserActiveFromDate;
            this.SelectedUser.UserMaster.ActiveTo = this.UserActiveToDate;
            this.SelectedUser.Group = this.setUpSelectedUserGroup;
            this.SelectedUser.UserMaster.Photo = this.userPhoto;

            if (ApplicationSettings.UseDisplayPassword)
            {
                this.SelectedUser.UserMaster.DisplayPassword = this.setUpUserPassword;
            }

            return this.selectedUser;
        }

        /// <summary>
        /// Creates a blank selected user.
        /// </summary>
        private void CreateBlankUser()
        {
            this.SelectedUser = new User { UserMaster = new UserMaster(), Group = new UserGroup_ { Description = string.Empty } };
        }

        /// <summary>
        /// Method that determines if a search can be made.
        /// </summary>
        /// <returns>A flag, indicating a valid search.</returns>
        private bool IsSearchValid()
        {
            return !(string.IsNullOrWhiteSpace(this.SetUpUserName) && string.IsNullOrWhiteSpace(this.SetUpUserFullName)
                   && (this.SetUpSelectedUserGroup == null || this.SetUpSelectedUserGroup.Description.Equals(Strings.NoneSelected)));
        }

        /// <summary>
        /// Method that validates a new user.
        /// </summary>
        /// <param name="error">The error message.</param>
        /// <returns>A flag, indication successful validation or not.</returns>
        private bool ValidateUser(out string error)
        {
            error = string.Empty;
            //if (NouvemGlobal.CheckAuthorisation(Authorisation.AllowUserToAccessUsers) != Constant.FullAccess)
            //{
            //    error = Message.AccessDenied;
            //    return false;
            //}

            if (this.userInactiveFromDate != null && this.UserInactiveToDate != null &&
                DateTime.Today >= this.UserInactiveFromDate && DateTime.Today <= this.UserInactiveToDate)
            {
                error = string.Empty;
                return true;
            }

            if (string.IsNullOrWhiteSpace(this.SetUpUserName))
            {
                error = Message.EnterUserName;
            }
            else if (this.selectedUser != null && this.selectedUser.UserMaster != null && this.SetUpUserName != 
                     this.selectedUser.UserMaster.UserName && this.Users.Any(x => x.UserMaster.UserName != null && x.UserMaster.UserName.ToLower()
                                                                                      .Equals(this.SetUpUserName.ToLower())))
            {
                error = Message.UserNameExists;
            }
            else if (string.IsNullOrWhiteSpace(this.SetUpUserFullName))
            {
                error = Message.EnterFullName;
            }
            else if (this.SetUpSelectedUserGroup == null || this.SetUpSelectedUserGroup.Description.Equals(Strings.NoneSelected))
            {
                error = Message.EnterUserGroup;
            }
            else if (string.IsNullOrWhiteSpace(this.setUpUserPassword) && this.SelectedUser.UserMaster?.UserMasterID == 0)
            {
                error = Message.EnterPassword;
            }
            else if (!this.passwordConfirmed)
            {
                error = Message.ConfirmPassword;
            }
            else if (ApplicationSettings.PasswordMustBeUnique && this.Users
                .Any(x => Security.DecryptString(x.UserMaster.Password).CompareIgnoringCase(this.setUpUserPassword) && (this.selectedUser != null && x.UserMaster.UserMasterID != this.selectedUser.UserMaster.UserMasterID)))
            {
                // User passwords must be unique.
                error = Message.PasswordExists;
            }

            return error.Equals(string.Empty);
        }

        /// <summary>
        /// Method that clears the form.
        /// </summary>
        private void ClearForm()
        {
            this.SetUpUserEmail = string.Empty;
            this.SetUpUserName = string.Empty;
            this.SetUpUserMobile = string.Empty;
            this.SetUpUserFullName = string.Empty;
            this.Reference = string.Empty;
            this.setUpUserPassword = string.Empty;
            this.SetUpPasswordNeverExpires = false;
            this.SetUpChangePasswordAtNextLogin = false;
            this.SetUpSuspendUser = false;
            this.passwordConfirmed = false;
            this.UserActiveFromDate = null;
            this.UserActiveToDate = null;
            this.UserInactiveFromDate = null;
            this.UserInactiveToDate = null;
            this.CheckBoxInactive = false;
            this.CheckBoxActive = false;
            this.CreateBlankUser();
            Messenger.Default.Send(Token.Message, Token.ResetPasswords);
        }

        /// <summary>
        /// Gets the application users.
        /// </summary>
        private void GetUsers()
        {
            this.Users = new ObservableCollection<User>(this.DataManager.GetUsers().Where(x => x.UserMaster.InActiveFrom == null
                                                                                               || x.UserMaster.InActiveTo == null
                                                                                               || x.UserMaster.InActiveFrom > DateTime.Today
                                                                                               || x.UserMaster.InActiveTo < DateTime.Today));
        }

        /// <summary>
        /// Gets the application user groups.
        /// </summary>
        private void GetUserGroups()
        {
            this.UserGroups.Clear();
            this.UserGroups.Add(new UserGroup_ { Description = Strings.NoneSelected });
            foreach (var group in this.DataManager.GetUserGroups())
            {
                this.UserGroups.Add(group);
            }

            this.SetUpSelectedUserGroup =
                this.UserGroups.FirstOrDefault(x => x.Description.Equals(Strings.NoneSelected));
        }

        /// <summary>
        /// handles the selection of a new user, parsing to the ui.
        /// </summary>
        private void HandleSelectedUser()
        {
            this.CheckBoxInactive = false;
            this.CheckBoxActive = false;
            this.SetUpUserName = this.selectedUser.UserMaster.UserName;
            this.SetUpUserFullName = this.selectedUser.UserMaster.FullName;
            this.Reference = this.selectedUser.UserMaster.Reference;
            this.SetUpUserEmail = this.selectedUser.UserMaster.Email;
            this.SetUpUserMobile = this.selectedUser.UserMaster.Mobile;
            this.SetUpPasswordNeverExpires = this.selectedUser.UserMaster.PasswordNeverExpires.ToBool();
            this.SetUpChangePasswordAtNextLogin = this.selectedUser.UserMaster.ChangeAtNextLogin.ToBool();
            this.SetUpSuspendUser = this.selectedUser.UserMaster.SuspendUser.ToBool();
            this.UserActiveFromDate = this.selectedUser.UserMaster.ActiveFrom;
            this.UserActiveToDate = this.selectedUser.UserMaster.ActiveTo;
            this.UserInactiveFromDate = this.selectedUser.UserMaster.InActiveFrom;
            this.UserInactiveToDate = this.selectedUser.UserMaster.InActiveTo;
            this.SetUpSelectedUserGroup = this.UserGroups.FirstOrDefault(x => x.Description.Equals(this.selectedUser.Group.Description));
            this.SetUpUserPassword = Security.DecryptString(this.selectedUser.UserMaster.Password);
            this.passwordConfirmed = true;
            this.UserPhoto = this.selectedUser.UserMaster.Photo ?? new Uri("pack://application:,,,/Design/Image/NoImageAvailable.jpg").UriToBytes();
            if (this.userInactiveFromDate != null && this.UserInactiveToDate != null &&
                DateTime.Today >= this.UserInactiveFromDate && DateTime.Today <= this.UserInactiveToDate)
            {
                this.CheckBoxInactive = true;
            }

            Messenger.Default.Send(this.selectedUser.UserMaster.Password, Token.ResetPasswords);
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #endregion
    }
}