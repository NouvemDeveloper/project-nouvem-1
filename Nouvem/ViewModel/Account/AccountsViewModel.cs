﻿// -----------------------------------------------------------------------
// <copyright file="AccountsViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.Reporting.WinForms;
using Nouvem.Model.DataLayer;

namespace Nouvem.ViewModel.Account
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class AccountsViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected grid sales.
        /// </summary>
        private ObservableCollection<Sale> invoices = new ObservableCollection<Sale>();

        /// <summary>
        /// The selected grid sales.
        /// </summary>
        private IList<Sale> allInvoices = new List<Sale>();

        /// <summary>
        /// Gets or sets the posting date.
        /// </summary>
        private DateTime postingDate;

        /// <summary>
        /// The running total.
        /// </summary>
        private decimal runningTotal;

        /// <summary>
        /// The show all invoices flag.
        /// </summary>
        private bool showAllInvoices;
       
        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsViewModel"/> class.
        /// </summary>
        public AccountsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            // Register for the incoming customers sales.
            Messenger.Default.Register<IList<string>>(this, Token.RefreshInvoices, s =>
            {
                this.Refresh();
            });

            // Register for the a customers sales refresh.
            Messenger.Default.Register<string>(this, Token.RefreshInvoices, s =>
            {
                this.Refresh();
            });

            #endregion

            #region command handler

            this.RefreshCommand = new GalaSoft.MvvmLight.CommandWpf.RelayCommand(() =>
            {
                this.Refresh();
                SystemMessage.Write(MessageType.Priority, string.Format("Invoices refreshed. {0} invoices found", this.Invoices.Count));
            });

            // The handler for the load event.
            this.OnLoadingCommand= new RelayCommand(this.OnLoadingCommandExecute);

            // The handler for the grid selection changed event.
            this.OnSelectionChangedCommand = new RelayCommand(this.OnSelectionChangedCommandExecute);

            // Display the selected invoice.
            this.ShowInvoiceCommand = new RelayCommand(() =>
            {
                var localInvoice = this.SelectedInvoices.FirstOrDefault();
                if (localInvoice != null)
                {
                    localInvoice = this.DataManager.GetInvoicesById(localInvoice.SaleID);
                    Messenger.Default.Send(ViewType.Invoice);
                    this.Locator.Invoice.Sale = localInvoice;
                    SystemMessage.Write(MessageType.Priority, Message.InvoiceScreenLoaded);
                }
            });

            #endregion
        
            this.Refresh();
            this.SelectedInvoices = new List<Sale>();
            this.PostingDate = DateTime.Today;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets a value indicating whether all the invoices are to be dispalyed.
        /// </summary>
        public bool ShowAllInvoices
        {
            get
            {
                return this.showAllInvoices;
            }

            set
            {
                this.showAllInvoices = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    if (value)
                    {
                        this.Invoices = new ObservableCollection<Sale>(this.allInvoices);
                    }
                    else
                    {
                        this.Invoices = new ObservableCollection<Sale>(this.allInvoices.Where(x => x.NouDocStatusID != NouvemGlobal.NouDocStatusExported.NouDocStatusID
                           && x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID));
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the running total.
        /// </summary>
        public decimal RunningTotal
        {
            get
            {
                return this.runningTotal;
            }

            set
            {
                this.runningTotal = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the posting date.
        /// </summary>
        public DateTime PostingDate
        {
            get
            {
                return this.postingDate;
            }

            set
            {
                this.postingDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected invoice.
        /// </summary>
        //public Sale SelectedInvoice { get; set; }

        /// <summary>
        /// Gets or sets the non exported invoices.
        /// </summary>
        public IList<Sale> SelectedInvoices { get; set; }

        /// <summary>
        /// Gets or sets the non exported invoices.
        /// </summary>
        public ObservableCollection<Sale> Invoices
        {
            get
            {
                return this.invoices;
            }

            set
            {
                if (value != null)
                {
                    this.invoices = value;
                    this.RaisePropertyChanged();

                    if (value.Any())
                    {
                        this.SetControlMode(ControlMode.Export);
                    }
                    else
                    {
                        this.SetControlMode(ControlMode.OK);
                    }
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to display the selected dinvoice.
        /// </summary>
        public ICommand ShowInvoiceCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        /// <summary>
        /// Gets the grid selection changed command handler.
        /// </summary>
        public ICommand OnSelectionChangedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Override the cancellation to close locally.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected void OnClosingCommandExecute()
        {
            this.Invoices?.Clear();
            this.Invoices = null;
            this.Close();
        }

        /// <summary>
        /// Override, to only allow export/ok.
        /// </summary>
        /// <param name="mode">The current mode.</param>
        protected override void ControlCommandExecute(string mode)
        {
            if (this.CurrentMode == ControlMode.Find || this.CurrentMode == ControlMode.Update || this.CurrentMode == ControlMode.Add)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Export:
                    if (this.AccountsManager.CanConnectToAccounts)
                    {
                        this.ExportInvoices();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoAccountsAccess);
                    }
                 
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected void Close()
        {
            ViewModelLocator.ClearAccounts();
            Messenger.Default.Send(Token.Message, Token.CloseAccountsWindow);
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        private void OnSelectionChangedCommandExecute()
        {
            if (this.SelectedInvoices == null || !this.SelectedInvoices.Any() || !this.IsFormLoaded)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.RunningTotal = this.SelectedInvoices.Sum(x => x.SubTotalExVAT).ToDecimal();

            this.SetControlMode(ControlMode.Export);
            this.HasWriteAuthorisation = true;
        }

        #endregion

        #region helper

        /// <summary>
        /// Creates the invoices.
        /// </summary>
        private void ExportInvoices()
        {
            this.Log.LogDebug(this.GetType(), "ExportInvoices(): Exporting..");
            List<int> invoiceNos = null;
            List<int> invoiceNosToDisplay = new List<int>();
            try
            {
                if (this.SelectedInvoices != null)
                {
                    var localInvoices =
                        this.SelectedInvoices.Where(
                            x => x.NouDocStatusID != NouvemGlobal.NouDocStatusExported.NouDocStatusID).ToList();
                    if (localInvoices.Any())
                    {
                        invoiceNos = new List<int>();

                        ProgressBar.SetUp(0, localInvoices.Count * 2);

                        var message = this.AccountsManager.Connect();
                        if (message != string.Empty)
                        {
                            SystemMessage.Write(MessageType.Issue, message);
                            this.Log.LogError(this.GetType(), message);
                            return;
                        }

                        foreach (var invoiceToExport in localInvoices.OrderBy(x => x.Number))
                        {
                            var localInvoice = this.DataManager.GetInvoicesById(invoiceToExport.SaleID);

                            foreach (var item in localInvoice.SaleDetails)
                            {
                                item.SetInventoryItem();
                                item.SetPriceMethodOnly();
                            }

                            invoiceToExport.SaleDetails = localInvoice.SaleDetails;
                            invoiceToExport.BaseDocumentCreationDate = localInvoice.BaseDocumentCreationDate;
                            invoiceToExport.BaseDocumentNumber = localInvoice.BaseDocumentNumber;
                            invoiceToExport.DocketNote = localInvoice.DocketNote;
                            ProgressBar.Run();
                            invoiceToExport.CreationDate = this.postingDate;

                            var postMessage = this.AccountsManager.PostSalesInvoice(invoiceToExport);
                            if (!postMessage.Equals(string.Empty))
                            {
                                SystemMessage.Write(MessageType.Issue, postMessage);
                            }
                            else
                            {
                                invoiceNos.Add(invoiceToExport.SaleID);
                                invoiceNosToDisplay.Add(invoiceToExport.Number);
                            }

                            ProgressBar.Run();
                        }

                        if (invoiceNos.Any() && this.DataManager.ExportInvoices(invoiceNos))
                        {
                            var numbers = invoiceNosToDisplay.Count == 1 ? invoiceNosToDisplay.First().ToString() : string.Join(",", invoiceNosToDisplay.Select(x => x.ToString()));
                            SystemMessage.Write(MessageType.Priority, string.Format(Message.InvoiceExported, numbers));
                            this.Refresh();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                if (this.invoices != null && this.invoices.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.DataManager.ExportInvoices(invoiceNos);
                    }));
                }

                SystemMessage.Write(MessageType.Issue, ex.Message);
                this.Log.LogError(this.GetType(), ex.Message);
            }
            finally
            {
                try
                {
                    ProgressBar.Reset();
                    this.SetControlMode(ControlMode.OK);
                    this.AccountsManager.Logout();
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                }
            }
        }

        /// <summary>
        /// Refresh the ui.
        /// </summary>
        private void Refresh()
        {
            this.Log.LogDebug(this.GetType(), "AccountsViewMOdel(): Refresh()");
            this.allInvoices = this.DataManager.GetNonExportedInvoices().Where(x => x.DocumentDate != null).ToList();

            if (this.ShowAllInvoices)
            {
                this.Invoices = new ObservableCollection<Sale>(this.allInvoices);
                this.Log.LogDebug(this.GetType(), string.Format("ShowAllInvoices:true, Invoice count:{0}", this.Invoices.Count));
            }
            else
            {
                var invoicesReady =
                    this.allInvoices.Where(x => x.NouDocStatusID != NouvemGlobal.NouDocStatusExported.NouDocStatusID
                                                && x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList();
                this.DataManager.SetInvoiceBaseDocuments(invoicesReady);
                this.Invoices = new ObservableCollection<Sale>(invoicesReady);
                this.Log.LogDebug(this.GetType(), string.Format("ShowAllInvoices:false, Invoice count:{0}", this.Invoices.Count));
            }

            //this.SelectedInvoice = this.Invoices.FirstOrDefault();
        }

        #endregion

        #endregion
    }
}

