﻿// -----------------------------------------------------------------------
// <copyright file="ContainerViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Account
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class VatSetUpViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected vat.
        /// </summary>
        private Vat selectedVAT;

        /// <summary>
        /// The vats collection.
        /// </summary>
        private ObservableCollection<Vat> vat; 

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the VatSetUpViewModelclass.
        /// </summary>
        public VatSetUpViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessAccounts);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            #region instantiation

            this.VAT = new ObservableCollection<Vat>();

            #endregion

            this.GetVatCodes();
        }

        #endregion

        #region property


        /// <summary>
        /// Get or sets the selected vat.
        /// </summary>
        public Vat SelectedVAT
        {
            get
            {
                return this.selectedVAT;
            }

            set
            {
                this.selectedVAT = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the vat codes.
        /// </summary>
        public ObservableCollection<Vat> VAT
        {
            get
            {
                return this.vat;
            }

            set
            {
                this.vat = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the vat codes.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.selectedVAT != null)
            {
                this.selectedVAT.Deleted = true;
                this.UpdateVatCodes();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.VAT.Remove(this.selectedVAT);
            }
        }

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateVatCodes();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the vat codes group.
        /// </summary>
        private void UpdateVatCodes()
        {
            try
            {
                if (this.DataManager.AddOrUpdateVatCodes(this.VAT))
                {
                    SystemMessage.Write(MessageType.Priority, Message.VatCodesUpdated);

                    // refresh the corresponding inventory collection.
                    Messenger.Default.Send(Token.Message, Token.RefreshInventory);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.VatCodesNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.VatCodesNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearVatSetUp();
            Messenger.Default.Send(Token.Message, Token.CloseVatSetUpWindow);
        }

        /// <summary>
        /// Gets the application vat codes.
        /// </summary>
        private void GetVatCodes()
        {
            this.VAT.Clear();
            foreach (var code in this.DataManager.GetVAT())
            {
                this.VAT.Add(code);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}

