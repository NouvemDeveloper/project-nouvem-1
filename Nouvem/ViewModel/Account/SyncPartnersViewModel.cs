﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.Enum;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.BusinessPartner;
using Quartz.Util;

namespace Nouvem.ViewModel.Account
{
    public class SyncPartnersViewModel : BPSearchDataViewModel
    {
       #region field

        /// <summary>
        /// The incoming suppliers.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.BusinessPartner> filteredSuppliers = new ObservableCollection<Model.BusinessObject.BusinessPartner>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncPartnersViewModel"/> class.
        /// </summary>
        public SyncPartnersViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.OnSelectionChangedCommand = new RelayCommand(this.OnSelectionChangedCommandExecute);

            #endregion

            this.SelectedPartners = new List<Model.BusinessObject.BusinessPartner>();
            this.SelectedSuppliers = new List<Model.BusinessObject.BusinessPartner>();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the incoming suppliers.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.BusinessPartner> FilteredSuppliers
        {
            get
            {
                return this.filteredSuppliers;
            }

            set
            {
                this.filteredSuppliers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected partners.
        /// </summary>
        public IList<Model.BusinessObject.BusinessPartner> SelectedPartners { get; set; }

        /// <summary>
        /// Gets or sets the selected partners.
        /// </summary>
        public IList<Model.BusinessObject.BusinessPartner> SelectedSuppliers { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle a grid selection change.
        /// </summary>
        public ICommand OnSelectionChangedCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.IsBusy = true;

            if (!ApplicationSettings.SyncPartnersLoadedForFirstTime)
            {
                ApplicationSettings.SyncPartnersLoadedForFirstTime = true;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    System.Threading.Thread.Sleep(150);
                    Task.Factory.StartNew(() =>
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            this.GetAccountsPartners();
                            this.SetControlMode(ControlMode.OK);
                        });
                    }).ContinueWith(x =>
                    {
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            this.IsBusy = false;
                        }));
                    });
                }));

                return;
            }

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.GetAccountsPartners();
                    this.SetControlMode(ControlMode.OK);
                });
            }).ContinueWith(x =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.IsBusy = false;
                }));
            });
        }

        /// <summary>
        /// Override to close.
        /// </summary>
        protected override void OnClosingCommandExecute()
        {
            this.FilteredSuppliers?.Clear();
            this.FilteredSuppliers = null;
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Import:
                    if (this.AccountsManager.CanConnectToAccounts)
                    {
                        this.ImportPartners();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.NoAccountsAccess);
                    }

                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Handles the ui cancellation.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Handles partner search.
        /// </summary>
        protected override void HandlePartnersSearch()
        {
        }

        #endregion

        #region private

        /// <summary>
        /// Retrieves the accounts partners.
        /// </summary>
        private void GetAccountsPartners()
        {
            try
            {
                this.AccountsManager.Connect();
                this.FilteredBusinessPartners = new ObservableCollection<Model.BusinessObject.BusinessPartner>(
                    this.AccountsManager.GetAccountsBusinessPartners().Where(x => x.PartnerType != null).OrderBy(x => x.Type));
            }
            catch (Exception ex)
            {
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
            finally
            {
                this.AccountsManager.Logout();
            }
        }

        /// <summary>
        /// Handle the grid selection changed event.
        /// </summary>
        private void OnSelectionChangedCommandExecute()
        {
            if (this.SelectedPartners == null || !this.SelectedPartners.Any() || !this.IsFormLoaded)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.SetControlMode(ControlMode.Import);
        }

        /// <summary>
        /// Imports the selected partners.
        /// </summary>
        private void ImportPartners()
        {
            var error = string.Empty;
            var codes = new List<string>();

            try
            {
                ProgressBar.SetUp(0, this.SelectedPartners.Count);
                ProgressBar.Run();
                foreach (var selectedPartner in this.SelectedPartners)
                {
                    try
                    {
                        this.Locator.BPMaster.HandleAccountsPartnerImport(selectedPartner);
                        codes.Add(selectedPartner.Details.Code);
                        ProgressBar.Run();
                    }
                    catch (Exception ex)
                    {
                        error = Message.OneOrMorePartnersNotImported;
                        this.Log.LogError(this.GetType(), ex.Message);
                        SystemMessage.Write(MessageType.Issue, error);
                    }
                }

                foreach (var code in codes)
                {
                    var importedPartner =
                            this.FilteredBusinessPartners.FirstOrDefault(x => x.Details.Code == code);
                    if (importedPartner != null)
                    {
                        this.FilteredBusinessPartners.Remove(importedPartner);
                    }
                }

                ViewModelLocator.ClearBPMaster();
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemGlobal.RefreshPartners();

                    // Inform all observers of a new partner addition.
                    Messenger.Default.Send(Token.Message, Token.RefreshPartners);
                }));
            }
            finally
            {
                ProgressBar.Reset();
                if (error == string.Empty)
                {
                    SystemMessage.Write(MessageType.Priority, Message.PartnersImported);
                }
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearSyncPartners();
            Messenger.Default.Send(Token.Message, Token.CloseSyncPartners);
        }

        #endregion
    }
}
