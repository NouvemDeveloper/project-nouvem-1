﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using DevExpress.Mvvm.POCO;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.View.Production.IntoProduction;
using Nouvem.ViewModel.Purchases.APReceipt;

namespace Nouvem.ViewModel.Sales.ARReturn
{
    public class ARReturnViewModel : APReceiptTouchscreenViewModel
    {
        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ARReturnViewModel"/> class.
        /// </summary>
        public ARReturnViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            this.PartnerName = Strings.SelectCustomer;
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #region method

        /// <summary>
        /// Calls the search screen with the non invoiced dispatch dockets displayed.
        /// </summary>
        public void DisplayReturns()
        {
            var intakes = this.DataManager.GetNonInvoicedIntakes();
            this.SetActiveDocument(this);

            Application.Current.Dispatcher.Invoke(() =>
            {
                ViewModelLocator.CreditNoteCreationStatic.SetInvoices(intakes);
                Messenger.Default.Send(new List<Sale>(), Token.CreateCreditNote);
            });
        }

        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            if (this.Locator.SalesSearchData.AssociatedView != ViewType.ARReturns)
            {
                return;
            }

            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                var localSales = this.DataManager.GetARReturns(orderStatuses);
                this.CustomerSales = localSales
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetARReturns(orderStatuses)
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            this.Locator.SalesSearchData.SetSales(this.CustomerSales);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.ARReturns);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetARReturnByLastEdit();
        }

        #endregion

        #endregion

        #region protected


        /// <summary>
        /// Drill down to the selected item details.
        /// </summary>
        protected override void DrillDownToDetailsCommandExecute()
        {
            var localSaleDetail = this.SelectedSaleDetail;
            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            Messenger.Default.Send(ViewType.APReceiptDetails);
            localSaleDetail.DocketNo = this.Sale.Number;
            this.Locator.APReceiptDetails.IsReturn = true;
            this.Locator.APReceiptDetails.SelectedSaleDetail = localSaleDetail;
            this.Locator.APReceiptDetails.IsReadOnly = this.IsReadOnly;
        }

        /// <summary>
        /// Handles the update request.
        /// </summary>
        protected override void UpdateOrder()
        {
            this.CreateSale();
            var docketComplete = this.Sale.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID;

            this.Sale.NouDocStatusID = this.SelectedDocStatus.NouDocStatusID;

            if (docketComplete)
            {
                this.DataManager.UpdateARReturnStatus(this.Sale);
                SystemMessage.Write(MessageType.Priority, string.Format(Message.ARReturnUpdated, this.Sale.Number));
                this.ClearForm();
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                var localSales = this.DataManager.GetARReturns(orderStatuses);
                this.CustomerSales = localSales
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetARReturns(orderStatuses)
                    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.APReceipt), Token.SearchForSale);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.ARReturns);
            }
        }

        /// <summary>
        /// Override the selected purchase order, resetting it's totals.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            var localCopyValue = CopyingDocument;
            var localSaleId = this.Sale.SaleID;
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            if (CopyingDocument && !this.IsActiveDocument())
            {
                SystemMessage.Write(MessageType.Issue, Message.ClosedDocumentCopyRestriction);
                this.ClearForm(false);
                this.SetControlMode(ControlMode.Add);
                return;
            }

            #endregion

            try
            {
                this.EntitySelectionChange = true;

                this.IsReadOnly = false;
                this.DisableDocStatusChange = false;
                this.IsBaseDocument = false;

                // header
                var partnerId = this.Sale.BPCustomer != null
                    ? this.Sale.BPCustomer.BPMasterID
                    : this.Sale.Customer.BPMasterID;

                this.SelectedPartner = this.allPartners.FirstOrDefault(x => x.BPMasterID == partnerId);

                if (CopyingDocument || this.Sale.CopyingFromOrder)
                {
                    // Copying document, so we want to use the next document number (Not the document number of the copied sale)
                    this.ResetDocumentNumber();
                }
                else
                {
                    // Not copying, so use the selected sales document number.
                    this.SelectedDocNumbering =
                        this.DocNumberings.FirstOrDefault(x => x.DocumentNumberingID == this.Sale.DocumentNumberingID);
                    this.NextNumber = CopyingDocument && this.SelectedDocNumbering != null
                        ? this.SelectedDocNumbering.NextNumber
                        : this.Sale.Number;
                }

                this.SelectedDocStatus =
                    this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == this.Sale.NouDocStatusID);
                this.ValidUntilDate = this.Sale.QuoteValidDate;
                this.DeliveryDate = this.Sale.DeliveryDate;
                this.ProposedKillDate = this.Sale.ProposedKillDate;
                this.ShippingDate = this.Sale.ShippingDate;
                this.DocumentDate = this.Sale.DocumentDate;
                this.DiscountPercentage = this.Sale.DiscountPercentage.ToDecimal();
                this.Remarks = this.Sale.Remarks;
                this.CustomerPOReference = this.Sale.CustomerPOReference;
                this.SelectedSalesEmployee = this.Sale.SalesEmployeeID != null
                    ? this.SalesEmployees.FirstOrDefault(x => x.UserMaster.UserMasterID == this.Sale.SalesEmployeeID)
                    : null;
                this.SubTotal = this.Sale.SubTotalExVAT.ToDecimal();
                this.Tax = this.Sale.VAT.ToDecimal();
                this.Discount = this.Sale.DiscountIncVAT.ToDecimal();
                this.Total = this.Sale.GrandTotalIncVAT.ToDecimal();
                this.DocketNote = this.Sale.DocketNote;
                this.PopUpNote = this.Sale.PopUpNote;
                this.DeliveryTime = this.Sale.DeliveryTime;
                this.InvoiceNote = this.Sale.InvoiceNote;
                this.SelectedDeliveryAddress = this.Sale.DeliveryAddress != null &&
                                               this.Sale.DeliveryAddress.Details != null
                    ? this.Addresses.FirstOrDefault(
                        x => x.Details.BPAddressID == this.Sale.DeliveryAddress.Details.BPAddressID)
                    : null;
                this.SelectedInvoiceAddress = this.Sale.InvoiceAddress != null &&
                                              this.Sale.InvoiceAddress.Details != null
                    ? this.Addresses.FirstOrDefault(
                        x => x.Details.BPAddressID == this.Sale.InvoiceAddress.Details.BPAddressID)
                    : null;

                this.SelectedHaulier = null;
                this.SelectedContact = null;
                this.SelectedDeliveryContact = null;
          
                this.SelectedRoute = null;

                if (this.Sale.BPHaulier != null)
                {
                    this.SelectedHaulier =
                        this.Hauliers.FirstOrDefault(x => x.BPMasterID == this.Sale.BPHaulier.BPMasterID);
                }
                else if (this.Sale.Haulier != null)
                {
                    this.SelectedHaulier =
                        this.Hauliers.FirstOrDefault(x => x.BPMasterID == this.Sale.Haulier.BPMasterID);
                }

                if (this.Sale.MainContact != null && this.Sale.MainContact.Details != null)
                {
                    this.SelectedContact =
                        this.Contacts.FirstOrDefault(
                            x => x.Details.BPContactID == this.Sale.MainContact.Details.BPContactID);
                }

                if (this.Sale.DeliveryContact != null && this.Sale.DeliveryContact.Details != null)
                {
                    this.SelectedDeliveryContact =
                        this.Contacts.FirstOrDefault(
                            x => x.Details.BPContactID == this.Sale.DeliveryContact.Details.BPContactID);
                }

                if (!this.Sale.RouteID.IsNullOrZero() && this.Routes != null)
                {
                    this.SelectedRoute = this.Routes.FirstOrDefault(x => x.RouteID == this.Sale.RouteID);
                }

                this.PORequired = this.Sale.PORequired.ToBool();

                // Add the sale details.
                if (this.Sale.SaleDetails != null)
                {
                    this.SaleDetails = new ObservableCollection<SaleDetail>(this.Sale.SaleDetails);
                    this.CalculateMargin();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }
            finally
            {
                this.EntitySelectionChange = false;
                this.SetControlMode(ControlMode.OK);
                this.SearchTermCode = string.Empty;
                this.SearchTermName = string.Empty;

                // if we are copying a document then we need to put in into 'Add' mode.
                if (CopyingDocument)
                {
                    this.SetControlMode(ControlMode.Add);
                    this.Sale.BaseDocumentReferenceID = this.Sale.SaleID;
                    this.Sale.SaleID = 0;
                    CopyingDocument = false;
                }
            }

            this.Sale.SaleID = localSaleId;

            if (this.Sale.SaleDetails != null)
            {
                this.DataManager.GetTransactionData(this.Sale.SaleDetails);
            }

            //this.SetControlMode(ControlMode.OK);

            if (!localCopyValue && !this.Sale.CopyingFromOrder)
            {
                // We only reset the totals when we are copying from an order.
                return;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.TotalExVAT = 0;
            }

            this.Tax = 0;
            this.Total = 0;
            this.SubTotal = 0;

            // we need to keep the discount % (Setting discount to 0 will reset it to 0)
            this.CalculateSalesTotals = false;
            this.Discount = 0;
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            base.OnLoadingCommandExecute();
            this.SetControlMode(ControlMode.Add);
        }

        protected override void SetTouchscreen()
        {
            
        }

        /// <summary>
        /// Gets the local module warehouse.
        /// </summary>
        /// <returns></returns>
        protected override int GetWarehouseId()
        {
            return this.selectedWarehouse != null && this.selectedWarehouse.WarehouseID != 0
                ? this.selectedWarehouse.WarehouseID
                : NouvemGlobal.WarehouseReturnsId;
        }

        /// <summary>
        /// Gets the local module.
        /// </summary>
        /// <returns></returns>
        protected override int GetTransactionId()
        {
            return NouvemGlobal.TransactionTypeGoodsReturnId;
        }

        /// <summary>
        /// handles the barcode scan.
        /// </summary>
        /// <param name="data">The scanned barcode.</param>
        protected override void HandleScannerData(string data)
        {
            this.Log.LogInfo(this.GetType(), string.Format("HandleScannerData(): data:{0}, length:{1}", data, data.Length));
          
            if (ViewModelLocator.IsTransactionManagerLoaded())
            {
                Messenger.Default.Send(data.Trim(), Token.LabelSearch);
                return;
            }

            //if (this.SelectedPartner == null)
            //{
            //    this.Locator.TouchscreenSuppliers.SetPartners(ViewType.ARReturns);
            //    this.Locator.TouchscreenSuppliers.SelectedPartner = this.Locator.TouchscreenSuppliers.F
            //    return;
            //}

            var stockDetail = this.DataManager.GetStockTransactionBySerial(data.Trim().ToInt(), NouvemGlobal.TransactionTypeGoodsDeliveryId);
            if (!stockDetail.StockTransactionID.HasValue)
            {
                SystemMessage.Write(MessageType.Issue, Message.StockNotFound);
                NouvemMessageBox.Show(Message.StockNotFound, touchScreen: true);
                return;
            }

            this.SetAttributeValues(stockDetail);
            this.indicatorWeight = stockDetail.Transaction.TransactionWeight.ToDecimal();
            this.ProductQuantity = stockDetail.Transaction.TransactionQTY.ToDecimal();
            this.SelectedSaleDetail = new SaleDetail();
            this.SelectedSaleDetail.INMasterID = stockDetail.Transaction.INMasterID;
            this.SelectedSaleDetail.SetInventoryItem();
            var localDetail = this.SetSaleDetail(this.SelectedSaleDetail);
            if (localDetail != null)
            {
                this.DoNotHandleSelectedSaleDetail = true;
                this.SelectedSaleDetail = localDetail;
                this.DoNotHandleSelectedSaleDetail = false;
            }

            stockDetail.TransactionWeight = this.indicatorWeight;
            stockDetail.Transaction.NouTransactionTypeID = this.GetTransactionId();
            stockDetail.Transaction.Consumed = null;
            stockDetail.Transaction.Deleted = null;
            stockDetail.Transaction.WarehouseID = this.GetWarehouseId();
            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;
            this.SelectedSaleDetail.StockDetails.Add(stockDetail);
            this.SelectedSaleDetail.StockDetailToProcess = stockDetail;
            this.SelectedSaleDetail.Batch = this.BatchNumber;


            var localSaleDetail = this.SelectedSaleDetail;
            this.CompleteTransaction(this.indicatorWeight);

            Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);
            Messenger.Default.Send(Token.Message, Token.ResetSerialData);
            this.StockLabelsToPrint = 1;
            if (ApplicationSettings.TouchScreenMode)
            {
                this.CheckAttributeTransactionReset(localSaleDetail);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
                {
                    this.SelectedSaleDetail.TransactionCount =
                        this.DataManager.GetTransactionCount(this.SelectedSaleDetail.SaleDetailID,
                            NouvemGlobal.TransactionTypeGoodsReceiptId);
                }));
            }
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase("ARReturn")));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Gets the current orders live status.
        /// </summary>
        /// <returns>The current orders live status.</returns>
        protected override void GetLiveStatus()
        {
            if (this.Sale == null)
            {
                return;
            }

            var orderDetails = this.DataManager.GetReturnsStatusDetails(this.Sale.SaleID);
            if (orderDetails == null)
            {
                return;
            }

            foreach (var detail in this.SaleDetails)
            {
                detail.QuantityDelivered = 0;
                detail.WeightDelivered = 0;
            }

            var order = orderDetails.FirstOrDefault();
            if (order == null)
            {
                return;
            }

            foreach (var detail in orderDetails)
            {
                var localSaleDetail = this.SaleDetails.FirstOrDefault(x => x.SaleDetailID == detail.ARReturnDetailID);
                if (localSaleDetail != null)
                {
                    localSaleDetail.LoadingSale = true;
                    localSaleDetail.QuantityOrdered = detail.QuantityOrdered.ToDecimal();
                    localSaleDetail.WeightOrdered = detail.WeightOrdered.ToDecimal();
                    localSaleDetail.UnitPrice = detail.UnitPrice;
                    localSaleDetail.UnitPriceAfterDiscount = detail.UnitPrice;
                    localSaleDetail.QuantityReceived = detail.Qty.ToDecimal();
                    localSaleDetail.WeightReceived = detail.Wgt.ToDecimal();
                    if (detail.PriceListID.HasValue)
                    {
                        localSaleDetail.PriceListID = detail.PriceListID.ToInt();
                    }
                }
                else
                {
                    var newLine = new SaleDetail
                    {
                        LoadingSale = true,
                        SaleDetailID = detail.ARReturnDetailID,
                        SaleID = detail.ARReturnID,
                        INMasterID = detail.INMasterID,
                        QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                        WeightOrdered = detail.WeightOrdered.ToDecimal(),
                        QuantityReceived = detail.Qty.ToDecimal(),
                        WeightReceived = detail.Wgt.ToDecimal(),
                        UnitPrice = detail.UnitPrice,
                        UnitPriceAfterDiscount = detail.UnitPrice,
                        InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID)
                    };

                    if (detail.PriceListID.HasValue)
                    {
                        newLine.PriceListID = detail.PriceListID.ToInt();
                    }

                    this.SaleDetails.Add(newLine);
                }
            }
        }

        /// <summary>
        /// Gets the local module.
        /// </summary>
        /// <returns></returns>
        protected override string GetModule()
        {
            return Constant.ARReturn;
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateIntakes)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.Log.LogDebug(this.GetType(), "AddSale(): adding a new order");
            this.UpdateDocNumbering();

            var details = this.Sale.SaleDetails;
            if (details != null && details.Any())
            {
                if (!details.First().BatchNumberId.HasValue && ApplicationSettings.IntakeMode != IntakeMode.IntakeMultiBatch)
                {
                    var batch = this.BatchManager.GenerateBatchNumber(false, Strings.Returns);
                    foreach (var saleDetail in details)
                    {
                        saleDetail.Batch = batch;
                    }
                }
            }

            var receipt = this.DataManager.AddARReturn(this.Sale);
            var newSaleId = receipt.Item1;
            var transactionId = receipt.Item2;
            var localSaleDetail = this.SelectedSaleDetail;

            if (newSaleId > 0)
            {
                if (ApplicationSettings.TouchScreenMode)
                {
                    var message = string.Format(Message.ReturnsWeightRecorded, this.SelectedSaleDetail.StockDetailToProcess.TransactionWeight, this.SelectedSaleDetail.InventoryItem.Name);
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                    }));
                }

                if (this.NonStandardResponses.Any() || this.NonStandardBatchResponses.Any())
                {
                    foreach (var response in this.NonStandardResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: newSaleId);
                    }

                    foreach (var response in this.NonStandardBatchResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: newSaleId);
                    }
                }

                this.NonStandardResponses.Clear();
                this.Sale.NonStandardResponses = null;
                SystemMessage.Write(MessageType.Priority, string.Format(Message.ARReturnCreated, this.NextNumber));

                try
                {
                    if (!this.ValidateData(
                        this.traceabilityDataPostTransactions,
                        transactionId, 0, 0, 0, 0, 0, Constant.Intake))
                    {
                        return;
                    };
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                if (this.PrintProductLabel)
                {
                    this.Print(transactionId);
                }

                this.Sale.SaleID = newSaleId;
                this.AllSales.Add(this.Sale);
                this.PalletNumber = string.Empty;
                this.SetOrderLinesStatus();
                if (!ApplicationSettings.TouchScreenMode)
                {
                    this.ClearForm();
                    this.SetControlMode(ControlMode.Find);
                }
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.ReturnsNotCreated);
            }
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (searchSale != null)
            {
                this.DataManager.PriceReturnsDocket(searchSale.SaleID);
                this.Sale = this.DataManager.GetARReturnById(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearARReturn();
            Messenger.Default.Send(Token.Message, Token.CloseReturnWindow);
        }
       

        protected override void SetDefaultProcesses()
        {
    
        }

        /// <summary>
        /// Sets the ui panel.
        /// </summary>
        protected override void SetPanel()
        {
     
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateIntakes)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            string error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            //else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            //{
            //    error = Message.NoDocStatusSelected;
            //}
            else if (!this.CancellingCurrentSale && this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            var localSaleDetail = this.SelectedSaleDetail;
            var result = this.DataManager.UpdateARReturn(this.Sale);
            if (result)
            {
                if (this.NonStandardResponses.Any() || this.NonStandardBatchResponses.Any())
                {
                    foreach (var response in this.NonStandardResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: this.Sale.SaleID);
                    }

                    foreach (var response in this.NonStandardBatchResponses)
                    {
                        this.LogAlerts(response, apGoodsReceiptId: this.Sale.SaleID);
                    }
                }

                this.NonStandardResponses.Clear();
                this.Sale.NonStandardResponses = null;
                this.PalletNumber = string.Empty;
                SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                if (ApplicationSettings.TouchScreenMode)
                {
                    var message = string.Format(Message.ReturnsWeightRecorded, this.SelectedSaleDetail.StockDetailToProcess.TransactionWeight, this.SelectedSaleDetail.InventoryItem.Name);
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(message, touchScreen: true, flashMessage: true);
                    }));
                }

                try
                {
                    if (!this.ValidateData(
                        this.traceabilityDataPostTransactions,
                        this.Sale.ReturnedStockTransactionId, 0, 0, 0, 0, 0, Constant.Intake))
                    {
                        return;
                    };
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                if (this.PrintProductLabel)
                {
                    this.Print(this.Sale.ReturnedStockTransactionId);
                }

                this.SetOrderLinesStatus();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.ReturnsNotCreated, this.NextNumber));
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Completes the current order.
        /// </summary>
        protected override void CompleteOrder()
        {
            #region validation

            var error = string.Empty;

            var unCompleted = this.CheckRequiredBeforeCompletionAttributeData();
            if (unCompleted != string.Empty)
            {
                var message = string.Format(Message.RequiredBeforeCompletionAttributeValueMissing, unCompleted);
                SystemMessage.Write(MessageType.Issue, message);
                NouvemMessageBox.Show(message, touchScreen: true);
                return;
            }

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoPartnerSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.EmptyOrder;
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error, touchScreen: true);
                return;
            }

            try
            {
                if (!this.ValidateData(
                    this.traceabilityDataCompletion,
                    this.Sale.SaleID,
                    0, 0, 0, 0, this.Locator.ProcessSelection.SelectedProcess.ProcessID,
                    Constant.ARReturn))
                {
                    return;
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            #endregion

            NouvemMessageBox.Show(Message.VerifyReturnCompletion, NouvemMessageBoxButtons.YesNo, touchScreen: true, scanner: ApplicationSettings.ScannerMode);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            this.Sale.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
            if (this.DataManager.UpdateARReturnStatus(this.Sale))
            {
                SystemMessage.Write(MessageType.Priority, string.Format(Message.ReturnCompleted, this.Sale.Number));
                this.ClearForm();
                this.PartnerName = Strings.SelectCustomer;
                this.Reference = string.Empty;
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                this.RefreshDocStatusItems();
                Messenger.Default.Send(Token.Message, Token.ClearData);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.ReturnNotCompleted);
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Adds the incoming dispatch product.
        /// </summary>
        /// <param name="detail">The incoming sale detail.</param>
        private SaleDetail SetSaleDetail(SaleDetail detail)
        {
            if (this.SaleDetails == null)
            {
                this.SaleDetails = new ObservableCollection<SaleDetail>();
            }

            if (this.SaleDetails.Any())
            {
                var localDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                if (localDetail != null)
                {
                    return localDetail;
                }
            }

            this.SaleDetails.Add(detail);
            this.DoNotHandleSelectedSaleDetail = true;
            this.SelectedSaleDetail = detail;
            this.DoNotHandleSelectedSaleDetail = false;
            return null;
        }

        #endregion

    }
}
