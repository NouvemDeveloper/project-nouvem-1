﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Sales.ARReturn
{
    public class ARReturnTouchscreenViewModel : ARReturnViewModel
    {

        #region field



        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ARReturnTouchscreenViewModel"/> class.
        /// </summary>
        public ARReturnTouchscreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            #endregion

            ApplicationSettings.IntakeMode = IntakeMode.Returns;
            this.PartnerName = Strings.SelectCustomer;
        }

        #endregion

        #region public interface

        #region property



        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Override the sale selection.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            #region parse sale

            this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(x => x.NouDocStatusID == this.Sale.NouDocStatusID);
            this.ValidUntilDate = this.Sale.QuoteValidDate;
            this.DeliveryDate = this.Sale.DeliveryDate;
            this.DocumentDate = this.Sale.DocumentDate;
            this.DiscountPercentage = this.Sale.DiscountPercentage.ToDecimal();
            this.Remarks = this.Sale.Remarks;
            this.SelectedSalesEmployee = this.Sale.SalesEmployeeID != null ?
                this.SalesEmployees.FirstOrDefault(x => x.UserMaster.UserMasterID == this.Sale.SalesEmployeeID) : null;
            this.SubTotal = this.Sale.SubTotalExVAT.ToDecimal();
            this.Tax = this.Sale.VAT.ToDecimal();
            this.Discount = this.Sale.DiscountIncVAT.ToDecimal();
            this.Total = this.Sale.GrandTotalIncVAT.ToDecimal();
            this.DocketNote = this.Sale.DocketNote;
            this.InvoiceNote = this.Sale.InvoiceNote;
            this.SelectedDeliveryAddress = this.Sale.DeliveryAddress != null && this.Sale.DeliveryAddress.Details != null ?
                this.Addresses.FirstOrDefault(
                x => x.Details.BPAddressID == this.Sale.DeliveryAddress.Details.BPAddressID) : null;
            this.SelectedInvoiceAddress = this.Sale.InvoiceAddress != null && this.Sale.InvoiceAddress.Details != null ?
                this.Addresses.FirstOrDefault(
                x => x.Details.BPAddressID == this.Sale.InvoiceAddress.Details.BPAddressID) : null;
            this.Reference = this.Sale.OtherReference;

            #endregion

            #region parse sale details

            // Add the Sale details.
            this.DoNotHandleSelectedSaleDetail = true;
            this.SaleDetails = new ObservableCollection<SaleDetail>(this.Sale.SaleDetails);
            this.DoNotHandleSelectedSaleDetail = false;
            if (this.SaleDetails.Any())
            {
                this.SelectedSaleDetail = this.SaleDetails.Last();
                this.BatchNumber = this.SelectedSaleDetail.Batch;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                if (saleDetail.WeightOrdered == null)
                {
                    saleDetail.WeightOrdered = 0;
                }

                if (saleDetail.QuantityOrdered == null)
                {
                    saleDetail.QuantityOrdered = 0;
                }

                if (saleDetail.WeightReceived == null)
                {
                    saleDetail.WeightReceived = 0;
                }

                if (saleDetail.QuantityReceived == null)
                {
                    saleDetail.QuantityReceived = 0;
                }
            }

            #endregion

            if (this.BatchNumber == null)
            {
                this.AutoGenerateNewBatchCommand();
            }

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.SetOrderLinesStatus();
            }));

            if (!string.IsNullOrWhiteSpace(this.Sale.PopUpNote))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(this.Sale.PopUpNote, isPopUp: true);
                }));
            }

            if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp: true);
                    this.CustomerPopUpNote = string.Empty;
                }));
            }
        }

        protected override void SetTouchscreen()
        {
            this.Locator.Touchscreen.Quantity = 1;
            this.Locator.Touchscreen.ShowAttributes = ApplicationSettings.ShowIntakeAttributes;
            this.Locator.Touchscreen.UIText = Strings.TerminalLocation;
            ViewModelLocator.ClearTouchscreenSuppliers();
            ViewModelLocator.ClearStockMovementTouchscreen();
            this.Locator.Touchscreen.OutOfProductionExtensionWidth = 0;
            this.Locator.Indicator.WeighMode = WeighMode.Manual;
            this.Locator.Touchscreen.UIText = string.Empty;
        }

        protected override void SetDefaultProcesses()
        {
            this.Locator.ProcessSelection.SetDefaultProcesses("Returns", "Returns");
        }

        /// <summary>
        /// Sets the ui panel.
        /// </summary>
        protected override void SetPanel()
        {
            this.Locator.Touchscreen.PanelViewModel = this.Locator.ARReturnPanel;
        }

        #endregion

        #region private



        #endregion

    }
}
