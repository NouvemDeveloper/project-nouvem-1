﻿// -----------------------------------------------------------------------
// <copyright file="SalesSearchDataViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Entity.Migrations.History;
using Microsoft.Reporting.WinForms;

namespace Nouvem.ViewModel.Sales
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;
  
    /// <summary>
    /// Class that handles the search for the sale orders.
    /// </summary>
    public class SalesSearchDataViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected grid sale.
        /// </summary>
        protected Sale selectedSale;

        /// <summary>
        /// The selected sales.
        /// </summary>
        private ObservableCollection<Sale> selectedSales  = new ObservableCollection<Sale>();

        /// <summary>
        /// The filtered search sales.
        /// </summary>
        private IList<Sale> searchSales;

        /// <summary>
        /// The selected grid sale.
        /// </summary>
        private Sale currentSale;

        /// <summary>
        /// The filtered data collection.
        /// </summary>
        private ObservableCollection<Sale> filteredSales;

        /// <summary>
        /// The search from date.
        /// </summary>
        private DateTime fromDate;

        /// <summary>
        /// The searcht to date.
        /// </summary>
        private DateTime toDate;

        /// <summary>
        /// The keep the search form visible flag.
        /// </summary>
        protected bool keepVisible;

        /// <summary>
        /// The show all orders flag..
        /// </summary>
        protected bool showAllOrders;

        /// <summary>
        /// The show all orders flag..
        /// </summary>
        protected bool allowNullSale;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesSearchDataViewModel"/> class.
        /// </summary>
        public SalesSearchDataViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.EnableSalesSearchScreen, s =>
            {
                this.IsFormLoaded = false;
            });

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            Messenger.Default.Register<string>(this,Token.RefreshSearchSales, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.Refresh();
                }
            });

            // Register for the incoming customers sales.
            Messenger.Default.Register<IList<Sale>>(this, Token.DisplayCustomerSales, this.SetIncomingSales);

            Messenger.Default.Register<int>(this, Token.SaleSelected, o =>
            {
                this.SelectedSale = this.FilteredSales.FirstOrDefault(x => x.SaleID == o);
            });
            
            #endregion

            #region command registration

            // Handle the ui partner selection.
            this.SaleSelectedCommand = new RelayCommand(this.SaleSelectedCommandExecute);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Handle the form closing.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion

            this.FilteredSales = new ObservableCollection<Sale>();
            NouvemGlobal.SalesSearchScreenOpen = true;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected sales.
        /// </summary>
        public ObservableCollection<Sale> SelectedSales
        {
            get
            {
                return this.selectedSales;
            }

            set
            {
                this.selectedSales = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.HandleFilteredSales();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime FromDate
        {
            get
            {
                return this.fromDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.fromDate = DateTime.Today;
                }
                else
                {
                    this.fromDate = value;
                }

                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    //Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the search from date.
        /// </summary>
        public DateTime ToDate
        {
            get
            {
                return this.toDate;
            }

            set
            {
                if (value < DateTime.Today.AddYears(-100))
                {
                    this.toDate = DateTime.Today;
                }
                else
                {
                    this.toDate = value;
                }

                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    ApplicationSettings.SalesSearchToDate = value;
                    //Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected search sale.
        /// </summary>
        public Sale SelectedSale
        {
            get
            {
                return this.selectedSale;
            }

            set
            {
                this.HandleSelectedSale(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the ui is to be kept visible.
        /// </summary>
        public bool KeepVisible
        {
            get
            {
                return this.keepVisible;
            }

            set
            {
                this.keepVisible = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether all the orders are to be dispalyed.
        /// </summary>
        public bool ShowAllOrders
        {
            get
            {
                return this.showAllOrders;
            }

            set
            {
                this.showAllOrders = value;
                this.RaisePropertyChanged();
                if (this.IsFormLoaded)
                {
                    //Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    this.Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the filtered sales.
        /// </summary>
        public ObservableCollection<Sale> FilteredSales
        {
            get
            {
                return this.filteredSales;
            }

            set
            {
                this.filteredSales = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search is emanating from the master vm i.e. report display search.
        /// </summary>
        public bool ReportMode { get; set; }

        public bool INMasterMode { get; set; }

        /// <summary>
        /// Gets or sets the associated view.
        /// </summary>
        public ViewType AssociatedView { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the sale selection.
        /// </summary>
        public ICommand SaleSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the form close.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Set the filtered sales.
        /// </summary>
        /// <param name="sales">The incoming sales.</param>
        public void SetSales(IList<Sale> sales)
        {
            this.FilteredSales = new ObservableCollection<Sale>(sales);
        }

        /// <summary>
        /// Set the filtered sales.
        /// </summary>
        public void SetSearchType(string dateSearchType)
        {
            if (dateSearchType == "")
            {
                dateSearchType = Strings.DocumentDate;
            }

            this.IsFormLoaded = false;
            this.SelectedSearchType = dateSearchType;
            this.IsFormLoaded = true;
        }

        /// <summary>
        /// Set the incoming view type.
        /// </summary>
        /// <param name="type">The incoming view.</param>
        public void SetView(ViewType type)
        {
            this.ReportMode = false;
            this.AssociatedView = type;

            if (this.currentSale != null)
            {
                this.SelectedSale = this.filteredSales.FirstOrDefault(x => x.SaleID == this.currentSale.SaleID);
            }
        }

        /// <summary>
        /// Method that sets the sales to display.
        /// </summary>
        /// <param name="data">The sales to display.</param>
        public void SetFilteredSales(IList<Sale> data)
        {
            this.FilteredSales = new ObservableCollection<Sale>(data);
        }

        /// <summary>
        /// Method that sets the sales to display.
        /// </summary>
        /// <param name="sales">The sales to display.</param>
        public void SetDisplaySales(IList<Sale> sales)
        {
            this.SetIncomingSales(sales);
        }

        #endregion

        #endregion

        #region protected

        protected void SetSale(Sale sale)
        {
            this.selectedSale = sale;
        }

        #region virtual

        protected virtual void HandleSelectedSale(Sale sale)
        {
            if (sale != null && this.selectedSales.Count < 2)
            {
                this.selectedSale = sale;
                this.RaisePropertyChanged("SelectedSale");
            }
        }

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        protected virtual void ShowNotesWindow()
        {
            if (this.SelectedSale == null || this.SelectedSale.SaleID == 0)
            {
                return;
            }

            var notes = this.SelectedSale.TechnicalNotes ?? string.Empty;
            Messenger.Default.Send(notes, Token.DisplayNotes);
        }

        /// <summary>
        /// Handles the filter sales update.
        /// </summary>
        protected virtual void HandleFilteredSales()
        {
        }

        /// <summary>
        /// Method which sends a message to the view to close.
        /// </summary>
        protected virtual void Close()
        {
            this.ReportMode = false;
            if (!this.keepVisible)
            {
                NouvemGlobal.SalesSearchScreenOpen = false;
                Messenger.Default.Send(Token.Message, Token.CloseSearchSaleWindow);
                this.FilteredSales.Clear();
            }
        }

        /// <summary>
        /// Set the incoming sales.
        /// </summary>
        /// <param name="sales">The incoming sales.</param>
        protected virtual void SetIncomingSales(IList<Sale> sales)
        {
            this.FilteredSales = new ObservableCollection<Sale>(sales.OrderByDescending(x => x.SaleID));
        }

        #endregion

        #region override

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected override void HandleSelectedSearchType()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.SaveSearchDateType();
            this.Refresh();
        }

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected override void GetSearchTypes()
        {
            this.SearchTypes = new List<string> { Strings.DocumentDate, Strings.DeliveryDate, Strings.ShippingDate};
            this.SelectedSearchType = this.SearchTypes.First();
        }

        /// <summary>
        /// Handles the selection of a sale.
        /// </summary>
        protected virtual void SaleSelectedCommandExecute()
        {
            if (this.selectedSale != null)
            {
                if (this.ReportMode)
                {
                    Messenger.Default.Send(this.selectedSale, Token.SearchSaleSelectedForReport);
                }
                else
                {
                    this.currentSale = this.SelectedSale;
                    if (this.AssociatedView == ViewType.ARDispatch || this.AssociatedView == ViewType.ARDispatch2 ||
                        this.AssociatedView == ViewType.Order || this.AssociatedView == ViewType.Order2)
                    {
                        Messenger.Default.Send(this.AssociatedView, Token.DisplaySaleSearchScreen);
                    }
                    else if (!this.INMasterMode)
                    {
                        Messenger.Default.Send(this.AssociatedView);
                    }
            
                    Messenger.Default.Send(this.SelectedSale, Token.SearchSaleSelected);
                }
            }

            Messenger.Default.Send(Token.Message, Token.SetTopMost);
            this.Close();
        }

        /// <summary>
        /// Handler for the form loaded event.
        /// </summary>
        protected virtual void OnLoadingCommandExecute()
        {
            this.FromDate = ApplicationSettings.SalesSearchFromDate;
            this.ToDate = DateTime.Today;
            this.KeepVisible = Settings.Default.SaleSearchDataKeepVisible;
            this.ShowAllOrders = ApplicationSettings.SalesSearchShowAllOrders;
            this.IsFormLoaded = true;
            this.SetControlMode(ControlMode.OK);
        }

        protected void SaveSearchDateType()
        {
            if (this.AssociatedView == ViewType.LairageOrder)
            {
            
                return;
            }

            if (this.AssociatedView == ViewType.LairageIntake)
            {
          
                return;
            }

            if (this.AssociatedView == ViewType.Quote)
            {
    
                return;
            }

            if (this.AssociatedView == ViewType.Order)
            {
                ApplicationSettings.SalesSearchDateSaleOrder = this.SelectedSearchType;
                return;
            }

            if (this.AssociatedView == ViewType.StockMove)
            {
              
            }

            if (this.AssociatedView == ViewType.Order2)
            {
                ApplicationSettings.SalesSearchDateSaleOrder = this.SelectedSearchType;
                return;
            }

            if (this.AssociatedView == ViewType.ARDispatch)
            {
                ApplicationSettings.SalesSearchDateDispatch = this.SelectedSearchType;
                return;
            }

            if (this.AssociatedView == ViewType.ARDispatch2)
            {
                ApplicationSettings.SalesSearchDateDispatch = this.SelectedSearchType;
                return;
            }

            if (this.AssociatedView == ViewType.Invoice)
            {
                ApplicationSettings.SalesSearchDateInvoice = this.SelectedSearchType;
                return;
            }

            if (this.AssociatedView == ViewType.APQuote)
            {
 
                return;
            }

            if (this.AssociatedView == ViewType.APOrder)
            {
           
                return;
            }

            if (this.AssociatedView == ViewType.APReceipt)
            {
            
            }
        }

        /// <summary>
        /// Refresh the calling orders.
        /// </summary>
        protected virtual void Refresh()
        {
            ApplicationSettings.SalesSearchFromDate = this.fromDate;
            ApplicationSettings.SalesSearchShowAllOrders = this.showAllOrders;
            if (this.AssociatedView == ViewType.LairageOrder)
            {
                this.Locator.LairageOrder.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.LairageIntake)
            {
                this.Locator.Lairage.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.Quote)
            {
                this.Locator.Quote.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.Order)
            {
                this.Locator.Order.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.StockMove)
            {
                this.Locator.StockMovement.RefreshSales();
            }

            if (this.AssociatedView == ViewType.Order2)
            {
                this.Locator.Order2.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.ARDispatch)
            {
                this.Locator.ARDispatch.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.ARDispatch2)
            {
                this.Locator.ARDispatch2.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.Invoice)
            {
                this.Locator.Invoice.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.PurchaseInvoice)
            {
                this.Locator.APInvoice.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.APQuote)
            {
                this.Locator.APQuote.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.APOrder)
            {
                this.Locator.APOrder.RefreshSales();
                return;
            }

            if (this.AssociatedView == ViewType.APReceipt)
            {
                this.Locator.APReceipt.RefreshSales();
            }

            if (this.AssociatedView == ViewType.ARReturns)
            {
                this.Locator.ARReturn.RefreshSales();
            }

            if (this.AssociatedView == ViewType.CarcassDispatch)
            {
                Messenger.Default.Send(Token.Message, Token.RefreshCarcassDispatch);
            }

            if (this.AssociatedView == ViewType.MultiSelectReportDisplay)
            {
                var orderStatuses = new List<int?>
                {
                    NouvemGlobal.NouDocStatusComplete.NouDocStatusID, 
                    NouvemGlobal.NouDocStatusFilled.NouDocStatusID,
                    NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                    NouvemGlobal.NouDocStatusInProgress.NouDocStatusID
                };

                var reportItems = this.DataManager.GetAllOrdersForReport(orderStatuses).Where(x => x.DeliveryDate >= this.FromDate && x.DeliveryDate <= this.ToDate);
                this.FilteredSales = new ObservableCollection<Sale>(reportItems);
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.SelectedSale == null || this.SelectedSale.SaleID == 0)
            {
                return;
            }

            #endregion

            if (this.AssociatedView == ViewType.ARDispatch || this.AssociatedView == ViewType.ARDispatch2)
            {
                var dispatchId = this.SelectedSale.SaleID;
                var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { dispatchId.ToString() } } };
                var reportData = new ReportData { Name = ReportName.DispatchDocket, ReportParameters = reportParam };

                try
                {
                    if (preview)
                    {
                        this.ReportManager.PreviewReport(reportData);
                    }
                    else
                    {
                        this.ReportManager.PrintReport(reportData);
                    }
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }
            }
        }

        /// <summary>
        /// Handler for the form unloaded event.
        /// </summary>
        protected virtual void OnClosingCommandExecute()
        {
            #region message deregistration

            Messenger.Default.Unregister<string>(this);

            #endregion

            this.SaveSearchDateType();
            this.KeepVisible = false;
            this.IsFormLoaded = false;
            this.Close();
        }

        /// <summary>
        /// Handler for the selection of a sale.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            ApplicationSettings.SalesSearchFromDate = this.fromDate;
            ApplicationSettings.SalesSearchToDate = this.ToDate;
            Settings.Default.SaleSearchDataKeepVisible = this.KeepVisible;
            ApplicationSettings.SalesSearchShowAllOrders = this.ShowAllOrders;
            Settings.Default.Save();

            if (this.SelectedSales.Count > 1)
            {
                if (this.ReportMode)
                {
                    if (this.AssociatedView == ViewType.MultiSelectReportDisplay)
                    {
                        Messenger.Default.Send(this.selectedSales.ToList(), Token.SearchSaleSelectedForMultiSelectReport);
                    }
                    else
                    {
                        Messenger.Default.Send(this.selectedSales.ToList(), Token.SearchSaleSelectedForReport);
                    }
                }
                else if (this.AssociatedView == ViewType.CarcassDispatch)
                {
                    Messenger.Default.Send(this.selectedSales.ToList(), Token.SearchSaleSelectedForReport);
                }
            }
            else if (this.AssociatedView == ViewType.MultiSelectReportDisplay && this.SelectedSales.Count == 1)
            {
                Messenger.Default.Send(this.selectedSales.First(), Token.SearchSaleSelectedForReport);
            }
            else
            {
                this.SaleSelectedCommandExecute();
            }
        }

        /// <summary>
        /// Handler for the cancelling of the sale search.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            ApplicationSettings.SalesSearchFromDate = this.fromDate;
            ApplicationSettings.SalesSearchToDate = this.ToDate;
            Settings.Default.SaleSearchDataKeepVisible = this.KeepVisible;
            ApplicationSettings.SalesSearchShowAllOrders = this.ShowAllOrders;
            Settings.Default.Save();

            this.keepVisible = false;
            this.Close();
        }

        /// <summary>
        /// Overrides the control selection, ensuring OK is always the control mode.
        /// </summary>
        /// <param name="mode">The mode to change to.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #endregion

        #region private

        #region command execution

        #endregion

        #region helper

        

        #endregion

        #endregion
    }
}

