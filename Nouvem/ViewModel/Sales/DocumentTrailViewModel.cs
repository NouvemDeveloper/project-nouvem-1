﻿// -----------------------------------------------------------------------
// <copyright file="DocumentTrailViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Sales
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class DocumentTrailViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The trail documents.
        /// </summary>
        private ObservableCollection<Sale> documents = new ObservableCollection<Sale>();

        /// <summary>
        /// The selected document.
        /// </summary>
        private Sale selectedDocument;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentTrailViewModel"/> class.
        /// </summary>
        public DocumentTrailViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            // Display the selected document.
            this.DisplayDocumentCommand = new RelayCommand(this.DisplayDocumentCommandExecute);

            #endregion
        }

        #endregion

        #region public interface

        #region property
    
        /// <summary>
        /// Gets or sets the trail documents.
        /// </summary>
        public ObservableCollection<Sale> Documents
        {
            get
            {
                return this.documents;
            }

            set
            {
                this.documents = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected documents.
        /// </summary>
        public Sale SelectedDocument
        {
            get
            {
                return this.selectedDocument;
            }

            set
            {
                this.selectedDocument = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to display the selected document.
        /// </summary>
        public ICommand DisplayDocumentCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Sets the documents.
        /// </summary>
        /// <param name="docs">The document trail collection.</param>
        public void SetDocuments(IList<Sale> docs)
        {
            this.Documents = new ObservableCollection<Sale>(docs);
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Overrides the mode selection, to only allow add.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region helper

        #region command execution

        /// <summary>
        /// Displays the selected document.
        /// </summary>
        private void DisplayDocumentCommandExecute()
        {
            #region validation

            if (this.SelectedDocument == null)
            {
                return;
            }

            #endregion

            if (this.selectedDocument.DocumentTrailType.Equals(Constant.Quote))
            {
                Messenger.Default.Send(ViewType.Quote);
                this.Locator.Quote.Sale = this.SelectedDocument;
                return;
            }

            if (this.selectedDocument.DocumentTrailType.Equals(Constant.Order))
            {
                Messenger.Default.Send(ViewType.Order);
                this.Locator.Order.Sale = this.SelectedDocument;
                return;
            }

            if (this.selectedDocument.DocumentTrailType.Equals(Constant.Dispatch))
            {
                Messenger.Default.Send(ViewType.ARDispatch);
                this.Locator.ARDispatch.Sale = this.SelectedDocument;
                return;
            }

            if (this.selectedDocument.DocumentTrailType.Equals(Constant.Invoice))
            {
                Messenger.Default.Send(ViewType.Invoice);
                this.Locator.Invoice.Sale = this.SelectedDocument;
                return;
            }

            if (this.selectedDocument.DocumentTrailType.Equals(Constant.APQuote))
            {
                Messenger.Default.Send(ViewType.APQuote);
                this.Locator.APQuote.Sale = this.SelectedDocument;
                return;
            }

            if (this.selectedDocument.DocumentTrailType.Equals(Constant.APOrder))
            {
                Messenger.Default.Send(ViewType.APOrder);
                this.Locator.APOrder.Sale = this.SelectedDocument;
                return;
            }

            if (this.selectedDocument.DocumentTrailType.Equals(Constant.GoodsIn))
            {
                Messenger.Default.Send(ViewType.APReceipt);
                this.Locator.APReceipt.Sale = this.SelectedDocument;
            }

            if (this.selectedDocument.DocumentTrailType.Equals(Strings.Payment))
            {
                Messenger.Default.Send(ViewType.Payment);
                Messenger.Default.Send(this.SelectedDocument, Token.PaymentProposalSelected);
                return;
            }
        }

        #endregion

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearDocumentTrail();
            Messenger.Default.Send(Token.Message, Token.CloseDocumentTrailWindow);
        }

        #endregion

        #endregion
    }
}
