﻿// -----------------------------------------------------------------------
// <copyright file="ARDispatchDetailsViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Grid;
using Nouvem.View.Production.IntoProduction;

namespace Nouvem.ViewModel.Sales.ARDispatch
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.Model.DataLayer;
    using Nouvem.ViewModel.Purchases.APReceipt;

    public class ARDispatchDetailsViewModel : APReceiptDetailsViewModel
    {
        #region field

        /// <summary>
        /// The dispatch vm (dispatch touchscreen or desktop)
        /// </summary>
        private SalesViewModelBase dispatchViewModel;

        /// <summary>
        /// Are we scanning flag.
        /// </summary>
        private bool scanningStock;

        /// <summary>
        /// The selected production order data.
        /// </summary>
        private GoodsReceiptData productionOrderBatchData;

        /// <summary>
        /// The selected production order data.
        /// </summary>
        private GoodsReceiptData plusManualGoodsData;

        /// <summary>
        /// The manual weight.
        /// </summary>
        private bool manualWeight;

        /// <summary>
        /// The selected production batch, used for product weighings at dispatch.
        /// </summary>
        private ProductionData batchSelectedForProductWeighing;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ARDispatchDetailsViewModel"/> class.
        /// </summary>
        public ARDispatchDetailsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            this.OnLoadingCommand = new RelayCommand(() => this.SetControlMode(ControlMode.OK));

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the split product id.
        /// </summary>
        public int SplitProductId { get; set; }

        /// <summary>
        /// Gets or sets the incoming production batch number..
        /// </summary>
        public BatchNumber ProductionBatchNumber { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to record a dispatch transaction.
        /// </summary>
        public ICommand RecordDispatchTransactionCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        protected override void PrintCommandExecute()
        {
            try
            {
                if (this.SelectedGoodsInDetail != null)
                {
                    int? partnerGroupId = null;
                    int? productGroupId = null;
                    var transaction = this.DataManager.GetStockTransactionBySerial(this.SelectedGoodsInDetail.SerialNumber.ToInt(), 7);
                    var localPartner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(
                            x => x.Details.BPMasterID == transaction.Transaction.BPMasterID);
                    if (localPartner != null)
                    {
                        partnerGroupId = localPartner.Details.BPGroupID;
                    }

                    var localProduct =
                        NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == transaction.INMasterID);
                    if (localProduct != null)
                    {
                        productGroupId = localProduct.Master.INGroupID;
                    }

                    var process = this.Processes.FirstOrDefault(x => x.ProcessID == transaction.Transaction.ProcessID);
                    var labels = this.PrintManager.ProcessPrinting(transaction.BPMasterID,
                        partnerGroupId, transaction.INMasterID, productGroupId,
                        ViewType.ARDispatch, LabelType.Item, transaction.StockTransactionID.ToInt(),
                        1, warehouseId: transaction.Transaction.WarehouseID, process: process);
                    if (labels.Item1.Any())
                    {
                        Application.Current.Dispatcher.BeginInvoke(new Action(() => this.DataManager.AddLabelToTransaction(transaction.StockTransactionID.ToInt(), string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3)), DispatcherPriority.Background);
                    }

                    SystemMessage.Write(MessageType.Priority, string.Format(Message.LabelPrinted, transaction.Serial.ToInt()));
                    return;
                }
            }
            catch (Exception e)
            {
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
        }

        /// <summary>
        /// Override, to disable updating an invoiced docket.
        /// </summary>
        /// <param name="e">The event arg.</param>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            base.UpdateCommandExecute(e);
        }

        /// <summary>
        /// Override, to disable editing an invoiced docket.
        /// </summary>
        /// <param name="mode">The current mode.</param>
        protected override void ControlCommandExecute(string mode)
        {
            if (this.IsReadOnly)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            base.ControlCommandExecute(mode);
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    //this.AddSale();
                    break;

                case ControlMode.Find:
                    // this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateWeights();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Direct the incoming sale detail.
        /// </summary>
        protected override void HandleSaleDetail()
        {
            this.HandleDisplaySaleDetail(NouvemGlobal.TransactionTypeGoodsDeliveryId);
        }

        /// <summary>
        /// Update the transaction weights.
        /// </summary>
        protected override void UpdateWeights()
        {
            if (!this.AuthorisationsManager.AllowUserToEditTransactions)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            if (this.Locator.ARDispatch.IsBaseDocument)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            if (this.DataManager.UpdateTransactionWeight(this.transactionsToUpdate, ViewType.ARDispatch))
            {
                //this.Locator.APReceipt.UpdateSaleData();
                SystemMessage.Write(MessageType.Priority, Message.UpdateSuccessful);
                this.transactionsToUpdate.Clear();
                Messenger.Default.Send(Token.Message, Token.RefreshMasterDocument);
                this.SetControlMode(ControlMode.OK);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.UpdateSuccessful);
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearARDispatchDetails();
            Messenger.Default.Unregister(this);
            Messenger.Default.Send(true, Token.CloseARDispatchDetailsWindow);
        }

        #endregion

        #region private

        #endregion
    }
}

