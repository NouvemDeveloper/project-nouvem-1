﻿// -----------------------------------------------------------------------
// <copyright file="ARDispatchViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections;
using System.Windows;
using DevExpress.Xpf.Editors.Helpers;

namespace Nouvem.ViewModel.Sales.ARDispatch
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using System.Windows.Threading;
    using DevExpress.Utils;
    using DevExpress.Xpf.Grid;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.Reporting.WinForms;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.ViewModel.Sales;

    public class ARDispatchViewModel : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// Product removal in progress.
        /// </summary>
        private bool processingDeletion;

        /// <summary>
        /// The order no.
        /// </summary>
        private int orderNo;

        /// <summary>
        /// The transaction count.
        /// </summary>
        private int transactionCount;

        /// <summary>
        /// The transaction count.
        /// </summary>
        private int orderCount;

        /// <summary>
        /// The transaction count.
        /// </summary>
        private int boxCount;

        /// <summary>
        /// The transaction count.
        /// </summary>
        private decimal totalDocketWeight;

        /// <summary>
        /// The suppliers collection.
        /// </summary>
        private ObservableCollection<ViewBusinessPartner> suppliers;

        /// <summary>
        /// The incoming product quantity.
        /// </summary>
        private decimal productQuantity;

        /// <summary>
        /// The container reference.
        /// </summary>
        private string containerRef;

        /// <summary>
        /// Is a price change uncommitted flag.
        /// </summary>
        private bool uncommittedPriceChange;

        /// <summary>
        /// The selected dispatch containers.
        /// </summary>
        private Model.BusinessObject.DispatchContainer selectedDispatchContainer;

        /// <summary>
        /// The recent dispatches.
        /// </summary>
        private ObservableCollection<Sale> dispatches = new ObservableCollection<Sale>();

        /// <summary>
        /// The selected dispatch.
        /// </summary>
        private Sale selectedDispatch;

        /// <summary>
        /// The price methods.
        /// </summary>
        private IList<NouPriceMethod> priceMethods = new List<NouPriceMethod>();

        /// <summary>
        /// Flag, as to whether the dispatch is to be auto copied to an invoice.
        /// </summary>
        private bool autoCopyToInvoice;

        /// <summary>
        /// The allow auto weigh flag.
        /// </summary>
        protected bool allowAutoWeigh = true;

        /// <summary>
        /// Flag, as to whether the current product is highlighted on ui load.
        /// </summary>
        protected bool highlightSelectedProduct;

        /// <summary>
        /// The clear traceability flag.
        /// </summary>
        protected bool clearTraceability = true;

        /// <summary>
        /// The clear traceability flag.
        /// </summary>
        protected string OrderPopUpNote;

        /// <summary>
        /// The product batch id.
        /// </summary>
        protected int? prOrderId;

        /// <summary>
        /// Are we recoding a weight from the scales flag.
        /// </summary>
        protected bool recordingWeightFromScales;

        /// <summary>
        /// The display popup flag.
        /// </summary>
        protected bool displayPopUpNote;

        protected bool printLabel;

        /// <summary>
        /// Flag, as to whether tares from the calculator are to be applied.
        /// </summary>
        protected bool applyTares;

        /// <summary>
        /// Flag, as to whether we are scanning a multivac label for the batch data.
        /// </summary>
        protected bool scanningLabelForBatchNo;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ARDispatchViewModel"/> class.
        /// </summary>
        public ARDispatchViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.ClosingDispatchWindow, s =>
            {
                this.Close();
            });

            // Refresh the current sale when a price list is updated.
            Messenger.Default.Register<string>(this, Token.RefreshMasterDocument, s =>
            {
                this.DataManager.PriceDispatchDocket(this.Sale.SaleID);
                this.Sale = this.DataManager.GetARDispatchById(this.Sale.SaleID);
            });

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            // Register to handle the authorisation result.
            Messenger.Default.Register<string>(this, Token.AuthorisationResult, this.HandleUnfilledOrderAuthorisation);

            // Register to handle the scanner read.
            Messenger.Default.Register<string>(this, Token.Scanner, s =>
            {
                this.HandleScannerData(s);
            });

            // Register for a product selection.
            Messenger.Default.Register<Tuple<ViewType, InventoryItem>>(this, Token.ProductSelected,
                t =>
                {
                    var product = t.Item2;
                    if (t.Item1 == ViewType.ARDispatch)
                    {
                        if (product != null)
                        {
                            var detailToAdd = new SaleDetail { INMasterID = product.Master.INMasterID, InventoryItem = product, NouStockMode = product.StockMode };
                            this.SaleDetails.Add(detailToAdd);
                            this.SelectedSaleDetail = detailToAdd;
                        }
                    }
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<BusinessPartner>(
                this,
                Token.BusinessPartnerSelected,
                partner =>
                {
                    if (this.Locator.BPSearchData.CurrentSearchType == ViewType.ARDispatch)
                    {
                        this.SelectedPartner =
                            this.Customers.FirstOrDefault(x => x.BPMasterID == partner.Details.BPMasterID);
                    }
                });

            // Register for a business partner search screen selection.
            Messenger.Default.Register<Telesale>(
                this,
                Token.BusinessPartnerSelected,
                sale =>
                {
                    this.SelectedPartner =
                            this.Customers.FirstOrDefault(x => x.BPMasterID == sale.BPMasterID);
                    this.CurrentTelesale = sale;
                });

            // Register to handle the incoming goods in details (traceability data, and total weight).
            //Messenger.Default.Register<SaleDetail>(this, Token.UpdateGoodsInQtyReceived, this.HandleDispatchData);

            Messenger.Default.Register<decimal>(this, Token.UpdateWeight, d =>
            {
                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.WeightReceived += d;
                }
            });

            #endregion

            #region command handler

            this.RemoveDispatchItemCommand = new RelayCommand(() => this.RemoveDispatchItemCommandExecute());

            this.ResetCountCommand = new RelayCommand(() => this.TransactionCount = 0);

            this.MoveBackCommand = new RelayCommand(this.MoveBackCommandExecute);

            // Complete order command.
            this.CompleteOrderCommand = new RelayCommand(() => this.CompleteOrder());

            // Drul down to the items window.
            this.DrillDownToDispatchItemsCommand = new RelayCommand(this.DrillDownToDispatchItemsCommandExecute);

            // Handle the attempt to remove the selected product.
            this.RemoveProductCommand = new RelayCommand(this.RemoveProductCommandExecute);
   
            // Handler to display the search screen
            this.DisplaySearchScreenCommand = new RelayCommand<KeyEventArgs>(e => this.DisplaySearchScreenCommandExecute(Tuple.Create(false, ViewType.ARDispatch)),
                e => e.Key == Key.F1 && this.CurrentColumn != null && this.CurrentColumn.Equals("INMasterID"));

            // Handler to search for a sale using the user input search text.
            this.FindSaleCommand = new RelayCommand(this.FindSaleCommandExecute);

            // Command to remove the selected item from the order.
            this.RemoveItemCommand = new RelayCommand(() => this.RemoveItem(ViewType.ARDispatch));

            // Command to handle a drill down to manual serial entry.
            this.DrillDownManualSerialCommand = new RelayCommand(this.DrillDownManualSerialCommandExecute);

            // Handle the invoice selection.
            this.DispatchSelectedCommand = new RelayCommand(() =>
            {
                this.Sale = this.SelectedDispatch;
                this.SelectedTab = 0;
            });

            #endregion

            this.GetSearchTypes();
            this.AutoCopyToInvoice = ApplicationSettings.AutoCreateInvoiceOnOrderCompletion;
            this.GetLocalDocNumberings();
            this.ShowRoute = true;
            this.SetUpTimer();
            this.CanOverrideOrderLimit = this.AuthorisationsManager.AllowUserToOverrideOrderLimit;
            this.CanOverrideCreditLimit = this.AuthorisationsManager.AllowUserToCreateSaleOrderExceedingCreditLimit;
            this.SetRecordWeightModes();
        }

        #endregion

        #region public interface

        #region property

        public bool PrintLabel
        {
            get { return this.printLabel && !ApplicationSettings.DisablePrintLabelAtDispatch; }
        }

        /// <summary>
        /// The record weight modes.
        /// </summary>
        public IList<DispatchMode> RecordWeightModes { get; set; }

        /// <summary>
        /// Gets or sets the transaction count.
        /// </summary>
        public int OrderCount
        {
            get
            {
                return this.orderCount;
            }

            set
            {
                this.orderCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the transaction count.
        /// </summary>
        public int BoxCount
        {
            get
            {
                return this.boxCount;
            }

            set
            {
                this.boxCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the transaction count.
        /// </summary>
        public decimal TotalDocketWeight
        {
            get
            {
                return this.totalDocketWeight;
            }

            set
            {
                this.totalDocketWeight = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the transaction count.
        /// </summary>
        public int TransactionCount
        {
            get
            {
                return this.transactionCount;
            }

            set
            {
                if (value < 0)
                {
                    value = 0;
                }

                this.transactionCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the border number.
        /// </summary>
        public int OrderNo
        {
            get
            {
                return this.orderNo;
            }

            set
            {
                this.orderNo = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the container reference.
        /// </summary>
        public string ContainerRef
        {
            get
            {

                return this.containerRef;
            }

            set
            {
                this.containerRef = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected container.
        /// </summary>
        public Model.BusinessObject.DispatchContainer SelectedDispatchContainer
        {
            get
            {
                return this.selectedDispatchContainer;
            }

            set
            {
                this.selectedDispatchContainer = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    if (this.Sale != null && !this.EntitySelectionChange && ApplicationSettings.CheckForShippingContainerDateMatch)
                    {
                        // setting a container, so need to ensure shipping delivery date matches the container shipping date
                        var containerDate = this.DataManager.GetDispatchContainerDeliveryDate(value.DispatchContainerID);
                        if (containerDate != null && this.Sale.ShippingDate != null)
                        {
                            var localContainerDate = containerDate.ToDate().Date;
                            var dispatchDate = this.Sale.ShippingDate.ToDate().Date;
                            if (localContainerDate != dispatchDate)
                            {
                                NouvemMessageBox.Show(string.Format(Message.DispatchContainerDeliveryDateMismatch, localContainerDate.ToShortDateString(), dispatchDate.ToShortDateString()), touchScreen:true);
                                this.selectedDispatchContainer = null;
                                return;
                            }
                        }
                    }

                    this.ContainerRef = value.Reference;
                    this.MoveBackCommandExecute();
                }
                else
                {
                    this.ContainerRef = string.Empty;
                }
            }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether we are in display sale mode only.
        /// </summary>
        public bool DisplayModeOnly { get; set; }

        /// <summary>
        /// Gets or sets the dispatches.
        /// </summary>
        public ObservableCollection<Sale> Dispatches
        {
            get
            {
                return this.dispatches;
            }

            set
            {
                this.dispatches = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected dispatch.
        /// </summary>
        public Sale SelectedDispatch
        {
            get
            {
                return this.selectedDispatch;
            }

            set
            {
                this.selectedDispatch = value;
                this.RaisePropertyChanged();
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether a dispatch docket is to be auto copied to an invoice.
        /// </summary>
        public bool AutoCopyToInvoice
        {
            get
            {
                return this.autoCopyToInvoice;
            }

            set
            {
                this.autoCopyToInvoice = value;
                this.RaisePropertyChanged();
                ApplicationSettings.AutoCreateInvoiceOnOrderCompletion = value;
            }
        }

        /// <summary>
        /// Gets or sets the product qty selected.
        /// </summary>
        public decimal ProductQuantity
        {
            get
            {
                return this.productQuantity;
            }

            set
            {
                this.productQuantity = value;
                //Messenger.Default.Send(value, Token.QuantitySelected);
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to reset the scanned/weighed count to 0.
        /// </summary>
        public ICommand RemoveDispatchItemCommand { get; private set; }

        /// <summary>
        /// Gets the command to reset the scanned/weighed count to 0.
        /// </summary>
        public ICommand ResetCountCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unload event
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the dispatch completion.
        /// </summary>
        public ICommand CompleteOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selected dispatch.
        /// </summary>
        public ICommand DispatchSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to remove the selected product.
        /// </summary>
        public ICommand RemoveProductCommand { get; private set; }

        /// <summary>
        /// Gets the command to search for a sale.
        /// </summary>
        public ICommand FindSaleCommand { get; private set; }

        /// <summary>
        /// Gets the command to edit the transactions.
        /// </summary>
        public ICommand EditTransactionsCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down for manual serial entry.
        /// </summary>
        public ICommand DrillDownManualSerialCommand { get; private set; }

        /// <summary>
        /// Gets the command to drill down to the items.
        /// </summary>
        public ICommand DrillDownToDispatchItemsCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Show the sales.
        /// </summary>
        public void ShowAllSales()
        {
            this.ShowSales();
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        public void FindDispatchSales(int productID)
        {
            var dockets = this.DataManager.GetAllProductDockets(productID);
  
            var orderStatuses = this.DocStatusesOpen;
            this.CustomerSales = this.DataManager.GetAllARDispatches(orderStatuses)
                     .Where(x => dockets.Contains(x.SaleID)).ToList();

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.ARDispatch), Token.SearchForSale);
            Messenger.Default.Send(true, Token.SetTopMost);
            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.ARDispatch);
            }
        }


        /// <summary>
        /// Handle the box label print for any qty.
        /// </summary>
        public void PrintBoxLabelAnyQty()
        {
            #region validation

            if (this.SelectedSaleDetail == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductSelected);
                return;
            }

            //if (this.SelectedSaleDetail == null || this.SelectedSaleDetail.GoodsReceiptDatas == null)
            //{
            //    SystemMessage.Write(MessageType.Issue, Message.NoBoxProductSelected);
            //    return;
            //}

            //if (!this.SelectedSaleDetail.GoodsReceiptDatas.Any())
            //{
            //    return;
            //}

            #endregion

            // add the transaction weight to our stock transaction, and other relevant data.
            //var localGoodsReceiptData = this.SelectedSaleDetail.GoodsReceiptDatas.First();
            var qtyPerBox = -1;

            var trans = new StockTransaction
            {
                //BatchNumberID = localGoodsReceiptData.BatchNumber.BatchNumberID,
                MasterTableID = this.SelectedSaleDetail.SaleDetailID,
                TransactionDate = DateTime.Now,
                INMasterID = this.SelectedSaleDetail.InventoryItem.Master.INMasterID,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId,
                WarehouseID = NouvemGlobal.DispatchId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                IsBox = true
            };

            try
            {
                var transactionId = this.DataManager.AddBoxTransaction(trans, qtyPerBox);
                if (transactionId > 0)
                {
                    this.Print(transactionId, LabelType.Box);
                    this.BoxCount++;
                    SystemMessage.Write(MessageType.Priority, Message.BoxLabelPrinted);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBoxProductsFound);
                }
            }
            catch (Exception)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductsFound);
            }
        }

        /// <summary>
        /// Handles the incoming dispatch details, by setting the control mode.
        /// </summary>
        public void HandleDispatchData(SaleDetail detail, decimal wgt = 0, decimal qty = 0)
        {
            try
            {
                if (!this.ValidateData(
                    this.traceabilityDataTransactions,
                    this.Sale.SaleID,
                    this.SelectedSaleDetail.StockDetailToProcess.TransactionWeight.ToDecimal(),
                    this.SelectedSaleDetail.StockDetailToProcess.TransactionQty.ToDecimal(),
                    this.SelectedSaleDetail.StockDetailToProcess.INMasterID,
                    this.SelectedSaleDetail.StockDetailToProcess.WarehouseID,
                    this.SelectedSaleDetail.StockDetailToProcess.ProcessID.ToInt(),
                    Constant.Dispatch,
                    ApplicationSettings.UseLegacyUseByAttribute ? this.Attribute6 : this.Attribute1,
                    this.SelectedSaleDetail.UnitPrice.ToDecimal().ToString(),
                    this.AuthorisationsManager.AllowUserToDispatchZeroPricedProduct.ToString(),
                    this.recordingWeightFromScales.ToString()))
                {
                    return;
                };
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
            }

            try
            {
                this.Log.LogDebug(this.GetType(), "HandleDispatchData(): SaleDetail returned");
                bool? localIsBox = this.SelectedSaleDetail?.IsBox;
                var localDetailsToProcess = detail.StockDetailToProcess;
                if (ApplicationSettings.AllowDuplicateProductOnOrder)
                {
                    var localDetails = this.SaleDetails.Where(x => x.INMasterID == detail.INMasterID).ToList();
                    if (!localDetails.Any())
                    {
                        return;
                    }

                    var localDetail = localDetails.First();
                    if (localDetails.Count > 1 && localDetail.CheckLineStatus())
                    {
                        // line filled, so use duplicate product.
                        localDetail = localDetails.ElementAt(1);
                    }

                    this.DoNotHandleSelectedSaleDetail = true;
                    this.SelectedSaleDetail = localDetail;
                    this.DoNotHandleSelectedSaleDetail = false;
                }
                else
                {
                    this.DoNotHandleSelectedSaleDetail = true;

                    //if (this.SelectedSaleDetail.OrderBatchID.HasValue)
                    //{
                    //    this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault
                    //        (x => x.INMasterID == detail.INMasterID && x.OrderBatchID == this.SelectedSaleDetail.OrderBatchID);
                    //}
                    //else
                    //{
                    //    this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                    //}
                    this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);

                    this.DoNotHandleSelectedSaleDetail = false;
                }

                if (this.SelectedSaleDetail != null)
                {
                    this.SelectedSaleDetail.IsBox = localIsBox;

                    // ensure the price list data is set (this relates to the touchscreen only)
                    if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.PriceListID == 0)
                    {
                        this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
                    }

                    this.SelectedSaleDetail.StockDetailToProcess = localDetailsToProcess;
                    if (ApplicationSettings.UseHistoricDispatchMode)
                    {
                        // TODO: Temp fallback until we are sure new development is ok.
                        this.CompleteTransactionHistoric(wgt, qty);
                    }
                    else
                    {
                        var stockTransactions = this.StockLabelsToPrint > 1 ? this.StockLabelsToPrint : 1;
                        for (int i = 0; i < stockTransactions; i++)
                        {
                            this.CompleteTransaction(wgt, qty);
                        }
                    }
                }
            }
            finally
            {
                this.SetControlMode(ControlMode.Update);
                this.StockLabelsToPrint = 1;
            }
        }

        /// <summary>
        /// Determines if a product is on a sale order.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Flag, as to whether the product is on the order.</returns>
        public bool IsDispatchItemOnOrder(int productId)
        {
            if (this.Sale == null || this.SaleDetails == null || !this.SaleDetails.Any())
            {
                // no order details, so allow product to be scanned onto docket.
                return true;
            }

            return this.SaleDetails.Any(x => x.INMasterID == productId);
        }

        /// <summary>
        /// Adds the incoming dispatch product.
        /// </summary>
        /// <param name="detail">The incoming sale detail.</param>
        public void SetSaleDetail(SaleDetail detail)
        {
            if (this.SaleDetails == null)
            {
                this.SaleDetails = new ObservableCollection<SaleDetail>();
            }

            if (this.SaleDetails.Any())
            {
                var localDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == detail.INMasterID);
                if (localDetail != null)
                {
                    return;
                }
            }
          
            this.SaleDetails.Add(detail);
            this.DoNotHandleSelectedSaleDetail = true;
            this.SelectedSaleDetail = detail;
            this.DoNotHandleSelectedSaleDetail = false;
        }

        /// <summary>
        /// Copies a sale to another document.
        /// </summary>
        /// <param name="saleToCopy">The sale to copy to.</param>
        public override void CopySale(Sale saleToCopy)
        {
            this.Log.LogDebug(this.GetType(), "CopySale(): Copying sale");

            this.DisablePartnerPopUp = true;
            if (saleToCopy.SaleDetails != null)
            {
                // need to attach the inventory item, as it's needed for the in master snapshot.
                foreach (var detail in saleToCopy.SaleDetails)
                {
                    detail.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID);
                }
            }

            if (ApplicationSettings.AccountsCustomer.CompareIgnoringCase(Customer.Woolleys))
            {
                this.Sale.DocumentDate = saleToCopy.CreationDate;
            }

            this.Sale = saleToCopy;

            //var dispatchId = this.DataManager.GetARDispatchIDForSaleOrder(this.Sale.SaleID);

            this.Sale.SaleID = 0;
            this.SetControlMode(ControlMode.Add);
        }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Handles a serial dispatch entry.
        /// </summary>
        /// <param name="serial">The entered serial.</param>
        protected virtual void HandleSerialEntry(int serial) { }

        #region override
        protected override bool CheckForAutoBoxing()
        {
            return this.ScannerStockMode == ScannerMode.ReWeigh;
        }

        protected override void ShowAllRecentOrdersCommandExecute()
        {
            if (!this.ShowAllRecentOrders)
            {
                this.HandleRecentOrders(100);
                Messenger.Default.Send(true, Token.ShowAllRecentOrders);
            }
            else
            {
                this.HandleRecentOrders(ApplicationSettings.RecentOrdersAmtToTake);
                Messenger.Default.Send(false, Token.ShowAllRecentOrders);
            }

            this.ShowAllRecentOrders = !this.ShowAllRecentOrders;
        }

        /// <summary>
        /// Adds a new sale from the desktop.
        /// </summary>
        protected void AddDesktopSale()
        {
            if (this.Sale == null)
            {
                return;
            }

            // we use the copying from order flag to ensure all sale details get prcessed.
            this.Sale.CopyingFromOrder = true;
            this.AddTransaction();
        }

        /// <summary>
        /// Handles the incoming serach selected sale.
        /// </summary>
        /// <param name="searchSale">The incoming sale.</param>
        protected override void HandleSearchSale(Sale searchSale)
        {
            if (searchSale != null)
            {
                this.DataManager.PriceDispatchDocket(searchSale.SaleID);
                this.Sale = this.DataManager.GetARDispatchById(searchSale.SaleID);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (!ViewModelLocator.IsARDispatch2Null())
            {
                if (this.Locator.ARDispatch2.LastEdit > this.LastEdit)
                {
                    return;
                }
            }

            this.Sale = this.DataManager.GetARDispatchByLastEdit();
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetARDispatchByFirstLast(true);
                return;
            }

            this.DataManager.PriceDispatchDocket(this.Sale.SaleID - 1);
            var localSale = this.DataManager.GetARDispatchById(this.Sale.SaleID - 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetARDispatchByFirstLast(false);
                return;
            }

            this.DataManager.PriceDispatchDocket(this.Sale.SaleID + 1);
            var localSale = this.DataManager.GetARDispatchById(this.Sale.SaleID + 1);
            if (localSale != null)
            {
                this.Sale = localSale;
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetARDispatchByFirstLast(true);
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetARDispatchByFirstLast(false);
        }

        /// <summary>
        /// Updates the selected sale detail totals.
        /// </summary>
        protected override void UpdateSaleDetailTotals()
        {
            if (this.SelectedSaleDetail != null)
            {
                this.SelectedSaleDetail.UpdateTotal(dispatch:true);
            }
        }

        /// <summary>
        /// Updates the sale details with any price change.
        /// </summary>
        /// <param name="details">The updated details.</param>
        protected override void HandlePriceListUpdates(List<PriceDetail> details)
        {
            // do nothing
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        /// <param name="mode">The report mode. (Print or Preview).</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                //SystemMessage.Write(MessageType.Issue, Message.InvalidDispatchDocket);
                return;
            }

            #endregion

            this.ReportManager.ProcessModuleReports(65, this.Sale.SaleID.ToString(), !preview);
            return;

            var dispatchId = this.Sale.SaleID;
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { dispatchId.ToString() } } };
            var reportData = new ReportData {Name = ReportName.DispatchDocket, ReportParameters = reportParam};

            try
            {
                if (preview)
                {
                    this.ReportManager.PreviewReport(reportData);
                }
                else
                {
                    this.ReportManager.PrintReport(reportData);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Get the local document status types.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems.Where(x => x != NouvemGlobal.NouDocStatusExported && x != NouvemGlobal.NouDocStatusMoved).ToList();
            this.RefreshDocStatusItems();
        }

        /// <summary>
        /// Overrides the load event, adding a form clearance.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            if (!this.DisplayModeOnly)
            {
                Task.Factory.StartNew(this.GetAllSales);
            }

        
            base.OnLoadingCommandExecute();
            this.ClearForm();
            if (ApplicationSettings.DefaultToFindAtDesktopDispatch)
            {
                this.SetControlMode(ControlMode.Find);
            }
            else
            {
                this.SetControlMode(ControlMode.Add);
            }
          
            this.ScannerStockMode = ScannerMode.Scan;

            if (!this.DisplayModeOnly)
            {
                this.OpenScanner();
            }
        }

        /// <summary>
        /// Overrides the unload event, deregistering the copy message.
        /// </summary>
        protected override void OnUnloadedCommandExecute()
        {
            this.SaleDetails?.Clear();
            this.SaleDetails = null;
            this.Close();
        }

        protected virtual ViewType GetView()
        {
            return ViewType.ARDispatch;
        }

        /// <summary>
        /// Sets the search type values.
        /// </summary>
        protected override void GetSearchTypes()
        {
            base.GetSearchTypes();
            this.SelectedSearchType = this.SearchTypes.First();
        }

        /// <summary>
        /// Creates a sale order object.
        /// </summary>
        protected override void CreateSale()
        {
            this.Log.LogDebug(this.GetType(), "Creating Sale");

            // header
            this.Sale.QuoteValidDate = this.ValidUntilDate;
            this.Sale.DeliveryDate = this.DeliveryDate;
            this.Sale.ShippingDate = this.ShippingDate;
            this.Sale.DocumentDate = this.DocumentDate;
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.Remarks = this.Remarks;
            this.Sale.DeviceId = NouvemGlobal.DeviceId;
            this.Sale.EditDate = DateTime.Now;
            this.Sale.BPContactID = this.SelectedContact != null ? (int?)this.SelectedContact.Details.BPContactID : null;
            this.Sale.DocketNote = this.DocketNote;
            this.Sale.PopUpNote = this.PopUpNote;
            this.Sale.InvoiceNote = this.InvoiceNote;
            this.Sale.SalesEmployeeID = this.SelectedSalesEmployee != null ? (int?)this.SelectedSalesEmployee.UserMaster.UserMasterID : null;
            this.Sale.VAT = this.Tax;
            this.Sale.SubTotalExVAT = this.SubTotal;
            this.Sale.DiscountIncVAT = this.Discount;
            this.Sale.GrandTotalIncVAT = this.Total;
            this.Sale.DiscountPercentage = this.DiscountPercentage;
            this.Sale.Customer = this.SelectedPartner;
            this.Sale.Haulier = this.SelectedHaulier;
            this.Sale.DeliveryAddress = this.SelectedDeliveryAddress;
            this.Sale.InvoiceAddress = this.SelectedInvoiceAddress;
            this.Sale.DeliveryContact = this.SelectedDeliveryContact;
            this.Sale.MainContact = this.SelectedContact;
            this.Sale.PORequired = this.PORequired;
            this.Sale.CustomerPOReference = this.CustomerPOReference;
            this.Sale.PartnerInvoice = this.PartnerInvoiceNote;
            this.Sale.PartnerPopUp = this.CustomerPopUpNote;
            this.Sale.OtherReference = this.Temperature.ToDecimal().ToString();
            if (this.SelectedDispatchContainer != null)
            {
                this.Sale.DispatchContainerID = this.SelectedDispatchContainer.DispatchContainerID;
            }
            else
            {
                this.Sale.DispatchContainerID = null;
            }
            
            if (this.SelectedRoute != null)
            {
                this.Sale.RouteID = this.SelectedRoute.RouteID;
            }

            if (this.NonStandardResponses != null && this.NonStandardResponses.Any())
            {
                this.Sale.NonStandardResponses = new List<Model.DataLayer.Attribute>();
                foreach (var attributeAllocationData in this.NonStandardResponses)
                {
                    this.Sale.NonStandardResponses.Add(new Model.DataLayer.Attribute
                    {
                        Attribute1 = attributeAllocationData.TraceabilityValueNonStandard,
                        AttributeMasterID = attributeAllocationData.AttributeMaster.AttributeMasterID
                    });
                }
            }

            // add the sale details
            this.Sale.SaleDetails = this.SaleDetails;

            if (ApplicationSettings.CreateProductionStockAtDispatch)
            {
                var detail = this.Sale.SaleDetails.FirstOrDefault(x => x.Process);
                if (detail != null)
                {
                    var product = detail.StockDetailToProcess;
                    if (detail.ProductionStockDetails == null)
                    {
                        detail.ProductionStockDetails = this.DataManager.GetRecipeByIngredientProductId(product.INMasterID);
                    }
               
                    detail.StockDetailToProcess.StockDetails = this.DataManager.ParseRecipeByIngredientProducts(detail.ProductionStockDetails, product.INMasterID, product.TransactionWeight.ToDecimal(), this.prOrderId);
                }
            }
        }

        /// <summary>
        /// Override the selected purchase order, resetting it's totals.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            var localCopyValue = CopyingDocument;
            var localSaleId = this.Sale.SaleID;
            base.HandleSelectedSale();
            this.Sale.SaleID = localSaleId;
            this.IsBaseDocument = this.Sale.IsBaseDocument.ToBool();

            //if (this.Sale.SaleDetails != null)
            //{
            //    this.DataManager.GetDispatchTransactionData(this.Sale.SaleDetails);
            //}

            if (this.IsBaseDocument)
            {
                SystemMessage.Write(MessageType.Issue, Message.DocketInvoicedWarning);
                this.IsReadOnly = true;
                this.DisableDocStatusChange = true;
            }

            if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.CheckForExtraDispatchLines)
            {
                this.CheckForExtraDispatchLines();
            }

            if (!localCopyValue && !this.Sale.CopyingFromOrder)
            {
                // We only reset the totals when we are copying from an order.
                return;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.TotalExVAT = 0;
            }

            this.Tax = 0;
            this.Total = 0;
            this.SubTotal = 0;
            this.OrderCost = 0;
            this.OrderMargin = 0;

            // we need to keep the discount % (Setting discount to 0 will reset it to 0)
            this.CalculateSalesTotals = false;
            this.Discount = 0;
        }

        protected override void CalculateMargin()
        {
            try
            {
                if (this.Total > 0)
                {
                    if (this.SaleDetails != null && this.SaleDetails.Any())
                    {
                        this.OrderCost = 0;
                        this.OrderMargin = 0;

                        foreach (var item in this.SaleDetails)
                        {
                            item.CalculateCosts(useDelivery: true);
                            this.OrderCost += item.CostTotal.ToDecimal();
                            item.CostTotal = item.CostTotal;
                        }
                    }

                    this.OrderMargin = Math.Round(((this.Total - this.OrderCost) / this.Total * 100).ToDecimal(), 2);
                }
                else
                {
                    this.OrderMargin = 0;
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Clears the production batch data (In batch scanning mode for tsbloors)
        /// </summary>
        protected virtual void ClearBatchData()
        {
        }

        /// <summary>
        /// Updates the sale order totals.
        /// </summary>
        protected override void UpdateSaleOrderTotals(bool calcDisAmountOnly = true, bool calcDisPercentOnly = false)
        {
            if (this.EntitySelectionChange)
            {
                return;
            }

            // reset the totals/taxes
            this.SubTotal = 0;
            this.Total = 0;
            this.Tax = 0;

            if (this.SaleDetails == null)
            {
                return;
            }

            // get the accumulated totals and taxes.
            this.SaleDetails.ToList().ForEach(item =>
            {
                this.OrderCost += item.CostTotal.ToDecimal();
                this.SubTotal += item.TotalExVAT.ToDecimal();
                this.Total += item.TotalIncVAT.ToDecimal();
            });

            // Set the tax value.
            if (this.Total > this.SubTotal)
            {
                this.Tax = this.Total - this.SubTotal;
            }

            if (calcDisPercentOnly)
            {
                this.CalculateSalesTotals = false;

                // apply the discount (if any)
                if (this.DiscountPercentage > 0)
                {
                    var convertedDiscount = 100M - this.DiscountPercentage.ToDecimal();
                    this.Tax = Math.Round(this.Tax / 100 * convertedDiscount, 2);
                    var discountedTotal = Math.Round(this.Total / 100 * convertedDiscount, 2);

                    // convert the discount amount.
                    this.Discount = this.Total - discountedTotal;

                    // assign the discounted total.
                    this.Total = discountedTotal;
                }
                else
                {
                    this.Discount = 0;
                }
            }

            if (calcDisAmountOnly)
            {
                this.CalculateSalesTotals = false;

                // apply the discount amount (if any)
                if (this.Discount > 0)
                {
                    if (this.SubTotal > 0)
                    {
                        this.DiscountPercentage = Math.Round((this.Discount / this.SubTotal) * 100, 2).ToDecimal();
                        this.Total = this.Total - this.Discount;
                    }
                }
                else
                {
                    if (!this.LockDiscountPercentage)
                    {
                        this.DiscountPercentage = 0;
                    }
                    else
                    {
                        this.UpdateSaleOrderTotals(false, true);
                    }
                }
            }
        }

        /// <summary>
        /// Updates the sale order totals.
        /// </summary>
        protected void UpdateSaleOrderTotals(Sale sale)
        {
            // reset the totals/taxes
            var subTotal = 0M;
            var total = 0M;
            var tax = 0;

            // get the accumulated totals and taxes.
            sale.SaleDetails.ToList().ForEach(item =>
            {
                subTotal += item.TotalExVAT.ToDecimal();
                total += item.TotalIncVAT.ToDecimal();
            });

            sale.SubTotalExVAT = subTotal;
            sale.GrandTotalIncVAT = total;
        }

        /// <summary>
        /// Handle the column focus change.
        /// </summary>
        /// <param name="e">The event arg.</param>
        protected override void OnFocusedColumnChangedCommandExecute(EditorEventArgs e)
        {
            if (this.IsReadOnly)
            {
                return;
            }

            if (e != null && e.Column != null && e.Column.FieldName.Equals("CostTotal"))
            {
                this.DrillDownToCostPriceListCommandExecute();
                return;
            }

            if (e != null && e.Column != null && e.Column.FieldName.Equals("UnitPrice"))
            {
                if (e.Editor.EditValue != null && e.Row is SaleDetail
                    && (decimal?) e.Editor.EditValue != (e.Row as SaleDetail).UnitPriceOld)
                {
                    (e.Row as SaleDetail).UnitPriceOld = (decimal?) e.Editor.EditValue;

                    try
                    {
                        var addingToCustomerBook = false;
                        var message = Message.AddUnitPriceChangeToPriceBookConfirmation;
                        if (this.SelectedSaleDetail.PriceListID != this.SelectedPartner.PriceListID)
                        {
                            addingToCustomerBook = true;
                            message = Message.AddProductAndUnitPriceChangeToPriceBookConfirmation;
                        }
                     
                        var currency = this.SelectedPartner != null && this.SelectedPartner.Symbol != null ? this.SelectedPartner.Symbol.Trim() : ApplicationSettings.CustomerCurrency;

                        NouvemMessageBox.Show(
                            string.Format(message, currency, this.SelectedSaleDetail.UnitPrice), NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (this.SelectedPartner.PriceListID == 0)
                            {
                                SystemMessage.Write(MessageType.Issue, Message.NoPriceBookExists);
                                return;
                            }

                            if (this.SelectedSaleDetail.PriceListID == 0)
                            {
                                NouvemMessageBox.Show(Message.ProductNotOnAnyPriceList, NouvemMessageBoxButtons.YesNo);
                                if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                                {
                                    Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedSaleDetail.INMasterID), Token.DrillDown);
                                    this.Close();
                                }

                                return;
                            }

                            this.UpdatePriceToPriceBook();
                            //System.Threading.Tasks.Task.Factory.StartNew(this.RefreshPriceLists);
                            //this.RefreshPriceDetails();
                            if (addingToCustomerBook)
                            {
                                this.SelectedSaleDetail.LoadingSale = true;
                                this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
                            }
                        }

                        //this.SelectedSaleDetail.SetPriceMethod();
                        //this.SelectedSaleDetail.UnitPrice = (decimal?) e.Editor.EditValue;
                        this.SelectedSaleDetail.UnitPriceAfterDiscount = this.SelectedSaleDetail.UnitPrice;
                        this.UpdateSaleDetailTotals();
                        if (this.Sale != null && this.Sale.SaleID > 0)
                        {
                            this.SetControlMode(ControlMode.Update);
                        }
                        else
                        {
                            this.SetControlMode(ControlMode.Add);
                        }
                        
                        this.uncommittedPriceChange = true;
                    }
                    catch (Exception ex)
                    {
                        this.Log.LogError(this.GetType(), ex.Message);
                        SystemMessage.Write(MessageType.Issue, ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Drill down to the selected item details.
        /// </summary>
        private void DrillDownToDispatchItemsCommandExecute()
        {
            #region validation

            if (this.SelectedSaleDetail == null)
            {
                return;
            }

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            #endregion

            ApplicationSettings.TouchScreenMode = false;

            //this.SelectedSaleDetail.StockMode = StockMode.Serial;
            this.SelectedSaleDetail.DisplayOnly = true;
            var localSaleDetail = this.SelectedSaleDetail;
            localSaleDetail.DocketNo = this.Sale.Number;
            
            Messenger.Default.Send(ViewType.ARDispatchDetails);
            this.Locator.ARDispatchDetails.SelectedSaleDetail = localSaleDetail;
            this.Locator.ARDispatchDetails.IsReadOnly = this.IsReadOnly;
        }

        /// <summary>
        /// Drill down for manual serial entry.
        /// </summary>
        private void DrillDownManualSerialCommandExecute()
        {
            #region validation 

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPartnerSelected);
                return;
            }

            #endregion 
          
            this.DoNotHandleSelectedSaleDetail = true;
            this.SelectedSaleDetail = new SaleDetail();
            //this.SelectedSaleDetail.StockMode = StockMode.Serial;
            this.SelectedSaleDetail.DisplayOnly = false;
            this.DoNotHandleSelectedSaleDetail = false;

            Messenger.Default.Send(ViewType.ARDispatchDetails);
            Messenger.Default.Send(this.SelectedSaleDetail, Token.SaleDetailSelected); 
        }

        /// <summary>
        /// Override, to set the focus to here.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            base.UpdateCommandExecute(e);
            this.SetActiveDocument(this);

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
            {
                return;
            }

            if ((e.Cell.Property.Equals("INMasterID") || e.Cell.Property.Equals("PriceListID")) && 
                this.SelectedSaleDetail != null
                && (this.SelectedSaleDetail.WeightDelivered == null || this.SelectedSaleDetail.WeightDelivered == 0)
                && (this.SelectedSaleDetail.QuantityDelivered == null || this.SelectedSaleDetail.QuantityDelivered == 0))
            {
                this.SelectedSaleDetail.UnitPrice = null;
            }
        }

        /// <summary>
        /// Direct the F1 serach request to the appropriate handler.
        /// </summary>
        protected override void DirectSearchCommandExecute()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.CurrentMode == ControlMode.OK)
            {
                this.ShowSearchGridCommandExecute(Constant.Dispatch);
            }
            else if (this.CurrentMode == ControlMode.Find)
            {
                this.FindSaleCommandExecute();
            }
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            this.ShowSales();
        }

        /// <summary>
        /// Override, to set the selected detail flag.
        /// </summary>
        protected override void HandleSelectedSaleDetail()
        {
            base.HandleSelectedSaleDetail();

            // TODO when search for the sales, the price method is not set (no selected supplier) this works, but need to fix properly.
            this.SelectedSaleDetail.UpdateTotals = false;
            this.SelectedSaleDetail.INMasterID = this.SelectedSaleDetail.INMasterID;

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;

            if (ApplicationSettings.ShowProductRemarksAtSaleOrder)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (this.SelectedSaleDetail?.InventoryItem != null &&
                        !string.IsNullOrWhiteSpace(this.SelectedSaleDetail.InventoryItem.Remarks)
                        && !this.SelectedSaleDetail.RemarksDisplayed
                        && (this.SelectedDocStatus.NouDocStatusID == 1 || this.SelectedDocStatus.NouDocStatusID == 4))
                    {
                        this.SelectedSaleDetail.RemarksDisplayed = true;
                        var localRemarks = this.SelectedSaleDetail.InventoryItem.Remarks;
                        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                        {
                            NouvemMessageBox.Show(localRemarks, isPopUp: true);
                        }));
                    }
                }));
            }
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            this.allowAutoWeigh = true;
            base.ParsePartner();
            Messenger.Default.Send(false, Token.ShowAllRecentOrders);
            this.ShowAllRecentOrders = false;

            if (this.SelectedPartner == null)
            {
                return;
            }

            if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.ShowPartnerPopUpNoteAtDesktopModules && !this.DisablePartnerPopUp)
            {
                if (!string.IsNullOrWhiteSpace(this.CustomerPopUpNote))
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(this.CustomerPopUpNote, isPopUp: true);
                        this.CustomerPopUpNote = string.Empty;
                    }));
                }
            }

            if (ApplicationSettings.CheckPartnerForLiveOrder)
            {
                var partnerId = this.SelectedPartner.BPMasterID;
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    if (this.CurrentMode == ControlMode.Add)
                    {
                        var liveOrderNumber = this.DataManager.IsPartnerLiveOrderOnSystem(partnerId);
                        if (liveOrderNumber > 0)
                        {
                            SystemMessage.Write(MessageType.Issue, string.Format(Message.LiveOrderOnSystemWarning, liveOrderNumber));
                            NouvemMessageBox.Show(string.Format(Message.LiveOrderOnSystemWarning, liveOrderNumber), touchScreen: ApplicationSettings.TouchScreenMode);
                        }
                    }
                }));
            }

            if (this.DataManager.GetPartnerOnHoldStatus(this.SelectedPartner.BPMasterID))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.PartnerOnHold, this.SelectedPartner.Name));
                    NouvemMessageBox.Show(string.Format(Message.PartnerOnHold, this.SelectedPartner.Name), touchScreen: ApplicationSettings.TouchScreenMode);
                    var localBusinessPartner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(
                            x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
                    if (localBusinessPartner != null)
                    {
                        localBusinessPartner.Details.OnHold = true;
                    }

                    this.SelectedPartner = null;
                }));

                return;
            }
            else
            {
                var localBusinessPartner =
                   NouvemGlobal.BusinessPartners.FirstOrDefault(
                       x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
                if (localBusinessPartner != null)
                {
                    localBusinessPartner.Details.OnHold = false;
                }
            }

            if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    try
                    {
                        int? productId = null;
                        int? productGroupId = null;

                        if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null)
                        {
                            productId = this.SelectedSaleDetail.InventoryItem.Master.INMasterID;
                            productGroupId = this.SelectedSaleDetail.InventoryItem.Master.INGroupID;
                        }

                        this.LabelManager.GetLabelAndDisplay(this.SelectedPartner.BPMasterID, this.SelectedPartner.BPGroupID, productId, productGroupId, LabelType.Item);
                    }
                    catch (Exception ex)
                    {
                        this.Log.LogError(this.GetType(), ex.Message);
                    }
                }), DispatcherPriority.Background);
            }

            if (ApplicationSettings.TouchScreenMode)
            {
                //if (!string.IsNullOrWhiteSpace(this.SelectedPartner.PopUpNotes))
                //{
                //    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                //    {
                //        NouvemMessageBox.Show(this.SelectedPartner.PopUpNotes, touchScreen:true);
                //    }));
                //}

                this.CustomerPopUpNote = this.SelectedPartner.PopUpNotes;
                return;
            }

            var localPartner = this.SelectedPartner;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.CustomerSales = this.DataManager.GetRecentDispatches(localPartner.BPMasterID);           
                this.RecentOrdersCount = 0;
                this.SetRecentOrders(true);
            }));
        }

        [Obsolete]
        protected void HandleRecentOrders(int ordersToTake)
        {
            if (this.ProductRecentOrderDatas != null)
            {
                this.ProductRecentOrderDatas.Clear();
            }

            var localPartner = this.SelectedPartner;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.CustomerSales = this.DataManager.GetARDispatches(localPartner.BPMasterID, ordersToTake);
                var recentOrders = this.CustomerSales.OrderByDescending(x => x.CreationDate).ToList();

                // Get sthe sale details (used on the recent order items tab grid)
                this.CustomerSaleDetails = new ObservableCollection<SaleDetail>(recentOrders.SelectMany(x => x.SaleDetails));

                // We're binding the recent order headers to their dates.
                this.ResetOrderDates();

                if (this.Sale != null && this.Sale.SaleID == 0)
                {
                    this.SetControlMode(ControlMode.Add);
                }

                if (!recentOrders.Any())
                {
                    return;
                }

                if (recentOrders.Count > 0)
                {
                    var order = recentOrders.ElementAt(0);
                    this.Order1Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder1 = order;
                }

                if (recentOrders.Count > 1)
                {
                    var order = recentOrders.ElementAt(1);
                    this.Order2Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder2 = order;
                }

                if (recentOrders.Count > 2)
                {
                    var order = recentOrders.ElementAt(2);
                    this.Order3Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder3 = order;
                }

                if (recentOrders.Count > 3)
                {
                    var order = recentOrders.ElementAt(3);
                    this.Order4Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder4 = order;
                }

                if (recentOrders.Count > 4)
                {
                    var order = recentOrders.ElementAt(4);
                    this.Order5Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder5 = order;
                }

                if (recentOrders.Count > 5)
                {
                    var order = recentOrders.ElementAt(5);
                    this.Order6Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder6 = order;
                }

                if (recentOrders.Count > 6)
                {
                    var order = recentOrders.ElementAt(6);
                    this.Order7Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder7 = order;
                }

                if (recentOrders.Count > 7)
                {
                    var order = recentOrders.ElementAt(7);
                    this.Order8Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder8 = order;
                }

                if (recentOrders.Count > 8)
                {
                    var order = recentOrders.ElementAt(8);
                    this.Order9Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder9 = order;
                }

                if (recentOrders.Count > 9)
                {
                    var order = recentOrders.ElementAt(9);
                    this.Order10Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder10 = order;
                }

                if (recentOrders.Count > 10)
                {
                    var order = recentOrders.ElementAt(10);
                    this.Order11Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder11 = order;
                }

                if (recentOrders.Count > 11)
                {
                    var order = recentOrders.ElementAt(11);
                    this.Order12Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder12 = order;
                }

                if (recentOrders.Count > 12)
                {
                    var order = recentOrders.ElementAt(12);
                    this.Order13Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder13 = order;
                }

                if (recentOrders.Count > 13)
                {
                    var order = recentOrders.ElementAt(13);
                    this.Order14Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder14 = order;
                }

                if (recentOrders.Count > 14)
                {
                    var order = recentOrders.ElementAt(14);
                    this.Order15Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder15 = order;
                }

                if (recentOrders.Count > 15)
                {
                    var order = recentOrders.ElementAt(15);
                    this.Order16Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder16 = order;
                }

                if (recentOrders.Count > 16)
                {
                    var order = recentOrders.ElementAt(16);
                    this.Order17Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder17 = order;
                }

                if (recentOrders.Count > 17)
                {
                    var order = recentOrders.ElementAt(17);
                    this.Order18Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder18 = order;
                }

                if (recentOrders.Count > 18)
                {
                    var order = recentOrders.ElementAt(18);
                    this.Order19Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder19 = order;
                }

                if (recentOrders.Count > 19)
                {
                    var order = recentOrders.ElementAt(19);
                    this.Order20Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder20 = order;
                }

                #region overflow


                if (recentOrders.Count > 20) { var order = recentOrders.ElementAt(20); this.Order21Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder21 = order; }
                if (recentOrders.Count > 21) { var order = recentOrders.ElementAt(21); this.Order22Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder22 = order; }
                if (recentOrders.Count > 22) { var order = recentOrders.ElementAt(22); this.Order23Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder23 = order; }
                if (recentOrders.Count > 23) { var order = recentOrders.ElementAt(23); this.Order24Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder24 = order; }
                if (recentOrders.Count > 24) { var order = recentOrders.ElementAt(24); this.Order25Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder25 = order; }
                if (recentOrders.Count > 25) { var order = recentOrders.ElementAt(25); this.Order26Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder26 = order; }
                if (recentOrders.Count > 26) { var order = recentOrders.ElementAt(26); this.Order27Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder27 = order; }
                if (recentOrders.Count > 27) { var order = recentOrders.ElementAt(27); this.Order28Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder28 = order; }
                if (recentOrders.Count > 28) { var order = recentOrders.ElementAt(28); this.Order29Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder29 = order; }
                if (recentOrders.Count > 29) { var order = recentOrders.ElementAt(29); this.Order30Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder30 = order; }
                if (recentOrders.Count > 30) { var order = recentOrders.ElementAt(30); this.Order31Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder31 = order; }
                if (recentOrders.Count > 31) { var order = recentOrders.ElementAt(31); this.Order32Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder32 = order; }
                if (recentOrders.Count > 32) { var order = recentOrders.ElementAt(32); this.Order33Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder33 = order; }
                if (recentOrders.Count > 33) { var order = recentOrders.ElementAt(33); this.Order34Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder34 = order; }
                if (recentOrders.Count > 34) { var order = recentOrders.ElementAt(34); this.Order35Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder35 = order; }
                if (recentOrders.Count > 35) { var order = recentOrders.ElementAt(35); this.Order36Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder36 = order; }
                if (recentOrders.Count > 36) { var order = recentOrders.ElementAt(36); this.Order37Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder37 = order; }
                if (recentOrders.Count > 37) { var order = recentOrders.ElementAt(37); this.Order38Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder38 = order; }
                if (recentOrders.Count > 38) { var order = recentOrders.ElementAt(38); this.Order39Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder39 = order; }
                if (recentOrders.Count > 39) { var order = recentOrders.ElementAt(39); this.Order40Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder40 = order; }
                if (recentOrders.Count > 40) { var order = recentOrders.ElementAt(40); this.Order41Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder41 = order; }
                if (recentOrders.Count > 41) { var order = recentOrders.ElementAt(41); this.Order42Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder42 = order; }
                if (recentOrders.Count > 42) { var order = recentOrders.ElementAt(42); this.Order43Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder43 = order; }
                if (recentOrders.Count > 43) { var order = recentOrders.ElementAt(43); this.Order44Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder44 = order; }
                if (recentOrders.Count > 44) { var order = recentOrders.ElementAt(44); this.Order45Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder45 = order; }
                if (recentOrders.Count > 45) { var order = recentOrders.ElementAt(45); this.Order46Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder46 = order; }
                if (recentOrders.Count > 46) { var order = recentOrders.ElementAt(46); this.Order47Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder47 = order; }
                if (recentOrders.Count > 47) { var order = recentOrders.ElementAt(47); this.Order48Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder48 = order; }
                if (recentOrders.Count > 48) { var order = recentOrders.ElementAt(48); this.Order49Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder49 = order; }
                if (recentOrders.Count > 49) { var order = recentOrders.ElementAt(49); this.Order50Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder50 = order; }
                if (recentOrders.Count > 50) { var order = recentOrders.ElementAt(50); this.Order51Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder51 = order; }
                if (recentOrders.Count > 51) { var order = recentOrders.ElementAt(51); this.Order52Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder52 = order; }
                if (recentOrders.Count > 52) { var order = recentOrders.ElementAt(52); this.Order53Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder53 = order; }
                if (recentOrders.Count > 53) { var order = recentOrders.ElementAt(53); this.Order54Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder54 = order; }
                if (recentOrders.Count > 54) { var order = recentOrders.ElementAt(54); this.Order55Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder55 = order; }
                if (recentOrders.Count > 55) { var order = recentOrders.ElementAt(55); this.Order56Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder56 = order; }
                if (recentOrders.Count > 56) { var order = recentOrders.ElementAt(56); this.Order57Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder57 = order; }
                if (recentOrders.Count > 57) { var order = recentOrders.ElementAt(57); this.Order58Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder58 = order; }
                if (recentOrders.Count > 58) { var order = recentOrders.ElementAt(58); this.Order59Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder59 = order; }
                if (recentOrders.Count > 59) { var order = recentOrders.ElementAt(59); this.Order60Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder60 = order; }
                if (recentOrders.Count > 60) { var order = recentOrders.ElementAt(60); this.Order61Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder61 = order; }
                if (recentOrders.Count > 61) { var order = recentOrders.ElementAt(61); this.Order62Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder62 = order; }
                if (recentOrders.Count > 62) { var order = recentOrders.ElementAt(62); this.Order63Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder63 = order; }
                if (recentOrders.Count > 63) { var order = recentOrders.ElementAt(63); this.Order64Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder64 = order; }
                if (recentOrders.Count > 64) { var order = recentOrders.ElementAt(64); this.Order65Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder65 = order; }
                if (recentOrders.Count > 65) { var order = recentOrders.ElementAt(65); this.Order66Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder66 = order; }
                if (recentOrders.Count > 66) { var order = recentOrders.ElementAt(66); this.Order67Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder67 = order; }
                if (recentOrders.Count > 67) { var order = recentOrders.ElementAt(67); this.Order68Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder68 = order; }
                if (recentOrders.Count > 68) { var order = recentOrders.ElementAt(68); this.Order69Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder69 = order; }
                if (recentOrders.Count > 69) { var order = recentOrders.ElementAt(69); this.Order70Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder70 = order; }
                if (recentOrders.Count > 70) { var order = recentOrders.ElementAt(70); this.Order71Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder71 = order; }
                if (recentOrders.Count > 71) { var order = recentOrders.ElementAt(71); this.Order72Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder72 = order; }
                if (recentOrders.Count > 72) { var order = recentOrders.ElementAt(72); this.Order73Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder73 = order; }
                if (recentOrders.Count > 73) { var order = recentOrders.ElementAt(73); this.Order74Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder74 = order; }
                if (recentOrders.Count > 74) { var order = recentOrders.ElementAt(74); this.Order75Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder75 = order; }
                if (recentOrders.Count > 75) { var order = recentOrders.ElementAt(75); this.Order76Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder76 = order; }
                if (recentOrders.Count > 76) { var order = recentOrders.ElementAt(76); this.Order77Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder77 = order; }
                if (recentOrders.Count > 77) { var order = recentOrders.ElementAt(77); this.Order78Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder78 = order; }
                if (recentOrders.Count > 78) { var order = recentOrders.ElementAt(78); this.Order79Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder79 = order; }
                if (recentOrders.Count > 79) { var order = recentOrders.ElementAt(79); this.Order80Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder80 = order; }
                if (recentOrders.Count > 80) { var order = recentOrders.ElementAt(80); this.Order81Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder81 = order; }
                if (recentOrders.Count > 81) { var order = recentOrders.ElementAt(81); this.Order82Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder82 = order; }
                if (recentOrders.Count > 82) { var order = recentOrders.ElementAt(82); this.Order83Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder83 = order; }
                if (recentOrders.Count > 83) { var order = recentOrders.ElementAt(83); this.Order84Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder84 = order; }
                if (recentOrders.Count > 84) { var order = recentOrders.ElementAt(84); this.Order85Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder85 = order; }
                if (recentOrders.Count > 85) { var order = recentOrders.ElementAt(85); this.Order86Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder86 = order; }
                if (recentOrders.Count > 86) { var order = recentOrders.ElementAt(86); this.Order87Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder87 = order; }
                if (recentOrders.Count > 87) { var order = recentOrders.ElementAt(87); this.Order88Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder88 = order; }
                if (recentOrders.Count > 88) { var order = recentOrders.ElementAt(88); this.Order89Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder89 = order; }
                if (recentOrders.Count > 89) { var order = recentOrders.ElementAt(89); this.Order90Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder90 = order; }
                if (recentOrders.Count > 90) { var order = recentOrders.ElementAt(90); this.Order91Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder91 = order; }
                if (recentOrders.Count > 91) { var order = recentOrders.ElementAt(91); this.Order92Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder92 = order; }
                if (recentOrders.Count > 92) { var order = recentOrders.ElementAt(92); this.Order93Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder93 = order; }
                if (recentOrders.Count > 93) { var order = recentOrders.ElementAt(93); this.Order94Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder94 = order; }
                if (recentOrders.Count > 94) { var order = recentOrders.ElementAt(94); this.Order95Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder95 = order; }
                if (recentOrders.Count > 95) { var order = recentOrders.ElementAt(95); this.Order96Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder96 = order; }
                if (recentOrders.Count > 96) { var order = recentOrders.ElementAt(96); this.Order97Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder97 = order; }
                if (recentOrders.Count > 97) { var order = recentOrders.ElementAt(97); this.Order98Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder98 = order; }
                if (recentOrders.Count > 98) { var order = recentOrders.ElementAt(98); this.Order99Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder99 = order; }
                if (recentOrders.Count > 99) { var order = recentOrders.ElementAt(99); this.Order100Date = order.CreationDate.ToString(); ProductRecentOrderData.RecentOrder100 = order; }


                #endregion

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.ProductRecentOrderDatas =
                        new ObservableCollection<ProductRecentOrderData>(this.DataManager.GetRecentOrderData(recentOrders));

                    if (this.ProductRecentOrderDatas.Any())
                    {
                        // set focus to the recent orders grid.
                        Messenger.Default.Send(Token.Message, Token.SetRecentOrdersFocus);
                    }
                }));
            }));
        }

        /// <summary>
        /// Overrides the mode selection, to disable user add.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            //if (this.CurrentMode == ControlMode.Add)
            //{
            //    this.SetControlMode(ControlMode.OK);
            //}
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.AddDesktopSale();
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdateOrder();
                    Messenger.Default.Send(Token.Message, Token.RefreshSearchSales);
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        public void AddTransaction()
        {
            // ensure the price list data is set (this relates to the touchscreen only)
            if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.PriceListID == 0)
            {
                this.SelectedSaleDetail.PriceListID = this.SelectedPartner.PriceListID.ToInt();
            }

            #region validation

            var error = string.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (this.SaleDetails == null || !this.SaleDetails.Any())
            {
                //error = Message.NoSaleDetails;
            }

            if (error != string.Empty)
            {
                if (ApplicationSettings.TouchScreenMode)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(error, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(error, touchScreen: true);
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, error);
                }

                return;
            }

            #endregion

            this.CreateSale();

            if (this.Sale.SaleID == 0)
            {
                this.AddSale();
            }
            else
            {
                this.UpdateSale();
            }
        }

        /// <summary>
        /// Completes a transaction, check the orders live status.
        /// </summary>
        /// <param name="wgt">The weight to apply.</param>
        /// <param name="qty">The qty to apply.</param>
        protected void CompleteTransaction(decimal wgt, decimal qty)
        {
            if (qty <= 0)
            {
                qty = 1;
            }

            if (this.AutoBoxing && !this.SelectedSaleDetail.CompleteAutoBoxing)
            {
                wgt = 0;
            }

            if (this.SelectedSaleDetail.WeightDelivered == null)
            {
                this.SelectedSaleDetail.WeightDelivered = this.SelectedSaleDetail.WeightDelivered.ToDecimal();
            }

            if (this.SelectedSaleDetail.QuantityDelivered == null)
            {
                this.SelectedSaleDetail.QuantityDelivered = this.SelectedSaleDetail.QuantityDelivered.ToDecimal();
            }

            this.GetLiveStatus();

            if (this.Sale != null)
            {
                this.displayPopUpNote = false;
                if (this.Sale.OrderStatus == OrderStatus.Complete)
                {
                    var deviceName = this.Sale.Device != null ? this.Sale.Device.DeviceName : string.Empty;
                    this.Log.LogDebug(this.GetType(), string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName));
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName));

                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName), scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName), touchScreen: true);
                    }

                    return;
                }
            }

            if (!this.AuthorisationsManager.AllowUserToDispatchOverOrderAmounts)
            {
                var wgtOrdered = this.SelectedSaleDetail.WeightOrdered.ToDecimal();
                var qtyOrdered = this.SelectedSaleDetail.QuantityOrdered.ToDecimal();
                var wgtDel = this.SelectedSaleDetail.WeightDelivered.ToDecimal() + wgt;
                var qtyDel = this.SelectedSaleDetail.QuantityDelivered.ToDecimal() + qty;
                var overAmount = false;
                if (this.SelectedSaleDetail.NouOrderMethod == OrderMethod.Weight)
                {
                    if (wgtOrdered > 0 && wgtDel > wgtOrdered)
                    {
                        overAmount = true;
                    }
                }
                else if (this.SelectedSaleDetail.NouOrderMethod == OrderMethod.Quantity)
                {
                    if (qtyOrdered > 0 && qtyDel > qtyOrdered)
                    {
                        overAmount = true;
                    }
                }
                else
                {
                    if ((wgtOrdered > 0 && wgtDel > wgtOrdered)
                        || (qtyOrdered > 0 && qtyDel > qtyOrdered))
                    {
                        overAmount = true;
                    }
                }

                if (overAmount)
                {
                    if (ApplicationSettings.DispatchOverAmountHardStop)
                    {
                        // hard stop. user cannot proceed.
                        SystemMessage.Write(MessageType.Issue, Message.DispatchOverAmountHardStopWarning);
                        NouvemMessageBox.Show(Message.DispatchOverAmountHardStopWarning, touchScreen: true);
                        return;
                    }

                    // soft stop. user has choice.
                    if (!this.SelectedSaleDetail.DispatchOverAmountWarningGiven)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.DispatchOverOrderAmtWarning);
                        NouvemMessageBox.Show(Message.DispatchOverOrderAmtWarning, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }

                        this.SelectedSaleDetail.DispatchOverAmountWarningGiven = true;
                    }
                }
            }

            if (wgt > 0)
            {
                this.SelectedSaleDetail.WeightDelivered = Math.Round(this.SelectedSaleDetail.WeightDelivered.ToDecimal() + wgt, 5);
            }

            if (qty > 0)
            {
                this.SelectedSaleDetail.QuantityDelivered += qty;
            }

            if (this.SelectedSaleDetail.IsBox.IsTrue())
            {
                this.SelectedSaleDetail.BoxCount++;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;

            this.ProductQuantity = 0;
            this.AddTransaction();
        }

        /// <summary>
        /// Completes a transaction, check the orders live status.
        /// </summary>
        /// <param name="wgt">The weight to apply.</param>
        /// <param name="qty">The qty to apply.</param>
        protected void CompleteTransactionHistoric(decimal wgt, decimal qty)
        {
            if (qty <= 0)
            {
                qty = 1;
            }

            if (this.SelectedSaleDetail.WeightDelivered == null)
            {
                this.SelectedSaleDetail.WeightDelivered = this.SelectedSaleDetail.WeightDelivered.ToDecimal();
            }

            if (this.SelectedSaleDetail.QuantityDelivered == null)
            {
                this.SelectedSaleDetail.QuantityDelivered = this.SelectedSaleDetail.QuantityDelivered.ToDecimal();
            }

            Sale liveUpdate = null;

            //if (ApplicationSettings.GetLiveOrderStatusDispatch)
            // {
            liveUpdate = this.GetLiveOrderStatus();
            // }

            if (liveUpdate != null)
            {
                var localSelectedSaleDetail = this.SelectedSaleDetail;
                var localTemp = this.Temperature;

                this.displayPopUpNote = false;
                this.SetLiveSale(liveUpdate);
                this.SaleDetails = new ObservableCollection<SaleDetail>(this.Sale.SaleDetails);
              
                this.Temperature = localTemp;
                if (this.SaleDetails.All(x => x.INMasterID != localSelectedSaleDetail.INMasterID))
                {
                    this.SaleDetails.Add(localSelectedSaleDetail);
                }

                this.DoNotHandleSelectedSaleDetail = true;
                this.SelectedSaleDetail = this.SaleDetails.FirstOrDefault(x => x.INMasterID == localSelectedSaleDetail.INMasterID);
                this.DoNotHandleSelectedSaleDetail = false;
                this.SelectedSaleDetail.StockDetails = localSelectedSaleDetail.StockDetails;
                this.SelectedSaleDetail.StockDetailToProcess = localSelectedSaleDetail.StockDetailToProcess;
                this.SelectedSaleDetail.ProductionStockDetails = localSelectedSaleDetail.ProductionStockDetails;
                this.SelectedSaleDetail.Concessions = localSelectedSaleDetail.Concessions;
                this.SelectedSaleDetail.Batch = localSelectedSaleDetail.Batch;
                this.SelectedSaleDetail.DispatchOverAmountWarningGiven = localSelectedSaleDetail.DispatchOverAmountWarningGiven;
                this.Sale.NouDocStatusID = liveUpdate.NouDocStatusID;
                if (liveUpdate.OrderStatus == OrderStatus.Complete)
                {
                    var deviceName = liveUpdate.Device != null ? liveUpdate.Device.DeviceName : string.Empty;
                    this.Log.LogDebug(this.GetType(), string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName));
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName));

                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName), scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(string.Format(Message.OrderAlreadyCompletedAndClosed, deviceName), touchScreen: true);
                    }

                    return;
                }

                if (liveUpdate.OrderStatus == OrderStatus.InProgress || liveUpdate.OrderStatus == OrderStatus.Filled || liveUpdate.OrderStatus == OrderStatus.Active)
                {
                    this.Log.LogDebug(this.GetType(), "Order in progress");

                    if (liveUpdate.SaleDetails.Any())
                    {
                        var matchSaleDetail =
                            liveUpdate.SaleDetails.FirstOrDefault(
                                x => x.SaleDetailID == this.SelectedSaleDetail.SaleDetailID);

                        if (matchSaleDetail != null)
                        {
                            this.Log.LogDebug(this.GetType(), "Checking if received amounts have been updated at another terminal");

                            if ((matchSaleDetail.WeightDelivered.ToDouble() > this.SelectedSaleDetail.WeightDelivered.ToDouble()
                                || matchSaleDetail.QuantityDelivered.ToDouble() > this.SelectedSaleDetail.QuantityDelivered.ToDouble())
                                || (matchSaleDetail.WeightDelivered.ToDouble() < this.SelectedSaleDetail.WeightDelivered.ToDouble()
                                || matchSaleDetail.QuantityDelivered.ToDouble() < this.SelectedSaleDetail.QuantityDelivered.ToDouble()))
                            {
                                this.Log.LogDebug(this.GetType(), string.Format("Updating Qty:{0}, Wgt:{1}", matchSaleDetail.QuantityDelivered, matchSaleDetail.WeightDelivered));

                                this.SelectedSaleDetail.WeightDelivered = matchSaleDetail.WeightDelivered;
                                this.SelectedSaleDetail.QuantityDelivered = matchSaleDetail.QuantityDelivered;

                                SystemMessage.Write(MessageType.Priority, Message.OrderItemUpdatedElsewhere);
                                if (ApplicationSettings.DisplayDispatchOrderItemUpdatedAtAnotherTerminalMsgBox)
                                {
                                    if (ApplicationSettings.ScannerMode)
                                    {
                                        NouvemMessageBox.Show(Message.OrderItemUpdatedElsewhere, scanner: true);
                                    }
                                    else
                                    {
                                        NouvemMessageBox.Show(Message.OrderItemUpdatedElsewhere, touchScreen: true);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (!ApplicationSettings.AllowDispatchOverAmount)
            {
                var wgtOrdered = this.SelectedSaleDetail.WeightOrdered.ToDecimal();
                var qtyOrdered = this.SelectedSaleDetail.QuantityOrdered.ToDecimal();
                var wgtDel = this.SelectedSaleDetail.WeightDelivered.ToDecimal() + wgt;
                var qtyDel = this.SelectedSaleDetail.QuantityDelivered.ToDecimal() + qty;
                var overAmount = false;
                if (this.SelectedSaleDetail.NouOrderMethod == OrderMethod.Weight)
                {
                    if (wgtOrdered > 0 && wgtDel > wgtOrdered)
                    {
                        overAmount = true;
                    }
                }
                else if (this.SelectedSaleDetail.NouOrderMethod == OrderMethod.Quantity)
                {
                    if (qtyOrdered > 0 && qtyDel > qtyOrdered)
                    {
                        overAmount = true;
                    }
                }
                else
                {
                    if ((wgtOrdered > 0 && wgtDel > wgtOrdered)
                        || (qtyOrdered > 0 && qtyDel > qtyOrdered))
                    {
                        overAmount = true;
                    }
                }

                if (overAmount)
                {
                    if (ApplicationSettings.DispatchOverAmountHardStop)
                    {
                        // hard stop. user cannot proceed.
                        SystemMessage.Write(MessageType.Issue, Message.DispatchOverAmountHardStopWarning);
                        NouvemMessageBox.Show(Message.DispatchOverAmountHardStopWarning, touchScreen: true);
                        return;
                    }

                    // soft stop. user has choice.
                    if (!this.SelectedSaleDetail.DispatchOverAmountWarningGiven)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.DispatchOverOrderAmtWarning);
                        NouvemMessageBox.Show(Message.DispatchOverOrderAmtWarning, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }

                        this.SelectedSaleDetail.DispatchOverAmountWarningGiven = true;
                    }
                }
            }

            if (wgt > 0)
            {
                this.SelectedSaleDetail.WeightDelivered = Math.Round(this.SelectedSaleDetail.WeightDelivered.ToDecimal() + wgt, 5);
            }

            if (qty > 0)
            {
                this.SelectedSaleDetail.QuantityDelivered += qty;
            }

            if (this.SelectedSaleDetail.IsBox.IsTrue())
            {
                this.SelectedSaleDetail.BoxCount++;
            }

            foreach (var saleDetail in this.SaleDetails)
            {
                saleDetail.IsSelectedSaleDetail = false;
            }

            this.SelectedSaleDetail.IsSelectedSaleDetail = true;

            this.ProductQuantity = 0;
            this.AddTransaction();
        }

        /// <summary>
        /// Adds a sale.
        /// </summary>
        protected override void AddSale()
        {
            if (this.OrderUpdateInProgress)
            {
                return;
            }

            try
            {
                this.OrderUpdateInProgress = true;

                #region validation

                if (!this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
                {
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    this.SetControlMode(ControlMode.OK);
                    if (ApplicationSettings.TouchScreenMode)
                    {
                        if (ApplicationSettings.ScannerMode)
                        {
                            NouvemMessageBox.Show(Message.AccessDenied, scanner:true);
                        }
                        else
                        {
                            NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                        }
                    }

                    return;
                }

                var error = string.Empty;

                if (ApplicationSettings.RouteMustBeSetOnOrder && this.SelectedRoute == null)
                {
                    error = Message.SelectRoute;
                }
                else if (ApplicationSettings.CheckRoutesAtOrder && this.SelectedRoute != null)
                {
                    error = this.CheckRoutesAtOrder();
                }
                else if (ApplicationSettings.DeliveryDateCannotBeInThePast && this.DeliveryDate.HasValue && this.DeliveryDate < DateTime.Today)
                {
                    error = Message.DeliveryDateInPastError;
                }

                if (ApplicationSettings.ShippingDateMustBeAfterDeliveryDate && this.DeliveryDate.HasValue && this.ShippingDate.HasValue)
                {
                    if (this.DeliveryDate < this.ShippingDate)
                    {
                        error = Message.ShippingDateBeforeDeliveryDate;
                    }
                }

                if (ApplicationSettings.DefaultShippingDateToPreviousDay)
                {
                    if (this.ShippingDate.ToDate() < DateTime.Today)
                    {
                        error = Message.InvalidShippingDate;
                    }
                }

                if (error != string.Empty)
                {
                    SystemMessage.Write(MessageType.Issue, error);
                    NouvemMessageBox.Show(error, touchScreen:ApplicationSettings.TouchScreenMode);
                    return;
                }

                //if (this.ScannerStockMode == ScannerMode.ReWeigh)
                //{
                //    var detail = this.Sale.SaleDetails.FirstOrDefault(x => x.Process);
                //    if (detail != null)
                //    {
                //        var data = detail.GoodsReceiptDatas.FirstOrDefault();
                //        if (data != null)
                //        {
                //            var transData = data.TransactionData.FirstOrDefault();
                //            if (transData != null && transData.StockAttribute == null)
                //            {
                //                this.Log.LogDebug(this.GetType(), "Stock attribute null...retrieving");
                //                transData.StockAttribute =
                //                     this.DataManager.GetTransactionAttributesCarcassSerial(transData.Serial);
                //            }
                //        }
                //    }
                //}

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate)
                {
                    if (this.SaleDetails != null && this.SaleDetails.Any(x => x.UnitPrice.IsNullOrZero()))
                    {
                        NouvemMessageBox.Show(Message.NoPriceWarningOnOrderLines, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }

                    if (this.SaleDetails != null && this.SaleDetails.Any(x => x.PriceListID == 0))
                    {
                        NouvemMessageBox.Show(Message.NoPriceBookFoundOnOrderLine, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }
                    }
                }

                if (!this.CheckOrderLimit())
                {
                    return;
                }

                #endregion

                if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.CheckForOutstandingOrders)
                {
                    var localSales = this.DataManager.GetOpenARDispatches(this.SelectedPartner.BPMasterID);
                    Sale matchingSale = null;
                    var match = true;
                    foreach (var localSale in localSales)
                    {
                        if (localSale.SaleDetails.Count != this.Sale.SaleDetails.Count)
                        {
                            continue;
                        }

                        foreach (var localSaleDetail in localSale.SaleDetails)
                        {
                            var existingDetail =
                                this.Sale.SaleDetails.FirstOrDefault(x => x.INMasterID == localSaleDetail.INMasterID);
                            if (existingDetail == null)
                            {
                                match = false;
                                break;
                            }

                            if (existingDetail.QuantityOrdered != localSaleDetail.QuantityOrdered
                                || existingDetail.WeightOrdered != localSaleDetail.WeightOrdered)
                            {
                                match = false;
                                break;
                            }
                        }

                        if (match)
                        {
                            matchingSale = localSale;
                            break;
                        }
                    }

                    if (matchingSale != null)
                    {
                        NouvemMessageBox.Show(Message.DuplicateOrderDetected, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            this.SetActiveDocument(this);
                            IList<Sale> localSaleToSend = new List<Sale> { matchingSale };
                            Messenger.Default.Send(Tuple.Create(localSaleToSend, ViewType.ARDispatch), Token.SearchForSale);

                            if (this.Sale != null)
                            {
                                this.Locator.SalesSearchData.SetView(ViewType.ARDispatch);
                            }

                            return;
                        }
                    }
                }

                this.Log.LogWarning(this.GetType(), "6. AddSale(): adding a new dispatch docket");
                this.UpdateDocNumbering();
                this.OrderNo = 0;
                this.SetLineStatus(this.SelectedSaleDetail);
                var newSaleId = this.DataManager.AddAPDispatch(this.Sale);

                if (newSaleId > 0)
                {
                    this.CheckAttributeTransactionReset(this.SelectedSaleDetail);
                    this.LastEdit = DateTime.Now;
                    this.TransactionCount++;
                    this.OrderCount++;
                    this.UpdatePalletCount(this.Sale.ReturnedStockTransactionId);
                    this.Log.LogDebug(this.GetType(), "Docket created");

                    try
                    {
                        if (!this.ValidateData(
                            this.traceabilityDataPostTransactions,
                            this.Sale.ReturnedStockTransactionId, 0, 0, 0, 0, 0, Constant.Batch))
                        {
                            return;
                        };
                    }
                    catch (Exception e)
                    {
                        this.Log.LogError(this.GetType(), e.Message);
                    }

                    if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight && this.Sale.ReturnedStockTransactionId > 0)
                    {
                        if (this.SelectedSaleDetail != null && !this.SelectedSaleDetail.DontPrintLabel)
                        {
                            this.Print(this.Sale.ReturnedStockTransactionId);
                            this.PrintBoxLabel();
                        }
                    }
                    else if (this.PrintLabel && newSaleId > 0)
                    {
                        this.Print(this.Sale.ReturnedStockTransactionId);
                    }

                    this.OrderNo = this.Sale.Number;
                    this.Sale.SaleID = newSaleId;
                    //this.AllSales.Add(this.Sale);
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.DispatchDocketCreated, this.NextNumber));
                    this.RefreshGoodsIn();
                    //SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                    Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);

                    // send message the the order screen (if copying order to dispatch) of successful goods receipt addition
                    //Messenger.Default.Send(Token.Message, Token.CopySuccessful);

                    if (this.CurrentTelesale != null)
                    {
                        this.Close();
                        Messenger.Default.Send(Tuple.Create(this.CurrentTelesale,this.OrderNo), Token.SaleSelected);
                    }

                    if (ApplicationSettings.TouchScreenMode)
                    {
                        // this.SelectedSaleDetail = null;
                        var localDetail = this.SelectedSaleDetail;
                        //this.SelectedSaleDetail = null;
                        this.clearTraceability = false;
                        this.Locator.Touchscreen.UIText = this.Sale.Number.ToString();

                        if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                        {
                            if (this.scanningLabelForBatchNo)
                            {
                                // clear the batch data (we're in scan multivac label mode)
                                this.ClearBatchData();
                            }

                            this.highlightSelectedProduct = true;
                            this.SelectedSaleDetail = localDetail;
                            //this.SetLineStatus(this.SelectedSaleDetail);
                            this.clearTraceability = true;
                        }
                        else
                        {
                            if (this.ScannerStockMode == ScannerMode.ReWeigh &&
                                ApplicationSettings.SelectingProductInReweighMode)
                            {
                                this.DoNotHandleSelectedSaleDetail = true;
                                this.SelectedSaleDetail = localDetail;
                                this.DoNotHandleSelectedSaleDetail = false;
                            }
                        }
                    }
                    else
                    {
                        this.ClearForm();
                    }

                    if (this.applyTares)
                    {
                        // applying tares from the calculator
                        this.applyTares = false;
                        Messenger.Default.Send(Token.Message, Token.AddTare);
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DispatchDocketNotCreated);
                }
            }
            finally
            {
                this.OrderUpdateInProgress = false;
                this.SetOrderLinesStatus();
                this.SetWeightData();
            }
        }

        /// <summary>
        /// Print the label.
        /// </summary>
        /// <param name="stockTransactionId">The stock transaction id to pass in.</param>
        /// <param name="labelType">The label type.</param>
        protected void Print(int stockTransactionId, LabelType labelType = LabelType.Item)
        {
            int? productId = null;
            int? productGroupId = null;
            int? customerId = this.SelectedPartner.BPMasterID;
            int? customerGroupId = this.SelectedPartner.BPGroupID;

            if (labelType != LabelType.Shipping && this.SelectedSaleDetail.InventoryItem != null &&
                this.SelectedSaleDetail.InventoryItem.Master != null)
            {
                productId = this.SelectedSaleDetail.InventoryItem.Master.INMasterID;
                productGroupId = this.SelectedSaleDetail.InventoryItem.Master.INGroupID;
            }

            try
            {
                var labelsToPrint = this.Locator.Touchscreen.LabelsToPrint < 1
                    ? 1 : this.Locator.Touchscreen.LabelsToPrint;
                var labels = this.PrintManager.ProcessPrinting(customerId, customerGroupId, productId, productGroupId,
                    ViewType.ARDispatch, labelType, stockTransactionId, labelsToPrint);

                if (labels.Item1.Any())
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        this.DataManager.AddLabelToTransaction(stockTransactionId,
                            string.Join(",", labels.Item1.Select(x => x)), labels.Item2, labels.Item3);
                    }), DispatcherPriority.Background);
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals(Message.NoLabelFound))
                {
                    SystemMessage.Write(MessageType.Priority, ex.Message);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                this.Log.LogError(this.GetType(), ex.Message);
            }
            finally
            {
                this.Locator.Touchscreen.LabelsToPrint = 1;
            }
        }

        protected void SetWeightData(bool setCount = false)
        {
            if (this.Sale == null || this.SaleDetails == null || !this.SaleDetails.Any())
            {
                this.BoxCount = 0;
                this.TotalDocketWeight = 0;
                return;
            }

            this.BoxCount = this.SaleDetails.Sum(x => x.BoxCount);
            this.TotalDocketWeight = Math.Round(this.SaleDetails.Sum(x => x.WeightDelivered.ToDecimal()), ApplicationSettings.TouchscreenWgtMaskNo);

            if (setCount)
            {
                this.OrderCount = this.SaleDetails.Sum(x => x.TransactionCount);
            }
        }

        /// <summary>
        /// Cancel the current order.
        /// </summary>
        protected void CancelOrder()
        {
            #region validation

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            #endregion

            if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusCancelled.NouDocStatusID))
            {
                this.LastEdit = DateTime.Now;
                SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCancelled, this.Sale.Number));
                this.ClearForm();
            }
            else
            {
                SystemMessage.Write(MessageType.Issue, Message.OrderNotCancelled);
            }
        }

        /// <summary>
        /// Completes the current order.
        /// </summary>
        protected void CompleteOrder(bool updatingFromDesktop = false)
        {
            if (this.DisableProcessing)
            {
                return;
            }

            try
            {
                this.DisableProcessing = true;
                this.Log.LogInfo(this.GetType(), "CompleteOrder()...Attempting to complete the order");
                this.displayPopUpNote = false;

                if (!this.AuthorisationsManager.AllowUserToCompleteDispatches || !this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
                {
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    if (ApplicationSettings.ScannerMode)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            // refresh the dispatch continer ecreen if open
                            NouvemMessageBox.Show(Message.AccessDenied, scanner: true);
                        });
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(Message.AccessDenied, touchScreen: ApplicationSettings.TouchScreenMode);
                        });
                    }

                    return;
                }

                if (!updatingFromDesktop)
                {
                    //var localSale = this.GetLiveOrderStatus();
                    //if (localSale == null)
                    //{
                    //    return;
                    //}

                    //this.Sale = localSale;
                    this.GetLiveStatus();
                    this.displayPopUpNote = true;
                }

                #region validation

                SaleDetail missingTransactionSaleDetail = null;
                var error = string.Empty;

                if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        error = Message.NoOrderHasBeenSelected;
                    }
                    else
                    {
                        error = Message.NoPartnerSelected;
                    }
                }
                else if (this.SaleDetails == null || !this.SaleDetails.Any())
                {
                    error = Message.EmptyOrder;
                }

                if (error != string.Empty)
                {
                    if (ApplicationSettings.TouchScreenMode)
                    {
                        SystemMessage.Write(MessageType.Issue, error);

                        if (ApplicationSettings.ScannerMode)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                NouvemMessageBox.Show(error, scanner: true);
                            });
                        }
                        else
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                NouvemMessageBox.Show(error, touchScreen: true);
                            });
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, error);
                    }

                    return;
                }

                try
                {
                    if (!this.ValidateData(
                        this.traceabilityDataTransactions,
                        this.Sale.SaleID,
                        0, 0, 0, 0, this.Locator.ProcessSelection.SelectedProcess.ProcessID,
                        Constant.Dispatch))
                    {
                        return;
                    };
                }
                catch (Exception e)
                {
                    this.Log.LogError(this.GetType(), e.Message);
                }

                #endregion

                #region empty order

                if (ApplicationSettings.TouchScreenMode && this.SaleDetails != null)
                {
                    if (this.SaleDetails.All(x => x.IsDispatchLineEmpty))
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(Message.DispatchOrderEmptyWarning, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        });
                   
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusCancelled.NouDocStatusID))
                            {
                                this.ClearForm();
                                this.PartnerName = Strings.SelectCustomer;
                                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                                this.RefreshDocStatusItems();
                                Messenger.Default.Send(Token.Message, Token.ClearData);
                            }
                            else
                            {
                                SystemMessage.Write(MessageType.Priority, Message.OrderNotCancelled);
                            }
                        }

                        return;
                    }
                }

                #endregion

                #region order completed check

                var status = this.Sale.NouDocStatusID;
                if (status.HasValue && status == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Priority, Message.DispatchDocketCompletedAtAnotherTerminal);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        NouvemMessageBox.Show(Message.DispatchDocketCompletedAtAnotherTerminal, touchScreen: true);
                    });
              
                    this.OrderCompletionCleanUp();
                    return;
                }

                #endregion

                var displayStandardCompletionMessage = true;

                if (ApplicationSettings.CheckForPartBoxesAtDispatch)
                {
                    var halfBoxes = this.DataManager.CheckBoxTransactions(this.SaleDetails);
                    if (halfBoxes.Any())
                    {
                        var message = halfBoxes.Count == 1
                            ? Message.PartBoxOrderCompletionVerification
                            : Message.PartBoxesOrderCompletionVerification;

                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        });
               
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            foreach (var item in halfBoxes)
                            {
                                var trans = new StockTransaction
                                {
                                    INMasterID = item.INMasterID,
                                    MasterTableID = item.SaleDetailID,
                                    WarehouseID = NouvemGlobal.DispatchId,
                                    UserMasterID = NouvemGlobal.UserId,
                                    DeviceMasterID = NouvemGlobal.DeviceId,
                                    TransactionDate = DateTime.Now,
                                    IsBox = true,
                                    NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId
                                };

                                try
                                {
                                    this.DoNotHandleSelectedSaleDetail = true;
                                    this.SelectedSaleDetail = item;
                                    this.DoNotHandleSelectedSaleDetail = false;
                                    var transactionIds = this.DataManager.AddBoxTransactionOnCompleteOrder(trans, -1);
                                    if (transactionIds.Any())
                                    {
                                        foreach (var transactionId in transactionIds)
                                        {
                                            this.Print(transactionId, LabelType.Box);
                                            SystemMessage.Write(MessageType.Priority, Message.BoxLabelPrinted);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    this.Log.LogError(this.GetType(), ex.Message);
                                    SystemMessage.Write(MessageType.Issue, ex.Message);
                                }
                            }
                        }
                    }
                }

                var touchscreen = ApplicationSettings.TouchScreenMode && !ApplicationSettings.ScannerMode;
                if (ApplicationSettings.DispatchOrderNotFilledWarning)
                {
                    if (!this.IsOrderFilled())
                    {
                        if (ApplicationSettings.UnfilledDispatchCompletionAuthorisationCheck)
                        {
                            // need to check if user is authorised to complete unfilled orders.
                            var isAuthorised = this.AuthorisationsManager.AllowUserToCompleteAnUnfilledOrder;

                            if (!isAuthorised)
                            {
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    NouvemMessageBox.Show(Message.UnfilledOrderCompletionAuthorisationRequired, scanner: ApplicationSettings.ScannerMode, touchScreen: touchscreen);
                                });
                                
                                //Messenger.Default.Send(Tuple.Create(ViewType.KeyboardPassword, ViewType.ARDispatchAuthorisation));
                                this.Locator.LoginUser.Authorisation = Authorisation.AllowUserToCompleteAnUnfilledOrder;
                                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenUsers);
                                return;
                            }
                        }

                        displayStandardCompletionMessage = false;
                        this.Log.LogInfo(this.GetType(), "Unfilled order ..before message box");
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(Message.UnfilledOrderWarning, NouvemMessageBoxButtons.OKCancelBackOrder, scanner: ApplicationSettings.ScannerMode, touchScreen: touchscreen);
                        });
                        
                        this.Log.LogInfo(this.GetType(), "Unfilled order ..after message box");
                        if (NouvemMessageBox.UserSelection == UserDialogue.Cancel)
                        {
                            return;
                        }

                        if (NouvemMessageBox.UserSelection == UserDialogue.BackOrder)
                        {
                            var number = this.DataManager.CreateBackOrder(this.Sale.SaleID);
                            if (number > 0)
                            {
                                NouvemMessageBox.Show(string.Format(Message.BackOrderCreated,number),touchScreen:true);
                            }
                        }
                    }
                }

                if (displayStandardCompletionMessage)
                {
                    this.Log.LogInfo(this.GetType(), "Complete order ..before message box");
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        NouvemMessageBox.Show(Message.VerifyOrderCompletion, NouvemMessageBoxButtons.OKCancel, scanner: ApplicationSettings.ScannerMode, touchScreen: touchscreen);
                    });
                  
                    this.Log.LogInfo(this.GetType(), "Unfilled order ..after message box");
                    if (NouvemMessageBox.UserSelection != UserDialogue.OK)
                    {
                        return;
                    }
                }

                ProgressBar.SetUp(0,3);
                ProgressBar.Run();

                //this.CompleteOrderCheck();
                this.Sale.CustomerPOReference = this.CustomerPOReference;
                this.Sale.DocketNote = this.DocketNote;
                this.Log.LogInfo(this.GetType(), string.Format("Attempting to change the nou doc status id to:{0}", NouvemGlobal.NouDocStatusComplete.NouDocStatusID));
                var docStatus = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                if (ApplicationSettings.MarkCompletedDispatchesAsActiveEdit && ApplicationSettings.TouchScreenMode)
                {
                    docStatus = NouvemGlobal.NouDocStatusActiveEdit.NouDocStatusID;
                }

                if (this.DataManager.ChangeARDispatchStatus(this.Sale, docStatus))
                {
                    if (this.Sale.OrderAlreadyCompleted)
                    {
                        SystemMessage.Write(MessageType.Priority, Message.DispatchDocketCompletedAtAnotherTerminal);
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(Message.DispatchDocketCompletedAtAnotherTerminal, touchScreen: true);
                        });
                      
                        this.OrderCompletionCleanUp();
                        return;
                    }

                    this.LastEdit = DateTime.Now;
                    var docketId = this.Sale.SaleID;
                    var partnerId = this.Sale.BPMasterID;
                    this.Log.LogInfo(this.GetType(), string.Format("Order {0} has been successfully completed. AutoCopyToInvoive:{1}, AutoCreateInvoiceOnOrderCompletion:{2} ", docketId, this.AutoCopyToInvoice, ApplicationSettings.AutoCreateInvoiceOnOrderCompletion));
                    if ((this.AutoCopyToInvoice || ApplicationSettings.AutoCreateInvoiceOnOrderCompletion) && docStatus == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                    {
                        ProgressBar.Run();
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            this.Log.LogInfo(this.GetType(), "CompleteOrder(): Auto copying to an invoice");
                            try
                            {
                                this.AutoCopyDispatchToInvoice();
                            }
                            finally
                            {
                                this.OrderCompletionCleanUp();
                            }
                        });
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.Sale.Number));
                        if (!ApplicationSettings.IgnoreCompleteDispatchCheck)
                        {
                            this.DataManager.PriceDispatchDocket(docketId);
                        }

                        this.DataManager.AddDeliveryCharges(docketId);
                    }
           
                    this.OrderCompletionCleanUp();

                    if (ApplicationSettings.PrintReportOnCompletion && !ApplicationSettings.ScannerMode)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            NouvemMessageBox.Show(Message.PrintDocketPrompt, NouvemMessageBoxButtons.YesNo, touchScreen: true, showCounter: true);
                        });
           
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (ApplicationSettings.AutoPrintDispatchReport)
                            {
                                this.PrintReport(docketId);
                            }

                            if (ApplicationSettings.AutoPrintDispatchDetailReport)
                            {
                                this.PrintDispatchDetailReport(docketId);
                            }

                            if (ApplicationSettings.AutoPrintInvoiceReport)
                            {
                                if (this.Locator.Invoice.CurrentInvoiceId > 0)
                                {
                                    var id = this.Locator.Invoice.CurrentInvoiceId;
                                    this.PrintInvoiceReport(id);
                                }
                            }
                        }
                    }

                    this.ReportManager.EmailModuleReports(Properties.Module.Dispatch, partnerId.ToInt(), docketId.ToString());
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, Message.OrderNotCompleted);
                }
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                ProgressBar.Reset();
                this.DisableProcessing = false;
                this.OrderCount = 0;
            }
        }

        /// <summary>
        /// Completes the current order.
        /// </summary>
        protected void CompleteMultiOrder(bool printReport)
        {
            this.Log.LogDebug(this.GetType(), "CompleteOrder()...Attempting to complete the order");
            this.displayPopUpNote = false;

            if (!this.AuthorisationsManager.AllowUserToCompleteDispatches)
            {
                SystemMessage.Write(MessageType.Issue, Message.CompleteDispatchAuthorisationIssue);
                if (ApplicationSettings.ScannerMode)
                {
                    NouvemMessageBox.Show(Message.CompleteDispatchAuthorisationIssue, scanner: true);
                }
                else
                {
                    NouvemMessageBox.Show(Message.CompleteDispatchAuthorisationIssue, touchScreen: ApplicationSettings.TouchScreenMode);
                }

                return;
            }

            this.displayPopUpNote = true;

            if (this.SaleDetails.Any(x => x.GoodsReceiptDatas == null || !x.GoodsReceiptDatas.Any()))
            {
                this.DataManager.GetDispatchTransactionData(this.Sale.SaleDetails);
            }

            try
            {
                #region validation

                var error = string.Empty;
                if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        error = Message.NoOrderHasBeenSelected;
                    }
                    else
                    {
                        error = Message.NoPartnerSelected;
                    }
                }
                else if (this.SaleDetails == null || !this.SaleDetails.Any())
                {
                    error = Message.EmptyOrder;
                }

                if (error != string.Empty)
                {
                    if (ApplicationSettings.TouchScreenMode)
                    {
                        SystemMessage.Write(MessageType.Issue, error);

                        if (ApplicationSettings.ScannerMode)
                        {
                            NouvemMessageBox.Show(error, scanner: true);
                        }
                        else
                        {
                            NouvemMessageBox.Show(error, touchScreen: true);
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, error);
                    }

                    return;
                }

                #endregion

                #region empty order

                if (ApplicationSettings.TouchScreenMode && this.SaleDetails != null)
                {
                    if (this.SaleDetails.All(x => x.IsDispatchLineEmpty))
                    {
                        NouvemMessageBox.Show(Message.DispatchOrderEmptyWarning, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusCancelled.NouDocStatusID))
                            {
                                this.ClearForm();
                                this.PartnerName = Strings.SelectCustomer;
                                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                                this.RefreshDocStatusItems();
                                Messenger.Default.Send(Token.Message, Token.ClearData);
                            }
                            else
                            {
                                SystemMessage.Write(MessageType.Priority, Message.OrderNotCancelled);
                            }
                        }

                        return;
                    }
                }

                #endregion

                if (ApplicationSettings.CheckForPartBoxesAtDispatch)
                {
                    var halfBoxes = this.DataManager.CheckBoxTransactions(this.SaleDetails);
                    if (halfBoxes.Any())
                    {
                        var message = halfBoxes.Count == 1
                            ? Message.PartBoxOrderCompletionVerification
                            : Message.PartBoxesOrderCompletionVerification;

                        NouvemMessageBox.Show(message, NouvemMessageBoxButtons.YesNo, touchScreen: true);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            foreach (var item in halfBoxes)
                            {
                                var trans = new StockTransaction
                                {
                                    INMasterID = item.INMasterID,
                                    MasterTableID = item.SaleDetailID,
                                    WarehouseID = NouvemGlobal.DispatchId,
                                    UserMasterID = NouvemGlobal.UserId,
                                    DeviceMasterID = NouvemGlobal.DeviceId,
                                    TransactionDate = DateTime.Now,
                                    IsBox = true,
                                    NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId
                                };

                                try
                                {
                                    this.DoNotHandleSelectedSaleDetail = true;
                                    this.SelectedSaleDetail = item;
                                    this.DoNotHandleSelectedSaleDetail = false;
                                    var transactionIds = this.DataManager.AddBoxTransactionOnCompleteOrder(trans, -1);
                                    if (transactionIds.Any())
                                    {
                                        foreach (var transactionId in transactionIds)
                                        {
                                            this.Print(transactionId, LabelType.Box);
                                            SystemMessage.Write(MessageType.Priority, Message.BoxLabelPrinted);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    this.Log.LogError(this.GetType(), ex.Message);
                                    SystemMessage.Write(MessageType.Issue, ex.Message);
                                }
                            }
                        }
                    }
                }

                var touchscreen = ApplicationSettings.TouchScreenMode && !ApplicationSettings.ScannerMode;
                if (ApplicationSettings.DispatchOrderNotFilledWarning)
                {
                    if (!this.IsOrderFilled())
                    {
                        if (ApplicationSettings.UnfilledDispatchCompletionAuthorisationCheck)
                        {
                            // need to check if user is authorised to complete unfilled orders.
                            var isAuthorised = this.AuthorisationsManager.AllowUserToCompleteAnUnfilledOrder;

                            if (!isAuthorised)
                            {
                                NouvemMessageBox.Show(Message.UnfilledOrderCompletionAuthorisationRequired, scanner: ApplicationSettings.ScannerMode, touchScreen: touchscreen);
                                //Messenger.Default.Send(Tuple.Create(ViewType.KeyboardPassword, ViewType.ARDispatchAuthorisation));
                                this.Locator.LoginUser.Authorisation = Authorisation.AllowUserToCompleteAnUnfilledOrder;
                                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenUsers);
                                return;
                            }
                        }
                      
                        NouvemMessageBox.Show(Message.UnfilledOrderWarning, NouvemMessageBoxButtons.OKCancel, scanner: ApplicationSettings.ScannerMode, touchScreen: touchscreen);
                        if (NouvemMessageBox.UserSelection != UserDialogue.OK)
                        {
                            return;
                        }
                    }
                }
              
                ProgressBar.SetUp(0, 3);
                ProgressBar.Run();
         
                this.Sale.CustomerPOReference = this.CustomerPOReference;
                this.Sale.DocketNote = this.DocketNote;
                if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusComplete.NouDocStatusID))
                {
                    this.LastEdit = DateTime.Now;
                    var docketId = this.Sale.SaleID;
                    this.Log.LogDebug(this.GetType(), string.Format("Order {0} has been successfully completed. AutoCopyToInvoive:{1}, AutoCreateInvoiceOnOrderCompletion:{2} ", docketId, this.AutoCopyToInvoice, ApplicationSettings.AutoCreateInvoiceOnOrderCompletion));
                    if (this.AutoCopyToInvoice || ApplicationSettings.AutoCreateInvoiceOnOrderCompletion)
                    {
                        ProgressBar.Run();
                        this.Log.LogDebug(this.GetType(), "CompleteOrder(): Auto copying to an invoice");
                        this.AutoCopyDispatchToInvoice();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.Sale.Number));
                        if (!ApplicationSettings.IgnoreCompleteDispatchCheck)
                        {
                            this.DataManager.PriceDispatchDocket(docketId);
                        }
                    }

                    this.ClearForm();
                    this.SelectedDispatchContainer = null;
                    this.PartnerName = Strings.SelectCustomer;
                    this.NextNumber = this.SelectedDocNumbering.NextNumber;
                    this.RefreshDocStatusItems();
                    Messenger.Default.Send(Token.Message, Token.ClearAndResetData);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        // refresh the dispatch continer ecreen if open
                        Messenger.Default.Send(Token.Message, Token.RefreshDispatchContainers);
                    }));

                    if (printReport)
                    {
                        if (ApplicationSettings.AutoPrintDispatchReport)
                        {
                            this.PrintReport(docketId);
                        }

                        if (ApplicationSettings.AutoPrintDispatchDetailReport)
                        {
                            this.PrintDispatchDetailReport(docketId);
                        }

                        if (ApplicationSettings.AutoPrintInvoiceReport)
                        {
                            if (this.Locator.Invoice.CurrentInvoiceId > 0)
                            {
                                var id = this.Locator.Invoice.CurrentInvoiceId;
                                this.PrintInvoiceReport(id);
                            }
                        }
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, Message.OrderNotCompleted);
                }
            }
            finally
            {
                Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Handles the base documents(s) recollection.
        /// </summary>
        /// <param name="command">The view type (direct base document or document trail).</param>
        protected override void BaseDocumentsCommandExecute(string command)
        {
            #region validation

            if (this.Sale == null)
            {
                return;
            }

            #endregion

            if (command.Equals(Constant.Base))
            {
                if (this.Sale.BaseDocumentReferenceID.IsNullOrZero())
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoBaseDocument);
                    return;
                }

                this.ShowBaseDocument();
                return;
            }

            this.ShowDocumentTrail();
        }

        /// <summary>
        /// Finds a sale quote.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Updates a sale.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                this.SetControlMode(ControlMode.OK);
                if (ApplicationSettings.TouchScreenMode)
                {
                    if (ApplicationSettings.ScannerMode)
                    {
                        NouvemMessageBox.Show(Message.AccessDenied, scanner: true);
                    }
                    else
                    {
                        NouvemMessageBox.Show(Message.AccessDenied, touchScreen: true);
                    }
                }

                return;
            }

            string error = String.Empty;

            if (this.SelectedPartner == null || this.SelectedPartner.BPMasterID == 0)
            {
                error = Message.NoCustomerSelected;
            }
            else if (this.SelectedDocNumbering == null || this.SelectedDocNumbering.DocumentNumberingID == 0)
            {
                error = Message.NoDocNumberingSelected;
            }
            else if (this.SelectedDocStatus == null || this.SelectedDocStatus.NouDocStatusID == 0)
            {
                error = Message.NoDocStatusSelected;
            }
            else if (ApplicationSettings.DeliveryDateCannotBeInThePast && this.DeliveryDate.HasValue && this.DeliveryDate < DateTime.Today)
            {
                error = Message.DeliveryDateInPastError;
            }
            else if (!this.CancellingCurrentSale && this.SaleDetails == null || !this.SaleDetails.Any())
            {
                error = Message.NoSaleDetails;
            }
            else if (ApplicationSettings.ShippingDateMustBeAfterDeliveryDate && this.DeliveryDate.HasValue && this.ShippingDate.HasValue)
            {
                if (this.DeliveryDate < this.ShippingDate)
                {
                    error = Message.ShippingDateBeforeDeliveryDate;
                }
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                NouvemMessageBox.Show(error, touchScreen: ApplicationSettings.TouchScreenMode);
                return;
            }

            //if (this.ScannerStockMode == ScannerMode.ReWeigh)
            //{
            //    var detail = this.Sale.SaleDetails.FirstOrDefault(x => x.Process);
            //    if (detail != null)
            //    {
            //        var data = detail.GoodsReceiptDatas.FirstOrDefault();
            //        if (data != null)
            //        {
            //            var transData = data.TransactionData.FirstOrDefault();
            //            if (transData != null && transData.StockAttribute == null)
            //            {
            //                this.Log.LogDebug(this.GetType(), "Stock attribute null...retrieving");
            //                transData.StockAttribute =
            //                    this.DataManager.GetTransactionAttributesCarcassSerial(transData.Serial);
            //            }
            //        }
            //    }
            //}

            if (!ApplicationSettings.TouchScreenMode && ApplicationSettings.NoPriceEnteredOnSaleOrderLineWarningAtAddUpdate)
            {
                if (this.SaleDetails != null && this.SaleDetails.Any(x => x.UnitPrice.IsNullOrZero()))
                {
                    NouvemMessageBox.Show(Message.NoPriceWarningOnOrderLines, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }
                }

                if (this.SaleDetails != null && this.SaleDetails.Any(x => x.PriceListID == 0))
                {
                    NouvemMessageBox.Show(Message.NoPriceBookFoundOnOrderLine, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }
                }
            }

            #endregion

            try
            {
                this.SetLineStatus(this.SelectedSaleDetail);
                if (this.DataManager.UpdateARDispatch(this.Sale))
                {
                    this.CheckAttributeTransactionReset(this.SelectedSaleDetail);
                    this.LastEdit = DateTime.Now;
                    this.TransactionCount++;
                    this.OrderCount++;
                    this.UpdatePalletCount(this.Sale.ReturnedStockTransactionId);

                    try
                    {
                        if (!this.ValidateData(
                            this.traceabilityDataPostTransactions,
                            this.Sale.ReturnedStockTransactionId, 0, 0, 0, 0, 0, Constant.Dispatch))
                        {
                            return;
                        };
                    }
                    catch (Exception e)
                    {
                        this.Log.LogError(this.GetType(), e.Message);
                    }

                    if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight &&
                        this.Sale.ReturnedStockTransactionId > 0)
                    {
                        if (this.SelectedSaleDetail != null && !this.SelectedSaleDetail.DontPrintLabel)
                        {
                            this.Print(this.Sale.ReturnedStockTransactionId);
                            this.PrintBoxLabel();
                        }
                    }
                    else if (this.PrintLabel && this.Sale.ReturnedStockTransactionId > 0)
                    {
                        this.Print(this.Sale.ReturnedStockTransactionId);
                    }

                    SystemMessage.Write(MessageType.Priority, Message.TransactionRecorded);
                    Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);
                    Messenger.Default.Send(Token.Message, Token.ResetSerialData);

                    if (ApplicationSettings.TouchScreenMode)
                    {
                        // this.SelectedSaleDetail = null;
                        var localDetail = this.SelectedSaleDetail;
                        //this.SelectedSaleDetail = null;

                        if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                        {
                            if (this.scanningLabelForBatchNo)
                            {
                                // clear the batch data (we're in scan multivac label mode)
                                this.ClearBatchData();
                            }

                            this.highlightSelectedProduct = true;
                            this.clearTraceability = false;
                            this.SelectedSaleDetail = localDetail;
                            //this.SetLineStatus(this.SelectedSaleDetail);
                            this.clearTraceability = true;
                        }
                        else if (this.ScannerStockMode == ScannerMode.ReWeigh &&
                                ApplicationSettings.SelectingProductInReweighMode)
                        {
                            this.DoNotHandleSelectedSaleDetail = true;
                            this.SelectedSaleDetail = localDetail;
                            this.DoNotHandleSelectedSaleDetail = false;
                        }
                        else
                        {
                            //this.SetLineStatus(localDetail);
                        }
                    }

                    if (this.applyTares)
                    {
                        // applying tares from the calculator
                        this.applyTares = false;
                        Messenger.Default.Send(Token.Message, Token.AddTare);
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, String.Format(Message.GoodsInNotCreated, this.NextNumber));
                }

                this.uncommittedPriceChange = false;
                this.SetControlMode(ControlMode.Add);
            }
            finally
            {
                this.SetOrderLinesStatus();
                this.SetWeightData();
            }
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings = new ObservableCollection<DocNumber>(this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.ARDelivery)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(ApplicationSettings.DispatchDocketNumberType));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        protected override void AllowWindowToClose()
        {
            this.CanCloseForm = true;
            Messenger.Default.Send(true, Token.CanCloseDispatchWindow);
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            if (!this.CanCloseForm)
            {
                this.CheckForUnsavedData();
                return;
            }

            ViewModelLocator.ClearARDispatch();

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new Action(() =>
            {
                Messenger.Default.Send(Token.Message, Token.CloseDispatchWindow);
                Messenger.Default.Send(Token.Message, Token.SetActiveDocument);
                Messenger.Default.Unregister(this);
            }));           
        }

        /// <summary>
        /// Copy to the selected document.
        /// </summary>
        protected override void CopyDocumentTo()
        {
            #region validation

            if (!this.IsActiveDocument())
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoQuoteSelected);
                return;
            }

            #endregion

            if (this.SelectedDocumentCopyTo.Equals(Strings.Invoice))
            {
                CopyingDocument = true;
                AutoCopyDispatchToInvoice();
            }
        }

        /// <summary>
        /// Copy from the selected document.
        /// </summary>
        protected override void CopyDocumentFrom()
        {
            #region validation

            if (this.SelectedPartner == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoCustomerSelected);
                return;
            }

            #endregion

            var documents = this.DataManager.GetOrders(this.SelectedPartner.BPMasterID);

            if (!documents.Any())
            {
                SystemMessage.Write(MessageType.Issue, string.Format(Message.NoSalesFound, Strings.Orders));
                this.ClearForm(false);
                return;
            }

            this.SetActiveDocument(this);

            Messenger.Default.Send(Tuple.Create(documents, ViewType.ARDispatch), Token.SearchForSale);
            CopyingDocument = true;
            this.LockDiscountPercentage = true;
        }

        #endregion

        #region virtual

        /// <summary>
        /// Handles the incoming scanner data.
        /// </summary>
        /// <param name="data">The incoming data.</param>
        protected override void HandleScannerData(string data)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Desktop: HandleScannerData(): Data:{0}, Data length:{1}", data, data.Length));

            #region validation

            if (string.IsNullOrEmpty(data))
            {
                SystemMessage.Write(MessageType.Issue, Message.InvalidScannerRead);
                return;
            }

            #endregion

            try
            {
                this.DrillDownCommandExecute();
                if (this.SelectedSaleDetail != null)
                {
                  
                }
            }
            finally
            {
              
            }
        }

        /// <summary>
        /// Updates the pallet item count.
        /// </summary>
        protected virtual void UpdatePalletCount(int id)
        {
        }

        #endregion

        #region method

        /// <summary>
        /// Handles the update request.
        /// </summary>
        protected void UpdateOrder()
        {
            if (this.IsBaseDocument)
            {
                this.SetControlMode(ControlMode.OK);
                return;
            }

            if (this.OrderUpdateInProgress)
            {
                return;
            }

            if (!this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                this.SetControlMode(ControlMode.OK);
                return;
            }

            this.OrderUpdateInProgress = true;

            try
            {
                this.Sale.BPMasterID = this.SelectedPartner.BPMasterID;
                this.Sale.VAT = this.Tax;
                this.Sale.SubTotalExVAT = this.SubTotal;
                this.Sale.DiscountIncVAT = this.Discount;
                this.Sale.GrandTotalIncVAT = this.Total;
                this.Sale.DiscountPercentage = this.DiscountPercentage;
                this.Sale.Remarks = this.Remarks;
                this.Sale.CustomerPOReference = this.CustomerPOReference;
                this.Sale.DeliveryDate = this.DeliveryDate;
                this.Sale.ShippingDate = this.ShippingDate;
                this.Sale.DocumentDate = this.DocumentDate;
                this.Sale.DocketNote = this.DocketNote;
                this.Sale.InvoiceNote = this.InvoiceNote;
                this.Sale.PopUpNote = this.PopUpNote;
                this.Sale.AgentID = this.SelectedAgent?.BPMasterID;
                if (this.SelectedRoute != null)
                {
                    this.Sale.RouteID = this.SelectedRoute.RouteID;
                }

                if (this.SelectedDeliveryAddress != null)
                {
                    this.Sale.DeliveryContact = this.SelectedDeliveryContact;
                }

                if (this.SelectedContact != null)
                {
                    this.Sale.MainContact = this.SelectedContact;
                }

                if (this.SelectedDeliveryAddress != null)
                {
                    this.Sale.DeliveryAddress = this.SelectedDeliveryAddress;
                }

                if (this.SelectedInvoiceAddress != null)
                {
                    this.Sale.InvoiceAddress = this.SelectedInvoiceAddress;
                }

                if (this.SelectedHaulier != null)
                {
                    this.Sale.Haulier = this.SelectedHaulier;
                }

                this.Sale.SaleDetails = this.SaleDetails;

                if (!this.CheckOrderLimit())
                {
                    return;
                }

                if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                    || this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusActiveEdit.NouDocStatusID
                    || this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusFilled.NouDocStatusID
                    || this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID)
                    //|| this.uncommittedPriceChange)
                {
                    var localDocStatus = this.SelectedDocStatus.NouDocStatusID;
                    if (this.uncommittedPriceChange)
                    {
                        //localDocStatus = NouvemGlobal.NouDocStatusFilled.NouDocStatusID;
                    }

                    if (this.DataManager.ChangeARDispatchStatus(this.Sale, localDocStatus))
                    {
                        if (this.uncommittedPriceChange)
                        {
                            this.uncommittedPriceChange = false;
                            //this.UpdateOrder();
                            //return;
                        }

                        this.LastEdit = DateTime.Now;
                        SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderUpdated, this.Sale.Number));
                        Task.Factory.StartNew(this.GetAllSales);
                        this.ClearForm();
                        this.RefreshDocStatusItems();
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.OrderNotUpdated);
                    }
                }
                else if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    var localsaleId = this.Sale.SaleID;
                    this.CompleteOrder(true);
                    this.DataManager.PriceDispatchDocket(localsaleId);
                }
                else if (this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                {
                    this.CancelOrder();
                    return;
                }

                if (ApplicationSettings.DefaultToFindAtDesktopDispatch)
                {
                    this.SetControlMode(ControlMode.Find);
                }
                else
                {
                    this.SetControlMode(ControlMode.Add);
                }
            }
            finally
            {
                this.OrderUpdateInProgress = false;

                if (this.Locator.SalesSearchData.IsFormLoaded)
                {
                    this.RefreshSales();
                }
            }
        }

        /// <summary>
        /// Gets the current orders live status.
        /// </summary>
        /// <returns>The current orders live status.</returns>
        protected virtual Sale GetLiveOrderStatus()
        {
            if (this.Sale == null)
            {
                return null;
            }

            var localSale = this.DataManager.GetARDispatchesById(this.Sale.SaleID);
            return localSale;
        }

        /// <summary>
        /// Gets the current orders live status.
        /// </summary>
        /// <returns>The current orders live status.</returns>
        protected void GetLiveStatus(IList<App_GetDispatchDetails_Result> data = null)
        {
            if (this.Sale == null)
            {
                return;
            }

            var orderDetails = data;
            if (orderDetails == null)
            {
                orderDetails = this.DataManager.GetDispatchStatusDetails(this.Sale.SaleID);
            }

            if (orderDetails == null)
            {
                return;
            }

            var order = orderDetails.FirstOrDefault();
            if (order == null)
            {
                return;
            }

            this.Sale.DispatchContainerID = order.DispatchContainerID;
            this.Sale.InvoiceNote = order.InvoiceNote;
            this.Sale.PopUpNote = order.PopUpNote;
            this.Sale.CustomerPOReference = order.CustomerPOReference;
            this.Sale.Printed = order.Printed;
            this.Sale.NouDocStatusID = order.NouDocStatusID;
            this.Sale.Remarks = order.Remarks;
           
            foreach (var detail in orderDetails)
            {
                var localSaleDetail = this.SaleDetails.FirstOrDefault(x => x.SaleDetailID == detail.ARDispatchDetailID);
                if (localSaleDetail != null)
                {
                    localSaleDetail.LoadingSale = true;
                    localSaleDetail.QuantityOrdered = detail.QuantityOrdered.ToDecimal();
                    localSaleDetail.WeightOrdered = detail.WeightOrdered.ToDecimal();
                    localSaleDetail.UnitPrice = detail.UnitPrice;
                    localSaleDetail.UnitPriceAfterDiscount = detail.UnitPrice;
                    localSaleDetail.QuantityDelivered = detail.Qty.ToDecimal();
                    localSaleDetail.WeightDelivered = detail.Wgt.ToDecimal();
                    localSaleDetail.BoxCount = detail.BoxCount.ToInt();
                    localSaleDetail.TransactionCount = detail.Generic1.ToInt();
                    localSaleDetail.BPMasterID_Intake = detail.Generic2.ToInt();
                    if (detail.PriceListID.HasValue)
                    {
                        localSaleDetail.PriceListID = detail.PriceListID.ToInt();
                    }
                }
                else
                {
                    var newLine = new SaleDetail
                    {
                        LoadingSale = true,
                        SaleDetailID = detail.ARDispatchDetailID,
                        SaleID = detail.ARDispatchID,
                        INMasterID = detail.INMasterID,
                        QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                        WeightOrdered = detail.WeightOrdered.ToDecimal(),
                        QuantityDelivered = detail.Qty.ToDecimal(),
                        WeightDelivered = detail.Wgt.ToDecimal(),
                        UnitPrice = detail.UnitPrice,
                        UnitPriceAfterDiscount = detail.UnitPrice,
                        BoxCount = detail.BoxCount.ToInt(),
                        BPMasterID_Intake = detail.Generic2.ToInt(),
                        InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == detail.INMasterID)
                   };

                    if (detail.PriceListID.HasValue)
                    {
                        newLine.PriceListID = detail.PriceListID.ToInt();
                    }

                    this.SaleDetails.Add(newLine);
                }
            }

            this.SetWeightData(true);
        }

        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Attempts to remove the selected product.
        /// </summary>
        private void RemoveDispatchItemCommandExecute()
        {
            if (this.processingDeletion)
            {
                return;
            }

            try
            {
                this.processingDeletion = true;
                if (!this.AuthorisationsManager.AllowUserToRemoveProductFromSaleOrder 
                    || !this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
                {
                    SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                    return;
                }

                ProgressBar.SetUp(0, 5);
                ProgressBar.Run();

                if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null
                    && this.SaleDetails != null && this.SaleDetails.Contains(this.SelectedSaleDetail))
                {
                    // If there are no transactions recorded against this product, remove.
                    if (!this.DataManager.DoesProductLineContainTransactions(this.SelectedSaleDetail.SaleDetailID))
                    {
                        ProgressBar.Run();
                        // Deleting product
                        this.Log.LogInfo(this.GetType(), $"Delete product prompt: Product:{this.SelectedSaleDetail.InventoryItem.Name}, Order line:{this.SelectedSaleDetail.SaleDetailID}");
                        NouvemMessageBox.Show(string.Format(Message.RemoveProductPrompt, this.SelectedSaleDetail.InventoryItem.Name), NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.Yes)
                        {
                            if (this.RemoveItem(ViewType.ARDispatch))
                            {
                                this.Log.LogInfo(this.GetType(), "Product removed");
                                SystemMessage.Write(MessageType.Priority, Message.ProductRemoved);
                            }
                        }

                        ProgressBar.Run();

                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.processingDeletion = false;
                this.SelectedSaleDetail = null;
            }
        }

        /// <summary>
        /// Moves back to the main dispatch screen.
        /// </summary>
        private void MoveBackCommandExecute()
        {
            if ((!string.IsNullOrWhiteSpace(this.CustomerPOReference) ||
                    !string.IsNullOrWhiteSpace(this.DocketNote))
                    || this.SelectedDispatchContainer != null && this.Sale != null && this.Sale.SaleID > 0)
            {
                this.Sale.DocketNote = this.DocketNote;
                this.Sale.CustomerPOReference = this.CustomerPOReference;
                if (this.SelectedDispatchContainer != null)
                {
                    this.Sale.DispatchContainerID = this.SelectedDispatchContainer.DispatchContainerID;
                }

                if (this.DataManager.UpdateNotes(this.Sale))
                {
                    SystemMessage.Write(MessageType.Background, Message.NotesUpdated);
                }
            }

            Messenger.Default.Send(true, Token.CloseNotesWindow);
        }

        /// <summary>
        /// Handle the box label command.
        /// </summary>
        private void PrintBoxLabel()
        {
            #region validation

            if (this.SelectedSaleDetail == null || this.SelectedSaleDetail.GoodsReceiptDatas == null)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductSelected);
                return;
            }

            if (!this.SelectedSaleDetail.GoodsReceiptDatas.Any())
            {
                return;
            }

            #endregion

            // add the transaction weight to our stock transaction, and other relevant data.
            var localGoodsReceiptData = this.SelectedSaleDetail.GoodsReceiptDatas.First();
            var qtyPerBox = this.SelectedSaleDetail.InventoryItem.Master.QtyPerBox.ToInt();
            if (qtyPerBox == 0)
            {
                return;
            }

            var trans = new StockTransaction
            {
                BatchNumberID = localGoodsReceiptData.BatchNumber.BatchNumberID,
                MasterTableID = this.SelectedSaleDetail.SaleDetailID,
                TransactionDate = DateTime.Now,
                INMasterID = this.SelectedSaleDetail.InventoryItem.Master.INMasterID,
                NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsDeliveryId,
                WarehouseID = NouvemGlobal.DispatchId,
                DeviceMasterID = NouvemGlobal.DeviceId,
                UserMasterID = NouvemGlobal.UserId,
                IsBox = true
            };

            try
            {
                var transactionId = this.DataManager.AddBoxTransaction(trans, qtyPerBox);
                if (transactionId > 0)
                {
                    this.Print(transactionId, LabelType.Box);
                    SystemMessage.Write(MessageType.Priority, Message.BoxLabelPrinted);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(Message.BoxLabelPrinted,flashMessage:true);
                    }));
                }
            }
            catch (Exception)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoBoxProductsFound);
            }
        }

        /// <summary>
        /// Attempts to remove the selected product.
        /// </summary>
        protected virtual void RemoveProductCommandExecute()
        {
            if (!this.AuthorisationsManager.AllowUserToRemoveProductFromSaleOrder || !this.AuthorisationsManager.AllowUserToAddOrUpdateDispatches)
            {
                SystemMessage.Write(MessageType.Issue, Message.AccessDenied);
                NouvemMessageBox.Show(Message.AccessDenied, touchScreen:ApplicationSettings.TouchScreenMode);
                return;
            }

            try
            {
                if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.InventoryItem != null)
                {
                    // If there are no transactions recorded against this product, remove.
                    if (!this.DataManager.DoesProductLineContainTransactions(this.SelectedSaleDetail.SaleDetailID))
                    {
                        NouvemMessageBox.Show(string.Format(Message.RemoveProductPrompt, this.GetProductName(this.SelectedSaleDetail.INMasterID)),
                            NouvemMessageBoxButtons.YesNo, ApplicationSettings.TouchScreenMode);
                        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                        {
                            return;
                        }

                        ProgressBar.SetUp(0, 5);
                        ProgressBar.Run();
                        ProgressBar.Run();
                        // Deleting product
                        if (this.RemoveItem(ViewType.ARDispatch))
                        {
                            SystemMessage.Write(MessageType.Priority, Message.ProductRemoved);
                            ProgressBar.Run();
                        }
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.TransRecordedNoAuthorisationToRemove);
                        NouvemMessageBox.Show(Message.TransRecordedNoAuthorisationToRemove, touchScreen: ApplicationSettings.TouchScreenMode);
                    }
                }
            }
            finally
            {
                ProgressBar.Reset();
                this.SelectedSaleDetail = null;
            }
        }

        #endregion

        #region helper

        private void CheckAutoBoxing()
        {
            if (this.ScannerStockMode != ScannerMode.ReWeigh)
            {
                return;
            }

            try
            {
                if (this.AutoBoxCount == 1 && !this.SelectedSaleDetail.CompleteAutoBoxing)
                {
                    //this.AutoBoxCount--;
                    if (this.SelectedSaleDetail != null)
                    {
                        this.SelectedSaleDetail.CompleteAutoBoxing = true;
                        this.IndicatorManager.IgnoreValidation(true);
                        this.HandleSerialEntry(this.SelectedSaleDetail.StockDetailToProcess.Serial.ToInt());
                        this.IndicatorManager.IgnoreValidation(false);
                        return;
                    }
                }

                if (this.SelectedSaleDetail != null && this.SelectedSaleDetail.CompleteAutoBoxing)
                {
                    this.SelectedSaleDetail.CompleteAutoBoxing = false;
                }

                if (this.AutoBoxCount > 0)
                {
                    SystemMessage.Write(MessageType.Priority, "Label scanned");
                    this.GetLiveStatus();
                }

                this.AutoBoxCount--;
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
        }

        /// <summary>
        /// Checks for any dispatch lines not on the associated sale order.
        /// </summary>
        private void CheckForExtraDispatchLines()
        {
            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            var data = this.DataManager.CheckForExtraDispatchLines(this.Sale.SaleID);
            if (data.Count > 0)
            {
                var products = string.Join(",", data);
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show($"{products} were not on the sale order. Please check the prices");
                }));
            }
        }

        /// <summary>
        /// Cleans up on order completion.
        /// </summary>
        private void OrderCompletionCleanUp()
        {
            this.ClearForm();
            this.SelectedDispatchContainer = null;
            this.PartnerName = Strings.SelectCustomer;
            this.NextNumber = this.SelectedDocNumbering.NextNumber;
            this.RefreshDocStatusItems();
            Messenger.Default.Send(Token.Message, Token.ClearAndResetData);
            this.Log.LogInfo(this.GetType(), "OrderCompletionCleanUp(): Order Cleared");

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                // refresh the dispatch continer ecreen if open
                Messenger.Default.Send(Token.Message, Token.RefreshDispatchContainers);
            }));
        }

        /// <summary>
        /// Sets the record weight modes.
        /// </summary>
        private void SetRecordWeightModes()
        {
            this.RecordWeightModes = new List<DispatchMode>
            {
                DispatchMode.DispatchRecordWeight,
                DispatchMode.DispatchRecordWeightWithNotes,
                DispatchMode.DispatchRecordWeightWithShipping
            };
        }

        /// <summary>
        /// Sets the fields to that of the live sale.
        /// </summary>
        /// <param name="order"></param>
        private void SetLiveSale(Sale order)
        {
            this.Sale.DispatchContainerID = order.DispatchContainerID;
            this.Sale.InvoiceNote = order.InvoiceNote;
            this.Sale.PopUpNote = order.PopUpNote;
            this.Sale.CustomerPOReference = order.CustomerPOReference;
            this.Sale.Printed = order.Printed;
            this.Sale.NouDocStatusID = order.NouDocStatusID;
            this.Sale.Remarks = order.Remarks;
            this.Sale.SaleDetails = order.SaleDetails;
        }

        /// <summary>
        /// Show the sales.
        /// </summary>
        private void ShowSales()
        {
            this.Log.LogDebug(this.GetType(), "FindSaleCommandExecute()");
            var orderStatuses = this.DocStatusesOpen;
            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales = 
                    this.DataManager.GetAllARDispatchesByDate(true, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateDispatch).ToList();
                //this.CustomerSales = localSales
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllARDispatchesByDate(false, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateDispatch).ToList();
                //this.CustomerSales = this.DataManager.GetAllARDispatches(orderStatuses)
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            Messenger.Default.Send(Tuple.Create(this.CustomerSales, ViewType.ARDispatch), Token.SearchForSale);
            this.Locator.SalesSearchData.SetSearchType(ApplicationSettings.SalesSearchDateDispatch);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.ARDispatch);
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        private void PrintReport(int dispatchId)
        {
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { dispatchId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.WoolleysDispatchDocket, ReportParameters = reportParam };

            try
            {
                for (int i = 0; i < Settings.Default.DispatchReportCopiesToPrint; i++)
                {
                    this.ReportManager.PrintReport(reportData);
                }
              
                SystemMessage.Write(MessageType.Priority, Message.DispatchDocketPrinted);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        private void PrintDispatchDetailReport(int dispatchId)
        {
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "DispatchID", Values = { dispatchId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.WoolleysDispatchDocketDetail, ReportParameters = reportParam };

            try
            {
                for (int i = 0; i < Settings.Default.DispatchReportCopiesToPrint; i++)
                {
                    this.ReportManager.PrintReport(reportData);
                }

                SystemMessage.Write(MessageType.Priority, Message.DispatchDocketPrinted);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Print the dispatch docket.
        /// </summary>
        private void PrintInvoiceReport(int dispatchId)
        {
            var reportParam = new List<ReportParameter> { new ReportParameter { Name = "InvoiceID", Values = { dispatchId.ToString() } } };
            var reportData = new ReportData { Name = ReportName.WoolleysInvoice, ReportParameters = reportParam };

            try
            {
                for (int i = 0; i < Settings.Default.DispatchReportCopiesToPrint; i++)
                {
                    this.ReportManager.PrintReport(reportData);
                }

                SystemMessage.Write(MessageType.Priority, Message.DispatchDocketPrinted);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, ex.Message);
            }
        }

        /// <summary>
        /// Handles an unfilled order authorisation entered password.
        /// </summary>
        /// <param name="result">The authorisation result.</param>
        private void HandleUnfilledOrderAuthorisation(string result)
        {
            if (result.Equals(Constant.Cancel))
            {
                NouvemMessageBox.Show(Message.OrderCompletionCancelled, touchScreen:true);
                return;
            }

            if (result.Equals(Constant.NoAuthorisation))
            {
                NouvemMessageBox.Show(Message.CredentialsNotAuthorised, touchScreen: true);
                return;
            }

            //this.CompleteOrderCheck();
            if (this.DataManager.ChangeARDispatchStatus(this.Sale, NouvemGlobal.NouDocStatusComplete.NouDocStatusID))
            {
                if (this.AutoCopyToInvoice || ApplicationSettings.AutoCreateInvoiceOnOrderCompletion)
                {
                    ProgressBar.Run();
                    this.Log.LogDebug(this.GetType(), "CompleteOrder(): Auto copying to an invoice");
                    this.AutoCopyDispatchToInvoice();
                }
                else
                {
                    SystemMessage.Write(MessageType.Priority, string.Format(Message.OrderCompleted, this.Sale.Number));
                }

                this.ClearForm();
                this.PartnerName = Strings.SelectCustomer;
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
                this.RefreshDocStatusItems();
                Messenger.Default.Send(Token.Message, Token.ClearAndResetData);
            }
            else
            {
                SystemMessage.Write(MessageType.Priority, Message.OrderNotCompleted);
            }
        }

        /// <summary>
        /// Determines if an order is filled.
        /// </summary>
        /// <returns>Flag, as to whether an order has been filled.</returns>
        private bool IsOrderFilled()
        {
            foreach (var detail in this.SaleDetails)
            {
                if (detail.NouOrderMethod == OrderMethod.Weight)
                {
                    if (detail.WeightDelivered.ToDouble() < detail.WeightOrdered.ToDouble())
                    {
                        return false;
                    }
                }
                else if (detail.NouOrderMethod == OrderMethod.Quantity)
                {
                    if (detail.QuantityDelivered.ToDouble() < detail.QuantityOrdered.ToDouble())
                    {
                        return false;
                    }
                }
                else
                {
                    if (detail.WeightDelivered.ToDouble() < detail.WeightOrdered.ToDouble()
                        || detail.QuantityDelivered.ToDouble() < detail.QuantityOrdered.ToDouble())
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Copies the current dispatch docket to an invoice.(We don't show the invoice screen).
        /// </summary>
        private void AutoCopyDispatchToInvoice()
        {
            try
            {
                this.DataManager.PriceDispatchDocket(this.Sale.SaleID);
                this.DataManager.AddDeliveryCharges(this.Sale.SaleID);
               
                ProgressBar.Run();
                this.AutoCopying = true;
                this.DataManager.CopyDispatchToInvoice(this.Sale.SaleID, NouvemGlobal.UserId.ToInt(),
                    NouvemGlobal.DeviceId.ToInt());
            }
            finally
            {
                ProgressBar.Reset();
                this.AutoCopying = false;
            }
        }

        /// <summary>
        /// Copies the current dispatch to an invoice
        /// </summary>
        private void CopyDispatchToInvoice()
        {
            this.Sale.CreationDate = DateTime.Now;
            this.Sale.BaseDocumentReferenceID = this.Sale.SaleID;
            this.Sale.BaseDocumentCreationDate = this.Sale.CreationDate;
            Messenger.Default.Send(ViewType.Invoice);
            CopyingDocument = true;
            this.Sale.CopyingFromOrder = true;
            this.Locator.Invoice.CopySale(this.Sale);
        }

        /// <summary>
        /// Gets the price methods.
        /// </summary>
        private void GetPriceMethods()
        {
            this.priceMethods = this.DataManager.GetPriceMethods();
        }

        /// <summary>
        /// Set up the product removal timer.
        /// </summary>
        private void SetUpTimer()
        {
            this.Timer.Interval = TimeSpan.FromSeconds(0.5);
            this.Timer.Tick += (sender, args) =>
            {
                this.DontChangeMode = true;
                this.Timer.Stop();
                if (ApplicationSettings.TouchScreenMode)
                {
                    Keypad.Show(KeypadTarget.ManualSerial, ApplicationSettings.ScannerMode, useKeypadSides:ApplicationSettings.UseKeypadSidesAtDispatch);
                }
                else
                {
                    this.DrillDownManualSerialCommandExecute();
                }               
            };
        }
    
        /// <summary>
        /// Gets all the quotes.
        /// </summary>
        private void GetAllSales()
        {
            this.Log.LogDebug(this.GetType(), "GetAllSales()");
            //this.AllSales = this.DataManager.GetAllARDispatches(this.DocStatusesOpen);
            //this.Dispatches = new ObservableCollection<Sale>(this.AllSales.Take(200));
        }

        /// <summary>
        /// Handler for sales refreshing.
        /// </summary>
        public override void RefreshSales()
        {
            this.Log.LogDebug(this.GetType(), "RefeshSales()");
            if (this.Locator.SalesSearchData.AssociatedView != this.GetView())
            {
                return;
            }

            var orderStatuses = this.DocStatusesOpen;

            if (ApplicationSettings.SalesSearchShowAllOrders)
            {
                orderStatuses = this.DocStatusesAll;
                this.CustomerSales =
                    this.DataManager.GetAllARDispatchesByDate(true, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateDispatch).ToList();
                //this.CustomerSales = localSales
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }
            else
            {
                this.CustomerSales = this.DataManager.GetAllARDispatchesByDate(false, ApplicationSettings.SalesSearchFromDate.Date, ApplicationSettings.SalesSearchToDate.Date, ApplicationSettings.SalesSearchDateDispatch).ToList();
                //this.CustomerSales = this.DataManager.GetAllARDispatches(orderStatuses)
                //    .Where(x => x.DocumentDate.ToDate().Date >= ApplicationSettings.SalesSearchFromDate.Date && x.DocumentDate.ToDate().Date <= ApplicationSettings.SalesSearchToDate.Date).ToList();
            }

            this.SetActiveDocument(this);
            this.Locator.SalesSearchData.SetSales(this.CustomerSales);
            this.Locator.SalesSearchData.SetSearchType(ApplicationSettings.SalesSearchDateDispatch);

            if (this.Sale != null)
            {
                this.Locator.SalesSearchData.SetView(ViewType.ARDispatch);
            }
        }

        /// <summary>
        /// Refreshes the goods in details.
        /// </summary>
        private void RefreshGoodsIn()
        {
            Messenger.Default.Send(Token.Message, Token.RefreshGoodsinDetails);
        }

        /// <summary>
        /// Displays the base document.
        /// </summary>
        private void ShowBaseDocument()
        {
            try
            {
                var baseDoc = this.DataManager.GetOrderByID(this.Sale.BaseDocumentReferenceID.ToInt());
                Messenger.Default.Send(ViewType.Order);
                this.Locator.Order.Sale = baseDoc;
                SystemMessage.Write(MessageType.Priority, Message.BaseDocumentLoaded);
            }
            finally
            {
                //this.Locator.ARDispatch.DisplayModeOnly = false;
            }
        }

        /// <summary>
        /// Show the current documents document trail.
        /// </summary>
        private void ShowDocumentTrail()
        {
            var sales = this.DataManager.GetDocumentTrail(this.Sale.SaleID, ViewType.ARDispatch);
            this.Locator.DocumentTrail.SetDocuments(sales);
            Messenger.Default.Send(ViewType.DocumentTrail);
        }

        /// <summary>
        /// Refreshes the purchase orders.
        /// </summary>
        [Obsolete]
        private void RefreshOrders()
        {
            Messenger.Default.Send(this.Sale.SaleID, Token.RefreshOrders);
        }

        #endregion

        #endregion
    }
}