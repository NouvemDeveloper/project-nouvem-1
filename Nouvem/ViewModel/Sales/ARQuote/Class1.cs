﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Sales
{
    public abstract class Attributes2ViewModel : NouvemViewModelBase
    {
        //#region field

        //#region dynamic attribute

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute1;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute2;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute3;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute4;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute5;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute6;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute7;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute8;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute9;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute10;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute11;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute12;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute13;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute14;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute15;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute16;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute17;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute18;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute19;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute20;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute21;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute22;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute23;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute24;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute25;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute26;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute27;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute28;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute29;

        ///// <summary>
        ///// The dynamic attribute.
        ///// </summary>
        //private string attribute30;

        //#endregion

        ///// <summary>
        ///// The application customers.
        ///// </summary>
        //private ObservableCollection<ViewBusinessPartner> customers;

        ///// <summary>
        ///// The intake details.
        ///// </summary>
        //private ObservableCollection<StockDetail> stockDetails;

        ///// <summary>
        ///// The application categories.
        ///// </summary>
        //private ObservableCollection<NouCategory> categories;

        ///// <summary>
        ///// The system kill types.
        ///// </summary>
        //private ObservableCollection<NouKillType> killTypes;

        ///// <summary>
        ///// The application categories.
        ///// </summary>
        //private ObservableCollection<CollectionData> freezerTypes;

        ///// <summary>
        ///// The selected detail.
        ///// </summary>
        //private StockDetail selectedStockDetail;

        ///// <summary>
        ///// The customer.
        ///// </summary>
        //private ViewBusinessPartner customer;

        ///// <summary>
        ///// The carcass supplier.
        ///// </summary>
        //private string carcassSupplier;

        ///// <summary>
        ///// The holding no.
        ///// </summary>
        //private int? intakeNumber;

        ///// <summary>
        ///// The holding no.
        ///// </summary>
        //private int? freezerIntakeNumber;

        ///// <summary>
        ///// The holding no.
        ///// </summary>
        //private string holdingNumber;

        ///// <summary>
        ///// The movement id.
        ///// </summary>
        //private string inwardMovementReferenceId;

        ///// <summary>
        ///// The identigen value.
        ///// </summary>
        //private string identigen;

        ///// <summary>
        ///// The carcass supplier.
        ///// </summary>
        //private string generic1;

        ///// <summary>
        ///// The animal eartag.
        ///// </summary>
        //private string farmAssured;

        ///// <summary>
        ///// The animal eartag.
        ///// </summary>
        //private string herdFarmAssured;

        ///// <summary>
        ///// The animal clipped value.
        ///// </summary>
        //private string clipped;

        ///// <summary>
        ///// The animal casualty value.
        ///// </summary>
        //private string casualty;

        ///// <summary>
        ///// The animal lame value.
        ///// </summary>
        //private string lame;

        ///// <summary>
        ///// The animal imported value.
        ///// </summary>
        //private string imported;

        ///// <summary>
        ///// The animal sex.
        ///// </summary>
        //private string sex;

        ///// <summary>
        ///// The animal category.
        ///// </summary>
        //private NouCategory selectedCategory;

        ///// <summary>
        ///// The animal age in months.
        ///// </summary>
        //private int ageInMonths;

        ///// <summary>
        ///// The animal age in days.
        ///// </summary>
        //private int ageInDays;

        ///// <summary>
        ///// The animal dob.
        ///// </summary>
        //private DateTime? dob;

        ///// <summary>
        ///// The animal dob.
        ///// </summary>
        //private DateTime? lastMoveDate;

        ///// <summary>
        ///// The animal breed.
        ///// </summary>
        //private NouBreed selectedBreed;

        ///// <summary>
        ///// The animal cleanliness rating.
        ///// </summary>
        //private NouCleanliness selectedCleanliness;

        ///// <summary>
        ///// The animal cleanliness rating.
        ///// </summary>
        //private CollectionData selectedFreezerType;

        ///// <summary>
        ///// The animal eartag.
        ///// </summary>
        //private string eartag;

        ///// <summary>
        ///// The animal herd no.
        ///// </summary>
        //private string herdNo;

        ///// <summary>
        ///// The animal country of origin.
        ///// </summary>
        //private Model.DataLayer.Country countryOfOrigin;

        ///// <summary>
        ///// The animal country of origin.
        ///// </summary>
        //private Model.DataLayer.Country rearedIn;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool condemned;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool isCasualty;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool detained;

        ///// <summary>
        ///// The current farm residency.
        ///// </summary>
        //private int daysOfResidencyCurrent;

        ///// <summary>
        ///// The total farm residency.
        ///// </summary>
        //private int daysOfResidencyTotal;

        ///// <summary>
        ///// The animal moves.
        ///// </summary>
        //private int numberOfMoves;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool condemnedSide2;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool detainedSide2;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool notInsured;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool tbYes;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool burstBelly;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool legMissing;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private bool abscess;

        ///// <summary>
        ///// The animal halal flag.
        ///// </summary>
        //private string halal;

        ///// <summary>
        ///// The animal condemned flag.
        ///// </summary>
        //private string fatColour;

        ///// <summary>
        ///// The selected date.
        ///// </summary>
        //private DateTime selectedCalenderDate;

        ///// <summary>
        ///// The selected destination.
        ///// </summary>
        //private string selectedDestination;

        ///// <summary>
        ///// The application breeds.
        ///// </summary>
        //private ObservableCollection<NouBreed> breeds;

        ///// <summary>
        ///// Show calender flag.
        ///// </summary>
        //private bool showCalender;

        ///// <summary>
        ///// The application fat colours.
        ///// </summary>
        //private ObservableCollection<NouFatColour> fatColours;

        ///// <summary>
        ///// The selected fat colour.
        ///// </summary>
        //private NouFatColour selectedFatColour;

        ///// <summary>
        ///// Flag, as to whether a category check is to be ignored or not.
        ///// </summary>
        //protected bool IgnoreCategoryCheck;

        ///// <summary>
        ///// Flag, as to whether a category check is to be ignored or not.
        ///// </summary>
        //protected bool IgnoreStockDetailEdit;

        //#endregion

        //#region constructor

        //public Attributes2ViewModel()
        //{
        //    if (this.IsInDesignMode)
        //    {
        //        return;
        //    }

        //    this.GetFreezerTypes();
        //}

        //#endregion

        //#region public interface

        //#region dynamic attribute

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute1
        //{
        //    get
        //    {
        //        return this.attribute1;
        //    }

        //    set
        //    {
        //        this.attribute1 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute2
        //{
        //    get
        //    {
        //        return this.attribute2;
        //    }

        //    set
        //    {
        //        this.attribute2 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute3
        //{
        //    get
        //    {
        //        return this.attribute3;
        //    }

        //    set
        //    {
        //        this.attribute3 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute4
        //{
        //    get
        //    {
        //        return this.attribute4;
        //    }

        //    set
        //    {
        //        this.attribute4 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute5
        //{
        //    get
        //    {
        //        return this.attribute5;
        //    }

        //    set
        //    {
        //        this.attribute5 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute6
        //{
        //    get
        //    {
        //        return this.attribute6;
        //    }

        //    set
        //    {
        //        this.attribute6 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute7
        //{
        //    get
        //    {
        //        return this.attribute7;
        //    }

        //    set
        //    {
        //        this.attribute7 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute8
        //{
        //    get
        //    {
        //        return this.attribute8;
        //    }

        //    set
        //    {
        //        this.attribute8 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute9
        //{
        //    get
        //    {
        //        return this.attribute9;
        //    }

        //    set
        //    {
        //        this.attribute9 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute10
        //{
        //    get
        //    {
        //        return this.attribute10;
        //    }

        //    set
        //    {
        //        this.attribute10 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute11
        //{
        //    get
        //    {
        //        return this.attribute11;
        //    }

        //    set
        //    {
        //        this.attribute11 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute12
        //{
        //    get
        //    {
        //        return this.attribute12;
        //    }

        //    set
        //    {
        //        this.attribute12 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute13
        //{
        //    get
        //    {
        //        return this.attribute13;
        //    }

        //    set
        //    {
        //        this.attribute13 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute14
        //{
        //    get
        //    {
        //        return this.attribute14;
        //    }

        //    set
        //    {
        //        this.attribute14 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute15
        //{
        //    get
        //    {
        //        return this.attribute15;
        //    }

        //    set
        //    {
        //        this.attribute15 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute16
        //{
        //    get
        //    {
        //        return this.attribute16;
        //    }

        //    set
        //    {
        //        this.attribute16 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute17
        //{
        //    get
        //    {
        //        return this.attribute17;
        //    }

        //    set
        //    {
        //        this.attribute17 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute18
        //{
        //    get
        //    {
        //        return this.attribute18;
        //    }

        //    set
        //    {
        //        this.attribute18 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute19
        //{
        //    get
        //    {
        //        return this.attribute19;
        //    }

        //    set
        //    {
        //        this.attribute19 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute20
        //{
        //    get
        //    {
        //        return this.attribute20;
        //    }

        //    set
        //    {
        //        this.attribute20 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute21
        //{
        //    get
        //    {
        //        return this.attribute21;
        //    }

        //    set
        //    {
        //        this.attribute21 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute22
        //{
        //    get
        //    {
        //        return this.attribute22;
        //    }

        //    set
        //    {
        //        this.attribute22 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute23
        //{
        //    get
        //    {
        //        return this.attribute23;
        //    }

        //    set
        //    {
        //        this.attribute23 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute24
        //{
        //    get
        //    {
        //        return this.attribute24;
        //    }

        //    set
        //    {
        //        this.attribute24 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute25
        //{
        //    get
        //    {
        //        return this.attribute25;
        //    }

        //    set
        //    {
        //        this.attribute25 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute26
        //{
        //    get
        //    {
        //        return this.attribute26;
        //    }

        //    set
        //    {
        //        this.attribute26 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute27
        //{
        //    get
        //    {
        //        return this.attribute27;
        //    }

        //    set
        //    {
        //        this.attribute27 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute28
        //{
        //    get
        //    {
        //        return this.attribute28;
        //    }

        //    set
        //    {
        //        this.attribute28 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute29
        //{
        //    get
        //    {
        //        return this.attribute29;
        //    }

        //    set
        //    {
        //        this.attribute29 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dynamic attribute.
        ///// </summary>
        //public string Attribute30
        //{
        //    get
        //    {
        //        return this.attribute30;
        //    }

        //    set
        //    {
        //        this.attribute30 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        //#endregion

        ///// <summary>
        ///// Gets or sets the selected date.
        ///// </summary>
        //public DateTime SelectedCalenderDate
        //{
        //    get
        //    {
        //        return this.selectedCalenderDate;
        //    }

        //    set
        //    {
        //        this.selectedCalenderDate = value;
        //        this.RaisePropertyChanged();
        //        this.HandleSelectedCalenderDate();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the calender is shown.
        ///// </summary>
        //public bool ShowCalender
        //{
        //    get
        //    {
        //        return this.showCalender;
        //    }

        //    set
        //    {
        //        this.showCalender = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is condemned or not.
        ///// </summary>
        //public bool Condemned
        //{
        //    get
        //    {
        //        return this.condemned;
        //    }

        //    set
        //    {
        //        this.condemned = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is detained or not.
        ///// </summary>
        //public bool Detained
        //{
        //    get
        //    {
        //        return this.detained;
        //    }

        //    set
        //    {
        //        this.detained = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is condemned or not.
        ///// </summary>
        //public bool CondemnedSide2
        //{
        //    get
        //    {
        //        return this.condemnedSide2;
        //    }

        //    set
        //    {
        //        this.condemnedSide2 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is detained or not.
        ///// </summary>
        //public bool DetainedSide2
        //{
        //    get
        //    {
        //        return this.detainedSide2;
        //    }

        //    set
        //    {
        //        this.detainedSide2 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is isured or not.
        ///// </summary>
        //public bool NotInsured
        //{
        //    get
        //    {
        //        return this.notInsured;
        //    }

        //    set
        //    {
        //        this.notInsured = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is tb yes or not.
        ///// </summary>
        //public bool TBYes
        //{
        //    get
        //    {
        //        return this.tbYes;
        //    }

        //    set
        //    {
        //        this.tbYes = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is condemned or not.
        ///// </summary>
        //public bool LegMissing
        //{
        //    get
        //    {
        //        return this.legMissing;
        //    }

        //    set
        //    {
        //        this.legMissing = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is condemned or not.
        ///// </summary>
        //public bool Abscess
        //{
        //    get
        //    {
        //        return this.abscess;
        //    }

        //    set
        //    {
        //        this.abscess = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is halal or not.
        ///// </summary>
        //public string Halal
        //{
        //    get
        //    {
        //        return this.halal;
        //    }

        //    set
        //    {
        //        this.halal = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets a value indicating whether the carcass is condemned or not.
        ///// </summary>
        //public bool BurstBelly
        //{
        //    get
        //    {
        //        return this.burstBelly;
        //    }

        //    set
        //    {
        //        this.burstBelly = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the current days residency.
        ///// </summary>
        //public int DaysOfResidencyCurrent
        //{
        //    get
        //    {
        //        return this.daysOfResidencyCurrent;
        //    }

        //    set
        //    {
        //        this.daysOfResidencyCurrent = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the total days residency.
        ///// </summary>
        //public int DaysOfResidencyTotal
        //{
        //    get
        //    {
        //        return this.daysOfResidencyTotal;
        //    }

        //    set
        //    {
        //        this.daysOfResidencyTotal = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the number of valid animal moves.
        ///// </summary>
        //public int NumberOfMoves
        //{
        //    get
        //    {
        //        return this.numberOfMoves;
        //    }

        //    set
        //    {
        //        this.numberOfMoves = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the carcass fat colour.
        ///// </summary>
        //public string FatColour
        //{
        //    get
        //    {
        //        return this.fatColour;
        //    }

        //    set
        //    {
        //        this.fatColour = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the identigen bottle number.
        ///// </summary>
        //public string Identigen
        //{
        //    get
        //    {
        //        return this.identigen;
        //    }

        //    set
        //    {
        //        this.identigen = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the holding number.
        ///// </summary>
        //public string InwardMovementReferenceId
        //{
        //    get
        //    {
        //        return this.inwardMovementReferenceId;
        //    }

        //    set
        //    {
        //        this.inwardMovementReferenceId = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the holding number.
        ///// </summary>
        //public string HoldingNumber
        //{
        //    get
        //    {
        //        return this.holdingNumber;
        //    }

        //    set
        //    {
        //        this.holdingNumber = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the holding number.
        ///// </summary>
        //public int? IntakeNumber
        //{
        //    get
        //    {
        //        return this.intakeNumber;
        //    }

        //    set
        //    {
        //        this.intakeNumber = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the holding number.
        ///// </summary>
        //public int? FreezerIntakeNumber
        //{
        //    get
        //    {
        //        return this.freezerIntakeNumber;
        //    }

        //    set
        //    {
        //        this.freezerIntakeNumber = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the carcass supplier.
        ///// </summary>
        //public string CarcassSupplier
        //{
        //    get
        //    {
        //        return this.carcassSupplier;
        //    }

        //    set
        //    {
        //        this.carcassSupplier = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the selected stock detail.
        ///// </summary>
        //public StockDetail SelectedStockDetail
        //{
        //    get
        //    {
        //        return this.selectedStockDetail;
        //    }

        //    set
        //    {
        //        this.selectedStockDetail = value;
        //        this.RaisePropertyChanged();

        //        if (value != null && !this.IgnoreStockDetailEdit)
        //        {
        //            this.HandleSelectedStockDetail(value);
        //        }
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the system kill types.
        ///// </summary>
        //public ObservableCollection<StockDetail> StockDetails
        //{
        //    get
        //    {
        //        return this.stockDetails;
        //    }

        //    set
        //    {
        //        this.stockDetails = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the cleanliness values.
        ///// </summary>
        //public IList<NouCleanliness> Cleanlinesses { get; set; }

        ///// <summary>
        ///// Gets or sets the categories.
        ///// </summary>
        //public ObservableCollection<CollectionData> FreezerTypes
        //{
        //    get
        //    {
        //        return this.freezerTypes;
        //    }

        //    set
        //    {
        //        this.freezerTypes = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the categories.
        ///// </summary>
        //public CollectionData SelectedFreezerType
        //{
        //    get
        //    {
        //        return this.selectedFreezerType;
        //    }

        //    set
        //    {
        //        this.selectedFreezerType = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the system kill types.
        ///// </summary>
        //public ObservableCollection<NouKillType> KillTypes
        //{
        //    get
        //    {
        //        return this.killTypes;
        //    }

        //    set
        //    {
        //        this.killTypes = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the categories.
        ///// </summary>
        //public ObservableCollection<NouCategory> Categories
        //{
        //    get
        //    {
        //        return this.categories;
        //    }

        //    set
        //    {
        //        this.categories = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the sexes.
        ///// </summary>
        //public IList<string> Sexes { get; set; }

        ///// <summary>
        ///// Gets or sets the yes/no values.
        ///// </summary>
        //public IList<string> YesNoValues { get; set; }

        ///// <summary>
        ///// Gets or sets the destinations.
        ///// </summary>
        //public IList<string> Destinations { get; set; }

        ///// <summary>
        ///// Gets or sets the look ups.
        ///// </summary>
        //public IList<AttributeLookup> AttributeLookUps { get; set; }

        ///// <summary>
        ///// Gets or sets the selected destination.
        ///// </summary>
        //public string SelectedDestination
        //{
        //    get
        //    {
        //        return this.selectedDestination;
        //    }

        //    set
        //    {
        //        this.selectedDestination = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the fat colours.
        ///// </summary>
        //public ObservableCollection<NouFatColour> FatColours
        //{
        //    get
        //    {
        //        return this.fatColours;
        //    }

        //    set
        //    {
        //        this.fatColours = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the selected fat colour.
        ///// </summary>
        //public NouFatColour SelectedFatColour
        //{
        //    get
        //    {
        //        return this.selectedFatColour;
        //    }

        //    set
        //    {
        //        this.selectedFatColour = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the breeds.
        ///// </summary>
        //public ObservableCollection<NouBreed> Breeds
        //{
        //    get
        //    {
        //        return this.breeds;
        //    }

        //    set
        //    {
        //        this.breeds = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the eartag.
        ///// </summary>
        //public ObservableCollection<ViewBusinessPartner> Customers
        //{
        //    get
        //    {
        //        return this.customers;
        //    }

        //    set
        //    {
        //        this.customers = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the eartag.
        ///// </summary>
        //public string Eartag
        //{
        //    get
        //    {
        //        return this.eartag;
        //    }

        //    set
        //    {
        //        this.eartag = value;
        //        this.RaisePropertyChanged();
        //        Messenger.Default.Send(Token.Message, Token.FocusToHerdNo);
        //        this.HandleEartagEntry();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the country of origin.
        ///// </summary>
        //public Model.DataLayer.Country CountryOfOrigin
        //{
        //    get
        //    {
        //        return this.countryOfOrigin;
        //    }

        //    set
        //    {
        //        this.countryOfOrigin = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the application countries.
        ///// </summary>
        //public IList<Model.DataLayer.Country> Countries { get; set; }

        ///// <summary>
        ///// Gets or sets the country of origin.
        ///// </summary>
        //public Model.DataLayer.Country RearedIn
        //{
        //    get
        //    {
        //        return this.rearedIn;
        //    }

        //    set
        //    {
        //        this.rearedIn = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the eartag.
        ///// </summary>
        //public string HerdNo
        //{
        //    get
        //    {
        //        return this.herdNo;
        //    }

        //    set
        //    {
        //        this.herdNo = value;
        //        this.RaisePropertyChanged();
        //        if (!string.IsNullOrEmpty(value) && value.Length >= 10 && value.Substring(0, 8).IsNumericSequence())
        //        {
        //            // scanned passport barcode data
        //            this.HandleScannedPassportData(value);
        //        }
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the breed.
        ///// </summary>
        //public NouBreed Breed
        //{
        //    get
        //    {
        //        return this.selectedBreed;
        //    }

        //    set
        //    {
        //        this.selectedBreed = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the cleanliness rating.
        ///// </summary>
        //public NouCleanliness SelectedCleanliness
        //{
        //    get
        //    {
        //        return this.selectedCleanliness;
        //    }

        //    set
        //    {
        //        this.selectedCleanliness = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dob.
        ///// </summary>
        //public DateTime? LastMoveDate
        //{
        //    get
        //    {
        //        return this.lastMoveDate;
        //    }

        //    set
        //    {
        //        this.lastMoveDate = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the dob.
        ///// </summary>
        //public DateTime? DOB
        //{
        //    get
        //    {
        //        return this.dob;
        //    }

        //    set
        //    {
        //        this.dob = value;
        //        this.RaisePropertyChanged();
        //        this.AgeInMonths = value.ToDate().DateDifferenceInMonths(DateTime.Today);
        //        this.AgeInDays = value.ToDate().DateDifferenceInDays(DateTime.Today);
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the age in months.
        ///// </summary>
        //public int AgeInMonths
        //{
        //    get
        //    {
        //        return this.ageInMonths;
        //    }

        //    set
        //    {
        //        this.ageInMonths = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the age in days.
        ///// </summary>
        //public int AgeInDays
        //{
        //    get
        //    {
        //        return this.ageInDays;
        //    }

        //    set
        //    {
        //        this.ageInDays = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the previously selected category.
        ///// </summary>
        //public NouCategory PreviousCategory { get; set; }

        ///// <summary>
        ///// Gets or sets the category.
        ///// </summary>
        //public NouCategory SelectedCategory
        //{
        //    get
        //    {
        //        return this.selectedCategory;
        //    }

        //    set
        //    {
        //        this.selectedCategory = value;
        //        if (value != null && !this.IgnoreCategoryCheck)
        //        {
        //            this.HandleCategorySelection(value);
        //        }

        //        this.RaisePropertyChanged();
        //        this.PreviousCategory = value;
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the animal sex.
        ///// </summary>
        //public string Sex
        //{
        //    get
        //    {
        //        return this.sex;
        //    }

        //    set
        //    {
        //        this.sex = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the farm assured flag.
        ///// </summary>
        //public string HerdFarmAssured
        //{
        //    get
        //    {
        //        return this.herdFarmAssured;
        //    }

        //    set
        //    {
        //        this.herdFarmAssured = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the generic 1 value.
        ///// </summary>
        //public string Generic1
        //{
        //    get
        //    {
        //        return this.generic1;
        //    }

        //    set
        //    {
        //        this.generic1 = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the farm assured flag.
        ///// </summary>
        //public string FarmAssured
        //{
        //    get
        //    {
        //        return this.farmAssured;
        //    }

        //    set
        //    {
        //        this.farmAssured = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the farm assured flag.
        ///// </summary>
        //public string Clipped
        //{
        //    get
        //    {
        //        return this.clipped;
        //    }

        //    set
        //    {
        //        this.clipped = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the farm assured flag.
        ///// </summary>
        //public bool IsCasualty
        //{
        //    get
        //    {
        //        return this.isCasualty;
        //    }

        //    set
        //    {
        //        this.isCasualty = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the farm assured flag.
        ///// </summary>
        //public string Casualty
        //{
        //    get
        //    {
        //        return this.casualty;
        //    }

        //    set
        //    {
        //        this.casualty = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the farm assured flag.
        ///// </summary>
        //public string Lame
        //{
        //    get
        //    {
        //        return this.lame;
        //    }

        //    set
        //    {
        //        this.lame = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the animal imported flag.
        ///// </summary>
        //public string Imported
        //{
        //    get
        //    {
        //        return this.imported;
        //    }

        //    set
        //    {
        //        this.imported = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        ///// <summary>
        ///// Gets or sets the customer.
        ///// </summary>
        //public ViewBusinessPartner SelectedCustomer
        //{
        //    get
        //    {
        //        return this.customer;
        //    }

        //    set
        //    {
        //        this.customer = value;
        //        this.RaisePropertyChanged();
        //    }
        //}

        //#endregion

        //#region protected

        ///// <summary>
        ///// Handle the selected stock detail parse/edit.
        ///// </summary>
        ///// <param name="detail">The detail to parse for edit.</param>
        //protected virtual void HandleSelectedStockDetail(StockDetail detail)
        //{
        //}

        ///// <summary>
        ///// Handle the secondary barcode scan on the passport.
        ///// </summary>
        ///// <param name="category">The scanned passport data barcode.</param>
        //protected virtual void HandleCategorySelection(NouCategory category)
        //{
        //}

        ///// <summary>
        ///// Handle the eartag entry.
        ///// </summary>
        //protected virtual void HandleEartagEntry()
        //{
        //}

        ///// <summary>
        ///// Handle the secondary barcode scan on the passport.
        ///// </summary>
        ///// <param name="data">The scanned passport data barcode.</param>
        //protected virtual void HandleScannedPassportData(string data)
        //{
        //}

        ///// <summary>
        ///// Handles the selected date.
        ///// </summary>
        //protected virtual void HandleSelectedCalenderDate()
        //{
        //}

        ///// <summary>
        ///// Creates an attribute.
        ///// </summary>
        ///// <returns>The newly created attribute.</returns>
        //protected virtual Nouvem.Model.DataLayer.Attribute CreateAttribute()
        //{
        //    var cleanlinessId = this.SelectedCleanliness != null ? this.SelectedCleanliness.NouCleanlinessID : (int?)null;
        //    var attribute = new Nouvem.Model.DataLayer.Attribute
        //    {
        //        Eartag = this.Eartag,
        //        Herd = this.HerdNo,
        //        DOB = this.DOB,
        //        DateOfLastMove = this.LastMoveDate,
        //        AgeInMonths = this.AgeInMonths,
        //        AgeInDays = this.AgeInDays,
        //        Category = this.SelectedCategory != null ? this.SelectedCategory.CategoryID : (int?)null,
        //        Breed = this.Breed != null ? this.Breed.NouBreedID : (int?)null,
        //        Sex = this.Sex,
        //        FarmAssured = this.FarmAssured.YesNoToBool(),
        //        Clipped = this.Clipped.YesNoToBool(),
        //        Casualty = this.Casualty.YesNoToBool(),
        //        Imported = this.Imported.YesNoToBool(),
        //        Lame = this.Lame.YesNoToBool(),
        //        Cleanliness = cleanlinessId,
        //        Generic1 = this.Generic1,
        //        CountryOfOrigin = this.CountryOfOrigin != null ? this.CountryOfOrigin.Name : string.Empty,
        //        RearedIn = this.RearedIn != null ? this.RearedIn.Name : string.Empty,
        //        KillType = this.KillType.ToString(),
        //        CurrentResidency = this.DaysOfResidencyCurrent,
        //        TotalResidency = this.DaysOfResidencyTotal,
        //        NoOfMoves = this.NumberOfMoves,
        //        HoldingNumber = this.HoldingNumber,
        //        RPA = this.SelectedStockDetail != null && this.SelectedStockDetail.RPA,
        //        Total = this.SelectedStockDetail != null && this.SelectedStockDetail.TotalExclVat.HasValue ? this.SelectedStockDetail.TotalExclVat : null,
        //        Attribute1 = this.Attribute1,
        //        Attribute2 = this.Attribute2,
        //        Attribute3 = this.Attribute3,
        //        Attribute4 = this.Attribute4,
        //        Attribute5 = this.Attribute5,
        //        Attribute6 = this.Attribute6,
        //        Attribute7 = this.Attribute7,
        //        Attribute8 = this.Attribute8,
        //        Attribute9 = this.Attribute9,
        //        Attribute10 = this.Attribute10,
        //        Attribute11 = this.Attribute11,
        //        Attribute12 = this.Attribute12,
        //        Attribute13 = this.Attribute13,
        //        Attribute14 = this.Attribute14,
        //        Attribute15 = this.Attribute15,
        //        Attribute16 = this.Attribute16,
        //        Attribute17 = this.Attribute17,
        //        Attribute18 = this.Attribute18,
        //        Attribute19 = this.Attribute19,
        //        Attribute20 = this.Attribute20,
        //        Attribute21 = this.Attribute21,
        //        Attribute22 = this.Attribute22,
        //        Attribute23 = this.Attribute23,
        //        Attribute24 = this.Attribute24,
        //        Attribute25 = this.Attribute25,
        //        Attribute26 = this.Attribute26,
        //        Attribute27 = this.Attribute27,
        //        Attribute28 = this.Attribute28,
        //        Attribute29 = this.Attribute29,
        //        Attribute30 = this.Attribute30
        //    };

        //    return attribute;
        //}

        ///// <summary>
        ///// reset the attributes.
        ///// </summary>
        //protected virtual void ClearAttributes()
        //{
        //    this.Eartag = string.Empty;
        //    this.DOB = DateTime.Today;
        //    this.LastMoveDate = null;
        //    this.AgeInMonths = 0;
        //    this.SelectedCategory = null;
        //    this.SelectedCleanliness = null;
        //    this.Breed = null;
        //    this.Sex = string.Empty;
        //    this.FarmAssured = Constant.No;
        //    this.Clipped = Constant.No;
        //    this.Casualty = Constant.No;
        //    this.Lame = Constant.No;
        //    this.Imported = Constant.No;
        //    this.CarcassSupplier = string.Empty;
        //    this.TBYes = false;
        //    this.NotInsured = false;
        //    this.Condemned = false;
        //    this.Detained = false;
        //    this.CondemnedSide2 = false;
        //    this.DetainedSide2 = false;
        //    this.BurstBelly = false;
        //    this.LegMissing = false;
        //    this.Abscess = false;
        //    this.Halal = string.Empty;
        //    this.SelectedFatColour = null;
        //    this.HerdNo = string.Empty;
        //    this.HoldingNumber = string.Empty;
        //    this.Identigen = string.Empty;
        //    this.CountryOfOrigin = null;
        //    this.RearedIn = null;
        //    this.DaysOfResidencyCurrent = 0;
        //    this.DaysOfResidencyTotal = 0;
        //    this.NumberOfMoves = 0;
        //    this.InwardMovementReferenceId = string.Empty;
        //    this.SelectedFreezerType = this.FreezerTypes.FirstOrDefault();
        //    this.FreezerIntakeNumber = null;
        //    this.IntakeNumber = null;
        //    this.Attribute1 = string.Empty;
        //    this.Attribute2 = string.Empty;
        //    this.Attribute3 = string.Empty;
        //    this.Attribute4 = string.Empty;
        //    this.Attribute5 = string.Empty;
        //    this.Attribute6 = string.Empty;
        //    this.Attribute7 = string.Empty;
        //    this.Attribute8 = string.Empty;
        //    this.Attribute9 = string.Empty;
        //    this.Attribute10 = string.Empty;
        //    this.Attribute11 = string.Empty;
        //    this.Attribute12 = string.Empty;
        //    this.Attribute13 = string.Empty;
        //    this.Attribute14 = string.Empty;
        //    this.Attribute15 = string.Empty;
        //    this.Attribute16 = string.Empty;
        //    this.Attribute17 = string.Empty;
        //    this.Attribute18 = string.Empty;
        //    this.Attribute19 = string.Empty;
        //    this.Attribute20 = string.Empty;
        //    this.Attribute21 = string.Empty;
        //    this.Attribute22 = string.Empty;
        //    this.Attribute23 = string.Empty;
        //    this.Attribute24 = string.Empty;
        //    this.Attribute25 = string.Empty;
        //    this.Attribute26 = string.Empty;
        //    this.Attribute27 = string.Empty;
        //    this.Attribute28 = string.Empty;
        //    this.Attribute29 = string.Empty;
        //    this.Attribute30 = string.Empty; ;
        //}

        ///// <summary>
        ///// Locates the assigned look up attribute at runtime, and gets it the attribute value.
        ///// </summary>
        ///// <param name="master">The attribute master.</param>
        ///// <param name="attribute">The value to search.</param>
        //protected string GetAttributeValue(AttributeMaster master, Nouvem.Model.DataLayer.Attribute attribute)
        //{
        //    var lookup = this.AttributeLookUps.FirstOrDefault(x => x.AttributeMasterID == master.AttributeMasterID);
        //    if (lookup == null || attribute == null)
        //    {
        //        return string.Empty;
        //    }

        //    var name = lookup.Attribute_Name;
        //    var typeSource = attribute.GetType();

        //    // Get all the properties of source object type
        //    var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        //    foreach (var property in propertyInfo)
        //    {
        //        var fieldName = property.Name;
        //        var match = name.Equals(fieldName);
        //        if (match)
        //        {
        //            var value = property.GetValue(attribute, null);

        //            if (value == null)
        //            {
        //                return string.Empty;
        //            }

        //            return value.ToString();
        //        }
        //    }

        //    return string.Empty;
        //}

        ///// <summary>
        ///// Locates the assigned look up attribute at runtime, and assigns it the attribute value.
        ///// </summary>
        ///// <param name="master">The attribute master.</param>
        ///// <param name="attributeValue">The value to assign.</param>
        //protected void SetAttributeValue(AttributeMaster master, string attributeValue)
        //{
        //    var lookup = this.AttributeLookUps.FirstOrDefault(x => x.AttributeMasterID == master.AttributeMasterID);
        //    if (lookup == null)
        //    {
        //        return;
        //    }

        //    var name = lookup.Attribute_Name;
        //    var typeSource = this.GetType();

        //    // Get all the properties of source object type
        //    var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        //    foreach (var property in propertyInfo)
        //    {
        //        var fieldName = property.Name;
        //        var match = name.Equals(fieldName);

        //        if (match)
        //        {
        //            property.SetValue(this, attributeValue);
        //            break;
        //        }
        //    }
        //}

        ///// <summary>
        ///// Gets the animal freezer types.
        ///// </summary>
        //protected void GetFreezerTypes()
        //{
        //    this.FreezerTypes = new ObservableCollection<CollectionData>();
        //    this.FreezerTypes.Add(new CollectionData { Data = Strings.NoneSelected });
        //    this.FreezerTypes.Add(new CollectionData { Data = Strings.Full });
        //    this.FreezerTypes.Add(new CollectionData { Data = Strings.Side });
        //    this.FreezerTypes.Add(new CollectionData { Data = Strings.ForeQtr });
        //    this.FreezerTypes.Add(new CollectionData { Data = Strings.HindQtr });
        //    this.SelectedFreezerType = this.FreezerTypes.First();
        //}

        ///// <summary>
        ///// Gets the attribute look ups.
        ///// </summary>
        //protected void GetAttributeLookUps()
        //{
        //    this.AttributeLookUps = this.DataManager.GetAttributeLookUps();
        //}

        ///// <summary>
        ///// Gets the destinations.
        ///// </summary>
        //protected void GetDestinations()
        //{
        //    this.Destinations = this.DataManager.GetDestinations().Select(x => x.Name).ToList();
        //}

        ///// <summary>
        ///// Gets the animal fat colours.
        ///// </summary>
        //protected void GetFatColours()
        //{
        //    this.FatColours = new ObservableCollection<NouFatColour>(this.DataManager.GetFatColours());
        //}

        ///// <summary>
        ///// Gets the animal breeds.
        ///// </summary>
        //protected void GetBreeds()
        //{
        //    this.Breeds = new ObservableCollection<NouBreed>(this.DataManager.GetBreeds());
        //}

        ///// <summary>
        ///// Gets the animal sexes.
        ///// </summary>
        //protected void GetSexes()
        //{
        //    this.Sexes = new List<string> { Strings.Male, Strings.Female };
        //}

        ///// <summary>
        ///// Gets the farm assured values.
        ///// </summary>
        //protected void GetFAValues()
        //{
        //    this.YesNoValues = new List<string> { Strings.Yes, Strings.No };
        //}

        ///// <summary>
        ///// Gets the animal sexes.
        ///// </summary>
        //protected virtual void GetCategories()
        //{
        //    this.Categories = new ObservableCollection<NouCategory>(this.DataManager.GetCategories());
        //}

        ///// <summary>
        ///// Gets the kill types.
        ///// </summary>
        //protected void GetKillTypes()
        //{
        //    this.KillTypes = new ObservableCollection<NouKillType>(NouvemGlobal.NouKillTypes);
        //}

        ///// <summary>
        ///// Gets the cleanliness values.
        ///// </summary>
        //protected void GetCleanlinesses()
        //{
        //    this.Cleanlinesses = this.DataManager.GetCleanlinesses();
        //}

        ///// <summary>
        ///// Gets the customers.
        ///// </summary>
        //protected virtual void GetCustomers()
        //{
        //    this.Customers = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.CustomerPartners.Select(x => x.Details));
        //}

        //#endregion

        //protected override void ControlSelectionCommandExecute()
        //{
        //    //throw new NotImplementedException();
        //}

        //protected override void CancelSelectionCommandExecute()
        //{
        //    // throw new NotImplementedException();
        //}
    }
}

