﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Nouvem.ViewModel.Sales;

namespace Nouvem.ViewModel.Payments
{
    public class PaymentsViewModelBase : SalesViewModelBase
    {
        #region field

        /// <summary>
        /// The payment deductions.
        /// </summary>
        private ObservableCollection<PaymentDeduction> paymentDeductions;

        /// <summary>
        /// The selected payment deductions.
        /// </summary>
        private PaymentDeduction selectedPaymentDeduction;

        /// <summary>
        /// The payment deductions per payment.
        /// </summary>
        protected IList<PaymentDeduction> killPaymentLinkedDeductions;

         /// <summary>
        /// The payment deductions per payment.
        /// </summary>
        protected IList<PaymentDeduction> killPaymentPerAnimalDeductions;

        /// <summary>
        /// The payment deductions per animal.
        /// </summary>
        protected IList<PaymentDeduction> killPaymentPerPaymentDeductions;

        protected IList<PricingMatrix> PricingMatrices = new List<PricingMatrix>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentsViewModelBase"/> class.
        /// </summary>
        public PaymentsViewModelBase()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.NotesEntered, s =>
            {
                if (string.IsNullOrWhiteSpace(s))
                {
                    return;
                }

                if (this.SelectedStockDetail != null)
                {
                    this.SelectedStockDetail.Notes = s;
                    this.SetControlMode(ControlMode.Update);
                }
            });

            Messenger.Default.Register<string>(this, Token.ShowPaymentNotesWindow, s => this.ShowNotesWindow());

            #endregion

            #region command handler

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the selected deduction.
        /// </summary>
        public PaymentDeduction SelectedPaymentDeduction
        {
            get
            {
                return this.selectedPaymentDeduction;
            }

            set
            {
                this.selectedPaymentDeduction = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the deductions.
        /// </summary>
        public ObservableCollection<PaymentDeduction> PaymentDeductions
        {
            get
            {
                return this.paymentDeductions;
            }

            set
            {
                this.paymentDeductions = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the apply methods.
        /// </summary>
        public IList<NouApplyMethod> ApplyMethods { get; set; }

        /// <summary>
        /// Gets or sets the apply methods.
        /// </summary>
        public IList<VATCode> VatCodes { get; set; }

        #endregion

        #region command



        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Get the matrix prices adjustments.
        /// </summary>
        protected virtual void GetPricingMatrix()
        {
            this.PricingMatrices = this.DataManager.GetPricingMatrix();
        }

        /// <summary>
        /// Gets the suppliers.
        /// </summary>
        protected override void GetSuppliers()
        {
            this.Suppliers = new ObservableCollection<ViewBusinessPartner>(NouvemGlobal.BusinessPartners
                 .Where(x => (x.PartnerType.Type.Equals(BusinessPartnerType.LivestockSupplier) 
                              || x.PartnerType.Type.Equals(BusinessPartnerType.CustomerAndLivestock))
                             && ((x.Details.InActiveFrom == null && x.Details.InActiveTo == null)
                                       || !(DateTime.Today >= x.Details.InActiveFrom && DateTime.Today < x.Details.InActiveTo)))
                .OrderBy(x => x.Details.Name)
                .Select(x => x.Details));
        }

        /// <summary>
        /// Gets the yes/noassured values.
        /// </summary>
        protected void GetYesNoValues()
        {
            this.YesNoValues = new List<string> { Strings.Yes, Strings.No };
        }

        /// <summary>
        /// Gets the payment apply methods.
        /// </summary>
        protected void GetApplyMethods()
        {
            this.ApplyMethods = this.DataManager.GetApplyMethods();
        }

        /// <summary>
        /// Gets the vat codes.
        /// </summary>
        protected void GetVatCodes()
        {
            this.VatCodes = this.DataManager.GetVATCodes();
        }

        /// <summary>
        /// Gets the payment deductions.
        /// </summary>
        protected void GetPaymentDeductions()
        {
            this.PaymentDeductions = new ObservableCollection<PaymentDeduction>(this.DataManager.GetPaymentDeductions());
        }

        #endregion

        #region private

        /// <summary>
        /// Opens the sql generator window, sending the current record sql.
        /// </summary>
        protected void ShowNotesWindow()
        {
            if (this.SelectedStockDetail == null)
            {
                return;
            }
           
            Messenger.Default.Send(this.SelectedStockDetail.Notes ?? string.Empty, Token.DisplayNotes);
        }

        #endregion

        protected override void CopyDocumentTo()
        {
            throw new NotImplementedException();
        }

        protected override void CopyDocumentFrom()
        {
            throw new NotImplementedException();
        }

        protected override void FindSale()
        {
            throw new NotImplementedException();
        }

        protected override void FindSaleCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void DirectSearchCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void AddSale()
        {
            throw new NotImplementedException();
        }

       

        protected override void Close()
        {
            throw new NotImplementedException();
        }

        protected override void UpdateSale()
        {
            throw new NotImplementedException();
        }
    }
}
