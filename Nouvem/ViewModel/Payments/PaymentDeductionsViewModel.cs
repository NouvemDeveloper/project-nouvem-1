﻿// -----------------------------------------------------------------------
// <copyright file="CountryMasterViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using Nouvem.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Payments
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class PaymentDeductionsViewModel : PaymentsViewModelBase
    {
        #region field
     
        /// <summary>
        /// The deductions to update.
        /// </summary>
        private IList<PaymentDeduction> paymentDeductionsToUpdate = new List<PaymentDeduction>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the PaymentDeductionsViewModel class.
        /// </summary>
        public PaymentDeductionsViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.SetControlMode(ControlMode.Update);
                if (this.SelectedPaymentDeduction != null)
                {
                    if (!this.paymentDeductionsToUpdate.Contains(this.SelectedPaymentDeduction))
                    {
                        this.paymentDeductionsToUpdate.Add(this.SelectedPaymentDeduction);
                    }
                }

            }, this.CanUpdate);

            #endregion

            this.GetApplyMethods();
            this.GetVatCodes();
            this.GetPaymentDeductions();
            this.GetCategories();
            this.GetKillTypes();
            this.SetControlMode(ControlMode.OK);
        }

        #endregion

        #region property
       

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Remove the deduction line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.SelectedPaymentDeduction != null)
            {
                this.SelectedPaymentDeduction.Deleted = DateTime.Now;
                this.paymentDeductionsToUpdate.Clear();
                this.paymentDeductionsToUpdate.Add(this.SelectedPaymentDeduction);
                this.UpdatePaymentDeductions();
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.PaymentDeductions.Remove(this.SelectedPaymentDeduction);
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdatePaymentDeductions();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearPaymentDeductions();
            Messenger.Default.Send(Token.Message, Token.ClosePaymentDeductionsWindow);
        }

        #endregion

        #region private

        /// <summary>
        /// Refreshes the ui.
        /// </summary>
        private void Refresh()
        {
            this.paymentDeductionsToUpdate.Clear();
        }

        /// <summary>
        /// Updates the deductions.
        /// </summary>
        private void UpdatePaymentDeductions()
        {
            #region validation

            var missingType = this.paymentDeductionsToUpdate
                .Any(x => x.NouApplyMethodName.CompareIgnoringCase(ApplyMethod.OncePerAnimal) && !x.NouKillTypeID.HasValue);
            if (missingType)
            {
                SystemMessage.Write(MessageType.Issue, Message.NoPerAnimalKillTypeSelected);
                NouvemMessageBox.Show(Message.NoPerAnimalKillTypeSelected);
                return;
            }

            #endregion

            try
            {
                if (this.DataManager.AddOrUpdatePaymentDeductions(this.paymentDeductionsToUpdate))
                {
                    SystemMessage.Write(MessageType.Priority, Message.PaymentDeductionsUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PaymentDeductionsNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.CountryNotUpdated);
            }

            this.Refresh();
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}

