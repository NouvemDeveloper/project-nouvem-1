﻿// -----------------------------------------------------------------------
// <copyright file="PaymentViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using DevExpress.Xpf.Grid;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Reporting.WinForms;
using Nouvem.ReportService;

namespace Nouvem.ViewModel.Payments
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;

    public class PaymentViewModel : PaymentsViewModelBase
    {
        #region field

        /// <summary>
        /// The selected kill type.
        /// </summary>
        private NouKillType selectedKillType;

        /// <summary>
        /// The selected kill type.
        /// </summary>
        private int? carcassCount;

        /// <summary>
        /// The selected kill type.
        /// </summary>
        private decimal? weightChange;

        /// <summary>
        /// All the categories.
        /// </summary>
        private IList<NouCategory> allCategories = new List<NouCategory>();

        /// <summary>
        /// The system kill types.
        /// </summary>
        private ObservableCollection<NouKillType> killTypes;

        /// <summary>
        /// The payment deductions.
        /// </summary>
        private ObservableCollection<PaymentDeductionItem> paymentDeductionItems;

        /// <summary>
        /// The payment deductions grouped.
        /// </summary>
        private ObservableCollection<PaymentDeductionItem> paymentDeductionItemsTotal = new ObservableCollection<PaymentDeductionItem>();

        /// <summary>
        /// The selected payment deduction.
        /// </summary>
        private PaymentDeductionItem selectedPaymentDeductionItem;

        /// <summary>
        /// The deductions total.
        /// </summary>
        private decimal deductionsTotal;
        /// <summary>
        /// The deductions vat total.
        /// </summary>
        private decimal deductionsVAT;

        /// <summary>
        /// The supplier herds.
        /// </summary>
        private SupplierHerd selectedHerd;

        /// <summary>
        /// The supplier herds.
        /// </summary>
        private bool herdSelected;

        /// <summary>
        /// The payment cheque no.
        /// </summary>
        private int? chequeNumber;

        /// <summary>
        /// The supplier herds.
        /// </summary>
        private ObservableCollection<SupplierHerd> herds;

        /// <summary>
        /// The carcasses to update.
        /// </summary>
        private IList<StockDetail> carcassesToUpdate = new List<StockDetail>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentViewModel"/> class.
        /// </summary>
        public PaymentViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for the weight band prices.
            Messenger.Default.Register<List<WeightBandData>>(this, Token.ApplyWeightBandPrices, this.HandleWeightBands);

            // Register for a report print/preview instruction.
            Messenger.Default.Register<string>(this, Token.ReportCommand, s =>
            {
                if (this.IsFormLoaded)
                {
                    this.PrintReport(s);
                }
            });

            Messenger.Default.Register<List<StockDetail>>(this, Token.CarcassesSelected, this.CreateOrUpdateProposal);

            Messenger.Default.Register<bool>(this, Token.CalculatePayment, isDeduction =>
            {
                this.CalculateTotals();
                if (isDeduction)
                {
                    // coming from a deduction so update the totals
                    this.SetDeductionTotals();
                }

                this.SetControlMode(ControlMode.Update);
            });

            Messenger.Default.Register<Sale>(this, Token.PaymentProposalSelected, s =>
            {
                if (this.IsFormLoaded)
                {
                    var localSale = this.DataManager.GetPaymentById(s.SaleID);
                    if (localSale.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                    {
                        localSale.IsBatchPayment = true;
                    }
                  
                    this.Sale = localSale;
                    this.OpenSaleDetails();

                    this.CheckDeductions();
                }
            });

            #endregion

            #region command handler

            //this.CellValueChangedCommand = new RelayCommand<CellValueChangedEventArgs>((e) =>
            //{
            //    if ((e.Cell != null && e.Cell.Property != null && e.Cell.Property.Equals("NouCategoryID")))
            //    {
            //        // category changes, so we need to trigger deductions re-calculations.
            //        Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            //        {
            //            this.PaymentDeductionItems = null;
            //            this.CheckDeductions();
            //        }));
            //    }
            //});

            this.ApplyWeightChangeCommand = new RelayCommand(this.ApplyWeightChangeCommandExecute);
            this.ShowWeightBandPricingCommand = new RelayCommand(() => Messenger.Default.Send(ViewType.WeightBandPricing));

            this.ApplyPriceToAllCommand = new RelayCommand(() =>
            {
                if (this.SelectedStockDetail != null && this.SelectedStockDetail.UnitPrice.HasValue)
                {
                    foreach (var stockDetail in this.StockDetails)
                    {
                        stockDetail.UnitPrice = this.SelectedStockDetail.UnitPrice;
                    }
                }
            });

            #endregion

            if (NouvemGlobal.SupplierHerds == null)
            {
                NouvemGlobal.SupplierHerds = this.DataManager.GetSupplierHerds();
            }

            this.GetCategories();
            this.GetYesNoValues();
            this.Herds = new ObservableCollection<SupplierHerd>(NouvemGlobal.SupplierHerds);
            this.GetLocalDocNumberings();
            this.KillTypes = new ObservableCollection<NouKillType>(NouvemGlobal.NouKillTypes);
            this.GetPaymentDeductions();
            this.GetPricingMatrix();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or set the categories.
        /// </summary>
        public IList<NouCategory> NouCategories { get; set; }

        /// <summary>
        /// Gets or sets the payment cheque no.
        /// </summary>
        public decimal? WeightChange
        {
            get
            {
                return this.weightChange;
            }

            set
            {
                this.weightChange = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the payment cheque no.
        /// </summary>
        public int? CarcassCount
        {
            get
            {
                return this.carcassCount;
            }

            set
            {
                this.carcassCount = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the payment cheque no.
        /// </summary>
        public int? ChequeNumber
        {
            get
            {
                return this.chequeNumber;
            }

            set
            {
                this.chequeNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the supplier herds.
        /// </summary>
        public ObservableCollection<SupplierHerd> Herds
        {
            get
            {
                return this.herds;
            }

            set
            {
                this.herds = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the supplier herds.
        /// </summary>
        public SupplierHerd SelectedHerd
        {
            get
            {
                return this.selectedHerd;
            }

            set
            {
                this.selectedHerd = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.herdSelected = true;
                    this.SelectedPartner = this.allPartners.FirstOrDefault(x => x.BPMasterID == value.BPMasterID);
                    this.herdSelected = false;
                }
            }
        }
      
        /// <summary>
        /// Gets or sets the deductions total.
        /// </summary>
        public decimal DeductionsTotal
        {
            get
            {
                return this.deductionsTotal;
            }

            set
            {
                this.deductionsTotal = value;
                this.RaisePropertyChanged();
            }
        }
 
        /// <summary>
        /// Gets or sets the deductions total.
        /// </summary>
        public decimal DeductionsVAT
        {
            get
            {
                return this.deductionsVAT;
            }

            set
            {
                this.deductionsVAT = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the system kill types.
        /// </summary>
        public ObservableCollection<NouKillType> KillTypes
        {
            get
            {
                return this.killTypes;
            }

            set
            {
                this.killTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected kill type.
        /// </summary>
        public NouKillType SelectedKillType
        {
            get
            {
                return this.selectedKillType;
            }

            set
            {
                this.selectedKillType = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    var localCategories = this.allCategories.Where(x => x.CatType.CompareIgnoringCase(value.Name));
                    this.Categories = new ObservableCollection<NouCategory>(localCategories);
                }
            }
        }

        /// <summary>
        /// Gets or sets the deductions.
        /// </summary>
        public ObservableCollection<PaymentDeductionItem> PaymentDeductionItems
        {
            get
            {
                return this.paymentDeductionItems;
            }

            set
            {
                this.paymentDeductionItems = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the deductions totals.
        /// </summary>
        public ObservableCollection<PaymentDeductionItem> PaymentDeductionItemsTotal
        {
            get
            {
                return this.paymentDeductionItemsTotal;
            }

            set
            {
                this.paymentDeductionItemsTotal = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected deduction.
        /// </summary>
        public PaymentDeductionItem SelectedPaymentDeductionItem
        {
            get
            {
                return this.selectedPaymentDeductionItem;
            }

            set
            {
                this.selectedPaymentDeductionItem = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to aplly prices to all items.
        /// </summary>
        public ICommand CellValueChangedCommand { get; set; }

        /// <summary>
        /// Gets the command to aplly prices to all items.
        /// </summary>
        public ICommand ApplyWeightChangeCommand { get; set; }

        /// <summary>
        /// Gets the command to aplly prices to all items.
        /// </summary>
        public ICommand ApplyPriceToAllCommand { get; set; }

        /// <summary>
        /// Gets the command to aplly prices to all items.
        /// </summary>
        public ICommand ShowWeightBandPricingCommand { get; set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Gets the animal sexes.
        /// </summary>
        protected override void GetCategories()
        {
            this.allCategories = this.DataManager.GetCategories();
            this.Categories = new ObservableCollection<NouCategory>(this.allCategories);
        }

        /// <summary>
        /// Override, to set the focus to here.
        /// </summary>
        protected override void UpdateCommandExecute(CellValueChangedEventArgs e)
        {
            base.UpdateCommandExecute(e);
            this.SetActiveDocument(this);

            if (e == null || e.Cell == null || string.IsNullOrEmpty(e.Cell.Property) || !this.IsFormLoaded)
            {
                return;
            }

            if ((e.Cell.Property.Equals("NouCategoryID") || e.Cell.Property.Equals("Grade")) &&
                this.SelectedStockDetail != null)
            {
                var added =
                    this.carcassesToUpdate.FirstOrDefault(
                        x => x.StockTransactionID == this.SelectedStockDetail.StockTransactionID);
                if (added != null)
                {
                    this.carcassesToUpdate.Remove(added);
                }
            
                this.carcassesToUpdate.Add(this.SelectedStockDetail);
                this.SetControlMode(ControlMode.Update);

                if ((e.Cell != null && e.Cell.Property != null && e.Cell.Property.Equals("NouCategoryID")))
                {
                    // category changes, so we need to trigger deductions re-calculations.
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.PaymentDeductionItems = null;
                        this.CheckDeductions();
                    }));
                }
            }
        }

         /// <summary>
         /// Removes an item from the payment.
         /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            if (this.SelectedStockDetail != null)
            {
                if (this.Sale.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    SystemMessage.Write(MessageType.Issue, Message.CannotRemoveAnimalFromCompletedPayment);
                    return;
                }

                this.DataManager.RemoveCarcassFromPayment(this.SelectedStockDetail.StockTransactionID.ToInt());
                this.StockDetails.Remove(this.SelectedStockDetail);

                var perAnimalDeductionItems =
                    this.PaymentDeductionItems.Where(x => x.NouApplyMethodName.Equals(ApplyMethod.OncePerAnimal))
                        .GroupBy(x => x.KillPaymentDeductionID);

                foreach (var deductionItems in perAnimalDeductionItems)
                {
                    if (deductionItems.Any())
                    {
                        var localDeduction = deductionItems.First();
                        this.DataManager.RemovePaymentDeductionItemFromPayment(localDeduction.KillPaymentDeductionItemID);
                        this.PaymentDeductionItems.Remove(localDeduction);
                    }
                }

                this.CalculateTotals();
                this.UpdatePayment(false);
                SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
                this.OpenSaleDetails();
            }
        }

        /// <summary>
        /// Handler for the ui load event.
        /// </summary>
        protected override void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            //this.SetWriteAuthorisation(Authorisation.AllowUserToAccessOrderCreation);
            this.SetControlMode(ControlMode.OK);
            this.killPaymentPerPaymentDeductions =
                this.PaymentDeductions.Where(x => x.NouApplyMethodName.Equals(ApplyMethod.OncePerPayment)).ToList();

            this.killPaymentPerAnimalDeductions =
                this.PaymentDeductions.Where(x => x.NouApplyMethodName.Equals(ApplyMethod.OncePerAnimal)).ToList();

            this.killPaymentLinkedDeductions =
                this.PaymentDeductions.Where(x => x.NouApplyMethodName.Equals(ApplyMethod.LinkedToAttribute)).ToList();
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            //base.ParsePartner();

            // now get the associated customer quotes.
            if (this.SelectedPartner == null)
            {
                return;
            }

            var supplier =
               NouvemGlobal.SupplierPartners.FirstOrDefault(
                   x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);

            this.SelectedDeliveryAddressFull = string.Empty;

            if (supplier != null)
            {
                supplier = this.DataManager.GetBusinessPartner(supplier.Details.BPMasterID);
                if (supplier.Addresses != null)
                {
                    var deliveryAddress = supplier.Addresses.FirstOrDefault(x => x.Details.Shipping == true);
                    if (deliveryAddress != null)
                    {
                        this.SelectedDeliveryAddress = deliveryAddress;
                        this.SelectedDeliveryAddressFull = deliveryAddress.FullAddressMultiLine;
                    }
                }
            }

            if (!this.herdSelected)
            {
                this.SelectedHerd = this.Herds.FirstOrDefault(x => x.BPMasterID == this.SelectedPartner.BPMasterID);
            }

            if (!this.EntitySelectionChange)
            {
                this.SetControlMode(ControlMode.Add);
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation last edit command.
        /// </summary>
        public override void MoveLastEdit()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetPaymentByLastEdit();
            this.OpenSaleDetails();
            this.CheckDeductions();
            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Handler for the master ribbon navigation back command.
        /// </summary>
        public override void MoveBack()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetPaymentByFirstLast(true);
                this.OpenSaleDetails();
                this.CheckDeductions();
                return;
            }

            var localSale = this.DataManager.GetPaymentById(this.Sale.SaleID - 1);
            if (localSale != null)
            {
                this.Sale = localSale;
                this.OpenSaleDetails();
                this.CheckDeductions();
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation next command.
        /// </summary>
        public override void MoveNext()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                this.Sale = this.DataManager.GetPaymentByFirstLast(false);
                this.OpenSaleDetails();
                this.CheckDeductions();
                return;
            }

            var localSale = this.DataManager.GetPaymentById(this.Sale.SaleID + 1);
            if (localSale != null)
            {
                this.Sale = localSale;
                this.OpenSaleDetails();
                this.CheckDeductions();
            }
        }

        /// <summary>
        /// Handler for the master ribbon navigation first command.
        /// </summary>
        public override void MoveFirst()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetPaymentByFirstLast(true);
            this.OpenSaleDetails();
            this.CheckDeductions();
        }

        /// <summary>
        /// Handler for the master ribbon navigation last command.
        /// </summary>
        public override void MoveLast()
        {
            if (!this.IsFormLoaded)
            {
                return;
            }

            this.Sale = this.DataManager.GetLairageIntakeByFirstLast(false);
            this.OpenSaleDetails();
            this.CheckDeductions();
        }

        /// <summary>
        /// Resets the ui.
        /// </summary>
        /// <param name="clearSelectedCustomer">Flag, indicating whether the selected customer data is cleared.</param>
        protected override void ClearForm(bool clearSelectedCustomer = true)
        {
            base.ClearForm(clearSelectedCustomer);
            this.DeductionsTotal = 0;
            this.DeductionsVAT = 0;
            this.ChequeNumber = null;
            this.CarcassCount = null;
            this.WeightChange = null;
            this.carcassesToUpdate.Clear();

            if (this.StockDetails != null)
            {
                this.StockDetails.Clear();
            }

            if (this.PaymentDeductionItems != null)
            {
                this.PaymentDeductionItems.Clear();
            }
        }

        /// <summary>
        /// Handle a partner change.
        /// </summary>
        protected override void HandlePartnerChange()
        {
            if (!this.EntitySelectionChange && (this.Sale == null || this.Sale.SaleID == 0))
            {
                this.ClearForm(false);
            }
            else if (!this.EntitySelectionChange)
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.SetControlMode(ControlMode.Update);
                }));
            }

            this.PartnerName = this.SelectedPartner.Name;
            this.ParsePartner();
            this.CanCopyFrom = true;
        }

        /// <summary>
        /// Overrides the selected order.
        /// </summary>
        protected override void HandleSelectedSale()
        {
            base.HandleSelectedSale();
            this.DeductionsTotal = this.Sale.DeductionsIncVAT.ToDecimal();
            this.DeductionsVAT = this.Sale.DeductionsVAT.ToDecimal();
            this.ChequeNumber = this.Sale.ChequeNumber == 0 ? (int?)null : this.Sale.ChequeNumber;
            if (!string.IsNullOrEmpty(this.Sale.KillType))
            {
                this.SelectedKillType = this.KillTypes.FirstOrDefault(x => x.Name.CompareIgnoringCase(this.Sale.KillType));
            }
            else
            {
                this.SelectedKillType = null;
            }

            if (this.Sale.StockDetails != null)
            {
                if (this.PricingMatrices != null 
                    && this.PricingMatrices.Any() 
                    && (this.Sale.NouDocStatusID == 1 || this.Sale.NouDocStatusID == 4))
                {
                    foreach (var saleStockDetail in this.Sale.StockDetails)
                    {
                        var localAdjustment = this.PricingMatrices.FirstOrDefault(x =>
                            x.Grade.CompareIgnoringCaseAndWhitespace(saleStockDetail.Grade));
                        if (localAdjustment != null)
                        {
                            saleStockDetail.LoadingStockDetail = false;
                            saleStockDetail.PriceAdjust = localAdjustment.PriceAdjust;
                            saleStockDetail.UnitPrice = saleStockDetail.UnitPrice;
                            saleStockDetail.LoadingStockDetail = true;
                        }
                    }
                }

                this.StockDetails = new ObservableCollection<StockDetail>(this.Sale.StockDetails.OrderBy(x => x.CarcassNumber));
                this.CarcassCount = this.StockDetails.Count;
            }

            //if (this.Sale.PaymentDeductionItems != null)
            //{
            //    this.PaymentDeductionItems = new ObservableCollection<PaymentDeductionItem>(this.Sale.PaymentDeductionItems);
            //}

            if (this.PaymentDeductions == null)
            {
                this.GetPaymentDeductions();
            }

            if (this.Sale.SaleID == 0 || this.Sale.IsBatchPayment)
            {
                // new or batch selected payment, so set the deductions
                this.HandleDeductions();
            }
            else
            {
                if (this.Sale.PaymentDeductionItems != null)
                {
                    this.PaymentDeductionItems = new ObservableCollection<PaymentDeductionItem>(this.Sale.PaymentDeductionItems);
                }
            }

            if (this.PaymentDeductionItems != null && this.PaymentDeductions != null)
            {
                foreach (var localDeduction in this.PaymentDeductionItems)
                {
                    localDeduction.Deduction =
                        this.PaymentDeductions.FirstOrDefault(
                            x => localDeduction.KillPaymentDeductionID == x.KillPaymentDeductionID);
                }
            }
    
            this.SetDeductionTotals();
            this.HerdFarmAssured = this.Sale.HerdQAS.BoolToYesNo();
        }

        /// <summary>
        /// Gets the local quote document numberings.
        /// </summary>
        protected override void GetLocalDocNumberings()
        {
            base.GetLocalDocNumberings();
            this.DocNumberings =
                new ObservableCollection<DocNumber>(
                    this.allDocNumberings.Where(x => x.DocName.DocumentName.CompareIgnoringCase(Constant.PaymentProposal)));
            this.SelectedDocNumbering = this.DocNumberings.FirstOrDefault(x => x.DocType.Name.Equals(Constant.Standard));
            if (this.SelectedDocNumbering != null)
            {
                this.NextNumber = this.SelectedDocNumbering.NextNumber;
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Override, to handle a control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                    this.Locator.LairageAnimalsSearch.Mode = ViewType.Payment;
                    Messenger.Default.Send(ViewType.LairageAnimalsSearch);
                    break;

                case ControlMode.Find:
                    this.FindSale();
                    break;

                case ControlMode.Update:
                    this.UpdatePayment();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new or find partner, clearing the form.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            var controlMode = (ControlMode)Enum.Parse(typeof(ControlMode), mode);
            this.SetControlMode(controlMode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.ClearForm();
                this.SetControlMode(ControlMode.Find);
                this.RefreshDocStatusItems();
                CopyingDocument = false;
            }
        }

        /// <summary>
        /// Searches for a payment proposal.
        /// </summary>
        protected override void FindSale()
        {
            this.FindSaleCommandExecute();
        }

        /// <summary>
        /// Handler for searching for a sale.
        /// </summary>
        protected override void FindSaleCommandExecute()
        {
            this.Locator.PaymentProposalSearch.Mode = ViewType.Payment;
            Messenger.Default.Send(ViewType.PaymentProposalOpen);
        }

        /// <summary>
        /// print the report.
        /// </summary>
        /// <param name="mode">The report mode selected.</param>
        protected override void PrintReport(string mode)
        {
            var preview = mode.Equals(Constant.Preview);

            #region validation

            if (!this.IsFormLoaded)
            {
                return;
            }

            if (this.Sale == null || this.Sale.SaleID == 0)
            {
                return;
            }

            if (ApplicationSettings.DisableChequeReportAfterFirstPrint && this.DataManager.IsPaymentPrinted(this.Sale.SaleID))
            {
                SystemMessage.Write(MessageType.Issue, Message.PaymentChequePrinted);
                return;
            }

            #endregion

            try
            {
                var reportId = this.Sale.SaleID;
                var report = ReportName.ChequePaymentReport;
                if (this.Sale.KillType.CompareIgnoringCase(Constant.Sheep))
                {
                    report = ReportName.ChequePaymentReport_Sheep;
                }
                else if (this.Sale.KillType.CompareIgnoringCase(Constant.Pig))
                {
                    report = ReportName.ChequePaymentReport_Pig;
                }

                var reportParam = new List<ReportParameter>
            {
                 new ReportParameter { Name = "KillPaymentID", Values = { reportId.ToString() } }
            };

                var reportData = new ReportData { Name = report, ReportParameters = reportParam };

                try
                {

                    if (preview)
                    {
                        this.ReportManager.PreviewReport(reportData);
                    }
                    else
                    {
                        this.ReportManager.PrintReport(reportData);
                    }
                }
                catch (Exception ex)
                {
                    this.Log.LogError(this.GetType(), ex.Message);
                    SystemMessage.Write(MessageType.Issue, ex.Message);
                }

                if (!ApplicationSettings.PrintKillReportAtPayments)
                {
                    return;
                }

                if (this.Sale.KillType.CompareIgnoringCase(Constant.Beef))
                {
                    return;
                }

                var associatedIntakes = this.DataManager.GetPaymentConsolidatedIntakes(this.Sale.SaleID);
                if (!associatedIntakes.Any())
                {
                    associatedIntakes.Add(this.Sale.BaseDocumentReferenceID.ToInt());
                }
               
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    foreach (var i in associatedIntakes)
                    {
                        reportId = i;
                        Messenger.Default.Send(true, Token.DisableMultipleReports);
                        //reportId = this.Sale.BaseDocumentReferenceID.ToInt();

                        var reportName = ReportName.BeefKillReport;
                        if (this.Sale.KillType.CompareIgnoringCase(Constant.Sheep))
                        {
                            reportName = ReportName.SheepKillReport;
                        }
                        else if (this.Sale.KillType.CompareIgnoringCase(Constant.Pig))
                        {
                            reportName = ReportName.PigKillReport;
                        }

                        reportParam = new List<ReportParameter>
                        {
                            new ReportParameter { Name = "GoodsInID", Values = { reportId.ToString() } }
                        };

                        reportData = new ReportData { Name = reportName, ReportParameters = reportParam };

                        try
                        {

                            if (preview)
                            {
                                this.ReportManager.PreviewReport(reportData);
                            }
                            else
                            {
                                this.ReportManager.PrintReport(reportData);
                            }
                        }
                        catch (Exception ex)
                        {
                            this.Log.LogError(this.GetType(), ex.Message);
                            SystemMessage.Write(MessageType.Issue, ex.Message);
                        }
                    }
                }));
            }
            finally
            {
                if (ApplicationSettings.AutoCompletePaymentOnReportPrint && this.Sale.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                {
                    this.SelectedDocStatus = this.DocStatusItems.FirstOrDefault(
                            x => x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID);

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.ContextIdle, new Action(() =>
                    {
                        this.UpdatePayment(true, true);
                    }));
                }

                this.Sale.Printed = true;
                this.DataManager.MarkPaymentAsPrinted(this.Sale.SaleID);
            }
        }

        /// <summary>
        /// Gets the doc status items.
        /// </summary>
        protected override void GetDocStatusItems()
        {
            this.DocStatusItems = NouvemGlobal.NouDocStatusItems
                               .Where(x => x.NouDocStatusID == NouvemGlobal.NouDocStatusActive.NouDocStatusID
                                   || x.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID 
                                || x.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID ||
                                x.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID).ToList();
            this.RefreshDocStatusItems();
        }

        protected override void OnUnloadedCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        protected override void Close()
        {
            this.StockDetails?.Clear();
            this.StockDetails = null;
            this.PaymentDeductionItems?.Clear();
            this.PaymentDeductionItems = null;
            this.carcassesToUpdate?.Clear();
            this.carcassesToUpdate = null;
            ViewModelLocator.ClearPayment();
            Messenger.Default.Send(Token.Message, Token.ClosePaymentWindow);
        }

        #endregion

        #region command execution

        /// <summary>
        /// Aplys weight change to all carcasses.
        /// </summary>
        private void ApplyWeightChangeCommandExecute()
        {
            if (this.WeightChange.HasValue)
            {
                foreach (var stockDetail in this.StockDetails)
                {
                    stockDetail.PaidWeight = stockDetail.PaidWeight + this.WeightChange;
                }
            }
        }

        #endregion

        #region private

        /// <summary>
        /// Applies the iincoming weight bands.
        /// </summary>
        /// <param name="bands">The weight band pricing to apply.</param>
        private void HandleWeightBands(List<WeightBandData> bands)
        {
            #region validation

            if (this.StockDetails == null || !this.StockDetails.Any())
            {
                return;
            }

            #endregion

            foreach (var stockDetail in this.StockDetails)
            {
                var weight = stockDetail.ColdWeight;
                var matchingBands = bands.Where(x => weight >= x.MinWeight && weight < x.MaxWeight).OrderBy(x => x.MinWeight).ToList();
                if (matchingBands.Any())
                {
                    var matchingBand = matchingBands.First();
                    stockDetail.WeightBandId = matchingBand.WeightBandId;
                    if (matchingBand.ApplyCeilingWeight)
                    {
                        stockDetail.PaidWeight = matchingBand.MinWeight;
                        stockDetail.UnitPrice = matchingBand.Price.ToDecimal();
                        stockDetail.OutsideWeightBand = true;
                    }
                    else
                    {
                        stockDetail.UnitPrice = matchingBand.Price.ToDecimal();
                    }
                }
            }
        }

        /// <summary>
        /// Updates the payment.
        /// </summary>
        private void UpdatePayment(bool clear = true, bool disableMessagePrompt = false)
        {
            try
            {
                var complete = this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                var cancelled = this.SelectedDocStatus.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
                if (complete)
                {
                    if (!this.CheckIntakePartner())
                    {
                        return;
                    }

                    if (!disableMessagePrompt)
                    {
                        NouvemMessageBox.Show(Message.PaymentCompletionPrompt, NouvemMessageBoxButtons.YesNo);
                        if (NouvemMessageBox.UserSelection == UserDialogue.No)
                        {
                            return;
                        }
                        else
                        {
                            if (this.CheckColdAndPaidWeightDifference())
                            {
                                NouvemMessageBox.Show(Message.PaidColdWeightDifferences, NouvemMessageBoxButtons.YesNo);
                                if (NouvemMessageBox.UserSelection == UserDialogue.No)
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (this.CheckColdAndPaidWeightDifference())
                        {
                            NouvemMessageBox.Show(Message.PaidColdWeightDifferences, NouvemMessageBoxButtons.YesNo);
                            if (NouvemMessageBox.UserSelection == UserDialogue.No)
                            {
                                return;
                            }
                        }
                    }
                }

                if (cancelled)
                {
                    NouvemMessageBox.Show(Message.CancelPaymentPrompt, NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection == UserDialogue.No)
                    {
                        return;
                    }

                    this.DataManager.CancelPayment(this.Sale);
                    SystemMessage.Write(MessageType.Priority, Message.PaymentUpdated);
                    this.ClearForm();
                    return;
                }

                if (!this.CheckIntakePartner())
                {
                    return;
                }

                if (this.carcassesToUpdate.Any())
                {
                    this.DataManager.UpdateCarcasses(this.carcassesToUpdate);
                    this.carcassesToUpdate.Clear();
                }

                this.CheckDeductions();
                this.Sale.Customer = this.SelectedPartner;
                this.Sale.PaymentDeductionItems = this.PaymentDeductionItems;
                this.Sale.StockDetails = this.StockDetails;
                this.Sale.SubTotalExVAT = this.SubTotal;
                this.Sale.GrandTotalIncVAT = this.Total;
                this.Sale.DeductionsIncVAT = this.DeductionsTotal;
                this.Sale.DeductionsVAT = this.DeductionsVAT;
                this.Sale.Remarks = this.Remarks;
                this.Sale.ChequeNumber = this.ChequeNumber.ToInt();

                var localStatusId = this.SelectedDocStatus.NouDocStatusID;
                if (localStatusId == NouvemGlobal.NouDocStatusActive.NouDocStatusID)
                {
                    localStatusId = NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;
                }

                this.Sale.NouDocStatusID = localStatusId;
                if (this.DataManager.UpdatePayment(this.Sale))
                {
                    var paymentID = this.Sale.SaleID;
                    SystemMessage.Write(MessageType.Priority, Message.PaymentUpdated);
                    this.SetControlMode(ControlMode.OK);
                    //if (complete)
                    //{
                    //    if (this.PrintCheque(paymentID))
                    //    {

                    //    }
                    //}

                    if (clear)
                    {
                        this.ClearForm();
                    }
                    else
                    {
                        this.Sale = this.DataManager.GetPaymentById(paymentID);
                    }
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PaymentNotUpdated);
                }
            }
            finally
            {
                this.carcassesToUpdate.Clear();
                ProgressBar.Reset();
            }
        }

        /// <summary>
        /// Checks the deductions.
        /// </summary>
        private void CheckDeductions()
        {
            if (this.PaymentDeductionItems == null || !this.PaymentDeductionItems.Any() || !this.HaveCorrectDeductionsBeenApplied())
            {
                if (this.PaymentDeductions == null)
                {
                    this.GetPaymentDeductions();
                }

                this.HandleDeductions();
            }

            this.CalculateTotals();
        }

        /// <summary>
        /// Checks if any paid weight is different to a cold weight (ignoring any rounding differences).
        /// </summary>
        /// <returns>Flag, as to any differences.</returns>
        private bool CheckColdAndPaidWeightDifference()
        {
            if (this.StockDetails == null)
            {
                return false;
            }

            foreach (var stockDetail in this.StockDetails)
            {
                var paidWeight = stockDetail.PaidWeight;
                var coldWeight = stockDetail.ColdWeight;
                if (paidWeight.ToDecimal().GetDifference(coldWeight) > 0.05M)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Checks that the supplier on the intake docket matches the supplier on the payment.
        /// </summary>
        /// <returns>Flag, as to supplier match.</returns>
        private bool CheckIntakePartner()
        {
            #region validation

            if (this.Sale == null || this.Sale.BPSupplier == null || this.SelectedPartner == null)
            {
                return true;
            }

            #endregion

            if (this.Sale.BPSupplier.BPMasterID != this.SelectedPartner.BPMasterID)
            {
                var msg = string.Format(Message.IntakePaymentSupplierMismatch, this.Sale.BPSupplier.Name,
                    this.SelectedPartner.Name);
                SystemMessage.Write(MessageType.Issue, msg);
                NouvemMessageBox.Show(msg, NouvemMessageBoxButtons.YesNo);
                return NouvemMessageBox.UserSelection == UserDialogue.Yes;
            }

            return true;
        }

        /// <summary>
        /// Indicator as to whether the correct deduction amounts have been aplied.
        /// </summary>
        /// <returns>A flag, as to whether the correct deduction amounts have been aplied.</returns>
        private bool HaveCorrectDeductionsBeenApplied()
        {
            var animals = this.StockDetails.Count;
            var perAnimalDeductionItems =
                this.PaymentDeductionItems.Count(x => x.NouApplyMethodName.Equals(ApplyMethod.OncePerAnimal) && !x.HasCategory);
            var deductionCount = this.killPaymentPerAnimalDeductions
                .Count(x => ((string.IsNullOrEmpty(x.KillType) || x.KillType.CompareIgnoringCase(this.Sale.KillType)) || this.Sale.KillType == null) && x.CategoryID == null);

            return animals * deductionCount == perAnimalDeductionItems;
        }

        /// <summary>
        /// Calculates the payment.
        /// </summary>
        private void CalculateTotals()
        {
            if (this.PaymentDeductionItems != null)
            {
                this.DeductionsTotal = this.PaymentDeductionItems.Sum(x => x.SubTotalExclVAT);
                this.DeductionsVAT = this.PaymentDeductionItems.Sum(x => x.Vat);
            }

            if (this.StockDetails != null)
            {
                this.SubTotal = this.StockDetails.Sum(x => x.TotalExclVat.ToDecimal());
            }

            #region ifa levy

            if (this.Sale != null && this.Sale.IFALevyApplied == true)
            {
                var ifaDeduction =
               this.killPaymentPerPaymentDeductions.FirstOrDefault(x => x.Name.CompareIgnoringCase(Constant.IFALevy));
                {
                    if (ifaDeduction != null && this.PaymentDeductionItems != null)
                    {
                        ifaDeduction.Price = Math.Round(this.SubTotal * 0.001m, 2);

                        var ifaInstance =
                            this.PaymentDeductionItems.FirstOrDefault(
                                x => x.KillPaymentDeductionID == ifaDeduction.KillPaymentDeductionID);

                        if (ifaInstance == null)
                        {
                            var item = new PaymentDeductionItem
                            {
                                LoadingDeductionItem = true,
                                Deduction = ifaDeduction,
                                KillPaymentDeductionID = ifaDeduction.KillPaymentDeductionID
                            };

                            this.PaymentDeductionItems.Add(item);
                        }
                        else
                        {
                            ifaInstance.Price = ifaDeduction.Price.ToDecimal();
                        }

                        this.DeductionsTotal = this.DeductionsTotal + ifaDeduction.Price.ToDecimal();
                    }
                }
            }

            #endregion

            this.Total = this.SubTotal - this.DeductionsTotal - this.DeductionsVAT;
        }

        /// <summary>
        /// Handles the payment deductions.
        /// </summary>
        private void HandleDeductions()
        {
            this.PaymentDeductionItems = new ObservableCollection<PaymentDeductionItem>();

            if (this.killPaymentPerPaymentDeductions != null)
            {
                foreach (var killPaymentDeduction in this.killPaymentPerPaymentDeductions.Where(x => !x.Name.CompareIgnoringCase(Constant.IFALevy)))
                {
                    var item = new PaymentDeductionItem
                    {
                        Deduction = killPaymentDeduction,
                        KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                    };

                    this.PaymentDeductionItems.Add(item);
                }
            }

            #region per animal deductions

            if (this.StockDetails != null && this.StockDetails.Any() && this.killPaymentPerAnimalDeductions != null)
            {
                var allDeductions =
                    this.killPaymentPerAnimalDeductions.Where(x => string.IsNullOrEmpty(x.KillType)).ToList();

                var beefDeductions =
                    this.killPaymentPerAnimalDeductions.Where(x => x.KillType.CompareIgnoringCase(Constant.Beef)).ToList();

                var sheepDeductions =
                    this.killPaymentPerAnimalDeductions.Where(x => x.KillType.CompareIgnoringCase(Constant.Sheep)).ToList();

                var pigDeductions =
                    this.killPaymentPerAnimalDeductions.Where(x => x.KillType.CompareIgnoringCase(Constant.Pig)).ToList();

                #region all deductions

                foreach (var killPaymentDeduction in allDeductions)
                {
                    // these deductions apply to all the animals
                    for (int i = 0; i < this.StockDetails.Count; i++)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID,
                            AllAnimalDeductionItem = true
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                #endregion

                #region beef deductions

                // these deductions apply to beef animals only
                var beefAnimals = this.StockDetails.Where(x => x.KillType.CompareIgnoringCase(Constant.Beef)).ToList();

                // get the beef deductions that have to be applied to all beef animals
                var allBeefDeductions = beefDeductions.Where(x => !x.CategoryID.HasValue).ToList();

                foreach (var killPaymentDeduction in allBeefDeductions)
                {
                    // these deductions apply to all the beef animals
                    for (int i = 0; i < beefAnimals.Count(); i++)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                // get the beef deductions that have to be applied only to matching beef animals by category
                var categoryBeefDeductions = beefDeductions.Where(x => x.CategoryID.HasValue).ToList();

                foreach (var killPaymentDeduction in categoryBeefDeductions)
                {
                    var matchingAnimals = beefAnimals.Where(x => x.NouCategoryID == killPaymentDeduction.CategoryID).ToList();

                    // these deductions apply to all the beef animals with a matching category
                    for (int i = 0; i < matchingAnimals.Count(); i++)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                #endregion

                #region sheep deductions

                // these deductions apply to sheep animals only
                var sheepAnimals = this.StockDetails.Where(x => x.KillType.CompareIgnoringCase(Constant.Sheep)).ToList();

                // get the sheep deductions that have to be applied to all sheep animals
                var allSheepDeductions = sheepDeductions.Where(x => !x.CategoryID.HasValue).ToList();

                foreach (var killPaymentDeduction in allSheepDeductions)
                {
                    // these deductions apply to all the sheep animals
                    for (int i = 0; i < sheepAnimals.Count(); i++)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                // get the sheep deductions that have to be applied only to matching sheep animals by category
                var categorySheepDeductions = sheepDeductions.Where(x => x.CategoryID.HasValue).ToList();

                foreach (var killPaymentDeduction in categorySheepDeductions)
                {
                    var matchingAnimals = sheepAnimals.Where(x => x.NouCategoryID == killPaymentDeduction.CategoryID).ToList();

                    // these deductions apply to all the sheep animals with a matching category
                    for (int i = 0; i < matchingAnimals.Count(); i++)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                #endregion

                #region pig deductions

                // these deductions apply to pig animals only
                var pigAnimals = this.StockDetails.Where(x => x.KillType.CompareIgnoringCase(Constant.Pig)).ToList();

                // get the pig deductions that have to be applied to all pig animals
                var allPigDeductions = pigDeductions.Where(x => !x.CategoryID.HasValue).ToList();

                foreach (var killPaymentDeduction in allPigDeductions)
                {
                    // these deductions apply to all the pig animals
                    for (int i = 0; i < pigAnimals.Count(); i++)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                // get the pig deductions that have to be applied only to matching pig animals by category
                var categoryPigDeductions = pigDeductions.Where(x => x.CategoryID.HasValue).ToList();

                foreach (var killPaymentDeduction in categoryPigDeductions)
                {
                    var matchingAnimals = pigAnimals.Where(x => x.NouCategoryID == killPaymentDeduction.CategoryID).ToList();

                    // these deductions apply to all the pig animals with a matching category
                    for (int i = 0; i < matchingAnimals.Count(); i++)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                #endregion
            }

            #endregion

            #region linked deductions

            if (this.killPaymentLinkedDeductions != null)
            {
                if (this.StockDetails != null && this.StockDetails.Any())
                {
                    for (int i = 0; i < this.StockDetails.Count; i++)
                    {
                        foreach (var killPaymentDeduction in this.killPaymentLinkedDeductions)
                        {
                            if (killPaymentDeduction.Name.CompareIgnoringCaseAndWhitespace("Insurance U36M")
                                && this.StockDetails.ElementAt(i).AgeInMonths.ToInt() < ApplicationSettings.InsuranceAge)
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCaseAndWhitespace("Insurance O36M")
                                && this.StockDetails.ElementAt(i).AgeInMonths.ToInt() >= ApplicationSettings.InsuranceAge)
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.ContainsIgnoringCase("Slaughter Under")
                                && this.StockDetails.ElementAt(i).AgeInMonths.ToInt() <= ApplicationSettings.YoungBullMaxAge
                                && this.StockDetails.ElementAt(i).Condemned.IsTrue())
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.ContainsIgnoringCase("Slaughter Over")
                                && this.StockDetails.ElementAt(i).AgeInMonths.ToInt() > ApplicationSettings.YoungBullMaxAge
                                && this.StockDetails.ElementAt(i).Condemned.IsTrue())
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if ((killPaymentDeduction.Name.ContainsIgnoringCase("Condemnation") || killPaymentDeduction.Name.ContainsIgnoringCase("Condemned"))
                                && this.StockDetails.ElementAt(i).Condemned == true)
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if ((killPaymentDeduction.Name.ContainsIgnoringCase("Offal Deduction"))
                                && this.StockDetails.ElementAt(i).Condemned == true)
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase(Constant.ClipCharge)
                                && this.StockDetails.ElementAt(i).Clipped == true)
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if ((killPaymentDeduction.Name.ContainsIgnoringCase("Casualty") || killPaymentDeduction.Name.ContainsIgnoringCase("On Farm Slaughter"))
                                && this.StockDetails.ElementAt(i).Casualty == true)
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase("No Eartag")
                                && this.StockDetails.ElementAt(i).Attribute120.CompareIgnoringCase(Constant.Yes))
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if ((killPaymentDeduction.Name.CompareIgnoringCase("Tag Replacement"))
                                && this.StockDetails.ElementAt(i).Attribute120.CompareIgnoringCase(Constant.Yes))
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase(Constant.Insurance)
                                && this.Sale.Insured == true && this.StockDetails.ElementAt(i).KillType.CompareIgnoringCase(Constant.Beef))
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase(Constant.Scrapie)
                                && this.Sale.Scrapie == true
                                && this.StockDetails.ElementAt(i).KillType.CompareIgnoringCase(Constant.Sheep)
                                &&
                                (
                                    (this.StockDetails.ElementAt(i).CategoryName.CompareIgnoringCase(Constant.Ewe) && killPaymentDeduction.CategoryName.CompareIgnoringCase(Constant.Ewe))
                                 || (this.StockDetails.ElementAt(i).CategoryName.CompareIgnoringCase(Constant.Ram) && killPaymentDeduction.CategoryName.CompareIgnoringCase(Constant.Ram))
                                )
                                )
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase(Constant.FreezerChargeBeef)
                                && this.StockDetails.ElementAt(i).FreezerType.CompareIgnoringCase(Strings.Full)
                                && this.StockDetails.ElementAt(i).KillType.CompareIgnoringCase(Constant.Beef))
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase(Constant.FreezerChargeBeef)
                                && this.StockDetails.ElementAt(i).FreezerType.CompareIgnoringCase(Strings.Side)
                                && this.StockDetails.ElementAt(i).KillType.CompareIgnoringCase(Constant.Beef))
                            {
                                var localDeduction = this.CreateDeepCopyDeduction(killPaymentDeduction);
                                localDeduction.Price = localDeduction.Price / 2;
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = localDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase(Constant.FreezerChargeBeef)
                                && (this.StockDetails.ElementAt(i).FreezerType.CompareIgnoringCase(Strings.ForeQtr) || this.StockDetails.ElementAt(i).FreezerType.CompareIgnoringCase(Strings.HindQtr))
                                && this.StockDetails.ElementAt(i).KillType.CompareIgnoringCase(Constant.Beef))
                            {
                                var localDeduction = this.CreateDeepCopyDeduction(killPaymentDeduction);
                                localDeduction.Price = localDeduction.Price / 4;
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = localDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase(Constant.FreezerChargeSheep)
                               && this.StockDetails.ElementAt(i).FreezerType.CompareIgnoringCase(Strings.Full)
                               && this.StockDetails.ElementAt(i).KillType.CompareIgnoringCase(Constant.Sheep))
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }

                            if (killPaymentDeduction.Name.CompareIgnoringCase(Constant.FreezerChargePig)
                               && this.StockDetails.ElementAt(i).FreezerType.CompareIgnoringCase(Strings.Full)
                               && this.StockDetails.ElementAt(i).KillType.CompareIgnoringCase(Constant.Pig))
                            {
                                var item = new PaymentDeductionItem
                                {
                                    Deduction = killPaymentDeduction,
                                    KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                                };

                                this.PaymentDeductionItems.Add(item);
                            }
                        }
                    }
                }

                var haulageDeduction =
                    this.killPaymentLinkedDeductions.FirstOrDefault(
                        x => x.Name.CompareIgnoringCase(Constant.HaulageCharge));

                if (this.Sale != null && haulageDeduction != null && !this.Sale.HaulageCharge.IsNullOrZero())
                {
                    haulageDeduction.Price = this.Sale.HaulageCharge;
                    var item = new PaymentDeductionItem
                    {
                        Deduction = haulageDeduction,
                        KillPaymentDeductionID = haulageDeduction.KillPaymentDeductionID
                    };

                    this.PaymentDeductionItems.Add(item);
                }

                var agentDeduction =
                    this.killPaymentLinkedDeductions.FirstOrDefault(
                        x => x.Name.CompareIgnoringCase(Constant.AgentCharge));

                if (this.Sale != null && this.Sale.AgentID.HasValue)
                {
                    var item = new PaymentDeductionItem
                    {
                        Deduction = agentDeduction,
                        KillPaymentDeductionID = agentDeduction.KillPaymentDeductionID
                    };

                    this.PaymentDeductionItems.Add(item);
                }
            }

            #endregion

            this.CalculateTotals();
        }

        /// <summary>
        /// Sets the deduction totals.
        /// </summary>
        private void SetDeductionTotals()
        {
            this.PaymentDeductionItemsTotal.Clear();
            if (this.PaymentDeductionItems == null)
            {
                return;
            }

            var totalDeductions = new List<PaymentDeductionItem>();
            foreach (var deduction in this.PaymentDeductionItems)
            {
                var deductionTotal =
                    totalDeductions.FirstOrDefault(
                        x => x.KillPaymentDeductionID == deduction.KillPaymentDeductionID);
                if (deductionTotal == null)
                {
                    var localDeduction = new PaymentDeductionItem
                    {
                        LoadingDeductionItem = true,
                        KillPaymentDeductionID = deduction.KillPaymentDeductionID,
                        KillPaymentDeductionItemID = deduction.KillPaymentDeductionItemID,
                        SubTotalExclVAT = deduction.SubTotalExclVAT,
                        TotalIncVAT = deduction.TotalIncVAT,
                        Vat = deduction.Vat
                    };

                    totalDeductions.Add(localDeduction);
                }
                else
                {
                    deductionTotal.SubTotalExclVAT += deduction.SubTotalExclVAT;
                    deductionTotal.TotalIncVAT += deduction.TotalIncVAT;
                    deductionTotal.Vat += deduction.Vat;
                }
            }
            
            this.PaymentDeductionItemsTotal = new ObservableCollection<PaymentDeductionItem>(totalDeductions);
        }

        /// <summary>
        /// Allows the cattle details to be editable.
        /// </summary>
        private void OpenSaleDetails()
        {
            if (this.StockDetails != null)
            {
                foreach (var stockDetail in this.StockDetails)
                {
                    stockDetail.LoadingStockDetail = false;
                }
            }

            if (this.Sale != null && this.Sale.StockDetails != null)
            {
                foreach (var stockDetail in this.Sale.StockDetails)
                {
                    stockDetail.LoadingStockDetail = false;
                }
            }

            if (this.PaymentDeductionItems != null)
            {
                foreach (var paymentDeductionItem in this.PaymentDeductionItems)
                {
                    paymentDeductionItem.LoadingDeductionItem = false;
                }
            }
        }

        /// <summary>
        /// Creates or updates payment proposal.
        /// </summary>
        /// <param name="carcasses">The carcasses to add to the payment.</param>
        private void CreateOrUpdateProposal(IList<StockDetail> carcasses)
        {
            try
            {
                if (this.SelectedPartner == null)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoSupplierSelected);
                    return;
                }

                if (this.Sale == null)
                {
                    this.Sale = new Sale();
                }

                if (this.StockDetails == null)
                {
                    this.StockDetails = new ObservableCollection<StockDetail>();
                }

                foreach (var stockDetail in carcasses)
                {
                    this.StockDetails.Add(stockDetail);
                }

                // add the deductions.
                if (this.Sale.SaleID == 0)
                {
                    // new payment, so add the per payment deductions
                    foreach (var killPaymentDeduction in this.killPaymentPerPaymentDeductions)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                for (int i = 0; i < carcasses.Count; i++)
                {
                    foreach (var killPaymentDeduction in this.killPaymentPerAnimalDeductions)
                    {
                        var item = new PaymentDeductionItem
                        {
                            Deduction = killPaymentDeduction,
                            KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                        };

                        this.PaymentDeductionItems.Add(item);
                    }
                }

                for (int i = 0; i < carcasses.Count; i++)
                {
                    foreach (var killPaymentDeduction in this.killPaymentLinkedDeductions)
                    {
                        if (killPaymentDeduction.NouApplyMethodName.CompareIgnoringCase(ApplyMethod.LinkedToAttribute)
                            && this.StockDetails.ElementAt(i).Clipped == true)
                        {
                            var item = new PaymentDeductionItem
                            {
                                Deduction = killPaymentDeduction,
                                KillPaymentDeductionID = killPaymentDeduction.KillPaymentDeductionID
                            };

                            this.PaymentDeductionItems.Add(item);
                        }
                    }
                }

                this.CalculateTotals();

                if (this.Sale.SaleID > 0)
                {
                    // existing document
                    this.Sale.StockDetails = this.StockDetails;
                    this.UpdatePayment(false);
                    return;
                }

                this.Sale.Customer = this.SelectedPartner;
                this.Sale.DocumentDate = this.DocumentDate;
                this.Sale.CreationDate = DateTime.Now;
                this.Sale.Remarks = this.Remarks;
                this.Sale.SubTotalExVAT = this.SubTotal;
                this.Sale.GrandTotalIncVAT = this.Total;
                this.Sale.DeductionsIncVAT = this.DeductionsTotal;
                this.Sale.DeductionsVAT = this.DeductionsVAT;
                this.Sale.SaleDetails = new List<SaleDetail> { new SaleDetail { StockDetails = carcasses } };
                this.Sale.PaymentDeductionItems = this.PaymentDeductionItems;

                var newSale = this.DataManager.AddNewPaymentProposal(this.Sale);
                var newSaleId = newSale.Item1;
                if (newSaleId > 0)
                {
                    var details = this.StockDetails;
                    this.Sale.SaleID = newSaleId;
                    this.Sale = this.DataManager.GetPaymentById(newSaleId);
                    this.StockDetails = new ObservableCollection<StockDetail>(details);
                    SystemMessage.Write(MessageType.Priority, Message.PaymentProposalCreated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PaymentProposalNotCreated);
                }
            }
            finally
            {
                this.OpenSaleDetails();
            }
        }

        /// <summary>
        /// Prints a cheque.
        /// </summary>
        /// <param name="paymentId">The payment id.</param>
        private bool PrintCheque(int paymentId)
        {
            // TODO create cheque report
            var chequeNo = this.DataManager.UpdatePaymentChequeNumber(paymentId);
            if (chequeNo > 0)
            {
                //var message = string.Format(Message.ChequePrinted, chequeNo);
                //SystemMessage.Write(MessageType.Priority, message);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Creates a deep copy deduction.
        /// </summary>
        /// <param name="deduction">The deduction to copy.</param>
        /// <returns>A deep copy deduction.</returns>
        private PaymentDeduction CreateDeepCopyDeduction(PaymentDeduction deduction)
        {
            return new PaymentDeduction
            {
                KillPaymentDeductionID = deduction.KillPaymentDeductionID,
                Name = deduction.Name,
                NouApplyMethod = deduction.NouApplyMethod,
                NouApplyMethodID = deduction.NouApplyMethodID,
                VatCodeID = deduction.VatCodeID,
                Vat = deduction.Vat,
                CategoryID = deduction.CategoryID,
                NouKillTypeID = deduction.NouKillTypeID,
                Price = deduction.Price,
                AllowPriceEdit = deduction.AllowPriceEdit,
                AllowZeroPrice = deduction.AllowZeroPrice,
                Deleted = deduction.Deleted
            };
        }

        #endregion
    }
}
