﻿// -----------------------------------------------------------------------
// <copyright file="PriceListDetailViewModel .cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Pricing
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class PriceListDetailViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The products.
        /// </summary>
        private ObservableCollection<InventoryItem> products;

        /// <summary>
        /// Flag, as to whether an update is in progress or not.
        /// </summary>
        private bool updateInProgress;

        /// <summary>
        /// The selected price master reference.
        /// </summary>
        private PriceMaster selectedPriceMaster;

        /// <summary>
        /// The selected price master reference.
        /// </summary>
        private PriceMaster selectedPriceList;

        /// <summary>
        /// The price list details reference.
        /// </summary>
        private ObservableCollection<PriceDetail> priceListDetails;

        /// <summary>
        /// The selected details to add or edit.
        /// </summary>
        private HashSet<PriceDetail> detailsToUpdate = new HashSet<PriceDetail>();

        /// <summary>
        /// The selected price list details reference.
        /// </summary>
        private PriceDetail selectedPriceListDetail;

        /// <summary>
        /// Flag that determines if all products are to be added to the list.
        /// </summary>
        private bool selectAllProducts;

        /// <summary>
        /// The asssociated customer groups.
        /// </summary>
        private IList<App_GetPriceListCustomerGroups_Result> priceListCustomerGroups;

        /// <summary>
        /// Flag that determines if user specified products are to be added to the list.
        /// </summary>
        private bool specifyProducts;

        /// <summary>
        /// Flag that determines if the group prices.
        /// </summary>
        private bool setGroupPrices;

        /// <summary>
        /// Refresh the lists flag.
        /// </summary>
        private bool dontRefresh;

        /// <summary>
        /// Amount to increase group price by.
        /// </summary>
        private string increaseAmount;

        /// <summary>
        /// Amount to decrease group price by.
        /// </summary>
        private string decreaseAmount;

        /// <summary>
        /// Amount to increase group price by.
        /// </summary>
        private string increaseAmountPercentage;

        /// <summary>
        /// Amount to decrease group price by.
        /// </summary>
        private string decreaseAmountPercentage;

        /// <summary>
        /// Amount to set the group price to.
        /// </summary>
        private string setPrice;

        /// <summary>
        /// the all prices mode flag.
        /// </summary>
        private bool allPricesMode;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the PriceListDetailViewModel class.
        /// </summary>
        public PriceListDetailViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<string>(this, Token.RefreshPriceDetails, x => this.Refresh());

            // Register to display the incoming price list.
            Messenger.Default.Register<int>(
                this,
                Token.DisplayPriceList, i =>
                {
                    this.Locator.PricingMaster.SelectedPriceMaster =
                        this.Locator.PricingMaster.LocalPriceMasters.FirstOrDefault(x => x.PriceListID == i);

                    this.GetPriceListDetails();
                });

            // Register to display the incoming price list, selecting the incoming product.
            Messenger.Default.Register<Tuple<int, int>>(
                this,
                Token.DisplayPriceList, tuple =>
                {
                    var priceListId = tuple.Item1;
                    var productId = tuple.Item2;

                    this.Locator.PricingMaster.SelectedPriceMaster =
                        this.Locator.PricingMaster.LocalPriceMasters.FirstOrDefault(x => x.PriceListID == priceListId);

                    this.GetPriceListDetails();
                    this.SelectedPriceListDetail = this.PriceListDetails.FirstOrDefault(x => x.INMasterID == productId);
                });

            // Register to highlight the incoming inventory item.
            Messenger.Default.Register<int>(this, Token.HightlightInventoryItem, i => this.SelectedPriceListDetail = this.PriceListDetails.FirstOrDefault(x => x.INMasterID == i));

            #endregion

            #region command registration

            this.RemovePriceListDetailCommand = new RelayCommand(() =>
            {
                if (this.IsFormLoaded && this.SelectedPriceListDetail != null)
                {
                    this.detailsToUpdate.Clear();
                    this.selectedPriceListDetail.Remove = true;
                    this.detailsToUpdate.Add(this.selectedPriceListDetail);
                    this.UpdatePriceListDetails();
                }
            });

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.SetControlMode(ControlMode.Update);
            }, this.CanUpdate);

            // Specify the products for the list.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            // Specify the products for the list.
            this.OnClosingCommand = new RelayCommand(this.OnClosingCommandExecute);

            #endregion

            #region instantiation

            this.PriceListDetails = new ObservableCollection<PriceDetail>();
            this.OrderMethods = new ObservableCollection<NouOrderMethod>();
            this.PriceMethods = new ObservableCollection<NouPriceMethod>();

            #endregion

            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessPricing);
            this.GetPriceMethods();
            this.GetOrderMethods();
            this.GetPriceListCustomerGroups();
            this.Products = new ObservableCollection<InventoryItem>(NouvemGlobal.InventoryItems.OrderBy(x => x.Master.Name));
            this.PriceLists = NouvemGlobal.PriceListMasters.OrderBy(x => x.CurrentPriceListName).ToList();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Get or sets the price masters.
        /// </summary>
        public IList<PriceMaster> PriceLists { get; set; }

        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        public ObservableCollection<InventoryItem> Products
        {
            get
            {
                return this.products;
            }

            set
            {
                this.products = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the amount to increase group price by.
        /// </summary>
        public string IncreaseAmount
        {
            get
            {
                return this.increaseAmount;
            }

            set
            {
                this.increaseAmount = value;
                this.RaisePropertyChanged();
     
                if (value.ToDecimal() > 0)
                {
                    this.SetPrice = "0";
                    this.DecreaseAmount = "0";
                    this.IncreaseAmountPercentage = "0";
                    this.DecreaseAmountPercentage = "0";

                    if (this.CanUpdate())
                    {
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount to decrease group price by.
        /// </summary>
        public string DecreaseAmount
        {
            get
            {
                return this.decreaseAmount;
            }

            set
            {
                this.decreaseAmount = value;
                this.RaisePropertyChanged();

                if (value.ToDecimal() > 0)
                {
                    this.IncreaseAmount = "0";
                    this.SetPrice = "0";
                    this.IncreaseAmountPercentage = "0";
                    this.DecreaseAmountPercentage = "0";

                    if (this.CanUpdate())
                    {
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount to increase group price by.
        /// </summary>
        public string IncreaseAmountPercentage
        {
            get
            {
                return this.increaseAmountPercentage;
            }

            set
            {
                this.increaseAmountPercentage = value;
                this.RaisePropertyChanged();

                if (value.ToDecimal() > 0)
                {
                    this.IncreaseAmount = "0";
                    this.DecreaseAmount = "0";
                    this.SetPrice = "0";
                    this.DecreaseAmountPercentage = "0";

                    if (this.CanUpdate())
                    {
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount to increase group price by.
        /// </summary>
        public string DecreaseAmountPercentage
        {
            get
            {
                return this.decreaseAmountPercentage;
            }

            set
            {
                this.decreaseAmountPercentage = value;
                this.RaisePropertyChanged();

                if (value.ToDecimal() > 0)
                {
                    this.IncreaseAmount = "0";
                    this.DecreaseAmount = "0";
                    this.IncreaseAmountPercentage = "0";
                    this.SetPrice = "0";

                    if (this.CanUpdate())
                    {
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount to set the group price to.
        /// </summary>
        public string SetPrice
        {
            get
            {
                return this.setPrice;
            }

            set
            {
                this.setPrice = value;
                this.RaisePropertyChanged();

                if (value.ToDecimal() > 0)
                {
                    this.IncreaseAmount = "0";
                    this.DecreaseAmount = "0";
                    this.IncreaseAmountPercentage = "0";
                    this.DecreaseAmountPercentage = "0";

                    if (this.CanUpdate())
                    {
                        this.SetControlMode(ControlMode.Update);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether all products are to be added to the list.
        /// </summary>
        public bool SelectAllOfProducts
        {
            get
            {
                return this.selectAllProducts;
            }

            set
            {
                this.selectAllProducts = value;
                this.RaisePropertyChanged();

                if (this.dontRefresh)
                {
                    return;
                }

                if (value)
                {
                    this.SelectAllProducts();
                    this.SetControlMode(ControlMode.Update);
                }
                else if (!this.specifyProducts)
                {
                    this.Refresh();
                    this.SetControlMode(ControlMode.OK);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether user specified products are to be added to the list.
        /// </summary>
        public bool SpecifyProducts
        {
            get
            {
                return this.specifyProducts;
            }

            set
            {
                this.specifyProducts = value;
                this.RaisePropertyChanged();

                if (this.dontRefresh)
                {
                    return;
                }

                if (value)
                {
                    this.SpecifyProductsForList();
                    this.SetControlMode(ControlMode.Update);
                }
                else if (!this.selectAllProducts)
                {
                    this.Refresh();
                    this.SetControlMode(ControlMode.OK);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether user specified products are to be added to the list.
        /// </summary>
        public bool SetGroupPrices
        {
            get
            {
                return this.setGroupPrices;
            }

            set
            {
                this.setGroupPrices = value;
                this.RaisePropertyChanged();

                if (this.dontRefresh)
                {
                    return;
                }

                if (value)
                {
                    this.SetControlMode(ControlMode.Update);
                }
                else
                {
                    this.SetControlMode(ControlMode.OK);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected price master.
        /// </summary>
        public PriceMaster SelectedPriceList
        {
            get
            {
                return this.selectedPriceList;
            }

            set
            {
                this.selectedPriceList = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.SetControlMode(ControlMode.Update);
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected price master.
        /// </summary>
        public PriceMaster SelectedPriceMaster
        {
            get
            {
                return this.selectedPriceMaster;
            }

            set
            {
                this.selectedPriceMaster = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected price list detail.
        /// </summary>
        public PriceDetail SelectedPriceListDetail
        {
            get
            {
                return this.selectedPriceListDetail;
            }

            set
            {
                this.selectedPriceListDetail = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded)
                {
                    this.detailsToUpdate.Add(value);
                }
            }
        }

        /// <summary>
        /// Get or sets the price list details.
        /// </summary>
        public ObservableCollection<PriceDetail> PriceListDetails
        {
            get
            {
                return this.priceListDetails;
            }

            set
            {
                this.priceListDetails = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the order methods.
        /// </summary>
        public ObservableCollection<NouOrderMethod> OrderMethods { get; set; }

        /// <summary>
        /// Get or sets the price methods.
        /// </summary>
        public ObservableCollection<NouPriceMethod> PriceMethods { get; set; }

        /// <summary>
        /// Get or sets the price methods.
        /// </summary>
        public ObservableCollection<NouPriceMethod> RecipePriceMethods { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the uom masters.
        /// </summary>
        public ICommand RemovePriceListDetailCommand { get; private set; }

        /// <summary>
        /// Gets the command to update the uom masters.
        /// </summary>
        public ICommand UpdateCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui unload.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Gets the price list details.
        /// </summary>
        public void GetAllDetails(bool getUpdates = false)
        {
            this.IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.allPricesMode = true;
                    if (!getUpdates)
                    {
                       var localDetails =
                          this.DataManager.GetPriceListDetails().OrderBy(x => x.PriceListName)
                              .ThenBy(x => x.ItemGroup)
                              .ThenBy(x => x.ItemDescription);

                        foreach (var localDetail in localDetails)
                        {
                            var group =
                                this.priceListCustomerGroups.FirstOrDefault(
                                    x => x.PriceListID == localDetail.PriceListID);

                            if (group != null)
                            {
                                localDetail.CustomerGroup = group.BPGroupName;
                            }
                        }

                        this.PriceListDetails = new ObservableCollection<PriceDetail>(localDetails);
                        localDetails = null;
                    }
                });
            }).ContinueWith(x =>
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.IsBusy = false;
                    this.IsFormLoaded = true;
                }));
            });
        }

        /// <summary>
        /// Adds the products corresponding to the products selected on the product selection screen.
        /// </summary>
        /// <param name="productIDs">The product id's of the products to be added.</param>
        public bool AddSpecifiedProdcts(HashSet<int> productIDs)
        {
            foreach (var product in this.DataManager.GetProducts().Where(x => productIDs.Contains(x.INMasterID)))
            {
                if (this.PriceListDetails.Any(x => x.INMasterID == product.INMasterID))
                {
                    //product already exists in the list
                    continue;
                }

                var basePriceListId = this.SelectedPriceMaster.BasePriceListID;
                if (basePriceListId.HasValue)
                {
                    // get the base price list product attributes
                    var baseDetail = this.DataManager.GetPriceListDetail(basePriceListId.ToInt(), product.INMasterID);

                    if (baseDetail != null)
                    {
                        product.NouOrderMethodID = baseDetail.NouOrderMethodID;
                        product.NouPriceMethodID = baseDetail.NouPriceMethodID;
                        product.ManualPriceAndOrMethod = baseDetail.ManualPriceAndOrMethod;
                        product.BasePrice = baseDetail.Price;
                        product.Price = baseDetail.Price;
                        product.ItemGroup = baseDetail.ItemGroup;
                        product.LabelPrice = baseDetail.LabelPrice;
                        product.NouPriceMethodIDLabel = baseDetail.NouPriceMethodIDLabel;
                    }
                }
                else
                {
                    product.NouOrderMethodID = this.OrderMethods.First(x => x.Name.CompareIgnoringCase(OrderMethod.QuantityOrWeight)).NouOrderMethodID;
                    product.NouPriceMethodID = this.PriceMethods.First(x => x.Name.CompareIgnoringCase(Constant.WeightByPrice)).NouPriceMethodID;
                }

                this.PriceListDetails.Add(product);
                this.detailsToUpdate.Add(product);
            }

            this.SelectedPriceListDetail = this.PriceListDetails.LastOrDefault();

            this.dontRefresh = true;
            this.SpecifyProducts = false;
            this.SelectAllOfProducts = false;
            this.dontRefresh = false;

            return true;
        }

        /// <summary>
        /// Adds a product to the price list details.
        /// </summary>
        /// <param name="priceListId">The price list id.</param>
        /// <param name="product">The product.</param>
        public void AddProductToPriceListDetail(int priceListId, INMaster product, decimal price = 0m)
        {
            this.SelectedPriceMaster = NouvemGlobal.PriceListMasters.FirstOrDefault(x => x.PriceListID == priceListId);
            var localPriceDetail = new PriceDetail
            {
                INMasterID = product.INMasterID,
                ItemCode = product.Code,
                ItemDescription = product.Name,
                Price = price,
                NouOrderMethodID = this.OrderMethods.First(x => x.Name.Equals(OrderMethod.QuantityAndWeight)).NouOrderMethodID
            };
  
            var localProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == product.INMasterID);
            if (localProduct != null && localProduct.Group != null)
            {
                localPriceDetail.ItemGroup = localProduct.Group.Name;
            }

            if (this.SelectedPriceMaster != null)
            {
                this.GetDetails();

                var basePriceListId = this.SelectedPriceMaster.BasePriceListID;
                if (basePriceListId.HasValue)
                {
                    // get the base price list product attributes
                    var baseDetail = this.DataManager.GetPriceListDetail(basePriceListId.ToInt(), product.INMasterID);

                    if (baseDetail != null)
                    {
                        localPriceDetail.NouOrderMethodID = baseDetail.NouOrderMethodID;
                        localPriceDetail.NouPriceMethodID = baseDetail.NouPriceMethodID;
                        localPriceDetail.ManualPriceAndOrMethod = baseDetail.ManualPriceAndOrMethod;
                        localPriceDetail.BasePrice = baseDetail.Price;
                        
                        //localPriceDetail.Price = baseDetail.Price;
                    }
                }

                this.PriceListDetails.Add(localPriceDetail);
                this.SelectedPriceListDetail = this.PriceListDetails.LastOrDefault();
            }
        }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:

                    if (this.updateInProgress)
                    {
                        return;
                    }

                    try
                    {
                        this.updateInProgress = true;
                        this.UpdatePriceListDetails();
                    }
                    finally
                    {
                        this.updateInProgress = false;
                        ProgressBar.Reset();
                    }
                    
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Drill down handler.
        /// </summary>
        protected override void DrillDownCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.SetTopMost);
            SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
            Messenger.Default.Send(Tuple.Create(ViewType.InventoryMaster, this.SelectedPriceListDetail.INMasterID), Token.DrillDown);
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// handles the ui load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.Refresh();
            this.IsFormLoaded = true;
            this.SetWriteAuthorisation(Authorisation.AllowUserToAccessPricing);
        }

        /// <summary>
        /// handles the ui unload event.
        /// </summary>
        private void OnClosingCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region helper

        /// <summary>
        /// Gets the price list customer groups.
        /// </summary>
        private void GetPriceListCustomerGroups()
        {
            this.priceListCustomerGroups = this.DataManager.GetPriceListCustomerGroups();
        }

        /// <summary>
        /// Validates new price list entrys.
        /// </summary>
        /// <returns>A flag, indicating a validation or not.</returns>
        private bool Validate()
        {
            var error = string.Empty;

            foreach (var x in this.detailsToUpdate.Where(x => x != null))
            {
                if (x.NouOrderMethodID == 0)
                {
                    error = Message.OrderMethodEmpty;
                }
                else if (x.NouPriceMethodID == 0)
                {
                    error = Message.PriceMethodEmpty;
                }

                if (error != string.Empty)
                {
                    this.SelectedPriceListDetail = x;
                    break;
                }
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Updates the price list details.
        /// </summary>
        private void UpdatePriceListDetails()
        {
            if (this.SelectedPriceList != null && this.selectedPriceMaster != null)
            {
                try
                {
                    if (this.PriceListDetails.Any())
                    {
                        SystemMessage.Write(MessageType.Issue, Message.PriceListNotEmptyStop);
                        return;
                    }

                    NouvemMessageBox.Show(string.Format(Message.CopyPriceLIstDetailsPrompt,
                        this.SelectedPriceList.CurrentPriceListName, this.selectedPriceMaster.CurrentPriceListName), NouvemMessageBoxButtons.YesNo);
                    if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                    {
                        return;
                    }

                    if (this.DataManager.CopyPriceListDetails(this.SelectedPriceList.PriceListID,
                        this.selectedPriceMaster.PriceListID, NouvemGlobal.DeviceId.ToInt(), NouvemGlobal.UserId.ToInt()))
                    {
                        SystemMessage.Write(MessageType.Priority, Message.PriceListUpdated);
                        this.Refresh(true);
                        this.Locator.PricingMaster.Refresh();

                        this.SetGroupPrices = false;
                        Messenger.Default.Send(Token.Message, Token.PriceListsUpdated);
                    }
                    else
                    {
                        SystemMessage.Write(MessageType.Issue, Message.PriceListNotUpdated);
                    }
                }
                finally
                {
                    this.SelectedPriceList = null;
                }

                return;
            }

            #region validation

            if (!this.Validate())
            {
                return;
            }

            #endregion

            NouvemMessageBox.Show(Message.UpdatePricesPrompt, NouvemMessageBoxButtons.YesNo);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }
     
            // associate the selected price list with all the details records.
            this.AddPriceListToDetails();

            foreach (var detail in this.priceListDetails.Where(x => x != null && x.GroupPriceChange))
            {
                if (this.IncreaseAmount.ToDecimal() > 0)
                {
                    detail.Price += this.IncreaseAmount.ToDecimal();
                }
                else if (this.DecreaseAmount.ToDecimal() > 0)
                {
                    detail.Price -= this.DecreaseAmount.ToDecimal();
                    if (detail.Price < 0)
                    {
                        detail.Price = 0;
                    }
                }
                else if (this.IncreaseAmountPercentage.ToDecimal() > 0)
                {
                    var convertedDiscount = 100M + this.IncreaseAmountPercentage.ToDecimal();
                    detail.Price = Math.Round(detail.Price / 100 * convertedDiscount, 2);
                }
                else if (this.DecreaseAmountPercentage.ToDecimal() > 0)
                {
                    var convertedDiscount = 100M - this.DecreaseAmountPercentage.ToDecimal();
                    detail.Price = Math.Round(detail.Price / 100 * convertedDiscount, 2);
                }
                else 
                {
                    detail.Price = this.SetPrice.ToDecimal();
                }

                var remove = this.detailsToUpdate.FirstOrDefault(x => x != null && x.PriceListDetailID == detail.PriceListDetailID);
                if (remove != null)
                {
                    this.detailsToUpdate.Remove(remove);
                }

                this.detailsToUpdate.Add(detail);
            }

            try
            {
                var localDetails = this.detailsToUpdate.Where(x => x != null).ToList();

                if (this.DataManager.AddOrUpdatePriceListDetails(localDetails, 0))
                {
                    SystemMessage.Write(MessageType.Priority, Message.PriceListUpdated);
                    Messenger.Default.Send(localDetails, Token.PriceListsUpdated);

                    this.Refresh(true);
                    this.Locator.PricingMaster.Refresh();

                    this.SetGroupPrices = false;
                    Messenger.Default.Send(Token.Message, Token.PriceListsUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PriceListNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.PriceListNotUpdated);
            }
            finally
            {
                ProgressBar.Reset();
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            this.IsFormLoaded = false;
            this.PriceListDetails = null;
            ViewModelLocator.ClearPriceListDetail();
            Messenger.Default.Send(Token.Message, Token.ClosePriceListDetailWindow);
        }

        /// <summary>
        /// Gets the application order methods.
        /// </summary>
        private void GetOrderMethods()
        {
            this.OrderMethods = new ObservableCollection<NouOrderMethod>(this.DataManager.GetOrderMethods());
        }

        /// <summary>
        /// Gets the application order methods.
        /// </summary>
        private void GetPriceMethods()
        {
            var methods = this.DataManager.GetPriceMethods();
            this.PriceMethods = new ObservableCollection<NouPriceMethod>(methods.Where(x => x.Number != 4));
            this.RecipePriceMethods = new ObservableCollection<NouPriceMethod>(methods);
        }

        /// <summary>
        /// Gets the application price list details.
        /// </summary>
        private void GetPriceListDetails()
        {
            if (this.Locator.PricingMaster == null || this.Locator.PricingMaster.SelectedPriceMaster == null)
            {
                return;
            }

            this.SelectedPriceMaster = this.Locator.PricingMaster.SelectedPriceMaster;
            this.GetDetails();
        }

        /// <summary>
        /// Gets the price list details.
        /// </summary>
        private void GetDetails()
        {
            var localUpdatedDetails = this.DataManager.GetUpdatedPriceListDetails(this.selectedPriceMaster.PriceListID).OrderBy(x => x.ItemDescription);
            this.PriceListDetails = new ObservableCollection<PriceDetail>(localUpdatedDetails);
        }

        /// <summary>
        /// Refreshes the application price list details.
        /// </summary>
        private void Refresh(bool getAllUpdates = false)
        {
            this.detailsToUpdate = new HashSet<PriceDetail>();

            if (this.allPricesMode)
            {
                this.GetAllDetails(getAllUpdates);
            }
            else
            {
                this.GetPriceListDetails();
            }

            this.IncreaseAmount = "0";
            this.DecreaseAmount = "0";
            this.IncreaseAmountPercentage = "0";
            this.DecreaseAmountPercentage = "0";
            this.SetPrice = "0";
            this.SetGroupPrices = false;
        }

        /// <summary>
        /// Adds all the products to the list.
        /// </summary>
        private void SelectAllProducts()
        {
            foreach (var product in this.DataManager.GetProducts())
            {
                if (this.PriceListDetails.Any(x => x.INMasterID == product.INMasterID))
                {
                    // product is already on the list.
                    continue;
                }

                product.NouOrderMethodID = this.OrderMethods.First(x => x.Name.CompareIgnoringCase(OrderMethod.QuantityOrWeight)).NouOrderMethodID;
                product.NouPriceMethodID = this.PriceMethods.First(x => x.Name.CompareIgnoringCase(Constant.WeightByPrice)).NouPriceMethodID;
                this.PriceListDetails.Add(product);
                this.detailsToUpdate.Add(product);
            }
        }

        /// <summary>
        /// Opens the products selection view.
        /// </summary>
        private void SpecifyProductsForList()
        {
            // open the product selection window.
            Messenger.Default.Send(ViewType.ProductSelection);
            Messenger.Default.Send(Token.Message, Token.SetTopMost);
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        /// <summary>
        /// Adds the price list id to the price list detail records (new records won't have this association).
        /// </summary>
        private void AddPriceListToDetails()
        {
            if (this.selectedPriceMaster == null)
            {
                return;
            }

            foreach (var detail in this.PriceListDetails)
            {
                detail.PriceListID = this.selectedPriceMaster.PriceListID;
            }
        }

        #endregion

        #endregion
    }
}


