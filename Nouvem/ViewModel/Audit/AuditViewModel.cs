﻿// -----------------------------------------------------------------------
// <copyright file="ContainerViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace Nouvem.ViewModel.Audit
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class AuditViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The data to filter by.
        /// </summary>
        private string dataFilter;

        /// <summary>
        /// The audit data.
        /// </summary>
        private ObservableCollection<Audit> auditData;

        /// <summary>
        /// The audit data snapshot values.
        /// </summary>
        private IList<Audit> auditDataSnapshot;

        /// <summary>
        /// The  selected audit data.
        /// </summary>
        private Audit selectedAuditData;

        /// <summary>
        /// The incoming audit filter data.
        /// </summary>
        private Tuple<string, int> data;

        /// <summary>
        /// The selected audit transaction changes.
        /// </summary>
        private string dataChanges;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the AuditViewModel class.
        /// </summary>
        public AuditViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<Tuple<string, int>>(this, Token.AuditDataReceived, t =>
            {
                NouvemGlobal.GetAuditData();
                this.data = t;
                this.FilterAuditData();
            });

            #endregion

            #region command

            this.OnUnloadedCommand = new GalaSoft.MvvmLight.CommandWpf.RelayCommand(ViewModelLocator.ClearAudit);

            #endregion

            #region instantiation

            this.AuditData = new ObservableCollection<Audit>();
            this.auditDataSnapshot = new List<Audit>();

            #endregion
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the selected audit data changes.
        /// </summary>
        public string DataChanges
        {
            get
            {
                return this.dataChanges;
            }

            set
            {
                this.dataChanges = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the audit data.
        /// </summary>
        public ObservableCollection<Audit> AuditData
        {
            get
            {
                return this.auditData;
            }

            set
            {
                this.auditData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Get or sets the selected audit data.
        /// </summary>
        public Audit SelectedAuditData
        {
            get
            {
                return this.selectedAuditData;
            }

            set
            {
                this.selectedAuditData = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.HandleSelectedAuditData();
                }
            }
        }

        /// <summary>
        /// Get or sets the data to filter by.
        /// </summary>
        public string DataFilter
        {
            get
            {
                return this.dataFilter;
            }

            set
            {
                this.dataFilter = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    this.FilterCurrentAuditData();
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the load event.
        /// </summary>
        public ICommand OnLoadingCommand{ get; set; }

        /// <summary>
        /// Gets the command to handle the unload event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.Close();
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearAudit();
            Messenger.Default.Send(Token.Message, Token.CloseAuditWindow);
        }

        /// <summary>
        /// Gets the filtered audit data.
        /// </summary>
        private void FilterAuditData()
        {
            var tableName = this.data.Item1;
            var tableID = this.data.Item2;

            this.AuditData = new ObservableCollection<Audit>(NouvemGlobal.AuditData.Where(x => x.TableName.Trim().Equals(tableName) && x.TableID == tableID));
            this.auditDataSnapshot = new List<Audit>(this.auditData);
        }

        /// <summary>
        /// Gets the filtered audit data.
        /// </summary>
        private void FilterCurrentAuditData()
        {
            try
            {
                this.DataChanges = string.Empty;
                this.AuditData = new ObservableCollection<Audit>(this.auditDataSnapshot);

                if (this.dataFilter == string.Empty)
                {
                    // filter is blank, so revert to original entry data.
                    return;
                }

                this.AuditData.Clear();
                foreach (var audit in this.auditDataSnapshot)
                {
                    if (string.IsNullOrEmpty(audit.DataChanges))
                    {
                        continue;
                    }

                    if (XmlHelper.ParseXml(audit.DataChanges, this.dataFilter))
                    {
                        this.AuditData.Add(audit);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException)); 
            }
        }

        /// <summary>
        /// Handles the selection of an audit trail data.
        /// </summary>
        private void HandleSelectedAuditData()
        {
            // format, and indent the xml
            var formattedXml = XmlHelper.FormatXml(this.selectedAuditData.DataChanges);
            this.DataChanges = formattedXml;
        }

        #endregion
    }
}
