﻿// -----------------------------------------------------------------------
// <copyright file="TelesalesViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.DataLayer;
using Nouvem.Properties;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Telesales
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;
    using Nouvem.ViewModel.Sales;

    public class TelesalesViewModel : SalesViewModelBase
    {
       #region field

        /// <summary>
        /// The selected partner.
        /// </summary>
        private Model.BusinessObject.User selectedUser;

        /// <summary>
        /// The date.
        /// </summary>
        private DateTime telesalesDate;

        /// <summary>
        /// The current telesales.
        /// </summary>
        private ObservableCollection<Model.BusinessObject.BusinessPartner> filteredTelesales;

        /// <summary>
        /// The current selected telesale.
        /// </summary>
        private Model.BusinessObject.BusinessPartner selectedTelesale;

        /// <summary>
        /// The current agents.
        /// </summary>
        private IList<int?> agents = new List<int?>();

        /// <summary>
        /// The next date to call on.
        /// </summary>
        private DateTime nextCallDate;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string nextCallTime;

        /// <summary>
        /// The next time to call on.
        /// </summary>
        private string remarks;

        /// <summary>
        /// The weekly call id.
        /// </summary>
        private NouCallFrequency weeklyCall;

        /// <summary>
        /// The bi-weekly call id.
        /// </summary>
        private NouCallFrequency biWeeklyCall;

        /// <summary>
        /// The monthly call id.
        /// </summary>
        private NouCallFrequency monthlyCall;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TelesalesViewModel"/> class.
        /// </summary>
        public TelesalesViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            Messenger.Default.Register<ObservableCollection<object>>(this, Token.UpdateUsers, this.RefreshTelesales);
            Messenger.Default.Register<Tuple<Telesale,int>>(this, Token.SaleSelected, t =>
            {
                var localTelesale = t.Item1;
                this.SelectedTelesale =
                    this.FilteredTelesales.FirstOrDefault(x => x.Telesale.TelesalesID == localTelesale.TelesalesID);
                if (this.SelectedTelesale != null)
                {
                    this.SelectedTelesale.OrderNumber = t.Item2;
                    this.HandleTelesaleCompletion();
                }
            });

            #endregion

            #region command handler

            this.DrillDownCommand = new RelayCommand(() =>
            {
                if (this.SelectedTelesale == null)
                {
                    return;
                }

                SystemMessage.Write(MessageType.Priority, Message.DrillingDown);
                Messenger.Default.Send(Tuple.Create(ViewType.BPMaster, this.selectedTelesale.Details.BPMasterID), Token.DrillDown);
            });

            this.PlaceAnOrderCommand = new RelayCommand<string>(this.PlaceAnOrderCommandExecute);
            this.UpdateTelesaleCommand = new RelayCommand<string>(s =>
            {
                Messenger.Default.Send(Token.Message,Token.CloseTelesalesEndCallWindow);
                if (s.CompareIgnoringCase(Constant.Yes))
                {
                    this.UpdateSale();
                }

                Messenger.Default.Send(Token.Message, Token.FocusToButton);
            });

            #endregion
      
            this.GetUsers();
            this.GetCallFrequencies();
            this.TelesalesDate = DateTime.Today;
        }

        #endregion

        #region public interface

        #region property

        public DateTime TelesalesDate
        {
            get
            {
                return this.telesalesDate;
            }

            set
            {
                this.telesalesDate = value;
                this.RaisePropertyChanged();
                this.GetTelesales();
            }
        }

        public DateTime NextCallDate
        {
            get
            {
                return this.nextCallDate;
            }

            set
            {
                this.SetMode(value, this.nextCallDate);
                this.nextCallDate = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the next call time.
        /// </summary>
        public string NextCallTime
        {
            get
            {
                return this.nextCallTime;
            }

            set
            {
                if (value != null)
                {
                    value = value.Replace(".", ":");
                    if (!this.ValidateTime(value))
                    {
                        value = null;
                    }
                }
      
                this.nextCallTime = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the telesalec remarks.
        /// </summary>
        public string Remarks
        {
            get
            {
                return this.remarks;
            }

            set
            {
                this.remarks = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        public IList<Model.BusinessObject.User> Users { get; set; }

        /// <summary>
        /// Gets or sets the selected telesale.
        /// </summary>
        public Model.BusinessObject.BusinessPartner SelectedTelesale
        {
            get
            {
                return this.selectedTelesale;
            }

            set
            {
                this.selectedTelesale = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.HandleTelesale();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected customer.
        /// </summary>
        public ObservableCollection<Model.BusinessObject.BusinessPartner> FilteredTelesales
        {
            get
            {
                return this.filteredTelesales;
            }

            set
            {
                this.filteredTelesales = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to place an order.
        /// </summary>
        public ICommand DrillDownCommand { get; private set; }

        /// <summary>
        /// Gets the command to place an order.
        /// </summary>
        public ICommand PlaceAnOrderCommand { get; private set; }

        /// <summary>
        /// Gets the command to place an order.
        /// </summary>
        public ICommand UpdateTelesaleCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Overrides the mode selection.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (this.CurrentMode != ControlMode.OK)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Close, and clean up.
        /// </summary>
        protected override void Close()
        {
            ViewModelLocator.ClearTelesales();
            Messenger.Default.Send(Token.Message, Token.CloseTelesalesWindow);
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Add:
                   
                    break;

                case ControlMode.Update:
                    //this.UpdateContainers();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Parses the selected customers data, and gets the associated quotes.
        /// </summary>
        protected override void ParsePartner()
        {
            if (this.SelectedPartner == null)
            {
                return;
            }

            var customer =
                NouvemGlobal.CustomerPartners.FirstOrDefault(
                    x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);

            if (customer != null)
            {
                if (!ApplicationSettings.IgnoreCustomerWithNoPriceListCheck && customer.Details.PriceListID.IsNullOrZero())
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        NouvemMessageBox.Show(string.Format(Message.CustomerDoesNotHavePriceList, customer.Details.Name), touchScreen: ApplicationSettings.TouchScreenMode);
                        Messenger.Default.Send(Token.Message, Token.FocusToButton);
                    }));

                    return;
                }

                this.SelectedContact = null;
                this.SelectedSalesEmployee = null;
                this.SelectedDeliveryContact = null;
                this.SelectedInvoiceAddress = null;
                this.SelectedDeliveryAddress = null;

                if (customer.Contacts != null)
                {
                    this.Contacts = new ObservableCollection<BusinessPartnerContact>(customer.Contacts);
                    var primaryContact = customer.Contacts.FirstOrDefault(x => x.Details.PrimaryContact == true);

                    if (primaryContact != null)
                    {
                        this.SelectedContact = primaryContact;
                    }
                }

                if (customer.Addresses != null)
                {
                    this.Addresses = new ObservableCollection<BusinessPartnerAddress>(customer.Addresses.OrderBy(x => x.FullAddress));
                    var billingAddress = customer.Addresses.FirstOrDefault(x => x.Details.Billing == true);
                    var deliveryAddress = customer.Addresses.FirstOrDefault(x => x.Details.Shipping == true);

                    if (billingAddress != null)
                    {
                        this.SelectedInvoiceAddress = billingAddress;
                    }

                    if (deliveryAddress != null)
                    {
                        this.SelectedDeliveryAddress = deliveryAddress;
                    }
                }
            }

            if (this.SelectedPartner == null)
            {
                return;
            }

            if (this.DataManager.GetPartnerOnHoldStatus(this.SelectedPartner.BPMasterID))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    SystemMessage.Write(MessageType.Issue, string.Format(Message.PartnerOnHold, this.SelectedPartner.Name));
                    NouvemMessageBox.Show(string.Format(Message.PartnerOnHold, this.SelectedPartner.Name), touchScreen: ApplicationSettings.TouchScreenMode);
                    var localBusinessPartner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(
                            x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
                    if (localBusinessPartner != null)
                    {
                        localBusinessPartner.Details.OnHold = true;
                    }

                    this.SelectedPartner = null;
                }));

                return;
            }
            else
            {
                var localBusinessPartner =
                   NouvemGlobal.BusinessPartners.FirstOrDefault(
                       x => x.Details.BPMasterID == this.SelectedPartner.BPMasterID);
                if (localBusinessPartner != null)
                {
                    localBusinessPartner.Details.OnHold = false;
                }
            }

            var localPartner = this.SelectedPartner;
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                this.CustomerSales = this.DataManager.GetARDispatches(localPartner.BPMasterID, ApplicationSettings.RecentOrdersAmtToTake);
                var recentOrders = this.CustomerSales.OrderByDescending(x => x.CreationDate).ToList();

                // Get sthe sale details (used on the recent order items tab grid)
                this.CustomerSaleDetails = new ObservableCollection<SaleDetail>(recentOrders.SelectMany(x => x.SaleDetails));

                // We're binding the recent order headers to their dates.
                this.ResetOrderDates();

                if (this.Sale != null && this.Sale.SaleID == 0)
                {
                    this.SetControlMode(ControlMode.Add);
                }

                if (!recentOrders.Any())
                {
                    return;
                }

                if (recentOrders.Count > 0)
                {
                    var order = recentOrders.ElementAt(0);
                    this.Order1Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder1 = order;
                }

                if (recentOrders.Count > 1)
                {
                    var order = recentOrders.ElementAt(1);
                    this.Order2Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder2 = order;
                }

                if (recentOrders.Count > 2)
                {
                    var order = recentOrders.ElementAt(2);
                    this.Order3Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder3 = order;
                }

                if (recentOrders.Count > 3)
                {
                    var order = recentOrders.ElementAt(3);
                    this.Order4Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder4 = order;
                }

                if (recentOrders.Count > 4)
                {
                    var order = recentOrders.ElementAt(4);
                    this.Order5Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder5 = order;
                }

                if (recentOrders.Count > 5)
                {
                    var order = recentOrders.ElementAt(5);
                    this.Order6Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder6 = order;
                }

                if (recentOrders.Count > 6)
                {
                    var order = recentOrders.ElementAt(6);
                    this.Order7Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder7 = order;
                }

                if (recentOrders.Count > 7)
                {
                    var order = recentOrders.ElementAt(7);
                    this.Order8Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder8 = order;
                }

                if (recentOrders.Count > 8)
                {
                    var order = recentOrders.ElementAt(8);
                    this.Order9Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder9 = order;
                }

                if (recentOrders.Count > 9)
                {
                    var order = recentOrders.ElementAt(9);
                    this.Order10Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder10 = order;
                }

                if (recentOrders.Count > 10)
                {
                    var order = recentOrders.ElementAt(10);
                    this.Order11Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder11 = order;
                }

                if (recentOrders.Count > 11)
                {
                    var order = recentOrders.ElementAt(11);
                    this.Order12Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder12 = order;
                }

                if (recentOrders.Count > 12)
                {
                    var order = recentOrders.ElementAt(12);
                    this.Order13Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder13 = order;
                }

                if (recentOrders.Count > 13)
                {
                    var order = recentOrders.ElementAt(13);
                    this.Order14Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder14 = order;
                }

                if (recentOrders.Count > 14)
                {
                    var order = recentOrders.ElementAt(14);
                    this.Order15Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder15 = order;
                }

                if (recentOrders.Count > 15)
                {
                    var order = recentOrders.ElementAt(15);
                    this.Order16Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder16 = order;
                }

                if (recentOrders.Count > 16)
                {
                    var order = recentOrders.ElementAt(16);
                    this.Order17Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder17 = order;
                }

                if (recentOrders.Count > 17)
                {
                    var order = recentOrders.ElementAt(17);
                    this.Order18Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder18 = order;
                }

                if (recentOrders.Count > 18)
                {
                    var order = recentOrders.ElementAt(18);
                    this.Order19Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder19 = order;
                }

                if (recentOrders.Count > 19)
                {
                    var order = recentOrders.ElementAt(19);
                    this.Order20Date = order.CreationDate.ToString();
                    ProductRecentOrderData.RecentOrder20 = order;
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.ProductRecentOrderDatas =
                        new ObservableCollection<ProductRecentOrderData>(this.DataManager.GetRecentOrderData(recentOrders));

                    if (this.ProductRecentOrderDatas.Any())
                    {
                        // set focus to the recent orders grid.
                        Messenger.Default.Send(Token.Message, Token.SetRecentOrdersFocus);
                    }
                }));
            }));
        }

        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        protected override void CopyDocumentTo()
        {
            // not implemented.
        }

        protected override void CopyDocumentFrom()
        {
            // not implemented.
        }

        protected override void FindSale()
        {
            // not implemented.
        }

        protected override void FindSaleCommandExecute()
        {
            // not implemented.
        }

        protected override void DirectSearchCommandExecute()
        {
            // not implemented.
        }

        protected override void AddSale()
        {
            // not implemented.
        }

        /// <summary>
        /// Updates a telesale, closing it off for the day.
        /// </summary>
        protected override void UpdateSale()
        {
            #region validation

            if (this.SelectedTelesale == null)
            {
                return;
            }

            #endregion

            this.SelectedTelesale.Telesale.NextCallDate = this.NextCallDate;
            this.SelectedTelesale.Telesale.NextCallTime = this.NextCallTime;
            this.SelectedTelesale.Telesale.Remarks = this.Remarks;
            this.SelectedTelesale.Telesale.UserMasterID_Agent = NouvemGlobal.UserId.ToInt();
            if (this.DataManager.AddOrUpdateTelesale(this.SelectedTelesale.Telesale))
            {
                var call = new TelesaleCall
                {
                    TelesaleID = this.SelectedTelesale.Telesale.TelesalesID,
                    TelesaleDate = this.telesalesDate
                };

                this.DataManager.AddTelesaleCall(call);
            }

            this.GetTelesales();
        }

        #endregion

        #region private

        private void PlaceAnOrderCommandExecute(string placeOrder)
        {
            #region validation

            if (this.SelectedTelesale == null || this.SelectedPartner == null)
            {
                return;
            }

            #endregion

            if (placeOrder == Constant.Yes)
            {
                this.Locator.Master.ShowDispatchScreen(this.SelectedTelesale.Telesale);
                return;
            }

            // Not placing order, so close telesale off for the day.
            this.HandleTelesaleCompletion();
        }

        /// <summary>
        /// Validates a time (must be HH:MM)
        /// </summary>
        /// <param name="time">The time to validate.</param>
        /// <returns>Flag, as to valid time or not.</returns>
        private bool ValidateTime(string time)
        {
            if (!time.Is24HourTime())
            {
                SystemMessage.Write(MessageType.Issue, Message.IncorrectTimeFormat);
                NouvemMessageBox.Show(Message.IncorrectTimeFormat);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Handles the selected telesale.
        /// </summary>
        private void HandleTelesale()
        {
            var partner =
                NouvemGlobal.BusinessPartners.FirstOrDefault(
                    x => x.Details.BPMasterID == this.selectedTelesale.Details.BPMasterID);
            if (partner != null)
            {
                this.SelectedPartner = partner.Details;
            }
        }

        /// <summary>
        /// Refreshes the sales on an agent refresh.
        /// </summary>
        /// <param name="users">The agents to refresh telesales for.</param>
        private void RefreshTelesales(ObservableCollection<object> users)
        {
            this.agents.Clear();
            foreach (var user in users)
            {
                if (user is Model.BusinessObject.User)
                {
                    this.agents.Add((user as Model.BusinessObject.User).UserMaster.UserMasterID);
                }
            }

            this.GetTelesales();
        }

        /// <summary>
        /// Gets the telesales for the selected agents.
        /// </summary>
        private void GetTelesales()
        {
            var sales = this.DataManager.GetTelesales(this.agents, this.telesalesDate);
            this.FilteredTelesales = new ObservableCollection<Model.BusinessObject.BusinessPartner>(sales.OrderBy(x => x.CallTime));
            this.SelectedTelesale = this.filteredTelesales.FirstOrDefault(x => x.TelesaleStatus == Strings.Active);
            Messenger.Default.Send(Token.Message, Token.FocusToButton);
        }

        /// <summary>
        /// Gets the application users.
        /// </summary>
        private void GetUsers()
        {
            this.Users = NouvemGlobal.Users.Where(x => !string.IsNullOrWhiteSpace(x.UserMaster.FullName)).ToList();
        }

        /// <summary>
        /// Gets the call frequencies.
        /// </summary>
        private void GetCallFrequencies()
        {
            var frequencies = this.DataManager.GetCallFrequencies();
            this.weeklyCall = frequencies.FirstOrDefault(x => x.Frequency.CompareIgnoringCase(Constant.Weekly));
            this.biWeeklyCall = frequencies.FirstOrDefault(x => x.Frequency.CompareIgnoringCase(Constant.BiWeekly));
            this.monthlyCall = frequencies.FirstOrDefault(x => x.Frequency.CompareIgnoringCase(Constant.Monthly));
        }

        /// <summary>
        /// Sets the next dates and displays the screen.
        /// </summary>
        private void HandleTelesaleCompletion()
        {
            var localTelesale = this.SelectedTelesale.Telesale;
            this.Remarks = localTelesale.Remarks;
            var currentDay = this.telesalesDate.DayOfWeek;

            #region weekly

            if (localTelesale.NouCallFrequencyID == this.weeklyCall.NouCallFrequencyID)
            {
                this.NextCallDate = this.telesalesDate;
                if (currentDay == DayOfWeek.Monday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(7);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                }
                else if (currentDay == DayOfWeek.Tuesday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(7);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                }
                else if (currentDay == DayOfWeek.Wednesday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(7);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                }
                else if (currentDay == DayOfWeek.Thursday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(7);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                }
                else if (currentDay == DayOfWeek.Friday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(7);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                }
                else if (currentDay == DayOfWeek.Saturday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(7);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                }
                else if (currentDay == DayOfWeek.Sunday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(7);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                }
            }

            #endregion

            #region bi-weekly

            if (localTelesale.NouCallFrequencyID == this.biWeeklyCall.NouCallFrequencyID)
            {
                this.NextCallDate = this.telesalesDate;
                if (currentDay == DayOfWeek.Monday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(14);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                }
                else if (currentDay == DayOfWeek.Tuesday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(13);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(14);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                }
                else if (currentDay == DayOfWeek.Wednesday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(12);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(13);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(14);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                }
                else if (currentDay == DayOfWeek.Thursday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(11);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(12);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(13);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(14);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                }
                else if (currentDay == DayOfWeek.Friday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(10);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(11);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(12);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(13);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(14);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                }
                else if (currentDay == DayOfWeek.Saturday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(9);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(10);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(11);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(12);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(13);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(14);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                }
                else if (currentDay == DayOfWeek.Sunday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(8);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(9);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(10);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(11);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(12);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(13);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(14);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                }
            }

            #endregion

            #region monthly

            if (localTelesale.NouCallFrequencyID == this.monthlyCall.NouCallFrequencyID)
            {
                this.NextCallDate = this.telesalesDate;
                if (currentDay == DayOfWeek.Monday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(6);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(28);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                }
                else if (currentDay == DayOfWeek.Tuesday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(5);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(27);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(28);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                }
                else if (currentDay == DayOfWeek.Wednesday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(4);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(26);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(27);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(28);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                }
                else if (currentDay == DayOfWeek.Thursday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(3);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(25);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(26);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(27);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(28);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                }
                else if (currentDay == DayOfWeek.Friday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(2);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(24);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(25);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(26);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(27);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(28);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                }
                else if (currentDay == DayOfWeek.Saturday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeSunday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(1);
                        this.NextCallTime = localTelesale.CallTimeSunday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(23);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(24);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(25);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(26);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(27);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(28);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                }
                else if (currentDay == DayOfWeek.Sunday)
                {
                    if (!string.IsNullOrEmpty(localTelesale.CallTimeMonday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(22);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeTuesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(23);
                        this.NextCallTime = localTelesale.CallTimeTuesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeWednesday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(24);
                        this.NextCallTime = localTelesale.CallTimeWednesday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeThursday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(25);
                        this.NextCallTime = localTelesale.CallTimeThursday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeFriday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(26);
                        this.NextCallTime = localTelesale.CallTimeFriday;
                    }
                    else if (!string.IsNullOrEmpty(localTelesale.CallTimeSaturday))
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(27);
                        this.NextCallTime = localTelesale.CallTimeSaturday;
                    }
                    else
                    {
                        this.NextCallDate = this.NextCallDate.AddDays(28);
                        this.NextCallTime = localTelesale.CallTimeMonday;
                    }
                }
            }

            #endregion

            Messenger.Default.Send(ViewType.TelesalesEndCall);
        }

        #endregion
    }
}
