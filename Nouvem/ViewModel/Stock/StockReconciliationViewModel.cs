﻿// -----------------------------------------------------------------------
// <copyright file="StockReconciliationViewModel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Stock
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;

    public class StockReconciliationViewModel : NouvemViewModelBase
    {
       #region field

        //private DispatcherTimer timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(200) };

        /// <summary>
        /// The application stocktakes.
        /// </summary>
        private ObservableCollection<StockTake> stockTakes;

        /// <summary>
        /// The missing stock.
        /// </summary>
        private ObservableCollection<App_GetStockTakeData_Result> missingStock;

        /// <summary>
        /// The recovered stock.
        /// </summary>
        private ObservableCollection<App_GetStockTakeData_Result> recoveredStock;

        /// <summary>
        /// The unknown stock.
        /// </summary>
        private ObservableCollection<App_GetStockTakeData_Result> unknownStock;

        /// <summary>
        /// The accounted for stock.
        /// </summary>
        private ObservableCollection<App_GetStockTakeData_Result> accountedForStock;

        /// <summary>
        /// The selectecd stock take.
        /// </summary>
        private StockTake selectedStockTake;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="StockReconciliationViewModel"/> class.
        /// </summary>
        public StockReconciliationViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            this.OnLoadingCommand = new RelayCommand(() =>
            {
                this.GetStockTakes();
                this.IsFormLoaded = true;
            });

            this.OnClosingCommand = new RelayCommand(() => this.IsFormLoaded = false);

            #endregion

            this.HasWriteAuthorisation = true;
            this.SetControlMode(ControlMode.OK);
            //this.timer.Tick += this.TimerOnTick;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the missing stock.
        /// </summary>
        public ObservableCollection<App_GetStockTakeData_Result> MissingStock
        {
            get
            {
                return this.missingStock;
            }

            set
            {
                this.missingStock = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the recovered stock.
        /// </summary>
        public ObservableCollection<App_GetStockTakeData_Result> RecoveredStock
        {
            get
            {
                return this.recoveredStock;
            }

            set
            {
                this.recoveredStock = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the unknown stock.
        /// </summary>
        public ObservableCollection<App_GetStockTakeData_Result> UnknownStock
        {
            get
            {
                return this.unknownStock;
            }

            set
            {
                this.unknownStock = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the accounted for stock.
        /// </summary>
        public ObservableCollection<App_GetStockTakeData_Result> AccountedForStock
        {
            get
            {
                return this.accountedForStock;
            }

            set
            {
                this.accountedForStock = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the stock takes.
        /// </summary>
        public ObservableCollection<StockTake> StockTakes
        {
            get
            {
                return this.stockTakes;
            }

            set
            {
                this.stockTakes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected stock stake.
        /// </summary>
        public StockTake SelectedStockTake
        {
            get
            {
                return this.selectedStockTake;
            }

            set
            {
                this.selectedStockTake = value;
                this.RaisePropertyChanged();
                if (value != null && !value.Description.Equals(Strings.NoneSelected))
                {
                    this.HandleSelectedStockTake();
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the load event command handler.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the close event command handler.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.ReconcileStock();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Process the selected stock take.
        /// </summary>
        private void HandleSelectedStockTake()
        {
            var data = this.DataManager.GetStockTakeData(this.selectedStockTake);
            this.MissingStock = new ObservableCollection<App_GetStockTakeData_Result>(data.Item1);
            this.RecoveredStock = new ObservableCollection<App_GetStockTakeData_Result>(data.Item2);
            this.UnknownStock = new ObservableCollection<App_GetStockTakeData_Result>(data.Item3);
            this.AccountedForStock = new ObservableCollection<App_GetStockTakeData_Result>(data.Item4);

            this.SetControlMode(ControlMode.Update);
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            //if (this.timer != null)
            //{
            //    this.timer.Stop();
            //}

            this.MissingStock = null;
            this.RecoveredStock = null;
            this.UnknownStock = null;
            this.AccountedForStock = null;
            ViewModelLocator.ClearStockReconciliation();
            Messenger.Default.Send(Token.Message, Token.CloseStockReconciliationWindow);
        }

        /// <summary>
        /// Gets the stock takes.
        /// </summary>
        private void GetStockTakes()
        {
            this.StockTakes = new ObservableCollection<StockTake>(this.DataManager.GetStockTakes()
                .Where(take => take.NouDocStatusID == 
                    NouvemGlobal.NouDocStatusActive.NouDocStatusID || take.NouDocStatusID == NouvemGlobal.NouDocStatusInProgress.NouDocStatusID));

            this.StockTakes.Insert(0, new StockTake{Description = Strings.NoneSelected});
            this.SelectedStockTake = this.StockTakes.First();
        }

        /// <summary>
        /// Reconcile the stock.
        /// </summary>
        private async void ReconcileStock()
        {
            NouvemMessageBox.Show(Message.ReconcileStockConfirmation, NouvemMessageBoxButtons.YesNo);
            if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
            {
                return;
            }

            ProgressBar.SetUp(0, 3);
            ProgressBar.Run();
            //this.timer.Start();
            //var data = this.MissingStock.Concat(this.RecoveredStock).Concat(this.AccountedForStock).ToList();
            //this.selectedStockTake.StockTakeDetails = data;
            await System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(200);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    try
                    {
                        if (this.DataManager.ReconcileStock(this.selectedStockTake))
                        {
                            ProgressBar.Run();
                            this.GetStockTakes();
                            this.MissingStock.Clear();
                            this.RecoveredStock.Clear();
                            this.UnknownStock.Clear();
                            SystemMessage.Write(MessageType.Priority, Message.StockReconciliationComplete);
                            this.SetControlMode(ControlMode.OK);

                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                            {
                                NouvemMessageBox.Show(Message.StockReconciliationComplete);
                            }));
                        }
                        else
                        {
                            SystemMessage.Write(MessageType.Issue, Message.StockReconciliationNotComplete);
                        }
                    }
                    finally
                    {
                        ProgressBar.Reset();
                        //this.timer.Stop();
                    }
                });
            });
        }

        //private void TimerOnTick(object sender, EventArgs e)
        //{
        //    Application.Current.Dispatcher.Invoke(() =>
        //    {
        //        ProgressBar.Run();
        //    });
        //}

        #endregion
    }
}
