﻿// -----------------------------------------------------------------------
// <copyright file="DocumentNumberModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Document
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class DocumentNumberViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The document types collection.
        /// </summary>
        private ObservableCollection<DocumentNumberingType> documentTypes;

        /// <summary>
        /// The document types collection.
        /// </summary>
        private ObservableCollection<DocNumber> documentNumbers;

        /// <summary>
        /// The selected document document.
        /// </summary>
        private DocNumber selectedDocumentNumber;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentNumberViewModel"/> class.
        /// </summary>
        public DocumentNumberViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region message registration



            #endregion

            #region command handler

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() => this.SetControlMode(ControlMode.Update), this.CanUpdate);

            #endregion

            this.GetDocumentNumbers();
            this.GetNouDocumentNames();
            this.GetDocumentTypes();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the noudocument names.
        /// </summary>
        public List<NouDocumentName> NouDocumentNames { get; set; }

        /// <summary>
        /// Gets or sets the document types.
        /// </summary>
        public ObservableCollection<DocumentNumberingType> DocumentTypes
        {
            get
            {
                return this.documentTypes;
            }

            set
            {
                this.documentTypes = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected document number.
        /// </summary>
        public DocNumber SelectedDocumentNumber
        {
            get
            {
                return this.selectedDocumentNumber;
            }

            set
            {
                this.selectedDocumentNumber = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the document types.
        /// </summary>
        public ObservableCollection<DocNumber> DocumentNumbers
        {
            get
            {
                return this.documentNumbers;
            }

            set
            {
                this.documentNumbers = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the containers.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region method



        #endregion

        #endregion

        #region protected

        #region override

        /// <summary>
        /// Remove the vat line.
        /// </summary>
        protected override void RemoveItemCommandExecute()
        {
            //if (this.selectedDocumentNumber != null)
            //{
            //    this.selectedDocumentNumber.Deleted = DateTime.Now;
            //    this.UpdateDocumentNumbers();
            //    SystemMessage.Write(MessageType.Priority, Message.ItemRemoved);
            //    this.DocumentNumbers.Remove(this.selectedDocumentNumber);
            //}
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new or find partner, clearing the form.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Override the handling of the control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdateDocumentNumbers();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region method



        #endregion

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion

        #region helper

        /// <summary>
        /// Updates the document numbers.
        /// </summary>
        private void UpdateDocumentNumbers()
        {
            #region validation

            var error = string.Empty;
            foreach (var doc in this.DocumentNumbers)
            {
                if (doc.NouDocumentNameID == 0)
                {
                    error = Message.NoDocumentSelected;
                    break;
                }

                if (doc.DocumentNumberingTypeID == 0)
                {
                    error = Message.NoDocumentTypeSelected;
                    break;
                }
            }

            if (error != string.Empty)
            {
                SystemMessage.Write(MessageType.Issue, error);
                return;
            }

            #endregion

            try
            {
                if (this.DataManager.AddOrUpdateDocumentNumbers(this.DocumentNumbers))
                {
                    SystemMessage.Write(MessageType.Priority, Message.DocumentNumbersUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.DocumentNumbersNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.DocumentNumbersNotUpdated);
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearDocumentNumber();
            Messenger.Default.Send(Token.Message, Token.CloseDocumentNumberWindow);
        }

        /// <summary>
        /// Gets the Nou Document Names.
        /// </summary>
        private void GetNouDocumentNames()
        {
            this.NouDocumentNames = this.DataManager.GetNouDocumentNames().ToList();
        }

        /// <summary>
        /// Gets the Document Numbers.
        /// </summary>
        private void GetDocumentNumbers()
        {
            this.DocumentNumbers = new ObservableCollection<DocNumber>(this.DataManager.GetDocumentNumbers());
        }

        /// <summary>
        /// Gets the Nou Document Names.
        /// </summary>
        private void GetDocumentTypes()
        {
            this.DocumentTypes = new ObservableCollection<DocumentNumberingType>(this.DataManager.GetDocumentNumberingTypes());
        }

        #endregion

        #endregion
    }
}

