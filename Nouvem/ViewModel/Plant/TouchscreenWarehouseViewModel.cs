﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.BusinessObject;
using Nouvem.Model.Enum;
using Nouvem.Shared.Localisation;

namespace Nouvem.ViewModel.Plant
{
    public class TouchscreenWarehouseViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The selected location.
        /// </summary>
        private Location selectedLocation;

        /// <summary>
        /// The application locations.
        /// </summary>
        private ObservableCollection<Location> locations = new ObservableCollection<Location>();

        /// <summary>
        /// The application locations.
        /// </summary>
        private IList<Location> allLocations = new List<Location>();

        /// <summary>
        /// The current location.
        /// </summary>
        private string currentLocation;

        /// <summary>
        /// The search/filter text.
        /// </summary>
        private string searchText;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="TouchscreenWarehouseViewModel"/> class.
        /// </summary>
        public TouchscreenWarehouseViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            #endregion

            #region command handler

            // Search for an order.
            this.FindBatchCommand = new RelayCommand(() =>
            {
                //this.SearchText = string.Empty;        
                //Messenger.Default.Send(true, Token.DisplayWarehouseKeyboard);
                this.Locator.TouchscreenProductionOrders.SetModule(ViewType.StockMovement);
                this.Locator.TouchscreenProductionOrders.GetAllOrders();
                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);

                //Messenger.Default.Send(false, Token.DisplayWarehouseKeyboard);
                //this.SearchText = string.Empty;
            });

            this.MoveBackCommand = new RelayCommand(this.MoveBackCommandExecute);
            this.LocationSelectedCommand = new RelayCommand(this.LocationSelectedCommandExecute);
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);
            this.RefreshCommand = new RelayCommand(this.RefreshCommandExecute);

            #endregion

            this.GetStockWarehouses();
            this.CurrentLocation = Strings.AllLocations;
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the orders search/filter text.
        /// </summary>
        public string SearchText
        {
            get
            {
                return this.searchText;
            }

            set
            {
                this.searchText = value;
                this.RaisePropertyChanged();

                if (!this.IsFormLoaded)
                {
                    return;
                }

                if (string.IsNullOrEmpty(value))
                {
                    this.Refresh();
                }
                else
                {
                    var prOrder = this.DataManager.GetPROrderByReference(value);
                    if (prOrder != null)
                    {
                        var localOrders = new List<ProductionData> { prOrder };
                        this.Locator.TouchscreenProductionOrders.SetModule(ViewType.StockMovement);
                        Messenger.Default.Send(Token.Message, Token.CreateTouchscreenProductionOrder);
                        this.Locator.TouchscreenProductionOrders.Orders = new ObservableCollection<ProductionData>(localOrders);
                        Messenger.Default.Send(false, Token.DisplayWarehouseKeyboard);
                        this.SearchText = string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are using locations for batch selectiuon.
        /// </summary>
        public bool UsingLocationsForBatches { get; set; }

        /// <summary>
        /// Gets or sets the current location.
        /// </summary>
        public string CurrentLocation
        {
            get
            {
                return this.currentLocation;
            }

            set
            {
                this.currentLocation = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the application locations.
        /// </summary>
        public ObservableCollection<Location> Locations
        {
            get
            {
                return this.locations;
            }

            set
            {
                this.locations = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected location.
        /// </summary>
        public Location SelectedLocation
        {
            get
            {
                return this.selectedLocation;
            }

            set
            {
                this.selectedLocation = value;
                this.RaisePropertyChanged();

                if (value != null)
                {
                    if (!value.WarehouseLocationId.HasValue)
                    {
                        this.CurrentLocation = value.Name;
                    }
                }
                else
                {
                    this.CurrentLocation = Strings.AllLocations;
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the order search.
        /// </summary>
        public ICommand FindBatchCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle a location selection.
        /// </summary>
        public ICommand LocationSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the main screen.
        /// </summary>
        public ICommand RefreshCommand { get; private set; }

        #endregion

        #region

        /// <summary>
        /// Calls the load event handler.
        /// </summary>
        public void OnEntry()
        {
            this.OnLoadingCommandExecute();
        }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            // not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Refreshes the ui.
        /// </summary>
        private void RefreshCommandExecute()
        {
            this.Refresh();
            SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
        }

        /// <summary>
        /// Handles the load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;
            this.Refresh();
        }

        /// <summary>
        /// Handle a location selection.
        /// </summary>
        private void LocationSelectedCommandExecute()
        {
            #region validation

            if (this.SelectedLocation == null)
            {
                return;
            }

            #endregion

            if (!this.selectedLocation.WarehouseLocationId.HasValue && !this.UsingLocationsForBatches)
            {
                // It's not a sub warehouse, so check for for sub warehouses, and display if any.
                var localName = this.selectedLocation.Name;
                this.selectedLocation = this.DataManager.GetStockWarehouse(this.selectedLocation.WarehouseId);
                if (this.SelectedLocation.SubLocations.Any())
                {
                    var subLocations = this.SelectedLocation.SubLocations.OrderBy(x => x.Name);
                    this.Locations.Clear();
                    foreach (var subLocation in subLocations)
                    {
                        this.Locations.Add(subLocation);
                    }

                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                    {
                        this.CurrentLocation = localName;
                    }));

                    return;
                }
            }

            this.SelectedLocation.UsingLocationsForBatches = this.UsingLocationsForBatches;
            Messenger.Default.Send(this.SelectedLocation, Token.LocationSelected);
            this.Close();
        }

        /// <summary>
        /// Move back to the previous module.
        /// </summary>
        private void MoveBackCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region helper

        /// <summary>
        /// Refreshes the ui.
        /// </summary>
        private void Refresh()
        {
            this.Locations.Clear();
            foreach (var allLocation in this.allLocations)
            {
                this.Locations.Add(allLocation);
            }

            this.CurrentLocation = Strings.AllLocations;
        }

        /// <summary>
        /// Gets the application warehouses and sub warehouses.
        /// </summary>
        private void GetStockWarehouses()
        {
            this.allLocations = this.DataManager.GetStockWarehouses().OrderBy(x => x.Name).ToList();
        }

        /// <summary>
        /// Close the ui.
        /// </summary>
        private void Close()
        {
            this.UsingLocationsForBatches = false;
            Messenger.Default.Send(true, Token.CloseTouchscreenWarehouseWindow);
            Messenger.Default.Send(false, Token.DisplayWarehouseKeyboard);
        }

        #endregion

        #endregion
    }
}
