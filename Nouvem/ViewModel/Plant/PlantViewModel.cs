﻿// -----------------------------------------------------------------------
// <copyright file="PlantViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;

namespace Nouvem.ViewModel.Plant
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class PlantViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The plants to update.
        /// </summary>
        private HashSet<ViewPlant> plantsToUpdate = new HashSet<ViewPlant>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the PlantViewModel class.
        /// </summary>
        public PlantViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            //this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);
            this.HasWriteAuthorisation = true; // TODO Investigate

            #region command registration

            // Change the control mode to update
            this.UpdateCommand = new RelayCommand(() =>
            {
                this.SetControlMode(ControlMode.Update);

                if (this.SelectedPlant != null)
                {
                    this.plantsToUpdate.Add(this.SelectedPlant);
                }

            }, this.CanUpdate);

            #endregion

            #region instantiation

            this.Plants = new ObservableCollection<ViewPlant>();
            this.CountryMasters = new ObservableCollection<Country>();

            #endregion

            this.GetPlants();
            this.GetCountryMasters();
        }

        #endregion

        #region property

        /// <summary>
        /// Get or sets the selected plant.
        /// </summary>
        public ViewPlant SelectedPlant { get; set; }

        /// <summary>
        /// Get or sets the plants.
        /// </summary>
        public ObservableCollection<ViewPlant> Plants { get; set; }

        /// <summary>
        /// Get or sets the country Masters.
        /// </summary>
        public ObservableCollection<Country> CountryMasters { get; set; }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to update the country masters.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                    this.UpdatePlants();
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Updates the plants group.
        /// </summary>
        private void UpdatePlants()
        {
            if (!this.plantsToUpdate.Any())
            {
                return;
            }

            try
            {
                if (this.DataManager.AddOrUpdatePlants(this.plantsToUpdate.ToList()))
                {
                    SystemMessage.Write(MessageType.Priority, Message.PlantUpdated);
                }
                else
                {
                    SystemMessage.Write(MessageType.Issue, Message.PlantNotUpdated);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
                SystemMessage.Write(MessageType.Issue, Message.PlantNotUpdated);
            }
            finally
            {
                this.plantsToUpdate.Clear();
            }

            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearPlant();
            Messenger.Default.Send(Token.Message, Token.ClosePlantWindow);
        }

        /// <summary>
        /// Gets the plants.
        /// </summary>
        private void GetPlants()
        {
            this.Plants.Clear();
            foreach (var plant in this.DataManager.GetPlants())
            {
                this.Plants.Add(plant);
            }
        }

        /// <summary>
        /// Gets the application countries.
        /// </summary>
        private void GetCountryMasters()
        {
            this.CountryMasters.Clear();
            foreach (var country in this.DataManager.GetCountryMaster())
            {
                this.CountryMasters.Add(country);
            }
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}
