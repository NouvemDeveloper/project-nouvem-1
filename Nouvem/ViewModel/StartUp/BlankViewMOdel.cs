﻿// -----------------------------------------------------------------------
// <copyright file="BlankViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.StartUp
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Class behind the blank view when windows in focus.
    /// </summary>
    public class BlankViewModel : NouvemViewModelBase
    {
        protected override void ControlSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }
    }
}
