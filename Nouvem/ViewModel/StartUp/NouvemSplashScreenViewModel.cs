﻿// <copyright file="NouvemSplashScreenViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Nouvem.ViewModel.StartUp
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Threading;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class NouvemSplashScreenViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// value corresponding to the progress bar
        /// </summary>
        private int progressBarValue;

        /// <summary>
        /// The form load timer reference.
        /// </summary>
        private DispatcherTimer loadTimer;

        private IList<string> assemblys = new List<string>();

        #endregion

        private int count = 0;

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="NouvemViewModelBase"/> class.
        /// </summary>
        public NouvemSplashScreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command registration
           
            // Command to handle the window load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);
            this.OnClosingCommand = new RelayCommand(() =>
            {
                ViewModelLocator.ClearNouvemSplashScreen();
                if (this.loadTimer != null)
                {
                    this.loadTimer.Stop();
                    this.loadTimer.Tick -= this.LoadTimerOnTick;
                }
            });

            #endregion
        }

        #endregion

        #region public interface

        #region property

        private string traceMessage;
        /// <summary>
        /// Gets or sets the progress bar value.
        /// </summary>
        public string TraceMessage
        {
            get
            {
                return this.traceMessage;
            }

            set
            {
                this.traceMessage = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the progress bar value.
        /// </summary>
        public int ProgressBarValue
        {
            get
            {
                return this.progressBarValue;
            }

            set
            {
                this.progressBarValue = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the screen load.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the screen load.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        #endregion

        #endregion

        #region private

        #region command handler

        /// <summary>
        /// Handler for the window load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Send, new Action(() =>
            {
                this.SetTimer();
            }));
        }
        
        /// <summary>
        /// Method that increments the progress bar, before calling the master ui.
        /// </summary>
        private void SetTimer()
        {
            this.loadTimer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(150) };
            this.loadTimer.Start();
            this.loadTimer.Tick += this.LoadTimerOnTick;
        }

        private void LoadTimerOnTick(object o, EventArgs eventArgs)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();
            if (this.count == assemblies.Count)
            {
                this.loadTimer.Stop();
                return;
            }

            Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Send,
                new Action(() =>
                {
                    try
                    {
                        this.TraceMessage = assemblies.ElementAt(this.count).FullName;
                        this.count++;
                    }
                    catch (Exception)
                    {
                        // no need to do anything.
                    }
                }));
        }

        #endregion

        #endregion

        protected override void ControlSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            //throw new NotImplementedException();
        }
    }
}
