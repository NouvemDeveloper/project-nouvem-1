﻿// -----------------------------------------------------------------------
// <copyright file="EntryViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.StartUp
{
    public class EntryViewModel : NouvemViewModelBase
    {
        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EntryViewModel"/> class.
        /// </summary>
        public EntryViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }
        }

        #endregion

        protected override void ControlSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new System.NotImplementedException();
        }
    }
}
