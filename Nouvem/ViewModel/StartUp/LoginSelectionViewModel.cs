﻿// -----------------------------------------------------------------------
// <copyright file="LoginViewSelectionModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows.Threading;

namespace Nouvem.ViewModel.StartUp
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.BusinessLogic;
    using Nouvem.Global;
    using Nouvem.Shared;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class LoginSelectionViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The user password.
        /// </summary>
        private string userPassword;

        /// <summary>
        /// The user id
        /// </summary>
        private string userId;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;
       
        /// <summary>
        /// The selected database name.
        /// </summary>
        private Database selectedDatabase;

        /// <summary>
        /// The selected server name.
        /// </summary>
        private Server selectedServer;

        /// <summary>
        /// The collection of databases attached to the current server.
        /// </summary>
        private ObservableCollection<Database> databaseNames;

        /// <summary>
        /// The collection of servers.
        /// </summary>
        private ObservableCollection<Server> servers;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginSelectionViewModel"/> class.
        /// </summary>
        public LoginSelectionViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration
           
            // receives the incoming password.
            Messenger.Default.Register<string>(
                this,
                Token.UserPassword,
                x =>
                    {
                        this.userPassword = x;
                        this.ErrorMessage = string.Empty;
                    });

            #endregion

            #region command registration
           
            // Handle the user verification.
            this.VerifyUserCommand = new RelayCommand(this.VerifyLogin);

            // Handle the form load.
            this.OnLoadingCommand = new RelayCommand(this.OnLoadingCommandExecute);

            #endregion
          
            this.GetServers();
            this.GetDatabases();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the databases attached to the selected server.
        /// </summary>
        public ObservableCollection<Database> DatabaseNames
        {
            get
            {
                return this.databaseNames;
            }

            set
            {
                this.databaseNames = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the servers collection.
        /// </summary>
        public ObservableCollection<Server> Servers
        {
            get
            {
                return this.servers;
            }

            set
            {
                this.servers = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected database.
        /// </summary>
        public Database SelectedDatabase
        {
            get
            {
                return this.selectedDatabase;
            }

            set
            {
                this.selectedDatabase = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded && value != null)
                {
                    this.HandleDatabaseChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the selected server.
        /// </summary>
        public Server SelectedServer
        {
            get
            {
                return this.selectedServer;
            }

            set
            {
                this.selectedServer = value;
                this.RaisePropertyChanged();

                if (this.IsFormLoaded && value != null)
                {
                    this.HandleServerChange();
                }
            }
        }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId
        {
            get
            {
                return this.userId;
            }

            set
            {
                this.userId = value;
                this.RaisePropertyChanged();
                this.ErrorMessage = string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }

            set
            {
                this.errorMessage = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to choose a database company.
        /// </summary>
        public ICommand ChooseCompanyCommand { get; private set; }

        /// <summary>
        /// Gets the command to verify the user.
        /// </summary>
        public ICommand VerifyUserCommand { get; private set; }

        /// <summary>
        /// Gets the loading event command.
        /// </summary>
        public ICommand OnLoadingCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Verify the user credentials.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            this.SetControlMode(ControlMode.OK);
            this.VerifyLogin();
        }

        /// <summary>
        /// Close the application
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            if (!ApplicationSettings.UserLoggedIn)
            {
                Application.Current.Shutdown();
                Process.GetCurrentProcess().Kill();
            }

            Messenger.Default.Send(Token.Message, Token.CloseLoginSelection);
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the databases on the newly selected server.
        /// </summary>
        private void HandleServerChange()
        {
            ApplicationSettings.ServerName = this.SelectedServer.ServerName;
            ApplicationSettings.DatabaseName = this.SelectedServer.DefaultDatabaseName;
            ApplicationSettings.UserId = this.SelectedServer.UserId;
            ApplicationSettings.UserPassword = this.SelectedServer.Password;

            // Reset the connection string
            EntityConnection.CreateConnectionString();

            if (this.DatabaseNames != null)
            {
                this.DatabaseNames.Clear();
                this.GetDatabases();
            }
        }

        /// <summary>
        /// Handles a db change by resetting the database connection string.
        /// </summary>
        private void HandleDatabaseChange()
        {
            ApplicationSettings.DatabaseName = this.SelectedDatabase.DatabaseName;
            ApplicationSettings.CompanyName = this.SelectedDatabase.CompanyName;

            // Reset the connection string
            EntityConnection.CreateConnectionString();
        }

        /// <summary>
        /// Method that verifies a user login.
        /// </summary>
        private void VerifyLogin()
        {
            #region validation

            var error = string.Empty;
            if (string.IsNullOrWhiteSpace(this.UserId))
            {
                error = Message.UserIdEmpty;
            }
            else if (string.IsNullOrWhiteSpace(this.userPassword))
            {
                error = Message.PasswordEmpty;
            }

            if (error != string.Empty)
            {
                this.ErrorMessage = error;
                return;
            }

            #endregion

            // verify the user, returning the user details if verified.
            Tuple<string, string, string, bool, bool, bool> userDetails;
            try
            {
                userDetails = this.DataManager.VerifyLoginUser(this.userId, this.userPassword);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                return;
            }

            var userName = userDetails.Item1;
            var fullName = userDetails.Item2;
            var identifier = userDetails.Item3;
            var alreadyLoggedIn = userDetails.Item4;
            var userSuspended = userDetails.Item5;
            var changePassword = userDetails.Item6;

            if (userSuspended)
            {
                NouvemMessageBox.Show(Message.UserSuspended);
                this.Close();
                return;
            }

            if (userName != string.Empty)
            {
                ApplicationSettings.LoggedInUser = userName;
                ApplicationSettings.LoggedInUserName = fullName;
                ApplicationSettings.LoggedInUserIdentifier = identifier;
                //Settings.Default.Save();

                // get the user/device licence credentials.
                this.CheckForLicence();

                //if (alreadyLoggedIn)
                //{
                //    // user logged in somewhere else, so check if they can force a log off.
                //    if (NouvemGlobal.CheckAuthorisation(Authorisation.AllowUserToForceLogOff) == Constant.FullAccess)
                //    {
                //        // permission granted, so ask the user
                //        NouvemMessageBox.Show(Message.ForceLogOut, NouvemMessageBoxButtons.YesNo);
                //        if (NouvemMessageBox.UserSelection != UserDialogue.Yes)
                //        {
                //            // user doesn't want to force a log out so exit.
                //            this.Close();
                //            return;
                //        }

                //        this.DataManager.ForceLogOff();
                //    }
                //    else
                //    {
                //        // user does not have permission to force log out the other user, so exit.
                //        NouvemMessageBox.Show(Message.UserAlreadyLoggedIn);
                //        this.Close();
                //        return;
                //    }
                //}

                if (changePassword)
                {
                    // user needs to set their own password
                    Messenger.Default.Send(Token.Message, Token.CreateUserPasswordChange);
                    return;
                }

                if (!ApplicationSettings.UserLoggedIn)
                {
                    // user verified at start up, open application.
                    Messenger.Default.Send(userName, Token.CreateMasterView);
                }
                else
                {
                    // User already logged in, and changing log in credentials.
                    SystemMessage.Write(MessageType.Priority, Message.UserLoginChange);
                }

                this.Close();
            }
            else
            {
                this.ErrorMessage = Message.InvalidLogin;
            }
        }

        /// <summary>
        /// Method that gets and sets the databases from the selected server.
        /// </summary>
        private void GetDatabases()
        {
            var localDatabases = this.DataManager.GetDatabaseNames();
            this.DatabaseNames = new ObservableCollection<Database>();

            foreach (var localDatabase in localDatabases)
            {
                var localServer = this.servers.FirstOrDefault(x => x.DefaultDatabaseName.CompareIgnoringCase(localDatabase));
                var companyName = localServer != null ? localServer.CompanyName : localDatabase;
                this.DatabaseNames.Add(new Database { CompanyName = companyName, DatabaseName = localDatabase});
            }
          
            this.SelectedDatabase =
                this.DatabaseNames.FirstOrDefault(x => x.DatabaseName.Trim().CompareIgnoringCase(ApplicationSettings.DatabaseName.Trim()));
        }

        /// <summary>
        /// Get the local licence.
        /// </summary>
        private void CheckForLicence()
        {
            var licenceManager = LicenceManager.Instance;
            licenceManager.CheckForLicence();
        }

        /// <summary>
        /// Method that gets and sets the servers.
        /// </summary>
        private void GetServers()
        {
            var localServers = this.DataManager.GetServers();
            this.Servers = new ObservableCollection<Server>(localServers);
            this.SelectedServer = this.Servers.FirstOrDefault(x => x.ServerName.Trim().CompareIgnoringCase(ApplicationSettings.ServerName.Trim()));

            // Store the servers globally
            NouvemGlobal.Servers = localServers;
        }

        /// <summary>
        /// Handle the form load event.
        /// </summary>
        private void OnLoadingCommandExecute()
        {
            this.IsFormLoaded = true;

            if (ApplicationSettings.UserLoggedIn)
            {
                // set control focus
                if (!string.IsNullOrEmpty(ApplicationSettings.LoggedInUserIdentifier))
                {
                    this.UserId = ApplicationSettings.LoggedInUserIdentifier;
                    Messenger.Default.Send(Token.Message, Token.FocusToPasswordBox);
                }
                else
                {
                    Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
                }
            }

            Messenger.Default.Send(Token.Message, Token.FocusToSelectedControl);
        }

        /// <summary>
        /// Close the window, freeing up all resources.
        /// </summary>
        private void Close()
        {
            this.UserId = string.Empty;

            // Close the view
            Messenger.Default.Send(Token.Message, Token.CloseLoginSelection);
        }

        #endregion
    }
}
