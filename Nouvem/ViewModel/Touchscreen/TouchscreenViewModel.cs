﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Windows;
using System.Windows.Threading;
using Nouvem.ReportService;
using Task = System.Threading.Tasks.Task;

namespace Nouvem.ViewModel.Touchscreen
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;
    using Nouvem.Shared.Localisation;
    using Nouvem.Properties;

    public class TouchscreenViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The process.
        /// </summary>
        private string process;

        /// <summary>
        /// The record button content.
        /// </summary>
        private string recordButtonContent;

        /// <summary>
        /// Flag, as to the batch add enabling.
        /// </summary>
        private bool canAddBatch;

        /// <summary>
        /// The show attributes flag.
        /// </summary>
        private bool showAttributes;

        /// <summary>
        /// Flag, as to whether the weight is currently being processed.
        /// </summary>
        private bool weightProcessing;

        /// <summary>
        /// Flag, as to whether the weight is currently being processed.
        /// </summary>
        private string uiText;

        /// <summary>
        /// The core view within the touch screen.
        /// </summary>
        private ViewModelBase coreDisplayViewModel;

        /// <summary>
        /// The corner view within the touch screen.
        /// </summary>
        private ViewModelBase cornerViewModel;

        /// <summary>
        /// The quantity value.
        /// </summary>
        private decimal quantity;

        /// <summary>
        /// The pieces value.
        /// </summary>
        private int pieces;

        /// <summary>
        /// The batch number.
        /// </summary>
        private BatchNumber batchNo;

        /// <summary>
        /// The labels to print value.
        /// </summary>
        private int labelsToPrint;

        /// <summary>
        /// The side panel view displayed.
        /// </summary>
        private ViewModelBase panelViewModel;

        /// <summary>
        /// The current module.
        /// </summary>
        private string module;

        /// <summary>
        /// The current label in use.
        /// </summary>
        private string currentLabel;

        /// <summary>
        /// The out of production extension width.
        /// </summary>
        private double outOfProductionExtensionWidth = 0;

        /// <summary>
        /// The production spec.
        /// </summary>
        private string productionSpec;

        /// <summary>
        /// The application touchscreen modules.
        /// </summary>
        private List<CollectionData> modules = new List<CollectionData>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the TouchscreenViewModel class.
        /// </summary>
        public TouchscreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            this.SetWriteAuthorisation(Authorisation.AllowUserToSetUpData);

            #region message registration

            Messenger.Default.Register<int>(this, Token.EmptyRequiredAttribute, i => this.ShowAttributes = true);

            // Register for the label change name.
            Messenger.Default.Register<Model.DataLayer.Process>(this, Token.ProcessSelected, i =>
            {
                this.Process = i.Name;
            });

            // Register for the label change name.
            Messenger.Default.Register<int>(this, Token.ProductPieces, i =>
            {
                this.Quantity = i;
            });

            // Register for the label change name.
            Messenger.Default.Register<Label>(this, Token.ChangeDisplayLabelName, l => this.CurrentLabel = l.Name);

            // Register for the serial stock selection flag.
            Messenger.Default.Register<string>(this, Token.SerialStockSelected, s =>
            {
                this.Quantity = 1;
            });

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.Labels, s => this.LabelsToPrint = s.ToInt());

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.Pieces, s => this.Pieces = s.ToInt());

            // Register for the labels to print selection.
            Messenger.Default.Register<string>(this, KeypadTarget.Quantity, s =>
            {
                this.Quantity = s.ToDecimal();
            });
           
            // Register for the labels to print selection.
            //Messenger.Default.Register<double>(this, Token.QuantitySelected, d => this.Quantity = d);

            // Register for the new generated batch.
            Messenger.Default.Register<BatchNumber>(this, Token.BatchNumberSelected, batch => this.BatchNo = batch);
            Messenger.Default.Register<BatchNumber>(this, Token.DispatchBatchNumberSelected, batch => this.BatchNo = batch);

            // Register for the new generated batch.
            Messenger.Default.Register<string>(this, Token.SpecSelected, spec => this.ProductionSpec = spec);

            // Register for a qty reset.
            Messenger.Default.Register<string>(this, Token.ResetTouchscreenValues, s =>
            {
                //this.Quantity = 1;
                this.Pieces = 1;
            });

            // Register to handle a collection data selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, this.HandleDataSelection);

            #endregion

            #region command registration


            // Move to the selected module.
            this.PrintBarcodeCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.PrintBarcode));

            this.ExpanderCommand = new RelayCommand(() => this.ShowAttributes = !this.ShowAttributes);

            // Display the label selection screen.
            this.LabelSelectionCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.DisplayLabelSelection);
            });

            // Display the generic selection screen.
            this.GenericSelectionCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.DisplayGenericScreenSelection);

            }, () => !ApplicationSettings.DisableTouchscreenGenericSelection);
            
            
                
            
            // Move to the selected module.
            this.SelectedModuleCommand = new RelayCommand<string>(this.SelectedModuleCommandExecute);

            // Move to the orders screeen.
            this.MenuCommand = new RelayCommand<string>(this.MenuCommandExecute);

            // Display the keypad to enter a qty.
            this.RecordQuantityCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.Quantity));

            // Display the keypad to enter the pieces qty.
            this.RecordPiecesCommand = new RelayCommand(() => Keypad.Show(KeypadTarget.Pieces));

            // Display the keypad to enter a labels to print qty.
            this.SetLabelsToPrintCommand = new RelayCommand(() =>
            {
                Keypad.Show(KeypadTarget.Labels);
            });

            // Validate, then broadcast the weight record action to all interested observers.
            this.RecordCommand = new RelayCommand(this.RecordCommandExecute);

            // Broadcast the complete transaction action to all interested observers.
            this.CompleteCommand= new RelayCommand(() =>
            {
                Messenger.Default.Send(Token.Message, Token.TouchscreenCompleteOrder);
            });

            // Broadcast the new batch action to all interested observers.
            //this.NewBatchCommand = new RelayCommand(() => Messenger.Default.Send(Token.Message, Token.TouchscreenNewBatch));

            // Handle the call to select a module.
            this.ModuleSelectionCommand = new RelayCommand(this.ModuleSelectionCommandExecute);

            // Handle the call to select a module.
            this.ProcessSelectionCommand = new RelayCommand(this.ProcessSelectionCommandExecute);

            #endregion

            this.Process = Strings.NoProcess;
            ApplicationSettings.TouchScreenMode = true;
            //Settings.Default.Save();
            this.GetModules();
            this.CurrentLabel = Strings.NoLabel;
            this.Pieces = 1;
            this.RecordButtonContent = Strings.Record;
            this.CornerViewModel = ViewModelLocator.IndicatorStatic;

            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                Messenger.Default.Send(this.showAttributes, Token.UpdateShowAttributes);
            }));
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the process name.
        /// </summary>
        public string Process
        {
            get
            {
                return this.process;
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    value = Strings.NoProcess;
                }

                this.process = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets a value indicating whether the attributes are to be expanded.
        /// </summary>
        public bool ShowAttributes
        {
            get
            {
                return this.showAttributes;
            }

            set
            {
                if (ApplicationSettings.AlwaysShowAttributes)
                {
                    value = true;
                }

                this.showAttributes = value;
                this.RaisePropertyChanged();
                Messenger.Default.Send(value, Token.UpdateShowAttributes);
            }
        }

        /// <summary>
        /// Gets or sets the record button content.
        /// </summary>
        public string UIText
        {
            get
            {
                return this.uiText;
            }

            set
            {
                this.uiText = value;
                this.RaisePropertyChanged();
            }
        }
       
        /// <summary>
        /// Gets or sets the record button content.
        /// </summary>
        public string RecordButtonContent
        {
            get
            {
                return this.recordButtonContent;
            }

            set
            {
                this.recordButtonContent = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current module.
        /// </summary>
        public string Module
        {
            get
            {
                return this.module;
            }

            set
            {
                this.module = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the production spec.
        /// </summary>
        public string ProductionSpec
        {
            get
            {
                return this.productionSpec;
            }

            set
            {
                this.productionSpec = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the current label in use.
        /// </summary>
        public string CurrentLabel
        {
            get
            {
                return this.currentLabel;
            }

            set
            {
                this.currentLabel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the out of production extension width..
        /// </summary>
        public double OutOfProductionExtensionWidth
        {
            get
            {
                return this.outOfProductionExtensionWidth;
            }

            set
            {
                this.outOfProductionExtensionWidth = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the side panel display vm.
        /// </summary>
        public ViewModelBase CornerViewModel
        {
            get
            {
                return this.cornerViewModel;
            }

            set
            {
                this.cornerViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the side panel display vm.
        /// </summary>
        public ViewModelBase PanelViewModel
        {
            get
            {
                return this.panelViewModel;
            }

            set
            {
                ViewModelLocator.CleanUpPanels();
                this.panelViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a new batch can be generated.
        /// </summary>
        public bool CanAddBatch
        {
            get
            {
                return this.canAddBatch;
            }

            set
            {
                this.canAddBatch = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the core view displayed within the touch screen.
        /// </summary>
        public ViewModelBase CoreDisplayViewModel
        {
            get
            {
                return this.coreDisplayViewModel;
            }

            set
            {
                this.coreDisplayViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNo
        {
            get
            {
                return this.batchNo;
            }

            set
            {
                this.batchNo = value;
                this.RaisePropertyChanged();
            }
        }
     
        /// <summary>
        /// Gets or sets the qty amount.
        /// </summary>
        public decimal Quantity
        {
            get
            {
                return this.quantity;
            }

            set
            {
                this.quantity = value;
                this.RaisePropertyChanged();
                Messenger.Default.Send(value, Token.QuantityUpdate);
            }
        }

        /// <summary>
        /// Gets or sets the pieces amount.
        /// </summary>
        public int Pieces
        {
            get
            {
                return this.pieces;
            }

            set
            {
                this.pieces = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the labels to print amount.
        /// </summary>
        public int LabelsToPrint
        {
            get
            {
                return this.labelsToPrint;
            }

            set
            {
                this.labelsToPrint = value;
                this.RaisePropertyChanged();
                Messenger.Default.Send(value, Token.LabelsToPrintUpdate);
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to dsplay the labels.
        /// </summary>
        public ICommand PrintBarcodeCommand { get; private set; }

        /// <summary>
        /// Gets the command to dsplay the labels.
        /// </summary>
        public ICommand ExpanderCommand { get; private set; }

        /// <summary>
        /// Gets the command to dsplay the labels.
        /// </summary>
        public ICommand GenericSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to dsplay the labels.
        /// </summary>
        public ICommand LabelSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to move to the menu screen.
        /// </summary>
        public ICommand SelectedModuleCommand { get; private set; }

        /// <summary>
        /// Gets the command to move to the menu screen.
        /// </summary>
        public ICommand ProcessSelectionCommand { get; private set; }

        /// <summary>
        /// Gets the command to move to the menu screen.
        /// </summary>
        public ICommand MenuCommand { get; private set; }

        /// <summary>
        /// Gets the command to set the labels to print.
        /// </summary>
        public ICommand SetLabelsToPrintCommand { get; private set; }

        /// <summary>
        /// Gets the command to record a quantity.
        /// </summary>
        public ICommand RecordQuantityCommand { get; private set; }

        /// <summary>
        /// Gets the command to record the pieces.
        /// </summary>
        public ICommand RecordPiecesCommand { get; private set; }
            
        /// <summary>
        /// Gets the command to add a new batch.
        /// </summary>
        public ICommand NewBatchCommand { get; private set; }

        /// <summary>
        /// Gets the command to record a weight.
        /// </summary>
        public ICommand RecordCommand { get; private set; }

        /// <summary>
        /// Gets the command to complete a transaction.
        /// </summary>
        public ICommand CompleteCommand { get; private set; }

        /// <summary>
        /// Gets the command to select a module.
        /// </summary>
        public ICommand ModuleSelectionCommand { get; private set; }

        #endregion

        #region method

        /// <summary>
        /// Displays the process selection ui.
        /// </summary>
        public void DisplayProcessSelection()
        {
            this.ProcessSelectionCommandExecute();
        }

        /// <summary>
        /// Sets the core vm.
        /// </summary>
        /// <param name="vm">The vm to set.</param>
        public void SetCoreViewModel(NouvemViewModelBase vm)
        {
            this.Locator.Touchscreen.CoreDisplayViewModel = vm;
        }

        public void DisableWeigh(bool disable)
        {
            this.weightProcessing = disable;
        }

        /// <summary>
        /// Sets the master module.
        /// </summary>
        /// <param name="view">The master module.</param>
        public void SetModule(ViewType view)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                switch (view)
                {
                    case ViewType.LairageIntake:
                        this.CoreDisplayViewModel = this.Locator.LairageTouchscreen;
                        break;

                    case ViewType.Sequencer:
                        this.CoreDisplayViewModel = this.Locator.Sequencer;
                        break;

                    case ViewType.Grader:
                        this.CoreDisplayViewModel = this.Locator.Grader;
                        break;

                    case ViewType.APReceipt:
                        this.CoreDisplayViewModel = this.Locator.APReceiptTouchscreen;
                        this.Locator.APReceiptTouchscreen.TouchscreenLoaded();
                        this.Locator.TouchscreenSuppliers.SetPartners(ViewType.APReceipt);
                        this.BatchNo = new BatchNumber();
                        this.LabelsToPrint = ApplicationSettings.GoodsInLabelsToPrint;
                        this.CanAddBatch = true;
                        if (ViewModelLocator.IsAPReceiptDetailsNull())
                        {
                            ViewModelLocator.CreateAPReceiptDetails();
                        }

                        break;

                    case ViewType.ARReturns:
                        this.CoreDisplayViewModel = this.Locator.ARReturnTouchscreen;
                        this.Locator.ARReturnTouchscreen.TouchscreenLoaded();
                        this.Locator.TouchscreenSuppliers.SetPartners(ViewType.ARReturns);
                        this.BatchNo = new BatchNumber();
                        this.LabelsToPrint = ApplicationSettings.GoodsInLabelsToPrint;
                        this.CanAddBatch = true;
                        
                        break;

                    case ViewType.ARDispatch:
                        this.CoreDisplayViewModel = this.Locator.ARDispatchTouchscreen;
                        this.Locator.ARDispatchTouchscreen.TouchscreenLoaded();
                        this.Locator.TouchscreenSuppliers.SetPartners(ViewType.ARDispatch);
                        this.LabelsToPrint = ApplicationSettings.DispatchLabelsToPrint;
                        this.CanAddBatch = false;
                        
                        break;

                    case ViewType.ScannerDispatch:
                        this.CoreDisplayViewModel = this.Locator.ScannerMainDispatch;
                        this.Locator.ScannerMainDispatch.TouchscreenLoaded();
                        this.Locator.TouchscreenSuppliers.SetPartners(ViewType.ARDispatch);
                        this.LabelsToPrint = ApplicationSettings.DispatchLabelsToPrint;
                        this.CanAddBatch = false;
                        
                        break;

                    //case ViewType.ScannerDispatch:
                    //    this.CoreDisplayViewModel = this.Locator.ScannerMainDispatch;
                    //    this.Locator.ScannerMainDispatch.TouchscreenLoaded();
                    //    this.Locator.ScannerCustomers.SetPartners(ViewType.ARDispatch);
                    //    this.LabelsToPrint = ApplicationSettings.DispatchLabelsToPrint;
                    //    this.CanAddBatch = false;
                    //    if (ViewModelLocator.IsARDispatchDetailsNull())
                    //    {
                    //        ViewModelLocator.CreateARDispatchDetails();
                    //    }
                    //    break;

                    case ViewType.IntoProduction:
                        this.CoreDisplayViewModel = this.Locator.IntoProduction;
                        //this.Locator.Touchscreen.TouchscreenLoaded();
                        this.LabelsToPrint = ApplicationSettings.IntoProductionLabelsToPrint;
                        this.CanAddBatch = false;
                        break;

                    case ViewType.OutOfProduction:
                        this.CoreDisplayViewModel = this.Locator.OutOfProduction;
                        //this.Locator.Touchscreen.TouchscreenLoaded();
                        this.LabelsToPrint = ApplicationSettings.OutOfProductionLabelsToPrint;
                        this.CanAddBatch = false;
                        break;
                }

                this.BatchNo = new BatchNumber { Number = "0" };
                Messenger.Default.Send(Token.Message, Token.ResetTouchscreenValues);
            });
        }

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:
                   
                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Validates, then broadcasts a successfull saved weight.
        /// </summary>
        private void RecordCommandExecute()
        {
            if (!this.weightProcessing)
            {
                this.weightProcessing = true;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.RecordButtonContent = Strings.Wait;
                });

                Task.Factory.StartNew(() =>
                {
                    System.Threading.Thread.Sleep(1);
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        try
                        {
                            this.Locator.Indicator.RecordWeight();
                        }
                        finally
                        {
                            if (!ViewModelLocator.IsARDispatchTouchscreenNull())
                            {
                                if (this.Locator.Indicator.WeighMode == WeighMode.Auto)
                                {
                                    this.RecordButtonContent = Strings.Auto;
                                }
                                else
                                {
                                    this.RecordButtonContent = Strings.Record;
                                }
                            }
                            else
                            {
                                this.RecordButtonContent = Strings.Record;
                            }

                            this.weightProcessing = false;
                        }
                    });
                });
            }
        }

        public void CreateWorkflow()
        {
            this.Module = Strings.Workflow;
            Messenger.Default.Send(Token.Message, Token.CreateWorkflow);
        }

        /// <summary>
        /// Handle the modle selection.
        /// </summary>
        /// <param name="module">The selected module.</param>
        public void SelectedModuleCommandExecute(string module)
        {
            if (ApplicationSettings.DisallowTouchscreenModuleChange)
            {
                return;
            }

            //this.CoreDisplayViewModel = null;
            //this.PanelViewModel = null;

            Messenger.Default.Send(Token.Message, Token.CloseModuleSelection);
            ViewModelLocator.ClearWorkflow();
            Messenger.Default.Send(Token.Message, Token.CloseWorkflow);

            //if (module.Equals(Constant.StockMovement))
            //{
            //    this.SetModule(ViewType.StockMovement);
            //    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.StockMovementPanelStatic;
            //    this.Module = Strings.StockMovement;
            //}
            if (module.Equals(Constant.Workflow))
            {
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearARReturnTouchscreen();
                ViewModelLocator.ClearGrader();
                //ViewModelLocator.ClearStockMovementTouchscreen();
                this.Module = Strings.Workflow;
                Messenger.Default.Send(Token.Message, Token.CreateWorkflow);
            }
            else if (module.Equals("Palletisation"))
            {
                Messenger.Default.Send(Token.Message, Token.CreateTouchscreenPalletisation);
                this.Locator.ScannerPalletisation.PrintLabel = true;
            }
            else if (module.Equals(Constant.Lairage))
            {
                //ViewModelLocator.ClearStockMovementTouchscreen();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearGrader();
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearARReturnTouchscreen();
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.LairageIntake);
                this.Module = Strings.Lairage;

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.LairagePanelStatic;
                }));
            }
            else if (module.Equals(Constant.Sequencer))
            {
                //ViewModelLocator.ClearStockMovementTouchscreen();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearARReturnTouchscreen();
                ViewModelLocator.ClearGrader();
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.Sequencer);
                this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.SequencerPanelStatic;
                this.Module = Strings.Sequencer;
            }
            else if (module.Equals(Constant.Grader))
            {
                //ViewModelLocator.ClearStockMovementTouchscreen();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearARReturnTouchscreen();
                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.Grader);
                this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.GraderPanelStatic;
                this.Module = Strings.Grader;
            }
            else if (module.Equals(Constant.GoodsIn))
            {
                //ViewModelLocator.ClearStockMovementTouchscreen();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearARReturnTouchscreen();
                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearGrader();
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.APReceipt);
                this.Locator.TouchscreenOrders.SetModule(ViewType.APReceipt);
                //this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.IntakeMode == IntakeMode.Intake ? ViewModelLocator.TouchscreenReceiptPanelStatic : ViewModelLocator.TouchscreenReceiptProductionPanelStatic;
                this.CoreDisplayViewModel = ViewModelLocator.APReceiptTouchscreenStatic;
                this.Module = Strings.GoodsInSmall;
                SystemMessage.Write(MessageType.Background, Message.ReceiptScreenLoaded);
            }
            else if (module.Equals("Returns"))
            {
                //ViewModelLocator.ClearStockMovementTouchscreen();
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearARDispatchDetails();

                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearGrader();
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.ARReturns);
                this.Locator.TouchscreenOrders.SetModule(ViewType.ARReturns);
                //this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.IntakeMode == IntakeMode.Intake ? ViewModelLocator.TouchscreenReceiptPanelStatic : ViewModelLocator.TouchscreenReceiptProductionPanelStatic;
                this.CoreDisplayViewModel = ViewModelLocator.ARReturnTouchscreenStatic;
                this.Module = Strings.Returns;
            }
            else if (module.Equals(Constant.IntoProduction))
            {
                //ViewModelLocator.ClearStockMovementTouchscreen();
                //ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearARReturnTouchscreen();
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearGrader();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearARDispatch();
                ViewModelLocator.ClearARDispatch2();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearAPReceiptDetails();
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.IntoProduction);
                this.Module = Strings.IntoProduction;
                SystemMessage.Write(MessageType.Background, Message.ProductionLoaded);
            }

            else if (module.Equals(Constant.OutOfProduction))
            {
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearARReturnTouchscreen();
                //ViewModelLocator.ClearStockMovementTouchscreen();
                ViewModelLocator.ClearProductionProducts();
                ViewModelLocator.ClearProductionProductsCards();
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearTouchscreenOrders();
                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearGrader();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearARDispatch();
                ViewModelLocator.ClearARDispatch2();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearAPReceiptDetails();
                this.SetModule(ViewType.OutOfProduction);
                if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfProductionStandard
                    || ApplicationSettings.OutOfProductionPanelMode == OutOfProductionPanel.Standard)
                {
                    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenOutOfProductionPanelStatic;

                }
                else
                {
                    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenOutOfProductionWithPiecesPanelStatic;
                }
                this.Module = Strings.OutOfProduction;
                SystemMessage.Write(MessageType.Background, Message.ProductionLoaded);
            }
            else if (module.Equals(Constant.Dispatch))
            {
                ViewModelLocator.ClearARReturn();
                ViewModelLocator.ClearARReturnTouchscreen();
                //ViewModelLocator.ClearStockMovementTouchscreen();
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatch();
                ViewModelLocator.ClearARDispatch2();
                ViewModelLocator.ClearAPReceiptDetails();
             
                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearGrader();
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.ARDispatch);
                this.Locator.TouchscreenOrders.SetModule(ViewType.ARDispatch);
                //if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight ||
                //    ApplicationSettings.ScanOrWeighAtDispatch)
                //{
                //    if (ApplicationSettings.Customer == Customer.Woolleys
                //        || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithNotes)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //           ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithNotesStatic;
                //    }
                //    else if (ApplicationSettings.Customer == Customer.Ballon
                //        || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithShipping)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //           ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithShippingStatic;
                //    }
                //    else if (ApplicationSettings.Customer == Customer.TSBloor || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeight)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //            ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
                //    }
                //    else
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //            ViewModelLocator.TouchscreenDispatchPanelStatic;
                //    }
                //}
                //else
                //{
                //    if (ApplicationSettings.Customer == Customer.Woolleys || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithNotes)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //            ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithNotesStatic;
                //    }
                //    else if (ApplicationSettings.Customer == Customer.Ballon || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithShipping)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //            ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithShippingStatic;
                //    }
                //    else if (ApplicationSettings.Customer == Customer.TSBloor || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeight)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //            ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
                //    }
                //    else
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //            ViewModelLocator.TouchscreenDispatchPanelStatic;
                //    }
                //}

                //this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.DispatchMode == DispatchMode.ScanStock ? ViewModelLocator.TouchscreenDispatchPanelStatic : ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
                this.Module = Strings.Dispatch;
                SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
            }
        }

        /// <summary>
        /// Handle the modle selection.
        /// </summary>
        /// <param name="module">The selected module.</param>
        //private void SelectedModuleCommandExecute(string module)
        //{
        //    if (ApplicationSettings.DisallowTouchscreenModuleChange)
        //    {
        //        return;
        //    }

        //    Messenger.Default.Send(Token.Message, Token.CloseModuleSelection);

        //    if (module.Equals(Constant.Sequencer))
        //    {
        //        ViewModelLocator.ClearAPReceipt();
        //        ViewModelLocator.ClearIntoProduction();
        //        ViewModelLocator.ClearOutOfProduction();
        //        ViewModelLocator.ClearARDispatchTouchscreen();
        //        ViewModelLocator.ClearARDispatchDetails();
        //        ViewModelLocator.ClearProductionDetails();
        //        ViewModelLocator.ClearGrader();
        //        ViewModelLocator.ClearProductionProductsCards();
        //        this.SetModule(ViewType.Sequencer);
        //        //this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.SequencerPanelStatic;
        //        this.Module = Strings.Sequencer;
        //    }
        //    else if (module.Equals(Constant.Grader))
        //    {
        //        ViewModelLocator.ClearAPReceipt();
        //        ViewModelLocator.ClearIntoProduction();
        //        ViewModelLocator.ClearOutOfProduction();
        //        ViewModelLocator.ClearARDispatchTouchscreen();
        //        ViewModelLocator.ClearARDispatchDetails();
        //        ViewModelLocator.ClearProductionDetails();
        //        ViewModelLocator.ClearSequencer();
        //        ViewModelLocator.ClearProductionProductsCards();
        //        this.SetModule(ViewType.Grader);
        //        //this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.GraderPanelStatic;
        //        this.Module = Strings.Grader;
        //    }
        //    else if (module.Equals(Constant.GoodsIn))
        //    {
        //        ViewModelLocator.ClearAPReceipt();
        //        ViewModelLocator.ClearIntoProduction();
        //        ViewModelLocator.ClearOutOfProduction();
        //        ViewModelLocator.ClearARDispatchTouchscreen();
        //        ViewModelLocator.ClearARDispatchDetails();
        //        ViewModelLocator.ClearProductionDetails();
        //        ViewModelLocator.ClearSequencer();
        //        ViewModelLocator.ClearGrader();
        //        ViewModelLocator.ClearProductionProductsCards();
        //        this.SetModule(ViewType.APReceipt);
        //        this.Locator.TouchscreenOrders.SetModule(ViewType.APReceipt);
        //        this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.IntakeMode == IntakeMode.Standard ? ViewModelLocator.TouchscreenReceiptPanelStatic : ViewModelLocator.TouchscreenReceiptProductionPanelStatic;
        //        this.CoreDisplayViewModel = ViewModelLocator.APReceiptTouchscreenStatic;
        //        this.Module = Strings.GoodsInSmall;
        //        SystemMessage.Write(MessageType.Background, Message.ReceiptScreenLoaded);
        //    }
        //    else if (module.Equals(Constant.IntoProduction))
        //    {
        //        ViewModelLocator.ClearAPReceiptTouchscreen();
        //        ViewModelLocator.ClearARDispatchTouchscreen();
        //        ViewModelLocator.ClearTouchscreenProductionOrders();
        //        ViewModelLocator.ClearSequencer();
        //        ViewModelLocator.ClearGrader();
        //        ViewModelLocator.ClearAPReceipt();
        //        ViewModelLocator.ClearARDispatch();
        //        ViewModelLocator.ClearARDispatch2();
        //        ViewModelLocator.ClearOutOfProduction();
        //        ViewModelLocator.ClearARDispatchDetails();
        //        ViewModelLocator.ClearAPReceiptDetails();
        //        ViewModelLocator.ClearProductionProductsCards();
        //        this.SetModule(ViewType.IntoProduction);
        //        this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard ? ViewModelLocator.TouchscreenProductionPanelStatic : ViewModelLocator.TouchscreenProductionRecallPanelStatic;
        //        this.Module = Strings.IntoProduction;
        //        SystemMessage.Write(MessageType.Background, Message.ProductionLoaded);
        //    }

        //    else if (module.Equals(Constant.OutOfProduction))
        //    {
        //        ViewModelLocator.ClearProductionProducts();
        //        ViewModelLocator.ClearProductionProductsCards();
        //        ViewModelLocator.ClearAPReceiptTouchscreen();
        //        ViewModelLocator.ClearARDispatchTouchscreen();
        //        ViewModelLocator.ClearTouchscreenOrders();
        //        ViewModelLocator.ClearSequencer();
        //        ViewModelLocator.ClearGrader();
        //        ViewModelLocator.ClearAPReceipt();
        //        ViewModelLocator.ClearARDispatch();
        //        ViewModelLocator.ClearARDispatch2();
        //        ViewModelLocator.ClearIntoProduction();
        //        ViewModelLocator.ClearARDispatchDetails();
        //        ViewModelLocator.ClearAPReceiptDetails();
        //        this.SetModule(ViewType.OutOfProduction);
        //        if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfProductionStandard
        //            || ApplicationSettings.OutOfProductionPanelMode == OutOfProductionPanel.Standard)
        //        {
        //            this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenOutOfProductionPanelStatic;

        //        }
        //        else
        //        {
        //            this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenOutOfProductionWithPiecesPanelStatic;
        //        }
        //        this.Module = Strings.OutOfProduction;
        //        SystemMessage.Write(MessageType.Background, Message.ProductionLoaded);
        //    }
        //    else if (module.Equals(Constant.Dispatch))
        //    {
        //        ViewModelLocator.ClearAPReceiptTouchscreen();
        //        ViewModelLocator.ClearIntoProduction();
        //        ViewModelLocator.ClearOutOfProduction();
        //        ViewModelLocator.ClearARDispatch();
        //        ViewModelLocator.ClearARDispatch2();
        //        ViewModelLocator.ClearAPReceiptDetails();
        //        ViewModelLocator.ClearProductionDetails();
        //        ViewModelLocator.ClearSequencer();
        //        ViewModelLocator.ClearGrader();
        //        ViewModelLocator.ClearProductionProductsCards();
        //        this.SetModule(ViewType.ARDispatch);
        //        this.Locator.TouchscreenOrders.SetModule(ViewType.ARDispatch);
        //        if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight ||
        //            ApplicationSettings.ScanOrWeighAtDispatch)
        //        {
        //            if (ApplicationSettings.Customer == Customer.Woolleys 
        //                || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithNotes)
        //            {
        //                this.Locator.Touchscreen.PanelViewModel =
        //                   ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithNotesStatic;
        //            }
        //            else if (ApplicationSettings.Customer == Customer.Ballon 
        //                || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithShipping)
        //            {
        //                this.Locator.Touchscreen.PanelViewModel =
        //                   ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithShippingStatic;
        //            }
        //            else if (ApplicationSettings.Customer == Customer.TSBloor || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeight)
        //            {
        //                this.Locator.Touchscreen.PanelViewModel =
        //                    ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
        //            }
        //            else
        //            {
        //                this.Locator.Touchscreen.PanelViewModel =
        //                    ViewModelLocator.TouchscreenDispatchPanelStatic;
        //            }
        //        }
        //        else
        //        {
        //            if (ApplicationSettings.Customer == Customer.Woolleys || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithNotes)
        //            {
        //                this.Locator.Touchscreen.PanelViewModel =
        //                    ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithNotesStatic;
        //            }
        //            else if (ApplicationSettings.Customer == Customer.Ballon || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeightWithShipping)
        //            {
        //                this.Locator.Touchscreen.PanelViewModel =
        //                    ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithShippingStatic;
        //            }
        //            else if (ApplicationSettings.Customer == Customer.TSBloor || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeight)
        //            {
        //                this.Locator.Touchscreen.PanelViewModel =
        //                    ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
        //            }
        //            else
        //            {
        //                this.Locator.Touchscreen.PanelViewModel =
        //                    ViewModelLocator.TouchscreenDispatchPanelStatic;
        //            }
        //        }

        //        //this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.DispatchMode == DispatchMode.ScanStock ? ViewModelLocator.TouchscreenDispatchPanelStatic : ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
        //        this.Module = Strings.Dispatch;
        //        SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
        //    }
        //}

        /// <summary>
        /// Calls and sets the menu screen, dependent on the module.
        /// </summary>
        /// <param name="module">The module to set the menu for.</param>
        private void MenuCommandExecute(string module)
        {
            var localModule = (ViewType) Enum.Parse(typeof (ViewType), module);
            this.Locator.Menu.AddMenuItems(localModule);
            this.Locator.FactoryScreen.SetMainViewModel(this.Locator.Menu);
        }

        /// <summary>
        /// Handle the module selection.
        /// </summary>
        private void ProcessSelectionCommandExecute()
        {
            Messenger.Default.Send(Token.Message, Token.DisplayProcessSelection);
        }

        /// <summary>
        /// Handle the module selection.
        /// </summary>
        private void ModuleSelectionCommandExecute()
        {
            if (ApplicationSettings.DisallowTouchscreenModuleChange)
            {
                return;
            }

            Messenger.Default.Send(Token.Message, Token.ClearAndResetData);
            Messenger.Default.Send(Token.Message, Token.CreateTouchscreenModuleSelection);
        }
        
        #endregion

        #region helper

        /// <summary>
        /// Handles the incoming module selection.
        /// </summary>
        /// <param name="data">The incoming module.</param>
        private void HandleDataSelection(CollectionData data)
        {
            if (data == null || !data.Identifier.Equals(ViewType.Production.ToString()))
            {
                return;
            }

            var localModule = data.Data;
            if (localModule.Equals(Strings.Intake))
            {
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearARDispatchDetails();
       
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.APReceipt);
                this.Locator.TouchscreenOrders.SetModule(ViewType.APReceipt);
                //this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.IntakeMode == IntakeMode.Standard ? ViewModelLocator.TouchscreenReceiptPanelStatic : ViewModelLocator.TouchscreenReceiptProductionPanelStatic;
                this.CoreDisplayViewModel = ViewModelLocator.APReceiptTouchscreenStatic;
                SystemMessage.Write(MessageType.Background, Message.ReceiptScreenLoaded);
            }
            else if (localModule.Equals(Strings.Dispatch))
            {
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatch();
                ViewModelLocator.ClearARDispatch2();
                ViewModelLocator.ClearAPReceiptDetails();
               
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.ARDispatch);
                this.Locator.TouchscreenOrders.SetModule(ViewType.ARDispatch);
                //if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight ||
                //   ApplicationSettings.ScanOrWeighAtDispatch)
                //{
                //    if (ApplicationSettings.Customer == Customer.Woolleys)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //           ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithNotesStatic;
                //    }
                //    else if (ApplicationSettings.Customer == Customer.Ballon)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //           ViewModelLocator.TouchscreenDispatchPanelRecordWeightWithShippingStatic;
                //    }
                //    else if (ApplicationSettings.Customer == Customer.TSBloor || ApplicationSettings.DispatchPanelMode == DispatchPanel.RecordWeight)
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //            ViewModelLocator.TouchscreenDispatchPanelRecordWeightStatic;
                //    }
                //    else
                //    {
                //        this.Locator.Touchscreen.PanelViewModel =
                //            ViewModelLocator.TouchscreenDispatchPanelStatic;
                //    }
                //}
                //else
                //{
                //    this.Locator.Touchscreen.PanelViewModel =
                //        ViewModelLocator.TouchscreenDispatchPanelStatic;
                //}
                SystemMessage.Write(MessageType.Background, Message.DispatchScreenLoaded);
            }
            else if (localModule.Equals(Strings.IntoProduction))
            {
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearTouchscreenProductionOrders();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearARDispatch();
                ViewModelLocator.ClearARDispatch2();
                ViewModelLocator.ClearOutOfProduction();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearAPReceiptDetails();
                ViewModelLocator.ClearProductionProductsCards();
                this.SetModule(ViewType.IntoProduction);
                this.Locator.Touchscreen.PanelViewModel = ApplicationSettings.IntoProductionMode == IntoProductionMode.IntoProductionStandard ? ViewModelLocator.TouchscreenProductionPanelStatic : ViewModelLocator.TouchscreenProductionRecallPanelStatic;
                SystemMessage.Write(MessageType.Background, Message.ProductionLoaded);
            }
            else if (localModule.Equals(Strings.OutOfProduction))
            {
                ViewModelLocator.ClearProductionProducts();
                ViewModelLocator.ClearProductionProductsCards();
                ViewModelLocator.ClearAPReceiptTouchscreen();
                ViewModelLocator.ClearARDispatchTouchscreen();
                ViewModelLocator.ClearTouchscreenOrders();
                ViewModelLocator.ClearAPReceipt();
                ViewModelLocator.ClearARDispatch();
                ViewModelLocator.ClearARDispatch2();
                ViewModelLocator.ClearIntoProduction();
                ViewModelLocator.ClearARDispatchDetails();
                ViewModelLocator.ClearAPReceiptDetails();
                this.SetModule(ViewType.OutOfProduction);
                if (ApplicationSettings.OutOfProductionMode == OutOfProductionMode.OutOfProductionStandard
                    || ApplicationSettings.OutOfProductionPanelMode == OutOfProductionPanel.Standard)
                {
                    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenOutOfProductionPanelStatic;

                }
                else
                {
                    this.Locator.Touchscreen.PanelViewModel = ViewModelLocator.TouchscreenOutOfProductionWithPiecesPanelStatic;
                }

                SystemMessage.Write(MessageType.Background, Message.ProductionLoaded);
            }

            ViewModelLocator.TouchscreenStatic.Module = localModule;
        }

        /// <summary>
        /// Set up the modules data.
        /// </summary>
        private void GetModules()
        {
            this.modules.Add(new CollectionData { Data = Strings.Intake, Identifier = ViewType.Production.ToString()});
            this.modules.Add(new CollectionData { Data = Strings.IntoProduction, Identifier = ViewType.Production.ToString() });
            this.modules.Add(new CollectionData { Data = Strings.OutOfProduction, Identifier = ViewType.Production.ToString() });
            this.modules.Add(new CollectionData { Data = Strings.Dispatch, Identifier = ViewType.Production.ToString() });
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearTouchscreen();
           // Messenger.Default.Send(Token.Message, Token.CloseCurrencyWindow);
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion

        #endregion
    }
}
