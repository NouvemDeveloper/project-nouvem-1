﻿// -----------------------------------------------------------------------
// <copyright file="TouchscreenViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Threading;
using Nouvem.Model.BusinessObject;
using Nouvem.Properties;
using Nouvem.Shared.Localisation;
using Quartz.Util;

namespace Nouvem.ViewModel.Touchscreen
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.Enum;

    public class FactoryScreenViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The current module name.
        /// </summary>
        private string moduleName;

        /// <summary>
        /// The main view displayed.
        /// </summary>
        private NouvemViewModelBase mainDisplayViewModel;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the FactoryScreenViewModel class.
        /// </summary>
        public FactoryScreenViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register to set the core vm.
            Messenger.Default.Register<NouvemViewModelBase>(this, Token.SetTouchscreenViewModel, this.SetMainViewModel);

            // Register to set the core vm to the collection display.
            Messenger.Default.Register<string>(this, Token.SetTouchscreenViewModel, s => this.SetMainViewModel(this.Locator.CollectionDisplay));

            #endregion

            #region command

            if (ApplicationSettings.TestMode)
            {
                this.KeyUpCommand = new GalaSoft.MvvmLight.CommandWpf.RelayCommand<KeyEventArgs>(e => this.KeyPressCommandExecute(true), e => e.Key == Key.F11 || e.Key == Key.F12);
                this.KeyDownCommand = new GalaSoft.MvvmLight.CommandWpf.RelayCommand<KeyEventArgs>(e => this.KeyPressCommandExecute(false), e => e.Key == Key.F11 || e.Key == Key.F12);
                this.TestWeightCommand = new RelayCommand<string>(this.TestWeightCommandExecute);
            }

            this.OnLoadedCommand = new RelayCommand(() => this.IsFormLoaded = true);

            // Clean up the child modules.
            this.OnClosingCommand = new RelayCommand(this.CleanUp);

            #endregion

            this.MainDisplayViewModel = this.Locator.Touchscreen;
            //Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
            // Messenger.Default.Send(Token.Message, Token.CloseLogin);
            this.CheckLicenceExpiryDate();
        }

        #endregion

        #region public interface

        #region property
    
        /// <summary>
        /// Gets or sets the current module name.
        /// </summary>
        public string ModuleName
        {
            get
            {
                return this.moduleName;
            }

            set
            {
                this.moduleName = value;
                this.RaisePropertyChanged();
            }
        }
        
        /// <summary>
        /// Gets or sets the main display vm.
        /// </summary>
        public NouvemViewModelBase MainDisplayViewModel
        {
            get
            {
                return this.mainDisplayViewModel;
            }

            set
            {
                this.mainDisplayViewModel = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// The test weight key press event capture.
        /// </summary>
        public ICommand KeyUpCommand { get; set; }

        /// <summary>
        /// The test weight key press event capture.
        /// </summary>
        public ICommand KeyDownCommand { get; set; }

        /// <summary>
        /// Command to use the test weight keys.
        /// </summary>
        public ICommand TestWeightCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }
    
        /// <summary>
        /// Gets the command to handle the closing event.
        /// </summary>
        public ICommand OnClosingCommand { get; private set; }

        /// <summary>
        /// Gets the command to update the currencies.
        /// </summary>
        public ICommand UpdateCommand { get; set; }

        #endregion

        #region method

        /// <summary>
        /// Sets the main vm.
        /// </summary>
        /// <param name="vm">The vm to set.</param>
        public void SetMainViewModel(NouvemViewModelBase vm)
        {
            this.Locator.FactoryScreen.MainDisplayViewModel = vm;
        }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Checks the licence validity date, warning the user if it's due to expire within 30 days.
        /// </summary>
        protected override void CheckLicenceExpiryDate()
        {
            if (NouvemGlobal.IsNouvemUser)
            {
                // nouvem engineer/installer...ignore.
                return;
            }

            if (NouvemGlobal.Licensee.LicenceStatus != LicenceStatus.Valid)
            {
                // user has no licence
                SystemMessage.Write(MessageType.Issue, Message.NoLicence);

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(Message.NoLicence, touchScreen:true);
                    Environment.Exit(0);
                }));

                return;
            }

            var daysLeft = this.LicenceManager.GetLicenceDaysRemaining();
            if (daysLeft <= 30)
            {
                // warn the user
                SystemMessage.Write(MessageType.Issue, string.Format("{0} {1} {2}", Message.LicenceExpiryWarning, daysLeft, Message.Days));
            }
        }

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            base.ControlCommandExecute(mode);

            if (!this.HasWriteAuthorisation || this.CurrentMode == ControlMode.Find)
            {
                this.SetControlMode(ControlMode.OK);
            }
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.Update:

                    break;

                case ControlMode.OK:
                    this.Close();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        /// <summary>
        /// Handles a key press in test mode.
        /// </summary>
        /// <param name="up">Key up/down flag.</param>
        private void TestWeightCommandExecute(string up)
        {
            this.Locator.Indicator.TestWeight(up);
        }

        /// <summary>
        /// Handles a key press in test mode.
        /// </summary>
        /// <param name="up">Key up/down flag.</param>
        private void KeyPressCommandExecute(bool up)
        {
            this.Locator.Indicator.KeyPressCommandExecute(up);
        }

        /// <summary>
        /// Cleans up the child vms.
        /// </summary>
        private void CleanUp()
        {
            ApplicationSettings.TouchScreenMode = false;
          
            this.IsFormLoaded = false;
            ViewModelLocator.ClearARReturnTouchscreen();
            ViewModelLocator.ClearAPReceiptTouchscreen();
            ViewModelLocator.ClearARDispatchTouchscreen();
            ViewModelLocator.ClearTouchscreen();
            ViewModelLocator.ClearTouchscreenOrders();
            ViewModelLocator.ClearTouchscreenContainer();
            ViewModelLocator.ClearTouchscreenProducts();
            ViewModelLocator.ClearTouchscreenSuppliers();

            if (ApplicationSettings.TouchScreenModeOnly)
            {
                this.DataManager.ApplicationClosingHandler();
            }
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearFactoryScreen();
            // Messenger.Default.Send(Token.Message, Token.CloseCurrencyWindow);
        }

        /// <summary>
        /// Determine if the user can update.
        /// </summary>
        /// <returns>A flag, indicating whether the user can update or not.</returns>
        private bool CanUpdate()
        {
            if (this.CurrentMode == ControlMode.Update)
            {
                return true;
            }

            return this.HasWriteAuthorisation;
        }

        #endregion
    }
}
