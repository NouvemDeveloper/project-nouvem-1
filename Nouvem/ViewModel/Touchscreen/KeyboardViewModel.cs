﻿// -----------------------------------------------------------------------
// <copyright file="KeyboardViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Touchscreen
{
    using System;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;

    public class KeyboardViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The entered keyboard value.
        /// </summary>
        private string keyboardInput;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyboardViewModel"/> class.
        /// </summary>
        public KeyboardViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region command

            // Handle the ui load event.
            this.OnLoadedCommand = new RelayCommand(() => this.IsFormLoaded = true);

            // Handle the un load event.
            this.OnUnloadedCommand = new RelayCommand(() => this.IsFormLoaded = false);
            
            // Hanle the keyboard enter press.
            this.OnLostFocusCommand = new RelayCommand(() =>
            {
                Messenger.Default.Send(this.keyboardInput, Token.KeyboardValueEntered);
                this.Close();
            });

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the keyboard input value.
        /// </summary>
        public string KeyboardInput
        {
            get
            {
                return this.keyboardInput;
            }

            set
            {
                this.keyboardInput = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the lost focus event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the lost focus event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the lost focus event.
        /// </summary>
        public ICommand OnLostFocusCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        /// <summary>
        /// Clear, and move back to the main ui.
        /// </summary>
        private void Close()
        {
            this.KeyboardInput = string.Empty;
            Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
        }

        #endregion
    }
}
