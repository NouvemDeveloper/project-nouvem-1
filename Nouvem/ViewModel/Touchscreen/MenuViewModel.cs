﻿// -----------------------------------------------------------------------
// <copyright file="MenuViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;
using Nouvem.Shared;

namespace Nouvem.ViewModel.Touchscreen
{
    using System;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared.Localisation;

    public class MenuViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The ui menu items.
        /// </summary>
        private ObservableCollection<CollectionData> menuItems = new ObservableCollection<CollectionData>();

        /// <summary>
        /// The selected ui menu item.
        /// </summary>
        private CollectionData selectedMenuItem;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuViewModel"/> class.
        /// </summary>
        public MenuViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register to handle a collection data selection.
            Messenger.Default.Register<CollectionData>(this, Token.CollectionDisplayValue, this.HandleReportSelection);

            #endregion

            #region command handler

            // Handle the menu item selection.
            this.MenuItemSelectedCommand = new RelayCommand(this.MenuItemSelectedCommandExecute, () => this.selectedMenuItem != null);

            // Handle the move back to the orders screen.
            this.MoveBackCommand = new RelayCommand(() =>
            {
                //Messenger.Default.Send(true, Token.HighlightSelectedProduct);

                if (this.Module == ViewType.Workflow)
                {
                    Messenger.Default.Send(Token.Message, Token.CloseMenuWindow);
                    return;
                }

                this.Locator.FactoryScreen.SetMainViewModel(this.Locator.Touchscreen);
            });

            // Handle the ui load event.
            this.OnLoadedCommand = new RelayCommand(() => this.SelectedMenuItem = null);

            this.LogOutCommand = new RelayCommand(this.LogOutCommandExecute);

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the calling moule.
        /// </summary>
        public ViewType Module { get; set; }

        /// <summary>
        /// Gets or sets the gs1 identifiers.
        /// </summary>
        public ObservableCollection<CollectionData> MenuItems
        {
            get
            {
                return this.menuItems;
            }

            set
            {
                this.menuItems = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected menu item
        /// </summary>
        public CollectionData SelectedMenuItem
        {
            get
            {
                return this.selectedMenuItem;
            }

            set
            {
                this.selectedMenuItem = value;
                this.RaisePropertyChanged();
                if (value != null)
                {
                    this.MenuItemSelectedCommandExecute();
                }
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Add the menu items here.
        /// </summary>
        public void AddMenuItems(ViewType module)
        {
            this.Module = module;
            this.MenuItems.Clear();
            var close = ApplicationSettings.TouchScreenModeOnly ? Strings.CloseApplication : Strings.CloseModule;
           
            switch (module)
            {
                case ViewType.Workflow:

                    this.MenuItems.Add(new CollectionData { Data = Strings.LabelHistory });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ZeroScales });
                    this.MenuItems.Add(new CollectionData { Data = Strings.LogOutLwr });
                    this.MenuItems.Add(new CollectionData { Data = MenuItem.SystemInformation });
                    this.MenuItems.Add(new CollectionData { Data = close });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ShutdownPC });

                    break;

                case ViewType.Grader:

                    this.MenuItems.Add(new CollectionData { Data = Strings.LabelHistory });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ZeroScales });
                    this.MenuItems.Add(new CollectionData { Data = Strings.SendCommandToPrinter });
                    this.MenuItems.Add(new CollectionData { Data = Strings.LogOutLwr });
                    this.MenuItems.Add(new CollectionData { Data = MenuItem.SystemInformation });
                    this.MenuItems.Add(new CollectionData { Data = close });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ShutdownPC });

                    break;

                case ViewType.Sequencer:

                    this.MenuItems.Add(new CollectionData { Data = Strings.LabelHistory });
                    this.MenuItems.Add(new CollectionData { Data = Strings.EnableLineReOrdering });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ZeroScales });
                    this.MenuItems.Add(new CollectionData { Data = Strings.LogOutLwr });
                    this.MenuItems.Add(new CollectionData { Data = MenuItem.SystemInformation });
                    this.MenuItems.Add(new CollectionData { Data = close });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ShutdownPC });

                    break;

                case ViewType.APReceipt:
                    this.MenuItems.Add(new CollectionData { Data = Strings.LabelHistory });
                    this.MenuItems.Add(new CollectionData { Data = Strings.RefreshData});

                    // TODO put in an authorisation or setting.
                    //if (ApplicationSettings.DispatchMode == DispatchMode.ScanStock)
                    //{
                        this.MenuItems.Add(new CollectionData { Data = Strings.RunStartOfDay });
                   // }

                    if (ApplicationSettings.DisplayReportsOnTouchscreen)
                    {
                        this.MenuItems.Add(new CollectionData { Data = Strings.Reports });
                    }

                    this.MenuItems.Add(new CollectionData { Data = Strings.ZeroScales });
                    this.MenuItems.Add(new CollectionData { Data = Strings.SendCommandToPrinter });
                    this.MenuItems.Add(new CollectionData { Data = Strings.LogOutLwr });
                    this.MenuItems.Add(new CollectionData { Data = MenuItem.SystemInformation });
                    this.MenuItems.Add(new CollectionData { Data = close });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ShutdownPC });
                 
                    break;

                case ViewType.IntoProduction:
                    this.MenuItems.Add(new CollectionData { Data = Strings.LabelHistory });
                    this.MenuItems.Add(new CollectionData { Data = Strings.RefreshData });

                    // TODO put in an authorisation or setting.
                    //if (ApplicationSettings.DispatchMode == DispatchMode.ScanStock)
                    //{
                        this.MenuItems.Add(new CollectionData { Data = Strings.RunStartOfDay });
                   // }

                    if (ApplicationSettings.DisplayReportsOnTouchscreen)
                    {
                        this.MenuItems.Add(new CollectionData { Data = Strings.Reports });
                    }

                    this.MenuItems.Add(new CollectionData { Data = Strings.ZeroScales });
                    this.MenuItems.Add(new CollectionData { Data = Strings.SendCommandToPrinter });
                    this.MenuItems.Add(new CollectionData { Data = Strings.LogOutLwr });
                    this.MenuItems.Add(new CollectionData { Data = MenuItem.SystemInformation });
                    this.MenuItems.Add(new CollectionData { Data = close });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ShutdownPC });
               
                    break;

                case ViewType.OutOfProduction:
                    this.MenuItems.Add(new CollectionData { Data = Strings.LabelHistory });
                    this.MenuItems.Add(new CollectionData { Data = Strings.RefreshData });

                    // TODO put in an authorisation or setting.
                    //if (ApplicationSettings.DispatchMode == DispatchMode.ScanStock)
                   // {
                        this.MenuItems.Add(new CollectionData { Data = Strings.RunStartOfDay });
                    //}

                    if (ApplicationSettings.DisplayReportsOnTouchscreen)
                    {
                        this.MenuItems.Add(new CollectionData { Data = Strings.Reports });
                    }

                    this.MenuItems.Add(new CollectionData { Data = Strings.ZeroScales });
                    this.MenuItems.Add(new CollectionData { Data = Strings.SendCommandToPrinter });
                    this.MenuItems.Add(new CollectionData { Data = ApplicationSettings.SortMode ? Strings.SortProductsOn : Strings.SortProductsOff });
                    this.MenuItems.Add(new CollectionData { Data = ApplicationSettings.SortGroupMode ? Strings.SortProductGroupsOn : Strings.SortProductGroupsOff });
                    this.MenuItems.Add(new CollectionData { Data = Strings.LogOutLwr });
                    this.MenuItems.Add(new CollectionData { Data = MenuItem.SystemInformation });
                    this.MenuItems.Add(new CollectionData { Data = close });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ShutdownPC });
                    this.MenuItems.Add(new CollectionData { Data = Strings.AlibiData});

                    break;

                case ViewType.ARDispatch:
                    this.MenuItems.Add(new CollectionData { Data = Strings.LabelHistory });
                    this.MenuItems.Add(new CollectionData { Data = Strings.RefreshData });

                    // TODO put in an authorisation or setting.
                    //if (ApplicationSettings.DispatchMode == DispatchMode.ScanStock)
                    //{
                        this.MenuItems.Add(new CollectionData { Data = Strings.RunStartOfDay });
                    //}

                    if (ApplicationSettings.DispatchMode == DispatchMode.DispatchRecordWeight)
                    {
                        this.MenuItems.Add(new CollectionData { Data = Strings.ChangeWeighMode });
                    }

                    if (ApplicationSettings.DisplayReportsOnTouchscreen)
                    {
                        this.MenuItems.Add(new CollectionData { Data = Strings.Reports });
                    }

                    this.MenuItems.Add(new CollectionData { Data = Strings.StockTake});
                    this.MenuItems.Add(new CollectionData { Data = Strings.ZeroScales });
                    this.MenuItems.Add(new CollectionData { Data = Strings.SendCommandToPrinter });
                    this.MenuItems.Add(new CollectionData { Data = Strings.LogOutLwr });
                    this.MenuItems.Add(new CollectionData { Data = MenuItem.SystemInformation });
                    this.MenuItems.Add(new CollectionData { Data = close });
                    this.MenuItems.Add(new CollectionData { Data = Strings.ShutdownPC });
                
                    break;
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the selected menu item.
        /// </summary>
        public ICommand MenuItemSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the selected menu item.
        /// </summary>
        public ICommand LogOutCommand { get; private set; }

        /// <summary>
        /// Gets the command to move back to the orders screen.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the ui load event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
         	// not implemented
        }

        protected override void CancelSelectionCommandExecute()
        {
            // not implemented
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Handle the menu item selection.
        /// </summary>
        private void MenuItemSelectedCommandExecute()
        {
            if (this.selectedMenuItem.Data.Equals(MenuItem.EnableLineReOrdering))
            {
                if (!this.Locator.Sequencer.CanReorderLine)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoLineReorderingAuthorisation);
                    NouvemMessageBox.Show(Message.NoLineReorderingAuthorisation, flashMessage:true);
                    return;
                }

                this.Locator.Sequencer.EnableReorderLine = true;
                SystemMessage.Write(MessageType.Priority, Message.LineReorderingEnabled);
                NouvemMessageBox.Show(Message.LineReorderingEnabled, touchScreen: true, flashMessage:true);
                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.AlibiData))
            {
                this.Locator.FactoryScreen.SetMainViewModel(this.Locator.Alibi);
                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.ZeroScales))
            {
                this.IndicatorManager.ZeroScales();
                SystemMessage.Write(MessageType.Priority, string.Format(Message.ScalesZeroed, ApplicationSettings.ZeroScalesCommand));
                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.SendCommandToPrinter))
            {
                this.PrintManager.SendCommand(ApplicationSettings.PrinterCommand, true);
                SystemMessage.Write(MessageType.Priority, string.Format(Message.CommandSentToPrinter, ApplicationSettings.PrinterCommand));
                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.ChangeWeighMode))
            {
                var msg = Message.WeighModeSetToAuto;
                if (this.Locator.Touchscreen.RecordButtonContent == Strings.Record)
                {
                    this.Locator.Indicator.WeighMode = WeighMode.Auto;
                }
                else
                {
                    this.Locator.Indicator.WeighMode = WeighMode.Manual;
                    msg = Message.WeighModeSetToManual;
                }

                SystemMessage.Write(MessageType.Priority, msg);
                return;
            }
            
            if (this.selectedMenuItem.Data.Equals(MenuItem.Reports))
            {
                var reportsToDisplay = ApplicationSettings.ReportsOnTouchscreen.Split(',');

                var data = (from report in this.ReportManager.Reports
                            where reportsToDisplay.Contains(report.Name)
                            select new CollectionData { Data = report.Name, Identifier = ViewType.Menu.ToString() })
                   .ToList();

                this.Locator.CollectionDisplay.IsReport = true;
                Messenger.Default.Send(data, Token.DisplayTouchscreenCollection);
                Messenger.Default.Send(ViewType.ReportDisplay);
                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.LogOut))
            {
                this.LogOutCommandExecute();
                return;
            }

            if (this.selectedMenuItem.Data.Equals(Strings.StockTake))
            {
                Messenger.Default.Send(Token.Message, Token.ShowStockTake);
                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.CloseApplication))
            {
                //NouvemMessageBox.Show(Message.ConfirmApplicationClose, NouvemMessageBoxButtons.OKCancel, true);
                //if (NouvemMessageBox.UserSelection == UserDialogue.OK)
                //{
                    this.Log.LogInfo(this.GetType(), "************* Closing Application *************");
                    //this.ClosePorts();
                    this.DataManager.SaveDeviceSettings();
                    Application.Current.Shutdown();
                    Process.GetCurrentProcess().Kill();
                //}

                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.StartOfDay))
            {
                NouvemMessageBox.Show(Message.StartOfDayConfirmation, NouvemMessageBoxButtons.OKCancel, true);
                if (NouvemMessageBox.UserSelection == UserDialogue.OK)
                {
                    try
                    {
                        NouvemGlobal.RefreshData();
                        this.DataManager.DisableAddingStockToBox();
                        SystemMessage.Write(MessageType.Priority, Message.StartOfDayComplete);
                    }
                    catch (Exception ex)
                    {
                        SystemMessage.Write(MessageType.Issue, Message.StartOfDayNotCompleted);
                        this.Log.LogError(this.GetType(), ex.Message);
                    }
                }

                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.SystemInformation))
            {
                Messenger.Default.Send(Token.Message, Token.CreateSystemInformation);
                return;
            }

            if (this.selectedMenuItem.Data.StartsWith("Sort Products"))
            {
                this.Locator.ProductionProductsCards.SortMode = !this.Locator.ProductionProductsCards.SortMode;
                var msg = string.Format(Message.SortProductsMode, this.Locator.ProductionProductsCards.SortMode.BoolToOnOff());
                SystemMessage.Write(MessageType.Priority,msg);

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(msg, flashMessage:true);
                }));

                this.SelectedMenuItem.Data = ApplicationSettings.SortMode
                    ? Strings.SortProductsOn
                    : Strings.SortProductsOff;

                return;
            }

            if (this.selectedMenuItem.Data.StartsWith("Sort Product Groups"))
            {
                this.Locator.ProductionProductsCards.SortGroupMode = !this.Locator.ProductionProductsCards.SortGroupMode;
                var msg = string.Format(Message.SortProductGroupsMode, this.Locator.ProductionProductsCards.SortGroupMode.BoolToOnOff());
                SystemMessage.Write(MessageType.Priority, msg);

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    NouvemMessageBox.Show(msg, flashMessage: true);
                }));

                this.SelectedMenuItem.Data = ApplicationSettings.SortGroupMode
                    ? Strings.SortProductGroupsOn
                    : Strings.SortProductGroupsOff;

                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.CloseModule))
            {
                this.Log.LogInfo(this.GetType(), "Attempting to close application");
                //if (!this.AuthorisationsManager.AllowUserToCloseApplicationOnTouchscreen)
                //{
                //    SystemMessage.Write(MessageType.Issue, Message.NoAuthoristionToCloseApplication);
                //    return;
                //}
             
                ViewModelLocator.ClearSequencer();
                ViewModelLocator.ClearGrader();
                Messenger.Default.Send(Token.Message, Token.CloseFactoryWindow);
                Messenger.Default.Send(Token.Message, Token.CloseWorkflow);
                Messenger.Default.Send(Token.Message, Token.CloseMenuWindow);
                return;
            }

            if (this.selectedMenuItem.Data.Equals(MenuItem.ShutdownPC))
            {
                this.Log.LogInfo(this.GetType(), "Attempting to shut down pc");
                if (!this.AuthorisationsManager.AllowUserToShutdownTouchscreenPC)
                {
                    SystemMessage.Write(MessageType.Issue, Message.NoAuthoristionToShutdownTerminal);
                    return;
                }

                NouvemMessageBox.Show(Message.ConfirmPCShutdown, NouvemMessageBoxButtons.OKCancel, true);
                if (NouvemMessageBox.UserSelection == UserDialogue.OK)
                {
                    System.Diagnostics.Process.Start("shutdown.exe", "-s"); 
                }

                return;
            }

            this.IsBusy = true;
            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(150);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (this.selectedMenuItem.Data.Equals(MenuItem.LabelHistory))
                    {
                        this.Locator.FactoryScreen.SetMainViewModel(this.Locator.TransactionManager);
                        return;
                    }

                    if (this.selectedMenuItem.Data.Equals(MenuItem.RefreshData))
                    {
                        Messenger.Default.Send(Token.Message, Token.LabelAssociationsUpdated);
                        NouvemGlobal.RefreshData();
                        ViewModelLocator.CleanUpTouchscreenChildModules();
                        SystemMessage.Write(MessageType.Priority, Message.DataRefreshed);
                        return;
                    }
                });
            }).ContinueWith(task => this.IsBusy = false);
        }

        #endregion

        #region private

        /// <summary>
        /// Close the ports.
        /// </summary>
        private void ClosePorts()
        {
            try
            {
                this.IndicatorManager.CloseIndicatorPort();
                this.PrintManager.ClosePrinterPort();
                this.HardwareManager.CloseScannerPort();
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), string.Format("Menu - Closing Ports:{0}", e.Message));
            }
        }

        /// <summary>
        /// Handles the report selection.
        /// </summary>
        /// <param name="reportData">The selected report data.</param>
        private void HandleReportSelection(CollectionData reportData)
        {
            this.Log.LogDebug(this.GetType(), string.Format("HandleReportSelection(): Report selected:{0}", reportData.Data));
            if (reportData.Identifier != ViewType.Menu.ToString())
            {
                return;
            }

            this.AddMenuItems(this.Module);

            var reportName = reportData.Data;
            this.ReportManager.ProcessReport("", reportName, true);
            //var report = this.ReportManager.Reports.FirstOrDefault(x => x.Name == reportName);
            //this.Log.LogDebug(this.GetType(), string.Format("Checking for match: Reports count{0}, match:{1}", this.ReportManager.Reports.Count, report != null));
            //if (report != null)
            //{
            //    this.Locator.Master.DisplayTouchscreenReport(report);
            //}
        }

        /// <summary>
        /// Logs the user out.
        /// </summary>
        private void LogOutCommandExecute()
        {
            NouvemMessageBox.Show(Message.LogOutConfirmation, NouvemMessageBoxButtons.OKCancel, true);
            if (NouvemMessageBox.UserSelection == UserDialogue.OK)
            {
                this.Log.LogInfo(this.GetType(), "************* Closing/Restarting Application *************");
                //this.ClosePorts();
                Settings.Default.AllowMultipleInstancesOfApplication = true;
                Settings.Default.Save();
                //ViewModelLocator.CleanUpTouchscreens();
                this.DataManager.LogOut();
                //this.DataManager.SaveDeviceSettings();
                System.Windows.Forms.Application.Restart();
                Application.Current.Shutdown();
                Process.GetCurrentProcess().Kill();
                //Messenger.Default.Send(Token.Message, Token.CreateTouchscreenLogin);
            }
        }

        private void CloseSelectionModules()
        {
            
        }

        #endregion

        #endregion
    }
}
