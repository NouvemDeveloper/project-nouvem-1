﻿// -----------------------------------------------------------------------
// <copyright file="CollectionDisplayViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.ViewModel.Touchscreen
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Properties;
    using Nouvem.Global;

    public class CollectionDisplayViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The incoming collection to display.
        /// </summary>
        private ObservableCollection<CollectionData> displayItems = new ObservableCollection<CollectionData>();

        /// <summary>
        /// The incoming collection to display.
        /// </summary>
        private IList<CollectionData> allDisplayItems = new List<CollectionData>();

        /// <summary>
        /// The selected display item.
        /// </summary>
        private CollectionData selectedDisplayItem;

        /// <summary>
        /// Flag as to whether the selected value is to be sent.
        /// </summary>
        private bool canSend;

        /// <summary>
        /// The search text entered.
        /// </summary>
        private string searchText;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionDisplayViewModel"/> class.
        /// </summary>
        public CollectionDisplayViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for incoming data.
            Messenger.Default.Register<List<object>>(this, Token.DisplayTouchscreenCollection, i =>
            {
                // add our incoming data to our display data.
                this.DisplayItems = new ObservableCollection<CollectionData>(from item in i
                                                                             select new CollectionData { Data = item.ToString(), Identifier = Constant.TouchscreenTraceability });
                this.allDisplayItems = this.DisplayItems.ToList();
            });

            // Register for incoming data.
            Messenger.Default.Register<List<CollectionData>>(this, Token.DisplayTouchscreenCollection, i =>
            {
                // add our incoming data to our display data.
                this.DisplayItems = new ObservableCollection<CollectionData>(i);
                this.allDisplayItems = this.DisplayItems.ToList();
            });

            // Register for incoming data.
            Messenger.Default.Register<List<string>>(this, Token.DisplayTouchscreenCollection, i =>
            {
                // add our incoming data to our display data.
                this.DisplayItems = new ObservableCollection<CollectionData>(from item in i
                                                                             select new CollectionData { Data = item, Identifier = Constant.TouchscreenCollection });
                this.allDisplayItems = this.DisplayItems.ToList();
            });

            #endregion

            #region command

            // Search for an order.
            this.FindItemCommand = new RelayCommand(() =>
            {
                this.SearchText = string.Empty;
                Messenger.Default.Send(Token.Message, Token.DisplayCollectionSearchKeyboard);
            });

            // Move back to main
            this.MoveBackCommand = new RelayCommand(this.Close);

            // Handle the load event
            this.OnLoadedCommand = new RelayCommand(() =>
            {
                this.IsFormLoaded = true;
                this.SelectedDisplayItem = null;
            });

            // Handle the unload event.
            this.OnUnloadedCommand = new RelayCommand(() =>
            {
                this.DisplayItems?.Clear();
                this.DisplayItems = null;
                this.allDisplayItems?.Clear();
                this.allDisplayItems = null;
                this.IsReport = false;
                this.IsFormLoaded = false;
            });

            // Handle the product selection.
            this.ItemSelectedCommand = new RelayCommand(() =>
            {
                if (this.SelectedDisplayItem == null)
                {
                    return;
                }

                Messenger.Default.Send(this.selectedDisplayItem, Token.CollectionDisplayValue);
                this.Close();
            });

            #endregion
        }

        #endregion

        #region public interface

        #region property

        public bool IsReport { get; set; }

        /// <summary>
        /// Gets or sets the text entered to search by.
        /// </summary>
        public string SearchText
        {
            get
            {
                return this.searchText;
            }

            set
            {
                this.searchText = value;
                this.RaisePropertyChanged();
                this.DisplayItems.Clear();

                if (string.IsNullOrWhiteSpace(value))
                {
                    foreach (var collectionData in this.allDisplayItems)
                    {
                        this.DisplayItems.Add(collectionData);
                    }
                }
                else
                {
                    foreach (var collectionData in this.allDisplayItems.Where(x => x.Data != null && x.Data.StartsWithIgnoringCase(value)))
                    {
                        this.DisplayItems.Add(collectionData);
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the incoming items to display.
        /// </summary>
        public ObservableCollection<CollectionData> DisplayItems
        {
            get
            {
                return this.displayItems;
            }

            set
            {
                this.displayItems = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the incoming items to display.
        /// </summary>
        public CollectionData SelectedDisplayItem
        {
            get
            {
                return this.selectedDisplayItem;
            }

            set
            {
                this.selectedDisplayItem = value;
                this.RaisePropertyChanged();
                if (value != null && this.IsReport)
                {
                    Messenger.Default.Send(value, Token.CollectionDisplayValue);
                    this.Close();
                }
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command to handle the loaded event.
        /// </summary>
        public ICommand OnLoadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the unloaded event.
        /// </summary>
        public ICommand OnUnloadedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the item selection.
        /// </summary>
        public ICommand FindItemCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the item selection.
        /// </summary>
        public ICommand ItemSelectedCommand { get; private set; }

        /// <summary>
        /// Gets the command to handle the move back command.
        /// </summary>
        public ICommand MoveBackCommand { get; private set; }

        #endregion

        #endregion

        #region protected

        protected override void ControlSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        protected override void CancelSelectionCommandExecute()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region private

        /// <summary>
        /// Clear, and move back to the main ui.
        /// </summary>
        private void Close()
        {
            this.IsReport = false;
            Messenger.Default.Send(Token.Message, Token.CloseContainerWindow);
            this.DisplayItems?.Clear();
        }

        #endregion
    }
}

