﻿// -----------------------------------------------------------------------
// <copyright file="SqlGeneratorViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ViewModel.Utility
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.Enum;
    using Nouvem.Global;
    using Nouvem.Shared.Localisation;

    public class SqlGeneratorViewModel : NouvemViewModelBase
    {
        #region field

        /// <summary>
        /// The sql script.
        /// </summary>
        private string sqlScript;

        /// <summary>
        /// The sql data string collection.
        /// </summary>
        private ObservableCollection<CollectionData> sqlData;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the SqlGeneratorViewModel class.
        /// </summary>
        public SqlGeneratorViewModel()
        {
            if (this.IsInDesignMode)
            {
                return;
            }

            #region message registration

            // Register for setting the incoming sql.
            Messenger.Default.Register<string>(this, Token.SetSql, s => this.SqlScript = s);

            #endregion

            #region command

            // Command handler for the sql execution.
            this.GetDataCommand = new RelayCommand(this.GetDataCommandExecute);

            #endregion
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets or sets the sql data.
        /// </summary>
        public ObservableCollection<CollectionData> SqlData
        {
            get
            {
                return this.sqlData;
            }

            set
            {
                this.sqlData = value;
                this.RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the sql script.
        /// </summary>
        public string SqlScript
        {
            get
            {
                return this.sqlScript;
            }

            set
            {
                this.sqlScript = value;
                this.RaisePropertyChanged();
            }
        }

        #endregion

        #region command

        /// <summary>
        /// Gets the command the execute the input sql, returning the data.
        /// </summary>
        public ICommand GetDataCommand { get; private set; }

        #endregion

        #endregion

        #region protected override

        /// <summary>
        /// Overrides the mode selection, to set up a new description creation.
        /// </summary>
        /// <param name="mode">The mode to move into.</param>
        protected override void ControlCommandExecute(string mode)
        {
            this.SetControlMode(ControlMode.OK);
        }

        /// <summary>
        /// Overrides the base control selection.
        /// </summary>
        protected override void ControlSelectionCommandExecute()
        {
            switch (this.CurrentMode)
            {
                case ControlMode.OK:
                    this.SendScript();
                    break;
            }
        }

        /// <summary>
        /// Make a call to close.
        /// </summary>
        protected override void CancelSelectionCommandExecute()
        {
            this.Close();
        }

        #endregion

        #region private

        #region command execution

        /// <summary>
        /// Gets the returned data.
        /// </summary>
        private void GetDataCommandExecute()
        {
            this.ValidateSql();
        }

        #endregion

        #region helper

        /// <summary>
        /// Validates the input sql.
        /// </summary>
        /// <returns>A flag, indicating  whether the input sql is valid or not.</returns>
        private bool ValidateSql()
        {
            #region validation

            if (string.IsNullOrWhiteSpace(this.sqlScript))
            {
                SystemMessage.Write(MessageType.Issue, Message.EmptySql);
                return false;
            }

            if (this.InvalidSql())
            {
                SystemMessage.Write(MessageType.Issue, Message.InvalidSql);
                return false;
            }

            #endregion

            try
            {
                var localData = (from data in this.DataManager.GetData(this.sqlScript)
                                 select new CollectionData { Data = data.ToString() })
                           .ToList();

                this.SqlData = new ObservableCollection<CollectionData>(localData);
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                SystemMessage.Write(MessageType.Issue, Message.InvalidSql);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Close and clean up.
        /// </summary>
        private void Close()
        {
            ViewModelLocator.ClearSqlGenerator();
            Messenger.Default.Send(Token.Message, Token.CloseSqlGeneratorWindow);
        }
       
        /// <summary>
        /// Sends the generated sql to interested observers.
        /// </summary>
        private void SendScript()
        {
            if (!this.ValidateSql())
            {
                return;
            }

            Messenger.Default.Send(this.SqlScript, Token.SqlScript);
            this.Close();
        }

        /// <summary>
        /// Determines if the sql is valid i.e. select only.
        /// </summary>
        /// <returns>A flag, that indicates if the sql is invalid.</returns>
        private bool InvalidSql()
        {
            return !this.sqlScript.ToLower().Contains("select") || this.sqlScript.ToLower().Contains("alter") ||
                   this.sqlScript.ToLower().Contains("update") ||
                   this.sqlScript.ToLower().Contains("delete") || this.sqlScript.ToLower().Contains("truncate");
        }

        #endregion

        #endregion
    }
}
