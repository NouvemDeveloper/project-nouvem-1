﻿// -----------------------------------------------------------------------
// <copyright file="InventoryManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Manager class for the inventory.
    /// </summary>
    public class InventoryManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly InventoryManager Manager = new InventoryManager();

        #endregion

        #region constructor

        private InventoryManager()
        {
        }

        #endregion

        #region property

        /// <summary>
        /// Gets the inventory manager singleton.
        /// </summary>
        public static InventoryManager Instance
        {
            get
            {
                return Manager;
            }
        }

        #endregion

        #region method

       

        #endregion
    }
}
