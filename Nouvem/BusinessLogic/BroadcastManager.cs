﻿// -----------------------------------------------------------------------
// <copyright file="BatchManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Net;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Threading;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Shared;

    public class BroadcastManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly BroadcastManager Manager = new BroadcastManager();

        #endregion

        #region constructor

        private BroadcastManager()
        {
            //this.StartListening();
        }

        #endregion

        #region public interface

        #region property

        /// <summary>
        /// Gets the batch manager singleton.
        /// </summary>
        public static BroadcastManager Instance
        {
            get
            {
                return Manager;
            }
        }

        private readonly UdpClient udp = new UdpClient(15000);
        public void StartListening()
        {
            this.udp.BeginReceive(this.Receive, new object());
        }

        private void Receive(IAsyncResult ar)
        {
            IPEndPoint ip = new IPEndPoint(IPAddress.Any, 15000);
            byte[] bytes = udp.EndReceive(ar, ref ip);
            string message = Encoding.ASCII.GetString(bytes);

            if (message.ToLower().Equals("Update Partners".ToLower()))
            {
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Messenger.Default.Send(Token.Message, Token.UpdatePartners);
                }));
          
            }

            
        
            this.StartListening();
        }

        #endregion


        #endregion

    }
}

