﻿// -----------------------------------------------------------------------
// <copyright file="HardwareManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Windows.Threading;
using Nouvem.Model.Enum;

namespace Nouvem.BusinessLogic
{
    using System.Linq;
    using System.Windows;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Hardware;
    using Nouvem.Shared;

    /// <summary>
    /// Manager class for the application hardware.
    /// </summary>
    public class HardwareManager : BusinessLogicBase
    {
        #region field

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly HardwareManager Manager = new HardwareManager();

        #endregion

        #region constructor

        private HardwareManager()
        {
            this.Scanner = new Scanner();
            this.SheepWand = new Scanner();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets the hardware manager singleton.
        /// </summary>
        public static HardwareManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets or sets the scanner.
        /// </summary>
        public Scanner Scanner { get; set; }

        /// <summary>
        /// Gets or sets the sheep wand.
        /// </summary>
        public Scanner SheepWand { get; set; }

        #endregion

        #region method

        #region scanner

        /// <summary>
        /// Connects to the scanner.
        /// </summary>
        public void ConnectToScanner()
        {
            if (ApplicationSettings.ConnectToScanner)
            {
                this.SetUpScanner();
                this.OpenScannerPort();
            }

            this.ConnectToWand();
        }

        /// <summary>
        /// Opens a connection to the scanner.
        /// </summary>
        public void SetUpScanner()
        {
            var settings = ApplicationSettings.ScannerSettings.Split(',');
            var com = settings.ElementAt(0);
            var baud = settings.ElementAt(1);
            var parity = settings.ElementAt(2);
            var stopBits = settings.ElementAt(3);
            var dataBits = settings.ElementAt(4);

            this.Log.LogInfo(this.GetType(), string.Format("Setting up scanner():Com:{0}, Baud:{1}, Parity:{2}, StopBits:{3}, DataBits:{4}, Wait:{5}", com, baud, parity, stopBits, dataBits, ApplicationSettings.ScannerPortDataWaitTime));
            this.Scanner = new Scanner(com, baud, parity, stopBits, dataBits, ApplicationSettings.ScannerPortDataWaitTime);

            this.Scanner.DataReceived += (sender, args)
                => Application.Current.Dispatcher.BeginInvoke(
                   DispatcherPriority.Background,
                      new Action(() =>
                      {
                          Messenger.Default.Send(args.SerialData, Token.Scanner);
                      }));
        }

        /// <summary>
        /// Opens the port.
        /// </summary>
        /// <returns>A flag, indicating a successful port opening.</returns>
        public bool OpenScannerPort()
        {
            this.Log.LogInfo(this.GetType(), "OpenScannerPort(): Opening the scanner connection");
            try
            {
                this.Scanner.OpenPort();
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), ex.Message);
            }

            this.Log.LogInfo(this.GetType(), string.Format("Port open:{0}", this.Scanner.IsPortOpen));
            return this.Scanner.IsPortOpen;
        }

        /// <summary>
        /// Closes the port.
        /// </summary>
        public void CloseScannerPort()
        {
            this.Log.LogDebug(this.GetType(), "CloseScannerPort(): Closing the scanner connection");
            if (this.Scanner.IsPortOpen)
            {
                this.Scanner.ClosePort();
                this.Log.LogDebug(this.GetType(), "Port closed");
            }
        }

        /// <summary>
        /// Determines if the port is open.
        /// </summary>
        /// <returns>Flag, as to whether the port is open.</returns>
        public bool IsScannerPortOpen()
        {
            this.Log.LogDebug(this.GetType(), "IsScannerPortOpen(): Detecting..");
            var result = this.Scanner != null && this.Scanner.IsPortOpen;
            this.Log.LogDebug(this.GetType(), string.Format("Port Open:{0}", result.BoolToYesNo()));
            return result;
        }

        /// <summary>
        /// Connects to the scanner.
        /// </summary>
        public void ConnectToWand()
        {
            if (ApplicationSettings.ConnectToWand)
            {
                this.SetUpWand();
            }
        }

        /// <summary>
        /// Opens a connection to the scanner.
        /// </summary>
        public void SetUpWand()
        {
            var settings = ApplicationSettings.WandSettings.Split(',');
            var com = settings.ElementAt(0);
            var baud = string.Empty;
            var parity = string.Empty;
            var stopBits = string.Empty;
            var dataBits = string.Empty;

            if (settings.Length > 1)
            {
                baud = settings.ElementAt(1);
                parity = settings.ElementAt(2);
                stopBits = settings.ElementAt(3);
                dataBits = settings.ElementAt(4);
                this.Log.LogInfo(this.GetType(), string.Format("Setting up wand: Com:{0}, Baud:{1}, Parity:{2}, StopBits:{3}, DataBits:{4}, Wait:{5}", com, baud, parity, stopBits, dataBits, ApplicationSettings.ScannerPortDataWaitTime));
                this.SheepWand = new Scanner(com, baud, parity, stopBits, dataBits, ApplicationSettings.WandPortDataWaitTime);
            }
            else
            {
                this.Log.LogInfo(this.GetType(), string.Format("Setting up wand: Com:{0}, Wait:{1}", com, ApplicationSettings.ScannerPortDataWaitTime));
                this.SheepWand = new Scanner(com, ApplicationSettings.WandPortDataWaitTime);
            }

            this.SheepWand.DataReceived += (sender, args)
                => Application.Current.Dispatcher.BeginInvoke(
                   DispatcherPriority.Background,
                      new Action(() =>
                      {
                          Messenger.Default.Send(args.SerialData, Token.Wand);
                      }));
        }

        /// <summary>
        /// Opens the port.
        /// </summary>
        /// <returns>A flag, indicating a successful port opening.</returns>
        public bool OpenWandPort()
        {
            try
            {
                this.Log.LogInfo(this.GetType(), "OpenWandPort(): Opening the wand connection");
                if (!this.SheepWand.IsPortOpen)
                {
                    this.SheepWand.OpenPort();
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }

            this.Log.LogInfo(this.GetType(), string.Format("Port open:{0}", this.SheepWand.IsPortOpen));
            return this.SheepWand.IsPortOpen;
        }

        /// <summary>
        /// Closes the port.
        /// </summary>
        public void CloseWandPort()
        {
            this.Log.LogInfo(this.GetType(), "CloseWandPort(): Closing the wand connection");
            try
            {
                if (this.SheepWand.IsPortOpen)
                {
                    this.SheepWand.ClosePort();
                    this.Log.LogInfo(this.GetType(), "Port closed");
                }
            }
            catch (Exception e)
            {
                this.Log.LogError(this.GetType(), e.Message);
                SystemMessage.Write(MessageType.Issue, e.Message);
            }
        }

        /// <summary>
        /// Determines if the port is open.
        /// </summary>
        /// <returns>Flag, as to whether the port is open.</returns>
        public bool IsWandPortOpen()
        {
            this.Log.LogDebug(this.GetType(), "IsWandPortOpen(): Detecting..");
            var result = this.SheepWand != null && this.SheepWand.IsPortOpen;
            this.Log.LogDebug(this.GetType(), string.Format("Port Open:{0}", result.BoolToYesNo()));
            return result;
        }

        #endregion

        #endregion

        #region private

        #endregion
    }
}
