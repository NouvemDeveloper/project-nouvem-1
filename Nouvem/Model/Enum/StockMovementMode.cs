﻿// -----------------------------------------------------------------------
// <copyright file="StockMovementMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum StockMovementMode
    {
        /// <summary>
        /// Into Cookers mode.
        /// </summary>
        LowRiskCook,

        /// <summary>
        /// Into smokers mode
        /// </summary>
        Smokers,

        /// <summary>
        /// Into chillers mode.
        /// </summary>
        HighRiskCook,

        /// <summary>
        /// Packing mode.
        /// </summary>
        Packing,

        /// <summary>
        /// Packing mode.
        /// </summary>
        HighRiskJoints,

        /// <summary>
        /// Packing mode.
        /// </summary>
        HighRiskSlicing,

        /// <summary>
        /// Split batch mode.
        /// </summary>
        SplitBatch,

        /// <summary>
        /// Coldstores mode.
        /// </summary>
        Coldstores,
    }
}
