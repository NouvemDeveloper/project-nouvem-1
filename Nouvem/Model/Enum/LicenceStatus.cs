﻿// -----------------------------------------------------------------------
// <copyright file="LicenceStatus.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// Enumeration which models a licence status.
    /// </summary>
    public enum LicenceStatus
    {
        /// <summary>
        /// Current, valid licence.
        /// </summary>
        Valid,

        /// <summary>
        /// No licence allocated.
        /// </summary>
        NoLicence,

        /// <summary>
        /// Licence expired.
        /// </summary>
        Expired,

        /// <summary>
        /// /The in-house nouvem installer/engineer licence.
        /// </summary>
        Nouvem
    }
}
