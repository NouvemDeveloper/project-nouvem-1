﻿// -----------------------------------------------------------------------
// <copyright file="IntakeMode.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum GraderMode
    {
        /// <summary>
        /// Grading beef
        /// </summary>
        Beef,

        /// <summary>
        /// Grading sheep
        /// </summary>
        Sheep,

        /// <summary>
        /// Grading pig.
        /// </summary>
        Pig
    }
}

