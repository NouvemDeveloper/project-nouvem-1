﻿// -----------------------------------------------------------------------
// <copyright file="ControlMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// Enumeration to model the control button modes.
    /// </summary>
    public enum ControlMode
    {
        /// <summary>
        /// Control ok mode
        /// </summary>
        OK,

        /// <summary>
        /// Control ok mode
        /// </summary>
        Add,

        /// <summary>
        /// Control update mode
        /// </summary>
        Update,

        /// <summary>
        /// Control find mode
        /// </summary>
        Find,

         /// <summary>
        /// Control export mode
        /// </summary>
        Export,

        /// <summary>
        /// Control import mode
        /// </summary>
        Import,

        /// <summary>
        /// Control delete mode
        /// </summary>
        Delete,

        /// <summary>
        /// Control reconcile mode
        /// </summary>
        Reconcile,

        Copy
    }
}
