﻿// -----------------------------------------------------------------------
// <copyright file="MessageType.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// Enumeration of the possible information types.
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// The background message type.
        /// </summary>
        Background,

        /// <summary>
        /// The priority message type.
        /// </summary>
        Priority,

        /// <summary>
        /// The warning message type.
        /// </summary>
        Warning,

        /// <summary>
        /// The issue message type.
        /// </summary>
        Issue
    }
}
