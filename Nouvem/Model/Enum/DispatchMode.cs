﻿// -----------------------------------------------------------------------
// <copyright file="DispatchMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum DispatchMode
    {
        /// <summary>
        /// Scan stock to dispatch.
        /// </summary>
        Dispatch,

        /// <summary>
        /// Scan stock to dispatch.
        /// </summary>
        DispatchBatch,

        /// <summary>
        /// Record weight and traceability attributes.
        /// </summary>
        DispatchRecordWeight,

        /// <summary>
        /// Record weight and traceability attributes.
        /// </summary>
        DispatchRecordWeightWithNotes,

        /// <summary>
        /// Record weight and traceability attributes.
        /// </summary>
        DispatchRecordWeightWithShipping,

        /// <summary>
        /// Scan or weigh mode.
        /// </summary>
        ScanOrWeight
    }
}
