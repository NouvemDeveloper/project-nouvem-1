﻿// -----------------------------------------------------------------------
// <copyright file="LabeType.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum LabelType
    {
        /// <summary>
        /// The box label type.
        /// </summary>
        Box,

        /// <summary>
        /// The item label type.
        /// </summary>
        Item,

        /// <summary>
        /// The piece label type.
        /// </summary>
        Piece,

        /// <summary>
        /// The pallet label type.
        /// </summary>
        Pallet,

        /// <summary>
        /// The shipping label type.
        /// </summary>
        Shipping
    }
}
