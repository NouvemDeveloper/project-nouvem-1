﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.Enum
{
    public enum BatchNumberType
    {
        /// <summary>
        /// Creates a new batch only when user decides.
        /// </summary>
        Standard,

        /// <summary>
        /// Creates a new batch for every product.
        /// </summary>
        Colour
    }
}
