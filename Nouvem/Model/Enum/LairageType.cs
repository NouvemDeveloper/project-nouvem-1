﻿// -----------------------------------------------------------------------
// <copyright file="ControlMode.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// Enumeration to model the control button modes.
    /// </summary>
    public enum LairageType
    {
        Ireland,
        UK,
        NI
    }
}

