﻿// -----------------------------------------------------------------------
// <copyright file="OutOfProductionPanel.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    public enum OutOfProductionPanel
    {
        /// <summary>
        /// Standard intake
        /// </summary>
        Standard,

        /// <summary>
        /// Production order created as part of intake.
        /// </summary>
        WithPieces
    }
}
