﻿// -----------------------------------------------------------------------
// <copyright file="ResidencyCalculationType.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.Enum
{
    /// <summary>
    /// An enumeration for the types of residency calculation that are supported.
    /// </summary>
    public enum ResidencyCalculationType
    {
        /// <summary>
        /// No type specified
        /// </summary>
        None,

        /// <summary>
        /// The number of days spent on the current farm.
        /// </summary>
        CurrentFarm,

        /// <summary>
        /// The number of days spent on the last farm.
        /// </summary>
        PreviousFarm,

        /// <summary>
        /// The number days spent on all farms.
        /// </summary>
        TotalFarm
    }
}
