//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class INMaster
    {
        public INMaster()
        {
            this.APGoodsReceiptDetails = new HashSet<APGoodsReceiptDetail>();
            this.APQuoteDetails = new HashSet<APQuoteDetail>();
            this.ARInvoiceDetails = new HashSet<ARInvoiceDetail>();
            this.AROrderDetails = new HashSet<AROrderDetail>();
            this.ARQuoteDetails = new HashSet<ARQuoteDetail>();
            this.INAttachments = new HashSet<INAttachment>();
            this.INPropertySelections = new HashSet<INPropertySelection>();
            this.InvoiceDetails = new HashSet<InvoiceDetail>();
            this.PROrderInputs = new HashSet<PROrderInput>();
            this.PROrderOutputs = new HashSet<PROrderOutput>();
            this.StockTakeDetails = new HashSet<StockTakeDetail>();
            this.StockTransactions = new HashSet<StockTransaction>();
            this.SpecialPriceLists = new HashSet<SpecialPriceList>();
            this.ARDispatchDetails = new HashSet<ARDispatchDetail>();
            this.CarcassSplitOptions = new HashSet<CarcassSplitOption>();
            this.LairageOrderDetails = new HashSet<LairageOrderDetail>();
            this.DateDays = new HashSet<DateDay>();
            this.PRSpecOutputs = new HashSet<PRSpecOutput>();
            this.LabelAssociations = new HashSet<LabelAssociation>();
            this.PROrderDetails = new HashSet<PROrderDetail>();
            this.INMasterStockDatas = new HashSet<INMasterStockData>();
            this.PriceListDetails = new HashSet<PriceListDetail>();
            this.APInvoiceDetails = new HashSet<APInvoiceDetail>();
            this.ARReturnDetails = new HashSet<ARReturnDetail>();
            this.ARReturnInvoiceDetails = new HashSet<ARReturnInvoiceDetail>();
            this.APReturnDetails = new HashSet<APReturnDetail>();
            this.APReturnInvoiceDetails = new HashSet<APReturnInvoiceDetail>();
            this.StockMoveOrderDetails = new HashSet<StockMoveOrderDetail>();
            this.APOrderDetails = new HashSet<APOrderDetail>();
            this.BarcodeParses = new HashSet<BarcodeParse>();
        }
    
        public int INMasterID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int INGroupID { get; set; }
        public Nullable<int> TraceabilityTemplateNameID { get; set; }
        public Nullable<int> DateTemplateNameID { get; set; }
        public Nullable<int> QualityTemplateNameID { get; set; }
        public Nullable<bool> StockItem { get; set; }
        public Nullable<bool> SalesItem { get; set; }
        public Nullable<bool> PurchaseItem { get; set; }
        public Nullable<bool> FixedAsset { get; set; }
        public Nullable<int> BoxTareContainerID { get; set; }
        public Nullable<int> PiecesTareContainerID { get; set; }
        public Nullable<int> PalletTareContainerID { get; set; }
        public Nullable<decimal> MinWeight { get; set; }
        public Nullable<decimal> MaxWeight { get; set; }
        public Nullable<decimal> NominalWeight { get; set; }
        public string SalesNominalCode { get; set; }
        public string SalesNominalDeptCode { get; set; }
        public string PurchaseNominalCode { get; set; }
        public string PurchaseNominalDeptCode { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<int> TypicalPieces { get; set; }
        public Nullable<int> VATCodeID { get; set; }
        public string Remarks { get; set; }
        public Nullable<System.DateTime> ActiveFrom { get; set; }
        public Nullable<System.DateTime> ActiveTo { get; set; }
        public Nullable<System.DateTime> InActiveFrom { get; set; }
        public Nullable<System.DateTime> InActiveTo { get; set; }
        public byte[] ItemImage { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public Nullable<int> UserMasterID { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<int> NouStockModeID { get; set; }
        public Nullable<bool> ProductionProduct { get; set; }
        public Nullable<int> ProductLabelFieldID { get; set; }
        public Nullable<decimal> PricePerUnit { get; set; }
        public Nullable<int> QtyPerBox { get; set; }
        public Nullable<System.DateTime> EditDate { get; set; }
        public Nullable<int> DeviceID { get; set; }
        public Nullable<bool> PrintPieceLabels { get; set; }
        public Nullable<int> BPMasterID_LabelPartner { get; set; }
        public Nullable<int> WarehouseID { get; set; }
        public Nullable<int> TareProductContainerID { get; set; }
        public Nullable<bool> WeightRequired { get; set; }
        public Nullable<int> AttributeTemplateID { get; set; }
        public Nullable<int> NouPriceMethodID { get; set; }
        public Nullable<bool> AccumulateTares { get; set; }
        public Nullable<int> Scales { get; set; }
        public Nullable<bool> ReceipeProduct { get; set; }
        public Nullable<int> BPMasterID { get; set; }
        public Nullable<int> SortIndex { get; set; }
        public Nullable<int> NouOrderMethodID { get; set; }
        public Nullable<bool> ReprintLabelAtDispatch { get; set; }
        public Nullable<int> INMasterImageID { get; set; }
        public Nullable<int> CatID { get; set; }
        public Nullable<decimal> MinimumStockLevel { get; set; }
        public Nullable<int> APOrderID_Copy { get; set; }
        public Nullable<int> OrderLeadTime { get; set; }
        public Nullable<int> PROrderID_Copy { get; set; }
        public string Reference { get; set; }
        public Nullable<bool> IsDeduction { get; set; }
        public Nullable<decimal> DeductionRate { get; set; }
        public Nullable<bool> ApplyDeductionToDocket { get; set; }
    
        public virtual ICollection<APGoodsReceiptDetail> APGoodsReceiptDetails { get; set; }
        public virtual ICollection<APQuoteDetail> APQuoteDetails { get; set; }
        public virtual ICollection<ARInvoiceDetail> ARInvoiceDetails { get; set; }
        public virtual ICollection<AROrderDetail> AROrderDetails { get; set; }
        public virtual ICollection<ARQuoteDetail> ARQuoteDetails { get; set; }
        public virtual Container BoxTareContainer { get; set; }
        public virtual Container PiecesTareContainer { get; set; }
        public virtual Container PalletTareContainer { get; set; }
        public virtual DateTemplateName DateTemplateName { get; set; }
        public virtual ICollection<INAttachment> INAttachments { get; set; }
        public virtual INGroup INGroup { get; set; }
        public virtual NouStockMode NouStockMode { get; set; }
        public virtual QualityTemplateName QualityTemplateName { get; set; }
        public virtual TraceabilityTemplateName TraceabilityTemplateName { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public virtual VATCode VATCode { get; set; }
        public virtual ICollection<INPropertySelection> INPropertySelections { get; set; }
        public virtual ICollection<InvoiceDetail> InvoiceDetails { get; set; }
        public virtual ICollection<PROrderInput> PROrderInputs { get; set; }
        public virtual ICollection<PROrderOutput> PROrderOutputs { get; set; }
        public virtual ICollection<StockTakeDetail> StockTakeDetails { get; set; }
        public virtual ICollection<StockTransaction> StockTransactions { get; set; }
        public virtual DeviceMaster DeviceMaster { get; set; }
        public virtual ICollection<SpecialPriceList> SpecialPriceLists { get; set; }
        public virtual ICollection<ARDispatchDetail> ARDispatchDetails { get; set; }
        public virtual ProductLabelField ProductLabelField { get; set; }
        public virtual Container Container3 { get; set; }
        public virtual ICollection<CarcassSplitOption> CarcassSplitOptions { get; set; }
        public virtual ICollection<LairageOrderDetail> LairageOrderDetails { get; set; }
        public virtual AttributeTemplate AttributeTemplate { get; set; }
        public virtual ICollection<DateDay> DateDays { get; set; }
        public virtual INMaster INMaster1 { get; set; }
        public virtual INMaster INMaster2 { get; set; }
        public virtual NouPriceMethod NouPriceMethod { get; set; }
        public virtual ICollection<PRSpecOutput> PRSpecOutputs { get; set; }
        public virtual ICollection<LabelAssociation> LabelAssociations { get; set; }
        public virtual ICollection<PROrderDetail> PROrderDetails { get; set; }
        public virtual ICollection<INMasterStockData> INMasterStockDatas { get; set; }
        public virtual Department Department { get; set; }
        public virtual ICollection<PriceListDetail> PriceListDetails { get; set; }
        public virtual ICollection<APInvoiceDetail> APInvoiceDetails { get; set; }
        public virtual ICollection<ARReturnDetail> ARReturnDetails { get; set; }
        public virtual ICollection<ARReturnInvoiceDetail> ARReturnInvoiceDetails { get; set; }
        public virtual ICollection<APReturnDetail> APReturnDetails { get; set; }
        public virtual ICollection<APReturnInvoiceDetail> APReturnInvoiceDetails { get; set; }
        public virtual ICollection<StockMoveOrderDetail> StockMoveOrderDetails { get; set; }
        public virtual ICollection<APOrderDetail> APOrderDetails { get; set; }
        public virtual ICollection<BarcodeParse> BarcodeParses { get; set; }
    }
}
