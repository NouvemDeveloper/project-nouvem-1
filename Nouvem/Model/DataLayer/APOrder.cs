//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class APOrder
    {
        public APOrder()
        {
            this.APOrderAttachments = new HashSet<APOrderAttachment>();
            this.APGoodsReceipts = new HashSet<APGoodsReceipt>();
            this.APOrderDetails = new HashSet<APOrderDetail>();
        }
    
        public int APOrderID { get; set; }
        public int DocumentNumberingID { get; set; }
        public int Number { get; set; }
        public Nullable<int> BPMasterSnapshotID_Supplier { get; set; }
        public Nullable<int> BPMasterSnapshotID_Haulier { get; set; }
        public Nullable<int> BPAddressSnapshotID_Invoice { get; set; }
        public Nullable<int> BPAddressSnapshotID_Delivery { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public Nullable<System.DateTime> DocumentDate { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<bool> Printed { get; set; }
        public Nullable<int> NouDocStatusID { get; set; }
        public string Remarks { get; set; }
        public Nullable<decimal> TotalExVAT { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> SubTotalExVAT { get; set; }
        public Nullable<decimal> DiscountIncVAT { get; set; }
        public Nullable<decimal> DiscountPercentage { get; set; }
        public Nullable<decimal> GrandTotalIncVAT { get; set; }
        public Nullable<int> UserMasterID { get; set; }
        public Nullable<int> BPAddressID_Delivery { get; set; }
        public Nullable<int> BPAddressID_Invoice { get; set; }
        public Nullable<int> BPMasterID_Haulier { get; set; }
        public Nullable<int> BPMasterID_Supplier { get; set; }
        public Nullable<System.DateTime> QuoteValidDate { get; set; }
        public Nullable<int> BPContactID { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public Nullable<int> BaseDocumentReferenceID { get; set; }
        public Nullable<int> DeviceID { get; set; }
        public Nullable<System.DateTime> EditDate { get; set; }
        public string DeliveryTime { get; set; }
        public string PopUpNote { get; set; }
        public string Notes { get; set; }
        public string IntakeNote { get; set; }
        public string InvoiceNote { get; set; }
        public Nullable<int> UserID_CreatedBy { get; set; }
        public Nullable<int> UserID_CompletedBy { get; set; }
        public Nullable<int> BPMasterID_Agent { get; set; }
    
        public virtual BPContact BPContact { get; set; }
        public virtual DocumentNumbering DocumentNumbering { get; set; }
        public virtual NouDocStatu NouDocStatu { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public virtual ICollection<APOrderAttachment> APOrderAttachments { get; set; }
        public virtual BPAddressSnapshot BPAddressSnapshot { get; set; }
        public virtual BPAddressSnapshot BPAddressSnapshot1 { get; set; }
        public virtual ICollection<APGoodsReceipt> APGoodsReceipts { get; set; }
        public virtual BPAddress BPAddressDelivery { get; set; }
        public virtual BPAddress BPAddressInvoice { get; set; }
        public virtual DateMaster DateMaster { get; set; }
        public virtual DeviceMaster DeviceMaster { get; set; }
        public virtual BPMasterSnapshot BPMasterSnapshot { get; set; }
        public virtual BPMasterSnapshot BPMasterSnapshot1 { get; set; }
        public virtual BPMaster BPMasterSupplier { get; set; }
        public virtual BPMaster BPMasterHaulier { get; set; }
        public virtual ICollection<APOrderDetail> APOrderDetails { get; set; }
    }
}
