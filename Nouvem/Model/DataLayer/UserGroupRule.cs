//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserGroupRule
    {
        public int UserGroupRuleID { get; set; }
        public int UserGroupID { get; set; }
        public int NouAuthorisationListtID { get; set; }
        public int NouAuthorisationValueID { get; set; }
        public bool Deleted { get; set; }
    
        public virtual NouAuthorisation NouAuthorisation { get; set; }
        public virtual NouAuthorisationValue NouAuthorisationValue { get; set; }
        public virtual UserGroup_ UserGroup_ { get; set; }
    }
}
