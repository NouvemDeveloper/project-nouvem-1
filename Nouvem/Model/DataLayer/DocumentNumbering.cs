//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class DocumentNumbering
    {
        public DocumentNumbering()
        {
            this.ARQuotes = new HashSet<ARQuote>();
            this.AROrders = new HashSet<AROrder>();
            this.APQuotes = new HashSet<APQuote>();
            this.APOrders = new HashSet<APOrder>();
            this.ARInvoices = new HashSet<ARInvoice>();
            this.APGoodsReceipts = new HashSet<APGoodsReceipt>();
            this.ARDispatches = new HashSet<ARDispatch>();
            this.LairageOrders = new HashSet<LairageOrder>();
            this.APInvoices = new HashSet<APInvoice>();
            this.ARReturnInvoices = new HashSet<ARReturnInvoice>();
            this.APReturnInvoices = new HashSet<APReturnInvoice>();
            this.PROrders = new HashSet<PROrder>();
        }
    
        public int DocumentNumberingID { get; set; }
        public int NouDocumentNameID { get; set; }
        public int DocumentNumberingTypeID { get; set; }
        public int FirstNumber { get; set; }
        public int LastNumber { get; set; }
        public int NextNumber { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
    
        public virtual DocumentNumberingType DocumentNumberingType { get; set; }
        public virtual NouDocumentName NouDocumentName { get; set; }
        public virtual ICollection<ARQuote> ARQuotes { get; set; }
        public virtual ICollection<AROrder> AROrders { get; set; }
        public virtual ICollection<APQuote> APQuotes { get; set; }
        public virtual ICollection<APOrder> APOrders { get; set; }
        public virtual ICollection<ARInvoice> ARInvoices { get; set; }
        public virtual ICollection<APGoodsReceipt> APGoodsReceipts { get; set; }
        public virtual ICollection<ARDispatch> ARDispatches { get; set; }
        public virtual ICollection<LairageOrder> LairageOrders { get; set; }
        public virtual ICollection<APInvoice> APInvoices { get; set; }
        public virtual ICollection<ARReturnInvoice> ARReturnInvoices { get; set; }
        public virtual ICollection<APReturnInvoice> APReturnInvoices { get; set; }
        public virtual ICollection<PROrder> PROrders { get; set; }
    }
}
