﻿// -----------------------------------------------------------------------
// <copyright file="RouteRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;

    public class RouteRepository : NouvemRepositoryBase, IRouteRepository
    {
        /// <summary>
        /// Returns the application routes.
        /// </summary>
        /// <returns>The application routes.</returns>
        public IList<Route> GetRoutes()
        {
            this.Log.LogDebug(this.GetType(), "GetRoutes(): Attempting to retrieve all the routes");
            var routes = new List<Route>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    routes = (from route in entities.RouteMasters
                               where route.Deleted == null
                               select new Route
                               {
                                   RouteID = route.RouteID,
                                   Name = route.Name,
                                   RunNumber = route.RunNumber,
                                   Remark = route.Remark,
                                   RegionID = route.RegionID,
                                   Description = route.Description,
                                   Deleted = route.Deleted
                               }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} routes successfully retrieved", routes.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return routes;
        }

        /// <summary>
        /// Add or updates the routes.
        /// </summary>
        /// <param name="routes">The routes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateRoutes(IList<Route> routes)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateRoutes(): Attempting to update the routes");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    routes.ToList().ForEach(x =>
                    {
                        if (x.RouteID == 0)
                        {
                            // new
                            var newRoute = new RouteMaster
                            {
                                Name = x.Name,
                                RunNumber = x.RunNumber,
                                Description = x.Description,
                                RegionID = x.RegionID,
                                Remark = x.Remark
                            };

                            entities.RouteMasters.Add(newRoute);
                        }
                        else
                        {
                            // update
                            var dbRoute =
                                entities.RouteMasters.FirstOrDefault(
                                    route => route.RouteID == x.RouteID && route.Deleted == null);

                            if (dbRoute != null)
                            {
                                dbRoute.Name = x.Name;
                                dbRoute.RunNumber = x.RunNumber;
                                dbRoute.Remark = x.Remark;
                                dbRoute.RegionID = x.RegionID;
                                dbRoute.Description = x.Description;
                                dbRoute.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Routes successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }
    }
}
