﻿// -----------------------------------------------------------------------
// <copyright file="RepositoryBase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Nouvem.Model.BusinessObject;

namespace Nouvem.Model.DataLayer.Repository
{
    using Nouvem.Logging;

    /// <summary>
    /// Base class for the repository classes.
    /// </summary>
    public class NouvemRepositoryBase
    {
        /// <summary>
        /// The application logger reference.
        /// </summary>
        protected Logger Log = new Logger();

        /// <summary>
        /// Creates a transaction from a stock detail.
        /// </summary>
        /// <param name="transaction">The stock detail to copy</param>
        /// <returns>A copied transaction.</returns>
        protected StockTransaction CreateTransaction(StockDetail transaction)
        {
            return new StockTransaction
            {
                MasterTableID = transaction.MasterTableID,
                BatchNumberID = transaction.BatchNumberID,
                Serial = transaction.Serial,
                TransactionDate = transaction.TransactionDate,
                NouTransactionTypeID = transaction.NouTransactionTypeID,
                TransactionQTY = transaction.TransactionQty,
                TransactionWeight = transaction.TransactionWeight,
                GrossWeight = transaction.GrossWeight,
                Tare = transaction.Tare,
                Pieces = transaction.Pieces,
                Alibi = transaction.Alibi,
                WarehouseID = transaction.WarehouseID,
                INMasterID = transaction.INMasterID,
                ContainerID = transaction.ContainerID,
                Consumed = transaction.Consumed,
                ManualWeight = transaction.ManualWeight,
                DeviceMasterID = transaction.DeviceMasterID,
                UserMasterID = transaction.UserMasterID,
                IsBox = transaction.IsBox,
                StoredLabelID = transaction.StoredLabelId,
                InLocation = transaction.InLocation,
                StockTransactionID_Container = transaction.StockTransactionID_Container,
                Reference = transaction.Reference,
                BatchID_Base = transaction.BatchNumberBaseID,
                Deleted = transaction.Deleted,
                BPMasterID = transaction.BPMasterID,
                SplitID = transaction.SplitID,
                ProcessID = transaction.ProcessID,
                LabelID = transaction.LabelID.HasValue ? transaction.LabelID.ToString() : null
            };
        }

        /// <summary>
        /// Creates an attribute from a stock detail.
        /// </summary>
        /// <param name="attribute">The stock detail to copy</param>
        /// <returns>A copied attribute.</returns>
        protected Attribute CreateAttribute(StockDetail attribute)
        {
            #region attributes

            return new Attribute
            {
                Attribute1 = attribute.Attribute1,
                Attribute2 = attribute.Attribute2,
                Attribute3 = attribute.Attribute3,
                Attribute4 = attribute.Attribute4,
                Attribute5 = attribute.Attribute5,
                Attribute6 = attribute.Attribute6,
                Attribute7 = attribute.Attribute7,
                Attribute8 = attribute.Attribute8,
                Attribute9 = attribute.Attribute9,
                Attribute10 = attribute.Attribute10,
                Attribute11 = attribute.Attribute11,
                Attribute12 = attribute.Attribute12,
                Attribute13 = attribute.Attribute13,
                Attribute14 = attribute.Attribute14,
                Attribute15 = attribute.Attribute15,
                Attribute16 = attribute.Attribute16,
                Attribute17 = attribute.Attribute17,
                Attribute18 = attribute.Attribute18,
                Attribute19 = attribute.Attribute19,
                Attribute20 = attribute.Attribute20,
                Attribute21 = attribute.Attribute21,
                Attribute22 = attribute.Attribute22,
                Attribute23 = attribute.Attribute23,
                Attribute24 = attribute.Attribute24,
                Attribute25 = attribute.Attribute25,
                Attribute26 = attribute.Attribute26,
                Attribute27 = attribute.Attribute27,
                Attribute28 = attribute.Attribute28,
                Attribute29 = attribute.Attribute29,
                Attribute30 = attribute.Attribute30,
                Attribute31 = attribute.Attribute31,
                Attribute32 = attribute.Attribute32,
                Attribute33 = attribute.Attribute33,
                Attribute34 = attribute.Attribute34,
                Attribute35 = attribute.Attribute35,
                Attribute36 = attribute.Attribute36,
                Attribute37 = attribute.Attribute37,
                Attribute38 = attribute.Attribute38,
                Attribute39 = attribute.Attribute39,
                Attribute40 = attribute.Attribute40,
                Attribute41 = attribute.Attribute41,
                Attribute42 = attribute.Attribute42,
                Attribute43 = attribute.Attribute43,
                Attribute44 = attribute.Attribute44,
                Attribute45 = attribute.Attribute45,
                Attribute46 = attribute.Attribute46,
                Attribute47 = attribute.Attribute47,
                Attribute48 = attribute.Attribute48,
                Attribute49 = attribute.Attribute49,
                Attribute50 = attribute.Attribute50,
                Attribute51 = attribute.Attribute51,
                Attribute52 = attribute.Attribute52,
                Attribute53 = attribute.Attribute53,
                Attribute54 = attribute.Attribute54,
                Attribute55 = attribute.Attribute55,
                Attribute56 = attribute.Attribute56,
                Attribute57 = attribute.Attribute57,
                Attribute58 = attribute.Attribute58,
                Attribute59 = attribute.Attribute59,
                Attribute60 = attribute.Attribute60,
                Attribute61 = attribute.Attribute61,
                Attribute62 = attribute.Attribute62,
                Attribute63 = attribute.Attribute63,
                Attribute64 = attribute.Attribute64,
                Attribute65 = attribute.Attribute65,
                Attribute66 = attribute.Attribute66,
                Attribute67 = attribute.Attribute67,
                Attribute68 = attribute.Attribute68,
                Attribute69 = attribute.Attribute69,
                Attribute70 = attribute.Attribute70,
                Attribute71 = attribute.Attribute71,
                Attribute72 = attribute.Attribute72,
                Attribute73 = attribute.Attribute73,
                Attribute74 = attribute.Attribute74,
                Attribute75 = attribute.Attribute75,
                Attribute76 = attribute.Attribute76,
                Attribute77 = attribute.Attribute77,
                Attribute78 = attribute.Attribute78,
                Attribute79 = attribute.Attribute79,
                Attribute80 = attribute.Attribute80,
                Attribute81 = attribute.Attribute81,
                Attribute82 = attribute.Attribute82,
                Attribute83 = attribute.Attribute83,
                Attribute84 = attribute.Attribute84,
                Attribute85 = attribute.Attribute85,
                Attribute86 = attribute.Attribute86,
                Attribute87 = attribute.Attribute87,
                Attribute88 = attribute.Attribute88,
                Attribute89 = attribute.Attribute89,
                Attribute90 = attribute.Attribute90,
                Attribute91 = attribute.Attribute91,
                Attribute92 = attribute.Attribute92,
                Attribute93 = attribute.Attribute93,
                Attribute94 = attribute.Attribute94,
                Attribute95 = attribute.Attribute95,
                Attribute96 = attribute.Attribute96,
                Attribute97 = attribute.Attribute97,
                Attribute98 = attribute.Attribute98,
                Attribute99 = attribute.Attribute99,
                Attribute100 = attribute.Attribute100,

                Attribute101 = attribute.Attribute101,
                Attribute102 = attribute.Attribute102,
                Attribute103 = attribute.Attribute103,
                Attribute104 = attribute.Attribute104,
                Attribute105 = attribute.Attribute105,
                Attribute106 = attribute.Attribute106,
                Attribute107 = attribute.Attribute107,
                Attribute108 = attribute.Attribute108,
                Attribute109 = attribute.Attribute109,
                Attribute110 = attribute.Attribute110,
                Attribute111 = attribute.Attribute111,
                Attribute112 = attribute.Attribute112,
                Attribute113 = attribute.Attribute113,
                Attribute114 = attribute.Attribute114,
                Attribute115 = attribute.Attribute115,
                Attribute116 = attribute.Attribute116,
                Attribute117 = attribute.Attribute117,
                Attribute118 = attribute.Attribute118,
                Attribute119 = attribute.Attribute119,
                Attribute120 = attribute.Attribute120,
                Attribute121 = attribute.Attribute121,
                Attribute122 = attribute.Attribute122,
                Attribute123 = attribute.Attribute123,
                Attribute124 = attribute.Attribute124,
                Attribute125 = attribute.Attribute125,
                Attribute126 = attribute.Attribute126,
                Attribute127 = attribute.Attribute127,
                Attribute128 = attribute.Attribute128,
                Attribute129 = attribute.Attribute129,
                Attribute130 = attribute.Attribute130,
                Attribute131 = attribute.Attribute131,
                Attribute132 = attribute.Attribute132,
                Attribute133 = attribute.Attribute133,
                Attribute134 = attribute.Attribute134,
                Attribute135 = attribute.Attribute135,
                Attribute136 = attribute.Attribute136,
                Attribute137 = attribute.Attribute137,
                Attribute138 = attribute.Attribute138,
                Attribute139 = attribute.Attribute139,
                Attribute140 = attribute.Attribute140,
                Attribute141 = attribute.Attribute141,
                Attribute142 = attribute.Attribute142,
                Attribute143 = attribute.Attribute143,
                Attribute144 = attribute.Attribute144,
                Attribute145 = attribute.Attribute145,
                Attribute146 = attribute.Attribute146,
                Attribute147 = attribute.Attribute147,
                Attribute148 = attribute.Attribute148,
                Attribute149 = attribute.Attribute149,
                Attribute150 = attribute.Attribute150,

                Attribute151 = attribute.Attribute151,
                Attribute152 = attribute.Attribute152,
                Attribute153 = attribute.Attribute153,
                Attribute154 = attribute.Attribute154,
                Attribute155 = attribute.Attribute155,
                Attribute156 = attribute.Attribute156,
                Attribute157 = attribute.Attribute157,
                Attribute158 = attribute.Attribute158,
                Attribute159 = attribute.Attribute159,
                Attribute160 = attribute.Attribute160,
                Attribute161 = attribute.Attribute161,
                Attribute162 = attribute.Attribute162,
                Attribute163 = attribute.Attribute163,
                Attribute164 = attribute.Attribute164,
                Attribute165 = attribute.Attribute165,
                Attribute166 = attribute.Attribute166,
                Attribute167 = attribute.Attribute167,
                Attribute168 = attribute.Attribute168,
                Attribute169 = attribute.Attribute169,
                Attribute170 = attribute.Attribute170,
                Attribute171 = attribute.Attribute171,
                Attribute172 = attribute.Attribute172,
                Attribute173 = attribute.Attribute173,
                Attribute174 = attribute.Attribute174,
                Attribute175 = attribute.Attribute175,
                Attribute176 = attribute.Attribute176,
                Attribute177 = attribute.Attribute177,
                Attribute178 = attribute.Attribute178,
                Attribute179 = attribute.Attribute179,
                Attribute180 = attribute.Attribute180,
                Attribute181 = attribute.Attribute181,
                Attribute182 = attribute.Attribute182,
                Attribute183 = attribute.Attribute183,
                Attribute184 = attribute.Attribute184,
                Attribute185 = attribute.Attribute185,
                Attribute186 = attribute.Attribute186,
                Attribute187 = attribute.Attribute187,
                Attribute188 = attribute.Attribute188,
                Attribute189 = attribute.Attribute189,
                Attribute190 = attribute.Attribute190,
                Attribute191 = attribute.Attribute191,
                Attribute192 = attribute.Attribute192,
                Attribute193 = attribute.Attribute193,
                Attribute194 = attribute.Attribute194,
                Attribute195 = attribute.Attribute195,
                Attribute196 = attribute.Attribute196,
                Attribute197 = attribute.Attribute197,
                Attribute198 = attribute.Attribute198,
                Attribute199 = attribute.Attribute199,
                Attribute200 = attribute.Attribute200,
                Attribute201 = attribute.Attribute201,
                Attribute202 = attribute.Attribute202,
                Attribute203 = attribute.Attribute203,
                Attribute204 = attribute.Attribute204,
                Attribute205 = attribute.Attribute205,
                Attribute206 = attribute.Attribute206,
                Attribute207 = attribute.Attribute207,
                Attribute208 = attribute.Attribute208,
                Attribute209 = attribute.Attribute209,
                Attribute210 = attribute.Attribute210,
                Attribute211 = attribute.Attribute211,
                Attribute212 = attribute.Attribute212,
                Attribute213 = attribute.Attribute213,
                Attribute214 = attribute.Attribute214,
                Attribute215 = attribute.Attribute215,
                Attribute216 = attribute.Attribute216,
                Attribute217 = attribute.Attribute217,
                Attribute218 = attribute.Attribute218,
                Attribute219 = attribute.Attribute219,
                Attribute220 = attribute.Attribute220,
                Attribute221 = attribute.Attribute221,
                Attribute222 = attribute.Attribute222,
                Attribute223 = attribute.Attribute223,
                Attribute224 = attribute.Attribute224,
                Attribute225 = attribute.Attribute225,
                Attribute226 = attribute.Attribute226,
                Attribute227 = attribute.Attribute227,
                Attribute228 = attribute.Attribute228,
                Attribute229 = attribute.Attribute229,
                Attribute230 = attribute.Attribute230,
                Attribute231 = attribute.Attribute231,
                Attribute232 = attribute.Attribute232,
                Attribute233 = attribute.Attribute233,
                Attribute234 = attribute.Attribute234,
                Attribute235 = attribute.Attribute235,
                Attribute236 = attribute.Attribute236,
                Attribute237 = attribute.Attribute237,
                Attribute238 = attribute.Attribute238,
                Attribute239 = attribute.Attribute239,
                Attribute240 = attribute.Attribute240,
                Attribute241 = attribute.Attribute241,
                Attribute242 = attribute.Attribute242,
                Attribute243 = attribute.Attribute243,
                Attribute244 = attribute.Attribute244,
                Attribute245 = attribute.Attribute245,
                Attribute246 = attribute.Attribute246,
                Attribute247 = attribute.Attribute247,
                Attribute248 = attribute.Attribute248,
                Attribute249 = attribute.Attribute249,
                Attribute250 = attribute.Attribute250,
                CarcassNumber = attribute.CarcassNumber,
                Eartag = attribute.Eartag,
                GradingDate = attribute.GradingDate,
                SequencedDate = attribute.SequencedDate,
                SupplierID = attribute.SupplierID,
                Category = attribute.CategoryID,
                Herd = attribute.HerdNo
            };

            #endregion
        }

        /// <summary>
        /// Creates an attribute from a stock detail.
        /// </summary>
        /// <param name="attribute">The stock detail to copy</param>
        /// <returns>A copied attribute.</returns>
        protected Attribute CreateAttribute(Attribute attribute)
        {
            #region attributes

            return new Attribute
            {
                Attribute1 = attribute.Attribute1,
                Attribute2 = attribute.Attribute2,
                Attribute3 = attribute.Attribute3,
                Attribute4 = attribute.Attribute4,
                Attribute5 = attribute.Attribute5,
                Attribute6 = attribute.Attribute6,
                Attribute7 = attribute.Attribute7,
                Attribute8 = attribute.Attribute8,
                Attribute9 = attribute.Attribute9,
                Attribute10 = attribute.Attribute10,
                Attribute11 = attribute.Attribute11,
                Attribute12 = attribute.Attribute12,
                Attribute13 = attribute.Attribute13,
                Attribute14 = attribute.Attribute14,
                Attribute15 = attribute.Attribute15,
                Attribute16 = attribute.Attribute16,
                Attribute17 = attribute.Attribute17,
                Attribute18 = attribute.Attribute18,
                Attribute19 = attribute.Attribute19,
                Attribute20 = attribute.Attribute20,
                Attribute21 = attribute.Attribute21,
                Attribute22 = attribute.Attribute22,
                Attribute23 = attribute.Attribute23,
                Attribute24 = attribute.Attribute24,
                Attribute25 = attribute.Attribute25,
                Attribute26 = attribute.Attribute26,
                Attribute27 = attribute.Attribute27,
                Attribute28 = attribute.Attribute28,
                Attribute29 = attribute.Attribute29,
                Attribute30 = attribute.Attribute30,
                Attribute31 = attribute.Attribute31,
                Attribute32 = attribute.Attribute32,
                Attribute33 = attribute.Attribute33,
                Attribute34 = attribute.Attribute34,
                Attribute35 = attribute.Attribute35,
                Attribute36 = attribute.Attribute36,
                Attribute37 = attribute.Attribute37,
                Attribute38 = attribute.Attribute38,
                Attribute39 = attribute.Attribute39,
                Attribute40 = attribute.Attribute40,
                Attribute41 = attribute.Attribute41,
                Attribute42 = attribute.Attribute42,
                Attribute43 = attribute.Attribute43,
                Attribute44 = attribute.Attribute44,
                Attribute45 = attribute.Attribute45,
                Attribute46 = attribute.Attribute46,
                Attribute47 = attribute.Attribute47,
                Attribute48 = attribute.Attribute48,
                Attribute49 = attribute.Attribute49,
                Attribute50 = attribute.Attribute50,
                Attribute51 = attribute.Attribute51,
                Attribute52 = attribute.Attribute52,
                Attribute53 = attribute.Attribute53,
                Attribute54 = attribute.Attribute54,
                Attribute55 = attribute.Attribute55,
                Attribute56 = attribute.Attribute56,
                Attribute57 = attribute.Attribute57,
                Attribute58 = attribute.Attribute58,
                Attribute59 = attribute.Attribute59,
                Attribute60 = attribute.Attribute60,
                Attribute61 = attribute.Attribute61,
                Attribute62 = attribute.Attribute62,
                Attribute63 = attribute.Attribute63,
                Attribute64 = attribute.Attribute64,
                Attribute65 = attribute.Attribute65,
                Attribute66 = attribute.Attribute66,
                Attribute67 = attribute.Attribute67,
                Attribute68 = attribute.Attribute68,
                Attribute69 = attribute.Attribute69,
                Attribute70 = attribute.Attribute70,
                Attribute71 = attribute.Attribute71,
                Attribute72 = attribute.Attribute72,
                Attribute73 = attribute.Attribute73,
                Attribute74 = attribute.Attribute74,
                Attribute75 = attribute.Attribute75,
                Attribute76 = attribute.Attribute76,
                Attribute77 = attribute.Attribute77,
                Attribute78 = attribute.Attribute78,
                Attribute79 = attribute.Attribute79,
                Attribute80 = attribute.Attribute80,
                Attribute81 = attribute.Attribute81,
                Attribute82 = attribute.Attribute82,
                Attribute83 = attribute.Attribute83,
                Attribute84 = attribute.Attribute84,
                Attribute85 = attribute.Attribute85,
                Attribute86 = attribute.Attribute86,
                Attribute87 = attribute.Attribute87,
                Attribute88 = attribute.Attribute88,
                Attribute89 = attribute.Attribute89,
                Attribute90 = attribute.Attribute90,
                Attribute91 = attribute.Attribute91,
                Attribute92 = attribute.Attribute92,
                Attribute93 = attribute.Attribute93,
                Attribute94 = attribute.Attribute94,
                Attribute95 = attribute.Attribute95,
                Attribute96 = attribute.Attribute96,
                Attribute97 = attribute.Attribute97,
                Attribute98 = attribute.Attribute98,
                Attribute99 = attribute.Attribute99,
                Attribute100 = attribute.Attribute100,
                Attribute101 = attribute.Attribute101,
                Attribute102 = attribute.Attribute102,
                Attribute103 = attribute.Attribute103,
                Attribute104 = attribute.Attribute104,
                Attribute105 = attribute.Attribute105,
                Attribute106 = attribute.Attribute106,
                Attribute107 = attribute.Attribute107,
                Attribute108 = attribute.Attribute108,
                Attribute109 = attribute.Attribute109,
                Attribute110 = attribute.Attribute110,
                Attribute111 = attribute.Attribute111,
                Attribute112 = attribute.Attribute112,
                Attribute113 = attribute.Attribute113,
                Attribute114 = attribute.Attribute114,
                Attribute115 = attribute.Attribute115,
                Attribute116 = attribute.Attribute116,
                Attribute117 = attribute.Attribute117,
                Attribute118 = attribute.Attribute118,
                Attribute119 = attribute.Attribute119,
                Attribute120 = attribute.Attribute120,
                Attribute121 = attribute.Attribute121,
                Attribute122 = attribute.Attribute122,
                Attribute123 = attribute.Attribute123,
                Attribute124 = attribute.Attribute124,
                Attribute125 = attribute.Attribute125,
                Attribute126 = attribute.Attribute126,
                Attribute127 = attribute.Attribute127,
                Attribute128 = attribute.Attribute128,
                Attribute129 = attribute.Attribute129,
                Attribute130 = attribute.Attribute130,
                Attribute131 = attribute.Attribute131,
                Attribute132 = attribute.Attribute132,
                Attribute133 = attribute.Attribute133,
                Attribute134 = attribute.Attribute134,
                Attribute135 = attribute.Attribute135,
                Attribute136 = attribute.Attribute136,
                Attribute137 = attribute.Attribute137,
                Attribute138 = attribute.Attribute138,
                Attribute139 = attribute.Attribute139,
                Attribute140 = attribute.Attribute140,
                Attribute141 = attribute.Attribute141,
                Attribute142 = attribute.Attribute142,
                Attribute143 = attribute.Attribute143,
                Attribute144 = attribute.Attribute144,
                Attribute145 = attribute.Attribute145,
                Attribute146 = attribute.Attribute146,
                Attribute147 = attribute.Attribute147,
                Attribute148 = attribute.Attribute148,
                Attribute149 = attribute.Attribute149,
                Attribute150 = attribute.Attribute150,

                Attribute151 = attribute.Attribute151,
                Attribute152 = attribute.Attribute152,
                Attribute153 = attribute.Attribute153,
                Attribute154 = attribute.Attribute154,
                Attribute155 = attribute.Attribute155,
                Attribute156 = attribute.Attribute156,
                Attribute157 = attribute.Attribute157,
                Attribute158 = attribute.Attribute158,
                Attribute159 = attribute.Attribute159,
                Attribute160 = attribute.Attribute160,
                Attribute161 = attribute.Attribute161,
                Attribute162 = attribute.Attribute162,
                Attribute163 = attribute.Attribute163,
                Attribute164 = attribute.Attribute164,
                Attribute165 = attribute.Attribute165,
                Attribute166 = attribute.Attribute166,
                Attribute167 = attribute.Attribute167,
                Attribute168 = attribute.Attribute168,
                Attribute169 = attribute.Attribute169,
                Attribute170 = attribute.Attribute170,
                Attribute171 = attribute.Attribute171,
                Attribute172 = attribute.Attribute172,
                Attribute173 = attribute.Attribute173,
                Attribute174 = attribute.Attribute174,
                Attribute175 = attribute.Attribute175,
                Attribute176 = attribute.Attribute176,
                Attribute177 = attribute.Attribute177,
                Attribute178 = attribute.Attribute178,
                Attribute179 = attribute.Attribute179,
                Attribute180 = attribute.Attribute180,
                Attribute181 = attribute.Attribute181,
                Attribute182 = attribute.Attribute182,
                Attribute183 = attribute.Attribute183,
                Attribute184 = attribute.Attribute184,
                Attribute185 = attribute.Attribute185,
                Attribute186 = attribute.Attribute186,
                Attribute187 = attribute.Attribute187,
                Attribute188 = attribute.Attribute188,
                Attribute189 = attribute.Attribute189,
                Attribute190 = attribute.Attribute190,
                Attribute191 = attribute.Attribute191,
                Attribute192 = attribute.Attribute192,
                Attribute193 = attribute.Attribute193,
                Attribute194 = attribute.Attribute194,
                Attribute195 = attribute.Attribute195,
                Attribute196 = attribute.Attribute196,
                Attribute197 = attribute.Attribute197,
                Attribute198 = attribute.Attribute198,
                Attribute199 = attribute.Attribute199,
                Attribute200 = attribute.Attribute200
            };

            #endregion
        }

        /// <summary>
        /// Creates an attribute from a stock detail.
        /// </summary>
        /// <param name="attribute">The stock detail to copy</param>
        /// <returns>A copied attribute.</returns>
        protected StockDetail CreateStockAttribute(Attribute attribute)
        {
            #region attributes

            return new StockDetail
            {
                Attribute1 = attribute.Attribute1,
                Attribute2 = attribute.Attribute2,
                Attribute3 = attribute.Attribute3,
                Attribute4 = attribute.Attribute4,
                Attribute5 = attribute.Attribute5,
                Attribute6 = attribute.Attribute6,
                Attribute7 = attribute.Attribute7,
                Attribute8 = attribute.Attribute8,
                Attribute9 = attribute.Attribute9,
                Attribute10 = attribute.Attribute10,
                Attribute11 = attribute.Attribute11,
                Attribute12 = attribute.Attribute12,
                Attribute13 = attribute.Attribute13,
                Attribute14 = attribute.Attribute14,
                Attribute15 = attribute.Attribute15,
                Attribute16 = attribute.Attribute16,
                Attribute17 = attribute.Attribute17,
                Attribute18 = attribute.Attribute18,
                Attribute19 = attribute.Attribute19,
                Attribute20 = attribute.Attribute20,
                Attribute21 = attribute.Attribute21,
                Attribute22 = attribute.Attribute22,
                Attribute23 = attribute.Attribute23,
                Attribute24 = attribute.Attribute24,
                Attribute25 = attribute.Attribute25,
                Attribute26 = attribute.Attribute26,
                Attribute27 = attribute.Attribute27,
                Attribute28 = attribute.Attribute28,
                Attribute29 = attribute.Attribute29,
                Attribute30 = attribute.Attribute30,
                Attribute31 = attribute.Attribute31,
                Attribute32 = attribute.Attribute32,
                Attribute33 = attribute.Attribute33,
                Attribute34 = attribute.Attribute34,
                Attribute35 = attribute.Attribute35,
                Attribute36 = attribute.Attribute36,
                Attribute37 = attribute.Attribute37,
                Attribute38 = attribute.Attribute38,
                Attribute39 = attribute.Attribute39,
                Attribute40 = attribute.Attribute40,
                Attribute41 = attribute.Attribute41,
                Attribute42 = attribute.Attribute42,
                Attribute43 = attribute.Attribute43,
                Attribute44 = attribute.Attribute44,
                Attribute45 = attribute.Attribute45,
                Attribute46 = attribute.Attribute46,
                Attribute47 = attribute.Attribute47,
                Attribute48 = attribute.Attribute48,
                Attribute49 = attribute.Attribute49,
                Attribute50 = attribute.Attribute50,
                Attribute51 = attribute.Attribute51,
                Attribute52 = attribute.Attribute52,
                Attribute53 = attribute.Attribute53,
                Attribute54 = attribute.Attribute54,
                Attribute55 = attribute.Attribute55,
                Attribute56 = attribute.Attribute56,
                Attribute57 = attribute.Attribute57,
                Attribute58 = attribute.Attribute58,
                Attribute59 = attribute.Attribute59,
                Attribute60 = attribute.Attribute60,
                Attribute61 = attribute.Attribute61,
                Attribute62 = attribute.Attribute62,
                Attribute63 = attribute.Attribute63,
                Attribute64 = attribute.Attribute64,
                Attribute65 = attribute.Attribute65,
                Attribute66 = attribute.Attribute66,
                Attribute67 = attribute.Attribute67,
                Attribute68 = attribute.Attribute68,
                Attribute69 = attribute.Attribute69,
                Attribute70 = attribute.Attribute70,
                Attribute71 = attribute.Attribute71,
                Attribute72 = attribute.Attribute72,
                Attribute73 = attribute.Attribute73,
                Attribute74 = attribute.Attribute74,
                Attribute75 = attribute.Attribute75,
                Attribute76 = attribute.Attribute76,
                Attribute77 = attribute.Attribute77,
                Attribute78 = attribute.Attribute78,
                Attribute79 = attribute.Attribute79,
                Attribute80 = attribute.Attribute80,
                Attribute81 = attribute.Attribute81,
                Attribute82 = attribute.Attribute82,
                Attribute83 = attribute.Attribute83,
                Attribute84 = attribute.Attribute84,
                Attribute85 = attribute.Attribute85,
                Attribute86 = attribute.Attribute86,
                Attribute87 = attribute.Attribute87,
                Attribute88 = attribute.Attribute88,
                Attribute89 = attribute.Attribute89,
                Attribute90 = attribute.Attribute90,
                Attribute91 = attribute.Attribute91,
                Attribute92 = attribute.Attribute92,
                Attribute93 = attribute.Attribute93,
                Attribute94 = attribute.Attribute94,
                Attribute95 = attribute.Attribute95,
                Attribute96 = attribute.Attribute96,
                Attribute97 = attribute.Attribute97,
                Attribute98 = attribute.Attribute98,
                Attribute99 = attribute.Attribute99,
                Attribute100 = attribute.Attribute100,
                Attribute101 = attribute.Attribute101,
                Attribute102 = attribute.Attribute102,
                Attribute103 = attribute.Attribute103,
                Attribute104 = attribute.Attribute104,
                Attribute105 = attribute.Attribute105,
                Attribute106 = attribute.Attribute106,
                Attribute107 = attribute.Attribute107,
                Attribute108 = attribute.Attribute108,
                Attribute109 = attribute.Attribute109,
                Attribute110 = attribute.Attribute110,
                Attribute111 = attribute.Attribute111,
                Attribute112 = attribute.Attribute112,
                Attribute113 = attribute.Attribute113,
                Attribute114 = attribute.Attribute114,
                Attribute115 = attribute.Attribute115,
                Attribute116 = attribute.Attribute116,
                Attribute117 = attribute.Attribute117,
                Attribute118 = attribute.Attribute118,
                Attribute119 = attribute.Attribute119,
                Attribute120 = attribute.Attribute120,
                Attribute121 = attribute.Attribute121,
                Attribute122 = attribute.Attribute122,
                Attribute123 = attribute.Attribute123,
                Attribute124 = attribute.Attribute124,
                Attribute125 = attribute.Attribute125,
                Attribute126 = attribute.Attribute126,
                Attribute127 = attribute.Attribute127,
                Attribute128 = attribute.Attribute128,
                Attribute129 = attribute.Attribute129,
                Attribute130 = attribute.Attribute130,
                Attribute131 = attribute.Attribute131,
                Attribute132 = attribute.Attribute132,
                Attribute133 = attribute.Attribute133,
                Attribute134 = attribute.Attribute134,
                Attribute135 = attribute.Attribute135,
                Attribute136 = attribute.Attribute136,
                Attribute137 = attribute.Attribute137,
                Attribute138 = attribute.Attribute138,
                Attribute139 = attribute.Attribute139,
                Attribute140 = attribute.Attribute140,
                Attribute141 = attribute.Attribute141,
                Attribute142 = attribute.Attribute142,
                Attribute143 = attribute.Attribute143,
                Attribute144 = attribute.Attribute144,
                Attribute145 = attribute.Attribute145,
                Attribute146 = attribute.Attribute146,
                Attribute147 = attribute.Attribute147,
                Attribute148 = attribute.Attribute148,
                Attribute149 = attribute.Attribute149,
                Attribute150 = attribute.Attribute150,

                Attribute151 = attribute.Attribute151,
                Attribute152 = attribute.Attribute152,
                Attribute153 = attribute.Attribute153,
                Attribute154 = attribute.Attribute154,
                Attribute155 = attribute.Attribute155,
                Attribute156 = attribute.Attribute156,
                Attribute157 = attribute.Attribute157,
                Attribute158 = attribute.Attribute158,
                Attribute159 = attribute.Attribute159,
                Attribute160 = attribute.Attribute160,
                Attribute161 = attribute.Attribute161,
                Attribute162 = attribute.Attribute162,
                Attribute163 = attribute.Attribute163,
                Attribute164 = attribute.Attribute164,
                Attribute165 = attribute.Attribute165,
                Attribute166 = attribute.Attribute166,
                Attribute167 = attribute.Attribute167,
                Attribute168 = attribute.Attribute168,
                Attribute169 = attribute.Attribute169,
                Attribute170 = attribute.Attribute170,
                Attribute171 = attribute.Attribute171,
                Attribute172 = attribute.Attribute172,
                Attribute173 = attribute.Attribute173,
                Attribute174 = attribute.Attribute174,
                Attribute175 = attribute.Attribute175,
                Attribute176 = attribute.Attribute176,
                Attribute177 = attribute.Attribute177,
                Attribute178 = attribute.Attribute178,
                Attribute179 = attribute.Attribute179,
                Attribute180 = attribute.Attribute180,
                Attribute181 = attribute.Attribute181,
                Attribute182 = attribute.Attribute182,
                Attribute183 = attribute.Attribute183,
                Attribute184 = attribute.Attribute184,
                Attribute185 = attribute.Attribute185,
                Attribute186 = attribute.Attribute186,
                Attribute187 = attribute.Attribute187,
                Attribute188 = attribute.Attribute188,
                Attribute189 = attribute.Attribute189,
                Attribute190 = attribute.Attribute190,
                Attribute191 = attribute.Attribute191,
                Attribute192 = attribute.Attribute192,
                Attribute193 = attribute.Attribute193,
                Attribute194 = attribute.Attribute194,
                Attribute195 = attribute.Attribute195,
                Attribute196 = attribute.Attribute196,
                Attribute197 = attribute.Attribute197,
                Attribute198 = attribute.Attribute198,
                Attribute199 = attribute.Attribute199,
                Attribute200 = attribute.Attribute200,
                Attribute201 = attribute.Attribute201,
                Attribute202 = attribute.Attribute202,
                Attribute203 = attribute.Attribute203,
                Attribute204 = attribute.Attribute204,
                Attribute205 = attribute.Attribute205,
                Attribute206 = attribute.Attribute206,
                Attribute207 = attribute.Attribute207,
                Attribute208 = attribute.Attribute208,
                Attribute209 = attribute.Attribute209,
                Attribute210 = attribute.Attribute210,
                Attribute211 = attribute.Attribute211,
                Attribute212 = attribute.Attribute212,
                Attribute213 = attribute.Attribute213,
                Attribute214 = attribute.Attribute214,
                Attribute215 = attribute.Attribute215,
                Attribute216 = attribute.Attribute216,
                Attribute217 = attribute.Attribute217,
                Attribute218 = attribute.Attribute218,
                Attribute219 = attribute.Attribute219,
                Attribute220 = attribute.Attribute220,
                Attribute221 = attribute.Attribute221,
                Attribute222 = attribute.Attribute222,
                Attribute223 = attribute.Attribute223,
                Attribute224 = attribute.Attribute224,
                Attribute225 = attribute.Attribute225,
                Attribute226 = attribute.Attribute226,
                Attribute227 = attribute.Attribute227,
                Attribute228 = attribute.Attribute228,
                Attribute229 = attribute.Attribute229,
                Attribute230 = attribute.Attribute230,
                Attribute231 = attribute.Attribute231,
                Attribute232 = attribute.Attribute232,
                Attribute233 = attribute.Attribute233,
                Attribute234 = attribute.Attribute234,
                Attribute235 = attribute.Attribute235,
                Attribute236 = attribute.Attribute236,
                Attribute237 = attribute.Attribute237,
                Attribute238 = attribute.Attribute238,
                Attribute239 = attribute.Attribute239,
                Attribute240 = attribute.Attribute240,
                Attribute241 = attribute.Attribute241,
                Attribute242 = attribute.Attribute242,
                Attribute243 = attribute.Attribute243,
                Attribute244 = attribute.Attribute244,
                Attribute245 = attribute.Attribute245,
                Attribute246 = attribute.Attribute246,
                Attribute247 = attribute.Attribute247,
                Attribute248 = attribute.Attribute248,
                Attribute249 = attribute.Attribute249,
                Attribute250 = attribute.Attribute250,

            };

            #endregion
        }

        /// <summary>
        /// Creates a deep copy transaction.
        /// </summary>
        /// <param name="transaction">The transaction to deep copy</param>
        /// <returns>A deep copy transaction.</returns>
        protected StockTransaction CreateTransaction(StockTransaction transaction)
        {
            return new StockTransaction
            {
                MasterTableID = transaction.MasterTableID,
                BatchNumberID = transaction.BatchNumberID,
                Serial = transaction.Serial,
                TransactionDate = DateTime.Now,
                NouTransactionTypeID = transaction.NouTransactionTypeID,
                TransactionQTY = transaction.TransactionQTY,
                TransactionWeight = transaction.TransactionWeight,
                GrossWeight = transaction.GrossWeight,
                Tare = transaction.Tare,
                Pieces = transaction.Pieces,
                Alibi = transaction.Alibi,
                WarehouseID = transaction.WarehouseID,
                LocationID = transaction.LocationID,
                INMasterID = transaction.INMasterID,
                ContainerID = transaction.ContainerID,
                Consumed = transaction.Consumed,
                ManualWeight = transaction.ManualWeight,
                DeviceMasterID = transaction.DeviceMasterID,
                UserMasterID = transaction.UserMasterID,
                IsBox = transaction.IsBox,
                CanAddToBox = transaction.CanAddToBox,
                InLocation = transaction.InLocation,
                StockTransactionID_Container = transaction.StockTransactionID_Container,
                LabelID = transaction.LabelID,
                Comments = transaction.Comments,
                Reference = transaction.Reference,
                Deleted = transaction.Deleted,
                BPMasterID = transaction.BPMasterID,
                StoredLabelID = transaction.StoredLabelID,
                Price = transaction.Price,
                AttributeID = transaction.AttributeID,
                BatchID_Base = transaction.BatchID_Base,
                SplitID = transaction.SplitID,
                ProcessID = transaction.ProcessID,
                StockTransactionID_Pallet = transaction.StockTransactionID_Pallet
            };
        }

        /// <summary>
        /// Creates a copy order.
        /// </summary>
        /// <param name="order">The order to copy.</param>
        /// <returns>A copied order.</returns>
        protected PROrder CreatePROrder(PROrder order)
        {
            return new PROrder
            {
                DocumentNumberingID = order.DocumentNumberingID,
                Number = order.Number,
                NouDocStatusID = order.NouDocStatusID,
                ReleaseDateTime = order.ReleaseDateTime,
                Reference = order.Reference,
                CreationDate = order.CreationDate,
                PRSpecID = order.PRSpecID,
                SpecificationNameIfModified = order.SpecificationNameIfModified,
                BPContactID = order.BPContactID,
                UserID = order.UserID,
                DeviceID = order.DeviceID,
                BatchNumberID = order.BatchNumberID,
                ScheduledDate = order.ScheduledDate,
                BPMasterID = order.BPMasterID,
                Deleted = order.Deleted,
                QALabelPrinted = order.QALabelPrinted,
                EditDate = order.EditDate
            };
        }

        /// <summary>
        /// Updates the db attribute.
        /// </summary>
        /// <param name="attribute">The db attribute.</param>
        /// <param name="batchAttribute">The neww batch attribute data.</param>
        protected void UpdateAttribute(Attribute attribute, StockDetail batchAttribute)
        {
            #region attributes

            if (!string.IsNullOrEmpty(batchAttribute.Attribute1)) { attribute.Attribute1 = batchAttribute.Attribute1; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute2)) { attribute.Attribute2 = batchAttribute.Attribute2; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute3)) { attribute.Attribute3 = batchAttribute.Attribute3; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute4)) { attribute.Attribute4 = batchAttribute.Attribute4; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute5)) { attribute.Attribute5 = batchAttribute.Attribute5; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute6)) { attribute.Attribute6 = batchAttribute.Attribute6; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute7)) { attribute.Attribute7 = batchAttribute.Attribute7; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute8)) { attribute.Attribute8 = batchAttribute.Attribute8; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute9)) { attribute.Attribute9 = batchAttribute.Attribute9; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute10)) { attribute.Attribute10 = batchAttribute.Attribute10; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute11)) { attribute.Attribute11 = batchAttribute.Attribute11; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute12)) { attribute.Attribute12 = batchAttribute.Attribute12; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute13)) { attribute.Attribute13 = batchAttribute.Attribute13; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute14)) { attribute.Attribute14 = batchAttribute.Attribute14; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute15)) { attribute.Attribute15 = batchAttribute.Attribute15; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute16)) { attribute.Attribute16 = batchAttribute.Attribute16; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute17)) { attribute.Attribute17 = batchAttribute.Attribute17; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute18)) { attribute.Attribute18 = batchAttribute.Attribute18; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute19)) { attribute.Attribute19 = batchAttribute.Attribute19; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute20)) { attribute.Attribute20 = batchAttribute.Attribute20; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute21)) { attribute.Attribute21 = batchAttribute.Attribute21; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute22)) { attribute.Attribute22 = batchAttribute.Attribute22; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute23)) { attribute.Attribute23 = batchAttribute.Attribute23; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute24)) { attribute.Attribute24 = batchAttribute.Attribute24; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute25)) { attribute.Attribute25 = batchAttribute.Attribute25; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute26)) { attribute.Attribute26 = batchAttribute.Attribute26; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute27)) { attribute.Attribute27 = batchAttribute.Attribute27; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute28)) { attribute.Attribute28 = batchAttribute.Attribute28; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute29)) { attribute.Attribute29 = batchAttribute.Attribute29; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute30)) { attribute.Attribute30 = batchAttribute.Attribute30; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute31)) { attribute.Attribute31 = batchAttribute.Attribute31; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute32)) { attribute.Attribute32 = batchAttribute.Attribute32; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute33)) { attribute.Attribute33 = batchAttribute.Attribute33; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute34)) { attribute.Attribute34 = batchAttribute.Attribute34; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute35)) { attribute.Attribute35 = batchAttribute.Attribute35; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute36)) { attribute.Attribute36 = batchAttribute.Attribute36; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute37)) { attribute.Attribute37 = batchAttribute.Attribute37; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute38)) { attribute.Attribute38 = batchAttribute.Attribute38; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute39)) { attribute.Attribute39 = batchAttribute.Attribute39; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute40)) { attribute.Attribute40 = batchAttribute.Attribute40; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute41)) { attribute.Attribute41 = batchAttribute.Attribute41; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute42)) { attribute.Attribute42 = batchAttribute.Attribute42; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute43)) { attribute.Attribute43 = batchAttribute.Attribute43; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute44)) { attribute.Attribute44 = batchAttribute.Attribute44; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute45)) { attribute.Attribute45 = batchAttribute.Attribute45; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute46)) { attribute.Attribute46 = batchAttribute.Attribute46; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute47)) { attribute.Attribute47 = batchAttribute.Attribute47; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute48)) { attribute.Attribute48 = batchAttribute.Attribute48; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute49)) { attribute.Attribute49 = batchAttribute.Attribute49; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute50)) { attribute.Attribute50 = batchAttribute.Attribute50; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute51)) { attribute.Attribute51 = batchAttribute.Attribute51; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute52)) { attribute.Attribute52 = batchAttribute.Attribute52; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute53)) { attribute.Attribute53 = batchAttribute.Attribute53; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute54)) { attribute.Attribute54 = batchAttribute.Attribute54; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute55)) { attribute.Attribute55 = batchAttribute.Attribute55; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute56)) { attribute.Attribute56 = batchAttribute.Attribute56; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute57)) { attribute.Attribute57 = batchAttribute.Attribute57; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute58)) { attribute.Attribute58 = batchAttribute.Attribute58; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute59)) { attribute.Attribute59 = batchAttribute.Attribute59; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute60)) { attribute.Attribute60 = batchAttribute.Attribute60; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute61)) { attribute.Attribute61 = batchAttribute.Attribute61; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute62)) { attribute.Attribute62 = batchAttribute.Attribute62; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute63)) { attribute.Attribute63 = batchAttribute.Attribute63; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute64)) { attribute.Attribute64 = batchAttribute.Attribute64; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute65)) { attribute.Attribute65 = batchAttribute.Attribute65; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute66)) { attribute.Attribute66 = batchAttribute.Attribute66; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute67)) { attribute.Attribute67 = batchAttribute.Attribute67; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute68)) { attribute.Attribute68 = batchAttribute.Attribute68; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute69)) { attribute.Attribute69 = batchAttribute.Attribute69; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute70)) { attribute.Attribute70 = batchAttribute.Attribute70; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute71)) { attribute.Attribute71 = batchAttribute.Attribute71; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute72)) { attribute.Attribute72 = batchAttribute.Attribute72; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute73)) { attribute.Attribute73 = batchAttribute.Attribute73; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute74)) { attribute.Attribute74 = batchAttribute.Attribute74; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute75)) { attribute.Attribute75 = batchAttribute.Attribute75; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute76)) { attribute.Attribute76 = batchAttribute.Attribute76; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute77)) { attribute.Attribute77 = batchAttribute.Attribute77; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute78)) { attribute.Attribute78 = batchAttribute.Attribute78; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute79)) { attribute.Attribute79 = batchAttribute.Attribute79; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute80)) { attribute.Attribute80 = batchAttribute.Attribute80; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute81)) { attribute.Attribute81 = batchAttribute.Attribute81; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute82)) { attribute.Attribute82 = batchAttribute.Attribute82; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute83)) { attribute.Attribute83 = batchAttribute.Attribute83; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute84)) { attribute.Attribute84 = batchAttribute.Attribute84; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute85)) { attribute.Attribute85 = batchAttribute.Attribute85; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute86)) { attribute.Attribute86 = batchAttribute.Attribute86; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute87)) { attribute.Attribute87 = batchAttribute.Attribute87; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute88)) { attribute.Attribute88 = batchAttribute.Attribute88; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute89)) { attribute.Attribute89 = batchAttribute.Attribute89; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute90)) { attribute.Attribute90 = batchAttribute.Attribute90; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute91)) { attribute.Attribute91 = batchAttribute.Attribute91; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute92)) { attribute.Attribute92 = batchAttribute.Attribute92; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute93)) { attribute.Attribute93 = batchAttribute.Attribute93; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute94)) { attribute.Attribute94 = batchAttribute.Attribute94; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute95)) { attribute.Attribute95 = batchAttribute.Attribute95; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute96)) { attribute.Attribute96 = batchAttribute.Attribute96; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute97)) { attribute.Attribute97 = batchAttribute.Attribute97; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute98)) { attribute.Attribute98 = batchAttribute.Attribute98; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute99)) { attribute.Attribute99 = batchAttribute.Attribute99; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute100)) { attribute.Attribute100 = batchAttribute.Attribute100; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute101)) { attribute.Attribute101 = batchAttribute.Attribute101; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute102)) { attribute.Attribute102 = batchAttribute.Attribute102; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute103)) { attribute.Attribute103 = batchAttribute.Attribute103; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute104)) { attribute.Attribute104 = batchAttribute.Attribute104; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute105)) { attribute.Attribute105 = batchAttribute.Attribute105; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute106)) { attribute.Attribute106 = batchAttribute.Attribute106; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute107)) { attribute.Attribute107 = batchAttribute.Attribute107; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute108)) { attribute.Attribute108 = batchAttribute.Attribute108; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute109)) { attribute.Attribute109 = batchAttribute.Attribute109; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute110)) { attribute.Attribute110 = batchAttribute.Attribute110; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute111)) { attribute.Attribute111 = batchAttribute.Attribute111; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute112)) { attribute.Attribute112 = batchAttribute.Attribute112; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute113)) { attribute.Attribute113 = batchAttribute.Attribute113; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute114)) { attribute.Attribute114 = batchAttribute.Attribute114; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute115)) { attribute.Attribute115 = batchAttribute.Attribute115; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute116)) { attribute.Attribute116 = batchAttribute.Attribute116; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute117)) { attribute.Attribute117 = batchAttribute.Attribute117; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute118)) { attribute.Attribute118 = batchAttribute.Attribute118; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute119)) { attribute.Attribute119 = batchAttribute.Attribute119; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute120)) { attribute.Attribute120 = batchAttribute.Attribute120; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute121)) { attribute.Attribute121 = batchAttribute.Attribute121; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute122)) { attribute.Attribute122 = batchAttribute.Attribute122; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute123)) { attribute.Attribute123 = batchAttribute.Attribute123; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute124)) { attribute.Attribute124 = batchAttribute.Attribute124; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute125)) { attribute.Attribute125 = batchAttribute.Attribute125; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute126)) { attribute.Attribute126 = batchAttribute.Attribute126; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute127)) { attribute.Attribute127 = batchAttribute.Attribute127; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute128)) { attribute.Attribute128 = batchAttribute.Attribute128; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute129)) { attribute.Attribute129 = batchAttribute.Attribute129; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute130)) { attribute.Attribute130 = batchAttribute.Attribute130; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute131)) { attribute.Attribute131 = batchAttribute.Attribute131; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute132)) { attribute.Attribute132 = batchAttribute.Attribute132; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute133)) { attribute.Attribute133 = batchAttribute.Attribute133; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute134)) { attribute.Attribute134 = batchAttribute.Attribute134; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute135)) { attribute.Attribute135 = batchAttribute.Attribute135; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute136)) { attribute.Attribute136 = batchAttribute.Attribute136; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute137)) { attribute.Attribute137 = batchAttribute.Attribute137; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute138)) { attribute.Attribute138 = batchAttribute.Attribute138; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute139)) { attribute.Attribute139 = batchAttribute.Attribute139; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute140)) { attribute.Attribute140 = batchAttribute.Attribute140; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute141)) { attribute.Attribute141 = batchAttribute.Attribute141; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute142)) { attribute.Attribute142 = batchAttribute.Attribute142; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute143)) { attribute.Attribute143 = batchAttribute.Attribute143; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute144)) { attribute.Attribute144 = batchAttribute.Attribute144; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute145)) { attribute.Attribute145 = batchAttribute.Attribute145; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute146)) { attribute.Attribute146 = batchAttribute.Attribute146; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute147)) { attribute.Attribute147 = batchAttribute.Attribute147; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute148)) { attribute.Attribute148 = batchAttribute.Attribute148; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute149)) { attribute.Attribute149 = batchAttribute.Attribute149; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute150)) { attribute.Attribute150 = batchAttribute.Attribute150; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute151)) { attribute.Attribute151 = batchAttribute.Attribute151; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute152)) { attribute.Attribute152 = batchAttribute.Attribute152; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute153)) { attribute.Attribute153 = batchAttribute.Attribute153; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute154)) { attribute.Attribute154 = batchAttribute.Attribute154; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute155)) { attribute.Attribute155 = batchAttribute.Attribute155; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute156)) { attribute.Attribute156 = batchAttribute.Attribute156; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute157)) { attribute.Attribute157 = batchAttribute.Attribute157; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute158)) { attribute.Attribute158 = batchAttribute.Attribute158; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute159)) { attribute.Attribute159 = batchAttribute.Attribute159; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute160)) { attribute.Attribute160 = batchAttribute.Attribute160; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute161)) { attribute.Attribute161 = batchAttribute.Attribute161; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute162)) { attribute.Attribute162 = batchAttribute.Attribute162; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute163)) { attribute.Attribute163 = batchAttribute.Attribute163; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute164)) { attribute.Attribute164 = batchAttribute.Attribute164; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute165)) { attribute.Attribute165 = batchAttribute.Attribute165; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute166)) { attribute.Attribute166 = batchAttribute.Attribute166; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute167)) { attribute.Attribute167 = batchAttribute.Attribute167; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute168)) { attribute.Attribute168 = batchAttribute.Attribute168; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute169)) { attribute.Attribute169 = batchAttribute.Attribute169; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute170)) { attribute.Attribute170 = batchAttribute.Attribute170; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute171)) { attribute.Attribute171 = batchAttribute.Attribute171; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute172)) { attribute.Attribute172 = batchAttribute.Attribute172; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute173)) { attribute.Attribute173 = batchAttribute.Attribute173; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute174)) { attribute.Attribute174 = batchAttribute.Attribute174; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute175)) { attribute.Attribute175 = batchAttribute.Attribute175; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute176)) { attribute.Attribute176 = batchAttribute.Attribute176; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute177)) { attribute.Attribute177 = batchAttribute.Attribute177; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute178)) { attribute.Attribute178 = batchAttribute.Attribute178; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute179)) { attribute.Attribute179 = batchAttribute.Attribute179; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute180)) { attribute.Attribute180 = batchAttribute.Attribute180; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute181)) { attribute.Attribute181 = batchAttribute.Attribute181; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute182)) { attribute.Attribute182 = batchAttribute.Attribute182; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute183)) { attribute.Attribute183 = batchAttribute.Attribute183; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute184)) { attribute.Attribute184 = batchAttribute.Attribute184; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute185)) { attribute.Attribute185 = batchAttribute.Attribute185; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute186)) { attribute.Attribute186 = batchAttribute.Attribute186; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute187)) { attribute.Attribute187 = batchAttribute.Attribute187; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute188)) { attribute.Attribute188 = batchAttribute.Attribute188; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute189)) { attribute.Attribute189 = batchAttribute.Attribute189; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute190)) { attribute.Attribute190 = batchAttribute.Attribute190; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute191)) { attribute.Attribute191 = batchAttribute.Attribute191; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute192)) { attribute.Attribute192 = batchAttribute.Attribute192; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute193)) { attribute.Attribute193 = batchAttribute.Attribute193; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute194)) { attribute.Attribute194 = batchAttribute.Attribute194; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute195)) { attribute.Attribute195 = batchAttribute.Attribute195; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute196)) { attribute.Attribute196 = batchAttribute.Attribute196; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute197)) { attribute.Attribute197 = batchAttribute.Attribute197; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute198)) { attribute.Attribute198 = batchAttribute.Attribute198; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute199)) { attribute.Attribute199 = batchAttribute.Attribute199; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute200)) { attribute.Attribute200 = batchAttribute.Attribute200; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute201)) { attribute.Attribute201 = batchAttribute.Attribute201; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute202)) { attribute.Attribute202 = batchAttribute.Attribute202; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute203)) { attribute.Attribute203 = batchAttribute.Attribute203; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute204)) { attribute.Attribute204 = batchAttribute.Attribute204; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute205)) { attribute.Attribute205 = batchAttribute.Attribute205; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute206)) { attribute.Attribute206 = batchAttribute.Attribute206; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute207)) { attribute.Attribute207 = batchAttribute.Attribute207; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute208)) { attribute.Attribute208 = batchAttribute.Attribute208; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute209)) { attribute.Attribute209 = batchAttribute.Attribute209; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute210)) { attribute.Attribute210 = batchAttribute.Attribute210; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute211)) { attribute.Attribute211 = batchAttribute.Attribute211; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute212)) { attribute.Attribute212 = batchAttribute.Attribute212; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute213)) { attribute.Attribute213 = batchAttribute.Attribute213; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute214)) { attribute.Attribute214 = batchAttribute.Attribute214; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute215)) { attribute.Attribute215 = batchAttribute.Attribute215; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute216)) { attribute.Attribute216 = batchAttribute.Attribute216; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute217)) { attribute.Attribute217 = batchAttribute.Attribute217; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute218)) { attribute.Attribute218 = batchAttribute.Attribute218; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute219)) { attribute.Attribute219 = batchAttribute.Attribute219; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute220)) { attribute.Attribute220 = batchAttribute.Attribute220; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute221)) { attribute.Attribute221 = batchAttribute.Attribute221; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute222)) { attribute.Attribute222 = batchAttribute.Attribute222; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute223)) { attribute.Attribute223 = batchAttribute.Attribute223; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute224)) { attribute.Attribute224 = batchAttribute.Attribute224; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute225)) { attribute.Attribute225 = batchAttribute.Attribute225; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute226)) { attribute.Attribute226 = batchAttribute.Attribute226; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute227)) { attribute.Attribute227 = batchAttribute.Attribute227; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute228)) { attribute.Attribute228 = batchAttribute.Attribute228; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute229)) { attribute.Attribute229 = batchAttribute.Attribute229; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute230)) { attribute.Attribute230 = batchAttribute.Attribute230; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute231)) { attribute.Attribute231 = batchAttribute.Attribute231; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute232)) { attribute.Attribute232 = batchAttribute.Attribute232; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute233)) { attribute.Attribute233 = batchAttribute.Attribute233; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute234)) { attribute.Attribute234 = batchAttribute.Attribute234; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute235)) { attribute.Attribute235 = batchAttribute.Attribute235; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute236)) { attribute.Attribute236 = batchAttribute.Attribute236; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute237)) { attribute.Attribute237 = batchAttribute.Attribute237; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute238)) { attribute.Attribute238 = batchAttribute.Attribute238; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute239)) { attribute.Attribute239 = batchAttribute.Attribute239; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute240)) { attribute.Attribute240 = batchAttribute.Attribute240; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute241)) { attribute.Attribute241 = batchAttribute.Attribute241; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute242)) { attribute.Attribute242 = batchAttribute.Attribute242; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute243)) { attribute.Attribute243 = batchAttribute.Attribute243; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute244)) { attribute.Attribute244 = batchAttribute.Attribute244; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute245)) { attribute.Attribute245 = batchAttribute.Attribute245; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute246)) { attribute.Attribute246 = batchAttribute.Attribute246; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute247)) { attribute.Attribute247 = batchAttribute.Attribute247; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute248)) { attribute.Attribute248 = batchAttribute.Attribute248; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute249)) { attribute.Attribute249 = batchAttribute.Attribute249; }
            if (!string.IsNullOrEmpty(batchAttribute.Attribute250)) { attribute.Attribute250 = batchAttribute.Attribute250; }


            #endregion
        }

        /// <summary>
        /// Logs data to the db.
        /// </summary>
        /// <param name="log">The data to log.</param>
        /// <returns>Flag, as to successful log or not.</returns>
        public bool LogToDatabase(Log log)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.Logs.Add(log);
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
    }
}
