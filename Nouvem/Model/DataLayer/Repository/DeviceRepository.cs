﻿// -----------------------------------------------------------------------
// <copyright file="SampleViewModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Global;
using Nouvem.Shared;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.DataLayer.Interface;

    public class DeviceRepository : NouvemRepositoryBase, IDeviceRepository
    {
        /// <summary>
        /// Add a new device to the database.
        /// </summary>
        /// <param name="device">The device to add.</param>
        /// <returns>A flag, indicating a successful add or not.</returns>
        public bool AddDevice(DeviceMaster device, int? copyId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to add a new device -" +
                                                           " mac:{0}, name:{1}, ip:{2}", device.DeviceMAC, device.DeviceName, device.DeviceIP));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.DeviceMasters.Add(device);
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), string.Format("New device with id {0} successfully added", device.DeviceID));

                    if (copyId.HasValue)
                    {
                        entities.App_CopyDeviceSettings(copyId, device.DeviceID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return true;
        }

        /// <summary>
        /// Method that updates an existing device.
        /// </summary>
        /// <param name="device">The device to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDevice(DeviceMaster device, int? copyId)
        {
            this.Log.LogDebug(this.GetType(), "UpdateDevice(): Attempting to update a device record.");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbDevice = entities.DeviceMasters.FirstOrDefault(
                        x => x.DeviceID == device.DeviceID);

                    if (dbDevice != null)
                    {
                        dbDevice.DeviceMAC = device.DeviceMAC;
                        dbDevice.DeviceName = device.DeviceName;
                        dbDevice.DeviceIP = device.DeviceIP;
                        dbDevice.Remark = device.Remark;
                        dbDevice.ActiveFrom = device.ActiveFrom;
                        dbDevice.ActiveTo = device.ActiveTo;
                        dbDevice.InActiveFrom = device.InActiveFrom;
                        dbDevice.InActiveTo = device.InActiveTo;
                        dbDevice.PreviousVersion = device.PreviousVersion;
                        dbDevice.CurrentVersion = device.CurrentVersion;

                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("Device with device id {0} has been successfully updated", device.DeviceID));

                        if (copyId.HasValue)
                        {
                            entities.App_CopyDeviceSettings(copyId, dbDevice.DeviceID);
                        }
                    }
                    else
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Device with device id {0} was not found", device.DeviceID));
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }
        }
      
        /// <summary>
        /// Add a new device to the database.
        /// </summary>
        /// <param name="device">The device to add.</param>
        /// <returns>A flag, indicating a successful add or not.</returns>
        public bool AddDeviceWithLookUp(DeviceMaster device, IList<int> departments, int? copyId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to add a new device -" +
                                                           " mac:{0}, name:{1}, ip:{2}", device.DeviceMAC, device.DeviceName, device.DeviceIP));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.DeviceMasters.Add(device);
                    entities.SaveChanges();

                    if (copyId.HasValue)
                    {
                        entities.App_CopyDeviceSettings(copyId, device.DeviceID);
                    }

                    if (departments.Any())
                    {
                        foreach (var department in departments)
                        {
                            entities.DepartmentLookUps.Add(new DepartmentLookUp
                            {
                                DeviceMasterID = device.DeviceID,
                                DepartmentID = department,
                                DeviceID = NouvemGlobal.DeviceId,
                                UserMasterID = NouvemGlobal.UserId
                            });

                            entities.SaveChanges();
                        }
                    }

                    this.Log.LogDebug(this.GetType(), string.Format("New device with id {0} successfully added", device.DeviceID));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }

            return true;
        }

        /// <summary>
        /// Method that updates an existing device.
        /// </summary>
        /// <param name="device">The device to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDeviceWithLookups(DeviceMaster device, IList<int> departments, int? copyId)
        {
            this.Log.LogDebug(this.GetType(), "UpdateDevice(): Attempting to update a device record.");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbDevice = entities.DeviceMasters.FirstOrDefault(
                        x => x.DeviceID == device.DeviceID);

                    if (dbDevice != null)
                    {
                        dbDevice.DeviceMAC = device.DeviceMAC;
                        dbDevice.DeviceName = device.DeviceName;
                        dbDevice.DeviceIP = device.DeviceIP;
                        dbDevice.Remark = device.Remark;
                        dbDevice.ActiveFrom = device.ActiveFrom;
                        dbDevice.ActiveTo = device.ActiveTo;
                        dbDevice.InActiveFrom = device.InActiveFrom;
                        dbDevice.InActiveTo = device.InActiveTo;
                        dbDevice.PreviousVersion = device.PreviousVersion;
                        dbDevice.CurrentVersion = device.CurrentVersion;
                        var lookUps = entities.DepartmentLookUps.Where(x => x.DeviceMasterID == dbDevice.DeviceID);
                        foreach (var departmentLookUp in lookUps)
                        {
                            departmentLookUp.Deleted = DateTime.Now;
                        }

                        entities.SaveChanges();

                        if (copyId.HasValue)
                        {
                            entities.App_CopyDeviceSettings(copyId, dbDevice.DeviceID);
                        }

                        if (departments.Any())
                        {
                            foreach (var department in departments)
                            {
                                entities.DepartmentLookUps.Add(new DepartmentLookUp
                                {
                                    DeviceMasterID = device.DeviceID,
                                    DepartmentID = department,
                                    DeviceID = NouvemGlobal.DeviceId,
                                    UserMasterID = NouvemGlobal.UserId
                                });

                                entities.SaveChanges();
                            }
                        }

                        this.Log.LogDebug(this.GetType(), string.Format("Device with device id {0} has been successfully updated", device.DeviceID));
                    }
                    else
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Device with device id {0} was not found", device.DeviceID));
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                throw;
            }
        }

        /// <summary>
        /// Retrieve all the database devices.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        public IList<App_GetDeviceSettings_Result> GetDeviceSettings()
        {
            this.Log.LogDebug(this.GetType(), "GetDevices(): Attempting to retrieve all the devices");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetDeviceSettings().ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieve all the database devices.
        /// </summary>
        /// <returns>A collection of database devices.</returns>
        public IList<DeviceMaster> GetDevices()
        {
            this.Log.LogDebug(this.GetType(), "GetDevices(): Attempting to retrieve all the devices");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var devices = entities.DeviceMasters.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} devices successfully retrieved", devices.Count));

                    return devices;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return new List<DeviceMaster>();
        }

        /// <summary>
        /// Retrieves any new entities that need to be added locally.
        /// </summary>
        /// <param name="deviceId">The local device.</param>
        /// <returns>A list of new entities that are to be added locally.</returns>
        public IList<EntityUpdate> CheckForNewEntities(int deviceId, string entityType = "")
        {
            var newEntities = new List<EntityUpdate>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (entityType == string.Empty)
                    {
                        newEntities = entities.EntityUpdates.Where(x => x.DeviceID == deviceId && x.Consumed == null).ToList();
                    }
                    else
                    {
                        newEntities = entities.EntityUpdates.Where(x => x.DeviceID == deviceId && x.Consumed == null && x.Name == entityType).ToList();
                    }
                  
                    if (newEntities.Any())
                    {
                        foreach (var entityUpdate in newEntities)
                        {
                            entityUpdate.Consumed = DateTime.Now;
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return newEntities;
        }

        /// <summary>
        /// Determines if a device name already exists in the db.
        /// </summary>
        /// <param name="name">The name to check.</param>
        /// <returns>Flag, as to whether the device already exists.</returns>
        public bool IsDeviceNameInSystem(string name)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.DeviceMasters.Any(x => x.DeviceMAC.ToLower() == name.ToLower());
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates the current device settings.
        /// </summary>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDeviceSettings(IList<DeviceSetting> settings, bool update)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to update {0} device settings for device id:{1}", settings.Count, NouvemGlobal.DeviceId));
            var deviceId = NouvemGlobal.DeviceId;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    #region test device

                    var first = entities.DeviceSettings.FirstOrDefault(x => x.Name == "ScannerMode");
                    if (first != null)
                    {
                        if (!string.IsNullOrWhiteSpace(first.Value16))
                        {
                            deviceId = first.Value16.ToInt();
                        }
                    }

                    #endregion

                    foreach (var setting in settings)
                    {
                        var dbSettings =
                                entities.DeviceSettings.FirstOrDefault(
                                    x => x.Name.Trim() == setting.Name.Trim() && x.DeviceMasterID == deviceId);

                        if (dbSettings == null)
                        {
                            entities.DeviceSettings.Add(setting);
                        }
                        else if (update)
                        {
                            dbSettings.Name = setting.Name;
                            dbSettings.Value1 = setting.Value1;
                            dbSettings.Value2 = setting.Value2;
                            dbSettings.Value3 = setting.Value3;
                            dbSettings.Value4 = setting.Value4;
                            dbSettings.Value5 = setting.Value5;
                            dbSettings.Value6 = setting.Value6;
                            dbSettings.Value7 = setting.Value7;
                            dbSettings.Value8 = setting.Value8;
                            dbSettings.Value9 = setting.Value9;
                            dbSettings.Value10 = setting.Value10;
                            dbSettings.Value11 = setting.Value11;
                            dbSettings.Value12 = setting.Value12;
                            dbSettings.Value13 = setting.Value13;
                            dbSettings.Value14 = setting.Value14;
                            dbSettings.Value15 = setting.Value15;
                            dbSettings.Value16 = setting.Value16;
                            dbSettings.Value17 = setting.Value17;
                            dbSettings.Value18 = setting.Value18;
                            dbSettings.Value19 = setting.Value19;
                            dbSettings.Value20 = setting.Value20;
                            dbSettings.Deleted = setting.Deleted;
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Device settings updated");
                }
            }
            catch(Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return true;
        }
        
        /// <summary>
        /// Gets the current device settings.
        /// </summary>
        /// <returns>The current device settings.</returns>
        public IList<DeviceSetting> GetDeviceSettings(int deviceId)
        {
            this.Log.LogInfo(this.GetType(), string.Format("Attempting to retrieve the device settings for device id:{0}", deviceId));
            var settings = new List<DeviceSetting>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    #region test device

                    var first = entities.DeviceSettings.FirstOrDefault(x => x.Name == "ScannerMode");
                    if (first != null)
                    {
                        if (!string.IsNullOrWhiteSpace(first.Value16))
                        {
                            deviceId = first.Value16.ToInt();
                        }
                    }

                    #endregion

                    settings = entities.DeviceSettings.Where(x => x.DeviceMasterID == deviceId).ToList();
                    this.Log.LogInfo(this.GetType(), string.Format("{0} device settings retrieved", settings.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return settings;
        }

        /// <summary>
        /// Determines if there are new price edits to apply.
        /// </summary>
        /// <returns>Flag, as to whether there are new edits to apply.</returns>
        public bool AreTherePriceEdits()
        {
            var areThereEdits = false;
            using (var entities = new NouvemEntities())
            {
                var localUpdateToCheck =
                    entities.ApplicationUpdates.FirstOrDefault(x => x.DeviceMasterID == NouvemGlobal.DeviceId);
                if (localUpdateToCheck != null)
                {
                    areThereEdits = localUpdateToCheck.Pricing.ToBool();
                    if (areThereEdits)
                    {
                        localUpdateToCheck.Pricing = false;
                        entities.SaveChanges();
                    }
                }
            }

            return areThereEdits;
        }

        /// <summary>
        /// Sets the aplication updates..
        /// </summary>
        /// <returns>Flag, as to whether the update have been updated..</returns>
        public bool SetApplicationUpdates()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var devices = entities.DeviceMasters;
                    var updates = entities.ApplicationUpdates;
                    foreach (var deviceMaster in devices)
                    {
                        var localUpdate = updates.FirstOrDefault(x => x.DeviceMasterID == deviceMaster.DeviceID);
                        if (localUpdate == null)
                        {
                            updates.Add(new ApplicationUpdate { DeviceMasterID = deviceMaster.DeviceID});
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Sets the default module process.
        /// </summary>
        /// <param name="module">The module to set.</param>
        /// <param name="process">The default process.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool SetDefaultModuleProcess(string module, string process, int deviceId)
        {
            var localModule = module;
            if (module.CompareIgnoringCase("StockMovement"))
            {
                localModule = "IntoProduction";
            }

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var settings = entities.DeviceSettings.Where(x => x.Name.ToLower() == localModule.ToLower() && x.DeviceMasterID == deviceId);
                    foreach (var deviceSetting in settings)
                    {
                        if (module.CompareIgnoringCase("StockMovement"))
                        {
                            deviceSetting.Value4 = process;
                        }
                        else
                        {
                            deviceSetting.Value1 = process;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
    }
}
