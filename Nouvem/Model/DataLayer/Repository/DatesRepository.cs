﻿// -----------------------------------------------------------------------
// <copyright file="DatesRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.BusinessObject;
using StyleCop.CSharp;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer.Interface;

    public class DatesRepository : NouvemRepositoryBase, IDatesRepository
    {
        /// <summary>
        /// Retrieve all the date masters.
        /// </summary>
        /// <returns>A collection of date masters.</returns>
        public IList<ViewDatesMaster> GetDateMasters()
        {
            this.Log.LogDebug(this.GetType(), "GetDateMasters(): Attempting to retrieve all the date masters");
            var dateMasters = new List<ViewDatesMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    dateMasters = entities.ViewDatesMasters.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} date masters retrieved", dateMasters.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dateMasters;
        }

        /// <summary>
        /// Retrieve all the date masters.
        /// </summary>
        /// <returns>A collection of date masters.</returns>
        public IList<SearchDateType> GetSearchDateTypes()
        {
            var dateMasters = new List<SearchDateType>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    dateMasters = (from dateType in entities.NouSearchDateTypes.Where(x => x.Deleted == null)
                        select new SearchDateType
                        {
                            NouSearchDateTypeID = dateType.NouSearchDateTypeID,
                            DateType = dateType.DateType,
                            IsSelected = true
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dateMasters;
        }

        /// <summary>
        /// Retrieve all the date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        public IList<ViewDateTemplateAllocation> GetViewDateTemplateAllocations()
        {
            this.Log.LogDebug(this.GetType(), "GetViewDateTemplateAllocations(): Attempting to retrieve all the date template allocations");
            var allocations = new List<ViewDateTemplateAllocation>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    allocations = entities.ViewDateTemplateAllocations.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} date template allocations retrieved", allocations.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return allocations;
        }

        /// <summary>
        /// Retrieve all the system date types.
        /// </summary>
        /// <returns>A collection of date types.</returns>
        public IList<NouDateType> GetDateTypes()
        {
            this.Log.LogDebug(this.GetType(), "GetDateType: Attempting to retrieve all the date types");
            var dateTypes = new List<NouDateType>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    dateTypes = entities.NouDateTypes.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} date types retrieved", dateTypes.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dateTypes;
        }

        /// <summary>
        /// Adds or updates date masters.
        /// </summary>
        /// <param name="dateMasters">The date masters to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateDateMasters(IList<ViewDatesMaster> dateMasters)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateDateMasters(): Attempting to update the date masters");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    dateMasters.ToList().ForEach(x =>
                    {
                        if (x.DateMasterID == 0)
                        {
                            var dateMaster = new DateMaster
                            {
                                NouDateTypeID = x.NouDateTypeID,
                                GS1AIMasterID = x.GS1AIMasterID,
                                DateCode = x.DateCode,
                                DateDescription = x.DateDescription,
                                DateMasterID_BasedOn = x.DateMasterID_BasedOn,
                                Deleted = false
                            };

                            entities.DateMasters.Add(dateMaster);
                        }
                        else
                        {
                            var dbDataMaster =
                                        entities.DateMasters.FirstOrDefault(
                                            data => data.DateMasterID == x.DateMasterID && !data.Deleted);

                            if (dbDataMaster != null)
                            {
                                dbDataMaster.NouDateTypeID = x.NouDateTypeID;
                                dbDataMaster.GS1AIMasterID = x.GS1AIMasterID;
                                dbDataMaster.DateCode = x.DateCode;
                                dbDataMaster.DateMasterID_BasedOn = x.DateMasterID_BasedOn;
                                dbDataMaster.DateDescription = x.DateDescription;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Data masters successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the system date template names.
        /// </summary>
        /// <returns>A collection of date template names.</returns>
        public IList<DateTemplateName> GetDateTemplateNames()
        {
            this.Log.LogDebug(this.GetType(),
                "GetDateTemplateNames(): Attempting to retrieve all the date template names");
            var names = new List<DateTemplateName>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                    var entities = new NouvemEntities();
                    names = entities.DateTemplateNames.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("{0} date template names retrieved", names.Count()));
               // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return names;
        }

        /// <summary>
        /// Retrieve all the system date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        public IList<DateTemplateAllocation> GetDateTemplateAllocations()
        {
            this.Log.LogDebug(this.GetType(),
                "GetDateTemplateAllocations(): Attempting to retrieve all the date template allocations");
            var allocations = new List<DateTemplateAllocation>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    allocations = entities.DateTemplateAllocations.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("{0} date template allocations retrieved", allocations.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return allocations;
        }

        /// <summary>
        /// Adds or updates date names.
        /// </summary>
        /// <param name="dateNames">The date names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateDateTemplateNames(IList<DateTemplateName> dateNames)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateDateTemplateNames(): Attempting to update the date template names");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    dateNames.ToList().ForEach(x =>
                    {
                        if (x.DateTemplateNameID == 0)
                        {
                            var templateName = new DateTemplateName
                            {
                                Name = x.Name,
                                Deleted = false
                            };

                            entities.DateTemplateNames.Add(templateName);
                        }
                        else
                        {
                            var dateName =
                                entities.DateTemplateNames.FirstOrDefault(
                                    template =>
                                        template.DateTemplateNameID == x.DateTemplateNameID &&
                                        !template.Deleted);

                            if (dateName != null)
                            {
                                dateName.Name = x.Name;
                                dateName.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Date tempate names successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Adds a date master to a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddDateToTemplate(int dateMasterId, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddDateToTemplate(): Attempting to add datemaster id:{0} to template id:{1}", dateMasterId, templateId));
          
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var allocation = new DateTemplateAllocation
                    {
                        DateMasterID = dateMasterId,
                        DateTemplateNameID = templateId,
                        Deleted = false
                    };

                    entities.DateTemplateAllocations.Add(allocation);
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Allocation successful");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes a date master from a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        public bool RemoveDateFromTemplate(int dateMasterId, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveDateFromTemplate(): Attempting to remove datemaster id:{0} from template id:{1}", dateMasterId, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAllocation =
                        entities.DateTemplateAllocations.FirstOrDefault(
                            x => x.DateMasterID == dateMasterId && x.DateTemplateNameID == templateId && !x.Deleted);

                    if (dbAllocation != null)
                    {
                        dbAllocation.Deleted = true;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Removal successful");
                        return true;
                    }

                    this.Log.LogDebug(this.GetType(), "Removal unsuccessful");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a date masters to a template.
        /// </summary>
        /// <param name="dateMasters">The collection of the date masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddDatesToTemplate(IList<ViewDatesMaster> dateMasters, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddDatesToTemplate(): Attempting to add {0} date masters to template id:{1}", dateMasters.Count, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // Remove the current template quality fields.
                    var currentDMs =
                        entities.DateTemplateAllocations.Where(x => x.DateTemplateNameID == templateId);
                    entities.DateTemplateAllocations.RemoveRange(currentDMs);
                    entities.SaveChanges();

                    foreach (var dateMaster in dateMasters)
                    {
                        var allocation = new DateTemplateAllocation
                        {
                            DateMasterID = dateMaster.DateMasterID,
                            DateTemplateNameID = templateId,
                            Batch = dateMaster.Batch,
                            Transaction = dateMaster.Transaction,
                            Required = dateMaster.Required,
                            Reset = dateMaster.Reset,
                            Deleted = false
                        };

                        entities.DateTemplateAllocations.Add(allocation);
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Allocations successful");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// retrieves the date days.
        /// </summary>
        /// <returns>A collection of date days.</returns>
        public IList<DateDay> GetDateDays()
        {
            this.Log.LogDebug(this.GetType(), "DateDaysGet(): Attempting to retrieve the date days");
            var dateDays = new List<DateDay>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    dateDays = entities.DateDays.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} date days retrieved", dateDays.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dateDays;
        }

        /// <summary>
        /// Adds or updates date days.
        /// </summary>
        /// <param name="dateDays">The date days to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateDateDays(IList<DateDay> dateDays)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateDateDays(): Attempting to update the date days");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var dateDay in dateDays)
                    {
                        if (dateDay.Days == null)
                        {
                            continue;
                        }

                        if (dateDay.DateDaysID == 0)
                        {
                            entities.DateDays.Add(dateDay);
                        }
                        else
                        {
                            var dbDateDay = entities.DateDays.FirstOrDefault(x => x.DateDaysID == dateDay.DateDaysID);
                            if (dbDateDay != null)
                            {
                                dbDateDay.Days = dateDay.Days;
                            }
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Date days successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }
    }
}



