﻿// -----------------------------------------------------------------------
// <copyright file="AuditrRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer.Interface;

    public class AuditRepository : NouvemRepositoryBase, IAuditRepository
    {
        /// <summary>
        /// Gets the audit details.
        /// </summary>
        /// <returns>A collection of audit details.</returns>
        public IList<Audit> GetAuditDetails()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the audit details");
            var details = new List<Audit>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details = entities.Audits.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} audit details retrieved", details.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }

        /// <summary>
        /// Gets the audit details for the current day.
        /// </summary>
        /// <returns>A collection of todays audit details.</returns>
        public IList<Audit> GetTodaysAuditDetails()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the audit details for the current day");
            var details = new List<Audit>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    details = entities.Audits.Where(x => x.TransactionDate >= DateTime.Today).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} audit details retrieved for the current day", details.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return details;
        }
    }
}
