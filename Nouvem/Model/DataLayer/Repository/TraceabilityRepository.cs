﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Properties;

    public class TraceabilityRepository : NouvemRepositoryBase, ITraceabilityRepository
    {
        #region new

        /// <summary>
        /// Gets the allocation data for the input template.
        /// </summary>
        /// <returns>The associated template data.</returns>
        public IList<AttributeAllocationData> GetAttributeAllocationData(int templateId)
        {
            var data = new List<AttributeAllocationData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbData =
                        entities.AttributeAllocations.Where(
                            x => x.AttributeTemplateID == templateId && x.Deleted == null).ToList();

                    foreach (var attributeAllocation in dbData)
                    {
                        data.Add(new AttributeAllocationData
                        {
                            AttributionAllocation = attributeAllocation,
                            AttributeMaster = attributeAllocation.AttributeMaster,
                            NouTraceabilityType = attributeAllocation.AttributeMaster.NouTraceabilityType,
                            NouTraceabilityTypeNonStandard = attributeAllocation.AttributeMaster.NouTraceabilityType1
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }

        /// <summary>
        /// Gets the template groups.
        /// </summary>
        /// <returns>The template groups.</returns>
        public IList<AttributeTemplateGroup> GetTemplateGroups()
        {
            var groups = new List<AttributeTemplateGroup>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    groups = entities.AttributeTemplateGroups.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return groups;
        }

        /// <summary>
        /// Gets the templates.
        /// </summary>
        /// <returns>The template names.</returns>
        public IList<AttributeTemplateData> GetTemplateNames()
        {
            var templates = new List<AttributeTemplateData>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var data = entities.AttributeTemplates.Where(x => x.Deleted == null).ToList();
                    foreach (var attributeTemplate in data)
                    {
                        templates.Add(new AttributeTemplateData
                        {
                            AttributeTemplateID = attributeTemplate.AttributeTemplateID,
                            AttributeTemplateGroupID = attributeTemplate.AttributeTemplateGroupID,
                            Name = attributeTemplate.Name,
                            UserMasterID = attributeTemplate.UserMasterID,
                            DeviceID = attributeTemplate.DeviceID,
                            IsWorkflow = attributeTemplate.IsWorkflow.ToBool(),
                            Notes = attributeTemplate.Notes,
                            Inactive = attributeTemplate.Inactive,
                            AttributeType = attributeTemplate.AttributeType
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return templates;
        }

        #endregion

        #region old

        /// <summary>
        /// Retrieve all the traceability masters.
        /// </summary>
        ///  <returns>A collection of traceability masters.</returns>
        public IList<ViewTraceabilityMaster> GetTraceabilityMasters()
        {
            this.Log.LogDebug(this.GetType(),
                "GetTraceabilityMasters: Attempting to retrieve all the traceability masters");
            var traceabilityMasters = new List<ViewTraceabilityMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    traceabilityMasters = entities.ViewTraceabilityMasters.ToList();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("{0} traceability masters retrieved", traceabilityMasters.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return traceabilityMasters;
        }


        /// <summary>
        /// Adds or updates traceability masters.
        /// </summary>
        /// <param name="traceabilityMasters">The raceability masters to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateTraceabilityMasters(IList<ViewTraceabilityMaster> traceabilityMasters)
        {
            this.Log.LogDebug(this.GetType(),
                "AddOrUpdateTraceabilityMasters(): Attempting to update the traceability masters");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    traceabilityMasters.ToList().ForEach(x =>
                    {
                        if (x.TraceabilityTemplatesMasterID == 0)
                        {
                            var traceabilityMaster = new TraceabilityTemplateMaster
                            {
                                NouTraceabilityTypeID = x.NouTraceabilityTypeID,
                                GS1AIMasterID = x.GS1AIMasterID,
                                TraceabilityCode = x.TraceabilityCode,
                                TraceabilityDescription = x.TraceabilityDescription,
                                SQL = x.SQL,
                                Collection = x.Collection,
                                Deleted = false
                            };

                            entities.TraceabilityTemplateMasters.Add(traceabilityMaster);
                        }
                        else
                        {
                            var dbTraceabilityMaster =
                                         entities.TraceabilityTemplateMasters.FirstOrDefault(
                                             data =>
                                                 data.TraceabilityTemplatesMasterID == x.TraceabilityTemplatesMasterID &&
                                                 !data.Deleted);

                            if (dbTraceabilityMaster != null)
                            {
                                dbTraceabilityMaster.NouTraceabilityTypeID = x.NouTraceabilityTypeID;
                                dbTraceabilityMaster.GS1AIMasterID = x.GS1AIMasterID;
                                dbTraceabilityMaster.TraceabilityCode = x.TraceabilityCode;
                                dbTraceabilityMaster.TraceabilityDescription = x.TraceabilityDescription;
                                dbTraceabilityMaster.SQL = x.SQL;
                                dbTraceabilityMaster.Collection = x.Collection;
                                dbTraceabilityMaster.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Traceability masters successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the system traceability types.
        /// </summary>
        /// <returns>A collection of traceability types.</returns>
        public IList<NouTraceabilityType> GetTraceabilityTypes()
        {
            this.Log.LogDebug(this.GetType(), "GetTraceabilityTypes(): Attempting to retrieve all the traceability types");
            var types = new List<NouTraceabilityType>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    types = entities.NouTraceabilityTypes.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} traceability types retrieved", types.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return types;
        }

        /// <summary>
        /// Retrieve all the system traceability template names.
        /// </summary>
        /// <returns>A collection of traceability template names.</returns>
        public IList<TraceabilityTemplateName> GetTraceabilityTemplateNames()
        {
            this.Log.LogDebug(this.GetType(),
                "GetTraceabilityTemplateNames(): Attempting to retrieve all the traceability template names");
            var names = new List<TraceabilityTemplateName>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                names = entities.TraceabilityTemplateNames.Where(x => !x.Deleted).ToList();
                this.Log.LogDebug(this.GetType(),
                    string.Format("{0} traceability template names retrieved", names.Count()));
                //}
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return names;
        }

        /// <summary>
        /// Adds or updates traceability names.
        /// </summary>
        /// <param name="traceabilityNames">The traceability names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        public bool AddOrUpdateTraceabilityTemplateNames(IList<TraceabilityTemplateName> traceabilityNames)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateTraceabilityTemplateNames(): Attempting to update the traceability template names");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    traceabilityNames.ToList().ForEach(x =>
                    {
                        if (x.TraceabilityTemplateNameID == 0)
                        {
                            var templateName = new TraceabilityTemplateName
                            {
                                Name = x.Name,
                                Deleted = false
                            };

                            entities.TraceabilityTemplateNames.Add(templateName);
                        }
                        else
                        {
                            var traceabilityName =
                                entities.TraceabilityTemplateNames.FirstOrDefault(
                                    template =>
                                        template.TraceabilityTemplateNameID == x.TraceabilityTemplateNameID &&
                                        !template.Deleted);

                            if (traceabilityName != null)
                            {
                                traceabilityName.Name = x.Name;
                                traceabilityName.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Traceability tempate names successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the system traceability template allocations.
        /// </summary>
        /// <returns>A collection of traceability template allocations.</returns>
        public IList<TraceabilityTemplateAllocation> GetTraceabilityTemplateAllocations()
        {
            this.Log.LogDebug(this.GetType(),
                "GetTraceabilityTemplateAllocations(): Attempting to retrieve all the traceability template allocations");
            var allocations = new List<TraceabilityTemplateAllocation>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    allocations = entities.TraceabilityTemplateAllocations.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("{0} traceability template allocations retrieved", allocations.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return allocations;
        }

        /// <summary>
        /// Adds a traceability master to a template.
        /// </summary>
        /// <param name="traceabilityMasterId">The id of the traceability master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddTraceabilityToTemplate(int traceabilityMasterId, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddTraceabilityToTemplate(): Attempting to add traceabilitymaster id:{0} to template id:{1}", traceabilityMasterId, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var allocation = new TraceabilityTemplateAllocation
                    {
                        TraceabilityTemplateMasterID = traceabilityMasterId,
                        TraceabilityTemplateNameID = templateId,
                        Deleted = false
                    };

                    entities.TraceabilityTemplateAllocations.Add(allocation);
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Allocation successful");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes a traceability master from a template.
        /// </summary>
        /// <param name="traceabilityMasterId">The id of the traceability master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        public bool RemoveTraceabilityFromTemplate(int traceabilityMasterId, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveTraceabilityFromTemplate(): Attempting to remove traceabilitymaster id:{0} from template id:{1}", traceabilityMasterId, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAllocation =
                        entities.TraceabilityTemplateAllocations.FirstOrDefault(
                            x => x.TraceabilityTemplateMasterID == traceabilityMasterId && x.TraceabilityTemplateNameID == templateId && !x.Deleted);

                    if (dbAllocation != null)
                    {
                        dbAllocation.Deleted = true;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Removal successful");
                        return true;
                    }

                    this.Log.LogDebug(this.GetType(), "Removal unsuccessful");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a date masters to a template.
        /// </summary>
        /// <param name="traceabilityMasters">The collection of the date masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddTraceabilitysToTemplate(IList<ViewTraceabilityMaster> traceabilityMasters, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddTraceabilitysToTemplate(): Attempting to add {0} traceability masters to template id:{1}", traceabilityMasters.Count, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // Remove the current template quality fields.
                    var currentTMs =
                        entities.TraceabilityTemplateAllocations.Where(x => x.TraceabilityTemplateNameID == templateId);
                    entities.TraceabilityTemplateAllocations.RemoveRange(currentTMs);
                    entities.SaveChanges();

                    foreach (var traceabilityMaster in traceabilityMasters)
                    {
                        var allocation = new TraceabilityTemplateAllocation
                        {
                            TraceabilityTemplateMasterID = traceabilityMaster.TraceabilityTemplatesMasterID,
                            TraceabilityTemplateNameID = templateId,
                            Batch = traceabilityMaster.Batch,
                            Transaction = traceabilityMaster.Transaction,
                            Required = traceabilityMaster.Required,
                            Reset = traceabilityMaster.Reset,
                            Deleted = false
                        };

                        entities.TraceabilityTemplateAllocations.Add(allocation);
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Allocations successful");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieve all the traceability template allocations.
        /// </summary>
        /// <returns>A collection of traceability template allocations.</returns>
        public IList<ViewTraceabilityTemplateAllocation> GetViewTraceabilityTemplateAllocations()
        {
            this.Log.LogDebug(this.GetType(), "GetViewDateTemplateAllocations(): Attempting to retrieve all the traceability template allocations");
            var allocations = new List<ViewTraceabilityTemplateAllocation>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    allocations = entities.ViewTraceabilityTemplateAllocations.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} traceability template allocations retrieved", allocations.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return allocations;
        }

        /// <summary>
        /// Retrieve all the system traceability data.
        /// </summary>
        /// <returns>A collection of traceability data.</returns>
        public IList<TraceabilityData> GetTraceabilityData()
        {
            this.Log.LogDebug(this.GetType(), "GetTraceabilityData(): Attempting to retrieve all the traceability data");
            var data = new List<TraceabilityData>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var traceability = (from allocation in entities.TraceabilityTemplateAllocations
                        where !allocation.Deleted
                        select new TraceabilityData
                        {
                            Batch = allocation.Batch,
                            Transaction = allocation.Transaction,
                            Required = allocation.Required,
                            Reset = allocation.Reset,
                            TraceabilityName = allocation.TraceabilityTemplateName.Name,
                            TraceabilityNameID = allocation.TraceabilityTemplateNameID,
                            TraceabilityType = allocation.TraceabilityTemplateMaster.NouTraceabilityType.TraceabilityType,
                            TraceabilityTypeID = allocation.TraceabilityTemplateMaster.NouTraceabilityTypeID,
                            TraceabilityMasterID = allocation.TraceabilityTemplateMasterID,
                            TraceabilityMasterCode = allocation.TraceabilityTemplateMaster.TraceabilityCode,
                            TraceabilityMasterDescription = allocation.TraceabilityTemplateMaster.TraceabilityDescription,
                            TraceabilityMasterSQL = allocation.TraceabilityTemplateMaster.SQL,
                            TraceabilityMasterCollection = allocation.TraceabilityTemplateMaster.Collection,
                            TraceabilityMethod = Constant.Traceability
                        }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} traceability data retrieved", traceability.Count));

                    var quality = (from allocation in entities.QualityTemplateAllocations
                                        where !allocation.Deleted
                                        select new TraceabilityData
                                        {
                                            Batch = allocation.Batch,
                                            Transaction = allocation.Transaction,
                                            Required = allocation.Required,
                                            Reset = allocation.Reset,
                                            TraceabilityName = allocation.QualityTemplateName.Name,
                                            TraceabilityNameID = allocation.QualityTemplateNameID,
                                            TraceabilityType = allocation.QualityMaster.NouQualityType.QualityType,
                                            TraceabilityTypeID = allocation.QualityMaster.NouQualityTypeID,
                                            TraceabilityMasterID = allocation.QualityMasterID,
                                            TraceabilityMasterCode = allocation.QualityMaster.QualityCode,
                                            TraceabilityMasterDescription = allocation.QualityMaster.QualityDescription,
                                            TraceabilityMasterSQL = allocation.QualityMaster.SQL,
                                            TraceabilityMasterCollection = allocation.QualityMaster.Collection,
                                            TraceabilityMethod = Constant.Quality
                                        }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} quality data retrieved", quality.Count));

                    var date = (from allocation in entities.DateTemplateAllocations
                                   where !allocation.Deleted
                                   select new TraceabilityData
                                   {
                                       Batch = allocation.Batch,
                                       Transaction = allocation.Transaction,
                                       Required = allocation.Required,
                                       Reset = allocation.Reset,
                                       TraceabilityName = allocation.DateTemplateName.Name,
                                       TraceabilityNameID = allocation.DateTemplateNameID,
                                       TraceabilityType = Constant.Calender,
                                       TraceabilityTypeID = allocation.DateMaster.NouDateTypeID,
                                       TraceabilityMasterID = allocation.DateMasterID,
                                       TraceabilityMasterCode = allocation.DateMaster.DateCode,
                                       DateMasterBasedOn = allocation.DateMaster.DateMaster2,
                                       TraceabilityMasterDescription = allocation.DateMaster.DateDescription,
                                       TraceabilityMethod = Constant.Date,
                                       DateDays = allocation.DateMaster.DateDays.ToList()
                                   }).ToList();

                    this.Log.LogDebug(this.GetType(), string.Format("{0} date data retrieved", date.Count));

                    data = traceability.Concat(quality).Concat(date).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }

        /// <summary>
        /// Gets the traceability methods.
        /// </summary>
        /// <returns>A collection of traceability methods.</returns>
        public IList<NouTraceabilityMethod> GetTraceabilityMethods()
        {
            this.Log.LogDebug(this.GetType(), "GetTraceabilityMethods(): Attempting to retrieve the nou traceability methods");
            var methods = new List<NouTraceabilityMethod>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    methods = entities.NouTraceabilityMethods.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} methods retrieved", methods.Count));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return methods;
        }

        #endregion
    }
}
