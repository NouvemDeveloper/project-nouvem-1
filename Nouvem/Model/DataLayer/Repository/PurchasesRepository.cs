﻿// -----------------------------------------------------------------------
// <copyright file="PurchasesRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Entity.SqlServer;
using System.Windows;
using System.Windows.Threading;
using DevExpress.Xpf.Editors.DateNavigator.Controls;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Model.BusinessObject;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;

    public class PurchasesRepository: NouvemRepositoryBase, IPurchasesRepository
    {
        #region quote

        /// <summary> Adds a new purchase order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>The newly created quote id, indicating a successful quote add, or not.</returns>
        public int AddAPQuote(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var quote = new APQuote
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        QuoteValidDate = sale.QuoteValidDate,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        NouDocStatusID = sale.NouDocStatusID,
                        Remarks = sale.Remarks,
                        TotalExVAT = sale.TotalExVAT,
                        VAT = sale.VAT,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now
                    };

                    if (sale.Customer != null)
                    {
                        // add the customer snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Customer.BPMasterID,
                            BPMaster_Name = sale.Customer.Name,
                            BPMaster_Code = sale.Customer.Code,
                            BPMaster_UpLift = sale.Customer.Uplift,
                            BPMaster_CreditLimit = sale.Customer.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Customer.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        quote.BPMasterSnapshotID_Supplier = snapshot.BPMasterSnapshotID;
                        quote.BPMasterID_Customer = snapshot.BPMasterID;
                    }

                    if (sale.Haulier != null)
                    {
                        // add the haulier snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Haulier.BPMasterID,
                            BPMaster_Name = sale.Haulier.Name,
                            BPMaster_Code = sale.Haulier.Code,
                            BPMaster_UpLift = sale.Haulier.Uplift,
                            BPMaster_CreditLimit = sale.Haulier.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Haulier.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        quote.BPMasterSnapshotID_Haulier = snapshot.BPMasterSnapshotID;
                        quote.BPMasterID_Haulier = snapshot.BPMasterID;
                    }

                    if (sale.DeliveryAddress != null)
                    {
                        // add the address snapshot
                        var deliverySnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress.Details.AddressLine1,
                            AddressLine2 = sale.DeliveryAddress.Details.AddressLine2,
                            AddressLine3 = sale.DeliveryAddress.Details.AddressLine3,
                            AddressLine4 = sale.DeliveryAddress.Details.AddressLine4,
                            PostCode = sale.DeliveryAddress.Details.PostCode,
                            Shipping = sale.DeliveryAddress.Details.Shipping,
                            Billing = sale.DeliveryAddress.Details.Billing,
                            PlantID = sale.DeliveryAddress.Details.PlantID
                        };

                        entities.BPAddressSnapshots.Add(deliverySnapShot);
                        entities.SaveChanges();
                        quote.BPAddressSnapshotID_Delivery = deliverySnapShot.BPAddressSnapshotID;
                        quote.BPAddressID_Delivery = sale.DeliveryAddress.Details.BPAddressID;
                    }

                    if (sale.InvoiceAddress != null)
                    {
                        // add the address snapshot
                        var invoiceSnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.InvoiceAddress.Details.AddressLine1,
                            AddressLine2 = sale.InvoiceAddress.Details.AddressLine2,
                            AddressLine3 = sale.InvoiceAddress.Details.AddressLine3,
                            AddressLine4 = sale.InvoiceAddress.Details.AddressLine4,
                            PostCode = sale.InvoiceAddress.Details.PostCode,
                            Shipping = sale.InvoiceAddress.Details.Shipping,
                            Billing = sale.InvoiceAddress.Details.Billing,
                            PlantID = sale.InvoiceAddress.Details.PlantID
                        };

                        entities.BPAddressSnapshots.Add(invoiceSnapShot);
                        entities.SaveChanges();
                        quote.BPAddressSnapshotID_Invoice = invoiceSnapShot.BPAddressSnapshotID;
                        quote.BPAddressID_Invoice = sale.InvoiceAddress.Details.BPAddressID;
                    }

                    entities.APQuotes.Add(quote);
                    entities.SaveChanges();

                    // add the attachments
                    foreach (var attachment in sale.Attachments)
                    {
                        var quoteAttachment = new APQuoteAttachment
                        {
                            APQuoteID = quote.APQuoteID,
                            AttachmentDate = attachment.AttachmentDate,
                            FilePath = attachment.FilePath,
                            FileName = attachment.FileName,
                            File = attachment.File
                        };

                        entities.APQuoteAttachments.Add(quoteAttachment);
                    }

                    // save the details
                    var saleID = quote.APQuoteID;
                    var localQouteDetails = new List<APQuoteDetail>();

                    foreach (var detail in sale.SaleDetails)
                    {
                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = detail.InventoryItem.Master.INMasterID,
                            Code = detail.InventoryItem.Master.Code,
                            Name = detail.InventoryItem.Master.Name,
                            MaxWeight = detail.InventoryItem.Master.MaxWeight,
                            MinWeight = detail.InventoryItem.Master.MinWeight,
                            NominalWeight = detail.InventoryItem.Master.NominalWeight,
                            TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        var quoteDetail = new APQuoteDetail
                        {
                            APQuoteID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            QuantityDelivered = detail.QuantityDelivered,
                            WeightDelivered = detail.WeightDelivered,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage,
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                            VAT = detail.VAT,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            Notes = detail.Notes,
                            PriceListID = detail.PriceListID == 0 ? (int?) null : detail.PriceListID,
                            INMasterID = detail.INMasterID,
                            INMasterSnapshotID = snapshot.INMasterSnapshotID
                        };

                        localQouteDetails.Add(quoteDetail);
                    }

                    entities.APQuoteDetails.AddRange(localQouteDetails);
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "New quote successfully created");
                    return quote.APQuoteID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates an existing quote (quote and details).
        /// </summary>
        /// <param name="sale">The purchase order quote details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPQuote(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbQuote = entities.APQuotes.FirstOrDefault(x => x.APQuoteID == sale.SaleID && x.Deleted == null);

                    if (dbQuote != null)
                    {
                        dbQuote.BPMasterID_Customer = sale.Customer.BPMasterID;
                        dbQuote.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                        dbQuote.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbQuote.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                        dbQuote.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                        dbQuote.QuoteValidDate = sale.QuoteValidDate;
                        dbQuote.DocumentDate = sale.DocumentDate;
                        dbQuote.CreationDate = sale.CreationDate;
                        dbQuote.NouDocStatusID = sale.NouDocStatusID;
                        dbQuote.Remarks = sale.Remarks;
                        dbQuote.TotalExVAT = sale.TotalExVAT;
                        dbQuote.DiscountPercentage = sale.DiscountPercentage;
                        dbQuote.VAT = sale.VAT;
                        dbQuote.DiscountIncVAT = sale.DiscountIncVAT;
                        dbQuote.SubTotalExVAT = sale.SubTotalExVAT;
                        dbQuote.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbQuote.UserMasterID = sale.SalesEmployeeID;
                        dbQuote.Deleted = sale.Deleted;
                        dbQuote.DeviceID = NouvemGlobal.DeviceId;
                        dbQuote.EditDate = DateTime.Now;

                        foreach (var attachment in sale.Attachments)
                        {
                            var dbAttachment =
                                    entities.APQuoteAttachments.FirstOrDefault(
                                        x => x.APQuoteAttachmentID == attachment.SaleAttachmentID);

                            if (dbAttachment != null)
                            {
                                // updating an existing attachment
                                if (attachment.Deleted != null)
                                {
                                    entities.APQuoteAttachments.Remove(dbAttachment);
                                }
                                else
                                {
                                    dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                    dbAttachment.FileName = attachment.FileName;
                                    dbAttachment.File = attachment.File;
                                }
                            }
                            else
                            {
                                // adding a new attachment
                                var localAttachment = new APQuoteAttachment
                                {
                                    AttachmentDate = attachment.AttachmentDate,
                                    APQuoteID = sale.SaleID,
                                    FileName = attachment.FileName,
                                    File = attachment.File
                                };

                                entities.APQuoteAttachments.Add(localAttachment);
                            }
                        }

                        var localQouteDetails = new List<APQuoteDetail>();

                        // update the details
                        foreach (var detail in sale.SaleDetails)
                        {
                            var dbQuoteDetail =
                                entities.APQuoteDetails.FirstOrDefault(
                                    x => x.APQuoteDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbQuoteDetail != null)
                            {
                                // existing sale item
                                dbQuoteDetail.QuantityOrdered = detail.QuantityOrdered;
                                dbQuoteDetail.QuantityDelivered = detail.QuantityDelivered;
                                dbQuoteDetail.WeightDelivered = detail.WeightDelivered;
                                dbQuoteDetail.WeightOrdered = detail.WeightOrdered;
                                dbQuoteDetail.UnitPrice = detail.UnitPrice;
                                dbQuoteDetail.DiscountPercentage = detail.DiscountPercentage;
                                dbQuoteDetail.DiscountAmount = detail.DiscountAmount;
                                dbQuoteDetail.DiscountPrice = detail.DiscountPrice;
                                dbQuoteDetail.LineDiscountAmount = detail.LineDiscountAmount;
                                dbQuoteDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                                dbQuoteDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                                dbQuoteDetail.VAT = detail.VAT;
                                dbQuoteDetail.Notes = detail.Notes;
                                dbQuoteDetail.VATCodeID = detail.VatCodeID;
                                dbQuoteDetail.TotalExclVAT = detail.TotalExVAT;
                                dbQuoteDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbQuoteDetail.PriceListID = detail.PriceListID == 0 ? (int?) null : detail.PriceListID;
                                dbQuoteDetail.INMasterID = detail.INMasterID;
                                dbQuoteDetail.Deleted = detail.Deleted;
                            }
                            else
                            {
                                // new sale item to add to the order

                                // Add to the master snapshot table first
                                var snapshot = new INMasterSnapshot
                                {
                                    INMasterID = detail.InventoryItem.Master.INMasterID,
                                    Code = detail.InventoryItem.Master.Code,
                                    Name = detail.InventoryItem.Master.Name,
                                    MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                    MinWeight = detail.InventoryItem.Master.MinWeight,
                                    NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                    TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                                };

                                entities.INMasterSnapshots.Add(snapshot);

                                var quoteDetail = new APQuoteDetail
                                {
                                    APQuoteID = sale.SaleID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    QuantityDelivered = detail.QuantityDelivered,
                                    WeightDelivered = detail.WeightDelivered,
                                    WeightOrdered = detail.WeightOrdered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    Notes = detail.Notes,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                    INMasterID = detail.INMasterID,
                                    INMasterSnapshotID = snapshot.INMasterSnapshotID
                                };

                                localQouteDetails.Add(quoteDetail);
                            }
                        }

                        if (localQouteDetails.Any())
                        {
                            entities.APQuoteDetails.AddRange(localQouteDetails);
                        }
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Quote order successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes a quote detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveApQuoteDetail(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveApQuoteDetail(): Attempting to mark item:{0} as deleted", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var itemToDelete = entities.APQuoteDetails.FirstOrDefault(x => x.APQuoteDetailID == itemId);
                    if (itemToDelete != null)
                    {
                        itemToDelete.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Item deleted");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "Item could not be found");
            return false;
        }

        /// <summary>
        /// Retrieves all the quotes (and associated quote details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all quotes (and associated quote details).</returns>
        public IList<Sale> GetAllAPQuotes(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPQuotes(): Attempting to get all the quotes");
            var quotes = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.APQuotes
                              where orderStatuses.Contains(quote.NouDocStatusID)
                              && quote.Deleted == null
                              orderby quote.CreationDate
                              select new Sale
                              {
                                  SaleID = quote.APQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  SalesEmployeeID = quote.UserMasterID,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  Deleted = quote.Deleted,
                                  Attachments = (from attachment in quote.APQuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APQuoteAttachmentID,
                                                     SaleAttachmentID = attachment.APQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.APQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APQuoteDetailID,
                                                     SaleID = detail.APQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        public IList<Sale> GetAPQuotes(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPQuotes(): Attempting to get the quotes for customer id:{0}", customerId));
            var quotes = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.APQuotes
                              where quote.BPMasterID_Customer == customerId && quote.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID && quote.Deleted == null
                              select new Sale
                              {
                                  SaleID = quote.APQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  SalesEmployeeID = quote.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  Deleted = quote.Deleted,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  Attachments = (from attachment in quote.APQuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APQuoteAttachmentID,
                                                     SaleAttachmentID = attachment.APQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.APQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APQuoteDetailID,
                                                     SaleID = detail.APQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                 
                                                     INMasterID = detail.INMasterID,
                                                     INMaster = detail.INMaster,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        public IList<Sale> GetRecentAPQuotes(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetRecentAPQuotes(): Attempting to get the quotes for customer id:{0}", customerId));
            var quotes = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.APQuotes
                              where quote.BPMasterID_Customer == customerId
                              && quote.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID 
                              && quote.Deleted == null
                              orderby quote.APQuoteID descending 
                              select new Sale
                              {
                                  SaleID = quote.APQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  SalesEmployeeID = quote.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  Deleted = quote.Deleted,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  Attachments = (from attachment in quote.APQuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APQuoteAttachmentID,
                                                     SaleAttachmentID = attachment.APQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.APQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APQuoteDetailID,
                                                     SaleID = detail.APQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     INMaster = detail.INMaster,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).Take(10).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer id.
        /// </summary>
        /// <param name="quoteId">The quote id.</param>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        public Sale GetAPQuoteByID(int quoteId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPQuoteByID(): Attempting to get the quote for id:{0}", quoteId));
            Sale apQuote = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var quote = entities.APQuotes.FirstOrDefault(x => x.APQuoteID == quoteId);
                    if (quote == null)
                    {
                        this.Log.LogError(this.GetType(), "Quote not found");
                        return null;
                    }
                    apQuote = new Sale
                    {
                        SaleID = quote.APQuoteID,
                        DocumentNumberingID = quote.DocumentNumberingID,
                        Number = quote.Number,
                        BPContactID = quote.BPContactID,
                        QuoteValidDate = quote.QuoteValidDate,
                        DocumentDate = quote.DocumentDate,
                        CreationDate = quote.CreationDate,
                        Printed = quote.Printed,
                        NouDocStatusID = quote.NouDocStatusID,
                        NouDocStatus = quote.NouDocStatu.Value,
                        Remarks = quote.Remarks,
                        TotalExVAT = quote.TotalExVAT,
                        VAT = quote.VAT,
                        SubTotalExVAT = quote.SubTotalExVAT,
                        DiscountIncVAT = quote.DiscountIncVAT,
                        GrandTotalIncVAT = quote.GrandTotalIncVAT,
                        DiscountPercentage = quote.DiscountPercentage,
                        SalesEmployeeID = quote.UserMasterID,
                        DeviceId = quote.DeviceID,
                        EditDate = quote.EditDate,
                        DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                        InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                        MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                        BPHaulier = quote.BPMasterHaulier,
                        BPCustomer = quote.BPMasterCustomer,
                        Deleted = quote.Deleted,
                        Attachments = (from attachment in quote.APQuoteAttachments
                                       where attachment.Deleted == null
                            select new Attachment
                            {
                                AttachmentID = attachment.APQuoteAttachmentID,
                                SaleAttachmentID = attachment.APQuoteID,
                                AttachmentDate = attachment.AttachmentDate,
                                FilePath = attachment.FilePath,
                                FileName = attachment.FileName,
                                File = attachment.File,
                                Deleted = attachment.Deleted
                            }).ToList(),
                        SaleDetails = (from detail in quote.APQuoteDetails
                                       where detail.Deleted == null
                            select new SaleDetail
                            {
                                LoadingSale = true,
                                SaleDetailID = detail.APQuoteDetailID,
                                SaleID = detail.APQuoteID,
                                QuantityOrdered = detail.QuantityOrdered,
                                WeightOrdered = detail.WeightOrdered,
                                QuantityDelivered = detail.QuantityDelivered,
                                WeightDelivered = detail.WeightDelivered,
                                UnitPrice = detail.UnitPrice,
                                DiscountPercentage = detail.DiscountPercentage,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountPercentage = detail.LineDiscountPercentage,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                VAT = detail.VAT,
                                TotalExVAT = detail.TotalExclVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                VatCodeID = detail.VATCodeID,
                                Notes = detail.Notes,
                                INMasterID = detail.INMasterID,
                                PriceListID = detail.PriceListID ?? 0,
                                Deleted = detail.Deleted
                            }).ToList(),

                    };
                }

                this.Log.LogDebug(this.GetType(), "Quote found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apQuote;
        }

        /// <summary>
        /// Retrieves the last local edited quote.
        /// </summary>
        /// <returns>A quote (and associated quote details).</returns>
        public Sale GetAPQuoteByLastEdit()
        {
            Sale apQuote = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var quote = entities.APQuotes
                    .Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                    .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (quote == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Quote not found");
                        return null;
                    }

                    apQuote = new Sale
                    {
                        SaleID = quote.APQuoteID,
                        DocumentNumberingID = quote.DocumentNumberingID,
                        Number = quote.Number,
                        BPContactID = quote.BPContactID,
                        QuoteValidDate = quote.QuoteValidDate,
                        DocumentDate = quote.DocumentDate,
                        CreationDate = quote.CreationDate,
                        Printed = quote.Printed,
                        NouDocStatusID = quote.NouDocStatusID,
                        NouDocStatus = quote.NouDocStatu.Value,
                        Remarks = quote.Remarks,
                        TotalExVAT = quote.TotalExVAT,
                        VAT = quote.VAT,
                        SubTotalExVAT = quote.SubTotalExVAT,
                        DiscountIncVAT = quote.DiscountIncVAT,
                        GrandTotalIncVAT = quote.GrandTotalIncVAT,
                        DiscountPercentage = quote.DiscountPercentage,
                        SalesEmployeeID = quote.UserMasterID,
                        DeviceId = quote.DeviceID,
                        EditDate = quote.EditDate,
                        DeliveryAddress = new BusinessPartnerAddress {Details = quote.BPAddressDelivery},
                        InvoiceAddress = new BusinessPartnerAddress {Details = quote.BPAddressInvoice},
                        MainContact = new BusinessPartnerContact {Details = quote.BPContact},
                        BPHaulier = quote.BPMasterHaulier,
                        BPCustomer = quote.BPMasterCustomer,
                        Deleted = quote.Deleted,
                        Attachments = (from attachment in quote.APQuoteAttachments
                                       where attachment.Deleted == null
                            select new Attachment
                            {
                                AttachmentID = attachment.APQuoteAttachmentID,
                                SaleAttachmentID = attachment.APQuoteID,
                                AttachmentDate = attachment.AttachmentDate,
                                FilePath = attachment.FilePath,
                                FileName = attachment.FileName,
                                File = attachment.File,
                                Deleted = attachment.Deleted
                            }).ToList(),
                        SaleDetails = (from detail in quote.APQuoteDetails
                                       where detail.Deleted == null
                            select new SaleDetail
                            {
                                LoadingSale = true,
                                SaleDetailID = detail.APQuoteDetailID,
                                SaleID = detail.APQuoteID,
                                QuantityOrdered = detail.QuantityOrdered,
                                WeightOrdered = detail.WeightOrdered,
                                QuantityDelivered = detail.QuantityDelivered,
                                WeightDelivered = detail.WeightDelivered,
                                UnitPrice = detail.UnitPrice,
                                DiscountPercentage = detail.DiscountPercentage,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountPercentage = detail.LineDiscountPercentage,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                VAT = detail.VAT,
                                TotalExVAT = detail.TotalExclVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                VatCodeID = detail.VATCodeID,
                                INMasterID = detail.INMasterID,
                                PriceListID = detail.PriceListID ?? 0,
                                Notes = detail.Notes,
                                Deleted = detail.Deleted
                            }).ToList(),

                    };
                }

                this.Log.LogDebug(this.GetType(), "Quote found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apQuote;
        }
     
        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetAPQuotesByPartner(int partnerId, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPQuotesByName(): Attempting to get the quotes for id:{0}", partnerId));
            var quotes = new List<Sale>();
         
            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.APQuotes
                              where quote.BPMasterID_Customer == partnerId
                              && orderStatuses.Contains(quote.NouDocStatusID)
                              && quote.Deleted == null
                              select new Sale
                              {
                                  SaleID = quote.APQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  SalesEmployeeID = quote.UserMasterID,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  Deleted = quote.Deleted,
                                  Attachments = (from attachment in quote.APQuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APQuoteAttachmentID,
                                                     SaleAttachmentID = attachment.APQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.APQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APQuoteDetailID,
                                                     SaleID = detail.APQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetAPQuotesByName(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPQuotesByName(): Attempting to get the quotes for search term:{0}", searchTerm));
            var quotes = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.APQuotes
                              where quote.BPMasterCustomer.Name.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(quote.NouDocStatusID) 
                              && quote.Deleted == null
                              select new Sale
                              {
                                  SaleID = quote.APQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  SalesEmployeeID = quote.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  Deleted = quote.Deleted,
                                  Attachments = (from attachment in quote.APQuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APQuoteAttachmentID,
                                                     SaleAttachmentID = attachment.APQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.APQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APQuoteDetailID,
                                                     SaleID = detail.APQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        public IList<Sale> GetAPQuotesByCode(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPQuotesByCode(): Attempting to get the quotes for search term:{0}", searchTerm));
            var quotes = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from quote in entities.APQuotes
                              where quote.BPMasterCustomer.Code.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(quote.NouDocStatusID) 
                              && quote.Deleted == null
                              select new Sale
                              {
                                  SaleID = quote.APQuoteID,
                                  DocumentNumberingID = quote.DocumentNumberingID,
                                  Number = quote.Number,
                                  BPContactID = quote.BPContactID,
                                  QuoteValidDate = quote.QuoteValidDate,
                                  DocumentDate = quote.DocumentDate,
                                  CreationDate = quote.CreationDate,
                                  Printed = quote.Printed,
                                  NouDocStatusID = quote.NouDocStatusID,
                                  NouDocStatus = quote.NouDocStatu.Value,
                                  Remarks = quote.Remarks,
                                  TotalExVAT = quote.TotalExVAT,
                                  VAT = quote.VAT,
                                  SubTotalExVAT = quote.SubTotalExVAT,
                                  DiscountIncVAT = quote.DiscountIncVAT,
                                  GrandTotalIncVAT = quote.GrandTotalIncVAT,
                                  DiscountPercentage = quote.DiscountPercentage,
                                  SalesEmployeeID = quote.UserMasterID,
                                  DeviceId = quote.DeviceID,
                                  EditDate = quote.EditDate,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = quote.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = quote.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = quote.BPContact },
                                  BPHaulier = quote.BPMasterHaulier,
                                  BPCustomer = quote.BPMasterCustomer,
                                  Deleted = quote.Deleted,
                                  Attachments = (from attachment in quote.APQuoteAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APQuoteAttachmentID,
                                                     SaleAttachmentID = attachment.APQuoteID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in quote.APQuoteDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APQuoteDetailID,
                                                     SaleID = detail.APQuoteID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityDelivered = detail.QuantityDelivered,
                                                     WeightDelivered = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} quotes successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        #endregion

        #region order

        /// <summary> Adds a new purchase order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id, indicating a successful order add, or not.</returns>
        public int AddAPOrder(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = new APOrder
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        QuoteValidDate = sale.QuoteValidDate,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        DeliveryDate = sale.DeliveryDate,
                        DeliveryTime = sale.DeliveryTime,
                        NouDocStatusID = sale.NouDocStatusID,
                        PopUpNote = sale.PopUpNote,
                        IntakeNote = sale.DocketNote,
                        InvoiceNote = sale.InvoiceNote,
                        Remarks = sale.Remarks,
                        TotalExVAT = sale.TotalExVAT,
                        VAT = sale.VAT,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        DeviceID = NouvemGlobal.DeviceId,
                        UserID_CreatedBy = NouvemGlobal.UserId,
                        EditDate = DateTime.Now
                    };

                    if (sale.Customer != null)
                    {
                        // add the customer snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Customer.BPMasterID,
                            BPMaster_Name = sale.Customer.Name,
                            BPMaster_Code = sale.Customer.Code,
                            BPMaster_UpLift = sale.Customer.Uplift,
                            BPMaster_CreditLimit = sale.Customer.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Customer.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        order.BPMasterSnapshotID_Supplier = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Supplier = snapshot.BPMasterID;
                    }

                    if (sale.Haulier != null)
                    {
                        // add the haulier snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Haulier.BPMasterID,
                            BPMaster_Name = sale.Haulier.Name,
                            BPMaster_Code = sale.Haulier.Code,
                            BPMaster_UpLift = sale.Haulier.Uplift,
                            BPMaster_CreditLimit = sale.Haulier.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Haulier.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        order.BPMasterSnapshotID_Haulier = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Haulier = snapshot.BPMasterID;
                    }

                    if (sale.DeliveryAddress != null)
                    {
                        // add the address snapshot
                        var deliverySnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress.Details.AddressLine1,
                            AddressLine2 = sale.DeliveryAddress.Details.AddressLine2,
                            AddressLine3 = sale.DeliveryAddress.Details.AddressLine3,
                            AddressLine4 = sale.DeliveryAddress.Details.AddressLine4,
                            PostCode = sale.DeliveryAddress.Details.PostCode,
                            Shipping = sale.DeliveryAddress.Details.Shipping,
                            Billing = sale.DeliveryAddress.Details.Billing,
                            PlantID = sale.DeliveryAddress.Details.PlantID
                        };

                        entities.BPAddressSnapshots.Add(deliverySnapShot);
                        entities.SaveChanges();
                        order.BPAddressSnapshotID_Delivery = deliverySnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Delivery = sale.DeliveryAddress.Details.BPAddressID;
                    }

                    if (sale.InvoiceAddress != null)
                    {
                        // add the address snapshot
                        var invoiceSnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.InvoiceAddress.Details.AddressLine1,
                            AddressLine2 = sale.InvoiceAddress.Details.AddressLine2,
                            AddressLine3 = sale.InvoiceAddress.Details.AddressLine3,
                            AddressLine4 = sale.InvoiceAddress.Details.AddressLine4,
                            PostCode = sale.InvoiceAddress.Details.PostCode,
                            Shipping = sale.InvoiceAddress.Details.Shipping,
                            Billing = sale.InvoiceAddress.Details.Billing,
                            PlantID = sale.InvoiceAddress.Details.PlantID
                        };

                        entities.BPAddressSnapshots.Add(invoiceSnapShot);
                        entities.SaveChanges();
                        order.BPAddressSnapshotID_Invoice = invoiceSnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Invoice = sale.InvoiceAddress.Details.BPAddressID;
                    }

                    entities.APOrders.Add(order);
                    entities.SaveChanges();

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", order.BaseDocumentReferenceID));
                        var baseDoc = entities.APQuotes.FirstOrDefault(x => x.APQuoteID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            this.Log.LogDebug(this.GetType(), string.Format("Base quote id:{0} marked as closed", order.BaseDocumentReferenceID));
                        }
                    }

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", order.BaseDocumentReferenceID));
                        var baseDoc = entities.AROrders.FirstOrDefault(x => x.AROrderID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            this.Log.LogDebug(this.GetType(), string.Format("Base quote id:{0} marked as closed", order.BaseDocumentReferenceID));
                        }
                    }

                    // add the attachments
                    foreach (var attachment in sale.Attachments)
                    {
                        var quoteAttachment = new APQuoteAttachment
                        {
                            APQuoteID = order.APOrderID,
                            AttachmentDate = attachment.AttachmentDate,
                            FilePath = attachment.FilePath,
                            FileName = attachment.FileName,
                            File = attachment.File
                        };

                        entities.APQuoteAttachments.Add(quoteAttachment);
                    }

                    // save the details
                    var saleID = order.APOrderID;
                    var localOrderDetails = new List<APOrderDetail>();

                    foreach (var detail in sale.SaleDetails)
                    {
                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = detail.InventoryItem.Master.INMasterID,
                            Code = detail.InventoryItem.Master.Code,
                            Name = detail.InventoryItem.Master.Name,
                            MaxWeight = detail.InventoryItem.Master.MaxWeight,
                            MinWeight = detail.InventoryItem.Master.MinWeight,
                            NominalWeight = detail.InventoryItem.Master.NominalWeight,
                            TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();

                        var orderDetail = new APOrderDetail
                        {
                            APOrderID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage,
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                            VAT = detail.VAT,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            Notes = detail.Notes,
                            INMasterID = detail.INMasterID,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterSnapshotID = snapshot.INMasterSnapshotID
                        };

                        localOrderDetails.Add(orderDetail);
                    }

                    entities.APOrderDetails.AddRange(localOrderDetails);

                    // mark the associated quote(if any) as complete
                    var dbQuote = entities.APQuotes.FirstOrDefault(x => x.APQuoteID == sale.SaleID);
                    if (dbQuote != null)
                    {
                        dbQuote.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "New order successfully created");
                    return order.APOrderID;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates an existing order (order and details).
        /// </summary>
        /// <param name="sale">The purchase order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPOrder(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.APOrders.FirstOrDefault(x => x.APOrderID == sale.SaleID && x.Deleted == null);

                    if (dbOrder != null)
                    {
                        dbOrder.BPMasterID_Supplier = sale.Customer.BPMasterID;
                        dbOrder.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                        dbOrder.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                        dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                        dbOrder.QuoteValidDate = sale.QuoteValidDate;
                        dbOrder.DocumentDate = sale.DocumentDate;
                        dbOrder.CreationDate = sale.CreationDate;
                        dbOrder.DeliveryDate = sale.DeliveryDate;
                        dbOrder.NouDocStatusID = sale.NouDocStatusID;
                        dbOrder.Remarks = sale.Remarks;
                        dbOrder.PopUpNote = sale.PopUpNote;
                        dbOrder.IntakeNote = sale.DocketNote;
                        dbOrder.InvoiceNote = sale.InvoiceNote;
                        dbOrder.DeliveryTime = sale.DeliveryTime;
                        dbOrder.TotalExVAT = sale.TotalExVAT;
                        dbOrder.DiscountPercentage = sale.DiscountPercentage;
                        dbOrder.VAT = sale.VAT;
                        dbOrder.DiscountIncVAT = sale.DiscountIncVAT;
                        dbOrder.SubTotalExVAT = sale.SubTotalExVAT;
                        dbOrder.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbOrder.UserMasterID = sale.SalesEmployeeID;
                        dbOrder.Deleted = sale.Deleted;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Now;

                        foreach (var attachment in sale.Attachments)
                        {
                            var dbAttachment =
                                    entities.APOrderAttachments.FirstOrDefault(
                                        x => x.APOrderAttachmentID == attachment.SaleAttachmentID);

                            if (dbAttachment != null)
                            {
                                // updating an existing attachment
                                if (attachment.Deleted != null)
                                {
                                    entities.APOrderAttachments.Remove(dbAttachment);
                                }
                                else
                                {
                                    dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                    dbAttachment.FileName = attachment.FileName;
                                    dbAttachment.File = attachment.File;
                                }
                            }
                            else
                            {
                                // adding a new attachment
                                var localAttachment = new APOrderAttachment
                                {
                                    AttachmentDate = attachment.AttachmentDate,
                                    APOrderID = sale.SaleID,
                                    FileName = attachment.FileName,
                                    File = attachment.File
                                };

                                entities.APOrderAttachments.Add(localAttachment);
                            }
                        }

                        var localOrderDetails = new List<APOrderDetail>();

                        // update the details
                        foreach (var detail in sale.SaleDetails)
                        {
                            var dbOrderDetail =
                                entities.APOrderDetails.FirstOrDefault(
                                    x => x.APOrderDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbOrderDetail != null)
                            {
                                // existing sale item
                                dbOrderDetail.QuantityOrdered = detail.QuantityOrdered;
                                dbOrderDetail.WeightOrdered = detail.WeightOrdered;
                                dbOrderDetail.UnitPrice = detail.UnitPrice;
                                dbOrderDetail.DiscountPercentage = detail.DiscountPercentage;
                                dbOrderDetail.DiscountAmount = detail.DiscountAmount;
                                dbOrderDetail.DiscountPrice = detail.DiscountPrice;
                                dbOrderDetail.LineDiscountAmount = detail.LineDiscountAmount;
                                dbOrderDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                                dbOrderDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                                dbOrderDetail.VAT = detail.VAT;
                                dbOrderDetail.VATCodeID = detail.VatCodeID;
                                dbOrderDetail.TotalExclVAT = detail.TotalExVAT;
                                dbOrderDetail.Notes = detail.Notes;
                                dbOrderDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbOrderDetail.INMasterID = detail.INMasterID;
                                dbOrderDetail.PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID;
                                dbOrderDetail.Deleted = detail.Deleted;
                            }
                            else
                            {
                                // new sale item to add to the order

                                // Add to the master snapshot table first
                                var snapshot = new INMasterSnapshot
                                {
                                    INMasterID = detail.InventoryItem.Master.INMasterID,
                                    Code = detail.InventoryItem.Master.Code,
                                    Name = detail.InventoryItem.Master.Name,
                                    MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                    MinWeight = detail.InventoryItem.Master.MinWeight,
                                    NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                    TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                                };

                                entities.INMasterSnapshots.Add(snapshot);

                                var orderDetail = new APOrderDetail
                                {
                                    APOrderID = sale.SaleID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    WeightOrdered = detail.WeightOrdered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    Notes = detail.Notes,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                    INMasterID = detail.INMasterID,
                                    INMasterSnapshotID = snapshot.INMasterSnapshotID
                                };

                                localOrderDetails.Add(orderDetail);
                            }
                        }

                        if (localOrderDetails.Any())
                        {
                            entities.APOrderDetails.AddRange(localOrderDetails);
                        }
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Order order successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentPurchaseOrders(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the recent orders for customer id:{0}", customerId));
            var orders = new List<Sale>();

            try
            {
                IList<App_GetRecentPurchaseOrders_Result> dbOrders;
                using (var entities = new NouvemEntities())
                {
                    dbOrders = entities.App_GetRecentPurchaseOrders(customerId).ToList();
                }

                var groupedOrders = dbOrders.OrderByDescending(x => x.APOrderID).GroupBy(x => x.APOrderID).ToList();
                foreach (var localOrders in groupedOrders)
                {
                    var arOrder = new Sale
                    {
                        SaleID = localOrders.First().APOrderID,
                        Number = localOrders.First().Number,
                        CreationDate = localOrders.First().CreationDate,                  
                        DeliveryDate = localOrders.First().DeliveryDate,
                        SaleDetails = new List<SaleDetail>()
                    };

                    foreach (var detail in localOrders)
                    {
                        arOrder.SaleDetails.Add(new SaleDetail
                        {
                            LoadingSale = true,
                            SaleID = detail.APOrderID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            INMasterID = detail.INMasterID,
                            PriceListID = detail.PriceListID ?? 0
                        });
                    }

                    orders.Add(arOrder);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            foreach (var receipt in orders)
            {
                receipt.Attachments = new List<Attachment>();
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentPurchaseReceiptOrders(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to get the recent orders for customer id:{0}", customerId));
            var orders = new List<Sale>();

            try
            {
                IList<App_GetRecentPurchaseReceiptOrders_Result> dbOrders;
                using (var entities = new NouvemEntities())
                {
                    dbOrders = entities.App_GetRecentPurchaseReceiptOrders(customerId).ToList();
                }

                var groupedOrders = dbOrders.OrderByDescending(x => x.APGoodsReceiptID).GroupBy(x => x.APGoodsReceiptID).ToList();
                foreach (var localOrders in groupedOrders)
                {
                    var arOrder = new Sale
                    {
                        SaleID = localOrders.First().APGoodsReceiptID,
                        Number = localOrders.First().Number,
                        CreationDate = localOrders.First().CreationDate,
                        DeliveryDate = localOrders.First().DeliveryDate,
                        SaleDetails = new List<SaleDetail>()
                    };

                    foreach (var detail in localOrders)
                    {
                        arOrder.SaleDetails.Add(new SaleDetail
                        {
                            LoadingSale = true,
                            SaleID = detail.APGoodsReceiptID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            INMasterID = detail.INMasterID,
                            PriceListID = detail.PriceListID ?? 0
                        });
                    }

                    orders.Add(arOrder);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            foreach (var receipt in orders)
            {
                receipt.Attachments = new List<Attachment>();
            }

            return orders;
        }

        /// <summary>
        /// Copies a purchase order.
        /// </summary>
        /// <param name="orderNo">The order no to copy from.</param>
        /// <param name="userId">The current user.</param>
        /// <param name="deviceId">The current device.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        public ProductStockData CopyPurchaseOrder(int orderNo, int productid, int userId, int deviceId, DateTime scheduledDate)
        {
            var data = new ProductStockData { OrderType = Strings.Intake };
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var id = new System.Data.Entity.Core.Objects.ObjectParameter("ID", typeof(int));
                    var number = new System.Data.Entity.Core.Objects.ObjectParameter("Number", typeof(int));
                    var wgt = new System.Data.Entity.Core.Objects.ObjectParameter("Wgt", typeof(decimal));
                    var qty = new System.Data.Entity.Core.Objects.ObjectParameter("Qty", typeof(decimal));
                    entities.App_CopyPurchaseOrder(orderNo, userId, deviceId, scheduledDate, productid,id, number, wgt, qty);
                    data.OrderId = id.Value != null ? id.Value.ToInt() : 0;
                    data.OrderNo = number.Value != null ? number.Value.ToInt() : 0;
                    data.AllocatedWgt = wgt.Value != null ? wgt.Value.ToDecimal() : 0;
                    data.AllocatedQty = qty.Value != null ? qty.Value.ToDecimal() : 0;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                data.Error = ex.Message;
            }

            return data;
        }

        /// <summary>
        /// Ckecks if a product is on a purchase order.
        /// </summary>
        /// <param name="inmasterid"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public bool IsProductOnPurchaseOrder(int inmasterid, int orderId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.APOrderDetails.Any(x =>
                        x.INMasterID == inmasterid && x.APOrderID == orderId && x.Deleted == null);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the intake docket id relating to the input purchase order id.
        /// </summary>
        /// <param name="id">The input order id.</param>
        /// <returns>The docket id relating to the input  order id.</returns>
        public int GetIntakeIdForOrder(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var intake = entities.APGoodsReceipts.FirstOrDefault(x => x.BaseDocumentReferenceID == id);
                    if (intake != null)
                    {
                        return intake.APGoodsReceiptID;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Updates an existing intake docket (docket and details) when a purchaseorder has been ammended.
        /// </summary>
        /// <param name="sale">The order data.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateIntakeFromOrder(Sale sale)
        {
            var intakeID = sale.ParentID.ToInt();

            this.Log.LogDebug(this.GetType(), string.Format("UpdateIntakeFromOrder(): Updating intake docket:{0}", intakeID));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == intakeID);

                    if (dbOrder != null)
                    {
                        dbOrder.NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID;
                        dbOrder.TotalExVAT = 0;
                        dbOrder.DiscountPercentage = sale.DiscountPercentage;
                        dbOrder.VAT = sale.VAT;
                        dbOrder.DiscountIncVAT = sale.DiscountIncVAT;
                        dbOrder.SubTotalExVAT = 0;
                        dbOrder.PopUpNote = sale.PopUpNote;
                        dbOrder.IntakeNote = sale.DocketNote;
                        dbOrder.InvoiceNote = sale.InvoiceNote;
                        dbOrder.GrandTotalIncVAT = 0;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Now;
                        dbOrder.BPMasterID_Supplier = sale.Customer != null
                                ? sale.Customer.BPMasterID
                                : sale.BPCustomer.BPMasterID;
                        dbOrder.BPContactID = sale.MainContact != null
                            ? (int?)sale.MainContact.Details.BPContactID
                            : null;
                        dbOrder.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null
                            ? (int?)sale.DeliveryAddress.Details.BPAddressID
                            : null;
                        dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null
                            ? (int?)sale.InvoiceAddress.Details.BPAddressID
                            : null;
                        dbOrder.DocumentDate = sale.DocumentDate;
                        dbOrder.Remarks = sale.Remarks;
                        dbOrder.DeliveryDate = sale.DeliveryDate;
                        dbOrder.DeliveryTime = sale.DeliveryTime;
                        dbOrder.UserMasterID = sale.SalesEmployeeID;
                        dbOrder.Deleted = sale.Deleted;

                        var dbDetails = dbOrder.APGoodsReceiptDetails.Where(x => x.Deleted == null);
                        foreach (var detail in dbDetails)
                        {
                            var dbDetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == detail.APGoodsReceiptDetailID);
                            if (dbDetail != null)
                            {
                                dbDetail.Deleted = DateTime.Now;
                            }
                        }

                        var details = sale.SaleDetails.Where(x => x.Deleted == null);
                        foreach (var detail in details)
                        {
                            // Add to the master snapshot table first
                            var snapshot = new INMasterSnapshot
                            {
                                INMasterID = detail.InventoryItem.Master.INMasterID,
                                Code = detail.InventoryItem.Master.Code,
                                Name = detail.InventoryItem.Master.Name,
                                MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                MinWeight = detail.InventoryItem.Master.MinWeight,
                                NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                            };

                            entities.INMasterSnapshots.Add(snapshot);

                            var orderDetail = new APGoodsReceiptDetail
                            {
                                APGoodsReceiptID = dbOrder.APGoodsReceiptID,
                                QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                                WeightOrdered = detail.WeightOrdered,
                                TotalExcVAT = 0,
                                TotalIncVAT = 0,
                                QuantityReceived = detail.QuantityReceived,
                                WeightReceived = detail.WeightDelivered,
                                UnitPrice = detail.UnitPrice,
                                DiscountPercentage = detail.DiscountPercentage,
                                DiscountAmount = detail.DiscountAmount,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount ?? detail.UnitPrice,
                                VAT = detail.VAT,
                                VATCodeID = detail.VatCodeID,
                                INMasterID = detail.INMasterID,
                                PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                INMasterSnapshotID = snapshot.INMasterSnapshotID,
                                Comments = detail.Comments
                            };

                            entities.APGoodsReceiptDetails.Add(orderDetail);
                            entities.SaveChanges();
                        }

                        // if we used a base document to create this, then we mark the base document as closed.
                        if (sale.SaleID != 0)
                        {
                            var baseDoc = entities.APOrders.FirstOrDefault(x => x.APOrderID == sale.SaleID);
                            if (baseDoc != null)
                            {
                                this.Log.LogWarning(this.GetType(), string.Format("8. Updating base order. Setting status to complete:{0}", sale.SaleID));
                                baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            }
                        }

                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("UpdateIntakeFromOrder(): Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Removes an order detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveApOrderDetail(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveApOrderDetail(): Attempting to mark item:{0} as deleted", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var itemToDelete = entities.APOrderDetails.FirstOrDefault(x => x.APOrderDetailID == itemId);
                    if (itemToDelete != null)
                    {
                        itemToDelete.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Item deleted");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "Item could not be found");
            return false;
        }


        /// <summary>
        /// Retrieves all the live orders (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPOrders(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPOrders(): Attempting to get all the orders");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APOrders
                              where orderStatuses.Contains(order.NouDocStatusID)
                                    && order.Deleted == null && order.DeliveryDate >= start && order.DeliveryDate <= end
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Number = order.Number,
                                  DeliveryTime = order.DeliveryTime,
                                  BPContactID = order.BPContactID,
                                  QuoteValidDate = order.QuoteValidDate,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  DocketNote = order.IntakeNote,
                                  InvoiceNote = order.InvoiceNote,
                                  PopUpNote = order.PopUpNote,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  SaleType = ViewType.APOrder,
                                  TechnicalNotes = order.Notes,
                                  UserIDModified = order.UserMasterID,
                                  OtherReference = order.BPMasterSupplier.Name,
                                  UserIDCreation = order.UserID_CreatedBy,
                                  //RouteNumber = order.BPMasterHaulier != null && order.BPMasterHaulier.RouteMaster != null
                                  //              ? order.BPMasterHaulier.RouteMaster.RunNumber : (int?) null,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  //Attachments = (from attachment in order.APOrderAttachments
                                  //               where attachment.Deleted == null
                                  //               select new Attachment
                                  //               {
                                  //                   AttachmentID = attachment.APOrderAttachmentID,
                                  //                   SaleAttachmentID = attachment.APOrderID,
                                  //                   AttachmentDate = attachment.AttachmentDate,
                                  //                   FilePath = attachment.FilePath,
                                  //                   FileName = attachment.FileName,
                                  //                   File = attachment.File,
                                  //                   Deleted = attachment.Deleted
                                  //               }).ToList(),
                                  //SaleDetails = (from detail in order.APOrderDetails
                                  //               where detail.Deleted == null
                                  //               select new SaleDetail
                                  //               {
                                  //                   LoadingSale = true,
                                  //                   SaleDetailID = detail.APOrderDetailID,
                                  //                   SaleID = detail.APOrderID,
                                  //                   QuantityOrdered = detail.QuantityOrdered,
                                  //                   WeightOrdered = detail.WeightOrdered,
                                  //                   QuantityReceived = detail.QuantityDelivered,
                                  //                   WeightReceived = detail.WeightDelivered,
                                  //                   UnitPrice = detail.UnitPrice,
                                  //                   DiscountPercentage = detail.DiscountPercentage,
                                  //                   DiscountPrice = detail.DiscountPrice,
                                  //                   LineDiscountPercentage = detail.LineDiscountPercentage,
                                  //                   LineDiscountAmount = detail.LineDiscountAmount,
                                  //                   UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                  //                   VAT = detail.VAT,
                                  //                   TotalExVAT = detail.TotalExclVAT,
                                  //                   TotalIncVAT = detail.TotalIncVAT,
                                  //                   VatCodeID = detail.VATCodeID,

                                  //                   INMasterID = detail.INMasterID,
                                  //                   PriceListID = detail.PriceListID ?? 0,
                                  //                   NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                  //                   Deleted = detail.Deleted
                                  //               }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the live orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetAPOrders(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPOrders(): Attempting to get the orders for customer id:{0}", customerId));
            var quotes = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from order in entities.APOrders
                              where order.BPMasterID_Supplier == customerId
                                    && order.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                                    && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  QuoteValidDate = order.QuoteValidDate,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  SaleType = ViewType.APOrder,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DocketNote = order.IntakeNote,
                                  InvoiceNote = order.InvoiceNote,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APOrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APOrderAttachmentID,
                                                     SaleAttachmentID = attachment.APOrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APOrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APOrderDetailID,
                                                     SaleID = detail.APOrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityDelivered,
                                                     WeightReceived = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                 
                                                     INMaster = detail.INMaster,
                                                     OrderDate = order.DocumentDate,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the live orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentAPOrders(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetRecentAPOrders(): Attempting to get the orders for customer id:{0}", customerId));
            var quotes = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    quotes = (from order in entities.APOrders
                              where order.BPMasterID_Supplier == customerId
                                    && order.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID
                                    && order.Deleted == null
                                    orderby order.APOrderID descending 
                              select new Sale
                              {
                                  SaleID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  QuoteValidDate = order.QuoteValidDate,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  DocketNote = order.IntakeNote,
                                  InvoiceNote = order.InvoiceNote,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  SaleType = ViewType.APOrder,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APOrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APOrderAttachmentID,
                                                     SaleAttachmentID = attachment.APOrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APOrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APOrderDetailID,
                                                     SaleID = detail.APOrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityDelivered,
                                                     WeightReceived = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                             
                                                     INMaster = detail.INMaster,
                                                     OrderDate = order.DocumentDate,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).Take(ApplicationSettings.RecentOrdersAmtToTake).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", quotes.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return quotes;
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPOrdersByPartner(int partnerId, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPOrdersByName(): Attempting to get the orders for search term:{0}", partnerId));
            var orders = new List<Sale>();
         
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APOrders
                              where order.BPMasterID_Supplier == partnerId
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  QuoteValidDate = order.QuoteValidDate,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APOrder,
                                  DeliveryDate = order.DeliveryDate,
                                  Printed = order.Printed,
                                  DocketNote = order.IntakeNote,
                                  InvoiceNote = order.InvoiceNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APOrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APOrderAttachmentID,
                                                     SaleAttachmentID = attachment.APOrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APOrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APOrderDetailID,
                                                     SaleID = detail.APOrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityDelivered,
                                                     WeightReceived = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                 
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetAPOrderByID(int orderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPOrderByID(): Attempting to get the order for search id:{0}", orderId));
            Sale apOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APOrders.FirstOrDefault(x => x.APOrderID == orderId);
                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }
                    apOrder = new Sale
                    {
                        SaleID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        Number = order.Number,
                        DeliveryTime = order.DeliveryTime,
                        BPContactID = order.BPContactID,
                        QuoteValidDate = order.QuoteValidDate,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        PopUpNote = order.PopUpNote,
                        SaleType = ViewType.APOrder,
                        DocketNote = order.IntakeNote,
                        InvoiceNote = order.InvoiceNote,
                        DeliveryDate = order.DeliveryDate,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterSupplier,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.APOrderAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.APOrderAttachmentID,
                                           SaleAttachmentID = attachment.APOrderID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.APOrderDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APOrderDetailID,
                                           SaleID = detail.APOrderID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityDelivered,
                                           WeightReceived = detail.WeightDelivered,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apOrder;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input number.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An order (and associated order details) for the input number.</returns>
        public Sale GetAPOrderByNumber(int number)
        {
            Sale apOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APOrders.FirstOrDefault(x => x.Number == number);
                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }
                    apOrder = new Sale
                    {
                        SaleID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        Number = order.Number,
                        DeliveryTime = order.DeliveryTime,
                        BPContactID = order.BPContactID,
                        QuoteValidDate = order.QuoteValidDate,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        PopUpNote = order.PopUpNote,
                        SaleType = ViewType.APOrder,
                        DocketNote = order.IntakeNote,
                        InvoiceNote = order.InvoiceNote,
                        DeliveryDate = order.DeliveryDate,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterSupplier,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.APOrderAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.APOrderAttachmentID,
                                           SaleAttachmentID = attachment.APOrderID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.APOrderDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APOrderDetailID,
                                           SaleID = detail.APOrderID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityDelivered,
                                           WeightReceived = detail.WeightDelivered,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apOrder;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        public Sale GetAPOrderByLastEdit()
        {
            Sale apOrder = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APOrders
                 .Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                 .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (order == null)
                    {
                        this.Log.LogError(this.GetType(), "Order not found");
                        return null;
                    }

                    apOrder = new Sale
                    {
                        SaleID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        QuoteValidDate = order.QuoteValidDate,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        DeliveryTime = order.DeliveryTime,
                        SaleType = ViewType.APOrder,
                        DeliveryDate = order.DeliveryDate,
                        PopUpNote = order.PopUpNote,
                        DocketNote = order.IntakeNote,
                        InvoiceNote = order.InvoiceNote,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterSupplier,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.APOrderAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.APOrderAttachmentID,
                                           SaleAttachmentID = attachment.APOrderID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.APOrderDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APOrderDetailID,
                                           SaleID = detail.APOrderID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityDelivered,
                                           Notes = detail.Notes,
                                           WeightReceived = detail.WeightDelivered,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,

                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList()
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order found");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apOrder;
        }
       
        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPOrdersByName(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPOrdersByName(): Attempting to get the orders for search term:{0}", searchTerm));
            var orders = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APOrders
                              where order.BPMasterSupplier.Name.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(order.NouDocStatusID) 
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  QuoteValidDate = order.QuoteValidDate,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APOrder,
                                  DeliveryDate = order.DeliveryDate,
                                  Printed = order.Printed,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  DocketNote = order.IntakeNote,
                                  InvoiceNote = order.InvoiceNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APOrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APOrderAttachmentID,
                                                     SaleAttachmentID = attachment.APOrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APOrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APOrderDetailID,
                                                     SaleID = detail.APOrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityDelivered,
                                                     WeightReceived = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                               
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPOrdersByCode(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPordersByCode(): Attempting to get the orders for search term:{0}", searchTerm));
            var orders = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APOrders
                              where order.BPMasterSupplier.Code.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  QuoteValidDate = order.QuoteValidDate,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  Printed = order.Printed,
                                  SaleType = ViewType.APOrder,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  TotalExVAT = order.TotalExVAT,
                                  DocketNote = order.IntakeNote,
                                  InvoiceNote = order.InvoiceNote,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APOrderAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APOrderAttachmentID,
                                                     SaleAttachmentID = attachment.APOrderID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APOrderDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APOrderDetailID,
                                                     SaleID = detail.APOrderID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityDelivered,
                                                     WeightReceived = detail.WeightDelivered,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExclVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order (and associated order details) for the input order id.
        /// </summary>
        /// <param name="apOrderId">The order id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        public Sale GetAPOrderById(int apOrderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPOrderById: Attempting to get the order for ap order id:{0}", apOrderId));
            Sale apOrder;
         
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APOrders.FirstOrDefault(x => x.APOrderID == apOrderId && x.Deleted == null);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "order Data corresponding to order id not found");
                        return null;
                    }

                    apOrder = new Sale
                    {
                        SaleID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        Number = order.Number,
                        NouDocStatusID = order.NouDocStatusID,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        SaleType = ViewType.APOrder,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        DocketNote = order.IntakeNote,
                        InvoiceNote = order.InvoiceNote,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        APGoodsReceipt = order.APGoodsReceipts.Any() ? new Sale
                        {
                            SaleID = order.APGoodsReceipts.First().APGoodsReceiptID,
                            TotalExVAT = order.APGoodsReceipts.First().TotalExVAT,
                            GrandTotalIncVAT = order.APGoodsReceipts.First().GrandTotalIncVAT,
                            DiscountIncVAT = order.APGoodsReceipts.First().DiscountIncVAT,
                            SaleDetails = (from detail in order.APGoodsReceipts.First().APGoodsReceiptDetails
                                           select new SaleDetail
                                           {
                                               SaleDetailID = detail.APGoodsReceiptDetailID,
                                               WeightReceived = detail.WeightReceived,
                                               QuantityReceived = detail.QuantityReceived
                                           }).ToList()
                        } : null,
                        
                        SaleDetails = (from detail in order.APOrderDetails
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APOrderDetailID,
                                           SaleID = detail.APOrderID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityDelivered,
                                           WeightReceived = detail.WeightDelivered,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                          
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }

                this.Log.LogDebug(this.GetType(), "Order successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return apOrder;
        }

        #endregion

        #region intake

        /// <summary> Adds a new purchase receipt (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public Tuple<int, int> AddAPReceipt(Sale sale)
        {
            var localStockTransactionId = 0;
            var batchNoId = 0;
            this.Log.LogDebug(this.GetType(), string.Format("AddAPReceipt() - Number:{0}, Base Ref:{1}", sale.Number, sale.BaseDocumentReferenceID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = new APGoodsReceipt
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                        Remarks = sale.Remarks,
                        TotalExVAT = sale.TotalExVAT.ToDecimal(),
                        DeliveryDate = sale.DeliveryDate,
                        GoodsReceiptDate = DateTime.Now,
                        VAT = sale.VAT,
                        PopUpNote = sale.PopUpNote,
                        IntakeNote = sale.DocketNote,
                        InvoiceNote = sale.InvoiceNote,
                        DeliveryTime = sale.DeliveryTime,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        UserID_CreatedBy = NouvemGlobal.UserId,
                        Reference = sale.Reference
                    };

                    if (sale.Customer != null)
                    {
                        // add the customer snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Customer.BPMasterID,
                            BPMaster_Name = sale.Customer.Name,
                            BPMaster_Code = sale.Customer.Code,
                            BPMaster_UpLift = sale.Customer.Uplift,
                            BPMaster_CreditLimit = sale.Customer.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Customer.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        order.BPMasterSnapshotID_Supplier = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Supplier = snapshot.BPMasterID;
                    }

                    if (sale.Haulier != null)
                    {
                        // add the haulier snapshot
                        var snapshot = new BPMasterSnapshot
                        {
                            BPMasterID = sale.Haulier.BPMasterID,
                            BPMaster_Name = sale.Haulier.Name,
                            BPMaster_Code = sale.Haulier.Code,
                            BPMaster_UpLift = sale.Haulier.Uplift,
                            BPMaster_CreditLimit = sale.Haulier.CreditLimit,
                            BPType_Type = Constant.Customer,
                            BPGroup_BPGroupName = sale.Haulier.BPGroupName ?? string.Empty,
                            Deleted = false
                        };

                        entities.BPMasterSnapshots.Add(snapshot);
                        entities.SaveChanges();
                        order.BPMasterSnapshotID_Haulier = snapshot.BPMasterSnapshotID;
                        order.BPMasterID_Haulier = snapshot.BPMasterID;
                    }

                    if (sale.DeliveryAddress != null)
                    {
                        // add the address snapshot
                        var deliverySnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.DeliveryAddress.Details.AddressLine1,
                            AddressLine2 = sale.DeliveryAddress.Details.AddressLine2,
                            AddressLine3 = sale.DeliveryAddress.Details.AddressLine3,
                            AddressLine4 = sale.DeliveryAddress.Details.AddressLine4,
                            PostCode = sale.DeliveryAddress.Details.PostCode,
                            Shipping = sale.DeliveryAddress.Details.Shipping,
                            Billing = sale.DeliveryAddress.Details.Billing,
                            PlantID = sale.DeliveryAddress.Details.PlantID
                        };

                        entities.BPAddressSnapshots.Add(deliverySnapShot);
                        entities.SaveChanges();
                        order.BPAddressSnapshotID_Delivery = deliverySnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Delivery = sale.DeliveryAddress.Details.BPAddressID;
                    }

                    if (sale.InvoiceAddress != null)
                    {
                        // add the address snapshot
                        var invoiceSnapShot = new BPAddressSnapshot
                        {
                            AddressLine1 = sale.InvoiceAddress.Details.AddressLine1,
                            AddressLine2 = sale.InvoiceAddress.Details.AddressLine2,
                            AddressLine3 = sale.InvoiceAddress.Details.AddressLine3,
                            AddressLine4 = sale.InvoiceAddress.Details.AddressLine4,
                            PostCode = sale.InvoiceAddress.Details.PostCode,
                            Shipping = sale.InvoiceAddress.Details.Shipping,
                            Billing = sale.InvoiceAddress.Details.Billing,
                            PlantID = sale.InvoiceAddress.Details.PlantID
                        };

                        entities.BPAddressSnapshots.Add(invoiceSnapShot);
                        entities.SaveChanges();
                        order.BPAddressSnapshotID_Invoice = invoiceSnapShot.BPAddressSnapshotID;
                        order.BPAddressID_Invoice = sale.InvoiceAddress.Details.BPAddressID;
                    }

                    entities.APGoodsReceipts.Add(order);

                    // pass the newly created apgoodsreceipt id back out.
                    sale.SaleID = order.APGoodsReceiptID;

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Marking base document {0} as complete", order.BaseDocumentReferenceID));
                        var baseDoc = entities.APOrders.FirstOrDefault(x => x.APOrderID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                        }
                    }

                    // add the attachments
                    foreach (var attachment in sale.Attachments)
                    {
                        var goodsReceiptAttachment = new APGoodsReceiptAttachment
                        {
                            APGoodsReceiptID = order.APGoodsReceiptID,
                            AttachmentDate = attachment.AttachmentDate,
                            FilePath = attachment.FilePath,
                            FileName = attachment.FileName,
                            File = attachment.File
                        };

                        entities.APGoodsReceiptAttachments.Add(goodsReceiptAttachment);
                    }

                    entities.SaveChanges();

                    // save the details
                    var saleID = order.APGoodsReceiptID;

                    var saleDetailsToProcess = sale.CopyingFromOrder
                        ? sale.SaleDetails
                        : sale.SaleDetails.Where(x => x.Process).ToList();

                    if (saleDetailsToProcess.Any(x => x.Process))
                    {
                        order.NouDocStatusID = NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;
                    }

                    foreach (var detail in saleDetailsToProcess)
                    {
                        // Add to the master snapshot table first
                        var snapshot = new INMasterSnapshot
                        {
                            INMasterID = detail.InventoryItem.Master.INMasterID,
                            Code = detail.InventoryItem.Master.Code,
                            Name = detail.InventoryItem.Master.Name,
                            MaxWeight = detail.InventoryItem.Master.MaxWeight,
                            MinWeight = detail.InventoryItem.Master.MinWeight,
                            NominalWeight = detail.InventoryItem.Master.NominalWeight,
                            TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                        };

                        entities.INMasterSnapshots.Add(snapshot);

                        var orderDetail = new APGoodsReceiptDetail
                        {
                            APGoodsReceiptID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            QuantityReceived = detail.QuantityReceived,
                            WeightReceived = detail.WeightReceived,
                            WeightOrdered = detail.WeightOrdered,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                            VAT = detail.VAT,
                            Notes = detail.Notes,
                            BatchNumberID = detail.BatchNumberId,
                            VATCodeID = detail.VatCodeID,
                            TotalExcVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterID = detail.INMasterID,
                            INMasterSnapshotID = snapshot.INMasterSnapshotID
                        };

                        entities.APGoodsReceiptDetails.Add(orderDetail);
                        entities.SaveChanges();

                        // pass the apgoodsreceiptdetail id back out
                        detail.SaleDetailID = orderDetail.APGoodsReceiptDetailID;

                        #region stock transaction/traceability

                        var stockDetail = detail.StockDetailToProcess;
                        if (stockDetail != null)
                        {
                            if (stockDetail.StockTransactionID.HasValue)
                            {
                                var dbTransaction = entities.StockTransactions.FirstOrDefault(x =>
                                    x.StockTransactionID == stockDetail.StockTransactionID);

                                dbTransaction.TransactionWeight = stockDetail.TransactionWeight;
                                dbTransaction.Tare = stockDetail.Tare;
                                dbTransaction.TransactionDate = DateTime.Now;
                                dbTransaction.GrossWeight = stockDetail.GrossWeight;
                                dbTransaction.WarehouseID = stockDetail.WarehouseID;
                                dbTransaction.UserMasterID = stockDetail.UserMasterID;
                                dbTransaction.DeviceMasterID = stockDetail.DeviceMasterID;
                                dbTransaction.BatchNumberID = stockDetail.BatchNumberID;
                                dbTransaction.MasterTableID = orderDetail.APGoodsReceiptDetailID;
                                dbTransaction.ProcessID = stockDetail.ProcessID;
                                dbTransaction.INMasterID = detail.INMasterID;
                                dbTransaction.InLocation = true;
                                dbTransaction.LabelID = order.APGoodsReceiptID.ToString();
                                if (!dbTransaction.Serial.HasValue)
                                {
                                    dbTransaction.Serial = dbTransaction.StockTransactionID;
                                }

                                entities.SaveChanges();
                            }
                            else
                            {
                                var localAttribute = this.CreateAttribute(stockDetail);
                                entities.Attributes.Add(localAttribute);
                                entities.SaveChanges();

                                if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                {
                                    foreach (var nonStandardResponse in sale.NonStandardResponses)
                                    {
                                        nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                        entities.Attributes.Add(nonStandardResponse);
                                        entities.SaveChanges();
                                    }
                                }

                                var localTransaction = this.CreateTransaction(stockDetail);
                                if (localTransaction.TransactionQTY.IsNullOrZero())
                                {
                                    localTransaction.TransactionQTY = 1;
                                }

                                localTransaction.MasterTableID = orderDetail.APGoodsReceiptDetailID;
                                localTransaction.InLocation = true;
                                localTransaction.Comments = stockDetail.Comments;
                                localTransaction.LabelID = order.APGoodsReceiptID.ToString();
                                localTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                localTransaction.AttributeID = localAttribute.AttributeID;

                                if (stockDetail.BatchAttribute != null)
                                {
                                    var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                    batchAttribute.IsBatch = true;
                                    var localBatch =
                                        entities.BatchNumbers.FirstOrDefault(
                                            x => x.BatchNumberID == localTransaction.BatchNumberID);

                                    if (localBatch != null)
                                    {
                                        if (localBatch.AttributeID == null)
                                        {
                                            entities.Attributes.Add(batchAttribute);
                                            entities.SaveChanges();
                                            localBatch.AttributeID = batchAttribute.AttributeID;
                                        }
                                        else
                                        {
                                            #region batch attribute update

                                            var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                            if (attribute != null)
                                            {
                                                this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                            }

                                            #endregion
                                        }
                                    }
                                }

                                entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                    localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                    localTransaction.TransactionWeight, localTransaction.WarehouseID, localTransaction.NouTransactionTypeID);
                                entities.StockTransactions.Add(localTransaction);
                                entities.SaveChanges();
                                localStockTransactionId = localTransaction.StockTransactionID;
                                localTransaction.Serial = localTransaction.StockTransactionID;
                                entities.SaveChanges();
                            }
                        }

                        #endregion
                    }

                    return Tuple.Create(order.APGoodsReceiptID, localStockTransactionId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, 0);
        }

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPReceipt(Sale sale)
        {
            var batchNoId = 0;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == sale.SaleID && x.Deleted == null);

                    if (dbOrder != null)
                    {
                        dbOrder.TotalExVAT = sale.TotalExVAT.ToDecimal();
                        dbOrder.VAT = sale.VAT;
                        dbOrder.DiscountIncVAT = sale.DiscountIncVAT;
                        dbOrder.SubTotalExVAT = sale.SubTotalExVAT;
                        dbOrder.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Today;
                        dbOrder.Reference = sale.Reference;
                        dbOrder.PopUpNote = sale.PopUpNote;
                        dbOrder.NouDocStatusID = sale.IsOrderFilled() ? NouvemGlobal.NouDocStatusFilled.NouDocStatusID : NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;

                        if (!ApplicationSettings.TouchScreenMode)
                        {
                            dbOrder.BPMasterID_Supplier = sale.Customer.BPMasterID;
                            dbOrder.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                            dbOrder.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                            dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                            dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                            dbOrder.DocumentDate = sale.DocumentDate;
                            dbOrder.CreationDate = sale.CreationDate;
                            dbOrder.DiscountPercentage = sale.DiscountPercentage;
                            dbOrder.Remarks = sale.Remarks;
                            dbOrder.InvoiceNote = sale.InvoiceNote;
                            dbOrder.IntakeNote = sale.DocketNote;
                            dbOrder.DeliveryDate = sale.DeliveryDate;
                            dbOrder.DeliveryTime = sale.DeliveryTime;
                            dbOrder.DeliveryDate = sale.DeliveryDate;
                            dbOrder.UserMasterID = sale.SalesEmployeeID;
                            dbOrder.Deleted = sale.Deleted;

                            foreach (var attachment in sale.Attachments)
                            {
                                var dbAttachment =
                                        entities.APGoodsReceiptAttachments.FirstOrDefault(
                                            x => x.APGoodsReceiptAttachmentID == attachment.SaleAttachmentID);

                                if (dbAttachment != null)
                                {
                                    // updating an existing attachment
                                    if (attachment.Deleted != null)
                                    {
                                        entities.APGoodsReceiptAttachments.Remove(dbAttachment);
                                    }
                                    else
                                    {
                                        dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                        dbAttachment.FileName = attachment.FileName;
                                        dbAttachment.File = attachment.File;
                                    }
                                }
                                else
                                {
                                    // adding a new attachment
                                    var localAttachment = new APGoodsReceiptAttachment
                                    {
                                        AttachmentDate = attachment.AttachmentDate,
                                        APGoodsReceiptID = sale.SaleID,
                                        FileName = attachment.FileName,
                                        File = attachment.File
                                    };

                                    entities.APGoodsReceiptAttachments.Add(localAttachment);
                                }
                            }
                        }

                        // update the details
                        foreach (var detail in sale.SaleDetails.Where(x => x.Process))
                        {
                            if (detail.SaleDetailID != 0)
                            {
                                // existing sale item
                                var dbOrderDetail =
                                      entities.APGoodsReceiptDetails.FirstOrDefault(
                                        x => x.APGoodsReceiptDetailID == detail.SaleDetailID && x.Deleted == null);

                                if (dbOrderDetail == null)
                                {
                                    // serious error - should never happen
                                    this.Log.LogError(this.GetType(), string.Format("APReceiptDetail:{0} not found in the database", detail.SaleDetailID));
                                    return false;
                                }

                                dbOrderDetail.QuantityReceived = detail.QuantityReceived;
                                dbOrderDetail.WeightReceived = detail.WeightReceived;
                                dbOrderDetail.BatchNumberID = detail.BatchNumberId;

                                #region stock transaction/traceability

                                var stockDetail = detail.StockDetailToProcess;
                                if (stockDetail != null)
                                {
                                    if (stockDetail.StockTransactionID.HasValue)
                                    {
                                        var dbTransaction = entities.StockTransactions.FirstOrDefault(x =>
                                            x.StockTransactionID == stockDetail.StockTransactionID);

                                        dbTransaction.TransactionWeight = stockDetail.TransactionWeight;
                                        dbTransaction.Tare = stockDetail.Tare;
                                        dbTransaction.GrossWeight = stockDetail.GrossWeight;
                                        dbTransaction.WarehouseID = stockDetail.WarehouseID;
                                        dbTransaction.TransactionDate = DateTime.Now;
                                        dbTransaction.UserMasterID = stockDetail.UserMasterID;
                                        dbTransaction.DeviceMasterID = stockDetail.DeviceMasterID;
                                        dbTransaction.BatchNumberID = stockDetail.BatchNumberID;
                                        dbTransaction.MasterTableID = dbOrderDetail.APGoodsReceiptDetailID;
                                        dbTransaction.ProcessID = stockDetail.ProcessID;
                                        dbTransaction.INMasterID = dbOrderDetail.INMasterID;
                                        dbTransaction.InLocation = true;
                                        dbTransaction.LabelID = dbOrder.APGoodsReceiptID.ToString();
                                        if (!dbTransaction.Serial.HasValue)
                                        {
                                            dbTransaction.Serial = dbTransaction.StockTransactionID;
                                        }

                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        var localAttribute = this.CreateAttribute(stockDetail);
                                        entities.Attributes.Add(localAttribute);
                                        entities.SaveChanges();

                                        if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                        {
                                            foreach (var nonStandardResponse in sale.NonStandardResponses)
                                            {
                                                nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                                entities.Attributes.Add(nonStandardResponse);
                                                entities.SaveChanges();
                                            }
                                        }

                                        var localTransaction = this.CreateTransaction(stockDetail);
                                        localTransaction.MasterTableID = dbOrderDetail.APGoodsReceiptDetailID;
                                        if (localTransaction.TransactionQTY.IsNullOrZero())
                                        {
                                            localTransaction.TransactionQTY = 1;
                                        }

                                        localTransaction.InLocation = true;
                                        localTransaction.Comments = stockDetail.Comments;
                                        localTransaction.LabelID = dbOrder.APGoodsReceiptID.ToString();
                                        localTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                        localTransaction.AttributeID = localAttribute.AttributeID;

                                        if (stockDetail.BatchAttribute != null)
                                        {
                                            var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                            batchAttribute.IsBatch = true;
                                            var localBatch =
                                                entities.BatchNumbers.FirstOrDefault(
                                                    x => x.BatchNumberID == localTransaction.BatchNumberID);

                                            if (localBatch != null)
                                            {
                                                if (localBatch.AttributeID == null)
                                                {
                                                    entities.Attributes.Add(batchAttribute);
                                                    entities.SaveChanges();
                                                    localBatch.AttributeID = batchAttribute.AttributeID;
                                                }
                                                else
                                                {
                                                    #region batch attribute update

                                                    var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                                    if (attribute != null)
                                                    {
                                                        this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                    }

                                                    #endregion
                                                }
                                            }
                                        }

                                        entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                            localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                            localTransaction.TransactionWeight, localTransaction.WarehouseID, localTransaction.NouTransactionTypeID);

                                        entities.StockTransactions.Add(localTransaction);
                                        entities.SaveChanges();
                                        sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;
                                        localTransaction.Serial = localTransaction.StockTransactionID;
                                        entities.SaveChanges();
                                    }
                                }

                                #endregion

                                entities.SaveChanges();
                            }
                            else
                            {
                                // Add to the master snapshot table first
                                var snapshot = new INMasterSnapshot
                                {
                                    INMasterID = detail.InventoryItem.Master.INMasterID,
                                    Code = detail.InventoryItem.Master.Code,
                                    Name = detail.InventoryItem.Master.Name,
                                    MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                    MinWeight = detail.InventoryItem.Master.MinWeight,
                                    NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                    TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                                };

                                entities.INMasterSnapshots.Add(snapshot);

                                var orderDetail = new APGoodsReceiptDetail
                                {
                                    APGoodsReceiptID = dbOrder.APGoodsReceiptID,
                                    QuantityOrdered = detail.QuantityOrdered,
                                    QuantityReceived = detail.QuantityReceived,
                                    WeightOrdered = detail.WeightOrdered,
                                    WeightReceived = detail.WeightReceived,
                                    UnitPrice = detail.UnitPrice,
                                    BatchNumberID = detail.BatchNumberId,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    Notes = detail.Notes,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExcVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                    INMasterID = detail.INMasterID,
                                    INMasterSnapshotID = snapshot.INMasterSnapshotID
                                };

                                entities.APGoodsReceiptDetails.Add(orderDetail);
                                entities.SaveChanges();

                                // pass the apgoodsreceiptdetail id back out
                                detail.SaleDetailID = orderDetail.APGoodsReceiptDetailID;

                                #region stock transaction/traceability

                                var stockDetail = detail.StockDetailToProcess;
                                if (stockDetail != null)
                                {
                                    if (stockDetail.StockTransactionID.HasValue)
                                    {
                                        var dbTransaction = entities.StockTransactions.FirstOrDefault(x =>
                                            x.StockTransactionID == stockDetail.StockTransactionID);

                                        dbTransaction.TransactionWeight = stockDetail.TransactionWeight;
                                        dbTransaction.Tare = stockDetail.Tare;
                                        dbTransaction.GrossWeight = stockDetail.GrossWeight;
                                        dbTransaction.WarehouseID = stockDetail.WarehouseID;
                                        dbTransaction.UserMasterID = stockDetail.UserMasterID;
                                        dbTransaction.DeviceMasterID = stockDetail.DeviceMasterID;
                                        dbTransaction.BatchNumberID = stockDetail.BatchNumberID;
                                        dbTransaction.MasterTableID = orderDetail.APGoodsReceiptDetailID;
                                        dbTransaction.ProcessID = stockDetail.ProcessID;
                                        dbTransaction.INMasterID = detail.INMasterID;
                                        dbTransaction.TransactionDate = DateTime.Now;
                                        dbTransaction.InLocation = true;
                                        dbTransaction.LabelID = dbOrder.APGoodsReceiptID.ToString();
                                        if (!dbTransaction.Serial.HasValue)
                                        {
                                            dbTransaction.Serial = dbTransaction.StockTransactionID;
                                        }

                                        entities.SaveChanges();
                                    }
                                    else
                                    {
                                        var localAttribute = this.CreateAttribute(stockDetail);
                                        entities.Attributes.Add(localAttribute);
                                        entities.SaveChanges();

                                        if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                        {
                                            foreach (var nonStandardResponse in sale.NonStandardResponses)
                                            {
                                                nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                                entities.Attributes.Add(nonStandardResponse);
                                                entities.SaveChanges();
                                            }
                                        }

                                        var localTransaction = this.CreateTransaction(stockDetail);
                                        localTransaction.MasterTableID = orderDetail.APGoodsReceiptDetailID;
                                        localTransaction.InLocation = true;
                                        localTransaction.LabelID = dbOrder.APGoodsReceiptID.ToString();
                                        localTransaction.NouTransactionTypeID = NouvemGlobal.TransactionTypeGoodsReceiptId;
                                        localTransaction.AttributeID = localAttribute.AttributeID;
                                        localTransaction.Comments = stockDetail.Comments;

                                        if (stockDetail.BatchAttribute != null)
                                        {
                                            var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                            batchAttribute.IsBatch = true;
                                            var localBatch =
                                                entities.BatchNumbers.FirstOrDefault(
                                                    x => x.BatchNumberID == localTransaction.BatchNumberID);

                                            if (localBatch != null)
                                            {
                                                if (localBatch.AttributeID == null)
                                                {
                                                    entities.Attributes.Add(batchAttribute);
                                                    entities.SaveChanges();
                                                    localBatch.AttributeID = batchAttribute.AttributeID;
                                                }
                                                else
                                                {
                                                    #region batch attribute update

                                                    var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                                    if (attribute != null)
                                                    {
                                                        this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                    }

                                                    #endregion
                                                }
                                            }
                                        }

                                        entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                            localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                            localTransaction.TransactionWeight, localTransaction.WarehouseID, localTransaction.NouTransactionTypeID);

                                        entities.StockTransactions.Add(localTransaction);
                                        entities.SaveChanges();
                                        sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;
                                        localTransaction.Serial = localTransaction.StockTransactionID;
                                        entities.SaveChanges();
                                    }
                                }

                                #endregion
                            }
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a line with a batch no.
        /// </summary>
        /// <param name="id">The line id.</param>
        /// <param name="batchNo">The batch no.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateAPReceiptBatchNumber(int id, int batchNo)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrderDetail = entities.APGoodsReceiptDetails.FirstOrDefault(x => x.APGoodsReceiptDetailID == id);
                    if (dbOrderDetail != null)
                    {
                        dbOrderDetail.BatchNumberID = batchNo;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the intake (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public IList<App_GetIntakeDetails_Result> GetIntakeStatusDetails(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetIntakeDetails(id).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        public bool PriceIntakeDocket(int docketId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_PriceIntakeDocket(docketId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Prices a purchase invoice docket.
        /// </summary>
        /// <param name="intakeId">The intake id used to price it.</param>
        /// <param name="invoiceId">The invoice id if updating.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Invoice number if successful, otherwise 0.</returns>
        public int PricePurchaseInvoice(int intakeId, int invoiceId, int userId, int deviceId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoiceNo = new System.Data.Entity.Core.Objects.ObjectParameter("InvoiceNo", typeof(int));
                    entities.App_PricePurchaseInvoice(intakeId, invoiceId, userId, deviceId, invoiceNo);
                    if (invoiceNo.Value != null)
                    {
                        var invoiceNumber = invoiceNo.Value.ToInt();
                        this.Log.LogInfo(this.GetType(), $"Invoice no {invoiceNumber} created");
                        return invoiceNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Gets the batch numbers associated with an intake product line.
        /// </summary>
        /// <returns>Batch ids.</returns>
        public IList<BatchNumber> GetIntakeLineBatchNumbers(int intakeLineId)
        {
            var batches = new List<BatchNumber>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    batches = entities.StockTransactions
                        .Where(x => x.MasterTableID == intakeLineId && x.Deleted == null &&
                                    x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId)
                        .Select(x => x.BatchNumber).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return batches;
        }

        /// <summary>
        /// Gets the batch numbers associated with an intake product line.
        /// </summary>
        /// <returns>Batch ids.</returns>
        public IList<string> GetIntakeLineBatchReferences(int intakeLineId)
        {
            var batches = new List<string>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    batches = entities.StockTransactions
                        .Where(x => x.BatchNumber != null && x.MasterTableID == intakeLineId && x.Deleted == null &&
                                    x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId)
                        .Select(x => x.BatchNumber.Number).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return batches;
        }

        /// <summary>
        /// Gets the intake details.
        /// </summary>
        /// <param name="id">The intake id to search for.</param>
        /// <returns>An intake, and it's details.</returns>
        public Sale GetTouchscreenReceiptById(int id)
        {
            var dbOrder = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == id);
                    if (order == null)
                    {
                        return dbOrder;
                    }

                    dbOrder = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        APOrderID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        SaleType = ViewType.APReceipt,
                        DeviceId = order.DeviceID,
                        PopUpNote = order.PopUpNote,
                        InvoiceNote = order.InvoiceNote,
                        DocketNote = order.IntakeNote,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        DeliveryDate = order.DeliveryDate,
                        DeliveryTime = order.DeliveryTime,
                        VAT = order.VAT,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterSupplier,
                        Reference = order.Reference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.APGoodsReceiptDetails.Where(x => x.Deleted == null)
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APGoodsReceiptDetailID,
                                           SaleID = detail.APGoodsReceiptID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           Batch = detail.BatchNumber,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           SaleDetailType = ViewType.APReceipt,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted,
                                           TransactionCount = entities.StockTransactions
                                           .Count(x => x.MasterTableID == detail.APGoodsReceiptDetailID && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId && x.Deleted == null)
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dbOrder;
        }

        /// <summary>
        /// Gets the intake details.
        /// </summary>
        /// <param name="id">The intake id to search for.</param>
        /// <returns>An intake, and it's details.</returns>
        public Sale GetTouchscreenReceiptByNextQueueNo(string killType)
        {
            var dbOrder = new Sale();
            var today = DateTime.Today;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APGoodsReceipts.Where(x => x.ProposedKillDate != null && x.ReferenceID != null 
                                                               && x.Deleted == null && x.NouDocStatusID != 2
                                                               && x.ProposedKillDate == today && x.Processed != true && x.KillType == killType)
                                                               .OrderBy(x => x.ReferenceID)
                                                              .FirstOrDefault();
                    if (order == null)
                    {
                        return null;
                    }

                    dbOrder = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        APOrderID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        SaleType = ViewType.APReceipt,
                        DeviceId = order.DeviceID,
                        PopUpNote = order.PopUpNote,
                        EditDate = order.EditDate,
                        InvoiceNote = order.InvoiceNote,
                        DocketNote = order.IntakeNote,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        DeliveryDate = order.DeliveryDate,
                        DeliveryTime = order.DeliveryTime,
                        VAT = order.VAT,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterSupplier,
                        Reference = order.Reference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.APGoodsReceiptDetails.Where(x => x.Deleted == null)
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APGoodsReceiptDetailID,
                                           SaleID = detail.APGoodsReceiptID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           Batch = detail.BatchNumber,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           SaleDetailType = ViewType.APReceipt,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted,
                                           TransactionCount = entities.StockTransactions
                                           .Count(x => x.MasterTableID == detail.APGoodsReceiptDetailID && x.NouTransactionTypeID == NouvemGlobal.TransactionTypeGoodsReceiptId && x.Deleted == null)
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dbOrder;
        }

        /// <summary>
        /// Cancels the input intake order.
        /// </summary>
        /// <param name="id">The purchase order to cancel the associated intake for.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        public bool CancelIntakeFromOrder(int id)
        {
            this.Log.LogWarning(this.GetType(), string.Format("CancelIntakeFromOrder(): Attempting to cancel intake docket(s) associated with order:{0}.", id));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var orders = entities.APGoodsReceipts.Where(x => x.BaseDocumentReferenceID == id);
                    foreach (var order in orders)
                    {
                        this.Log.LogWarning(this.GetType(), string.Format("Intake Order:{0} cancelled.", id));
                        order.NouDocStatusID = NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="batches">The workflow batches.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        public bool UpdateIntakes(HashSet<Sale> batches, ViewType view)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var i in batches)
                    {
                        if (view == ViewType.APOrder)
                        {
                            var batch =
                          entities.APOrders.FirstOrDefault(x => x.APOrderID == i.SaleID);
                            if (batch != null)
                            {
                                batch.Notes = i.TechnicalNotes;
                                batch.EditDate = DateTime.Now;
                                batch.UserMasterID = NouvemGlobal.UserId;
                                entities.SaveChanges();
                            }
                        }
                        else
                        {
                            var batch =
                          entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == i.SaleID);
                            if (batch != null)
                            {
                                batch.Notes = i.TechnicalNotes;
                                batch.EditDate = DateTime.Now;
                                batch.UserMasterID = NouvemGlobal.UserId;

                                entities.Notes.Add(new Note
                                {
                                    Notes = i.TechnicalNotes,
                                    MasterTableID = i.SaleID,
                                    CreationDate = DateTime.Now,
                                    DeviceMasterID = NouvemGlobal.DeviceId.ToInt(),
                                    UserMasterID = NouvemGlobal.UserId.ToInt(),
                                    NoteType = Constant.Intake
                                });

                                entities.SaveChanges();
                            }
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
       
        /// <summary>
        /// Removes stock from item.
        /// </summary>
        /// <param name="sale">The sale/detail/transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        public bool RemoveIntakeStockFromOrder(Sale sale)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveIntakeStockFromOrder(): Attempting to mark item:{0} as deleted", sale.SaleID));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbSale =
                        entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == sale.SaleID && x.Deleted == null);
                    if (dbSale != null)
                    {
                        dbSale.DiscountPercentage = sale.DiscountPercentage;
                        dbSale.DiscountIncVAT = sale.DiscountIncVAT;
                        dbSale.SubTotalExVAT = sale.SubTotalExVAT;
                        dbSale.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbSale.DeviceID = NouvemGlobal.DeviceId;
                        dbSale.EditDate = DateTime.Now;

                        this.Log.LogDebug(this.GetType(), string.Format("APReceipt id:{0} updated. TotalExVAT:{1}, GrandTotalIncVAT:{2}", sale.SaleID, sale.TotalExVAT, sale.GrandTotalIncVAT));
                        var saleDetail = sale.SaleDetails.FirstOrDefault(x => x.IsSelectedSaleDetail);
                        if (saleDetail != null)
                        {
                            var dbReceiptDetail =
                                entities.APGoodsReceiptDetails.FirstOrDefault(
                                    x => x.APGoodsReceiptDetailID == saleDetail.SaleDetailID);

                            if (dbReceiptDetail != null)
                            {
                                dbReceiptDetail.TotalExcVAT = saleDetail.TotalExVAT;
                                dbReceiptDetail.TotalIncVAT = saleDetail.TotalIncVAT;
                                dbReceiptDetail.WeightReceived = saleDetail.WeightReceived;
                                dbReceiptDetail.QuantityReceived = saleDetail.QuantityReceived;
                                this.Log.LogDebug(this.GetType(), string.Format("ARGoodsReceiptDetail id:{0} updated. TotalExVAT:{1}, TotalIncVAT:{2}",
                                    dbReceiptDetail.APGoodsReceiptDetailID, dbReceiptDetail.TotalExcVAT, dbReceiptDetail.TotalIncVAT));
                            }

                            entities.SaveChanges();
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), "ARReceipt not found");
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPReceiptStatus(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder =
                        entities.APGoodsReceipts.FirstOrDefault(
                            x => x.APGoodsReceiptID == sale.SaleID && x.Deleted == null);

                    if (dbOrder != null)
                    {
                        dbOrder.NouDocStatusID = sale.NouDocStatusID;
                        dbOrder.EditDate = DateTime.Now;
                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateDesktopAPReceipt(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder =
                        entities.APGoodsReceipts.FirstOrDefault(
                            x => x.APGoodsReceiptID == sale.SaleID && x.Deleted == null);

                    if (dbOrder != null)
                    {
                        if (sale.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                        {
                            dbOrder.UserID_CompletedBy = NouvemGlobal.UserId;
                        }

                        dbOrder.NouDocStatusID = sale.NouDocStatusID;
                        dbOrder.BPContactID = sale.MainContact != null && sale.MainContact.Details != null
                            ? (int?) sale.MainContact.Details.BPContactID
                            : null;
                        dbOrder.BPMasterID_Haulier = sale.Haulier != null && sale.Haulier.BPMasterID > 0 ? (int?) sale.Haulier.BPMasterID : null;
                        dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null
                            ? (int?) sale.DeliveryAddress.Details.BPAddressID
                            : null;
                        dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null
                            ? (int?) sale.InvoiceAddress.Details.BPAddressID
                            : null;
                        dbOrder.DocumentDate = sale.DocumentDate;
                        dbOrder.CreationDate = sale.CreationDate;
                        dbOrder.PopUpNote = sale.PopUpNote;
                        dbOrder.InvoiceNote = sale.InvoiceNote;
                        dbOrder.IntakeNote = sale.DocketNote;
                        dbOrder.Remarks = sale.Remarks;
                        dbOrder.DeliveryDate = sale.DeliveryDate;
                        dbOrder.UserMasterID = sale.SalesEmployeeID;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Now;
                        dbOrder.Deleted = sale.Deleted;

                        entities.SaveChanges();

                        foreach (var detail in sale.SaleDetails.Where(x => x.SaleDetailID > 0))
                        {
                            var dbOrderDetail =
                                     entities.APGoodsReceiptDetails.FirstOrDefault(
                                       x => x.APGoodsReceiptDetailID == detail.SaleDetailID);

                            if (dbOrderDetail == null)
                            {
                                continue;
                            }
                          
                            dbOrderDetail.QuantityOrdered = detail.QuantityOrdered.ToDecimal();
                            dbOrderDetail.WeightOrdered = detail.WeightOrdered.ToDecimal();
                            dbOrderDetail.TotalExcVAT = detail.TotalExVAT;
                            dbOrderDetail.TotalIncVAT = detail.TotalIncVAT;
                            dbOrderDetail.UnitPrice = detail.UnitPrice;
                            dbOrderDetail.UnitPriceAfterDiscount = detail.UnitPrice;
                            if (detail.PriceListID > 0)
                            {
                                dbOrderDetail.PriceListID = detail.PriceListID;
                            }

                            // only allow product change if nothing has been booked out.
                            if (detail.QuantityDelivered <= 0 && detail.WeightDelivered <= 0)
                            {
                                dbOrderDetail.INMasterID = detail.INMasterID;
                            }
                        }

                        foreach (var detail in sale.SaleDetails.Where(x => x.SaleDetailID == 0))
                        {
                            // Add to the master snapshot table first
                            var snapshot = new INMasterSnapshot
                            {
                                INMasterID = detail.InventoryItem.Master.INMasterID,
                                Code = detail.InventoryItem.Master.Code,
                                Name = detail.InventoryItem.Master.Name,
                                MaxWeight = detail.InventoryItem.Master.MaxWeight,
                                MinWeight = detail.InventoryItem.Master.MinWeight,
                                NominalWeight = detail.InventoryItem.Master.NominalWeight,
                                TypicalPieces = detail.InventoryItem.Master.TypicalPieces
                            };

                            entities.INMasterSnapshots.Add(snapshot);
                            entities.SaveChanges();

                            var orderDetail = new APGoodsReceiptDetail
                            {
                                APGoodsReceiptID = sale.SaleID,
                                QuantityOrdered = detail.QuantityOrdered.ToDecimal(),
                                WeightOrdered = detail.WeightOrdered,
                                UnitPrice = detail.UnitPrice,
                                DiscountPercentage = null,
                                DiscountAmount = null,
                                DiscountPrice = null,
                                LineDiscountAmount = null,
                                LineDiscountPercentage = 0,
                                UnitPriceAfterDiscount = detail.UnitPrice,
                                VAT = detail.VAT,
                                VATCodeID = detail.VatCodeID,
                                TotalExcVAT = detail.TotalExVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                INMasterID = detail.INMasterID,
                                PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                INMasterSnapshotID = snapshot.INMasterSnapshotID,
                                Comments = detail.Comments
                            };

                            entities.APGoodsReceiptDetails.Add(orderDetail);
                            entities.SaveChanges();
                        }

                        entities.SaveChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the transaction count for a given header row.
        /// </summary>
        /// <param name="id">The header row id.</param>
        /// <param name="transactionTypeId">The transaction type id</param>
        /// <returns>The transaction count for a given header row.</returns>
        public int GetTransactionCount(int id, int transactionTypeId)
        {
            var count = 0;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    count = entities.StockTransactions.Count(
                        x =>
                            x.MasterTableID == id && x.Deleted == null &&
                            x.NouTransactionTypeID == transactionTypeId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return count;
        }

        /// <summary>
        /// Removes a receipt detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveApReceiptDetail(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveApReceiptDetail(): Attempting to mark item:{0} as deleted", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var itemToDelete = entities.APGoodsReceiptDetails.FirstOrDefault(x => x.APGoodsReceiptDetailID == itemId);
                    if (itemToDelete != null)
                    {
                        itemToDelete.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Item deleted");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "Item could not be found");
            return false;
        }

        /// <summary>
        /// Removes a receipt detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        public bool RemoveApInvoiceDetail(int itemId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveApReceiptDetail(): Attempting to mark item:{0} as deleted", itemId));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var itemToDelete = entities.APInvoiceDetails.FirstOrDefault(x => x.APInvoiceDetailID == itemId);
                    if (itemToDelete != null)
                    {
                        itemToDelete.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Item deleted");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogError(this.GetType(), "Item could not be found");
            return false;
        }

        /// <summary>
        /// Adds or removes pallet stock from the order.
        /// </summary>
        /// <param name="serial">The pallet id.</param>
        /// <param name="intakeId">The order id.</param>
        /// <param name="addToOrder">Add or remove flag.</param>
        /// <returns>Flag, as to success or not.</returns>
        public bool AddOrRemovePalletStock(int? serial, int? intakeId, bool addToOrder)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_AddPalletStockToIntake(serial, intakeId, NouvemGlobal.DeviceId, NouvemGlobal.UserId,
                        addToOrder);
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Chages the status on an intake line.
        /// </summary>
        /// <param name="id">The line id.</param>
        /// <param name="statusId">The status to change to.</param>
        /// <returns>Flag, as to sucessful change or not.</returns>
        public bool ChangeAPReceiptDetailOrderStatus(int id, int statusId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.BatchNumbers.FirstOrDefault(x => x.BatchNumberID == id);
                    if (order != null)
                    {
                        order.NouDocStatusID = statusId;
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="complete">The complete order flag.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        public bool CompleteOrCancelAPReceipt(Sale sale, bool complete = true)
        {
            this.Log.LogDebug(this.GetType(), string.Format("Attempting to complete or close goods receipt:{0}. Complete:{1}", sale.SaleID, complete));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == sale.SaleID);
                    if (order != null)
                    {
                        order.NouDocStatusID = complete ? NouvemGlobal.NouDocStatusComplete.NouDocStatusID : NouvemGlobal.NouDocStatusCancelled.NouDocStatusID;
                        order.DeviceID = NouvemGlobal.DeviceId;
                        order.EditDate = DateTime.Now;
                       
                        if (order.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                        {
                            order.UserID_CompletedBy = NouvemGlobal.UserId;
                        }
                        
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), string.Format("Order:{0} marked as completed on device id:{1}", sale.SaleID, NouvemGlobal.DeviceId.ToInt()));
                    }
                    else
                    {
                        // No corresponding aporder, so nothing to do.
                        this.Log.LogDebug(this.GetType(), string.Format("CompleteOrCancelAPReceipt(): APReceipt:{0} has no corresponding aporder. Completing", sale.SaleID));
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsForReport(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceipts(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                              where order.Deleted == null
                              && orderStatuses.Contains(order.NouDocStatusID)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APReceipt,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  VAT = order.VAT,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  OtherReference = ApplicationSettings.IntakeMode == IntakeMode.WithProductionOrder ? entities.PROrders.FirstOrDefault(x => x.BatchNumberID == order.APGoodsReceiptDetails.FirstOrDefault().BatchNumberID).Reference : string.Empty,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APGoodsReceiptAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                                     SaleAttachmentID = attachment.APGoodsReceiptID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList()
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsForReportByDates(IList<int?> orderStatuses, DateTime start, DateTime end)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceipts(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                              where order.Deleted == null
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.DocumentDate >= start && order.DocumentDate <= end
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APReceipt,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  VAT = order.VAT,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceipts(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceipts(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                              where order.Deleted == null
                              && orderStatuses.Contains(order.NouDocStatusID)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APReceipt,
                                  DeviceId = order.DeviceID,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  PopUpNote = order.PopUpNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  DeliveryTime = order.DeliveryTime,
                                  VAT = order.VAT,
                                  Time = order.DeliveryTime,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  TechnicalNotes = order.Notes,
                                  UserIDModified = order.UserMasterID,
                                  OtherReference = order.BPMasterSupplier.Name,
                                  Reference = order.Reference,
                                  UserIDCreation = order.UserID_CreatedBy,
                                  UserIDCompletion = order.UserID_CompletedBy,
                                  ProductLine = order.APGoodsReceiptDetails.FirstOrDefault(x => x.Deleted == null),
                                  //OtherReference = order.APGoodsReceiptDetails.Any() ? order.APGoodsReceiptDetails.FirstOrDefault().BatchNumber.Attribute.Attribute30 : string.Empty,
                                  Deleted = order.Deleted,
                                  //Attachments = (from attachment in order.APGoodsReceiptAttachments
                                  //               where attachment.Deleted == null
                                  //               select new Attachment
                                  //               {
                                  //                   AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                  //                   SaleAttachmentID = attachment.APGoodsReceiptID,
                                  //                   AttachmentDate = attachment.AttachmentDate,
                                  //                   FilePath = attachment.FilePath,
                                  //                   FileName = attachment.FileName,
                                  //                   File = attachment.File,
                                  //                   Deleted = attachment.Deleted
                                  //               }).ToList(),
                                  //SaleDetails = (from detail in order.APGoodsReceiptDetails
                                  //               where detail.Deleted == null
                                  //               select new SaleDetail
                                  //               {
                                  //                   LoadingSale = true,
                                  //                   SaleDetailID = detail.APGoodsReceiptDetailID,
                                  //                   SaleID = detail.APGoodsReceiptID,
                                  //                   QuantityOrdered = detail.QuantityOrdered,
                                  //                   WeightOrdered = detail.WeightOrdered,
                                  //                   QuantityReceived = detail.QuantityReceived,
                                  //                   WeightReceived = detail.WeightReceived,
                                  //                   Batch = detail.BatchNumber,
                                  //                   UnitPrice = detail.UnitPrice,
                                  //                   DiscountPercentage = detail.DiscountPercentage,
                                  //                   DiscountPrice = detail.DiscountPrice,
                                  //                   LineDiscountPercentage = detail.LineDiscountPercentage,
                                  //                   LineDiscountAmount = detail.LineDiscountAmount,
                                  //                   UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                  //                   SaleDetailType = ViewType.APReceipt,
                                  //                   VAT = detail.VAT,
                                  //                   TotalExVAT = detail.TotalExcVAT,
                                  //                   TotalIncVAT = detail.TotalIncVAT,
                                  //                   VatCodeID = detail.VATCodeID,
                                  //                   INMasterID = detail.INMasterID,
                                  //                   PriceListID = detail.PriceListID ?? 0,
                                  //                   NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                  //                   Deleted = detail.Deleted

                                  //               }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received by kill type (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsByType(IList<int?> orderStatuses, string type)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceipts(): Attempting to get all the goods received");
            var orders = new List<Sale>();
            var today = DateTime.Today;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                              where order.Deleted == null
                              && order.KillType == type
                              && (order.DocumentDate >= today)
                              && orderStatuses.Contains(order.NouDocStatusID)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APReceipt,
                                  DeviceId = order.DeviceID,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  VAT = order.VAT,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Reference = order.Reference,
                                  OtherReference = string.Empty, 
                                  Deleted = order.Deleted,
                                  SaleDetails = (from detail in order.APGoodsReceiptDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APGoodsReceiptDetailID,
                                                     SaleID = detail.APGoodsReceiptID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityReceived,
                                                     WeightReceived = detail.WeightReceived,
                                                     Batch = detail.BatchNumber,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExcVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the receipts (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetAPReceipts(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPReceipts(): Attempting to get the quotes for customer id:{0}", customerId));
            var receipts = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    receipts = (from order in entities.APGoodsReceipts
                                where order.BPMasterID_Supplier == customerId && order.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APReceipt,
                                  Printed = order.Printed,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  PopUpNote = order.PopUpNote,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APGoodsReceiptAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                                     SaleAttachmentID = attachment.APGoodsReceiptID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APGoodsReceiptDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APGoodsReceiptID,
                                                     SaleID = detail.APGoodsReceiptID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityReceived,
                                                     WeightReceived = detail.WeightReceived,
                                                     UnitPrice = detail.UnitPrice,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExcVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                  
                                                     INMasterID = detail.INMasterID,
                                                     INMaster = detail.INMaster,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     OrderDate = order.DocumentDate,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted,
                                                     //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                                     //                     select new GoodsReceiptData
                                                     //                     {
                                                     //                         BatchNumber = batchNumber,
                                                     //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                                     //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                                     //                                            select new StockTransactionData
                                                     //                                            {
                                                     //                                                Transaction = transaction,
                                                     //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                     //                                            }).ToList()
                                                     //                     }).ToList(),
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders receipts successfully retrieved", receipts.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return receipts;
        }

        /// <summary>
        /// Retrieves the receipts (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        public IList<Sale> GetRecentAPReceipts(int customerId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetRecentAPReceipts(): Attempting to get the quotes for customer id:{0}", customerId));
            var receipts = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    receipts = (from order in entities.APGoodsReceipts
                                where order.BPMasterID_Supplier == customerId 
                                && order.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID 
                                && order.Deleted == null
                                orderby order.APGoodsReceiptID descending 
                                select new Sale
                                {
                                    SaleID = order.APGoodsReceiptID,
                                    APOrderID = order.APOrderID,
                                    DocumentNumberingID = order.DocumentNumberingID,
                                    Number = order.Number,
                                    BPContactID = order.BPContactID,
                                    DocumentDate = order.DocumentDate,
                                    CreationDate = order.CreationDate,
                                    DeviceId = order.DeviceID,
                                    EditDate = order.EditDate,
                                    SaleType = ViewType.APReceipt,
                                    PopUpNote = order.PopUpNote,
                                    InvoiceNote = order.InvoiceNote,
                                    DocketNote = order.IntakeNote,
                                    Printed = order.Printed,
                                    NouDocStatusID = order.NouDocStatusID,
                                    NouDocStatus = order.NouDocStatu.Value,
                                    BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                    Remarks = order.Remarks,
                                    TotalExVAT = order.TotalExVAT,
                                    DeliveryDate = order.DeliveryDate,
                                    VAT = order.VAT,
                                    SubTotalExVAT = order.SubTotalExVAT,
                                    DiscountIncVAT = order.DiscountIncVAT,
                                    GrandTotalIncVAT = order.GrandTotalIncVAT,
                                    DiscountPercentage = order.DiscountPercentage,
                                    SalesEmployeeID = order.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                    MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                    BPHaulier = order.BPMasterHaulier,
                                    BPCustomer = order.BPMasterSupplier,
                                    Deleted = order.Deleted,
                                    //Attachments = (from attachment in order.APGoodsReceiptAttachments
                                    //               where attachment.Deleted == null
                                    //               select new Attachment
                                    //               {
                                    //                   AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                    //                   SaleAttachmentID = attachment.APGoodsReceiptID,
                                    //                   AttachmentDate = attachment.AttachmentDate,
                                    //                   FilePath = attachment.FilePath,
                                    //                   FileName = attachment.FileName,
                                    //                   File = attachment.File,
                                    //                   Deleted = attachment.Deleted
                                    //               }).ToList(),
                                    SaleDetails = (from detail in order.APGoodsReceiptDetails
                                                   where detail.Deleted == null
                                                   select new SaleDetail
                                                   {
                                                       LoadingSale = true,
                                                       SaleDetailID = detail.APGoodsReceiptID,
                                                       SaleID = detail.APGoodsReceiptID,
                                                       QuantityOrdered = detail.QuantityOrdered,
                                                       WeightOrdered = detail.WeightOrdered,
                                                       QuantityReceived = detail.QuantityReceived,
                                                       WeightReceived = detail.WeightReceived,
                                                       UnitPrice = detail.UnitPrice,
                                                       SaleDetailType = ViewType.APReceipt,
                                                       DiscountPercentage = detail.DiscountPercentage,
                                                       DiscountPrice = detail.DiscountPrice,
                                                       LineDiscountPercentage = detail.LineDiscountPercentage,
                                                       LineDiscountAmount = detail.LineDiscountAmount,
                                                       UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                       VAT = detail.VAT,
                                                       TotalExVAT = detail.TotalExcVAT,
                                                       TotalIncVAT = detail.TotalIncVAT,
                                                       VatCodeID = detail.VATCodeID,
                                                 
                                                       INMasterID = detail.INMasterID,
                                                       INMaster = detail.INMaster,
                                                       PriceListID = detail.PriceListID ?? 0,
                                                       OrderDate = order.DocumentDate,
                                                       NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                       Deleted = detail.Deleted,
                                                       //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                                       //                     select new GoodsReceiptData
                                                       //                     {
                                                       //                         BatchNumber = batchNumber,
                                                       //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                                       //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                                       //                                            select new StockTransactionData
                                                       //                                            {
                                                       //                                                Transaction = transaction,
                                                       //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                       //                                            }).ToList()
                                                       //                     }).ToList(),
                                                   }).ToList(),
                                }).Take(10).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} orders receipts successfully retrieved", receipts.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return receipts;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsForBatchEdit()
        {
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                            where order.Deleted == null
                            orderby order.APGoodsReceiptID descending
                            select new Sale
                            {
                                SaleID = order.APGoodsReceiptID,
                                Number = order.Number,
                                Reference = order.Reference ?? SqlFunctions.StringConvert((double)order.Number)
                            }).Take(1000)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAllAPReceiptsByAttribute(string searchText)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceiptsByAttribute(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var batches =
                        entities.BatchNumbers.Where(
                            x => x.Attribute != null && x.Attribute.Attribute30.ToLower().Equals(searchText.ToLower())).Select(x => (int?)x.BatchNumberID);
                    var details = entities.APGoodsReceiptDetails.Where(x => batches.Contains(x.BatchNumberID)).Select(x => x.APGoodsReceiptID).Distinct();

                    orders = (from order in entities.APGoodsReceipts
                              where details.Contains(order.APGoodsReceiptID)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APReceipt,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  PopUpNote = order.PopUpNote,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  DeliveryTime = order.DeliveryTime,
                                  VAT = order.VAT,
                                  Time = order.DeliveryTime,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  OtherReference = order.BPMasterSupplier.Name,
                                  ProductLine = order.APGoodsReceiptDetails.FirstOrDefault(x => x.Deleted == null),
                                  Deleted = order.Deleted
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPReceiptsByPartner(int partnerId, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPReceiptsByName(): Attempting to get the orders for id:{0}", partnerId));
            var orders = new List<Sale>();
         
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                              where order.BPMasterID_Supplier == partnerId
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  SaleType = ViewType.APReceipt,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  PopUpNote = order.PopUpNote,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APGoodsReceiptAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                                     SaleAttachmentID = attachment.APGoodsReceiptID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APGoodsReceiptDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APGoodsReceiptDetailID,
                                                     SaleID = detail.APGoodsReceiptID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityReceived,
                                                     WeightReceived = detail.WeightReceived,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExcVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                               
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted,
                                                     //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                                     //                     select new GoodsReceiptData
                                                     //                     {
                                                     //                         BatchNumber = batchNumber,
                                                     //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                                     //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                                     //                                            select new StockTransactionData
                                                     //                                            {
                                                     //                                                Transaction = transaction,
                                                     //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                     //                                            }).ToList()
                                                     //                     }).ToList(),
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPReceiptsByName(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPReceiptsByName(): Attempting to get the orders for search term:{0}", searchTerm));
            var orders = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                              where order.BPMasterSupplier.Name.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  SaleType = ViewType.APReceipt,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeliveryDate = order.DeliveryDate,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  PopUpNote = order.PopUpNote,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APGoodsReceiptAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                                     SaleAttachmentID = attachment.APGoodsReceiptID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APGoodsReceiptDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APGoodsReceiptDetailID,
                                                     SaleID = detail.APGoodsReceiptID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityReceived,
                                                     WeightReceived = detail.WeightReceived,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExcVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                            
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted,
                                                     //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                                     //                     select new GoodsReceiptData
                                                     //                     {
                                                     //                         BatchNumber = batchNumber,
                                                     //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                                     //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                                     //                                            select new StockTransactionData
                                                     //                                            {
                                                     //                                                Transaction = transaction,
                                                     //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                     //                                            }).ToList()
                                                     //                     }).ToList(),
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public IList<Sale> GetAPReceiptsByCode(string searchTerm, IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPReceiptsByCode(): Attempting to get the order receipts for search term:{0}", searchTerm));
            var orders = new List<Sale>();
            var localSearchTerm = searchTerm.ToLower();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                              where order.BPMasterSupplier.Code.ToLower().StartsWith(localSearchTerm)
                              && orderStatuses.Contains(order.NouDocStatusID)
                              && order.Deleted == null
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  APOrderID = order.APOrderID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  SaleType = ViewType.APReceipt,
                                  CreationDate = order.CreationDate,
                                  PopUpNote = order.PopUpNote,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  Printed = order.Printed,
                                  DeliveryDate = order.DeliveryDate,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPHaulier = order.BPMasterHaulier,
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  Attachments = (from attachment in order.APGoodsReceiptAttachments
                                                 where attachment.Deleted == null
                                                 select new Attachment
                                                 {
                                                     AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                                     SaleAttachmentID = attachment.APGoodsReceiptID,
                                                     AttachmentDate = attachment.AttachmentDate,
                                                     FilePath = attachment.FilePath,
                                                     FileName = attachment.FileName,
                                                     File = attachment.File,
                                                     Deleted = attachment.Deleted
                                                 }).ToList(),
                                  SaleDetails = (from detail in order.APGoodsReceiptDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APGoodsReceiptDetailID,
                                                     SaleID = detail.APGoodsReceiptID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityReceived,
                                                     WeightReceived = detail.WeightReceived,
                                                     UnitPrice = detail.UnitPrice,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExcVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted,
                                                     //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                                     //                     select new GoodsReceiptData
                                                     //                     {
                                                     //                         BatchNumber = batchNumber,
                                                     //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                                     //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                                     //                                            select new StockTransactionData
                                                     //                                            {
                                                     //                                                Transaction = transaction,
                                                     //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                                     //                                            }).ToList()
                                                     //                     }).ToList(),
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="apReceiptId">The order serch id.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        public Sale GetAPReceiptFullById(int apReceiptId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPReceiptFullById(): Attempting to get the order receipts for search term:{0}", apReceiptId));
            var apReceipt = new Sale();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == apReceiptId && x.Deleted == null);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "receipt Data corresponding to order id not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        APOrderID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        SaleType = ViewType.APReceipt,
                        CreationDate = order.CreationDate,
                        Printed = order.Printed,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        DeliveryDate = order.DeliveryDate,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterSupplier,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.APGoodsReceiptAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                           SaleAttachmentID = attachment.APGoodsReceiptID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.APGoodsReceiptDetails
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APGoodsReceiptDetailID,
                                           SaleID = detail.APGoodsReceiptID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           UnitPrice = detail.UnitPrice,
                                           SaleDetailType = ViewType.APReceipt,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           Notes = detail.Notes,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted,
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apReceipt;
        }

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the last local edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details)</returns>
        public Sale GetAPReceiptFullByLastEdit()
        {
            var apReceipt = new Sale();

            try
            {
                int id;
                using (var entities = new NouvemEntities())
                {
                    var orderId = entities.APGoodsReceipts
                        .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (orderId == null)
                    {
                        return null;
                    }

                    id = orderId.APGoodsReceiptID;
                    this.PriceIntakeDocket(id);
                }

                using (var entities = new NouvemEntities())
                {
                    var order = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == id);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "receipt Data not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        APOrderID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        SaleType = ViewType.APReceipt,
                        PopUpNote = order.PopUpNote,
                        InvoiceNote = order.InvoiceNote,
                        DocketNote = order.IntakeNote,
                        CreationDate = order.CreationDate,
                        Printed = order.Printed,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        DeliveryDate = order.DeliveryDate,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterSupplier,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.APGoodsReceiptAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                           SaleAttachmentID = attachment.APGoodsReceiptID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.APGoodsReceiptDetails
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APGoodsReceiptDetailID,
                                           SaleID = detail.APGoodsReceiptID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           UnitPrice = detail.UnitPrice,
                                           SaleDetailType = ViewType.APReceipt,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted,
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return apReceipt;
        }

        /// <summary>
        /// Retrieves the receipt (and associated receipt details) for the input receipt id.
        /// </summary>
        /// <param name="apReceiptId">The receipt id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public Sale GetAPReceiptById(int apReceiptId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPReceiptById: Attempting to get the receipt for ap receipt id:{0}", apReceiptId));
            Sale apReceipt;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == apReceiptId && x.Deleted == null);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "receipt Data corresponding to order id not found");
                        return null;
                    }

                    apReceipt = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        NouDocStatusID = order.NouDocStatusID,
                        Device = order.DeviceMaster,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        SaleDetails = (from detail in order.APGoodsReceiptDetails
                                       select new SaleDetail
                                       {
                                           SaleDetailID = detail.APGoodsReceiptDetailID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived
                                       }).ToList(),
                    };
                }

                this.Log.LogDebug(this.GetType(), "Receipt successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return apReceipt;
        }

        /// <summary>
        /// Retrieves the receipt (and associated order details) for the input ap order id.
        /// </summary>
        /// <param name="orderId">The ap order id.</param>
        /// <returns>The receipt (and associated order details) for the input ap order id.</returns>
        public Sale GetAPReceiptsByApOrderId(int orderId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetAPReceiptsByApOrderId(): Attempting to get the ap receipts for order id:{0}", orderId));
            Sale receiptData;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order =
                        entities.APGoodsReceipts.FirstOrDefault(x => x.APOrderID == orderId && x.Deleted == null);

                    if (order == null)
                    {
                        this.Log.LogDebug(this.GetType(), "receiptData corresponding to order number not found");
                        return null;
                    }

                    receiptData = new Sale
                    {
                        SaleID = order.APGoodsReceiptID,
                        APOrderID = order.APOrderID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        DeviceId = order.DeviceID,
                        EditDate = order.EditDate,
                        SaleType = ViewType.APReceipt,
                        BPContactID = order.BPContactID,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        DeliveryDate = order.DeliveryDate,
                        PopUpNote = order.PopUpNote,
                        InvoiceNote = order.InvoiceNote,
                        DocketNote = order.IntakeNote,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        TotalExVAT = order.TotalExVAT,
                        VAT = order.VAT,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        BPHaulier = order.BPMasterHaulier,
                        BPCustomer = order.BPMasterSupplier,
                        Deleted = order.Deleted,
                        Attachments = (from attachment in order.APGoodsReceiptAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.APGoodsReceiptAttachmentID,
                                           SaleAttachmentID = attachment.APGoodsReceiptID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in order.APGoodsReceiptDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APGoodsReceiptID,
                                           SaleID = detail.APGoodsReceiptID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           UnitPrice = detail.UnitPrice,
                                           SaleDetailType = ViewType.APReceipt,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           INMaster = detail.INMaster,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           OrderDate = order.DocumentDate,
                                           Deleted = detail.Deleted
                                           //GoodsReceiptDatas = (from batchNumber in detail.BatchNumbers
                                           //                     select new GoodsReceiptData
                                           //                     {
                                           //                         BatchNumber = batchNumber,
                                           //                         BatchTraceabilities = batchNumber.BatchTraceabilities.ToList(),
                                           //                         TransactionData = (from transaction in batchNumber.StockTransactions
                                           //                                            select new StockTransactionData
                                           //                                            {
                                           //                                                Transaction = transaction,
                                           //                                                TransactionTraceabilities = transaction.TransactionTraceabilities.ToList()
                                           //                                            }).ToList()
                                           //                     }).ToList(),
                                       }).ToList(),
                    };
                }

                this.Log.LogDebug(this.GetType(), "Orders receipt successfully retrieved");
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return null;
            }

            return receiptData;
        }

        /// <summary>
        /// Gets the batch numbers.
        /// </summary>
        /// <returns>A collection of batch numbers.</returns>
        public IList<BatchNumber> GetBatchNumbers()
        {
            this.Log.LogDebug(this.GetType(), "Attempting to retrieve all the batch numbers");
            var numbers = new List<BatchNumber>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    numbers = entities.BatchNumbers.Where(x => x.Deleted == null).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} batch numbets retrieved", numbers.Count));
                }
            }
            catch(Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return numbers;
        }

        #endregion

        #region invoice

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public Tuple<int, DocNumber> AddNewAPInvoice(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (!sale.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        var alreadyInvoiced = entities.APInvoices.Any(x =>
                            x.BaseDocumentReferenceID == sale.BaseDocumentReferenceID && x.Deleted == null &&
                            x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID);

                        if (alreadyInvoiced)
                        {
                            return Tuple.Create(0, new DocNumber { DocumentNumberingTypeID = 1000 });
                        }
                    }

                    var dbNumber =
                        entities.DocumentNumberings.First(
                            x =>
                                x.NouDocumentName.DocumentName.Equals(Constant.APInvoice) &&
                                x.DocumentNumberingType.Name.Equals(Constant.Standard));

                    sale.Number = dbNumber.NextNumber;
                    sale.DocumentNumberingID = dbNumber.DocumentNumberingID;

                    var currentNo = dbNumber.NextNumber;
                    var nextNo = dbNumber.NextNumber + 1;
                    if (nextNo > dbNumber.LastNumber)
                    {
                        // Last number reached, so reset back to first number.
                        nextNo = dbNumber.FirstNumber;
                    }

                    dbNumber.NextNumber = nextNo;
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Document number updated");

                    var order = new APInvoice
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null && sale.MainContact.Details != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        BPContactID_Delivery = sale.DeliveryContact != null && sale.DeliveryContact.Details != null ? (int?)sale.DeliveryContact.Details.BPContactID : null,
                        BPMasterID_Supplier = sale.BPCustomer != null ? sale.BPCustomer.BPMasterID : sale.Customer != null ? sale.Customer.BPMasterID : (int?)null,
                        BPMasterID_Haulier = sale.BPHaulier != null ? sale.BPHaulier.BPMasterID : (int?)null,
                        BPAddressID_Delivery = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.BPAddressID : (int?)null,
                        BPAddressID_Invoice = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.BPAddressID : (int?)null,
                        RouteID = sale.RouteID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        DeliveryDate = sale.DeliveryDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                        CustomerPOReference = sale.CustomerPOReference,
                        Remarks = sale.Remarks,
                        InvoiceNote = sale.InvoiceNote,
                        TotalExVAT = sale.TotalExVAT,
                        VAT = sale.VAT,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        PORequired = sale.PORequired
                    };

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", order.BaseDocumentReferenceID));
                        var baseDoc = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            if (baseDoc.Invoiced == true)
                            {
                                // already invoiced, return;
                                return Tuple.Create(0, new DocNumber());
                            }

                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            baseDoc.Invoiced = true;
                            this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", order.BaseDocumentReferenceID));
                        }
                    }
                    else if (sale.IdsToMarkAsInvoiced != null && sale.IdsToMarkAsInvoiced.Any())
                    {
                        // Consolidated invoice created, so we mark all the associated intake dockets as invoiced.
                        var intakesToInvoice =
                            entities.APGoodsReceipts.Where(
                                x => sale.IdsToMarkAsInvoiced.Contains(x.APGoodsReceiptID) && x.Invoiced != true);

                        foreach (var docket in intakesToInvoice)
                        {
                            docket.Invoiced = true;
                            docket.APInvoiceID = order.APInvoiceID;
                        }
                    }

                    entities.APInvoices.Add(order);
                    entities.SaveChanges();

                    var docNumber =
                    new DocNumber
                    {
                        DocumentNumberingID = dbNumber.DocumentNumberingID,
                        NouDocumentNameID = dbNumber.NouDocumentNameID,
                        DocumentNumberingTypeID = dbNumber.DocumentNumberingTypeID,
                        FirstNumber = dbNumber.FirstNumber,
                        LastNumber = dbNumber.LastNumber,
                        CurrentNumber = currentNo,
                        NextNumber = dbNumber.NextNumber,
                        DocName = dbNumber.NouDocumentName,
                        DocType = dbNumber.DocumentNumberingType
                    };

                    // add the attachments
                    foreach (var attachment in sale.Attachments)
                    {
                        var orderAttachment = new APInvoiceAttachment
                        {
                            APInvoiceID = order.APInvoiceID,
                            AttachmentDate = attachment.AttachmentDate,
                            FilePath = attachment.FilePath,
                            FileName = attachment.FileName,
                            File = attachment.File
                        };

                        entities.APInvoiceAttachments.Add(orderAttachment);
                    }

                    // save the details
                    var saleID = order.APInvoiceID;
                    var localInvoiceDetails = new List<ARInvoiceDetail>();

                    foreach (var detail in sale.SaleDetails)
                    {
                        var invoiceDetail = new APInvoiceDetail
                        {
                            APInvoiceID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            QuantityReceived = detail.QuantityReceived,
                            WeightReceived = detail.WeightReceived,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage,
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                            VAT = detail.VAT,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterID = detail.INMasterID,
                            DeductionRate = detail.DeductionRate
                        };

                        entities.APInvoiceDetails.Add(invoiceDetail);
                        entities.SaveChanges();
                    }

                    this.Log.LogDebug(this.GetType(), "New invoice successfully created");
                    return Tuple.Create(order.APInvoiceID, docNumber);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, new DocNumber());
        }

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPInvoice(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbInvoice = entities.APInvoices.FirstOrDefault(x => x.APInvoiceID == sale.SaleID && x.Deleted == null);

                    if (dbInvoice != null)
                    {
                        dbInvoice.DeviceID = NouvemGlobal.DeviceId;
                        dbInvoice.EditDate = DateTime.Now;

                        if (sale.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Cancelling invoice...Base Doc Id:{0}", dbInvoice.BaseDocumentReferenceID.ToInt()));

                            // cancelling invoice
                            dbInvoice.NouDocStatusID = sale.NouDocStatusID;

                            if (dbInvoice.BaseDocumentReferenceID != null)
                            {
                                this.Log.LogDebug(this.GetType(), "Cancelling single invoice...");

                                // single dispath docket used for invoice.
                                var intake =
                                    entities.APGoodsReceipts.FirstOrDefault(
                                        x => x.APGoodsReceiptID == dbInvoice.BaseDocumentReferenceID);

                                if (intake != null)
                                {
                                    intake.Invoiced = null;
                                }
                            }

                            entities.SaveChanges();
                            return true;
                        }

                        dbInvoice.BPMasterID_Supplier = sale.Customer.BPMasterID;
                        dbInvoice.BPContactID_Delivery = sale.DeliveryContact != null ? (int?)sale.DeliveryContact.Details.BPContactID : null;
                        dbInvoice.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                        dbInvoice.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbInvoice.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                        dbInvoice.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                        dbInvoice.RouteID = sale.RouteID;
                        dbInvoice.BaseDocumentReferenceID = sale.BaseDocumentReferenceID;
                        dbInvoice.DocumentDate = sale.DocumentDate;
                        dbInvoice.CreationDate = sale.CreationDate;
                        dbInvoice.DeliveryDate = sale.DeliveryDate;
                        dbInvoice.NouDocStatusID = sale.NouDocStatusID;
                        dbInvoice.Remarks = sale.Remarks;
                        dbInvoice.InvoiceNote = sale.InvoiceNote;
                        dbInvoice.TotalExVAT = sale.TotalExVAT;
                        dbInvoice.DiscountPercentage = sale.DiscountPercentage;
                        dbInvoice.VAT = sale.VAT;
                        dbInvoice.DiscountIncVAT = sale.DiscountIncVAT;
                        dbInvoice.SubTotalExVAT = sale.SubTotalExVAT;
                        dbInvoice.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbInvoice.UserMasterID = sale.SalesEmployeeID;
                        dbInvoice.PORequired = sale.PORequired;
                        dbInvoice.Deleted = sale.Deleted;

                        // if we used a base document to create this, then we mark the base document as closed.
                        if (!dbInvoice.BaseDocumentReferenceID.IsNullOrZero())
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", dbInvoice.BaseDocumentReferenceID));
                            var baseDoc = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID== dbInvoice.BaseDocumentReferenceID);
                            if (baseDoc != null)
                            {
                                baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                                baseDoc.Invoiced = true;
                                this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", dbInvoice.BaseDocumentReferenceID));
                            }
                        }

                        foreach (var attachment in sale.Attachments)
                        {
                            var dbAttachment =
                                    entities.APInvoiceAttachments.FirstOrDefault(
                                        x => x.APInvoiceAttachmentID == attachment.SaleAttachmentID);

                            if (dbAttachment != null)
                            {
                                // updating an existing attachment
                                if (attachment.Deleted != null)
                                {
                                    entities.APInvoiceAttachments.Remove(dbAttachment);
                                }
                                else
                                {
                                    dbAttachment.AttachmentDate = attachment.AttachmentDate;
                                    dbAttachment.FileName = attachment.FileName;
                                    dbAttachment.File = attachment.File;
                                }
                            }
                            else
                            {
                                // adding a new attachment
                                var localAttachment = new APInvoiceAttachment
                                {
                                    AttachmentDate = attachment.AttachmentDate,
                                    APInvoiceID = sale.SaleID,
                                    FileName = attachment.FileName,
                                    File = attachment.File
                                };

                                entities.APInvoiceAttachments.Add(localAttachment);
                            }
                        }

                        var localInvoiceDetails = new List<APInvoiceDetail>();

                        // update the details
                        foreach (var detail in sale.SaleDetails)
                        {
                            var dbInvoiceDetail =
                                entities.APInvoiceDetails.FirstOrDefault(
                                    x => x.APInvoiceDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbInvoiceDetail != null)
                            {
                                // existing sale item
                                dbInvoiceDetail.QuantityOrdered = detail.QuantityOrdered;
                                dbInvoiceDetail.WeightOrdered = detail.WeightOrdered;
                                dbInvoiceDetail.QuantityReceived = detail.QuantityDelivered;
                                dbInvoiceDetail.WeightReceived = detail.WeightDelivered;
                                dbInvoiceDetail.UnitPrice = detail.UnitPrice;
                                dbInvoiceDetail.DiscountPercentage = detail.DiscountPercentage;
                                dbInvoiceDetail.DiscountAmount = detail.DiscountAmount;
                                dbInvoiceDetail.DiscountPrice = detail.DiscountPrice;
                                dbInvoiceDetail.LineDiscountAmount = detail.LineDiscountAmount;
                                dbInvoiceDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                                dbInvoiceDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                                dbInvoiceDetail.VAT = detail.VAT;
                                dbInvoiceDetail.VATCodeID = detail.VatCodeID;
                                dbInvoiceDetail.TotalExclVAT = detail.TotalExVAT;
                                dbInvoiceDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbInvoiceDetail.INMasterID = detail.INMasterID;
                                dbInvoiceDetail.PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID;
                                dbInvoiceDetail.Deleted = detail.Deleted;
                                dbInvoiceDetail.DeductionRate = detail.DeductionRate;
                            }
                            else
                            {
                                // new sale item to add to the order
                                var invoiceDetail = new APInvoiceDetail
                                {
                                    APInvoiceID = sale.SaleID,
                                    QuantityReceived = detail.QuantityDelivered,
                                    WeightReceived = detail.WeightDelivered,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                    INMasterID = detail.INMasterID,
                                    DeductionRate = detail.DeductionRate
                                };

                                localInvoiceDetails.Add(invoiceDetail);
                            }
                        }

                        if (localInvoiceDetails.Any())
                        {
                            entities.APInvoiceDetails.AddRange(localInvoiceDetails);
                        }
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Invoice successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllAPInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end, string dateType)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the invoices");
            var invoices = new List<Sale>();
            end = end.AddDays(1);

            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.APInvoices
                                where invoiceStatuses.Contains(invoice.NouDocStatusID)
                                && invoice.Deleted == null
                                && (invoice.DocumentDate >= start && invoice.DocumentDate <= end)
                                orderby invoice.CreationDate
                                select new Sale
                                {
                                    SaleID = invoice.APInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    CustomerPOReference = invoice.CustomerPOReference,
                                    Number = invoice.Number,
                                    InvoiceNumber = invoice.Number,
                                    IntakeNumber = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == invoice.BaseDocumentReferenceID && x.Deleted == null).Number,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPCustomer = invoice.BPMaster1,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted
                                }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        public IList<Sale> GetAllAPInvoices(int supplierId)
        {
            this.Log.LogDebug(this.GetType(), "Attempting to get all the invoices");
            var invoices = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.APInvoices
                                where invoice.BPMasterID_Supplier == supplierId
                              && invoice.NouDocStatusID != NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                              && invoice.Deleted == null
                                select new Sale
                                {
                                    SaleID = invoice.APInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    CustomerPOReference = invoice.CustomerPOReference,
                                    Number = invoice.Number,
                                    InvoiceNumber = invoice.Number,
                                    IntakeNumber = entities.APGoodsReceipts.FirstOrDefault(x => x.APGoodsReceiptID == invoice.BaseDocumentReferenceID && x.Deleted == null).Number,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPCustomer = invoice.BPMaster1,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted
                                }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }
        

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetAPInvoiceByLastEdit()
        {
            Sale localInvoice = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoice = entities.APInvoices
                      .Where(x => x.DeviceID == NouvemGlobal.DeviceId)
                        .OrderByDescending(x => x.EditDate).FirstOrDefault();

                    if (invoice == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Invoice not found");
                        return null;
                    }

                    localInvoice = new Sale
                    {
                        SaleID = invoice.APInvoiceID,
                        DocumentNumberingID = invoice.DocumentNumberingID,
                        Number = invoice.Number,
                        DeviceId = invoice.DeviceID,
                        EditDate = invoice.EditDate,
                        BPContactID = invoice.BPContactID,
                        RouteID = invoice.RouteID,
                        BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                        DocumentDate = invoice.DocumentDate,
                        CreationDate = invoice.CreationDate,
                        DeliveryDate = invoice.DeliveryDate,
                        Printed = invoice.Printed,
                        NouDocStatusID = invoice.NouDocStatusID,
                        NouDocStatus = invoice.NouDocStatu.Value,
                        Remarks = invoice.Remarks,
                        DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                        InvoiceNote = invoice.InvoiceNote,
                        TotalExVAT = invoice.TotalExVAT,
                        VAT = invoice.VAT,
                        SubTotalExVAT = invoice.SubTotalExVAT,
                        DiscountIncVAT = invoice.DiscountIncVAT,
                        GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                        DiscountPercentage = invoice.DiscountPercentage,
                        OtherReference = invoice.OtherReference,
                        SalesEmployeeID = invoice.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                        InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                        DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                        MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                        BPCustomer = invoice.BPMaster1,
                        PORequired = invoice.PORequired,
                        Deleted = invoice.Deleted,
                        Attachments = (from attachment in invoice.APInvoiceAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.APInvoiceAttachmentID,
                                           SaleAttachmentID = attachment.APInvoiceID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in invoice.APInvoiceDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APInvoiceDetailID,
                                           SaleID = detail.APInvoiceID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityDelivered = detail.QuantityReceived,
                                           WeightDelivered = detail.WeightReceived,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           BPMasterID_Dispatch = detail.BPMasterID_Dispatch,
                                           DeductionRate = detail.DeductionRate,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return localInvoice;
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetAPInvoiceByID(int id)
        {
            Sale localInvoice = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoice = entities.APInvoices.FirstOrDefault(x => x.APInvoiceID == id);
                    if (invoice == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Invoice not found");
                        return null;
                    }

                    localInvoice = new Sale
                    {
                        SaleID = invoice.APInvoiceID,
                        DocumentNumberingID = invoice.DocumentNumberingID,
                        Number = invoice.Number,
                        DeviceId = invoice.DeviceID,
                        EditDate = invoice.EditDate,
                        BPContactID = invoice.BPContactID,
                        RouteID = invoice.RouteID,
                        BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                        DocumentDate = invoice.DocumentDate,
                        CreationDate = invoice.CreationDate,
                        DeliveryDate = invoice.DeliveryDate,
                        Printed = invoice.Printed,
                        NouDocStatusID = invoice.NouDocStatusID,
                        NouDocStatus = invoice.NouDocStatu.Value,
                        Remarks = invoice.Remarks,
                        DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                        InvoiceNote = invoice.InvoiceNote,
                        TotalExVAT = invoice.TotalExVAT,
                        VAT = invoice.VAT,
                        SubTotalExVAT = invoice.SubTotalExVAT,
                        DiscountIncVAT = invoice.DiscountIncVAT,
                        GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                        DiscountPercentage = invoice.DiscountPercentage,
                        OtherReference = invoice.OtherReference,
                        SalesEmployeeID = invoice.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                        InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                        DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                        MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                        BPCustomer = invoice.BPMaster1,
                        PORequired = invoice.PORequired,
                        Deleted = invoice.Deleted,
                        Attachments = (from attachment in invoice.APInvoiceAttachments
                                       where attachment.Deleted == null
                                       select new Attachment
                                       {
                                           AttachmentID = attachment.APInvoiceAttachmentID,
                                           SaleAttachmentID = attachment.APInvoiceID,
                                           AttachmentDate = attachment.AttachmentDate,
                                           FilePath = attachment.FilePath,
                                           FileName = attachment.FileName,
                                           File = attachment.File,
                                           Deleted = attachment.Deleted
                                       }).ToList(),
                        SaleDetails = (from detail in invoice.APInvoiceDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APInvoiceDetailID,
                                           SaleID = detail.APInvoiceID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityDelivered = detail.QuantityReceived,
                                           WeightDelivered = detail.WeightReceived,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           BPMasterID_Dispatch = detail.BPMasterID_Dispatch,
                                           DeductionRate = detail.DeductionRate,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return localInvoice;
        }

        /// <summary>
        /// Determines if an intake has all it's stock on sales invoices.
        /// </summary>
        /// <param name="id">The intake id.</param>
        /// <returns></returns>
        public bool HasIntakeBeenSaleInvoiced(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var result = new System.Data.Entity.Core.Objects.ObjectParameter("Result", typeof(bool));
                    entities.App_HasIntakeBeenSaleInvoiced(id,result);
                    if (result.Value != null)
                    {
                        return result.Value.ToBool();
                    }
                }
             }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves all the non invoiced intake data.
        /// </summary>
        /// <returns>A collection of non invoiced intake data (and associated order details).</returns>
        public IList<Sale> GetNonInvoicedIntakes(bool pricingInvoice = false)
        {
          var orders = new List<Sale>();
          var docStatus = new List<int?>();
          docStatus.Add(NouvemGlobal.NouDocStatusComplete.NouDocStatusID);
          if (pricingInvoice)
            {
                docStatus.Add(NouvemGlobal.NouDocStatusInProgress.NouDocStatusID);
            }

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APGoodsReceipts
                              where order.Deleted == null
                              && docStatus.Contains(order.NouDocStatusID)
                              && order.Invoiced != true
                              orderby order.Number descending
                              select new Sale
                              {
                                  SaleID = order.APGoodsReceiptID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  SaleType = ViewType.APReceipt,
                                  IsBaseDocument = order.Invoiced,
                                  DeviceId = order.DeviceID,
                                  EditDate = order.EditDate,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  Printed = order.Printed,
                                  NouDocStatusID = order.NouDocStatusID,
                                  NouDocStatus = order.NouDocStatu.Value,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  PopUpNote = order.PopUpNote,
                                  VAT = order.VAT,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                                  InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                                  DeliveryContact = new BusinessPartnerContact { Details = order.BPContact },
                                  MainContact = new BusinessPartnerContact { Details = order.BPContact },
                                  BPCustomer = order.BPMasterSupplier,
                                  Deleted = order.Deleted,
                                  SaleDetails = (from detail in order.APGoodsReceiptDetails
                                                 where detail.Deleted == null
                                                 select new SaleDetail
                                                 {
                                                     LoadingSale = true,
                                                     SaleDetailID = detail.APGoodsReceiptDetailID,
                                                     SaleID = detail.APGoodsReceiptID,
                                                     QuantityOrdered = detail.QuantityOrdered,
                                                     WeightOrdered = detail.WeightOrdered,
                                                     QuantityReceived = detail.QuantityReceived,
                                                     WeightReceived = detail.WeightReceived,
                                                     UnitPrice = detail.UnitPrice,
                                                     DiscountPercentage = detail.DiscountPercentage,
                                                     DiscountPrice = detail.DiscountPrice,
                                                     LineDiscountPercentage = detail.LineDiscountPercentage,
                                                     LineDiscountAmount = detail.LineDiscountAmount,
                                                     UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                                     SaleDetailType = ViewType.APReceipt,
                                                     VAT = detail.VAT,
                                                     TotalExVAT = detail.TotalExcVAT,
                                                     TotalIncVAT = detail.TotalIncVAT,
                                                     VatCodeID = detail.VATCodeID,
                                                     INMasterID = detail.INMasterID,
                                                     PriceListID = detail.PriceListID ?? 0,
                                                     NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                                     Deleted = detail.Deleted
                                                 }).ToList(),
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} non invoiced dispatches successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves the apinvoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public IList<Sale> GetAPInvoices()
        {
            this.Log.LogDebug(this.GetType(), "GetNonExportedInvoices(): Attempting to get the non exported invoices");
            var invoices = new List<Sale>();
      
            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.APInvoices
                                where
                                invoice.Deleted == null
                                orderby invoice.APInvoiceID descending
                                select new Sale
                                {
                                    SaleID = invoice.APInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    Number = invoice.Number,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    CustomerPOReference = invoice.CustomerPOReference,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    ExcludeFromPurchaseAccounts = invoice.Exclude,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPCustomer = invoice.BPMaster1,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted
                                }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        #endregion

        #region returns

        /// <summary> Adds a new stock return (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        public Tuple<int, int> AddAPReturn(Sale sale)
        {
            var localStockTransactionId = 0;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = new APReturn
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusActive.NouDocStatusID,
                        Remarks = sale.Remarks,
                        TotalExVAT = sale.TotalExVAT.ToDecimal(),
                        DeliveryDate = sale.DeliveryDate,
                        GoodsReceiptDate = DateTime.Now,
                        VAT = sale.VAT,
                        PopUpNote = sale.PopUpNote,
                        IntakeNote = sale.DocketNote,
                        InvoiceNote = sale.InvoiceNote,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        BPMasterID_Supplier = sale.Customer.BPMasterID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        UserID_CreatedBy = NouvemGlobal.UserId,
                        Reference = sale.Reference
                    };

                    if (sale.DeliveryAddress != null)
                    {
                        order.BPAddressID_Delivery = sale.DeliveryAddress.Details.BPAddressID;
                    }

                    if (sale.InvoiceAddress != null)
                    {
                        order.BPAddressID_Invoice = sale.InvoiceAddress.Details.BPAddressID;
                    }

                    entities.APReturns.Add(order);

                    // pass the newly created apgoodsreceipt id back out.
                    sale.SaleID = order.APReturnID;
                    entities.SaveChanges();

                    // save the details
                    var saleID = order.APReturnID;
                    foreach (var detail in sale.SaleDetails)
                    {
                        if (detail != null)
                        {
                            var orderDetail = new APReturnDetail
                            {
                                APReturnID = saleID,
                                QuantityOrdered = detail.QuantityOrdered,
                                QuantityReceived = detail.QuantityReceived,
                                WeightReceived = detail.WeightReceived,
                                WeightOrdered = detail.WeightOrdered,
                                UnitPrice = detail.UnitPrice,
                                DiscountPercentage = detail.DiscountPercentage,
                                DiscountAmount = detail.DiscountAmount,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                VAT = detail.VAT,
                                Notes = detail.Notes,
                                BatchNumberID = detail.BatchNumberId,
                                VATCodeID = detail.VatCodeID,
                                TotalExcVAT = detail.TotalExVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                INMasterID = detail.INMasterID
                            };

                            entities.APReturnDetails.Add(orderDetail);
                            entities.SaveChanges();

                            // pass the apgoodsreceiptdetail id back out
                            detail.SaleDetailID = orderDetail.APReturnDetailID;

                            #region stock transaction/traceability

                            var stockDetail = detail.StockDetailToProcess;
                            if (stockDetail != null)
                            {
                                if (stockDetail.Transaction != null)
                                {
                                    // scanned stock
                                    stockDetail.Transaction.MasterTableID = detail.SaleDetailID;
                                    entities.StockTransactions.Add(stockDetail.Transaction);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    var localAttribute = this.CreateAttribute(stockDetail);
                                    entities.Attributes.Add(localAttribute);
                                    entities.SaveChanges();

                                    if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                    {
                                        foreach (var nonStandardResponse in sale.NonStandardResponses)
                                        {
                                            nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                            entities.Attributes.Add(nonStandardResponse);
                                            entities.SaveChanges();
                                        }
                                    }

                                    var localTransaction = this.CreateTransaction(stockDetail);
                                    if (localTransaction.TransactionQTY.IsNullOrZero())
                                    {
                                        localTransaction.TransactionQTY = 1;
                                    }

                                    localTransaction.MasterTableID = orderDetail.APReturnDetailID;
                                    localTransaction.Comments = stockDetail.Comments;
                                    localTransaction.NouTransactionTypeID = 9;
                                    localTransaction.AttributeID = localAttribute.AttributeID;

                                    if (stockDetail.BatchAttribute != null)
                                    {
                                        var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                        batchAttribute.IsBatch = true;
                                        var localBatch =
                                            entities.BatchNumbers.FirstOrDefault(
                                                x => x.BatchNumberID == localTransaction.BatchNumberID);

                                        if (localBatch != null)
                                        {
                                            if (localBatch.AttributeID == null)
                                            {
                                                entities.Attributes.Add(batchAttribute);
                                                entities.SaveChanges();
                                                localBatch.AttributeID = batchAttribute.AttributeID;
                                            }
                                            else
                                            {
                                                #region batch attribute update

                                                var attribute = entities.Attributes.FirstOrDefault(x => x.AttributeID == localBatch.AttributeID);
                                                if (attribute != null)
                                                {
                                                    this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                }

                                                #endregion
                                            }
                                        }
                                    }

                                    entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                        localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                        localTransaction.TransactionWeight, localTransaction.WarehouseID, localTransaction.NouTransactionTypeID);
                                    entities.StockTransactions.Add(localTransaction);
                                    entities.SaveChanges();
                                    localStockTransactionId = localTransaction.StockTransactionID;
                                    localTransaction.Serial = localTransaction.StockTransactionID;
                                    entities.SaveChanges();
                                }
                            }

                            #endregion
                        }
                    }

                    return Tuple.Create(order.APReturnID, localStockTransactionId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, 0);
        }

        /// <summary>
        /// Updates an existing return (receipt and details).
        /// </summary>
        /// <param name="sale">The return receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPReturn(Sale sale)
        {
            var batchNoId = 0;
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbOrder =
                        entities.APReturns.FirstOrDefault(x => x.APReturnID == sale.SaleID && x.Deleted == null);

                    if (dbOrder != null)
                    {
                        dbOrder.TotalExVAT = sale.TotalExVAT.ToDecimal();
                        dbOrder.VAT = sale.VAT;
                        dbOrder.DiscountIncVAT = sale.DiscountIncVAT;
                        dbOrder.SubTotalExVAT = sale.SubTotalExVAT;
                        dbOrder.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbOrder.DeviceID = NouvemGlobal.DeviceId;
                        dbOrder.EditDate = DateTime.Today;
                        dbOrder.Reference = sale.Reference;
                        dbOrder.PopUpNote = sale.PopUpNote;
                        dbOrder.NouDocStatusID = sale.IsOrderFilled()
                            ? NouvemGlobal.NouDocStatusFilled.NouDocStatusID
                            : NouvemGlobal.NouDocStatusInProgress.NouDocStatusID;

                        if (!ApplicationSettings.TouchScreenMode)
                        {
                            //dbOrder.BPMasterID_Supplier = sale.Customer.BPMasterID;
                            dbOrder.BPContactID = sale.MainContact != null
                                ? (int?)sale.MainContact.Details.BPContactID
                                : null;
                            dbOrder.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                            dbOrder.BPAddressID_Delivery = sale.DeliveryAddress != null
                                ? (int?)sale.DeliveryAddress.Details.BPAddressID
                                : null;
                            dbOrder.BPAddressID_Invoice = sale.InvoiceAddress != null
                                ? (int?)sale.InvoiceAddress.Details.BPAddressID
                                : null;
                            dbOrder.DocumentDate = sale.DocumentDate;
                            dbOrder.CreationDate = sale.CreationDate;
                            dbOrder.DiscountPercentage = sale.DiscountPercentage;
                            dbOrder.Remarks = sale.Remarks;
                            dbOrder.InvoiceNote = sale.InvoiceNote;
                            dbOrder.IntakeNote = sale.DocketNote;
                            dbOrder.DeliveryDate = sale.DeliveryDate;
                            dbOrder.DeliveryDate = sale.DeliveryDate;
                            dbOrder.UserMasterID = sale.SalesEmployeeID;
                            dbOrder.Deleted = sale.Deleted;
                        }

                        var detail = sale.SaleDetails.FirstOrDefault(x => x.Process);
                        if (detail.SaleDetailID != 0)
                        {
                            // existing sale item
                            var dbOrderDetail =
                                entities.APReturnDetails.FirstOrDefault(
                                    x => x.APReturnDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbOrderDetail == null)
                            {
                                // serious error - should never happen
                                this.Log.LogError(this.GetType(),
                                    string.Format("APReceiptDetail:{0} not found in the database",
                                        detail.SaleDetailID));
                                return false;
                            }

                            dbOrderDetail.QuantityReceived = detail.QuantityReceived;
                            dbOrderDetail.WeightReceived = detail.WeightReceived;
                            dbOrderDetail.BatchNumberID = detail.BatchNumberId;

                            #region stock transaction/traceability

                            var stockDetail = detail.StockDetailToProcess;
                            if (stockDetail != null)
                            {
                                if (stockDetail.Transaction != null)
                                {
                                    // scanned stock
                                    stockDetail.Transaction.MasterTableID = detail.SaleDetailID;
                                    entities.StockTransactions.Add(stockDetail.Transaction);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    var localAttribute = this.CreateAttribute(stockDetail);
                                    entities.Attributes.Add(localAttribute);
                                    entities.SaveChanges();

                                    if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                    {
                                        foreach (var nonStandardResponse in sale.NonStandardResponses)
                                        {
                                            nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                            entities.Attributes.Add(nonStandardResponse);
                                            entities.SaveChanges();
                                        }
                                    }

                                    var localTransaction = this.CreateTransaction(stockDetail);
                                    localTransaction.MasterTableID = dbOrderDetail.APReturnDetailID;
                                    if (localTransaction.TransactionQTY.IsNullOrZero())
                                    {
                                        localTransaction.TransactionQTY = 1;
                                    }

                                    localTransaction.Comments = stockDetail.Comments;
                                    localTransaction.NouTransactionTypeID = 9;
                                    localTransaction.AttributeID = localAttribute.AttributeID;

                                    if (stockDetail.BatchAttribute != null)
                                    {
                                        var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                        batchAttribute.IsBatch = true;
                                        var localBatch =
                                            entities.BatchNumbers.FirstOrDefault(
                                                x => x.BatchNumberID == localTransaction.BatchNumberID);

                                        if (localBatch != null)
                                        {
                                            if (localBatch.AttributeID == null)
                                            {
                                                entities.Attributes.Add(batchAttribute);
                                                entities.SaveChanges();
                                                localBatch.AttributeID = batchAttribute.AttributeID;
                                            }
                                            else
                                            {
                                                #region batch attribute update

                                                var attribute = entities.Attributes.FirstOrDefault(x =>
                                                    x.AttributeID == localBatch.AttributeID);
                                                if (attribute != null)
                                                {
                                                    this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                }

                                                #endregion
                                            }
                                        }
                                    }

                                    entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                        localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                        localTransaction.TransactionWeight, localTransaction.WarehouseID,
                                        localTransaction.NouTransactionTypeID);

                                    entities.StockTransactions.Add(localTransaction);
                                    entities.SaveChanges();
                                    sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;
                                    localTransaction.Serial = localTransaction.StockTransactionID;
                                    entities.SaveChanges();
                                }
                            }

                            #endregion

                            entities.SaveChanges();
                        }
                        else
                        {
                            var orderDetail = new APReturnDetail
                            {
                                APReturnID = dbOrder.APReturnID,
                                QuantityOrdered = detail.QuantityOrdered,
                                QuantityReceived = detail.QuantityReceived,
                                WeightOrdered = detail.WeightOrdered,
                                WeightReceived = detail.WeightReceived,
                                UnitPrice = detail.UnitPrice,
                                BatchNumberID = detail.BatchNumberId,
                                DiscountPercentage = detail.DiscountPercentage,
                                DiscountAmount = detail.DiscountAmount,
                                DiscountPrice = detail.DiscountPrice,
                                LineDiscountAmount = detail.LineDiscountAmount,
                                LineDiscountPercentage = detail.LineDiscountPercentage.ToDecimal(),
                                UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                VAT = detail.VAT,
                                Notes = detail.Notes,
                                VATCodeID = detail.VatCodeID,
                                TotalExcVAT = detail.TotalExVAT,
                                TotalIncVAT = detail.TotalIncVAT,
                                PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                INMasterID = detail.INMasterID
                            };

                            entities.APReturnDetails.Add(orderDetail);
                            entities.SaveChanges();

                            // pass the apgoodsreceiptdetail id back out
                            detail.SaleDetailID = orderDetail.APReturnDetailID;

                            #region stock transaction/traceability

                            var stockDetail = detail.StockDetailToProcess;
                            if (stockDetail != null)
                            {
                                if (stockDetail.Transaction != null)
                                {
                                    // scanned stock
                                    stockDetail.Transaction.MasterTableID = detail.SaleDetailID;
                                    entities.StockTransactions.Add(stockDetail.Transaction);
                                    entities.SaveChanges();
                                }
                                else
                                {
                                    var localAttribute = this.CreateAttribute(stockDetail);
                                    entities.Attributes.Add(localAttribute);
                                    entities.SaveChanges();

                                    if (sale.NonStandardResponses != null && sale.NonStandardResponses.Any())
                                    {
                                        foreach (var nonStandardResponse in sale.NonStandardResponses)
                                        {
                                            nonStandardResponse.AttributeID_Parent = localAttribute.AttributeID;
                                            entities.Attributes.Add(nonStandardResponse);
                                            entities.SaveChanges();
                                        }
                                    }

                                    var localTransaction = this.CreateTransaction(stockDetail);
                                    localTransaction.MasterTableID = orderDetail.APReturnDetailID;
                                    localTransaction.NouTransactionTypeID = 9;
                                    localTransaction.AttributeID = localAttribute.AttributeID;
                                    localTransaction.Comments = stockDetail.Comments;

                                    if (stockDetail.BatchAttribute != null)
                                    {
                                        var batchAttribute = this.CreateAttribute(stockDetail.BatchAttribute);
                                        batchAttribute.IsBatch = true;
                                        var localBatch =
                                            entities.BatchNumbers.FirstOrDefault(
                                                x => x.BatchNumberID == localTransaction.BatchNumberID);

                                        if (localBatch != null)
                                        {
                                            if (localBatch.AttributeID == null)
                                            {
                                                entities.Attributes.Add(batchAttribute);
                                                entities.SaveChanges();
                                                localBatch.AttributeID = batchAttribute.AttributeID;
                                            }
                                            else
                                            {
                                                #region batch attribute update

                                                var attribute = entities.Attributes.FirstOrDefault(x =>
                                                    x.AttributeID == localBatch.AttributeID);
                                                if (attribute != null)
                                                {
                                                    this.UpdateAttribute(attribute, stockDetail.BatchAttribute);
                                                }

                                                #endregion
                                            }
                                        }
                                    }

                                    entities.App_UpdateProductStockData(localTransaction.INMasterID,
                                        localTransaction.BatchNumberID, localTransaction.TransactionQTY,
                                        localTransaction.TransactionWeight, localTransaction.WarehouseID,
                                        localTransaction.NouTransactionTypeID);

                                    entities.StockTransactions.Add(localTransaction);
                                    entities.SaveChanges();
                                    sale.ReturnedStockTransactionId = localTransaction.StockTransactionID;
                                    localTransaction.Serial = localTransaction.StockTransactionID;
                                    entities.SaveChanges();
                                }
                            }

                            #endregion
                        }

                    }

                    return true;
                }

            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the returns (and associated details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        public IList<App_GetReturnDetails_Result> GetReturnsStatusDetails(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.App_GetReturnDetails(id).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAPReturns(IList<int?> orderStatuses)
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceipts(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APReturns
                              where order.Deleted == null
                              && orderStatuses.Contains(order.NouDocStatusID)
                              orderby order.CreationDate
                              select new Sale
                              {
                                  SaleID = order.APReturnID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPMasterID = order.BPMasterID_Supplier,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeviceId = order.DeviceID,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  PopUpNote = order.PopUpNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  VAT = order.VAT,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  TechnicalNotes = order.Notes,
                                  UserIDModified = order.UserMasterID,
                                  Reference = order.Reference,
                                  Address = entities.BPAddresses.FirstOrDefault(x => x.BPAddressID == order.BPAddressID_Delivery),
                                  BPCustomer = entities.BPMasters.FirstOrDefault(x => x.BPMasterID == order.BPMasterID_Supplier),
                                  UserIDCreation = order.UserID_CreatedBy,
                                  UserIDCompletion = order.UserID_CompletedBy,
                                  Deleted = order.Deleted,
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        public IList<Sale> GetAPReturnsNonInvoiced()
        {
            this.Log.LogDebug(this.GetType(), "GetAllAPReceipts(): Attempting to get all the goods received");
            var orders = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    orders = (from order in entities.APReturns
                              where order.Deleted == null
                              && order.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID
                              && order.Invoiced != true
                              orderby order.Number descending
                              select new Sale
                              {
                                  SaleID = order.APReturnID,
                                  DocumentNumberingID = order.DocumentNumberingID,
                                  Number = order.Number,
                                  BPMasterID = order.BPMasterID_Supplier,
                                  BPContactID = order.BPContactID,
                                  DocumentDate = order.DocumentDate,
                                  CreationDate = order.CreationDate,
                                  DeviceId = order.DeviceID,
                                  InvoiceNote = order.InvoiceNote,
                                  DocketNote = order.IntakeNote,
                                  EditDate = order.EditDate,
                                  Printed = order.Printed,
                                  PopUpNote = order.PopUpNote,
                                  NouDocStatusID = order.NouDocStatusID,
                                  Remarks = order.Remarks,
                                  TotalExVAT = order.TotalExVAT,
                                  DeliveryDate = order.DeliveryDate,
                                  VAT = order.VAT,
                                  BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                                  SubTotalExVAT = order.SubTotalExVAT,
                                  DiscountIncVAT = order.DiscountIncVAT,
                                  GrandTotalIncVAT = order.GrandTotalIncVAT,
                                  DiscountPercentage = order.DiscountPercentage,
                                  SalesEmployeeID = order.UserMasterID,
                                  TechnicalNotes = order.Notes,
                                  UserIDModified = order.UserMasterID,
                                  Reference = order.Reference,
                                  Address = entities.BPAddresses.FirstOrDefault(x => x.BPAddressID == order.BPAddressID_Delivery),
                                  BPCustomer = entities.BPMasters.FirstOrDefault(x => x.BPMasterID == order.BPMasterID_Supplier),
                                  UserIDCreation = order.UserID_CreatedBy,
                                  UserIDCompletion = order.UserID_CompletedBy,
                                  Deleted = order.Deleted,
                              }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} order receipts successfully retrieved", orders.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return orders;
        }

        /// <summary>
        /// Gets the return details.
        /// </summary>
        /// <param name="id">The returns id to search for.</param>
        /// <returns>A return, and it's details.</returns>
        public Sale GetAPReturnById(int id)
        {
            var dbOrder = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APReturns.FirstOrDefault(x => x.APReturnID == id);
                    if (order == null)
                    {
                        return dbOrder;
                    }

                    dbOrder = new Sale
                    {
                        SaleID = order.APReturnID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        BPMasterID = order.BPMasterID_Supplier,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        SaleType = ViewType.APReceipt,
                        DeviceId = order.DeviceID,
                        PopUpNote = order.PopUpNote,
                        InvoiceNote = order.InvoiceNote,
                        DocketNote = order.IntakeNote,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        //NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        DeliveryDate = order.DeliveryDate,
                        VAT = order.VAT,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        //DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        //InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        //MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        //BPHaulier = order.BPMasterHaulier,

                        Reference = order.Reference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.APReturnDetails.Where(x => x.Deleted == null)
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APReturnDetailID,
                                           SaleID = detail.APReturnID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           //Batch = detail.BatchNumber,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           SaleDetailType = ViewType.APReceipt,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            dbOrder.BPCustomer = new BPMaster { BPMasterID = dbOrder.BPMasterID.ToInt() };
            return dbOrder;
        }

        /// <summary>
        /// Gets the return details by last edit date.
        /// </summary>
        /// <returns>A return, and it's details.</returns>
        public Sale GetAPReturnByLastEdit()
        {
            var dbOrder = new Sale();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APReturns.OrderByDescending(x => x.EditDate).FirstOrDefault();
                    if (order == null)
                    {
                        return dbOrder;
                    }

                    dbOrder = new Sale
                    {
                        SaleID = order.APReturnID,
                        DocumentNumberingID = order.DocumentNumberingID,
                        Number = order.Number,
                        BPContactID = order.BPContactID,
                        BPMasterID = order.BPMasterID_Supplier,
                        DocumentDate = order.DocumentDate,
                        CreationDate = order.CreationDate,
                        SaleType = ViewType.APReceipt,
                        DeviceId = order.DeviceID,
                        PopUpNote = order.PopUpNote,
                        InvoiceNote = order.InvoiceNote,
                        DocketNote = order.IntakeNote,
                        Printed = order.Printed,
                        NouDocStatusID = order.NouDocStatusID,
                        //NouDocStatus = order.NouDocStatu.Value,
                        Remarks = order.Remarks,
                        TotalExVAT = order.TotalExVAT,
                        DeliveryDate = order.DeliveryDate,
                        VAT = order.VAT,
                        BaseDocumentReferenceID = order.BaseDocumentReferenceID,
                        SubTotalExVAT = order.SubTotalExVAT,
                        DiscountIncVAT = order.DiscountIncVAT,
                        GrandTotalIncVAT = order.GrandTotalIncVAT,
                        DiscountPercentage = order.DiscountPercentage,
                        SalesEmployeeID = order.UserMasterID,
                        //DeliveryAddress = new BusinessPartnerAddress { Details = order.BPAddressDelivery },
                        //InvoiceAddress = new BusinessPartnerAddress { Details = order.BPAddressInvoice },
                        //MainContact = new BusinessPartnerContact { Details = order.BPContact },
                        //BPHaulier = order.BPMasterHaulier,

                        Reference = order.Reference,
                        Deleted = order.Deleted,
                        SaleDetails = (from detail in order.APReturnDetails.Where(x => x.Deleted == null)
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APReturnDetailID,
                                           SaleID = detail.APReturnID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityReceived = detail.QuantityReceived,
                                           WeightReceived = detail.WeightReceived,
                                           //Batch = detail.BatchNumber,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           SaleDetailType = ViewType.APReceipt,
                                           Notes = detail.Notes,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExcVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            dbOrder.BPCustomer = new BPMaster { BPMasterID = dbOrder.BPMasterID.ToInt() };
            return dbOrder;
        }

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="complete">The complete order flag.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        public bool UpdateAPReturnStatus(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var order = entities.APReturns.FirstOrDefault(x => x.APReturnID == sale.SaleID);
                    if (order != null)
                    {
                        order.NouDocStatusID = sale.NouDocStatusID;
                        order.DeviceID = NouvemGlobal.DeviceId;
                        order.EditDate = DateTime.Now;

                        if (order.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID)
                        {
                            order.UserID_CompletedBy = NouvemGlobal.UserId;
                        }

                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        public bool PriceReturnsDocket(int docketId)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    entities.App_PriceReturnsDocket(docketId);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        public Tuple<int, DocNumber> AddAPReturnInvoice(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    if (!sale.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        var alreadyInvoiced = entities.APReturnInvoices.Any(x =>
                            x.BaseDocumentReferenceID == sale.BaseDocumentReferenceID && x.Deleted == null &&
                            x.NouDocStatusID != NouvemGlobal.NouDocStatusCancelled.NouDocStatusID);

                        if (alreadyInvoiced)
                        {
                            return Tuple.Create(0, new DocNumber { DocumentNumberingTypeID = 1000 });
                        }
                    }

                    var dbNumber =
                        entities.DocumentNumberings.First(
                            x =>
                                x.NouDocumentName.DocumentName.Equals(Constant.APReturnInvoice) &&
                                x.DocumentNumberingType.Name.Equals(Constant.Standard));

                    sale.Number = dbNumber.NextNumber;
                    sale.DocumentNumberingID = dbNumber.DocumentNumberingID;

                    var currentNo = dbNumber.NextNumber;
                    var nextNo = dbNumber.NextNumber + 1;
                    if (nextNo > dbNumber.LastNumber)
                    {
                        // Last number reached, so reset back to first number.
                        nextNo = dbNumber.FirstNumber;
                    }

                    dbNumber.NextNumber = nextNo;
                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Document number updated");

                    var order = new APReturnInvoice
                    {
                        DocumentNumberingID = sale.DocumentNumberingID,
                        Number = sale.Number,
                        BPContactID = sale.MainContact != null && sale.MainContact.Details != null ? (int?)sale.MainContact.Details.BPContactID : null,
                        BPContactID_Delivery = sale.DeliveryContact != null && sale.DeliveryContact.Details != null ? (int?)sale.DeliveryContact.Details.BPContactID : null,
                        BPMasterID_Supplier = sale.BPCustomer != null ? sale.BPCustomer.BPMasterID : sale.Customer != null ? sale.Customer.BPMasterID : (int?)null,
                        BPMasterID_Haulier = sale.BPHaulier != null ? sale.BPHaulier.BPMasterID : (int?)null,
                        BPAddressID_Delivery = sale.DeliveryAddress != null && sale.DeliveryAddress.Details != null ? sale.DeliveryAddress.Details.BPAddressID : (int?)null,
                        BPAddressID_Invoice = sale.InvoiceAddress != null && sale.InvoiceAddress.Details != null ? sale.InvoiceAddress.Details.BPAddressID : (int?)null,
                        RouteID = sale.RouteID,
                        DeviceID = NouvemGlobal.DeviceId,
                        EditDate = DateTime.Now,
                        DocumentDate = sale.DocumentDate,
                        CreationDate = sale.CreationDate,
                        DeliveryDate = sale.DeliveryDate,
                        NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID,
                        CustomerPOReference = sale.CustomerPOReference,
                        Remarks = sale.Remarks,
                        InvoiceNote = sale.InvoiceNote,
                        TotalExVAT = sale.TotalExVAT,
                        VAT = sale.VAT,
                        DiscountIncVAT = sale.DiscountIncVAT,
                        SubTotalExVAT = sale.SubTotalExVAT,
                        GrandTotalIncVAT = sale.GrandTotalIncVAT,
                        DiscountPercentage = sale.DiscountPercentage,
                        UserMasterID = sale.SalesEmployeeID,
                        BaseDocumentReferenceID = sale.BaseDocumentReferenceID,
                        PORequired = sale.PORequired
                    };

                    // if we used a base document to create this, then we mark the base document as closed.
                    if (!order.BaseDocumentReferenceID.IsNullOrZero())
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", order.BaseDocumentReferenceID));
                        var baseDoc = entities.APReturns.FirstOrDefault(x => x.APReturnID == order.BaseDocumentReferenceID);
                        if (baseDoc != null)
                        {
                            if (baseDoc.Invoiced == true)
                            {
                                // already invoiced, return;
                                return Tuple.Create(0, new DocNumber());
                            }

                            baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                            baseDoc.Invoiced = true;
                            this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", order.BaseDocumentReferenceID));
                        }
                    }
                    //else if (sale.IdsToMarkAsInvoiced != null && sale.IdsToMarkAsInvoiced.Any())
                    //{
                    //    // Consolidated invoice created, so we mark all the associated intake dockets as invoiced.
                    //    var returnsToInvoice =
                    //        entities.APReturns.Where(
                    //            x => sale.IdsToMarkAsInvoiced.Contains(x.APReturnID) && x.Invoiced != true);

                    //    foreach (var docket in returnsToInvoice)
                    //    {
                    //        docket.Invoiced = true;
                    //        docket.APInvoiceID = order.APInvoiceID;
                    //    }
                    //}

                    entities.APReturnInvoices.Add(order);
                    entities.SaveChanges();

                    var docNumber =
                    new DocNumber
                    {
                        DocumentNumberingID = dbNumber.DocumentNumberingID,
                        NouDocumentNameID = dbNumber.NouDocumentNameID,
                        DocumentNumberingTypeID = dbNumber.DocumentNumberingTypeID,
                        FirstNumber = dbNumber.FirstNumber,
                        LastNumber = dbNumber.LastNumber,
                        CurrentNumber = currentNo,
                        NextNumber = dbNumber.NextNumber,
                        DocName = dbNumber.NouDocumentName,
                        DocType = dbNumber.DocumentNumberingType
                    };

                    // save the details
                    var saleID = order.APReturnInvoiceID;
                    var localInvoiceDetails = new List<APReturnInvoiceDetail>();

                    foreach (var detail in sale.SaleDetails)
                    {
                        var invoiceDetail = new APReturnInvoiceDetail
                        {
                            APReturnInvoiceID = saleID,
                            QuantityOrdered = detail.QuantityOrdered,
                            WeightOrdered = detail.WeightOrdered,
                            QuantityReceived = detail.QuantityReceived,
                            WeightReceived = detail.WeightReceived,
                            UnitPrice = detail.UnitPrice,
                            DiscountPercentage = detail.DiscountPercentage,
                            DiscountAmount = detail.DiscountAmount,
                            DiscountPrice = detail.DiscountPrice,
                            LineDiscountAmount = detail.LineDiscountAmount,
                            LineDiscountPercentage = detail.LineDiscountPercentage,
                            UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                            VAT = detail.VAT,
                            VATCodeID = detail.VatCodeID,
                            TotalExclVAT = detail.TotalExVAT,
                            TotalIncVAT = detail.TotalIncVAT,
                            PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                            INMasterID = detail.INMasterID
                        };

                        entities.APReturnInvoiceDetails.Add(invoiceDetail);
                        entities.SaveChanges();
                    }

                    this.Log.LogDebug(this.GetType(), "New invoice successfully created");
                    return Tuple.Create(order.APReturnInvoiceID, docNumber);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(0, new DocNumber());
        }

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        public bool UpdateAPReturnInvoice(Sale sale)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbInvoice = entities.APReturnInvoices.FirstOrDefault(x => x.APReturnInvoiceID == sale.SaleID && x.Deleted == null);

                    if (dbInvoice != null)
                    {
                        dbInvoice.DeviceID = NouvemGlobal.DeviceId;
                        dbInvoice.EditDate = DateTime.Now;

                        if (sale.NouDocStatusID == NouvemGlobal.NouDocStatusCancelled.NouDocStatusID)
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Cancelling invoice...Base Doc Id:{0}", dbInvoice.BaseDocumentReferenceID.ToInt()));

                            // cancelling invoice
                            dbInvoice.NouDocStatusID = sale.NouDocStatusID;

                            if (dbInvoice.BaseDocumentReferenceID != null)
                            {
                                this.Log.LogDebug(this.GetType(), "Cancelling single invoice...");

                                // single dispath docket used for invoice.
                                var intake =
                                    entities.APGoodsReceipts.FirstOrDefault(
                                        x => x.APGoodsReceiptID == dbInvoice.BaseDocumentReferenceID);

                                if (intake != null)
                                {
                                    intake.Invoiced = null;
                                }
                            }

                            entities.SaveChanges();
                            return true;
                        }

                        dbInvoice.BPMasterID_Supplier = sale.Customer.BPMasterID;
                        dbInvoice.BPContactID_Delivery = sale.DeliveryContact != null ? (int?)sale.DeliveryContact.Details.BPContactID : null;
                        dbInvoice.BPContactID = sale.MainContact != null ? (int?)sale.MainContact.Details.BPContactID : null;
                        dbInvoice.BPMasterID_Haulier = sale.Haulier != null ? (int?)sale.Haulier.BPMasterID : null;
                        dbInvoice.BPAddressID_Delivery = sale.DeliveryAddress != null ? (int?)sale.DeliveryAddress.Details.BPAddressID : null;
                        dbInvoice.BPAddressID_Invoice = sale.InvoiceAddress != null ? (int?)sale.InvoiceAddress.Details.BPAddressID : null;
                        dbInvoice.RouteID = sale.RouteID;
                        dbInvoice.BaseDocumentReferenceID = sale.BaseDocumentReferenceID;
                        dbInvoice.DocumentDate = sale.DocumentDate;
                        dbInvoice.CreationDate = sale.CreationDate;
                        dbInvoice.DeliveryDate = sale.DeliveryDate;
                        dbInvoice.NouDocStatusID = sale.NouDocStatusID;
                        dbInvoice.Remarks = sale.Remarks;
                        dbInvoice.InvoiceNote = sale.InvoiceNote;
                        dbInvoice.TotalExVAT = sale.TotalExVAT;
                        dbInvoice.DiscountPercentage = sale.DiscountPercentage;
                        dbInvoice.VAT = sale.VAT;
                        dbInvoice.DiscountIncVAT = sale.DiscountIncVAT;
                        dbInvoice.SubTotalExVAT = sale.SubTotalExVAT;
                        dbInvoice.GrandTotalIncVAT = sale.GrandTotalIncVAT;
                        dbInvoice.UserMasterID = sale.SalesEmployeeID;
                        dbInvoice.PORequired = sale.PORequired;
                        dbInvoice.Deleted = sale.Deleted;

                        // if we used a base document to create this, then we mark the base document as closed.
                        if (!dbInvoice.BaseDocumentReferenceID.IsNullOrZero())
                        {
                            this.Log.LogDebug(this.GetType(), string.Format("Attempting to mark base quote id:{0} as closed", dbInvoice.BaseDocumentReferenceID));
                            var baseDoc = entities.APReturns.FirstOrDefault(x => x.APReturnID == dbInvoice.BaseDocumentReferenceID);
                            if (baseDoc != null)
                            {
                                baseDoc.NouDocStatusID = NouvemGlobal.NouDocStatusComplete.NouDocStatusID;
                                baseDoc.Invoiced = true;
                                this.Log.LogDebug(this.GetType(), string.Format("Base dispatch id:{0} marked as closed", dbInvoice.BaseDocumentReferenceID));
                            }
                        }

                        var localInvoiceDetails = new List<APReturnInvoiceDetail>();

                        // update the details
                        foreach (var detail in sale.SaleDetails)
                        {
                            var dbInvoiceDetail =
                                entities.APReturnInvoiceDetails.FirstOrDefault(
                                    x => x.APReturnInvoiceDetailID == detail.SaleDetailID && x.Deleted == null);

                            if (dbInvoiceDetail != null)
                            {
                                // existing sale item
                                dbInvoiceDetail.QuantityOrdered = detail.QuantityOrdered;
                                dbInvoiceDetail.WeightOrdered = detail.WeightOrdered;
                                dbInvoiceDetail.QuantityReceived = detail.QuantityDelivered;
                                dbInvoiceDetail.WeightReceived = detail.WeightDelivered;
                                dbInvoiceDetail.UnitPrice = detail.UnitPrice;
                                dbInvoiceDetail.DiscountPercentage = detail.DiscountPercentage;
                                dbInvoiceDetail.DiscountAmount = detail.DiscountAmount;
                                dbInvoiceDetail.DiscountPrice = detail.DiscountPrice;
                                dbInvoiceDetail.LineDiscountAmount = detail.LineDiscountAmount;
                                dbInvoiceDetail.LineDiscountPercentage = detail.LineDiscountPercentage;
                                dbInvoiceDetail.UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount;
                                dbInvoiceDetail.VAT = detail.VAT;
                                dbInvoiceDetail.VATCodeID = detail.VatCodeID;
                                dbInvoiceDetail.TotalExclVAT = detail.TotalExVAT;
                                dbInvoiceDetail.TotalIncVAT = detail.TotalIncVAT;
                                dbInvoiceDetail.INMasterID = detail.INMasterID;
                                dbInvoiceDetail.PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID;
                                dbInvoiceDetail.Deleted = detail.Deleted;
                            }
                            else
                            {
                                // new sale item to add to the order
                                var invoiceDetail = new APReturnInvoiceDetail
                                {
                                    APReturnInvoiceID = sale.SaleID,
                                    QuantityReceived = detail.QuantityReceived,
                                    WeightReceived = detail.WeightReceived,
                                    UnitPrice = detail.UnitPrice,
                                    DiscountPercentage = detail.DiscountPercentage,
                                    DiscountAmount = detail.DiscountAmount,
                                    DiscountPrice = detail.DiscountPrice,
                                    LineDiscountAmount = detail.LineDiscountAmount,
                                    LineDiscountPercentage = detail.LineDiscountPercentage,
                                    UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                    VAT = detail.VAT,
                                    VATCodeID = detail.VatCodeID,
                                    TotalExclVAT = detail.TotalExVAT,
                                    TotalIncVAT = detail.TotalIncVAT,
                                    PriceListID = detail.PriceListID == 0 ? (int?)null : detail.PriceListID,
                                    INMasterID = detail.INMasterID
                                };

                                localInvoiceDetails.Add(invoiceDetail);
                            }
                        }

                        if (localInvoiceDetails.Any())
                        {
                            entities.APReturnInvoiceDetails.AddRange(localInvoiceDetails);
                        }
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Invoice successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the ap return invoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        public IList<Sale> GetAPReturnInvoices()
        {
            this.Log.LogDebug(this.GetType(), "GetNonExportedInvoices(): Attempting to get the non exported invoices");
            var invoices = new List<Sale>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    invoices = (from invoice in entities.APReturnInvoices
                                where
                                invoice.Deleted == null
                                orderby invoice.APReturnInvoiceID descending
                                select new Sale
                                {
                                    SaleID = invoice.APReturnInvoiceID,
                                    DocumentNumberingID = invoice.DocumentNumberingID,
                                    Number = invoice.Number,
                                    BPContactID = invoice.BPContactID,
                                    RouteID = invoice.RouteID,
                                    DeviceId = invoice.DeviceID,
                                    EditDate = invoice.EditDate,
                                    BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                                    CustomerPOReference = invoice.CustomerPOReference,
                                    DocumentDate = invoice.DocumentDate,
                                    CreationDate = invoice.CreationDate,
                                    DeliveryDate = invoice.DeliveryDate,
                                    Printed = invoice.Printed,
                                    NouDocStatusID = invoice.NouDocStatusID,
                                    NouDocStatus = invoice.NouDocStatu.Value,
                                    Remarks = invoice.Remarks,
                                    DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                                    InvoiceNote = invoice.InvoiceNote,
                                    TotalExVAT = invoice.TotalExVAT,
                                    VAT = invoice.VAT,
                                    ExcludeFromPurchaseAccounts = invoice.Exclude,
                                    SubTotalExVAT = invoice.SubTotalExVAT,
                                    DiscountIncVAT = invoice.DiscountIncVAT,
                                    GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                                    DiscountPercentage = invoice.DiscountPercentage,
                                    OtherReference = invoice.OtherReference,
                                    SalesEmployeeID = invoice.UserMasterID,
                                    DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                                    InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                                    DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                                    MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                                    BPCustomer = invoice.BPMaster1,
                                    PORequired = invoice.PORequired,
                                    Deleted = invoice.Deleted
                                }).ToList();
                }

                this.Log.LogDebug(this.GetType(), string.Format("{0} invoices successfully retrieved", invoices.Count));
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return invoices;
        }

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        public Sale GetAPReturnInvoiceByID(int id)
        {
            Sale localInvoice = null;

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var invoice = entities.APReturnInvoices.FirstOrDefault(x => x.APReturnInvoiceID == id);
                    if (invoice == null)
                    {
                        this.Log.LogDebug(this.GetType(), "Invoice not found");
                        return null;
                    }

                    localInvoice = new Sale
                    {
                        SaleID = invoice.APReturnInvoiceID,
                        DocumentNumberingID = invoice.DocumentNumberingID,
                        Number = invoice.Number,
                        DeviceId = invoice.DeviceID,
                        EditDate = invoice.EditDate,
                        BPContactID = invoice.BPContactID,
                        RouteID = invoice.RouteID,
                        BaseDocumentReferenceID = invoice.BaseDocumentReferenceID,
                        DocumentDate = invoice.DocumentDate,
                        CreationDate = invoice.CreationDate,
                        DeliveryDate = invoice.DeliveryDate,
                        Printed = invoice.Printed,
                        NouDocStatusID = invoice.NouDocStatusID,
                        NouDocStatus = invoice.NouDocStatu.Value,
                        Remarks = invoice.Remarks,
                        DispatchNote1PopUp = invoice.InvoiceNotePopUp,
                        InvoiceNote = invoice.InvoiceNote,
                        TotalExVAT = invoice.TotalExVAT,
                        VAT = invoice.VAT,
                        SubTotalExVAT = invoice.SubTotalExVAT,
                        DiscountIncVAT = invoice.DiscountIncVAT,
                        GrandTotalIncVAT = invoice.GrandTotalIncVAT,
                        DiscountPercentage = invoice.DiscountPercentage,
                        OtherReference = invoice.OtherReference,
                        SalesEmployeeID = invoice.UserMasterID,
                        DeliveryAddress = new BusinessPartnerAddress { Details = invoice.BPAddress },
                        InvoiceAddress = new BusinessPartnerAddress { Details = invoice.BPAddress1 },
                        DeliveryContact = new BusinessPartnerContact { Details = invoice.BPContact1 },
                        MainContact = new BusinessPartnerContact { Details = invoice.BPContact },
                        BPCustomer = invoice.BPMaster1,
                        PORequired = invoice.PORequired,
                        Deleted = invoice.Deleted,
                        SaleDetails = (from detail in invoice.APReturnInvoiceDetails
                                       where detail.Deleted == null
                                       select new SaleDetail
                                       {
                                           LoadingSale = true,
                                           SaleDetailID = detail.APReturnInvoiceDetailID,
                                           SaleID = detail.APReturnInvoiceID,
                                           QuantityOrdered = detail.QuantityOrdered,
                                           WeightOrdered = detail.WeightOrdered,
                                           QuantityDelivered = detail.QuantityReceived,
                                           WeightDelivered = detail.WeightReceived,
                                           UnitPrice = detail.UnitPrice,
                                           DiscountPercentage = detail.DiscountPercentage,
                                           DiscountPrice = detail.DiscountPrice,
                                           LineDiscountPercentage = detail.LineDiscountPercentage,
                                           LineDiscountAmount = detail.LineDiscountAmount,
                                           UnitPriceAfterDiscount = detail.UnitPriceAfterDiscount,
                                           VAT = detail.VAT,
                                           TotalExVAT = detail.TotalExclVAT,
                                           TotalIncVAT = detail.TotalIncVAT,
                                           VatCodeID = detail.VATCodeID,
                                           INMasterID = detail.INMasterID,
                                           PriceListID = detail.PriceListID ?? 0,
                                           NouStockMode = detail.INMaster != null ? detail.INMaster.NouStockMode : null,
                                           Deleted = detail.Deleted
                                       }).ToList(),
                    };
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return localInvoice;
        }

        /// <summary>
        /// Exports credit note details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        public bool ExportAPReturnInvoice(IList<int> invoiceIds)
        {
            this.Log.LogDebug(this.GetType(), "ExportInvoices(): Exporting invoices..");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var invoiceId in invoiceIds)
                    {
                        this.Log.LogDebug(this.GetType(), string.Format("Attempting to export invoice id:{0}", invoiceId));
                        var invoice = entities.APReturnInvoices.FirstOrDefault(x => x.APReturnInvoiceID == invoiceId);
                        if (invoice != null)
                        {
                            invoice.NouDocStatusID = NouvemGlobal.NouDocStatusExported.NouDocStatusID;
                            invoice.EditDate = DateTime.Now;
                            this.Log.LogDebug(this.GetType(), "Invoice found and marked for export");
                        }
                        else
                        {
                            this.Log.LogError(this.GetType(), "Invoice not found");
                        }
                    }

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Invoices exported");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion
    }
}
