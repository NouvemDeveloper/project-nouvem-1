﻿// -----------------------------------------------------------------------
// <copyright file="QualityRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Model.DataLayer.Interface;

    public class QualityRepository : NouvemRepositoryBase, IQualityRepository
    {
        /// <summary>
        /// Retrieve all the quality masters.
        /// </summary>
        /// <returns>A collection of quality masters.</returns>
        public IList<ViewQualityMaster> GetQualityMasters()
        {
            this.Log.LogDebug(this.GetType(), "GetQualityMasters(): Attempting to retrieve all the quality masters");
            var qualityMasters = new List<ViewQualityMaster>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    qualityMasters = entities.ViewQualityMasters.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} quality masters retrieved", qualityMasters.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return qualityMasters;
        }

        /// <summary>
        /// Retrieve all the quality template allocations.
        /// </summary>
        /// <returns>A collection of quality template allocations.</returns>
        public IList<ViewQualityTemplateAllocation> GetViewQualityTemplateAllocations()
        {
            this.Log.LogDebug(this.GetType(), "GetViewQualityTemplateAllocations(): Attempting to retrieve all the quality template allocations");
            var allocations = new List<ViewQualityTemplateAllocation>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    allocations = entities.ViewQualityTemplateAllocations.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} quality template allocations retrieved", allocations.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return allocations;
        }

        /// <summary>
        /// Retrieve all the system quality types.
        /// </summary>
        /// <returns>A collection of quality types.</returns>
        public IList<NouQualityType> GetQualityTypes()
        {
            this.Log.LogDebug(this.GetType(), "GetQualityType: Attempting to retrieve all the quality types");
            var qualityTypes = new List<NouQualityType>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    qualityTypes = entities.NouQualityTypes.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} quality types retrieved", qualityTypes.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return qualityTypes;
        }

        /// <summary>
        /// Adds or upqualitys quality masters.
        /// </summary>
        /// <param name="qualityMasters">The quality masters to add or upquality.</param>
        /// <returns>A flag, indicating a succcessful add or upquality.</returns>
        public bool AddOrUpdateQualityMasters(IList<ViewQualityMaster> qualityMasters)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpqualityQualityMasters(): Attempting to upquality the quality masters");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    qualityMasters.ToList().ForEach(x =>
                    {
                        if (x.QualityMasterID == 0)
                        {
                            var qualityMaster = new QualityMaster
                            {
                                NouQualityTypeID = x.NouQualityTypeID,GS1AIMasterID = x.GS1AIMasterID,
                                QualityCode = x.QualityCode,
                                QualityDescription = x.QualityDescription,
                                SQL = x.SQL,
                                Collection = x.Collection,
                                Deleted = false
                            };

                            entities.QualityMasters.Add(qualityMaster);
                        }
                        else
                        {
                            var dbDataMaster =
                                        entities.QualityMasters.FirstOrDefault(
                                            data => data.QualityMasterID == x.QualityMasterID && !data.Deleted);

                            if (dbDataMaster != null)
                            {
                                dbDataMaster.NouQualityTypeID = x.NouQualityTypeID;
                                dbDataMaster.GS1AIMasterID = x.GS1AIMasterID;
                                dbDataMaster.QualityCode = x.QualityCode;
                                dbDataMaster.QualityDescription = x.QualityDescription;
                                dbDataMaster.SQL = x.SQL;
                                dbDataMaster.Collection = x.Collection;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Data masters successfully upqualityd");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Retrieve all the system quality template names.
        /// </summary>
        /// <returns>A collection of quality template names.</returns>
        public IList<QualityTemplateName> GetQualityTemplateNames()
        {
            this.Log.LogDebug(this.GetType(),
                "GetQualityTemplateNames(): Attempting to retrieve all the quality template names");
            var names = new List<QualityTemplateName>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                names = entities.QualityTemplateNames.Where(x => !x.Deleted).ToList();
                this.Log.LogDebug(this.GetType(),
                    string.Format("{0} quality template names retrieved", names.Count()));
                // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return names;
        }

        /// <summary>
        /// Retrieve all the system quality template allocations.
        /// </summary>
        /// <returns>A collection of quality template allocations.</returns>
        public IList<QualityTemplateAllocation> GetQualityTemplateAllocations()
        {
            this.Log.LogDebug(this.GetType(),
                "GetQualityTemplateAllocations(): Attempting to retrieve all the quality template allocations");
            var allocations = new List<QualityTemplateAllocation>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    allocations = entities.QualityTemplateAllocations.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(),
                        string.Format("{0} quality template allocations retrieved", allocations.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(),
                    string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return allocations;
        }

        /// <summary>
        /// Adds or upqualitys quality names.
        /// </summary>
        /// <param name="qualityNames">The quality names to add or upquality.</param>
        /// <returns>A flag, indicating a succcessful add or upquality.</returns>
        public bool AddOrUpdateQualityTemplateNames(IList<QualityTemplateName> qualityNames)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpqualityQualityTemplateNames(): Attempting to upquality the quality template names");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    qualityNames.ToList().ForEach(x =>
                    {
                        if (x.QualityTemplateNameID == 0)
                        {
                            var templateName = new QualityTemplateName
                            {
                                Name = x.Name,
                                Deleted = false
                            };

                            entities.QualityTemplateNames.Add(templateName);
                        }
                        else
                        {
                            var qualityName =
                                entities.QualityTemplateNames.FirstOrDefault(
                                    template =>
                                        template.QualityTemplateNameID == x.QualityTemplateNameID &&
                                        !template.Deleted);

                            if (qualityName != null)
                            {
                                qualityName.Name = x.Name;
                                qualityName.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Quality tempate names successfully upqualityd");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Adds a quality master to a template.
        /// </summary>
        /// <param name="qualityMasterId">The id of the quality master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddQualityToTemplate(int qualityMasterId, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddQualityToTemplate(): Attempting to add qualitymaster id:{0} to template id:{1}", qualityMasterId, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var allocation = new QualityTemplateAllocation
                    {
                        QualityMasterID = qualityMasterId,
                        QualityTemplateNameID = templateId,
                        Deleted = false
                    };

                    entities.QualityTemplateAllocations.Add(allocation);
                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Allocation successful");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
       
        /// <summary>
        /// Removes a quality master from a template.
        /// </summary>
        /// <param name="qualityMasterId">The id of the quality master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        public bool RemoveQualityFromTemplate(int qualityMasterId, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveQualityFromTemplate(): Attempting to remove qualitymaster id:{0} from template id:{1}", qualityMasterId, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbAllocation =
                        entities.QualityTemplateAllocations.FirstOrDefault(
                            x => x.QualityMasterID == qualityMasterId && x.QualityTemplateNameID == templateId && !x.Deleted);

                    if (dbAllocation != null)
                    {
                        dbAllocation.Deleted = true;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Removal successful");
                        return true;
                    }

                    this.Log.LogDebug(this.GetType(), "Removal unsuccessful");
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Adds a quality master to a template.
        /// </summary>
        /// <param name="qualityMasters">The collection of the quality masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        public bool AddQualitysToTemplate(IList<ViewQualityMaster> qualityMasters, int templateId)
        {
            this.Log.LogDebug(this.GetType(), string.Format("AddQualitysToTemplate(): Attempting to add {0} qualitymasters to template id:{1}", qualityMasters.Count, templateId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // Remove the current template quality fields.
                    var currentQMs =
                        entities.QualityTemplateAllocations.Where(x => x.QualityTemplateNameID == templateId);
                    entities.QualityTemplateAllocations.RemoveRange(currentQMs);
                    entities.SaveChanges();

                    foreach (var qualityMaster in qualityMasters)
                    {
                        var allocation = new QualityTemplateAllocation
                        {
                            QualityMasterID = qualityMaster.QualityMasterID,
                            QualityTemplateNameID = templateId,
                            Batch = qualityMaster.Batch,
                            Transaction = qualityMaster.Transaction,
                            Required = qualityMaster.Required,
                            Reset = qualityMaster.Reset,
                            Deleted = false
                        };

                        entities.QualityTemplateAllocations.Add(allocation);
                    }

                    entities.SaveChanges();

                    this.Log.LogDebug(this.GetType(), "Allocations successful");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
    }
}

