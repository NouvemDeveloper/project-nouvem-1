﻿// -----------------------------------------------------------------------
// <copyright file="DynamicRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Data;
using Nouvem.Global;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using Nouvem.Model.DataLayer.Interface;
    using Nouvem.Properties;

    public class DynamicRepository : NouvemRepositoryBase, IDynamicRepository
    {
        /// <summary>
        /// Retrieves the label data result row.
        /// </summary>
        public DataTable GetSPData(string spName)
        {
            var dataResult = new DataTable();

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand(spName, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        public Tuple<bool, string> GetSPResult(string spName, string value, int id)
        {
            var dataResult = false;
            var response = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand(spName, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = new SqlParameter("@Answer", SqlDbType.NVarChar);
                    param.Value = value;
                    var idParam = new SqlParameter("@ID", SqlDbType.Int);
                    idParam.Value = id;
                    var outputParam = new SqlParameter("@Result", SqlDbType.Bit);
                    outputParam.Direction = ParameterDirection.Output;
                    var outputParamResponse = new SqlParameter("@Message", SqlDbType.NVarChar, 1000);
                    outputParamResponse.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);
                    command.Parameters.Add(idParam);
                    command.Parameters.Add(outputParam);
                    command.Parameters.Add(outputParamResponse);

                    command.ExecuteNonQuery();

                    var localDataResult = command.Parameters["@Result"].Value;
                    var localResponse = command.Parameters["@Message"].Value;

                    if (localDataResult != DBNull.Value)
                    {
                        dataResult = (bool)localDataResult;
                    }

                    if (localResponse != DBNull.Value)
                    {
                        response = (string)localResponse;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(dataResult, response);
        }

        /// <summary>
        /// Gets the sp result for the input value.
        /// </summary>
        /// <param name="spName">The sp name.</param>
        /// <param name="value">The input value.</param>
        /// <param name="id">The optional input record id.</param>
        /// <returns>Flag, as to valid iinput value.</returns>
        public Tuple<string, string, string> GetSPResultReturn(string spName, string value, int id, int? referenceId = null, int? processId = null, int? productID = 0)
        {
            var dataResult = string.Empty;
            var response = string.Empty;
            var attributeResult = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand(spName, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = new SqlParameter("@Answer", SqlDbType.NVarChar);
                    param.Value = value;
                    var idParam = new SqlParameter("@ID", SqlDbType.Int);
                    idParam.Value = id;
                    var referenceidParam = new SqlParameter("@ReferenceID", SqlDbType.Int);
                    referenceidParam.Value = referenceId;
                    var processidParam = new SqlParameter("@ProcessID", SqlDbType.Int);
                    processidParam.Value = processId;
                    var productidParam = new SqlParameter("@ProductID", SqlDbType.Int);
                    productidParam.Value = productID;
                    var outputParam = new SqlParameter("@Result", SqlDbType.NVarChar, 200);
                    outputParam.Direction = ParameterDirection.Output;
                    var outputParamResponse = new SqlParameter("@Message", SqlDbType.NVarChar, 1000);
                    outputParamResponse.Direction = ParameterDirection.Output;
                    var outputParamAttribute = new SqlParameter("@Attribute", SqlDbType.NVarChar, 20);
                    outputParamAttribute.Direction = ParameterDirection.Output;
                    command.Parameters.Add(param);
                    command.Parameters.Add(idParam);
                    command.Parameters.Add(referenceidParam);
                    command.Parameters.Add(processidParam);
                    command.Parameters.Add(productidParam);
                    command.Parameters.Add(outputParam);
                    command.Parameters.Add(outputParamResponse);
                    command.Parameters.Add(outputParamAttribute);

                    command.ExecuteNonQuery();

                    var localDataResult = command.Parameters["@Result"].Value;
                    var localResponse = command.Parameters["@Message"].Value;
                    var localAttributeResult = command.Parameters["@Attribute"].Value;

                    if (localDataResult != DBNull.Value)
                    {
                        dataResult = (string)localDataResult;
                    }

                    if (localResponse != DBNull.Value)
                    {
                        response = (string)localResponse;
                    }

                    if (localAttributeResult != DBNull.Value)
                    {
                        attributeResult = (string)localAttributeResult;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(dataResult, response, attributeResult);
        }

        /// <summary>
        /// Validates a transaction.
        /// </summary>
        /// <param name="spName">The sp macro.</param>
        /// <param name="id">The document id.</param>
        /// <param name="param1">param 1</param>
        /// <param name="param2">param 2</param>
        /// <param name="param3">param 3</param>
        /// <param name="param4">param 4</param>
        /// <returns>A validation issue message, or empty if validated.</returns>
        public string GetSPDataResultReturn(string spName, int id, decimal wgt, decimal qty, int inmasterid, int warehouseId,
           int processId, int userId, int deviceId, string reference = "", string reference2 = "", string reference3 = "", string reference4 = "")
        {
            var dataResult = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var command = new SqlCommand(spName, conn);
                    command.CommandType = CommandType.StoredProcedure;
             
                    var idParam = new SqlParameter("@ID", SqlDbType.Int);
                    idParam.Value = id;

                    var spparam1 = new SqlParameter("@Wgt", SqlDbType.Decimal);
                    spparam1.Value = wgt;

                    var spparam2 = new SqlParameter("@Qty", SqlDbType.Decimal);
                    spparam2.Value = qty;

                    var spparam3 = new SqlParameter("@INMasterID", SqlDbType.Int);
                    spparam3.Value = inmasterid;

                    var spparam4 = new SqlParameter("@WarehouseID", SqlDbType.Int);
                    spparam4.Value = warehouseId;

                    var spparam5 = new SqlParameter("@ProcessID", SqlDbType.Int);
                    spparam5.Value = processId;

                    var spparam10 = new SqlParameter("@UserID", SqlDbType.Int);
                    spparam10.Value = userId;

                    var spparam11 = new SqlParameter("@DeviceID", SqlDbType.Int);
                    spparam11.Value = deviceId;

                    var spparam6 = new SqlParameter("@Reference", SqlDbType.NVarChar);
                    spparam6.Value = reference;

                    var spparam7 = new SqlParameter("@Reference2", SqlDbType.NVarChar);
                    spparam7.Value = reference2;

                    var spparam8 = new SqlParameter("@Reference3", SqlDbType.NVarChar);
                    spparam8.Value = reference3;

                    var spparam9 = new SqlParameter("@Reference4", SqlDbType.NVarChar);
                    spparam9.Value = reference4;

                    var outputParam = new SqlParameter("@Result", SqlDbType.NVarChar, 200);
                    outputParam.Direction = ParameterDirection.Output;

                    command.Parameters.Add(idParam);
                    command.Parameters.Add(spparam1);
                    command.Parameters.Add(spparam2);
                    command.Parameters.Add(spparam3);
                    command.Parameters.Add(spparam4);
                    command.Parameters.Add(spparam5);
                    command.Parameters.Add(spparam10);
                    command.Parameters.Add(spparam11);
                    command.Parameters.Add(spparam6);
                    command.Parameters.Add(spparam7);
                    command.Parameters.Add(spparam8);
                    command.Parameters.Add(spparam9);
                    command.Parameters.Add(outputParam);

                    command.ExecuteNonQuery();

                    dataResult = (string)command.Parameters["@Result"].Value;

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dataResult;
        }

        /// <summary>
        /// Retrieves the search screens macro data.
        /// </summary>
        /// <param name="name">The sp name.</param>
        /// <param name="start">The search start vaue.</param>
        /// <param name="end">The seach end value.</param>
        /// <param name="showAll">The show all in search flag.</param>
        /// <returns>Search scrren macro data.</returns>
        public Tuple<DataTable, string> GetSearchScreenMacroData(string name, DateTime start, DateTime end, bool showAll, string searchText, string searchDateType)
        {
            var dataResult = new DataTable();
            var returnMsg = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    var command = new SqlCommand(name, conn);
                    command.CommandType = CommandType.StoredProcedure;
                    var outputParamResponse = new SqlParameter("@Message", SqlDbType.NVarChar, 1000);
                    outputParamResponse.Direction = ParameterDirection.Output;
                    command.Parameters.Add("@Start", SqlDbType.DateTime).Value = start;
                    command.Parameters.Add("@End", SqlDbType.DateTime).Value = end;
                    command.Parameters.Add("@SearchText", SqlDbType.NVarChar, 100).Value = searchText ?? string.Empty;
                    command.Parameters.Add("@ShowAll", SqlDbType.Bit).Value = showAll;
                    command.Parameters.Add("@SearchDateType", SqlDbType.NVarChar, 100).Value = searchDateType ?? string.Empty;
                    command.Parameters.Add(outputParamResponse);
                    conn.Open();
                    var dataReader = command.ExecuteReader();
                    dataResult.Load(dataReader);

                    var localResult = command.Parameters["@Message"].Value;
                    if (localResult != DBNull.Value)
                    {
                        returnMsg = (string)localResult;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return Tuple.Create(dataResult, returnMsg);
        }

        /// <summary>
        /// Deletes a batch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteProductionStock(string serial, int id, int userId, int deviceId)
        {
            var messsage = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    var command = new SqlCommand("App_DeleteProductionStock", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Serial", SqlDbType.NVarChar).Value = serial;
                    command.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@DeviceId", SqlDbType.Int).Value = deviceId;
                    command.Parameters.Add("@Message", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    conn.Open();

                    command.ExecuteNonQuery();
                    var returnMessage = command.Parameters["@Message"].Value;

                    if (returnMessage != DBNull.Value)
                    {
                        messsage = (string)returnMessage;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                messsage = ex.Message;
            }

            return messsage;
        }

        /// <summary>
        /// Undeletes a stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The transactiontype id.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string UndeleteStock(int serial, int id, int userId, int deviceId, int orderId = 0)
        {
            var messsage = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    var command = new SqlCommand("App_UndeleteStock", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Serial", SqlDbType.Int).Value = serial;
                    command.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@DeviceId", SqlDbType.Int).Value = deviceId;
                    command.Parameters.Add("@OrderID", SqlDbType.Int).Value = orderId;
                    command.Parameters.Add("@Message", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    conn.Open();

                    command.ExecuteNonQuery();
                    var returnMessage = command.Parameters["@Message"].Value;

                    if (returnMessage != DBNull.Value)
                    {
                        messsage = (string)returnMessage;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                messsage = ex.Message;
            }

            return messsage;
        }

        /// <summary>
        /// Deletes a batch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteOutOfProductionStock(int serial, int userId, int deviceId)
        {
            var messsage = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    var command = new SqlCommand("App_DeleteOutOfProductionStock", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Serial", SqlDbType.Int).Value = serial;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@DeviceId", SqlDbType.Int).Value = deviceId;
                    command.Parameters.Add("@Message", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    conn.Open();

                    command.ExecuteNonQuery();
                    var returnMessage = command.Parameters["@Message"].Value;

                    if (returnMessage != DBNull.Value)
                    {
                        messsage = (string)returnMessage;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                messsage = ex.Message;
            }

            return messsage;
        }

        /// <summary>
        /// Deletes a dispatch stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteDispatchStock(int serial, int id, int userId, int deviceId, int warehouseId, int inmasterId = 0)
        {
            var messsage = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    var command = new SqlCommand("App_DeleteDispatchStock", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Serial", SqlDbType.Int).Value = serial;
                    command.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@DeviceId", SqlDbType.Int).Value = deviceId;
                    command.Parameters.Add("@WarehouseId", SqlDbType.Int).Value = warehouseId;
                    command.Parameters.Add("@ProductID", SqlDbType.Int).Value = inmasterId;
                    command.Parameters.Add("@Message", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    conn.Open();

                    command.ExecuteNonQuery();
                    var returnMessage = command.Parameters["@Message"].Value;

                    if (returnMessage != DBNull.Value)
                    {
                        messsage = (string)returnMessage;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                messsage = ex.Message;
            }

            return messsage;
        }

        /// <summary>
        /// Deletes a grader stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteGraderStock(int serial, int userId, int deviceId)
        {
            var messsage = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    var command = new SqlCommand("App_DeleteGraderStock", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Serial", SqlDbType.Int).Value = serial;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@DeviceId", SqlDbType.Int).Value = deviceId;
                    command.Parameters.Add("@Message", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    conn.Open();

                    command.ExecuteNonQuery();
                    var returnMessage = command.Parameters["@Message"].Value;

                    if (returnMessage != DBNull.Value)
                    {
                        messsage = (string)returnMessage;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                messsage = ex.Message;
            }

            return messsage;
        }

        /// <summary>
        /// Deletes an intake stock item.
        /// </summary>
        /// <param name="serial">The serial no.</param>
        /// <param name="id">The pr order is.</param>
        /// <param name="userId">The user.</param>
        /// <param name="deviceId">The device.</param>
        /// <returns>Error message if issue, otherwise empty string.</returns>
        public string DeleteIntakeStock(int serial, int id, int userId, int deviceId)
        {
            var messsage = string.Empty;

            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    var command = new SqlCommand("App_DeleteIntakeStock", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@Serial", SqlDbType.Int).Value = serial;
                    command.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                    command.Parameters.Add("@UserId", SqlDbType.Int).Value = userId;
                    command.Parameters.Add("@DeviceId", SqlDbType.Int).Value = deviceId;
                    command.Parameters.Add("@Message", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    conn.Open();

                    command.ExecuteNonQuery();
                    var returnMessage = command.Parameters["@Message"].Value;

                    if (returnMessage != DBNull.Value)
                    {
                        messsage = (string)returnMessage;
                    }

                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                messsage = ex.Message;
            }

            return messsage;
        }

        /// <summary>
        /// Gets the table data.
        /// </summary>
        /// <param name="command">The sql query.</param>
        /// <returns>A collection of table data.</returns>
        public IList<object> GetData(string command)
        {
            this.Log.LogDebug(this.GetType(), string.Format("GetData(): command:{0}", command));
            var data = new List<object>();
            try
            {
                using (var conn = new SqlConnection(ApplicationSettings.ApplicationConnectionString))
                {
                    conn.Open();
                    var cmd = new SqlCommand(command, conn);

                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        data.Add(reader[0]);
                    }

                    this.Log.LogDebug(this.GetType(), "Data retrieved");
                };
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return data;
        }
    }
}
