﻿// -----------------------------------------------------------------------
// <copyright file="PlantRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.BusinessObject;

namespace Nouvem.Model.DataLayer.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.DataLayer.Interface;

    public class PlantRepository : NouvemRepositoryBase, IPlantRepository
    {
        /// <summary>
        /// Retrieve all the cplants.
        /// </summary>
        /// <returns>A collection of plants.</returns>
        public IList<Plant> GetAllPlants()
        {
            this.Log.LogDebug(this.GetType(), "GetPlants(): Attempting to retrieve all the plants");
            var plants = new List<Plant>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // gets the plants, and if associated with a business partner, the partner name and address.
                    plants = entities.Plants.Where(x => !x.Deleted).ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} plants retrieved", plants.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return plants;
        }

        /// <summary>
        /// Method which returns a warehouse and it's sub warehouses.
        /// </summary>
        /// <returns>A warehouse.</returns>
        public Location GetStockWarehouse(int warehouseId)
        {
            var location = new Location();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbLocation = entities.Warehouses.FirstOrDefault(x => x.WarehouseID == warehouseId);
                    if (dbLocation != null)
                    {
                        location.WarehouseId = dbLocation.WarehouseID;
                        location.Name = dbLocation.Name;
                        location.SubLocations = (from sub in dbLocation.WarehouseLocations
                            where sub.Deleted == null
                            select new Location
                            {
                                WarehouseLocationId = sub.WarehouseLocationID,
                                WarehouseId = sub.WarehouseID,
                                Name = sub.Name,
                                PrimaryWarehouseName = location.Name
                            }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return location;
        }

        /// <summary>
        /// Method which returns the warehouses and sub warehouses.
        /// </summary>
        /// <returns>A collection of non deleted warehouses.</returns>
        public IList<Location> GetStockWarehouses()
        {
            var locations = new List<Location>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    locations = (from warehouse in entities.Warehouses
                        where !warehouse.Deleted
                        select new Location
                        {
                            WarehouseId = warehouse.WarehouseID,
                            Name = warehouse.Name
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return locations;
        }

        /// <summary>
        /// Method which returns the warehouse location matching the input id.
        /// </summary>
        /// <returns>A warehouse sub location.</returns>
        public WarehouseLocation GetSubLocation(int id)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.WarehouseLocations.FirstOrDefault(x => x.WarehouseLocationID == id);
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Retrieve all the cplants.
        /// </summary>
        /// <returns>A collection of plants.</returns>
        public IList<ViewPlant> GetPlants()
        {
            this.Log.LogDebug(this.GetType(), "GetPlants(): Attempting to retrieve all the plants");
            var plants = new List<ViewPlant>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // gets the plants, and if associated with a business partner, the partner name and address.
                    plants = entities.ViewPlants.ToList();
                    this.Log.LogDebug(this.GetType(), string.Format("{0} plants retrieved", plants.Count()));
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return plants;
        }

        /// <summary>
        /// Add or updates the plants.
        /// </summary>
        /// <param name="plants">The plants to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdatePlants(IList<ViewPlant> plants)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdatePlants(): Attempting to update the plants");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    plants.ToList().ForEach(x =>
                    {
                        if (x.PlantID == 0)
                        {
                            // new
                            var newPlant= new Plant
                            {
                                Code = x.Code,
                                SiteName = x.SiteName,
                                CountryID = x.CountryID,
                                Deleted = false
                            };

                            entities.Plants.Add(newPlant);
                        }
                        else
                        {
                            // update
                            var dbPlant =
                                entities.Plants.FirstOrDefault(
                                    plant => plant.PlantID == x.PlantID && !plant.Deleted);

                            if (dbPlant != null)
                            {
                                dbPlant.Code = x.Code;
                                dbPlant.SiteName = x.SiteName;
                                dbPlant.CountryID = x.CountryID;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Plants successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Method which returns the warehouses.
        /// </summary>
        /// <returns>A collection of non deleted warehouses.</returns>
        public IList<Warehouse> GetWarehouses()
        {
            this.Log.LogDebug(this.GetType(), "GetWarehouses(): Attempting to retrieve all the warehouse locations");
            var locations = new List<Warehouse>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                    var entities = new NouvemEntities();
                    locations = entities.Warehouses.Where(x => !x.Deleted).ToList();
               // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} Warehouses successfully retrieved.", locations.Count));
            return locations;
        }

        /// <summary>
        /// Add or updates the warehouses list.
        /// </summary>
        /// <param name="warehouses">The warehouses to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateWarehouses(IList<Warehouse> warehouses)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateWarehouses(): Attempting to update the warehouses");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    warehouses.ToList().ForEach(x =>
                    {
                        if (x.WarehouseID == 0)
                        {
                            entities.Warehouses.Add(x);
                        }
                        else
                        {
                            // update
                            var dbWarehouse =
                                entities.Warehouses.FirstOrDefault(
                                    warehouse => warehouse.WarehouseID == x.WarehouseID && !warehouse.Deleted);

                            if (dbWarehouse != null)
                            {
                                dbWarehouse.Name = x.Name;
                                dbWarehouse.Code = x.Code;
                                dbWarehouse.AvailableForSale = x.AvailableForSale;
                                dbWarehouse.StockLocation = x.StockLocation;
                                dbWarehouse.AvailableForProduction = x.AvailableForProduction;
                                dbWarehouse.Notes = x.Notes;
                                dbWarehouse.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Warehouses successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Deleted the input warehouse.
        /// </summary>
        /// <param name="warehouse">The warehouse to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public bool RemoveWarehouse(Warehouse warehouse)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveWarehouses(): Attempting to remove warehouse Id:{0}", warehouse.WarehouseID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbWarehouse = entities.Warehouses.FirstOrDefault(x => x.WarehouseID == warehouse.WarehouseID);
                    if (dbWarehouse != null)
                    {
                        dbWarehouse.Deleted = true;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Warehouse successfully removed");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Method which returns the warehouse locations.
        /// </summary>
        /// <returns>A collection of non deleted warehouse locations.</returns>
        public IList<WarehouseLocation> GetWarehouseLocations()
        {
            this.Log.LogDebug(this.GetType(), "GetWarehouseLocations(): Attempting to retrieve all the warehouse locations");
            var locations = new List<WarehouseLocation>();

            try
            {
                //using (var entities = new NouvemEntities())
                //{
                var entities = new NouvemEntities();
                locations = entities.WarehouseLocations.Where(x => x.Deleted == null).ToList();
                // }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            this.Log.LogDebug(this.GetType(), string.Format("{0} Warehouse locations successfully retrieved.", locations.Count));
            return locations;
        }

        /// <summary>
        /// Add or updates the warehouses list.
        /// </summary>
        /// <param name="locations">The warehouses to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        public bool AddOrUpdateWarehouseLocations(IList<WarehouseLocation> locations)
        {
            this.Log.LogDebug(this.GetType(), "AddOrUpdateWarehouseLocations(): Attempting to update the warehouses");

            try
            {
                using (var entities = new NouvemEntities())
                {
                    locations.ToList().ForEach(x =>
                    {
                        if (x.WarehouseLocationID == 0)
                        {
                            entities.WarehouseLocations.Add(x);
                        }
                        else
                        {
                            // update
                            var dbLocation =
                                entities.WarehouseLocations.FirstOrDefault(
                                    warehouse => warehouse.WarehouseLocationID == x.WarehouseLocationID && warehouse.Deleted == null);

                            if (dbLocation != null)
                            {
                                dbLocation.Name = x.Name;
                                dbLocation.Code = x.Code;
                                dbLocation.WarehouseID = x.WarehouseID;
                                dbLocation.Barcode = x.Barcode;
                                dbLocation.MultiProductLocation = x.MultiProductLocation;
                                dbLocation.Deleted = x.Deleted;
                            }
                        }
                    });

                    entities.SaveChanges();
                    this.Log.LogDebug(this.GetType(), "Warehouse locations successfully updated");
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return false;
            }
        }

        /// <summary>
        /// Deleted the input warehouse location.
        /// </summary>
        /// <param name="location">The warehouse location to remove.</param>
        /// <returns>A flag, indicating a successful removal or not.</returns>
        public bool RemoveWarehouseLocation(WarehouseLocation location)
        {
            this.Log.LogDebug(this.GetType(), string.Format("RemoveWarehousesLocation(): Attempting to remove warehouse location Id:{0}", location.WarehouseID));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var dbWarehouseLocation = entities.WarehouseLocations.FirstOrDefault(x => x.WarehouseLocationID == location.WarehouseLocationID);
                    if (dbWarehouseLocation != null)
                    {
                        dbWarehouseLocation.Deleted = DateTime.Now;
                        entities.SaveChanges();
                        this.Log.LogDebug(this.GetType(), "Warehouse location successfully removed");
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }
    }
}
