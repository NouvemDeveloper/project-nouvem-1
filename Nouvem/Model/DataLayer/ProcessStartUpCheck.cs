//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProcessStartUpCheck
    {
        public int ProcessStartUpCheckID { get; set; }
        public int ProcessID { get; set; }
        public int AttributeTemplateID { get; set; }
        public Nullable<System.DateTime> Deleted { get; set; }
        public Nullable<int> NouModuleID { get; set; }
        public Nullable<int> DeviceID { get; set; }
        public Nullable<int> INMasterID { get; set; }
    
        public virtual AttributeTemplate AttributeTemplate { get; set; }
        public virtual Process Process { get; set; }
    }
}
