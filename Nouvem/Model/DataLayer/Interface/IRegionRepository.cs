﻿// -----------------------------------------------------------------------
// <copyright file="IRegionRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IRegionRepository
    {
        /// <summary>
        /// Returns the application regions.
        /// </summary>
        /// <returns>The application regions.</returns>
        IList<Region> GetRegions();

        /// <summary>
        /// Add or updates the regions.
        /// </summary>
        /// <param name="regions">The regions to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateRegions(IList<Region> regions);
    }
}
