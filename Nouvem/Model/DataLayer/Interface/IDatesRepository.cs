﻿// -----------------------------------------------------------------------
// <copyright file="IDatesRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.BusinessObject;

namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
 
    public interface IDatesRepository
    {
        /// <summary>
        /// Retrieve all the date masters.
        /// </summary>
        /// <returns>A collection of date masters.</returns>
        IList<ViewDatesMaster> GetDateMasters();

        /// <summary>
        /// Retrieve all the system date types.
        /// </summary>
        /// <returns>A collection of date types.</returns>
        IList<NouDateType> GetDateTypes();

        /// <summary>
        /// Retrieve all the date masters.
        /// </summary>
        /// <returns>A collection of date masters.</returns>
        IList<SearchDateType> GetSearchDateTypes();

        /// <summary>
        /// Adds or updates date masters.
        /// </summary>
        /// <param name="dateMasters">The date masters to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateDateMasters(IList<ViewDatesMaster> dateMasters);

        /// <summary>
        /// Retrieve all the system date template names.
        /// </summary>
        /// <returns>A collection of date template names.</returns>
        IList<DateTemplateName> GetDateTemplateNames();

        /// <summary>
        /// Adds or updates date names.
        /// </summary>
        /// <param name="dateNames">The date names to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateDateTemplateNames(IList<DateTemplateName> dateNames);

        /// <summary>
        /// Retrieve all the system date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        IList<DateTemplateAllocation> GetDateTemplateAllocations();

        /// <summary>
        /// Adds a date master to a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        bool AddDateToTemplate(int dateMasterId, int templateId);

        /// <summary>
        /// Removes a date master from a template.
        /// </summary>
        /// <param name="dateMasterId">The id of the date master to remove.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful removel, or not.</returns>
        bool RemoveDateFromTemplate(int dateMasterId, int templateId);

        /// <summary>
        /// Retrieve all the date template allocations.
        /// </summary>
        /// <returns>A collection of date template allocations.</returns>
        IList<ViewDateTemplateAllocation> GetViewDateTemplateAllocations();

        /// <summary>
        /// Adds a date masters to a template.
        /// </summary>
        /// <param name="dateMasters">The collection of the date masters to add.</param>
        /// <param name="templateId">The id of the target template.</param>
        /// <returns>A flag, indicating a successful allocation, or not.</returns>
        bool AddDatesToTemplate(IList<ViewDatesMaster> dateMasters, int templateId);

        /// <summary>
        /// retrieves the date days.
        /// </summary>
        /// <returns>A collection of date days.</returns>
        IList<DateDay> GetDateDays();

        /// <summary>
        /// Adds or updates date days.
        /// </summary>
        /// <param name="dateDays">The date days to add or update.</param>
        /// <returns>A flag, indicating a succcessful add or update.</returns>
        bool AddOrUpdateDateDays(IList<DateDay> dateDays);
    }
}
