﻿// -----------------------------------------------------------------------
// <copyright file="IStockRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IStockRepository
    {
        /// <summary>
        /// Checks the dispatched stock against the production stock.
        /// </summary>
        /// <returns>Flag, as to successful run or not.</returns>
        bool CheckStock();

        /// <summary>
        /// Prices a group of stocktransactions.
        /// </summary>
        /// <param name="ids">The comman delimited stock ids.</param>
        /// <param name="price">The price to apply.</param>
        /// <returns>Flag, as to successful pricing or not.</returns>
        bool PriceStocktransactions(string ids, decimal price);

        /// <summary>
        /// Gets the batch/product stock levels.
        /// </summary>
        /// <returns>Batch/Product stock levels.</returns>
        int? GetSerialByCarcassNo(string carcassNo);

        /// <summary>
        /// Moves/Splits batch stock.
        /// </summary>
        /// <param name="stockdata">The batch stock data.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        bool MoveBatchStock(IList<StockTakeData> stockdata);

        /// <summary>
        /// Gets transactions related to the current device and module.
        /// </summary>
        /// <param name="deviceId">The current device.</param>
        /// <param name="nouTransactionTypeId">The module type.</param>
        /// <returns>A collection a related transactions.</returns>
        IList<App_GetDeviceTransactions_Result> GetTransactions(int deviceId, int nouTransactionTypeId);

        /// <summary>
        /// Gets all the edits.
        /// </summary>
        /// <returns>The transaction edits.</returns>
        IList<StockDetail> GetTransactionEdits();

        /// <summary>
        /// Updates batch/product stock levels.
        /// </summary>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateStock(IList<StockDetail> stocks);

        /// <summary>
        /// Gets the batch/product stock levels.
        /// </summary>
        /// <returns>Batch/Product stock levels.</returns>
        IList<StockDetail> GetStock();

        /// <summary>
        /// Adds a stock move order.
        /// </summary>
        /// <param name="order">The order to add.</param>
        /// <returns>Flag, as to successful add or not.</returns>
        int AddStockMoveOrder(Sale order);

        /// <summary>
        /// Updates a stock move order.
        /// </summary>
        /// <param name="order">The order to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateStockMoveOrder(Sale order);

        /// <summary>
        /// Updates a stock move order.
        /// </summary>
        /// <param name="order">The order to update.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool ChangeStockMoveOrderStatus(Sale order);

        /// <summary>
        /// Changes a batch data (product).
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="currentINMasterID">The id of the product to change.</param>
        /// <param name="inMasterId">The new product.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        bool ChangeBatchProduct(int prOrderid, int currentINMasterID, int inMasterId);

        /// <summary>
        /// Gets a stock move order by id.
        /// </summary>
        /// <param name="id">The id to retrieve the data for.</param>
        /// <returns>The retrieved data.</returns>
        Sale GetStockMoveOrderById(int id);

        /// <summary>
        /// Gets a stock move order by first/last.
        /// </summary>
        /// <param name="id">The id to retrieve the data for.</param>
        /// <returns>The retrieved data.</returns>
        Sale GetStockMoveOrderByFirstLast(bool first);

        /// <summary>
        /// Retrieves all the orders (and associated order details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetStockMoveOrders(IList<int?> orderStatuses, DateTime start, DateTime end);

        /// <summary>
        /// Splits batch stock.
        /// </summary>
        /// <param name="prOrderid">The productionn order.</param>
        /// <param name="stockToSplit">The stock to split.</param>
        /// <param name="splitStock">The split stock.</param>
        /// <returns>Flag, as to successful split or not.</returns>
        bool SplitBatchStock(int prOrderid, SaleDetail stockToSplit, SaleDetail splitStock);

        /// <summary>
        /// Changes a batch data (wgt,qty and product).
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="wgt">The new wgt.</param>
        /// <param name="qty">The new qty.</param>
        /// <param name="inMasterId">The new product.</param>
        /// <returns>Flag, as to successful change or not.</returns>
        bool ChangeBatchData(int prOrderid, decimal wgt, decimal qty, int inMasterId);

        /// <summary>
        /// Moves a batch to the input location.
        /// </summary>
        /// <param name="fromLocation">The move from location.</param>
        /// <param name="toLocation">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        bool MoveAllBatchStock(Location fromLocation, Location toLocation);

        /// <summary>
        /// Gets the location of the batch.
        /// </summary>
        /// <returns>The batch location id.</returns>
        Tuple<int, int?> GetBatchStockLocation(int prOrderId);

        /// <summary>
        /// Determines if another batch is in the location.
        /// </summary>
        /// <param name="batchId">The batch id.</param>
        /// <param name="location">The location to check.</param>
        /// <returns>Flag, as to whether another batch is in the location.</returns>
        bool IsAnotherBatchInLocation(int batchId, Location location);

        /// <summary>
        /// Determines if there's a lighter batch in a lower cooker rack.
        /// </summary>
        /// <param name="batchWgt">The move batch wgt.</param>
        /// <param name="location">The move location.</param>
        /// <returns>Data, as to whether there's a lighter batch wgt.</returns>
        Tuple<string, decimal, int> IsLighterBatchInCooker(decimal batchWgt, Location location);

        /// <summary>
        /// Moves a split batch to the input location, creating a replica batch.
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="location">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        bool MoveSplitBatchStock(PROrder prOrder, SaleDetail product, Location location);

        /// <summary>
        /// Moves a batch to the input location.
        /// </summary>
        /// <param name="prOrderid">The batch id.</param>
        /// <param name="location">The new location.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        bool MoveBatchStock(int prOrderid, Location location);
       
        /// <summary>
        /// Updates the adjusted stock items.
        /// </summary>
        /// <param name="stock">All the stock items.</param>
        /// <param name="transactionType">The epos transaction type.</param>
        /// <param name="warehouse">The warehouse location.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        bool AdjustStock(IList<ProductData> stock, NouTransactionType transactionType, Warehouse warehouse);

        /// <summary>
        /// Retrieves all the application stock takes.
        /// </summary>
        /// <returns>A collection of all the application stock takes.</returns>
        IList<StockTake> GetStockTakes();

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="barcode">The scanned barcode.</param>
        /// <param name="stockTakeId">The stock take the detail belongs to.</param>
        /// <param name="warehouseId">The current location.</param>
        /// <returns>A stock take detail result.</returns>
        StockTakeData AddToStockTake(string barcode, int stockTakeId, int warehouseId);

        /// <summary>
        /// Removes a stock take detail.
        /// </summary>
        /// <param name="barcode">The scanned barcode.</param>
        /// <returns>A flag, as to a successful removal or not.</returns>
        bool RemoveFromStockTake(string barcode);

        /// <summary>
        /// Retrieves the stock takes details for the input stock take.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        void GetStockTakeDetails(StockTake stockTake);

        /// <summary>
        /// Retrieves the stock takes details for the input stock take.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        IList<App_GetStockTakeData_Result> GetStockTakeData(StockTake stockTake);

        /// <summary>
        /// Retrieves the stock for the stock reconciliation.
        /// </summary>
        /// <returns>A collection stock take details.</returns>
        IList<StockTakeData> GetStock(StockTake stockTake);

        /// <summary>
        /// Reconciles the stock.
        /// </summary>
        /// <returns>A flag, indicating a successful reconciliation or not.</returns>
        bool ReconcileStock(Model.BusinessObject.StockTake data);

        /// <summary>
        /// Adds a new stock take.
        /// </summary>
        /// <param name="stockTake">The stock take to add.</param>
        /// <returns>A Flag, as to whether the stock take has been added.</returns>
        bool AddStockTake(Model.DataLayer.StockTake stockTake);

        /// <summary>
        /// Adds a new stock take.
        /// </summary>
        /// <param name="stockTake">The stock take to add.</param>
        /// <returns>A Flag, as to whether the stock take has been added.</returns>
        bool UpdateStockTake(DataLayer.StockTake stockTake);

        /// <summary>
        /// Start of day to ensure leftover stock cannot be boxed.
        /// </summary>
        /// <returns>A flag, as to a successful procedure.</returns>
        bool DisableAddingStockToBox();

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="location">The warehouse to move to.</param>
        /// <param name="subLocation">The sub warehouse to move to.</param>
        /// <param name="barcode">The scanned barcode.</param>
        /// <returns>A stock movement result.</returns>
        IList<StockTakeData> MoveStock(int location, int? subLocation, string barcode, int orderId,
            bool logForReprint);

        /// <summary>
        /// Moves stock down the slicing/joints production line.
        /// </summary>
        /// <param name="prOrder">The batch id.</param>
        /// <param name="inMasterId">The product to split off.</param>
        /// <param name="locationId">The new location.</param>
        /// <param name="wgt">The weight to split off.</param>
        /// <param name="qty">The qty to split off.</param>
        /// <returns>Flag, as to successful move or not.</returns>
        bool MoveBatchStockIntoProduction(PROrder prOrder, int inMasterId, int locationId, decimal wgt, int qty,
            int? processId);

        /// <summary>
        /// Adds a stock take detail.
        /// </summary>
        /// <param name="data">The stock move data.</param>
        /// <returns>A stock movement result.</returns>
        bool UndoMoveStock(StockTakeData data);
    }
}
