﻿// -----------------------------------------------------------------------
// <copyright file="IPurchasesRepositoryModel.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.Enum;

namespace Nouvem.Model.DataLayer.Interface
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IPurchasesRepository
    {
        /// <summary>
        /// Retrieves all the quotes (and associated quote details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all quotes (and associated quote details).</returns>
        IList<Sale> GetAllAPQuotes(IList<int?> orderStatuses);

        /// <summary>
        /// Gets the intake details.
        /// </summary>
        /// <param name="id">The intake id to search for.</param>
        /// <returns>An intake, and it's details.</returns>
        Sale GetTouchscreenReceiptByNextQueueNo(string killType);

        /// <summary>
        /// Ckecks if a product is on a purchase order.
        /// </summary>
        /// <param name="inmasterid"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        bool IsProductOnPurchaseOrder(int inmasterid, int orderId);

        /// <summary>
        /// Updates a line with a batch no.
        /// </summary>
        /// <param name="id">The line id.</param>
        /// <param name="batchNo">The batch no.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateAPReceiptBatchNumber(int id, int batchNo);

        /// <summary>
        /// Determines if an intake has all it's stock on sales invoices.
        /// </summary>
        /// <param name="id">The intake id.</param>
        /// <returns></returns>
        bool HasIntakeBeenSaleInvoiced(int id);

        /// <summary>
        /// Copies a purchase order.
        /// </summary>
        /// <param name="orderNo">The order no to copy from.</param>
        /// <param name="userId">The current user.</param>
        /// <param name="deviceId">The current device.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        ProductStockData CopyPurchaseOrder(int orderNo, int productid, int userId, int deviceId,
            DateTime scheduledDate);

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateAPReceiptStatus(Sale sale);

        /// <summary>
        /// Prices a purchase invoice docket.
        /// </summary>
        /// <param name="intakeId">The intake id used to price it.</param>
        /// <param name="invoiceId">The invoice id if updating.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <returns>Invoice number if successful, otherwise 0.</returns>
        int PricePurchaseInvoice(int intakeId, int invoiceId, int userId, int deviceId);

        /// <summary>
        /// Chages the status on an intake line.
        /// </summary>
        /// <param name="id">The line id.</param>
        /// <param name="statusId">The status to change to.</param>
        /// <returns>Flag, as to sucessful change or not.</returns>
        bool ChangeAPReceiptDetailOrderStatus(int id, int statusId);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllAPReceiptsForReportByDates(IList<int?> orderStatuses, DateTime start, DateTime end);

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        bool PriceIntakeDocket(int docketId);

        /// <summary>
        /// Retrieves the intake (and associated dispatch details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        IList<App_GetIntakeDetails_Result> GetIntakeStatusDetails(int id);

        /// <summary>
        /// Gets the batch numbers associated with an intake product line.
        /// </summary>
        /// <returns>Batch ids.</returns>
         IList<string> GetIntakeLineBatchReferences(int intakeLineId);

        /// <summary>
        /// Removes a receipt detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
         bool RemoveApInvoiceDetail(int itemId);

        /// <summary>
        /// Gets the batch numbers associated with an intake product line.
        /// </summary>
        /// <returns>Batch ids.</returns>
        IList<BatchNumber> GetIntakeLineBatchNumbers(int intakeLineId);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        IList<Sale> GetAPQuotes(int customerId);

        /// <summary>
        /// Gets the intake docket id relating to the input purchase order id.
        /// </summary>
        /// <param name="id">The input order id.</param>
        /// <returns>The docket id relating to the input  order id.</returns>
        int GetIntakeIdForOrder(int id);

        /// <summary>
        /// Updates an existing intake docket (docket and details) when a purchaseorder has been ammended.
        /// </summary>
        /// <param name="sale">The order data.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateIntakeFromOrder(Sale sale);
        
        /// <summary>
        /// Cancels the input intake order.
        /// </summary>
        /// <param name="id">The purchase order to cancel the associated intake for.</param>
        /// <returns>A flag, as to whether the order was cancelled not.</returns>
        bool CancelIntakeFromOrder(int id);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        IList<Sale> GetAPQuotesByName(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input customer.</returns>
        IList<Sale> GetRecentAPQuotes(int customerId);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer id.
        /// </summary>
        /// <param name="quoteId">The quote id.</param>
        /// <returns>A quote (and associated quote details) for the input id.</returns>
        Sale GetAPQuoteByID(int quoteId);

        /// <summary>
        /// Retrieves the last local edited quote.
        /// </summary>
        /// <returns>A quote (and associated quote details).</returns>
        Sale GetAPQuoteByLastEdit();


        /// <summary>
        /// Adds or removes pallet stock from the order.
        /// </summary>
        /// <param name="serial">The pallet id.</param>
        /// <param name="intakeId">The order id.</param>
        /// <param name="addToOrder">Add or remove flag.</param>
        /// <returns>Flag, as to success or not.</returns>
        bool AddOrRemovePalletStock(int? serial, int? intakeId, bool addToOrder);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        IList<Sale> GetAPQuotesByCode(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Adds a new sale order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>The newly created sale id, indicating a successful quote add, or not.</returns>
        int AddAPQuote(Sale sale);

        /// <summary>
        /// Adds a new sale order quote (quote and details).
        /// </summary>
        /// <param name="sale">The sale order quote details.</param>
        /// <returns>A flag, indicating a successful quote update, or not.</returns>
        bool UpdateAPQuote(Sale sale);


        /// <summary> Adds a new purchase order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id, indicating a successful order add, or not.</returns>
        int AddAPOrder(Sale sale);

        /// <summary>
        /// Updates an existing order (order and details).
        /// </summary>
        /// <param name="sale">The purchase order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateAPOrder(Sale sale);

        /// <summary>
        /// Retrieves all the live orders (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllAPOrders(IList<int?> orderStatuses, DateTime start, DateTime end);
       
        /// <summary>
        /// Retrieves the live orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetAPOrders(int customerId);

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An order (and associated order details) for the input id.</returns>
        Sale GetAPOrderByID(int orderId);

        /// <summary>
        /// Retrieves the order (and associated order details) for the input number.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>An order (and associated order details) for the input number.</returns>
        Sale GetAPOrderByNumber(int number);

        /// <summary>
        /// Retrieves the order (and associated order details) for the input id.
        /// </summary>
        /// <returns>An order (and associated order details) for the input id.</returns>
        Sale GetAPOrderByLastEdit();

        /// <summary>
        /// Retrieves the live orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetRecentAPOrders(int customerId);

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetAPOrdersByName(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetAPOrdersByCode(string searchTerm, IList<int?> orderStatuses);

        /// <summary> Adds a new purchase receipt (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        Tuple<int, int> AddAPReceipt(Sale sale);

        /// <summary>
        /// Retrieves all the goods received by kill type (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllAPReceiptsByType(IList<int?> orderStatuses, string type);

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateAPReceipt(Sale sale);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllAPReceipts(IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllAPReceiptsForReport(IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the receipts (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetAPReceipts(int customerId);

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="searchTerm">The customer serach term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetAPReceiptsByName(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="searchTerm">The customer code search term.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetAPReceiptsByCode(string searchTerm, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the receipt (and associated order details) for the input ap order id.
        /// </summary>
        /// <param name="orderId">The ap order id.</param>
        /// <returns>The receipt (and associated order details) for the input ap order id.</returns>
        Sale GetAPReceiptsByApOrderId(int orderId);

        /// <summary>
        /// Retrieves the order (and associated order details) for the input order id.
        /// </summary>
        /// <param name="apOrderId">The order id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        Sale GetAPOrderById(int apOrderId);

        /// <summary>
        /// Gets the batch numbers.
        /// </summary>
        /// <returns>A collection of batch numbers.</returns>
        IList<BatchNumber> GetBatchNumbers();

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="complete">The complete order flag.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        bool CompleteOrCancelAPReceipt(Sale sale, bool complete = true);

        /// <summary>
        /// Retrieves the receipt (and associated receipt details) for the input receipt id.
        /// </summary>
        /// <param name="apReceiptId">The receipt id to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        Sale GetAPReceiptById(int apReceiptId);

        /// <summary>
        /// Removes a quote detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        bool RemoveApQuoteDetail(int itemId);

        /// <summary>
        /// Removes an order detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        bool RemoveApOrderDetail(int itemId);

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="batches">The workflow batches.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateIntakes(HashSet<Sale> batches, ViewType view);

        /// <summary>
        /// Removes a receipt detail.
        /// </summary>
        /// <param name="itemId">The item id.</param>
        /// <returns>Flag, as to whether the item was removed, or not.</returns>
        bool RemoveApReceiptDetail(int itemId);

        /// <summary>
        /// Retrieves the quotes (and associated quote details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of quotes (and associated quote details) for the input search term.</returns>
        IList<Sale> GetAPQuotesByPartner(int partnerId, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer name search.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetAPOrdersByPartner(int partnerId, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customer id.
        /// </summary>
        /// <param name="partnerId">The customer id.</param>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        IList<Sale> GetAPReceiptsByPartner(int partnerId, IList<int?> orderStatuses);

        /// <summary>
        /// Retrieves the receipts (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetRecentAPReceipts(int customerId);

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the last local edit.
        /// </summary>
        /// <returns>A collection of orders (and associated order details)</returns>
        Sale GetAPReceiptFullByLastEdit();

        /// <summary>
        /// Updates an existing goods receipt (receipt and details).
        /// </summary>
        /// <param name="sale">The purchase order receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateDesktopAPReceipt(Sale sale);

        /// <summary>
        /// Removes stock from item.
        /// </summary>
        /// <param name="sale">The sale/detail/transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        bool RemoveIntakeStockFromOrder(Sale sale);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllAPReceiptsByAttribute(string searchText);

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetRecentPurchaseOrders(int customerId);       

        /// <summary>
        /// Retrieves the orders (and associated order details) for the input customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of orders (and associated order details) for the input customer.</returns>
        IList<Sale> GetRecentPurchaseReceiptOrders(int customerId);

        /// <summary>
        /// Retrieves the order receipts (and associated order details) for the input customers code search term.
        /// </summary>
        /// <param name="apReceiptId">The order serch id.</param>
        /// <returns>A collection of orders (and associated order details) for the input search term.</returns>
        Sale GetAPReceiptFullById(int apReceiptId);

        /// <summary>
        /// Retrieves all the goods received (and associated orders details).
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllAPReceiptsForBatchEdit();

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        IList<Sale> GetAllAPInvoicesByDate(IList<int?> invoiceStatuses, DateTime start, DateTime end,
            string dateType);

        /// <summary>
        /// Gets the intake details.
        /// </summary>
        /// <param name="id">The intake id to search for.</param>
        /// <returns>An intake, and it's details.</returns>
        Sale GetTouchscreenReceiptById(int id);

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        Sale GetAPInvoiceByLastEdit();

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        Sale GetAPInvoiceByID(int id);

        /// <summary>
        /// Gets the transaction count for a given header row.
        /// </summary>
        /// <param name="id">The header row id.</param>
        /// <param name="transactionTypeId">The transaction type id</param>
        /// <returns>The transaction count for a given header row.</returns>
        int GetTransactionCount(int id, int transactionTypeId);

        /// <summary>
        /// Retrieves all the invoices (and associated invoice details).
        /// </summary>
        /// <param name="invoiceStatuses">The invoice status ids.</param>
        /// <returns>A collection of all invoice (and associated invoice details).</returns>
        IList<Sale> GetAllAPInvoices(int supplierId);

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        Tuple<int, DocNumber> AddNewAPInvoice(Sale sale);
       
        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateAPInvoice(Sale sale);

        /// <summary>
        /// Retrieves all the non invoiced intake data.
        /// </summary>
        /// <returns>A collection of non invoiced intake data (and associated order details).</returns>
        IList<Sale> GetNonInvoicedIntakes(bool pricingInvoice = false);

        /// <summary>
        /// Retrieves the apinvoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        IList<Sale> GetAPInvoices();

        /// <summary> Adds a new stock return (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created order id and stock transaction id, indicating a successful order add, or not.</returns>
        Tuple<int, int> AddAPReturn(Sale sale);

        /// <summary>
        /// Updates an existing return (receipt and details).
        /// </summary>
        /// <param name="sale">The return receipt details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateAPReturn(Sale sale);


        /// <summary>
        /// Retrieves the returns (and associated details) for the input receipt id.
        /// </summary>
        /// <param name="id">The intakeid to search for.</param>
        /// <returns>The order (and associated order details) for the input order id.</returns>
        /// <remarks>Note: We just need the order status and receipt detail weights/qty.</remarks>
        IList<App_GetReturnDetails_Result> GetReturnsStatusDetails(int id);

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAPReturns(IList<int?> orderStatuses);

        /// <summary>
        /// Gets the return details.
        /// </summary>
        /// <param name="id">The returns id to search for.</param>
        /// <returns>A return, and it's details.</returns>
        Sale GetAPReturnById(int id);

        /// <summary>
        /// Completes the input order.
        /// </summary>
        /// <param name="sale">The order to mark as completed.</param>
        /// <param name="complete">The complete order flag.</param>
        /// <returns>A flag, as to whether the order was marked as completed or not.</returns>
        bool UpdateAPReturnStatus(Sale sale);

        /// <summary>
        /// Gets the return details by last edit date.
        /// </summary>
        /// <returns>A return, and it's details.</returns>
        Sale GetAPReturnByLastEdit();

        /// <summary>
        /// Checks that the docket lines are corect.
        /// </summary>
        /// <param name="docketId">The docket to check.</param>
        /// <returns>Flag, to to whether there was an issue checking or not.</returns>
        bool PriceReturnsDocket(int docketId);

        /// <summary>
        /// Retrieves all the goods returned.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAPReturnsNonInvoiced();

        /// <summary>
        /// Updates an existing invoice (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>A flag, indicating a successful update, or not.</returns>
        bool UpdateAPReturnInvoice(Sale sale);

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        Tuple<int, DocNumber> AddAPReturnInvoice(Sale sale);


        /// <summary>
        /// Retrieves the ap return invoices.
        /// </summary>
        /// <returns>A collection of non exported invoices (and associated invoice details).</returns>
        IList<Sale> GetAPReturnInvoices();

        /// <summary>
        /// Retrieves the last local edited invoice.
        /// </summary>
        /// <returns>A local edited invoice.</returns>
        Sale GetAPReturnInvoiceByID(int id);

        /// <summary>
        /// Exports credit note details.
        /// </summary>
        /// <param name="invoiceIds">The invoice ids to export.</param>
        /// <returns>Flag, as to whether the invoices were exported, or not.</returns>
        bool ExportAPReturnInvoice(IList<int> invoiceIds);
    }
}
