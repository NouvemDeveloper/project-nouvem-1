﻿// -----------------------------------------------------------------------
// <copyright file="IProductionRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IProductionRepository
    {
        /// <summary>
        /// Returns the specifications.
        /// </summary>
        /// <returns>The application specifications.</returns>
        IList<ProductionData> GetSpecifications();

        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        PROrder GetPROrderLastEdit();

        /// <summary>
        /// Edits the batch row data.
        /// </summary>
        /// <returns></returns>
        bool EditBatchData(BatchData batchData);

        /// <summary>
        /// Gets a prorder reference.
        /// </summary>
        /// <param name="prOrderId"></param>
        /// <returns></returns>
        string GetPROrderReference(int prOrderId);

        /// <summary>
        /// Returns the production order data.
        /// </summary>
        /// <returns>The production order data.</returns>
        IList<App_GetBatchData_Result> GetBatchData(int prOrderId, int transId, bool linesOnly);

        /// <summary>
        /// Returns the batch stock data for a product.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
         IList<App_ProductStockDataByBatch_Result> GetProductStockDataByBatch(int productId);

        /// <summary>
        /// Updates an order output.
        /// </summary>
        /// <param name="spec">The outputs to add.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        bool UpdateRecipeOrderAmounts(ProductionData order);

        /// <summary>
        /// Returns the production batch reference, or empty string if user generated.
        /// </summary>
        /// <returns>The production batch reference.</returns>
        string GetProductionBatchReference(int deviceId, int userId, int processid, DateTime scheduledDate);

        /// <summary>
        /// Returns the production multi batch intake orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        ///BatchNumber GetProductionProductOldestBatch(int? inMasterId);

        /// <summary>
        /// Returns the production multi batch intake orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        ProductionBatchData GetProductionBatchData(int? inMasterId);

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        ProductionData GetProductionBatchOrderByNumber(int orderId);

        /// <summary>
        /// Reworks recipe stock into the current batch.
        /// </summary>
        /// <param name="stockTransactionId">The rework stock id.</param>
        /// <param name="prOrderId">The batch id.</param>
        /// <param name="deviceid">The device id.</param>
        /// <param name="userid">The user id.</param>
        /// <returns>Rework message.</returns>
        string ReworkRecipeStock(int stockTransactionId, int prOrderId, int deviceid, int userid);

        /// <summary>
        /// Gets the recipes.
        /// </summary>
        /// <returns></returns>
        IList<Sale> GetRecipes();

        /// <summary>
        /// Checks if a product is a recipe product.
        /// </summary>
        /// <param name="inmasterid"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        bool IsProductRecipeProduct(int inmasterid, int orderId);

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        Sale GetRecipeOrderMethodByProductId(int id);

        /// <summary>
        /// Copies a production order.
        /// </summary>
        /// <param name="orderNo">The order no to copy from.</param>
        /// <param name="userId">The current user.</param>
        /// <param name="deviceId">The current device.</param>
        /// <returns>Flag, as to successful copy or not.</returns>
        ProductStockData CopyProductionOrder(int orderNo, int userId, int deviceId, DateTime scheduledDate);

        /// <summary>
        /// Issues recipe backflush.
        /// </summary>
        /// <param name="orderId">The pr orderid.</param>
        /// <param name="batchId">the batch number id.</param>
        /// <param name="qty">The issue qty.</param>
        /// <param name="wgt">The issue wgt.</param>
        /// <param name="warehouseId">The warehouse id.</param>
        /// <param name="inMasterId">The product id.</param>
        /// <param name="processId">The process id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="deviceId">The device id.</param>
        /// <param name="splitId">The mix count.</param>
        /// <returns>Flag , as to successful issue or not.</returns>
        bool IssueBackflush(int orderId, int batchId, decimal qty, decimal wgt, int warehouseId, int inMasterId,
            int? processId, int userId, int deviceId, int splitId);

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        IList<ProductionData> GetProductionBatchOrdersForDesktop(IList<int> orderStatuses, DateTime start,
            DateTime end);

        /// <summary>
        /// Gets the recipe order methods.
        /// </summary>
        /// <returns>The recipe order methods</returns>
        IList<App_GetRecipeOrderMethods_Result> GetRecipeOrderMethods();

        /// <summary>
        /// Adds a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was added.</returns>
        int AddUniqueProductionOrder(ProductionData order);

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        bool ChangeRecipeBatchStatus(int orderId, int? status);

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        bool ChangeOrderStatus(int orderId, int nouDocStatusId);

        /// <summary>
        /// Gets the production details.
        /// </summary>
        /// <param name="prorderid">The order id.</param>
        /// <param name="transactionTypeid">The into/out of value.</param>
        /// <returns>A production details.</returns>
        IList<App_GetProductionDetails_Result> GetProductionDetails(int prorderid, int transactionTypeid);

        /// <summary>
        /// Returns the production orders data for the reports.
        /// </summary>
        /// <returns>The application production orders.</returns>
        IList<Sale> GetProductionOrdersForReportByOrderId(DateTime start, DateTime end);

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        IList<StockDetail> GetRecipeByIngredientProductId(int id);

        /// <summary>
        /// Returns the production multi batch intake orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        IList<ProductionData> GetProductionMultiBatchOrders(IList<ProductionData> currentData = null, int? inmasterid = null, bool productionLocationOnly = true);

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
       int? GetProductionOrderProduct(int id);

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        ProductionData GetPROrderByReference(string reference);

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        IList<SaleDetail> GetBatchOrderDataPreDispatch(ProductionData order);

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        SaleDetail GetProductionOrderTransactionDataForProduct(int orderId, int inMasterId, int productionStage);

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        IList<SaleDetail> GetProductionOrderTransactionDataNotSplit(int orderId);

        /// <summary>
        /// Completes a Recipe mix.
        /// </summary>
        /// <param name="order">The batch/Recipe data.</param>
        /// <returns>Flag, as to successful completion or not.</returns>
        bool CompleteMix(ProductionData order);

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        ProductionData GetProductionBatchOrderLastEdit();

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        ProductionData GetProductionBatchOrderFirstLast(bool first);

        /// <summary>
        /// Gets the application production types.
        /// </summary>
        /// <returns>The application production types.</returns>
        IList<NouPROrderType> GetPrOrderTypes();

        /// <summary>
        /// Determines if a batch reference exists.
        /// </summary>
        /// <param name="reference">The reference to check.</param>
        /// <returns></returns>
        DateTime? DoesPROrderByReferenceExist(string reference);

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        bool UpdateRecipe(Sale sale);

        /// <summary>
        /// Adds a new sale order (order and details).
        /// </summary>
        /// <param name="sale">The sale order details.</param>
        /// <returns>The newly created quote id, indicating a successful order add, or not.</returns>
        int AddRecipe(Sale sale);

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        Sale GetRecipeById(int id);

        /// <summary>
        /// Gets a Recipe by id.
        /// </summary>
        /// <param name="id">The Recipe id.</param>
        /// <returns>A Recipe.</returns>
        Sale GetRecipeByProductId(int id);
        
        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        Sale GetRecipeByLastEdit();

        /// <summary>
        /// Gets a Recipe by last edit time.
        /// </summary>
        /// <returns>A Recipe.</returns>
        Sale GetRecipeByFirstLast(bool first);

        /// <summary>
        /// Gets the application issue methods.
        /// </summary>
        /// <returns>The application issue methods.</returns>
        IList<NouIssueMethod> GetNouIssueMethods();

        /// <summary>
        /// Gets the application issue methods.
        /// </summary>
        /// <returns>The application issue methods.</returns>
        IList<UOMMaster> GetNouUOMs();

        /// <summary>
        /// Determines if a carcass split scan already exists in the db.
        /// </summary>
        /// <param name="serial">The serial number to check.</param>
        /// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        ///IList<int> GetScannedCarcassSplitStockImport(string serial);

        /// <summary>
        /// Gets the pr order.
        /// </summary>
        /// <param name="id">The order id.</param>
        /// <returns>A pr order.</returns>
        PROrder GetPROrder(int id);

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        IList<SaleDetail> GetBatchOrderData(ProductionData order);

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        IList<ProductionData> GetProductionOrdersByLocation(int locationId);

        /// <summary>
        /// Consumes all items in a batch at the input module.
        /// </summary>
        /// <param name="prOrderId">The batch id.</param>
        /// <param name="transTypeId">The module type.</param>
        /// <returns>Flag, as to successfull consumption or not.</returns>
        bool ConsumeProductionBatch(int prOrderId, int transTypeId);

        /// <summary>
        /// Updates batch weight/qty.
        /// </summary>
        /// <param name="orderId">The prorder id.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateBatchWeight(int orderId);

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        IList<SaleDetail> GetStockMovementTransactionData(int orderId, int locationId = 0,
            int? subLocationId = null);

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        IList<ProductionData> GetProductionBatchOrders(IList<int> orderStatuses, DateTime start, DateTime end);

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        IList<SaleDetail> GetProductionOrderRecallData(int orderId);

        /// <summary>
        /// Returns the matching production order.
        /// </summary>
        /// <returns>The matching production order.</returns>
        ProductionData GetProductionBatchOrder(int orderId);

        /// <summary>
        /// Updates an order output.
        /// </summary>
        /// <param name="spec">The outputs to add.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        bool UpdateOrderSpecification(ProductionData spec);

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        ProductionData GetPROrderById(int id);

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        int? GetPROrderNumber(int transactionId);

        /// <summary>
        /// Updates a workflow status.
        /// </summary>
        /// <param name="batches">The workflow batches.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool UpdateBatches(HashSet<Sale> batches);

        /// <summary>
        /// Retrieves all the production batches.
        /// </summary>
        /// <param name="orderStatuses">The order status ids.</param>
        /// <returns>A collection of all orders (and associated order details).</returns>
        IList<Sale> GetAllBatches(IList<int?> orderStatuses, DateTime start, DateTime end);

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        bool UpdatePieceTransaction(int transactionId);

        /// <summary>
        /// Returns the production orders.
        /// </summary>
        /// <returns>The application production orders.</returns>
        IList<ProductionData> GetProductionOrders(IList<ProductionData> data = null);

        /// <summary>
        /// Removes stock from a production order.
        /// </summary>
        /// <param name="product">The transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        bool RemoveStockFromIntoProductionOrder(StockDetail product);

        /// <summary>
        /// Cancel a batch.
        /// </summary>
        /// <returns>The batch id.</returns>
        bool CancelBatch(int batchId);

        /// <summary>
        /// Open a batch.
        /// </summary>
        /// <returns>The batch id.</returns>
        bool OpenBatch(int batchId);

        /// <summary>
        /// Determines if a carcass split scan already exists in the db.
        /// </summary>
        /// <param name="serial">The serial number to check.</param>
        /// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        IList<int> GetScannedCarcassSplitStock(string serial, bool intake = false, int transactionTypeId = 3);

        /// <summary>
        /// Returns the production order for the associated batch.
        /// </summary>
        /// <returns>The associated production order.</returns>
        ProductionData GetProductionOrderForBatch(int batchId);

        /// <summary>
        /// Determines if a carcass split scan already exists in the db.
        /// </summary>
        /// <param name="inmasterid">The product to check.</param>
        /// <param name="serial">The serial number to check.</param>
        /// <returns>A flag, indicating whether a carcass split scan already exists in the db.</returns>
        bool HasCarcassSplitStockBeenScanned(int inmasterid, string serial);

        /// <summary>
        /// Returns the production orders data for the reports.
        /// </summary>
        /// <returns>The application production orders.</returns>
        IList<Sale> GetProductionOrdersForReport(DateTime start, DateTime end);

        /// <summary>
        /// Gets the oldest kill date from a production batch.
        /// </summary>
        /// <param name="prOrderId">The production batch id.</param>
        /// <param name="batchId">The batch id.</param>
        /// <returns>The oldest kill date from a production batch, or null if none exists.</returns>
        DateTime? GetProductionBatchOldestKillDate(int prOrderId, int batchId);

        /// <summary>
        /// Adds a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was added.</returns>
        int AddProductionOrder(ProductionData order);

        /// <summary>
        /// Adds stock to a production.
        /// </summary>
        /// <returns>Flag, as to whether stock has been added to a production.</returns>
        bool AddStockToProduction(SaleDetail product);

        /// <summary>
        /// Removes stock from a production order.
        /// </summary>
        /// <param name="product">The transactions to update.</param>
        /// <returns>Flag, indicating a successful update or not.</returns>
        bool RemoveStockFromOrder(SaleDetail product);

        /// <summary>
        /// Gets the external production batch data.
        /// </summary>
        /// <param name="prOrderId">The production batch id.</param>
        /// <returns>The production batch data.</returns>
        Tuple<string, string> GetBatchTraceData(int prOrderId);

        /// <summary>
        /// Gets the transaction data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        IList<SaleDetail> GetProductionOrderTransactionData(int orderId);

        /// <summary>
        /// Gets the oldest kill date from an external production batch.
        /// </summary>
        /// <param name="prOrderId">The production batch id.</param>
        /// <returns>The oldest kill date from a production batch, or null if none exists.</returns>
        DateTime? GetProductionBatchOldestKillDateExternal(int prOrderId);

        /// <summary>
        /// Updates a new production order.
        /// </summary>
        /// <returns>Flag, as to whether an order was updates.</returns>
        bool UpdateProductionOrder(PROrder order);

        /// <summary>
        /// Retrieves the products alloed out of production.
        /// </summary>
        /// <param name="prSpecId">The pr spec id.</param>
        /// <returns>A collection of allowable products.</returns>
        IList<SaleDetail> GetProductsAllowedOutOfProduction(int prSpecId);

        /// <summary>
        /// Retrieves the products allowed out of production.
        /// </summary>
        /// <param name="prSpecId">The pr spec id.</param>
        /// <returns>A collection of allowable products.</returns>
        IList<InventoryItem> GetAllProductsAllowedOutOfProduction(int prSpecId);

        /// <summary>
        /// Adds an out of production transaction data.
        /// </summary>
        /// <param name="transData">The transaction data.</param>
        /// <returns>Flag, indicating a successful addition or not.</returns>
        int AddOutOfProductionTransaction(StockDetail transData);

        /// <summary>
        /// Gets the spec and batch only data for the out of production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        IList<SaleDetail> GetOutOfProductionOrderData(int orderId);

        /// <summary>
        /// Gets the spec and batch only data for the input production order.
        /// </summary>
        /// <param name="orderId">The input order id.</param>
        IList<SaleDetail> GetProductionOrderData(int orderId);

        /// <summary>
        /// Retrieves the non-consumed transactions on the input order for the input product, and creates a box transaction.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The box transaction id.</returns>
        Tuple<int, List<int?>> AddBoxTransaction(StockTransaction transaction, decimal boxTare = 0, int qtyPerBox = 1,
            bool enforceBoxQtyMatch = false);

        /// <summary>
        /// Adds a new production spec.
        /// </summary>
        /// <param name="spec">The spec to add.</param>
        /// <returns>A flag, as to successful add or not.</returns>
        bool AddSpecification(ProductionData spec);

        /// <summary>
        /// AUpdates production spec.
        /// </summary>
        /// <param name="spec">The spec to update.</param>
        /// <returns>A flag, as to successful update or not.</returns>
        bool UpdateSpecification(ProductionData spec);
    }
}
