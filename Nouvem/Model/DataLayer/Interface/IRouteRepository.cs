﻿// -----------------------------------------------------------------------
// <copyright file="IRouteRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IRouteRepository
    {
        /// <summary>
        /// Returns the application routes.
        /// </summary>
        /// <returns>The application routes.</returns>
        IList<Route> GetRoutes();

        /// <summary>
        /// Add or updates the routes.
        /// </summary>
        /// <param name="routes">The routes to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateRoutes(IList<Route> routes);
    }
}
