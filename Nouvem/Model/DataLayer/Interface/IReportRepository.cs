﻿// -----------------------------------------------------------------------
// <copyright file="IReportRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System;
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IReportRepository
    {
        /// <summary>
        /// Retrieve the epos sales data falling between the input dates.
        /// </summary>
        /// <returns>A collection of sales data.</returns>
        IList<ProductData> GetEposSalesData(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets the system report data setting for the input report.
        /// </summary>
        /// <returns>The system reports data setting for the input report.</returns>
        IList<UserReport> GetReportSettings(int reportId);

        /// <summary>
        /// Saves a report date type agianst a report and user.
        /// </summary>
        /// <param name="reportId">The report to save against.</param>
        /// <param name="dateTypeId">The selected date type.</param>
        /// <param name="userId">The user to save against.</param>
        /// <returns>Flag, as to successful save or not.</returns>
        bool SaveReportDateType(int reportId, int dateTypeId, int userId);

        /// <summary>
        /// Gets the report emaiul lookups.
        /// </summary>
        /// <returns>The application report email lookups.</returns>
        IList<ReportEmail> GetReportEmailLookups();

        /// <summary>
        /// Updates the report email lookups.
        /// </summary>
        /// <param name="modules">The look ups to add.</param>
        /// <returns>Flag, as to successful operation or not.</returns>
        bool UpdateReportProcesses(IList<Module> modules);

        /// <summary>
        /// Gets the modules.  
        /// </summary>
        /// <returns>the user reports.</returns>
        IList<NouModule> GetModules();

        /// <summary>
        /// Gets a report by last edit.
        /// </summary>
        /// <returns>A system report data.</returns>
        ReportData GetReportByLastEdit();

        /// <summary>
        /// Adds or updates report data.
        /// </summary>
        /// <param name="reports">The report to add/update.</param>
        /// <returns>Flag, as to successfull add or update.</returns>
        bool AddOrUpdateReports(IList<ReportData> reports);

        /// <summary>
        /// Adds or updates the report folders.
        /// </summary>
        /// <returns>Flag, as to successfull add or update.</returns>
        bool AddOrUpdateReportFolders(IList<ReportFolder> folders);

        /// <summary>
        /// Gets the report folders.
        /// </summary>
        /// <returns></returns>
        IList<ReportFolder> GetReportFolders();

        /// <summary>
        /// Save a user report default values.
        /// </summary>
        /// <param name="setting">The default data.</param>
        /// <returns>Flag, as to successful update or not.</returns>
        bool SaveUserReportSettings(UserReport setting);

        /// <summary>
        /// Gets the system report data setting for the input report and user.
        /// </summary>
        /// <returns>The system reports data setting for the input report and user.</returns>
        UserReport GetUserReportSettings(ReportData report, int userId);

        /// <summary>
        /// Updates the multi values for the reports.
        /// </summary>
        /// <param name="values">The values to update.</param>
        void UpdateMultiValueReports(IList<MultiReportValue> values);

        /// <summary>
        /// Gets the system report data.
        /// </summary>
        /// <returns>The system reports data.</returns>
        IList<ReportData> GetReports();

        /// <summary>
        /// Gets a report by it's id.
        /// </summary>
        /// <param name="id">The id to search with.</param>
        /// <returns>A system report data.</returns>
        ReportData GetReportById(int id);


        /// <summary>
        /// Gets a report by first or last entered.
        /// </summary>
        /// <param name="first">First\last flag.</param>
        /// <returns>A system report data.</returns>
        ReportData GetReportByFirstLast(bool first);

        /// <summary>
        /// Adds or updates report data.
        /// </summary>
        /// <param name="report">The report to add/update.</param>
        /// <returns>Flag, as to successfull add or update.</returns>
        bool AddOrUpdateReport(ReportData report);

        /// <summary>
        /// Updates the db reports.
        /// </summary>
        /// <param name="reports">The reports to update.</param>
        /// <returns></returns>
        bool UpdateNouReports(IList<ReportData> reports);

        /// <summary>
        /// Gets the user reports.  
        /// </summary>
        /// <returns>the user reports.</returns>
        IList<UserReport> GetUserReports();

        /// <summary>
        /// Updates the multi kill values for the reports.
        /// </summary>
        /// <param name="values">The values to update.</param>
        void UpdateMultiKillValueReports(IList<MultiKillReportValue> values);

        /// <summary>
        /// Gets the intake kill ids between the input dates.
        /// </summary>
        /// <param name="start">Start date.</param>
        /// <param name="end">End date.</param>
        /// <returns>The intake kill ids between the input dates.</returns>
        IList<int?> GetKillReportIds(DateTime start, DateTime end);
    }
}
