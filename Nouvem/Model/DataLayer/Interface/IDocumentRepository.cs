﻿// -----------------------------------------------------------------------
// <copyright file="IDocumentRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.DataLayer.Interface
{
    using System.Collections.Generic;
    using Nouvem.Model.BusinessObject;

    public interface IDocumentRepository
    {
        /// <summary>
        /// Returns the application document types.
        /// </summary>
        /// <returns>The application document types.</returns>
        IList<DocumentNumberingType> GetDocumentNumberingTypes();

        /// <summary>
        /// Returns the application document names.
        /// </summary>
        /// <returns>The application document names.</returns>
        IList<NouDocumentName> GetNouDocumentNames();

        /// <summary>
        /// Add or updates the document types list.
        /// </summary>
        /// <param name="types">The document types to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateDocumentTypes(IList<DocumentNumberingType> types);

        /// <summary>
        /// Returns the application document numbers.
        /// </summary>
        /// <returns>The application document names.</returns>
        IList<DocNumber> GetDocumentNumbers();

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="numbers">The document numbers to add or update.</param>
        /// <returns>A flag, indicating a successful add or update.</returns>
        bool AddOrUpdateDocumentNumbers(IList<DocNumber> numbers);

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="docNumberId">The document number id of the current document.</param>
        /// <returns>The updated doc number.</returns>
        DocNumber SetDocumentNumber(int docNumberId);
    }
}
