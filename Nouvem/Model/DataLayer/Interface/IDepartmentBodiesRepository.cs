﻿// -----------------------------------------------------------------------
// <copyright file="IDepartmentBodiesRepository.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Nouvem.Model.DataLayer.Interface
{
    public interface IDepartmentBodiesRepository
    {
        /// <summary>
        /// Log the Bord Bia request in the database.
        /// </summary>
        /// <param name="herdNo">The herd no.</param>
        /// <param name="requestDate">The date of the request.</param>
        /// <param name="userName">The username for which the request is sent.</param>
        /// <param name="fileName">The file name associated with the request.</param>
        bool LogBordBiaRequest(string herdNo, string requestDate, string userName, string fileName);
       
        /// <summary>
        /// Log the Bord Bia response in the database.
        /// </summary>
        /// <param name="herdNo">The herd no.</param>
        /// <param name="responseDate">The date on which the response was received.</param>
        /// <param name="fileName">The file name associated with the response.</param>
        /// <param name="herdOwner">The owner of the herd.</param>
        /// <param name="certDate">The date of expiry of the validation cert.</param>
        bool LogBordBiaResponse(string herdNo, string responseDate, string fileName, string herdOwner, string certDate);

        /// <summary>
        /// Find the historical quality assurance records for a herd number.
        /// </summary>
        /// <param name="herdNumber">The herd number.</param>
        /// <returns>A collection of quality assurance expiration records for the herd number.</returns>
        IList<QualityAssuranceHistory> FindQualityAssuranceEntries(string herdNumber);

        /// <summary>
        /// Add a new quality assurance history entry for a herd number.
        /// </summary>
        /// <param name="herdNumber">The herd number.</param>
        /// <param name="herdOwner">The herd owner.</param>
        /// <param name="certExpiryDate">The expiry date for the certificate.</param>
        /// <returns>The id of the new entry.</returns>
        int AddQualityAssurance(string herdNumber, string herdOwner, DateTime certExpiryDate);
    }
}
