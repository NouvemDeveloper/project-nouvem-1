//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nouvem.Model.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class Container
    {
        public Container()
        {
            this.INMasters = new HashSet<INMaster>();
            this.INMasters1 = new HashSet<INMaster>();
            this.INMasters2 = new HashSet<INMaster>();
            this.INMasters3 = new HashSet<INMaster>();
        }
    
        public int ContainerID { get; set; }
        public string Name { get; set; }
        public double Tare { get; set; }
        public bool Deleted { get; set; }
        public int ContainerTypeID { get; set; }
        public byte[] Photo { get; set; }
    
        public virtual ContainerType ContainerType { get; set; }
        public virtual ICollection<INMaster> INMasters { get; set; }
        public virtual ICollection<INMaster> INMasters1 { get; set; }
        public virtual ICollection<INMaster> INMasters2 { get; set; }
        public virtual ICollection<INMaster> INMasters3 { get; set; }
    }
}
