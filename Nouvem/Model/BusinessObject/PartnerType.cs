﻿// -----------------------------------------------------------------------
// <copyright file="PartnerType.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    public class PartnerType
    {
        /// <summary>
        /// Gets or sets the group id.
        /// </summary>
        public int PartnerTypeId { get; set; }

        /// <summary>
        /// Gets or sets the type name.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type has been deleted.
        /// </summary>
        public bool Deleted { get; set; }
    }
}
