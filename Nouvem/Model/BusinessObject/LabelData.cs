﻿// -----------------------------------------------------------------------
// <copyright file="LabelData.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using Nouvem.Model.DataLayer;

    public class LabelData
    {
        /// <summary>
        /// The label to print.
        /// </summary>
        public Label Label { get; set; }

        /// <summary>
        /// The label qty to print.
        /// </summary>
        public int LabelQty { get; set; }

        /// <summary>
        /// The associated printer.
        /// </summary>
        public Printer Printer { get; set; }
    }
}
