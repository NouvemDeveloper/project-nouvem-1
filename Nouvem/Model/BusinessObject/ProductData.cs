﻿// -----------------------------------------------------------------------
// <copyright file="ProductData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.ComponentModel;
    using Nouvem.Model.DataLayer;
    using Nouvem.Shared;

    /// <summary>
    /// Class which models the epos items.
    /// </summary>
    public class ProductData : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets the item quantity
        /// </summary>
        private int quantity;

        /// <summary>
        /// Gets or sets the stock quantity
        /// </summary>
        private int stockQuantity;

        /// <summary>
        /// Gets or sets the item id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the item group id.
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// Gets or sets the items typical pieces.
        /// </summary>
        public int? TypicalPieces { get; set; }

        /// <summary>
        /// Gets or sets the item code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the item name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the item description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the item price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the item min weight.
        /// </summary>
        public decimal? MinWeight { get; set; }

        /// <summary>
        /// Gets or sets the item max weight.
        /// </summary>
        public decimal? MaxWeight { get; set; }

        /// <summary>
        /// Gets or sets the item min weight.
        /// </summary>
        public decimal? NominalWeight { get; set; }

        /// <summary>
        /// Gets or sets the item image.
        /// </summary>
        public byte[] ItemImage { get; set; }

        /// <summary>
        /// Gets or sets the current stock location.
        /// </summary>
        public Warehouse Location { get; set; }

        /// <summary>
        /// Gets or sets the opening stock value.
        /// </summary>
        public int OpeningStock { get; set; }

        /// <summary>
        /// Gets or sets the account sales collected.
        /// </summary>
        public int? Collection { get; set; }

        /// <summary>
        /// Gets or sets the account sales delivered.
        /// </summary>
        public int? Delivery { get; set; }

        /// <summary>
        /// Gets or sets the item vat rate.
        /// </summary>
        public decimal? VatRate { get; set; }

        /// <summary>
        /// Gets or sets the actual stored vat amount.
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the actual stored vat amount.
        /// </summary>
        public decimal? ActualVatAmount { get; set; }

        /// <summary>
        /// Gets or sets the actual stored vat amount.
        /// </summary>
        public decimal? ActualCCVatAmount { get; set; }

        /// <summary>
        /// Gets or sets the paid account sales.
        /// </summary>
        public decimal AccSalesPaid { get; set; }

        /// <summary>
        /// Gets or sets the unpaid account sales.
        /// </summary>
        public decimal AccSalesUnpaid { get; set; }
  
        /// <summary>
        /// Gets the item price including vat.
        /// </summary>
        public decimal PriceIncVat
        {
            get
            {
                var convertedVatValue = 100M + this.VatRate.ToDecimal();
                return Math.Round(this.Price/100 *convertedVatValue, 2);
            }
        }

        /// <summary>
        /// Gets or sets the closing stock value.
        /// </summary>
        public int ClosingStock
        {
            get
            {
                return this.OpeningStock + this.NewStock - (this.AccountSales + this.NonAccountSales) - this.ReturnedStock;
            }
        }

        /// <summary>
        /// Gets or sets the stock balance value.
        /// </summary>
        public int StockBalance
        {
            get
            {
                return (this.OpeningStock + this.NewStock) - this.AccountSales - this.ReturnedStock;
            }
        }

        /// <summary>
        /// Gets or sets the sales revenue.
        /// </summary>
        public decimal? SalesRevenue { get; set; }

        /// <summary>
        /// Gets or sets the cash sales revenue.
        /// </summary>
        public decimal CashSalesRevenue
        {
            get
            {
                return Math.Round(this.CashSales*this.Price, 2);
            }
        }

        /// <summary>
        /// Gets or sets the account sales paid.
        /// </summary>
        public decimal? AccSalesPaidRevenue { get; set; }

        /// <summary>
        /// Gets or sets the account sales unpaid.
        /// </summary>
        public decimal? AccSalesUnpaidRevenue { get; set; }

        /// <summary>
        /// Gets or sets the cash sales revenue vat inc.
        /// </summary>
        public decimal CashSalesRevenueIncVat
        {
            get
            {
                return this.CashSalesRevenue + this.ActualVatAmount.ToDecimal();
            }
        }

        /// <summary>
        /// Gets or sets the cc sales revenue.
        /// </summary>
        public decimal CreditCardSalesRevenue
        {
            get
            {
                return Math.Round(this.CreditCardSales * this.Price, 2) + this.ActualCCVatAmount.ToDecimal(); 
            }
        }

        /// <summary>
        /// Gets or sets the total vat amount.
        /// </summary>
        public decimal VatAmount
        {
            get
            {
                if (this.VatRate != null && this.VatRate != 0)
                {
                    return Math.Round(this.Quantity * this.Price / this.VatRate.ToDecimal(), 2);
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the newly received stock value.
        /// </summary>
        public int NewStock { get; set; }

        /// <summary> Gets or sets the returned stock value.
        /// </summary>
        public int ReturnedStock { get; set; }

        /// <summary>
        /// Gets or sets the amount sold on account value.
        /// </summary>
        public int AccountSales { get; set; }

        /// <summary>
        /// Gets or sets the non account amount value.
        /// </summary>
        public int NonAccountSales { get; set; }

        /// <summary>
        /// Gets or sets the cash sales amount value.
        /// </summary>
        public int CashSales { get; set; }

        /// <summary>
        /// Gets or sets the cc sales amount value.
        /// </summary>
        public int CreditCardSales { get; set; }

        /// <summary>
        /// Gets or sets a value indicating if the stock quantity has been adjusted.
        /// </summary>
        public bool StockAdjusted { get; set; }

        /// <summary>
        /// Gets or sets the stock quantity.
        /// </summary>
        public int StockQuantity
        {
            get
            {
                return this.stockQuantity;
            }

            set
            {
                this.stockQuantity = value;
                this.OnPropertyChanged("StockQuantity");
            }
        }

        /// <summary>
        /// Gets or sets the item quantity.
        /// </summary>
        public int Quantity
        {
            get
            {
                return this.quantity;
            }

            set
            {
                this.quantity = value;
                this.OnPropertyChanged("Quantity");
            }
        }

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
