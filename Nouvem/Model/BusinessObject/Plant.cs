﻿// -----------------------------------------------------------------------
// <copyright file="PlantContainerView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    /// <summary>
    /// Class which models a plant.
    /// </summary>
    public class Plant : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets the plant id.
        /// </summary>
        public int PlantID { get; set; }

        /// <summary>
        /// The selected falg.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Gets or sets the plant code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the plant name.
        /// </summary>
        public string SiteName { get; set; }

        /// <summary>
        /// Gets or sets the plant country id.
        /// </summary>
        public int CountryID { get; set; }

        /// <summary>
        /// Gets or sets the plant partner name.
        /// </summary>
        public string BPName { get; set; }

        /// <summary>
        /// Gets or sets the plant partner address.
        /// </summary>
        public string BPAddress { get; set; }

        /// <summary>
        /// The selected flag.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
                Messenger.Default.Send(Tuple.Create(this.PlantID, value), Token.PlantItemSelected);
            }
        }

        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The event raise.
        /// </summary>
        /// <param name="propertyName">The property whose value will be broadcast.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
