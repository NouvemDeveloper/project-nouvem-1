﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Model.DataLayer;

namespace Nouvem.Model.BusinessObject
{
    public class StocktransactionDetail
    {
        /// <summary>
        /// The stock transaction.
        /// </summary>
        public StockTransaction StockTransaction { get; set; }

        /// <summary>
        /// The stock transaction attribute.
        /// </summary>
        public DataLayer.Attribute Attribute { get; set; }

        /// <summary>
        /// The stock transaction attribute.
        /// </summary>
        public AimsData AimsData { get; set; }

        /// <summary>
        /// The stock transaction attribute.
        /// </summary>
        public IList<FarmMovement> Movements { get; set; }
    }
}
