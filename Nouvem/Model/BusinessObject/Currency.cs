﻿// -----------------------------------------------------------------------
// <copyright file="Currency.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Currency
    {
        /// <summary>
        /// Gets or sets the currency id.
        /// </summary>
        public int CurrencyId { get; set; }

        /// <summary>
        /// Gets or sets the rate to base rate.
        /// </summary>
        public decimal RateToBase { get; set; }

        /// <summary>
        /// Gets or sets the currency name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the currency symbol.
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// Gets or sets a value indicating where the currency is deleted.
        /// </summary>
        public bool Deleted { get; set; }
    }
}
