﻿// -----------------------------------------------------------------------
// <copyright file="AllUser.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    public class AllUser : INotifyPropertyChanged
    {
        /// <summary>
        /// The selection flag.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The selection flag.
        /// </summary>
        private bool canAdd;

        /// <summary>
        /// The selection flag.
        /// </summary>
        private bool canEdit;

        /// <summary>
        /// The user id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The user name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The user selection flag.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
                Messenger.Default.Send(Token.Message, Token.UserOrGroupSelected);
            }
        }

        /// <summary>
        /// The user selection flag.
        /// </summary>
        public bool CanAdd
        {
            get
            {
                return this.canAdd;
            }

            set
            {
                this.canAdd = value;
                this.OnPropertyChanged("CanAdd");
                Messenger.Default.Send(Token.Message, Token.UserOrGroupSelected);
            }
        }

        /// <summary>
        /// The user selection flag.
        /// </summary>
        public bool CanEdit
        {
            get
            {
                return this.canEdit;
            }

            set
            {
                this.canEdit = value;
                this.OnPropertyChanged("CanEdit");
                Messenger.Default.Send(Token.Message, Token.UserOrGroupSelected);
            }
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
