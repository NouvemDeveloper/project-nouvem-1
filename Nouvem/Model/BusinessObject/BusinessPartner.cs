﻿// -----------------------------------------------------------------------
// <copyright file="PartnerPartner.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.ComponentModel;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Nouvem.Model.DataLayer;

    /// <summary>
    /// Class that models a business partner.
    /// </summary>
    public class BusinessPartner : INotifyPropertyChanged
    {
        /// <summary>
        /// The telesales status.
        /// </summary>
        private string telesaleStatus;

        /// <summary>
        /// The selection flag.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Gets or sets the partner vdetails.
        /// </summary>
        public ViewBusinessPartner Details { get; set; }

        /// <summary>
        /// Gets or sets the partner addresses.
        /// </summary>
        public IList<BusinessPartnerAddress> Addresses { get; set; }

        /// <summary>
        /// Gets or sets the partner contacts.
        /// </summary>
        public IList<BusinessPartnerContact> Contacts { get; set; }

        /// <summary>
        /// Gets or sets the partner contacts.
        /// </summary>
        public IList<SupplierHerd> Herds { get; set; }

        /// <summary>
        /// Gets or sets the partner attachments.
        /// </summary>
        public IList<BPAttachment> Attachments { get; set; }

        /// <summary>
        /// Gets or sets the associated partner group.
        /// </summary>
        public BPGroup PartnerGroup { get; set; }

        /// <summary>
        /// Gets or sets the associated telesale agent.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the associated telesale caller.
        /// </summary>
        public string UserNameCaller { get; set; }

        /// <summary>
        /// Gets or sets the associated telesale agent.
        /// </summary>
        public int OrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the associated telesale agent.
        /// </summary>
        public string ExistingPartner { get; set; }

        /// <summary>
        /// Gets or sets the associated partner currency.
        /// </summary>
        public BPCurrency PartnerCurrency { get; set; }

        /// <summary>
        /// Gets or sets the associated partner type.
        /// </summary>
        public NouBPType PartnerType { get; set; }

        /// <summary>
        /// Gets or sets the associated route.
        /// </summary>
        public Route Route { get; set; }

        /// <summary>
        /// Gets or sets the associated telesale.
        /// </summary>
        public Telesale Telesale { get; set; }

        /// <summary>
        /// The user selection flag.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets or sets the associated telesale.
        /// </summary>
        public string TelesaleStatus
        {
            get
            {
                return this.telesaleStatus;
            }

            set
            {
                this.telesaleStatus = value;
                this.OnPropertyChanged("TelesaleStatus");
            }
        }

        public string OnHold
        {
            get { return (this.Details != null && this.Details.OnHold.ToBool()).BoolToYesNo(); }
        }

        /// <summary>
        /// Gerts or sets the partner properties id's.
        /// </summary>
        public IList<BusinessPartnerProperty> PartnerProperties { get; set; }

        /// <summary>
        /// Gets or sets the associated label field data.
        /// </summary>
        public BPLabelField LabelField { get; set; }

        /// <summary>
        /// Gets or sets the partner audit details.
        /// </summary>
        public Audit Audit { get; set; }

        /// <summary>
        /// Gets the ftelesale call freequency.
        /// </summary>
        public string OnHoldYesNo
        {
            get
            {
                return this.Details != null && this.Details.OnHold == true ? Strings.Yes : Strings.No;
            }
        }

        /// <summary>
        /// Gets or sets the current telesale call time.
        /// </summary>
        public string CallTime { get; set; }

        /// <summary>
        /// Gets the call frequency
        /// </summary>
        public NouCallFrequency CallFrequency { get; set; }

        /// <summary>
        /// Gets the ftelesale call freequency.
        /// </summary>
        public string Frequency
        {
            get
            {
                if (this.CallFrequency != null)
                {
                    return this.CallFrequency.Frequency;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the first, default address.
        /// </summary>
        public BusinessPartnerAddress DefaultAddress
        {
            get
            {
                if (this.Addresses != null && this.Addresses.Any())
                {
                    return this.Addresses.FirstOrDefault();
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the partner type.
        /// </summary>
        public string Type
        {
            get
            {
                return this.PartnerType != null ? this.PartnerType.Type : string.Empty;
            }
        }

        /// <summary>
        /// Gets the partner group.
        /// </summary>
        public string Group
        {
            get
            {
                return this.PartnerGroup!= null ? this.PartnerGroup.BPGroupName : string.Empty;
            }
        }

        public BusinessPartner Copy()
        {
            try
            {
                return new BusinessPartner
                {
                    Details = new ViewBusinessPartner
                    {
                        BPMasterID = Details.BPMasterID,
                        BPGroupName = Details.BPGroupName,
                        BPGroupID = Details.BPGroupID,
                        NouBPTypeID = Details.NouBPTypeID,
                        Type = Details.Type,
                        ActiveFrom = Details.ActiveFrom,
                        ActiveTo = Details.ActiveTo,
                        InActiveFrom = Details.InActiveFrom,
                        InActiveTo = Details.InActiveTo,
                        Name = Details.Name,
                        Code = Details.Code,
                        Web = Details.Web,
                        CompanyNo = Details.CompanyNo,
                        Balance = Details.Balance,
                        PORequired = Details.PORequired,
                        PopUpNotes = Details.PopUpNotes,
                        Remarks = Details.Remarks,
                        Notes = Details.Notes,
                        VATNo = Details.VATNo,
                        Tel = Details.Tel,
                        FAX = Details.FAX,
                        CreditLimit = Details.CreditLimit,
                        CurrencyName = Details.CurrencyName,
                        BPCurrencyID = Details.BPCurrencyID,
                        Uplift = Details.Uplift,
                        OnHold = Details.OnHold,
                        RateToBase = Details.RateToBase,
                        Symbol = Details.Symbol
                    },

                    Addresses = new List<BusinessPartnerAddress>(this.Addresses),
                    Contacts = new List<BusinessPartnerContact>(this.Contacts),
                    Attachments = new List<BPAttachment>(this.Attachments),
                    PartnerProperties = new List<BusinessPartnerProperty>(this.PartnerProperties)
                };
            }
            catch (Exception)
            {
                return new BusinessPartner();
            }
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
