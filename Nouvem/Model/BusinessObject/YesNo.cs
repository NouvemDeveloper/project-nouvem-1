﻿// -----------------------------------------------------------------------
// <copyright file="YesNo.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    public class YesNo
    {
        public string Value { get; set; }
    }
}
