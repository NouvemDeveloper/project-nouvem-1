﻿// -----------------------------------------------------------------------
// <copyright file="RecentOrders.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Properties;
    using Nouvem.Shared;

    public class ProductRecentOrderData : INotifyPropertyChanged
    {
        /// <summary>
        /// The order string.
        /// </summary>
        private string ordered;

        /// <summary>
        /// Gets or sets order 1 creation date.
        /// </summary>
        public static DateTime? Order1Date { get; set; }

        /// <summary>
        /// Gets or sets order 2 creation date.
        /// </summary>
        public static DateTime? Order2Date { get; set; }

        /// <summary>
        /// Gets or sets order 3 creation date.
        /// </summary>
        public static DateTime? Order3Date { get; set; }

        /// <summary>
        /// Gets or sets order 4 creation date.
        /// </summary>
        public static DateTime? Order4Date { get; set; }

        /// <summary>
        /// Gets or sets order 5 creation date.
        /// </summary>
        public static DateTime? Order5Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order6Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order7Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order8Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order9Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order10Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order11Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order12Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order13Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order14Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order15Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order16Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order17Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order18Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order19Date { get; set; }

        /// <summary>
        /// Gets or sets order 6 creation date.
        /// </summary>
        public static DateTime? Order20Date { get; set; }
      
        /// <summary>
        /// Gets or sets the recent order 1.
        /// </summary>
        public static Sale RecentOrder1 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 2.
        /// </summary>
        public static Sale RecentOrder2 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 3.
        /// </summary>
        public static Sale RecentOrder3 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 4.
        /// </summary>
        public static Sale RecentOrder4 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 5.
        /// </summary>
        public static Sale RecentOrder5 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder6 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder7 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder8 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder9 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder10 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder11 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder12 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder13 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder14 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder15 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder16 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder17 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder18 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder19 { get; set; }

        /// <summary>
        /// Gets or sets the recent order 6.
        /// </summary>
        public static Sale RecentOrder20 { get; set; }

        #region overflow


        public static Sale RecentOrder21 { get; set; }
        public static Sale RecentOrder22 { get; set; }
        public static Sale RecentOrder23 { get; set; }
        public static Sale RecentOrder24 { get; set; }
        public static Sale RecentOrder25 { get; set; }
        public static Sale RecentOrder26 { get; set; }
        public static Sale RecentOrder27 { get; set; }
        public static Sale RecentOrder28 { get; set; }
        public static Sale RecentOrder29 { get; set; }
        public static Sale RecentOrder30 { get; set; }
        public static Sale RecentOrder31 { get; set; }
        public static Sale RecentOrder32 { get; set; }
        public static Sale RecentOrder33 { get; set; }
        public static Sale RecentOrder34 { get; set; }
        public static Sale RecentOrder35 { get; set; }
        public static Sale RecentOrder36 { get; set; }
        public static Sale RecentOrder37 { get; set; }
        public static Sale RecentOrder38 { get; set; }
        public static Sale RecentOrder39 { get; set; }
        public static Sale RecentOrder40 { get; set; }
        public static Sale RecentOrder41 { get; set; }
        public static Sale RecentOrder42 { get; set; }
        public static Sale RecentOrder43 { get; set; }
        public static Sale RecentOrder44 { get; set; }
        public static Sale RecentOrder45 { get; set; }
        public static Sale RecentOrder46 { get; set; }
        public static Sale RecentOrder47 { get; set; }
        public static Sale RecentOrder48 { get; set; }
        public static Sale RecentOrder49 { get; set; }
        public static Sale RecentOrder50 { get; set; }
        public static Sale RecentOrder51 { get; set; }
        public static Sale RecentOrder52 { get; set; }
        public static Sale RecentOrder53 { get; set; }
        public static Sale RecentOrder54 { get; set; }
        public static Sale RecentOrder55 { get; set; }
        public static Sale RecentOrder56 { get; set; }
        public static Sale RecentOrder57 { get; set; }
        public static Sale RecentOrder58 { get; set; }
        public static Sale RecentOrder59 { get; set; }
        public static Sale RecentOrder60 { get; set; }
        public static Sale RecentOrder61 { get; set; }
        public static Sale RecentOrder62 { get; set; }
        public static Sale RecentOrder63 { get; set; }
        public static Sale RecentOrder64 { get; set; }
        public static Sale RecentOrder65 { get; set; }
        public static Sale RecentOrder66 { get; set; }
        public static Sale RecentOrder67 { get; set; }
        public static Sale RecentOrder68 { get; set; }
        public static Sale RecentOrder69 { get; set; }
        public static Sale RecentOrder70 { get; set; }
        public static Sale RecentOrder71 { get; set; }
        public static Sale RecentOrder72 { get; set; }
        public static Sale RecentOrder73 { get; set; }
        public static Sale RecentOrder74 { get; set; }
        public static Sale RecentOrder75 { get; set; }
        public static Sale RecentOrder76 { get; set; }
        public static Sale RecentOrder77 { get; set; }
        public static Sale RecentOrder78 { get; set; }
        public static Sale RecentOrder79 { get; set; }
        public static Sale RecentOrder80 { get; set; }
        public static Sale RecentOrder81 { get; set; }
        public static Sale RecentOrder82 { get; set; }
        public static Sale RecentOrder83 { get; set; }
        public static Sale RecentOrder84 { get; set; }
        public static Sale RecentOrder85 { get; set; }
        public static Sale RecentOrder86 { get; set; }
        public static Sale RecentOrder87 { get; set; }
        public static Sale RecentOrder88 { get; set; }
        public static Sale RecentOrder89 { get; set; }
        public static Sale RecentOrder90 { get; set; }
        public static Sale RecentOrder91 { get; set; }
        public static Sale RecentOrder92 { get; set; }
        public static Sale RecentOrder93 { get; set; }
        public static Sale RecentOrder94 { get; set; }
        public static Sale RecentOrder95 { get; set; }
        public static Sale RecentOrder96 { get; set; }
        public static Sale RecentOrder97 { get; set; }
        public static Sale RecentOrder98 { get; set; }
        public static Sale RecentOrder99 { get; set; }
        public static Sale RecentOrder100 { get; set; }


        #endregion

        /// <summary>
        /// Gets the recent orders collection.
        /// </summary>
        public static IList<Sale> RecentOrders
        {
            get
            {
                return new List<Sale>
                {
                    RecentOrder1, RecentOrder2, RecentOrder3, RecentOrder4, RecentOrder5, RecentOrder6, RecentOrder7, RecentOrder8, RecentOrder9, RecentOrder10,
                    RecentOrder11, RecentOrder12, RecentOrder13, RecentOrder14, RecentOrder15, RecentOrder16, RecentOrder17, RecentOrder18, RecentOrder19, RecentOrder20,
                    RecentOrder21, RecentOrder22, RecentOrder23, RecentOrder24, RecentOrder25, RecentOrder26, RecentOrder27, RecentOrder28, RecentOrder29, RecentOrder30,
                    RecentOrder31, RecentOrder32, RecentOrder33, RecentOrder34, RecentOrder35, RecentOrder36, RecentOrder37, RecentOrder38, RecentOrder39, RecentOrder40,
                    RecentOrder41, RecentOrder42, RecentOrder43, RecentOrder44, RecentOrder45, RecentOrder46, RecentOrder47, RecentOrder48, RecentOrder49, RecentOrder50,
                    RecentOrder51, RecentOrder52, RecentOrder53, RecentOrder54, RecentOrder55, RecentOrder56, RecentOrder57, RecentOrder58, RecentOrder59, RecentOrder60,
                    RecentOrder61, RecentOrder62, RecentOrder63, RecentOrder64, RecentOrder65, RecentOrder66, RecentOrder67, RecentOrder68, RecentOrder69, RecentOrder70,
                    RecentOrder71, RecentOrder72, RecentOrder73, RecentOrder74, RecentOrder75, RecentOrder76, RecentOrder77, RecentOrder78, RecentOrder79, RecentOrder80,
                    RecentOrder81, RecentOrder82, RecentOrder83, RecentOrder84, RecentOrder85, RecentOrder86, RecentOrder87, RecentOrder88, RecentOrder89, RecentOrder90,
                    RecentOrder91, RecentOrder92, RecentOrder93, RecentOrder94, RecentOrder95, RecentOrder96, RecentOrder97, RecentOrder98, RecentOrder99, RecentOrder100,
                };
            }
        }

        /// <summary>
        /// Gets or sets the order method.
        /// </summary>
        public string OrderMethod { get; set; }

        /// <summary>
        /// Gets or sets the order amount.
        /// </summary>
        /// <remarks>Note - Not using the standard ToDouble() extension methods 
        /// as we want an exception thrown to signify invalid input.</remarks>
        public string Ordered
        {
            get
            {
                return this.ordered;
            }

            set
            {
                this.ordered = value;
                this.OnPropertyChanged("Ordered");

                if (string.IsNullOrWhiteSpace(value))
                {
                    return;
                }
               
                try
                {
                    decimal quantity = 0;
                    decimal weight = 0;
                    var saleDetail = new SaleDetail { InventoryItem = InventoryItem, INMasterID = InventoryItem.Master.INMasterID };

                    if (value.Contains("/"))
                    {
                        // ordered amount of both quantity and weight e.g. 1/10
                        var backSlashPos = value.IndexOf('/');
                        quantity = Decimal.Parse(value.Substring(0, backSlashPos).Trim());
                        weight = Decimal.Parse(value.Substring(backSlashPos + 1, value.Length - (backSlashPos + 1)).Trim());
                    }
                    else
                    {
                        var orderMethod = string.Empty;
                        if (string.IsNullOrEmpty(this.OrderMethod))
                        {
                            orderMethod = Properties.OrderMethod.QuantityAndWeight;
                        }
                        else
                        {
                            orderMethod = this.OrderMethod.Trim();
                        }

                        // order amount quantity or weight only - look at the order method property to determine which to use.
                        if (orderMethod.Equals(Properties.OrderMethod.Quantity))
                        {
                            // ordering by quantity
                            quantity = Decimal.Parse(value);
                        }
                        else if (orderMethod.Equals(Properties.OrderMethod.Weight) || orderMethod.Equals(Properties.OrderMethod.QuantityAndWeight))
                        {
                            // ordering by quantity
                            weight = Decimal.Parse(value);
                        }
                    }

                    saleDetail.QuantityOrdered = quantity;
                    saleDetail.WeightOrdered = weight;

                    // Send to the vm to add to the order.
                    Messenger.Default.Send(saleDetail, Token.AddManualRecentOrderItem);
                }
                catch (Exception ex)
                {
                    Messenger.Default.Send(ex.Message, Token.RecentOrderError);
                }
            }
        }

        /// <summary>
        /// Gets or sets the associated inventory item.
        /// </summary>
        public InventoryItem InventoryItem { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order1.
        /// </summary>
        public SaleDetail SaleDetailOrderMostRecent { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order1.
        /// </summary>
        public SaleDetail SaleDetailOrder1 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order2.
        /// </summary>
        public SaleDetail SaleDetailOrder2 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order3.
        /// </summary>
        public SaleDetail SaleDetailOrder3 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order4.
        /// </summary>
        public SaleDetail SaleDetailOrder4 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order5.
        /// </summary>
        public SaleDetail SaleDetailOrder5 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder6 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder7 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder8 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder9 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder10 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder11 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder12 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder13 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder14 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder15 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder16 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder17 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder18 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder19 { get; set; }

        /// <summary>
        /// Gets or sets the product sale item detail from order6.
        /// </summary>
        public SaleDetail SaleDetailOrder20 { get; set; }


        public SaleDetail SaleDetailOrder21 { get; set; }
        public SaleDetail SaleDetailOrder22 { get; set; }
        public SaleDetail SaleDetailOrder23 { get; set; }
        public SaleDetail SaleDetailOrder24 { get; set; }
        public SaleDetail SaleDetailOrder25 { get; set; }
        public SaleDetail SaleDetailOrder26 { get; set; }
        public SaleDetail SaleDetailOrder27 { get; set; }
        public SaleDetail SaleDetailOrder28 { get; set; }
        public SaleDetail SaleDetailOrder29 { get; set; }
        public SaleDetail SaleDetailOrder30 { get; set; }
        public SaleDetail SaleDetailOrder31 { get; set; }
        public SaleDetail SaleDetailOrder32 { get; set; }
        public SaleDetail SaleDetailOrder33 { get; set; }
        public SaleDetail SaleDetailOrder34 { get; set; }
        public SaleDetail SaleDetailOrder35 { get; set; }
        public SaleDetail SaleDetailOrder36 { get; set; }
        public SaleDetail SaleDetailOrder37 { get; set; }
        public SaleDetail SaleDetailOrder38 { get; set; }
        public SaleDetail SaleDetailOrder39 { get; set; }
        public SaleDetail SaleDetailOrder40 { get; set; }
        public SaleDetail SaleDetailOrder41 { get; set; }
        public SaleDetail SaleDetailOrder42 { get; set; }
        public SaleDetail SaleDetailOrder43 { get; set; }
        public SaleDetail SaleDetailOrder44 { get; set; }
        public SaleDetail SaleDetailOrder45 { get; set; }
        public SaleDetail SaleDetailOrder46 { get; set; }
        public SaleDetail SaleDetailOrder47 { get; set; }
        public SaleDetail SaleDetailOrder48 { get; set; }
        public SaleDetail SaleDetailOrder49 { get; set; }
        public SaleDetail SaleDetailOrder50 { get; set; }
        public SaleDetail SaleDetailOrder51 { get; set; }
        public SaleDetail SaleDetailOrder52 { get; set; }
        public SaleDetail SaleDetailOrder53 { get; set; }
        public SaleDetail SaleDetailOrder54 { get; set; }
        public SaleDetail SaleDetailOrder55 { get; set; }
        public SaleDetail SaleDetailOrder56 { get; set; }
        public SaleDetail SaleDetailOrder57 { get; set; }
        public SaleDetail SaleDetailOrder58 { get; set; }
        public SaleDetail SaleDetailOrder59 { get; set; }
        public SaleDetail SaleDetailOrder60 { get; set; }
        public SaleDetail SaleDetailOrder61 { get; set; }
        public SaleDetail SaleDetailOrder62 { get; set; }
        public SaleDetail SaleDetailOrder63 { get; set; }
        public SaleDetail SaleDetailOrder64 { get; set; }
        public SaleDetail SaleDetailOrder65 { get; set; }
        public SaleDetail SaleDetailOrder66 { get; set; }
        public SaleDetail SaleDetailOrder67 { get; set; }
        public SaleDetail SaleDetailOrder68 { get; set; }
        public SaleDetail SaleDetailOrder69 { get; set; }
        public SaleDetail SaleDetailOrder70 { get; set; }
        public SaleDetail SaleDetailOrder71 { get; set; }
        public SaleDetail SaleDetailOrder72 { get; set; }
        public SaleDetail SaleDetailOrder73 { get; set; }
        public SaleDetail SaleDetailOrder74 { get; set; }
        public SaleDetail SaleDetailOrder75 { get; set; }
        public SaleDetail SaleDetailOrder76 { get; set; }
        public SaleDetail SaleDetailOrder77 { get; set; }
        public SaleDetail SaleDetailOrder78 { get; set; }
        public SaleDetail SaleDetailOrder79 { get; set; }
        public SaleDetail SaleDetailOrder80 { get; set; }
        public SaleDetail SaleDetailOrder81 { get; set; }
        public SaleDetail SaleDetailOrder82 { get; set; }
        public SaleDetail SaleDetailOrder83 { get; set; }
        public SaleDetail SaleDetailOrder84 { get; set; }
        public SaleDetail SaleDetailOrder85 { get; set; }
        public SaleDetail SaleDetailOrder86 { get; set; }
        public SaleDetail SaleDetailOrder87 { get; set; }
        public SaleDetail SaleDetailOrder88 { get; set; }
        public SaleDetail SaleDetailOrder89 { get; set; }
        public SaleDetail SaleDetailOrder90 { get; set; }
        public SaleDetail SaleDetailOrder91 { get; set; }
        public SaleDetail SaleDetailOrder92 { get; set; }
        public SaleDetail SaleDetailOrder93 { get; set; }
        public SaleDetail SaleDetailOrder94 { get; set; }
        public SaleDetail SaleDetailOrder95 { get; set; }
        public SaleDetail SaleDetailOrder96 { get; set; }
        public SaleDetail SaleDetailOrder97 { get; set; }
        public SaleDetail SaleDetailOrder98 { get; set; }
        public SaleDetail SaleDetailOrder99 { get; set; }
        public SaleDetail SaleDetailOrder100 { get; set; }

        /// <summary>
        /// Returns the recent order corresponding to the input date.
        /// </summary>
        /// <param name="date">The order date.</param>
        /// <returns>The recent order corresponding to the input date.</returns>
        public static Sale GetRecentOrder(DateTime? date)
        {
            return RecentOrders.FirstOrDefault(order => order != null && order.CreationDate.ToString().Equals(date.ToString()));
        }

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
