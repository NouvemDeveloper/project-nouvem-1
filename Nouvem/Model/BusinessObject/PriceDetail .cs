﻿// -----------------------------------------------------------------------
// <copyright file="PriceDetail.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.ComponentModel;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    /// <summary>
    /// Class which models a price list detail.
    /// </summary>
    public class PriceDetail : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        private decimal price;

        /// <summary>
        /// The group price change flag.
        /// </summary>
        private bool groupPriceChange;

        /// <summary>
        /// Gets or sets a price list detail id.
        /// </summary>
        public int PriceListDetailID { get; set; }

        /// <summary>
        /// Gets or sets the associated price list.
        /// </summary>
        public int PriceListID { get; set; }

        /// <summary>
        /// Gets or sets the associated price list name.
        /// </summary>
        public string PriceListName { get; set; }

        /// <summary>
        /// Gets or sets the associated price list name.
        /// </summary>
        public string CustomerGroup { get; set; }

        /// <summary>
        /// Gets or sets the associated price list base name.
        /// </summary>
        public string BasePriceListName { get; set; }

        /// <summary>
        /// Gets or sets the associated in master id.
        /// </summary>
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the associated in master code.
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// Gets or sets the associated in master description.
        /// </summary>
        public string ItemDescription { get; set; }

        /// <summary>
        /// Gets or sets the associated in master group.
        /// </summary>
        public string ItemGroup { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        public decimal Price
        {
            get
            {
                return this.price;
            }

            set
            {
                value = Math.Round(value, ApplicationSettings.TouchscreenPricingMaskNo);
                this.price = value;
                this.OnPropertyChanged("Price");
            }
        }

        /// <summary>
        /// Gets or sets the label price.
        /// </summary>
        public decimal? LabelPrice { get; set; }

        /// <summary>
        /// Gets or sets the base price.
        /// </summary>
        public decimal BasePrice { get; set; }

        /// <summary>
        /// Gets or sets the associated order method id.
        /// </summary>
        public int NouOrderMethodID { get; set; }

        /// <summary>
        /// Gets or sets the associated label price method id.
        /// </summary>
        public int? NouPriceMethodIDLabel { get; set; }

        /// <summary>
        /// Gets or sets the associated order method name.
        /// </summary>
        public string NouOrderMethodName { get; set; }

        /// <summary>
        /// Gets or sets the associated price method name.
        /// </summary>
        public string NouPriceMethodName { get; set; }

        /// <summary>
        /// Gets or sets the associated price method id.
        /// </summary>
        public int NouPriceMethodID { get; set; }

        /// <summary>
        /// Gets or sets the associated price method id.
        /// </summary>
        public int? NouRecipePriceMethodID { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether manual pricing/methods is selected.
        /// </summary>
        public bool ManualPriceAndOrMethod { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether the selected product is to be removed from the list.
        /// </summary>
        public bool Remove { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether the selected product is part of a group price change.
        /// </summary>
        public bool GroupPriceChange
        {
            get
            {
                return this.groupPriceChange;
            }

            set
            {
                this.groupPriceChange = value;
                this.OnPropertyChanged("GroupPriceChange");
            }
        }

        /// <summary>
        /// Gets or sets the associated price list factor.
        /// </summary>
        public double PriceListFactor { get; set; }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
