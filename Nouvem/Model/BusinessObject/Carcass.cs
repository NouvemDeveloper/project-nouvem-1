﻿// -----------------------------------------------------------------------
// <copyright file="Carcass.cs" company="DEM Machines Limited">
// Copyright (c) DEM Machines Limited. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;
using Nouvem.Properties;
using Nouvem.Shared;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
 

    /// <summary>
    /// Class which models a carcass.
    /// </summary>
    public class Carcass
    {
        #region Fields

        /// <summary>
        /// The list of movements of the animal.
        /// </summary>
        private IList<AnimalMovement> carcassMovement = new List<AnimalMovement>();

        /// <summary>
        /// The list of tests of the animal.
        /// </summary>
        //private IList<CarcassTest> carcassTest = new List<CarcassTest>();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Carcass"/> class.
        /// </summary>
        protected Carcass()
        {
        }

        #endregion

        #region Public Interface

        /// <summary>
        /// Gets the carcass id.
        /// </summary>
        public int CarcassId { get; internal set; }

        /// <summary>
        /// Gets or sets the batch no associated with the carcass.
        /// </summary>
        public int BatchNo { get; set; }

        /// <summary>
        /// Gets or sets the herd no for the carcass.
        /// </summary>
        public string HerdNo { get; set; }

        /// <summary>
        /// Gets or sets the ear tag for the carcass.
        /// </summary>
        public string Eartag { get; set; }

        /// <summary>
        /// Gets or sets the animal category. (A,B,C)
        /// </summary>
        public NouCategory Category { get; set; }

        /// <summary>
        /// Gets or sets the dob status of the carcass.
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the breed status of the carcass.
        /// </summary>
        public string Breed { get; set; }

        /// <summary>
        /// Gets or sets the sex of the carcass.
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the animal has offspring.
        /// </summary>
        public bool? HasOffspring { get; set; }

        /// <summary>
        /// Gets or sets the tag of the offspring. Contains the ear tag of it's mother if there is no offspring.
        /// </summary>
        public string TagOfOffSpring { get; set; }

        /// <summary>
        /// Gets or sets the age in months of the carcass.
        /// </summary>
        public int? AgeInMonths { get; set; }

        /// <summary>
        /// Gets or sets the age in days of the carcass.
        /// </summary>
        public int? AgeInDays { get; set; }

        /// <summary>
        /// Gets or sets the number of moves of the carcass.
        /// </summary>
        public int? NumberOfMoves { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the animal herd is bord bia qas.
        /// </summary>
        public bool IsHerdQAS { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the animal is bord bia qas.
        /// </summary>
        public bool IsQAS { get; set; }

        public string IsHerdQasYesNo
        {
            get
            {
                return this.IsHerdQAS.BoolToYesNo();
            }
        }

        public string IsQasYesNo
        {
            get
            {
                return this.IsQAS.BoolToYesNo();
            }
        }

        public string IsImportedYesNo
        {
            get
            {
                return this.IsAnimalImport.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the date of last movement.
        /// </summary>
        public string DateOfLastMovement { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether if carcass is clipped.
        /// </summary>
        public bool? IsClipped { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether if carcass is a casualty.
        /// </summary>
        public bool? IsCasuality { get; set; }

        /// <summary>
        /// Gets or sets the value to indicate if the carcass is clean.
        /// </summary>
        public CleanlinessRating Cleanliness { get; set; }

        /// <summary>
        /// Gets or sets the country of origin..
        /// </summary>
        public string CountryOfOrigin { get; set; }

        /// <summary>
        /// Gets or sets the country of origin..
        /// </summary>
        public string HerdOfOrigin { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the animal is lame.
        /// </summary>
        public bool? IsLame { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether AIM was used to find the animal detail.
        /// </summary>
        public bool UseAimToValidate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the date of birth of the animal according to AIM.
        /// </summary>
        public string AimDateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the breed of the animal according to AIM.
        /// </summary>
        public string AimBreed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the dam date of birth of the animal according to AIM.
        /// </summary>
        public string AimDamDateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the dam breed of the animal according to AIM.
        /// </summary>
        public string AimDamBreed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the dam breed of the animal according to AIM.
        /// </summary>
        public string DamTagNum { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the dam breed of the animal according to AIM.
        /// </summary>
        public string DamEvent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the sex of the animal according to AIM.
        /// </summary>
        public string AimSex { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the animal is imported or not.
        /// </summary>
        public bool IsAnimalImport { get; set; }

        /// <summary>
        /// Gets or sets the date on which the animal is imported. Will be empty if no date exists.
        /// </summary>
        public string DateOfImport { get; set; }

        /// <summary>
        /// Gets or sets the status of the lot file. i.e. whether it has been generated or not.
        /// </summary>
        public int LotFileStatus { get; set; }

        /// <summary>
        /// Gets or sets the status of the lot file.
        /// </summary>
        public string LotFileStatusDescription { get; set; }

        /// <summary>
        /// Gets or sets the lot file number
        /// </summary>
        public int LotFileNo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the movements of the animal.
        /// </summary>
        public IList<AnimalMovement> CarcassMovements
        {
            get
            {
                return this.carcassMovement;
            }

            set
            {
                this.carcassMovement = value;
            }
        }

        ///// <summary>
        ///// Gets or sets a value indicating the tests performed on the animal.
        ///// </summary>
        //public IList<CarcassTest> CarcassTests
        //{
        //    get
        //    {
        //        return this.carcassTest;
        //    }

        //    set
        //    {
        //        this.carcassTest = value;
        //    }
        //}

        /// <summary>
        /// Gets or sets the AIM status for the carcass where it is approved or not.
        /// </summary>
        public string AimStatus { get; set; }

        /// <summary>
        /// Gets or sets the AIM animal status of the carcass, whether it has been moved or not.
        /// </summary>
        public string AimAnimalStatus { get; set; }

        /// <summary>
        /// Gets or sets the kill number of the carcass.
        /// </summary>
        public int? KillNumber { get; set; }

        /// <summary>
        /// Gets or sets the assigned carcass number..
        /// </summary>
        public int? CarcassNumber { get; set; }

        /// <summary>
        /// Gets or sets the total days residency of the carcass.
        /// </summary>
        public int? DaysResidencyTotal { get; set; }

        /// <summary>
        /// Gets or sets the current days residency of the carcass.
        /// </summary>
        public int? DaysResidencyCurrent { get; set; }

        /// <summary>
        /// Gets or sets the days residency on the previous farm of the carcass.
        /// </summary>
        public int? DaysResidencyPreviousFarm { get; set; }

        /// <summary>
        /// Gets or sets the band set on the sequencer for the carcass.
        /// </summary>
        public int? SequencerBand { get; set; }

        /// <summary>
        /// Gets or sets the band set on the grader for the carcass.
        /// </summary>
        public int? GraderBand { get; set; }

        /// <summary>
        /// Gets or sets the price paid for the carcass.
        /// </summary>
        public decimal? PricePaid { get; set; }

        /// <summary>
        /// Gets or sets the missing tag status of the carcass. 0 indicates that the tag is present, 1 is for tag missing, 2 is tag replaced.
        /// </summary>
        public int? MissingTagStatus { get; set; }

        /// <summary>
        /// Gets or sets the TB restriction status.
        /// </summary>
        public bool? IsTBRestricted { get; set; }

        /// <summary>
        /// Gets or sets the value which indicates if the belly is burst.
        /// </summary>
        public bool? BurstBelly { get; set; }

        /// <summary>
        /// Gets or sets the carcass kill date.
        /// </summary>
        public string KillDate { get; set; }

        ///// <summary>
        ///// Gets or sets the feed approval rating for a batch.
        ///// </summary>
        //public FeedApproval FeedApproval { get; set; }

        ///// <summary>
        ///// Gets or sets the grading detail for the carcass.
        ///// </summary>
        //public CarcassGradingDetail GradingDetail { get; set; }

        /// <summary>
        /// Gets or sets the bonusrating.
        /// </summary>
        public bool? BonusApproved { get; set; }

        /// <summary>
        /// Gets or sets the sequenced date.
        /// </summary>
        public string SequencedDate { get; set; }

        /// <summary>
        /// Gets or sets the grader category.
        /// </summary>
        public NouCategory GraderCategory { get; set; }

        /// <summary>
        /// Gets or sets the qas rating.
        /// </summary>
        public bool? QAS { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the factory herd match value is true
        /// </summary>
        public bool? FactoryHerdMatch { get; set; }

        /// <summary>
        /// Gets the bonus conversion
        /// </summary>
        public string BonusConverted
        {
            get
            {
                if (this.BonusApproved == true)
                {
                    return Constant.Approved;
                }

                return Constant.NotApproved;
            }
        }

        /// <summary>
        /// Gets the qas conversion
        /// </summary>
        public string QASConverted
        {
            get
            {
                if (this.QAS == true)
                {
                    return Constant.Approved;
                }

                return Constant.NotApproved;
            }
        }

        /// <summary>
        /// Gets or sets the russian approval rating
        /// </summary>
        public bool? RussianApproved { get; set; }

        /// <summary>
        /// Gets or sets the manual grade rating
        /// </summary>
        public int? ManualGradeId { get; set; }

        /// <summary>
        /// Gets or sets the manual grade rating
        /// </summary>
        public string Error { get; set; }

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="Carcass"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="Carcass"/> object.</returns>
        public static Carcass CreateNewCarcass()
        {
            return new Carcass();
        }

        /// <summary>
        /// Create a new <see cref="Carcass"/> with the parameters specified.
        /// </summary>
        /// <param name="eartag">The ear tag of the animal.</param>
        /// <param name="herdNo">The herd no of the animal.</param>
        /// <param name="batchNo">The batch no of the animal.</param>
        /// <param name="dateOfBirth">The date of birth of the animal.</param>
        /// <param name="ageMonths">The age in months of the animal, splitting the age into months and days.</param>
        /// <param name="ageDays">The age in days of the animal, splitting the age into months and days.</param>
        /// <param name="breed">The breed of the animal.</param>
        /// <param name="category">The category of the animal.</param>
        /// <param name="sex">The sex of the animal.</param>
        /// <param name="numberOfMoves">The number of (non-mart) moves for the animal.</param>
        /// <param name="dateOfLastMovement">The date of last movement for the animal.</param>
        /// <param name="isClipped">Whether the animal is clipped or not.</param>
        /// <param name="cleanliness">The cleanliness rating of the animal. (A, B or C).</param>
        /// <param name="isLame">Whether the animal is lame or not.</param>
        /// <param name="countryOfOrigin">The country of the origin of the animal.</param>
        /// <param name="isAnimalImported">Flag to indicate if the animal is imported.</param>
        /// <param name="importDate">The import date of the animal.</param>
        /// <param name="isTBRestricted">Whether the animal is Tb restricted or not.</param>
        /// <param name="useAimToValidate">Whether aim was used to validate the animal.</param>
        /// <param name="aimDateOfBith">The date of birth of the animal according to AIM.</param>
        /// <param name="aimBreed">The breed of the animal according to AIM.</param>
        /// <param name="aimSex">The sex of the animal according to AIM.</param>
        /// <param name="hasOffspring">Whether the animal has offspring.</param>
        /// <param name="tagOfOffspring">The tag of the offspring. The mothers tag is returned if it does not exists.</param>
        /// <param name="daysResidencyTotal">The total days residency for the animal. NB : Total refers to the last two farms where the herd numbers are valid.</param>
        /// <param name="daysResidencyCurrent">The number of days residency on the current farm.</param>
        /// <param name="daysResidencyPreviousFarm">The number of days residency on the previous farm.</param>
        /// <param name="aimStatus">The status of the animal details according to AIM.</param>
        /// <param name="aimAnimalStatus">The status of the animal according to AIM. Whether it has been moved or not.</param>
        /// <param name="feedApproval">The approval rating for the feed.</param>
        /// <param name="moves">The list of movements of the animal.</param>
        /// <param name="tests">The list or tests performed on the animal.</param>
        /// <param name="gradingDetail">The grading detail (weights, detained, condemned etc for the carcass).</param>
        /// <param name="lotFileStatus">The status of the lot file.</param>
        /// <param name="isCasualty">Whether the animal is a casualty or not.</param>
        /// <param name="carcassId">The id of the carcass.</param>
        /// <param name="killNumber">The kill number for the carcass.</param>
        /// <param name="carcassNumber">The carcass number.</param>
        /// <param name="sequencerBand">The band assigned at the sequencer.</param>
        /// <param name="graderBand">The band assigned at the grader.</param>
        /// <param name="pricePaid">The price paid for the carcass.</param>
        /// <param name="missingTagStatus">The missing tag status.</param>
        /// <param name="burstBelly">Whether the carcass has a burst belly.</param>
        /// <param name="bordBiaApproval">The assurance body approval rating.</param>
        /// <param name="killDate">The carcass kill date..</param>
        /// <param name="sequencedDate">The date on which the carcass was sequenced.</param>
        /// <param name="graderCategory">The kill category.</param>
        /// <param name="qas">qas rating.</param>
        /// <param name="factoryHerdMatch">The presenting/factory herd match.</param>
        /// <param name="russianApproved">The russian approved rating</param>
        /// <param name="damDOB">The dam dob.</param>
        /// <param name="damBreed">The dam breed.</param>
        /// <returns>A new <see cref="Carcass"/> object.</returns>
        public static Carcass CreateCarcass(
            string eartag,
            string herdNo,
            int batchNo,
            DateTime? dateOfBirth,
            int? ageMonths,
            int? ageDays,
            string breed,
            NouCategory category,
            string sex,
            int? numberOfMoves,
            string dateOfLastMovement,
            bool? isClipped,
            CleanlinessRating cleanliness,
            bool? isLame,
            string countryOfOrigin,
            bool isAnimalImported,
            string importDate,
            bool? isTBRestricted,
            bool useAimToValidate,
            string aimDateOfBith,
            string aimBreed,
            string aimSex,
            bool? hasOffspring,
            string tagOfOffspring,
            int? daysResidencyTotal,
            int? daysResidencyCurrent,
            int? daysResidencyPreviousFarm,
            string aimStatus,
            string aimAnimalStatus,
            //FeedApproval feedApproval,
            IList<AnimalMovement> moves,
            //IList<CarcassTest> tests,
            //CarcassGradingDetail gradingDetail,
            int lotFileStatus,
            bool? isCasualty,
            int carcassId = 0,
            int? killNumber = 0,
            int? carcassNumber = 0,
            int? sequencerBand = 0,
            int? graderBand = 0,
            decimal? pricePaid = 0,
            int? missingTagStatus = 0,
            bool? burstBelly = false,
            bool? bordBiaApproval = false,
            string killDate = "",
            string sequencedDate = "",
            NouCategory graderCategory = null,
            bool? qas = null,
            bool? factoryHerdMatch = true,
            bool? russianApproved = false,
            string damDOB = "",
            string damBreed = "")
        {
            return new Carcass
            {
                CarcassId = carcassId,
                Eartag = eartag,
                HerdNo = herdNo,
                AimStatus = aimStatus,
                DateOfBirth = dateOfBirth,
                AimDateOfBirth = aimDateOfBith,
                Breed = breed,
                AimBreed = aimBreed,
                Category = category,
                Sex = sex,
                AimSex = aimSex,
                KillNumber = killNumber,
                CarcassNumber = carcassNumber,
                HasOffspring = hasOffspring,
                TagOfOffSpring = tagOfOffspring,
                BatchNo = batchNo,
                NumberOfMoves = numberOfMoves,
                DaysResidencyTotal = daysResidencyTotal,
                DaysResidencyCurrent = daysResidencyCurrent,
                DaysResidencyPreviousFarm = daysResidencyPreviousFarm,
                AimAnimalStatus = aimAnimalStatus,
                AgeInMonths = ageMonths,
                AgeInDays = ageDays,
                IsClipped = isClipped,
                IsLame = isLame,
                UseAimToValidate = useAimToValidate,
                CountryOfOrigin = countryOfOrigin,
                IsAnimalImport = isAnimalImported,
                DateOfImport = importDate,
                Cleanliness = cleanliness,
                SequencerBand = sequencerBand,
                GraderBand = graderBand,
                PricePaid = pricePaid,
                MissingTagStatus = missingTagStatus,
                DateOfLastMovement = dateOfLastMovement,
                IsTBRestricted = isTBRestricted,
                CarcassMovements = moves,
                //CarcassTests = tests,
               // GradingDetail = gradingDetail,
                BurstBelly = burstBelly,
                BonusApproved = bordBiaApproval,
                KillDate = killDate,
               // FeedApproval = feedApproval,
                LotFileStatus = lotFileStatus,
                IsCasuality = isCasualty,
                SequencedDate = sequencedDate,
                GraderCategory = graderCategory,
                QAS = qas,
                FactoryHerdMatch = factoryHerdMatch,
                RussianApproved = russianApproved,
                AimDamBreed = damBreed,
                AimDamDateOfBirth = damDOB
            };
        }

        #endregion

        #endregion
    }
}

