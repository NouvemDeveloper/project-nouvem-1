﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    public class UserGroup: INotifyPropertyChanged
    {
        /// <summary>
        /// The group id.
        /// </summary>
        public int UserGroupID { get; set; }

        /// <summary>
        /// The group description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The group deleted date.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// The node id.
        /// </summary>
        public int NodeID { get; set; }

        /// <summary>
        /// The parent id.
        /// </summary>
        public int ParentID { get; set; }

        /// <summary>
        /// The selected falg.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The selected flag.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
                if (this.ParentID == 0)
                {
                    Messenger.Default.Send(Tuple.Create(value, this.UserGroupID), Token.UpdateModules);
                }
            }
        }

        /// <summary>
        /// The modules.
        /// </summary>
        public IList<UserGroup> Modules { get; set; } = new List<UserGroup>();

        /// <summary>
        /// The property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The event raise.
        /// </summary>
        /// <param name="propertyName">The property whose value will be broadcast.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
