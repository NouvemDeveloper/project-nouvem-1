﻿// -----------------------------------------------------------------------
// <copyright file="PartnerPartner.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using Nouvem.View.User;

namespace Nouvem.Model.BusinessObject
{
    using Nouvem.Model.DataLayer;

    public class User
    {
        /// <summary>
        /// The user master,
        /// </summary>
        public UserMaster UserMaster { get; set; }

        /// <summary>
        /// The user group.
        /// </summary>
        public UserGroup_ Group { get; set; }

        /// <summary>
        /// The user shortcuts.
        /// </summary>
        public IList<UserShortcut> UserShortcuts { get; set; }

        /// <summary>
        /// The user audit details.
        /// </summary>
        public Audit Audit { get; set; }

        /// <summary>
        /// Gets or sets a deep copy of the current user.
        /// </summary>
        public User Copy
        {
            get
            {
                return new User
                {
                    UserMaster = new UserMaster
                    {
                        ActiveFrom = this.UserMaster.ActiveFrom,
                        ActiveTo = this.UserMaster.ActiveTo,
                        InActiveFrom = this.UserMaster.InActiveFrom,
                        InActiveTo = this.UserMaster.InActiveTo,ChangeAtNextLogin = this.UserMaster.ChangeAtNextLogin,
                        Email = this.UserMaster.Email,
                        FullName = this.UserMaster.FullName,
                        LicenceDetailID = this.UserMaster.LicenceDetailID,
                        Password = this.UserMaster.Password,
                        Photo = this.UserMaster.Photo,
                        UserName = this.UserMaster.UserName,
                        UserMasterID = this.UserMaster.UserMasterID,
                        Mobile = this.UserMaster.Mobile,
                        PasswordNeverExpires = this.UserMaster.PasswordNeverExpires,
                        SuspendUser = this.UserMaster.SuspendUser,
                        UserGroupID = this.UserMaster.UserGroupID
                    }
                };
            }
        }
    }
}
