﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    public class AttributeMasterData : INotifyPropertyChanged
    {
        /// <summary>
        /// The batch flag.
        /// </summary>
        private bool batch;

        /// <summary>
        /// The transaction flag.
        /// </summary>
        private bool transaction;

        /// <summary>
        /// The default sql.
        /// </summary>
        private string defaultSQL;

        /// <summary>
        /// Gets or sets the reset id.
        /// </summary>
        private int? nouAttributeResetId;

        public AttributeMasterData()
        {
            this.VisibleProcessesForCombo = new List<object>();
            this.EditProcessesForCombo = new List<object>();
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int AttributeMasterId { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the macro.
        /// </summary>
        public string LoadMacro { get; set; }

        /// <summary>
        /// Gets or sets the macro.
        /// </summary>
        public string PostSelectionMacro { get; set; }

        /// <summary>
        /// Gets or sets the Traceability type.
        /// </summary>
        public string TraceabilityType { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int? AttributeTabNameId { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Gets or sets the reset id.
        /// </summary>
        public int? NouAttributeResetId
        {
            get
            {
                return this.nouAttributeResetId;
            }

            set
            {
                this.nouAttributeResetId = value;
                this.OnPropertyChanged("NouAttributeResetId");
                if (value.HasValue)
                {
                    Messenger.Default.Send(Tuple.Create(value, this.Batch), Token.AttributeTypeSelected);
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the template allocation is a workflow.
        /// </summary>
        public bool UseAsWorkflow { get; set; }

        /// <summary>
        /// Gets or sets the sql.
        /// </summary>
        public string DefaultSQL
        {
            get
            {
                return this.defaultSQL;
            }

            set
            {
                this.defaultSQL = value;
                this.OnPropertyChanged("DefaultSQL");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a batch transaction.
        /// </summary>
        public bool Batch
        {
            get
            {
                return this.batch;
            }

            set
            {
                this.batch = value;
                this.OnPropertyChanged("Batch");
                if (value)
                {
                    this.Transaction = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a serial transaction.
        /// </summary>
        public bool Transaction
        {
            get
            {
                return this.transaction;
            }

            set
            {
                this.transaction = value;
                this.OnPropertyChanged("Transaction");
                if (value)
                {
                    this.Batch = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is required before user can continue.
        /// </summary>
        public bool RequiredBeforeContinue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is required before user can complete.
        /// </summary>
        public bool RequiredBeforeCompletion { get; set; }

        /// <summary>
        /// Gets or sets the associated piece labels for the grid combo.
        /// </summary>
        public IList<object> EditProcessesForCombo { get; set; }

        /// <summary>
        /// Gets or sets the associated item labels for the grid combo.
        /// </summary>
        public IList<object> VisibleProcessesForCombo { get; set; }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
