﻿// -----------------------------------------------------------------------
// <copyright file="PartnerGroup.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    public class PartnerGroup
    {
        /// <summary>
        /// Gets or sets the group id.
        /// </summary>
        public int PartnerId { get; set; }

        /// <summary>
        /// Gets or sets the group name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the group has been deleted.
        /// </summary>
        public bool Deleted { get; set; }
    }
}
