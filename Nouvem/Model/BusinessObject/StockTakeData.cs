﻿// -----------------------------------------------------------------------
// <copyright file="StockTakeData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using Nouvem.Shared;

    public class StockTakeData : INotifyPropertyChanged
    {
        /// <summary>
        /// The wgt to move.
        /// </summary>
        private decimal? closingWgt;

        /// <summary>
        /// The qty to move.
        /// </summary>
        private decimal? closingQty;

        /// <summary>
        /// The wgt to move.
        /// </summary>
        private decimal? wgtOutstanding;

        /// <summary>
        /// The qty to move.
        /// </summary>
        private int? inmasterId;

        /// <summary>
        /// The qty to move.
        /// </summary>
        private int? batchId;

        /// <summary>
        /// The qty to move.
        /// </summary>
        private decimal? qtyOutstanding;

        public StockTakeData()
        {
            this.Wgt = 0;
            this.Qty = 0;
            this.ClosingQty = 0;
            this.ClosingWgt = 0;
            this.WgtOutstanding = 0;
            this.QtyOutstanding = 0;
        }

        public IList<StockTakeData> PalletStock { get; set; }

        /// <summary>
        /// The tosck result enumeration.
        /// </summary>
        public enum DetailType
        {
            /// <summary>
            /// Missing stock 
            /// </summary>
            Missing,

            /// <summary>
            /// Recovered stock.
            /// </summary>
            Recovered,

            /// <summary>
            /// Unknown stock.
            /// </summary>
            Unknown,


            /// <summary>
            /// Accounted for stock.
            /// </summary>
            AccountedFor
        }

        /// <summary>
        /// The details type.
        /// </summary>
        public DetailType StockDetailType { get; set; }

        /// <summary>
        /// Gets or sets the stock take detail id.
        /// </summary>
        public int StockMoveDetailID { get; set; }

        /// <summary>
        /// Gets or sets the stock take detail id.
        /// </summary>
        public int StockTakeDetailID { get; set; }

        /// <summary>
        /// Gets or sets the stock take id.
        /// </summary>
        public int StockTakeID { get; set; }

        /// <summary>
        /// Gets or sets the in master id.
        /// </summary>
        public int? INMasterID
        {
            get { return this.inmasterId; }
            set
            {
                this.inmasterId = value;
                this.OnPropertyChanged("INMasterID");
            }
        }

        /// <summary>
        /// Gets the in master name.
        /// </summary>
        public string INMasterName
        {
            get
            {
                if (this.INMasterID.HasValue)
                {
                    var localProduct = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.INMasterID);
                    if (localProduct != null)
                    {
                        return localProduct.Name;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the warehouse id.
        /// </summary>
        public int? WarehouseID { get; set; }

        /// <summary>
        /// Gets or sets the previous sub warehouse id.
        /// </summary>
        public int? WarehouseLocationIDPrevious { get; set; }

        /// <summary>
        /// Gets or sets the previous warehouse id.
        /// </summary>
        public int? WarehouseIDPrevious { get; set; }

        /// <summary>
        /// Gets or sets the sub warehouse id.
        /// </summary>
        public int? WarehouseLocationID { get; set; }

        /// <summary>
        /// Gets or sets the warehouse name.
        /// </summary>
        public string WarehouseName { get; set; }

        /// <summary>
        /// Gets or sets the warehouse name.
        /// </summary>
        public string SubWarehouseName { get; set; }

        /// <summary>
        /// Gets or sets the current warehouse name.
        /// </summary>
        public string CurrentWarehouseName { get; set; }

        /// <summary>
        /// Gets or sets the barcode.
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        /// Gets or sets the iuser id.
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public int? StockTransactionId { get; set; }

        /// <summary>
        /// Gets or sets the stock take opening quantity.
        /// </summary>
        public decimal? OpeningQty { get; set; }

        /// <summary>
        /// Gets or sets the stock take opening weight.
        /// </summary>
        public decimal? OpeningWgt { get; set; }

        /// <summary>
        /// Gets or sets the stock take closing weight.
        /// </summary>
        public decimal? ClosingQty
        {
            get
            {
                return this.closingQty;
            }

            set
            {
                this.closingQty = value;
                if (value.HasValue)
                {
                    value = Math.Round(value.ToDecimal(), 2);
                    this.QtyOutstanding = Math.Round(this.Qty.ToDecimal() - this.ClosingQty.ToDecimal(), 2);
                    if (this.QtyOutstanding < 0)
                    {
                        this.QtyOutstanding = 0;
                    }
                }

                this.OnPropertyChanged("ClosingQty");
            }
        }

        /// <summary>
        /// Gets or sets the stock take closing weight.
        /// </summary>
        public decimal? ClosingWgt
        {
            get
            {
                return this.closingWgt;
            }

            set
            {
                this.closingWgt = value;
                if (value.HasValue)
                {
                    value = Math.Round(value.ToDecimal(), 2);
                    this.WgtOutstanding = Math.Round(this.Wgt.ToDecimal() - this.ClosingWgt.ToDecimal(), 2);
                    if (this.WgtOutstanding < 0)
                    {
                        this.WgtOutstanding = 0;
                    }
                }

                this.OnPropertyChanged("ClosingWgt");
            }
        }

        /// <summary>
        /// Gets or sets the stock take closing quantity.
        /// </summary>
        public decimal? Qty { get; set; }

        /// <summary>
        /// Gets or sets the stock take closing quantity.
        /// </summary>
        public decimal? Wgt { get; set; }

        /// <summary>
        /// Gets or sets the stock take closing quantity.
        /// </summary>
        public int? BatchID
        {
            get { return this.batchId; }
            set
            {
                this.batchId = value;
                Messenger.Default.Send(this.batchId, Token.BatchNumberChanged);
            }
        }

        /// <summary>
        /// Gets or sets the stock take closing quantity.
        /// </summary>
        public decimal? QtyOutstanding
        {
            get
            {
                return this.qtyOutstanding;
            }

            set
            {
                this.qtyOutstanding = value;
                this.OnPropertyChanged("QtyOutstanding");
            }
        }

        /// <summary>
        /// Gets or sets the stock take closing quantity.
        /// </summary>
        public decimal? WgtOutstanding
        {
            get
            {
                return this.wgtOutstanding;
            }

            set
            {
                this.wgtOutstanding = value;
                this.OnPropertyChanged("WgtOutstanding");
            }
        }

        /// <summary>
        /// Gets or sets the stock deletion date.
        /// </summary>
        public DateTime? StockDeletionDate { get; set; }

        /// <summary>
        /// Gets or sets the stock take detail deletion date.
        /// </summary>
        public DateTime Deleted { get; set; }

        /// <summary>
        /// Gets or sets the stock take detail deletion date.
        /// </summary>
        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Gets the quantity variance.
        /// </summary>
        public decimal VarianceQty
        {
            get
            {
                return this.ClosingQty.ToDecimal() - this.OpeningQty.ToDecimal();
            }
        }

        /// <summary>
        /// Gets the weight variance.
        /// </summary>
        public decimal VarianceWgt
        {
            get
            {
                return this.ClosingWgt.ToDecimal() - this.OpeningWgt.ToDecimal();
            }
        }

        /// <summary>
        /// Gets the variance percentage.
        /// </summary>
        public double? VarianceQtyPercentage
        {
            get
            {
                if (this.OpeningQty == 0)
                {
                    // infinity
                    return null;
                }

                if (this.ClosingQty == 0)
                {
                    return 100;
                }

                return Math.Round((this.ClosingQty.ToDecimal() - this.OpeningQty.ToDecimal()) / this.OpeningQty.ToDecimal() * 100, 2).ToDouble();
            }
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}

