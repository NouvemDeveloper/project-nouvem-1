﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System.Collections.Generic;
    using Nouvem.Model.DataLayer;

    public class TraceabilityData
    {
        /// <summary>
        /// The asociated product.
        /// </summary>
        public static int INMasterId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is required to be filled out.
        /// </summary>
        public bool? Required { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is to be reset.
        /// </summary>
        public bool? Reset { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is at batch level.
        /// </summary>
        public bool? Batch { get; set; }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is at transaction level.
        /// </summary>
        public bool? Transaction { get; set; }

        /// <summary>
        /// Gets or sets the traceability type.
        /// </summary>
        public string TraceabilityType { get; set; }

        /// <summary>
        /// Gets or sets the traceability type id.
        /// </summary>
        public int TraceabilityTypeID { get; set; }

        /// <summary>
        /// Gets or sets the traceability template name.
        /// </summary>
        public string TraceabilityName { get; set; }

        /// <summary>
        /// Gets or sets the traceability template id.
        /// </summary>
        public int TraceabilityNameID { get; set; }

        /// <summary>
        /// Gets or sets the traceability template master id.
        /// </summary>
        public int TraceabilityMasterID { get; set; }

        /// <summary>
        /// Gets or sets the traceability master code.
        /// </summary>
        public string TraceabilityMasterCode { get; set; }

        /// <summary>
        /// Gets or sets the traceability master description.
        /// </summary>
        public string TraceabilityMasterDescription { get; set; }

        /// <summary>
        /// Gets or sets the traceability master SQL.
        /// </summary>
        public string TraceabilityMasterSQL { get; set; }

        /// <summary>
        /// Gets or sets the traceability master collection.
        /// </summary>
        public string TraceabilityMasterCollection { get; set; }

        /// <summary>
        /// Gets or sets the traceability method.
        /// </summary>
        public string TraceabilityMethod { get; set; }

        /// <summary>
        /// Gets or sets the traceability value.
        /// </summary>
        public string TraceabilityValue { get; set; }

        /// <summary>
        /// Gets or sets the based on date master.
        /// </summary>
        public DateMaster DateMasterBasedOn { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are scanning stock at dispatch.
        /// </summary>
        public bool ScanningAtDispatch { get; set; }

        /// <summary>
        /// Gets or sets the traceability id.
        /// </summary>
        public int TraceabilityId { get; set; }

        /// <summary>
        /// Gets or sets the associated, dynamically created, ui control name.
        /// </summary>
        public string ControlName { get; set; }

        /// <summary>
        /// Gets the date days.
        /// </summary>
        public IList<DateDay> DateDays { get; set; }

        /// <summary>
        /// Gets or sets the associated, dynamically created, serial ui control name.
        /// </summary>
        public string DynamicColumnName { get; set; }
    }
}
