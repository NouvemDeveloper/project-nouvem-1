﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class ProductionSpecification
    {
        public int INMasterSpecificationID { get; set; }
        public string Name { get; set; }
        public int INMasterID { get; set; }
        public string CuttingSpec { get; set; }
        public string PackingSpec{ get; set; }
        public byte[] SpecImage { get; set; }
        public IList<int> Partners { get; set; } = new List<int>();
    }
}
