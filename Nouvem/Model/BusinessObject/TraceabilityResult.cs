﻿// -----------------------------------------------------------------------
// <copyright file="TraceabilityResult.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System.Linq;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;

    public class TraceabilityResult
    {
        /// <summary>
        /// The traceability method name.
        /// </summary>
        private string nouTraceabilityMethod;

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public BatchNumber BatchNumber { get; set; }

        /// <summary>
        /// Gets or sets the batch number value.
        /// </summary>
        public string BatchNo { get; set; }

        /// <summary>
        /// Gets or sets the traceability method id.
        /// </summary>
        public int NouTraceabilityMethodID { get; set; }

        /// <summary>
        /// Gets or sets the traceability master id.
        /// </summary>
        public int? TraceabilityMasterID { get; set; }

        /// <summary>
        /// Gets or sets the date master id.
        /// </summary>
        public int? DatemasterID { get; set; }

        /// <summary>
        /// Gets or sets the quality master id.
        /// </summary>
        public int? QualityMasterID { get; set; }

        /// <summary>
        /// Gets or sets the field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the traceability id.
        /// </summary>
        public int TraceabilityId { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string TraceabilityValue { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating whether this is a batch (or transaction) type.
        /// </summary>
        public bool BatchAttribute { get; set; }

        /// <summary>
        /// Gets or sets the traceability method.
        /// </summary>
        public string NouTraceabilityMethod
        {
            get
            {
                return this.nouTraceabilityMethod;
            }

            set
            {
                this.nouTraceabilityMethod = value;
                if (!string.IsNullOrEmpty(value))
                {
                    this.NouTraceabilityMethodID = NouvemGlobal.TraceabilityMethods.First(x => x.Method.Trim().Equals(value.Trim())).NouTraceabilityMethodID;
                }
            }
        }
    }
}
