﻿// -----------------------------------------------------------------------
// <copyright file="Database.cs" company="Nouvem Limited">
// Copyright (c)Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    /// <summary>
    /// Database model.
    /// </summary>
    public class Database
    {
        #region Public Interface

        /// <summary>
        /// Gets or sets the database name.
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        public string CompanyName { get; set; }

        #endregion
    }
}
