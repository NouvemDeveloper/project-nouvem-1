﻿// -----------------------------------------------------------------------
// <copyright file="Location.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.Enum;

namespace Nouvem.Model.BusinessObject
{
    using System.Collections.Generic;

    public class Location
    {
        /// <summary>
        /// Gets or sets the warehouse id.
        /// </summary>
        public int WarehouseId { get; set; }

        /// <summary>
        /// Gets or sets the sub warehouse id.
        /// </summary>
        public int? WarehouseLocationId { get; set; }

        /// <summary>
        /// Gets or sets the sub warehouse id.
        /// </summary>
        public int? ProcessId { get; set; }

        /// <summary>
        /// Gets or sets the sub warehouse name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the warehouse name of the location the sub warehouse resides.
        /// </summary>
        public string PrimaryWarehouseName { get; set; }

        /// <summary>
        /// Gets or sets the sub warehouses.
        /// </summary>
        public IList<Location> SubLocations { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are using locations for batch selectiuon.
        /// </summary>
        public bool UsingLocationsForBatches { get; set; }

        /// <summary>
        /// Gets or sets the stock move process.
        /// </summary>
        public StockMovementMode StockMoveMode { get; set; }

        /// <summary>
        /// Gets the combined location/sub location name.
        /// </summary>
        public string FullName
        {
            get
            {
                if (!this.WarehouseLocationId.HasValue)
                {
                    return this.Name;
                }

                return string.Format("{0} - {1}", this.PrimaryWarehouseName, this.Name);
            }
        }
    }
}


