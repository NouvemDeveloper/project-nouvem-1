﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Model.DataLayer;

namespace Nouvem.Model.BusinessObject
{
    public class ReportEmail
    {
        public int ReportEmailLookUpID { get; set; }
        public int? BPContactID { get; set; }
        public int ReportID { get; set; }
        public int? NouModuleID { get; set; }
        public bool? ShowEmail { get; set; }
        public bool? UseAtModule { get; set; }
        public string ModuleName { get; set; }
        public string ReportName { get; set; }
        public string ReportParameter { get; set; }
        public BPContact BPContact { get; set; }
    }
}
