﻿// -----------------------------------------------------------------------
// <copyright file="WeightBandData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Nouvem.Model.BusinessObject
{
    public class WeightBandData
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int WeightBandId { get; set; }

        /// <summary>
        /// Gets or sets the group id.
        /// </summary>
        public int WeightBandGroupId { get; set; }

        /// <summary>
        /// Gets or sets the band name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the min wgt.
        /// </summary>
        public decimal MinWeight { get; set; }

        /// <summary>
        /// Gets or sets the max wgt.
        /// </summary>
        public decimal MaxWeight { get; set; }

        /// <summary>
        /// Gets or sets the ceiling weight flag.
        /// </summary>
        public bool ApplyCeilingWeight { get; set; }

        /// <summary>
        /// Gets or sets the deleted flag.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets or sets the deleted flag.
        /// </summary>
        public decimal? Price { get; set; }
    }
}
