﻿// -----------------------------------------------------------------------
// <copyright file="SaleDetail.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Drawing;
using System.Windows;
using System.Windows.Threading;
using Nouvem.BusinessLogic;

namespace Nouvem.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.Global;
    using Nouvem.Model.DataLayer;
    using Nouvem.Model.Enum;
    using Nouvem.Properties;
    using Nouvem.Shared;
    using Nouvem.ViewModel;
    using Nouvem.ViewModel.Sales;

    /// <summary>
    /// Generic class which models the sale orders (quote, sale etc)
    /// </summary>
    public class SaleDetail : INotifyPropertyChanged
    {
        #region field

        /// <summary>
        /// The in master id.
        /// </summary>
        private int inMasterID;

        private int? nouRecipePriceMethodID;

        private string summary;

        private int inmasterIdProductStock;

        /// <summary>
        /// The in master id.
        /// </summary>
        private string notes;

        private bool changingTolerance;

        /// <summary>
        /// The transaction count.
        /// </summary>
        private int transactionCount;

        private int mixes;

        private bool? useAllBatch;

        private bool? recordLostWeight;

        private bool? ignoreTolerances;

        private bool? allowManualWeightEntry;

        /// <summary>
        /// The scanner box count.
        /// </summary>
        private int boxCount;

        /// <summary>
        /// Gets or sets a receipe ingredient + tolerance.
        /// </summary>
        public decimal? plusTolerance = 0;

        /// <summary>
        /// Gets or sets a receipe ingredient + tolerance.
        /// </summary>
        public decimal? plusToleranceUOM = 0;

        /// <summary>
        /// Gets or sets a receipe ingredient + tolerance.
        /// </summary>
        public decimal? minusTolerance = 0;

        /// <summary>
        /// Gets or sets a receipe ingredient + tolerance.
        /// </summary>
        public decimal? minusToleranceUOM = 0;

        private string uiPartnerName;

        /// <summary>
        /// The total ex vat value.
        /// </summary>
        private decimal? totalExVAT;

        private decimal? costPrice;

        private PriceMethod costPriceMethod;

        /// <summary>
        /// The total inc vat value.
        /// </summary>
        private decimal? totalIncVAT;

        /// <summary>
        /// The total inc vat value.
        /// </summary>
        private decimal? costTotal;

        /// <summary>
        /// The total inc vat value.
        /// </summary>
        private decimal? margin;

        /// <summary>
        /// The vat percentage.
        /// </summary>
        private decimal? vat;

        /// <summary>
        /// The unit price.
        /// </summary>
        private decimal? unitPrice;

        /// <summary>
        /// The unit price.
        /// </summary>
        private decimal? unitPriceOnPriceBook;

        /// <summary>
        /// The old unit price.
        /// </summary>
        private decimal? unitPriceOld;

        /// <summary>
        /// The quantity ordered.
        /// </summary>
        private decimal? baseQuantity;

        /// <summary>
        /// The quantity ordered.
        /// </summary>
        private decimal? quantityOrdered;

        /// <summary>
        /// The quantity received.
        /// </summary>
        private decimal? quantityReceived;

        /// <summary>
        /// The weight ordered.
        /// </summary>
        private decimal? weightOrdered;

        /// <summary>
        /// The weight received.
        /// </summary>
        private decimal? weightReceived;

        /// <summary>
        /// The weight into production.
        /// </summary>
        private decimal? weightIntoProduction;

        /// <summary>
        /// The qty into production.
        /// </summary>
        private decimal? quantityIntoProduction;

        /// <summary>
        /// The weight out of production.
        /// </summary>
        private decimal? weightOutOfProduction;

        /// <summary>
        /// The qty out of production.
        /// </summary>
        private decimal? quantityOutOfProduction;

        /// <summary>
        /// The quantity received under 30.
        /// </summary>
        private int? quantityReceivedUTM;

        /// <summary>
        /// The quantity received under 30.
        /// </summary>
        private int? killQuantityReceived;

        /// <summary>
        /// The quantity received over 30.
        /// </summary>
        private int? quantityReceivedOTM;

        /// <summary>
        /// The lairage order total.
        /// </summary>
        private int? lairageOrderTotal;

        /// <summary>
        /// The live available stock quantity.
        /// </summary>
        private decimal? availableStockQuantity;

        /// <summary>
        /// The live available weight quantity.
        /// </summary>
        private decimal? availableStockWeight;

        /// <summary>
        /// The associated price list id.
        /// </summary>
        private int priceListID;

        /// <summary>
        /// Is the line filled flag.
        /// </summary>
        private bool isFilled;

        /// <summary>
        /// The stock quantity of the inventory item.
        /// </summary>
        private decimal? stockQuantity;

        /// <summary>
        /// The stock weight of the inventory item.
        /// </summary>
        private decimal? stockWeight;

        /// <summary>
        /// The discount price.
        /// </summary>
        private decimal? discountPrice { get; set; }

        /// <summary>
        /// Gets or sets the discount percentage.
        /// </summary>
        private decimal? discountPercentage;

        /// <summary>
        /// Flag as to whether the current product line can be ordered by quantity.
        /// </summary>
        private bool canOrderByQuantity;

        /// <summary>
        /// Flag as to whether the current product line can be ordered by weight.
        /// </summary>
        private bool canOrderByWeight;

        /// <summary>
        /// Flag, as to whether a discount percentage is to be updated from the discount price property.
        /// </summary>
        private bool updateDiscountPercentage = true;

        /// <summary>
        /// The amount (qty or wgt) received. (used for the touchcreen)
        /// </summary>
        private double? received;

        /// <summary>
        /// The amount (qty or wgt) ordered. (used for the touchcreen)
        /// </summary>
        private double? ordered;

        /// <summary>
        /// The amount (wgt) outstanding. (used for the touchcreen)
        /// </summary>
        private decimal weightOutstanding;

        /// <summary>
        /// The amount (qty) outstanding. (used for the touchcreen)
        /// </summary>
        private decimal quantityOutstanding;

        /// <summary>
        /// The qty delivered.
        /// </summary>
        private decimal? quantityDelivered;

        /// <summary>
        /// The wgt delivered.
        /// </summary>
        private decimal? weightDelivered;

        /// <summary>
        /// Flag, as to whether totals should be updated.
        /// </summary>
        private bool updateTotals = true;

        /// <summary>
        /// The goods receipt data.
        /// </summary>
        private IList<GoodsReceiptData> goodsReceiptDatas = new List<GoodsReceiptData>();

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="SaleDetail"/> class.
        /// </summary>
        public SaleDetail()
        {
            this.weightOrdered = 0;
            this.weightReceived = 0;
            this.weightDelivered = 0;
            this.quantityOrdered = 0;
            this.quantityReceived = 0;
            this.quantityDelivered = 0;
            this.quantityIntoProduction = 0;
            this.weightIntoProduction = 0;
            this.Mixes = 1;
            this.NouIssueMethodID = 1;
            this.StockDetails = new List<StockDetail>();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string Summary
        {
            get
            {
                return this.summary;
            }

            set
            {
                this.summary = value;
                this.OnPropertyChanged("Summary");
            }
        }

        public bool? IgnoreTolerances
        {
            get
            {
                return this.ignoreTolerances;
            }

            set
            {
                this.ignoreTolerances = value ?? false;
            }
        }

        public bool? UseAllBatch
        {
            get
            {
                return this.useAllBatch;
            }

            set
            {
                this.useAllBatch = value ?? false;
            }
        }

        public bool? RecordLostWeight
        {
            get
            {
                return this.recordLostWeight;
            }

            set
            {
                this.recordLostWeight = value ?? false;
            }
        }

        public bool? AllowManualWeightEntry
        {
            get
            {
                return this.allowManualWeightEntry;
            }

            set
            {
                this.allowManualWeightEntry = value ?? false;
            }
        }

        public decimal? TypicalBatchSize { get; set; }
        public bool SpecDisplayed { get; set; }
        public bool BackflushBatchDataRetrieved { get; set; }

        public string Notes
        {
            get { return this.notes;}
            set
            {
                this.notes = value;
                this.OnPropertyChanged("Notes");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are minus scanning.
        /// </summary>
        public int? OrderIndex { get; set; }

        /// <summary>
        /// Gets the order index, placing lines with no index at the bottom.
        /// </summary>
        public int OrderIndexAll
        {
            get
            {
                if (!this.OrderIndex.HasValue)
                {
                    return 1000;
                }

                return this.OrderIndex.ToInt();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are minus scanning.
        /// </summary>
        public int? SplitStockID { get; set; }

        public PROrder PROrder { get; set; }
        public int? PROrderID { get; set; }

        public bool IgnoreRecipeTolerance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are minus scanning.
        /// </summary>
        public bool DontRecalculateMargin { get; set; }

        /// <summary>
        /// Gets or sets the transaction count.
        /// </summary>
        public int TransactionCount
        {
            get
            {
                return this.transactionCount;
            }

            set
            {
                this.transactionCount = value;
                this.OnPropertyChanged("TransactionCount");
            }
        }

        public int DocketNo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are minus scanning.
        /// </summary>
        public bool MinusScanning { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are scanning stock.
        /// </summary>
        public bool ScannedStock { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the dispatch over line order amount warning has been given.
        /// </summary>
        public bool DispatchOverAmountWarningGiven { get; set; }

        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public int? NouIssueMethodID { get; set; }

        /// <summary>
        /// Gets the issie method type.
        /// </summary>
        public string NouIssueMethodName
        {
            get { return this.NouIssueMethod?.IssueType; }
        }

        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public NouIssueMethod NouIssueMethod { get; set; }

        private int? nouUOMID;
        /// <summary>
        /// Gets or sets an alias name.
        /// </summary>
        public int? NouUOMID
        {
            get { return this.nouUOMID; }
            set
            {
                this.nouUOMID = value;
                if (!value.IsNullOrZero())
                {
                    if (NouvemGlobal.ReceipePriceList == null)
                    {
                        NouvemGlobal.ReceipePriceList = new PriceMaster();
                    }

                    this.HandleUpdatedReceipePriceList();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether we are retrieving the current sale from the db.
        /// </summary>
        //public static bool PriceChangingOnDesktop { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the price has been manually changed.
        /// </summary>
        public bool PriceHasBeenManuallyChanged { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are retrieving the current sale from the db.
        /// </summary>
        public bool LoadingSale { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are retrieving the current sale from the db.
        /// </summary>
        public bool IgnoreStockRetrieval { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we a label is to be printed for this product.
        /// </summary>
        public bool DontPrintLabel { get; set; }

        /// <summary>
        /// Gets or sets the temperature.
        /// </summary>
        public string Temp { get; set; }

        /// <summary>
        /// Gets or sets the carcass trans id (used in into production carcass split mode).
        /// </summary>
        public int CarcassStockTransactionId { get; set; }

        /// <summary>
        /// Gets or sets the item/price list, price method.
        /// </summary>
        public PriceMethod PriceMethod { get; set; }

        public string PriceMethodShort
        {
            get
            {
                if (this.PriceMethod == PriceMethod.Quantity)
                {
                    return "Qty";
                }

                return "Wgt";
            }
        }

        /// <summary>
        /// Gets or sets a value indicating if we are pricing by nominal wgt.
        /// </summary>
        public bool PriceByTypicalWeight { get; set; }

        /// <summary>
        /// Gets or sets the stock mode.
        /// </summary>
        public NouStockMode NouStockMode { get; set; }

        /// <summary>
        /// Gets or sets the stock mode.
        /// </summary>
        public bool? IsHeaderProduct { get; set; }

        /// <summary>
        /// Gets or sets the stock mode.
        /// </summary>
        public int BoxCount
        {
            get { return this.boxCount; }
            set
            {
                this.boxCount = value;
                this.OnPropertyChanged("BoxCount");
            }
        }

        /// <summary>
        /// Converts the system stock mode to our enumeration type.
        /// </summary>
        public StockMode StockMode
        {
            get
            {
                try
                {
                    return this.NouStockMode != null
                        ? (StockMode)Enum.Parse(typeof(StockMode), this.NouStockMode.Name.Trim().Replace(" ",""))
                        : StockMode.Serial;
                }
                catch (Exception e)
                {
                    return StockMode.Serial;
                }
            }
        }

        /// <summary>
        /// Converts the system stock mode to our enumeration type.
        /// </summary>
        public bool IsBatchStockMode
        {
            get
            {
                return this.StockMode == StockMode.BatchQty || this.StockMode == StockMode.BatchWeight ||
                       this.StockMode == StockMode.BatchQtyAndWeight;
            }
        }

        /// <summary>
        /// Converts the system stock mode to our enumeration type.
        /// </summary>
        public bool IsProductStockMode
        {
            get
            {
                return this.StockMode == StockMode.ProductQty || this.StockMode == StockMode.ProductWeight ||
                       this.StockMode == StockMode.ProductQtyAndWeight;
            }
        }

        /// <summary>
        /// Converts the system stock mode to our enumeration type.
        /// </summary>
        public bool IsSerialStockMode
        {
            get
            {
                return this.StockMode == StockMode.Serial;
            }
        }

        /// <summary> 
        /// Gets or sets the update totals flag.
        /// </summary>
        public bool UpdateTotals
        {
            get
            {
                return this.updateTotals;
            }

            set
            {
                this.updateTotals = value;
            }
        }

        /// <summary>
        /// Gets or sets the sale detail id.
        /// </summary>
        public int SaleDetailID { get; set; }

        /// <summary>
        /// Gets or sets the sale id.
        /// </summary>
        public int SaleID { get; set; }

        /// <summary>
        /// Gets or sets the sale id.
        /// </summary>
        public int? Serial { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a sale deetail is to be edited.
        /// </summary>
        public bool EditTransaction { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether a sale deetail is to be edited.
        /// </summary>
        public decimal? DeductionRate { get; set; }

        /// <summary>
        /// Gets or sets the associated price list id.
        /// </summary>
        public int PriceListID
        {
            get
            {
                return this.priceListID;
            }

            set
            {
                // if no item selected, disable price list change
                if (this.inMasterID == 0)
                {
                    value = 0;
                }

                this.priceListID = value;
                this.OnPropertyChanged("PriceListID");

                if (this.LoadingSale)
                {
                    // We are retrieving sale data from the db, so we don't re-calculate totals.
                    this.LoadingSale = false;
                    return;
                }

                if (value > 0)
                {
                    this.HandleUpdatedPriceList(value);
                    if (this.updateTotals)
                    {
                        var activeVM = this.GetActiveDocument();
                        if (activeVM != null)
                        {
                            if (!ViewModelLocator.IsAPReceiptNull() && activeVM == ViewModelLocator.APReceiptStatic)
                            {
                                this.UpdateTotal(true);
                            }
                            else if (!ViewModelLocator.IsARDispatchNull() && activeVM == ViewModelLocator.ARDispatchStatic)
                            {
                                this.UpdateTotal(dispatch: true);
                            }
                            else
                            {
                                this.UpdateTotal();
                            }
                        }
                    }
                    else
                    {
                        this.updateTotals = true;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the inventory item.
        /// </summary>
        public INMaster INMaster { get; set; }

        /// <summary>
        /// Gets or sets the associated inventory item.
        /// </summary>
        public InventoryItem InventoryItem { get; set; }

        /// <summary>
        /// Gets or sets the parent sale order date.
        /// </summary>
        public DateTime? OrderDate { get; set; }

        /// <summary>
        /// Gets or sets the individual qty ordered on the goods in touchscreen i.e not the accumalative qty.
        /// </summary>
        public decimal TouchscreenQtyOrdered { get; set; }

        /// <summary>
        /// Gets the product nominal weight;
        /// </summary>
        public decimal NominalWeight
        {
            get
            {
                if (this.InventoryItem == null || this.InventoryItem.Master == null ||
                    this.InventoryItem.Master.NominalWeight == null)
                {
                    return 0;
                }

                return this.InventoryItem.Master.NominalWeight.ToDecimal();
            }
        }

        public int? OrderBatchID { get; set; }

        public int? AROrderDetailID { get; set; }

        public string OrderBatchNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current product line can be ordered by quantity.
        /// </summary>
        public bool CanOrderByQuantity
        {
            get
            {
                return this.canOrderByQuantity;
            }

            set
            {
                this.canOrderByQuantity = value;
                this.OnPropertyChanged("CanOrderByQuantity");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the current product line can be ordered by weight.
        /// </summary>
        public bool CanOrderByWeight
        {
            get
            {
                return this.canOrderByWeight;
            }

            set
            {
                this.canOrderByWeight = value;
                this.OnPropertyChanged("CanOrderByWeight");
            }
        }
       
        /// <summary>
        /// Gets or sets the inventory item id.
        /// </summary>
        public int INMasterID
        {
            get
            {
                return this.inMasterID;
            }

            set
            {
                this.inMasterID = value;
                this.OnPropertyChanged("INMasterID");

                if (NouvemGlobal.RetrieveProductStockSummary && !this.IgnoreStockRetrieval && value != this.inmasterIdProductStock)
                {
                    var data = DataManager.Instance.GetProductStockAndOrderData(value);
                    if (data != null)
                    {
                        this.Summary = data.Item1;
                        this.WgtOnOrder = data.Item2;
                        this.QtyOnOrder = data.Item3;
                        this.WgtInStock = data.Item4;
                        this.QtyInStock = data.Item5;
                    }
                    else
                    {
                        this.Summary = string.Empty;
                        this.WgtOnOrder = 0;
                        this.QtyOnOrder = 0;
                        this.WgtInStock = 0;
                        this.QtyInStock = 0;
                    }
                    
                    this.inmasterIdProductStock = value;
                }

                if (this.LoadingSale)
                {
                    // We are retrieving sale data from the db, so we don't re-calculate totals.
                    return;
                }

                this.HandleItemSelection(value);
            }
        }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        public string CustomerName { get; set; }

        private int? bpMasterID_Intake;

        public int? BPMasterID_Intake
        {
            get { return this.bpMasterID_Intake; }
            set
            {
                this.bpMasterID_Intake = value;
                this.UIPartnerName = this.PartnerName;
            }
        }
        
        public string PartnerName
        {
            get
            {
                if (this.BPMasterID_Intake.HasValue)
                {
                    var partner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == this.BPMasterID_Intake);
                    if (partner != null)
                    {
                        return partner.Details.Name;
                    }
                }
            
                return string.Empty;
            }
        }
       
        public string UIPartnerName
        {
            get { return this.uiPartnerName; }
            set
            {
                this.uiPartnerName = value;
                this.OnPropertyChanged("UIPartnerName");
            }
        }

        public int? BPMasterID_Dispatch { get; set; }

        public string DispatchCustomerName
        {
            get
            {
                if (this.BPMasterID_Dispatch.HasValue)
                {
                    var partner =
                        NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == this.BPMasterID_Dispatch);
                    if (partner != null)
                    {
                        return partner.Details.Name;
                    }
                }

                return string.Empty;
            }
        }

        public decimal? RunningBaseQuantity { get; set; }

        /// <summary>
        /// Gets or sets the quantity ordered.
        /// </summary>
        public decimal? BaseQuantity
        {
            get
            {
                return this.baseQuantity;
            }

            set
            {
                this.baseQuantity = value;
                this.OnPropertyChanged("BaseQuantity");
            }
        }

        public decimal? RecipeQuantityOrdered { get; set; }
        public decimal? RecipeWeightOrdered { get; set; }

        /// <summary>
        /// Gets or sets the quantity ordered.
        /// </summary>
        public decimal? QuantityOrdered
        {
            get
            {
                return this.quantityOrdered;
            }

            set
            {
                this.quantityOrdered = value;
                this.OnPropertyChanged("QuantityOrdered");

                //if (this.NominalWeight > 0)
                //{
                //    this.WeightOrdered = this.NominalWeight.ToDouble() * value.ToDouble();
                //}

                if (value != null && value > 0)
                {
                    if (NouvemGlobal.ReceipePriceList != null)
                    {
                        this.PriceMethod = PriceMethod.Quantity;
                        this.UpdateTotal();
                        return;
                    }

                    if (this.PriceMethod == PriceMethod.Quantity || this.PriceByTypicalWeight)
                    {
                        if (!this.IsEndDocument())
                        {
                            this.UpdateTotal();
                        }
                    }

                    this.QuantityOutstanding = value.ToDecimal();
                }
            }
        }

        /// <summary>
        /// Gets or sets the quantity received.
        /// </summary>
        public decimal? QuantityReceived
        {
            get
            {
                return this.quantityReceived;
            }

            set
            {
                this.quantityReceived = value;
                this.OnPropertyChanged("QuantityReceived");

                if (value != null && value > 0)
                {
                    if (this.PriceMethod == PriceMethod.Quantity)
                    {
                        this.UpdateTotal(true);
                    }

                    this.QuantityOutstanding = this.QuantityOrdered.ToDecimal() - value.ToDecimal();
                }
            }
        }

        /// <summary>
        /// Gets or sets the weight ordered.
        /// </summary>
        public decimal? WeightOrdered
        {
            get
            {
                return this.weightOrdered;
            }

            set
            {
                this.weightOrdered = value;
                this.OnPropertyChanged("WeightOrdered");

                if (value != null && value > 0)
                {
                    if (this.PriceMethod == PriceMethod.Weight)
                    {
                        if (!this.IsEndDocument())
                        {
                            this.UpdateTotal();
                        }
                    }

                    this.WeightOutstanding = value.ToDecimal();
                }
            }
        }

        public decimal? TransactionWgt { get; set; }
        public decimal? TransactionQuantity { get; set; }

        /// <summary>
        /// Gets or sets the weight received.
        /// </summary>
        public decimal? WeightReceived
        {
            get
            {
                return this.weightReceived;
            }

            set
            {
                this.weightReceived = value;
                this.OnPropertyChanged("WeightReceived");

                if (value != null && value > 0)
                {
                    if (this.PriceMethod == PriceMethod.Weight)
                    {
                        this.UpdateTotal(true);
                    }

                    this.WeightOutstanding = this.WeightOrdered.ToDecimal() - value.ToDecimal();
                }
            }
        }

        private decimal? qty;

        public decimal? Qty
        {
            get { return this.qty; }
            set
            {
                this.qty = value;
                this.OnPropertyChanged("Qty");
            }
        }
        public decimal? Wgt { get; set; }

        public bool RemarksDisplayed { get; set; }

        /// <summary>
        /// Gets or sets the quantity received under 30 months.
        /// </summary>
        public int? KillQuantityReceived
        {
            get
            {
                return this.killQuantityReceived;
            }

            set
            {
                this.killQuantityReceived = value;
                this.OnPropertyChanged("KillQuantityReceived");
            }
        }

        /// <summary>
        /// Gets or sets the quantity received under 30 months.
        /// </summary>
        public int? QuantityReceivedUTM
        {
            get
            {
                return this.quantityReceivedUTM;
            }

            set
            {
                this.quantityReceivedUTM = value;
                this.OnPropertyChanged("QuantityReceivedUTM");
                this.LairageOrderTotal = value.ToInt() + this.quantityReceivedOTM.ToInt();
            }
        }

        /// <summary>
        /// Gets or sets the quantity received over 30 months.
        /// </summary>
        public int? QuantityReceivedOTM
        {
            get
            {
                return this.quantityReceivedOTM;
            }

            set
            {
                this.quantityReceivedOTM = value;
                this.OnPropertyChanged("QuantityReceivedOTM");
                this.LairageOrderTotal = value.ToInt() + this.quantityReceivedUTM.ToInt();
            }
        }

        /// <summary>
        /// Gets or sets the quantity received over 30 months.
        /// </summary>
        public int? LairageOrderTotal
        {
            get
            {
                return this.lairageOrderTotal;
            }

            set
            {
                this.lairageOrderTotal = value;
                this.OnPropertyChanged("LairageOrderTotal");
            }
        }

        /// <summary>
        /// Gets or sets the quantity ordered.
        /// </summary>
        public decimal? AvailableStockQuantity
        {
            get
            {
                return this.availableStockQuantity;
            }

            set
            {
                this.availableStockQuantity= value;
                this.OnPropertyChanged("AvailableStockQuantity");
            }
        }

        /// <summary>
        /// Gets or sets the weight ordered.
        /// </summary>
        public decimal? AvailableStockWeight
        {
            get
            {
                return this.availableStockWeight;
            }

            set
            {
                this.availableStockWeight = value;
                this.OnPropertyChanged("AvailableStockWeight");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the goods in receipt detail is newly created i.e. has no corresponsing aporderDetail.
        /// </summary>
        public bool IsNewGoodsInReceiptDetail { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the goods in receipt detail is the currently selected sale detail.
        /// </summary>
        public bool IsSelectedSaleDetail { get; set; }

        /// <summary>
        /// The sale detail type.
        /// </summary>
        public ViewType SaleDetailType { get; set; }
        
        /// <summary>
        /// Gets or sets the quantity delivered.
        /// </summary>
        public decimal? QuantityDelivered
        {
            get
            {
                return this.quantityDelivered;
            }

            set
            {
                this.quantityDelivered = value;
                this.OnPropertyChanged("QuantityDelivered");

                if (value != null && (value > 0 || this.MinusScanning))
                {
                    //if (this.PriceMethod == PriceMethod.Quantity)
                    //{
                    //    this.UpdateTotal(dispatch: true);
                    //}
                    if (!this.LoadingSale)
                    {
                        this.UpdateTotal(dispatch: true);
                    }
                    
                    this.QuantityOutstanding = this.QuantityOrdered.ToDecimal() - value.ToDecimal();
                }
            }
        }

        /// <summary>
        /// Gets or sets the weight delivered.
        /// </summary>
        public decimal? WeightDelivered
        {
            get
            {
                return this.weightDelivered;
            }

            set
            {
                this.weightDelivered = value;
                this.OnPropertyChanged("WeightDelivered");

                if (value != null)
                {
                    //if (this.PriceMethod == PriceMethod.Quantity)
                    //{
                    //    this.UpdateTotal(dispatch: true);
                    //}

                    value = Math.Round(value.ToDecimal(), 3);

                    if (!this.LoadingSale)
                    {
                        this.UpdateTotal(dispatch: true);
                    }
           
                    this.WeightOutstanding = this.WeightOrdered.ToDecimal() - value.ToDecimal();
                }
            }
        }

        /// <summary>
        /// Gets or sets the weight issued to production.
        /// </summary>
        public decimal? WeightIntoProduction
        {
            get
            {
                return this.weightIntoProduction;
            }

            set
            {
                this.weightIntoProduction = value;
                this.OnPropertyChanged("WeightIntoProduction");
                this.WeightOutstanding = this.WeightOrdered.ToDecimal() - value.ToDecimal();
            }
        }

        /// <summary>
        /// Gets or sets the qty issued to production.
        /// </summary>
        public decimal? QuantityIntoProduction
        {
            get
            {
                return this.quantityIntoProduction;
            }

            set
            {
                this.quantityIntoProduction = value;
                this.OnPropertyChanged("QuantityIntoProduction");
                this.QuantityOutstanding = this.QuantityOrdered.ToDecimal() - value.ToDecimal();
            }
        }

        /// <summary>
        /// Gets or sets the weight issued to production.
        /// </summary>
        public decimal? WeightOutOfProduction
        {
            get
            {
                return this.weightOutOfProduction;
            }

            set
            {
                this.weightOutOfProduction = value;
                this.OnPropertyChanged("WeightOutOfProduction");
            }
        }

        /// <summary>
        /// Gets or sets the qty issued out of production.
        /// </summary>
        public decimal? QuantityOutOfProduction
        {
            get
            {
                return this.quantityOutOfProduction;
            }

            set
            {
                this.quantityOutOfProduction = value;
                this.OnPropertyChanged("QuantityOutOfProduction");
            }
        }

        /// <summary>
        /// Gets or sets the transaction weight.
        /// </summary>
        public decimal? TransactionWeight { get; set; }

        /// <summary>
        /// Gets or sets the transaction weight.
        /// </summary>
        public decimal? TransactionQty { get; set; }

        /// <summary>
        /// Gets or sets the transaction weight.
        /// </summary>
        public decimal? BatchStockRemaining { get; set; }

        public bool AutoBoxing { get; set; }
        public bool CompleteAutoBoxing { get; set; }

        public int AutoBoxQty { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an order item can be processed.
        /// </summary>
        public bool Process
        {
            get
            {
                return this.IsSelectedSaleDetail
                       && ((this.QuantityReceived.ToDouble() > 0 || this.WeightReceived.ToDouble() > 0)
                       || (this.QuantityDelivered.ToDouble() > 0 || this.WeightDelivered.ToDouble() > 0));
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public int? BatchNumberId
        {
            get
            {
                return this.Batch != null ? this.Batch.BatchNumberID : (int?)null;
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public bool UseCostPriceBook { get; set; }
        public bool? Enforce { get; set; }
        public bool CompleteBatch { get; set; }

        /// <summary>
        /// Flag, as to whether a manual serial entry has completed.
        /// </summary>
        public bool ManualEntryComplete { get; set; }

        /// <summary>
        /// Gets or sets the stock details.
        /// </summary>
        public IList<StockDetail> StockDetails { get; set; }

        /// <summary>
        /// Gets or sets the stock details.
        /// </summary>
        public IList<StockDetail> ProductionStockDetails { get; set; }

        /// <summary>
        /// The base batch id.
        /// </summary>
        public int? BasePROrderId { get; set; }

        /// <summary>
        /// Gets or sets the stock detail to process.
        /// </summary>
        public StockDetail StockDetailToProcess { get; set; }

        /// <summary>
        /// Gets or sets the stock quantity of the inventory item.
        /// </summary>
        public decimal? StockQuantity
        {
            get
            {
                return this.stockQuantity;
            }

            set
            {
                this.stockQuantity = value;
                this.AvailableStockQuantity = value;
            }
        }

        /// <summary>
        /// Gets or sets the stock weight of the inventory item.
        /// </summary>
        public decimal? StockWeight
        {
            get
            {
                return this.stockWeight;
            }

            set
            {
                this.stockWeight = value;
                this.AvailableStockWeight = value;
            }
        }

        /// <summary>
        /// Gets or sets the stock quantity of the inventory item.
        /// </summary>
        public int ProductId
        {
            get
            {
                if (this.InventoryItem != null)
                {
                    return this.InventoryItem.INMasterID.ToInt();
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the stock quantity of the inventory item.
        /// </summary>
        public decimal StockQty
        {
            get
            {
                if (this.InventoryItem != null)
                {
                    return this.InventoryItem.StockQty;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the stock weight of the inventory item.
        /// </summary>
        public decimal AvailableStockWgt
        {
            get
            {
                if (this.InventoryItem != null)
                {
                    return this.InventoryItem.AvailableStockWgt;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the stock quantity of the inventory item.
        /// </summary>
        public decimal AvailableStockQty
        {
            get
            {
                if (this.InventoryItem != null)
                {
                    return this.InventoryItem.AvailableStockQty;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the stock weight of the inventory item.
        /// </summary>
        public decimal StockWgt
        {
            get
            {
                if (this.InventoryItem != null)
                {
                    return this.InventoryItem.StockWgt;
                }

                return 0;
            }
        }

        public decimal WgtOnOrder { get; set; }
        public decimal QtyOnOrder { get; set; }
        public decimal WgtInStock { get; set; }
        public decimal QtyInStock { get; set; }

        public decimal StockWgtAvailable
        {
            get { return this.WgtInStock - this.WgtOnOrder; }
        }

        public decimal StockQtyAvailable
        {
            get { return this.QtyInStock - this.QtyOnOrder; }
        }

        public bool IsSufficientWeightInStockForOrder
        {
            get
            {
                if (this.WeightOrdered <= 0)
                {
                    return true;
                }

                return this.StockWgtAvailable - this.WeightOrdered.ToDecimal() >= 0;
            }
        }

        public bool IsSufficientQuantityInStockForOrder
        {
            get
            {
                if (this.QuantityOrdered <= 0)
                {
                    return true;
                }

                return this.StockQtyAvailable - this.QuantityOrdered.ToDecimal() >= 0;
            }
        }

        public bool IsSufficientStockForOrder
        {
            get { return this.IsSufficientWeightInStockForOrder && this.IsSufficientQuantityInStockForOrder; }
        }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal? UnitPriceOnPriceBook
        {
            get
            {
                return this.unitPriceOnPriceBook;
            }

            set
            {
                this.unitPriceOnPriceBook = value;
                this.OnPropertyChanged("UnitPriceOnPriceBook");
            }
        }
        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal? UnitPrice
        {
            get
            {
                return this.unitPrice;
            }

            set
            {
                this.unitPrice = value;
                this.OnPropertyChanged("UnitPrice");
            }
        }

        /// <summary>
        /// Gets or sets the old unit price value.
        /// </summary>
        public decimal? UnitPriceOld
        {
            get
            {
                return this.unitPriceOld;
            }

            set
            {
                this.unitPriceOld = value;
                this.OnPropertyChanged("UnitPriceOld");
            }
        }

        /// <summary>
        /// Gets or sets the discount amount.
        /// </summary>
        public decimal? DiscountAmount { get; set; }

        /// <summary>
        /// Gets or sets the discount price.
        /// </summary>
        public decimal? DiscountPrice
        {
            get
            {
                return this.discountPrice;
            }

            set
            {
                if (value == null)
                {
                    return;
                }

                this.discountPrice = value;
                this.OnPropertyChanged("DiscountPrice");

                //if (ApplicationSettings.DisableSaleOrderDiscounts)
                //{
                //    return;
                //}

                //if (this.UnitPrice != null && this.UnitPrice > 0)
                //{
                //    var activeVM = this.GetActiveDocument();
                //    if (activeVM != null)
                //    {
                //        if (!ViewModelLocator.IsAPReceiptNull() && activeVM == ViewModelLocator.APReceiptStatic)
                //        {
                //            this.UpdateTotal(true);
                //        }
                //        else if (!ViewModelLocator.IsARDispatchNull() && activeVM == ViewModelLocator.ARDispatchStatic)
                //        {
                //            this.UpdateTotal(dispatch: true);
                //        }
                //        else
                //        {
                //            this.UpdateTotal();
                //        }

                //        if (this.updateDiscountPercentage)
                //        {
                //            this.DiscountPercentage = Math.Round((this.UnitPrice.ToDecimal() - value.ToDecimal()) / this.UnitPrice.ToDecimal() * 100, 2).ToDecimal();
                //        }
                //    }
                //}
            }
        }
        
        /// <summary>
        /// Gets or sets the discount percentage.
        /// </summary>
        public decimal? DiscountPercentage
        {
            get
            {
                return this.discountPercentage;
            }

            set
            {
                this.discountPercentage = value;
                this.OnPropertyChanged("DiscountPercentage");

                if (ApplicationSettings.DisableSaleOrderDiscounts)
                {
                    return;
                }

                if (value != null && this.UnitPrice != null && this.UnitPrice > 0)
                {
                    this.updateDiscountPercentage = false;
                    var activeVM = this.GetActiveDocument();
                    if (activeVM != null)
                    {
                        if (!ViewModelLocator.IsAPReceiptNull() && activeVM == ViewModelLocator.APReceiptStatic)
                        {
                            this.UpdateTotal(true);
                        }
                        else if (!ViewModelLocator.IsARDispatchNull() && activeVM == ViewModelLocator.ARDispatchStatic)
                        {
                            this.UpdateTotal(dispatch: true);
                        }
                        else
                        {
                            this.UpdateTotal();
                        }
                    }
                    //var convertedDiscount = 100M - value.ToDecimal();
                    //this.DiscountPrice = Math.Round(this.UnitPrice.ToDecimal() / 100 * convertedDiscount, 2);
                }

                this.updateDiscountPercentage = true;
            }
        }

        /// <summary>
        /// Gets or sets th linee discount percentage.
        /// </summary>
        public decimal? LineDiscountPercentage { get; set; }

        /// <summary>
        /// Gets or sets the line discount amount.
        /// </summary>
        public decimal? LineDiscountAmount { get; set; }

        /// <summary>
        /// Gets or sets the unit price after discount.
        /// </summary>
        public decimal? UnitPriceAfterDiscount { get; set; }

        /// <summary>
        /// Gets or sets the product batch wgt.
        /// </summary>
        public int Mixes
        {
            get
            {
                return this.mixes;
            }

            set
            {
                this.mixes = value;
                this.OnPropertyChanged("Mixes");
            }
        }

        /// <summary>
        /// Gets or sets the product batch wgt.
        /// </summary>
        public int MixCount { get; set; }

        /// <summary>
        /// Gets or sets the vat rate.
        /// </summary>
        public decimal? VAT
        {
            get
            {
                return this.vat;
            }

            set
            {
                this.vat = value;
                this.OnPropertyChanged("VAT");
            }
        }
        
        /// <summary>
        /// Gets or sets the total ex vat rate.
        /// </summary>
        public decimal? TotalIncVAT
        {
            get
            {
                return this.totalIncVAT;
            }

            set
            {
                this.totalIncVAT = value;
                this.OnPropertyChanged("TotalIncVAT");
                this.CalculateCosts();
            }
        }

        /// <summary>
        /// Gets or sets the total ex vat rate.
        /// </summary>
        public decimal? CostTotal
        {
            get
            {
                return this.costTotal;
            }

            set
            {
                this.costTotal = value;
                this.OnPropertyChanged("CostTotal");
            }
        }

        /// <summary>
        /// Gets or sets the total ex vat rate.
        /// </summary>
        public decimal? Margin
        {
            get
            {
                return this.margin;
            }

            set
            {
                this.margin = value;
                this.OnPropertyChanged("Margin");
            }
        }

        /// <summary>
        /// Gets the vat charged.
        /// </summary>
        public decimal ItemVat
        {
            get
            {
                return Math.Round(this.TotalExVAT.ToDecimal()/100*this.VAT.ToDecimal(), 2);
            }
        }
        
        /// <summary>
        /// Gets or sets the total inc vat.
        /// </summary>
        public decimal? TotalExVAT
        {
            get
            {
                return this.totalExVAT;
            }
            set
            {
                this.totalExVAT = value;
                this.OnPropertyChanged("TotalExVAT");

                if (!this.LoadingSale)
                {
                    try
                    {
                        var rounding = NouvemGlobal.ReceipePriceList != null ? 5 : 2;
                        this.TotalIncVAT = Math.Round(value.ToDecimal() / 100 * (this.VAT.ToDecimal() + 100), rounding);
                    }
                    catch (Exception)
                    {
                    }
                }

                if (this.VAT.ToDecimal() == 0)
                {
                    this.TotalIncVAT = this.TotalExVAT;
                }
            }
        }

        /// <summary>
        /// Gets the order amounts and price in a concatenated string.
        /// </summary>
        public string OrderString
        {
            get
            {
                return string.Format("{0}/{1} {2}", this.QuantityOrdered.ToDouble(), this.WeightOrdered.ToDouble(), this.UnitPrice.ToDecimal());
            }
        }

        /// <summary>
        /// Gets the order amounts and price in a concatenated string.
        /// </summary>
        public string OrderStringDispatch
        {
            get
            {
                if (ApplicationSettings.RecentOrdersAtDispatchByDelivered)
                {
                    return string.Format("{0}/{1} {2}", this.QuantityDelivered.ToDouble(), this.WeightDelivered.ToDouble(), this.UnitPrice.ToDecimal());
                }

                return string.Format("{0}/{1} {2}", this.QuantityOrdered.ToDouble(), this.WeightOrdered.ToDouble(), this.UnitPrice.ToDecimal());
            }
        }

        public IList<App_GetOrderBatchData_Result> OrderBatchData { get; set; }

        /// <summary>
        /// Gets or sets the vat code id.
        /// </summary>
        public int? VatCodeID { get; set; }

        /// <summary>
        /// Gets or sets the user comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the goods receipt data.
        /// </summary>
        public IList<GoodsReceiptData> GoodsReceiptDatas
        {
            get
            {
                return this.goodsReceiptDatas;
            }

            set
            {
                this.goodsReceiptDatas = value;
            }
        }

        /// <summary>
        /// The transactions details.
        /// </summary>
        public IList<StocktransactionDetail> StockTransactionDetails { get; set; }

        /// <summary>
        /// Gets or sets the deleted date.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets or sets the order method.
        /// </summary>
        public string NouOrderMethod { get; set; }

        /// <summary>
        /// Gets or sets the batch.
        /// </summary>
        public BatchNumber Batch { get; set; }

        /// <summary>
        /// Gets or sets the concessions.
        /// </summary>
        public IList<Concession> Concessions { get; set; }

        /// <summary>
        /// Gets a value indicating whether the dispatch line is empty.
        /// </summary>
        public bool IsDispatchLineEmpty
        {
            get
            {
                return this.WeightDelivered.ToDouble() <= 0 && this.QuantityDelivered.ToDouble() <= 0;
            }
        }

        /// <summary>
        /// Gets or sets the batch number.
        /// </summary>
        public string BatchNo
        {
            get
            {
                return this.Batch != null ? this.Batch.Number : string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the amount (qty or wgt) received (touchscreen only).
        /// </summary>
        public double? Received
        {
            get
            {
                return this.received;
            }

            set
            {
                this.received = value;
                this.OnPropertyChanged("Received");
            }
        }

        /// <summary>
        /// Gets or sets the amount (qty or wgt) rordered (touchscreen only).
        /// </summary>
        public double? Ordered
        {
            get
            {
                return this.ordered;
            }

            set
            {
                this.ordered = value;
                this.OnPropertyChanged("Ordered");
            }
        }

        /// <summary>
        /// Gets or sets the amount (wgt) outstanding (touchscreen only).
        /// </summary>
        public decimal WeightOutstanding
        {
            get
            {
                return this.weightOutstanding;
            }

            set
            {
                if (value < 0)
                {
                    value = 0.00m;
                }

                //value = Math.Round(value, 5);
                this.weightOutstanding = value;
                this.OnPropertyChanged("WeightOutstanding");
            }
        }

        /// <summary>
        /// Gets or sets the amount (qty) outstanding (touchscreen only).
        /// </summary>
        public decimal QuantityOutstanding
        {
            get
            {
                return this.quantityOutstanding;
            }

            set
            {
                if (value < 0)
                {
                    value = 0.00m;
                }

                //value = Math.Round(value, 5);
                this.quantityOutstanding = value;
                this.OnPropertyChanged("QuantityOutstanding");
            }
        }

        /// <summary>
        /// Gets or sets a receipe ingredient + tolerance.
        /// </summary>
        public decimal? PlusTolerance
        {
            get
            {
                return this.plusTolerance;
            }

            set
            {
                this.plusTolerance = value;
                this.OnPropertyChanged("PlusTolerance");

                try
                {
                    if (this.changingTolerance)
                    {
                        return;
                    }

                    this.changingTolerance = true;
                    this.PlusToleranceUOM = Math.Round(this.QuantityOrdered.ToDecimal() / 100 * value.ToDecimal(),2);
                }
                catch (Exception e)
                {
                }
                finally
                {
                    this.changingTolerance = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a receipe ingredient + tolerance uom.
        /// </summary>
        public decimal? PlusToleranceUOM
        {
            get
            {
                return this.plusToleranceUOM;
            }

            set
            {
                this.plusToleranceUOM = value;
                this.OnPropertyChanged("PlusToleranceUOM");

                try
                {
                    if (this.changingTolerance)
                    {
                        return;
                    }
                    this.changingTolerance = true;
                    this.PlusTolerance = Math.Round(value.ToDecimal()/this.QuantityOrdered.ToDecimal() * 100, 2);
                }
                catch (Exception e)
                {
                }
                finally
                {
                    this.changingTolerance = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a receipe ingredient + tolerance.
        /// </summary>
        public decimal? MinusTolerance
        {
            get
            {
                return this.minusTolerance;
            }

            set
            {
                this.minusTolerance = value;
                this.OnPropertyChanged("MinusTolerance");

                try
                {
                    if (this.changingTolerance)
                    {
                        return;
                    }

                    this.changingTolerance = true;
                    this.MinusToleranceUOM = Math.Round(this.QuantityOrdered.ToDecimal() / 100 * value.ToDecimal(),2);
                }
                catch (Exception e)
                {
                }
                finally
                {
                    this.changingTolerance = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets a receipe ingredient + tolerance.
        /// </summary>
        public decimal? MinusToleranceUOM
        {
            get
            {
                return this.minusToleranceUOM;
            }

            set
            {
                this.minusToleranceUOM = value;
                this.OnPropertyChanged("MinusToleranceUOM");

                try
                {
                    if (this.changingTolerance)
                    {
                        return;
                    }

                    this.changingTolerance = true;
                    this.MinusTolerance = Math.Round(value.ToDecimal() / this.QuantityOrdered.ToDecimal() * 100, 2);
                }
                catch (Exception e)
                {
                }
                finally
                {
                    this.changingTolerance = false;
                }
            }
        }

        /// <summary>
        /// Gets the group id.
        /// </summary>
        public int INGroupID
        {
            get
            {
                return this.InventoryItem != null && this.InventoryItem.Group != null
                    ? this.InventoryItem.Group.INGroupID
                    : 0;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a detail is display only.
        /// </summary>
        public bool DisplayOnly { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a detail is display only.
        /// </summary>
        public bool? IsBox { get; set; }

        ///// <summary>
        ///// Gets a value indicating whether the current trasantion is a box transaction.
        ///// </summary>
        //public bool IsBoxStock
        //{
        //    get { return false; }
        //}

        #endregion

        #region method

        public void SetNouStockMode()
        {
            if (this.InventoryItem == null)
            {
                return;
            }

            var modeName = "Serial";
            if (this.InventoryItem.Master.NouStockModeID == 4)
            {
                modeName = "Batch Qty";
            }
            else if (this.InventoryItem.Master.NouStockModeID == 5)
            {
                modeName = "Batch Weight";
            }
            else if (this.InventoryItem.Master.NouStockModeID == 6)
            {
                modeName = "Batch Qty And Weight";
            }
            else if (this.InventoryItem.Master.NouStockModeID == 7)
            {
                modeName = "Product Qty";
            }
            else if (this.InventoryItem.Master.NouStockModeID == 8)
            {
                modeName = "Product Weight";
            }
            else if (this.InventoryItem.Master.NouStockModeID == 9)
            {
                modeName = "Product Qty And Weight";
            }

            this.NouStockMode = new NouStockMode {Name = modeName, NouStockModeID = this.InventoryItem.Master.NouStockModeID.ToInt() };
        }

        /// <summary>
        /// Calculates the cost total and margin.
        /// </summary>
        /// <param name="resetCostPrice">Flag, as to whether the cost price is to be regenerated.</param>
        public void CalculateCosts(bool resetCostPrice = false, bool useDelivery = false)
        {
            if (this.DontRecalculateMargin || ApplicationSettings.TouchScreenMode)
            {
                return;
            }

            try
            {
                if (this.TotalIncVAT.IsNullOrZero() || !this.UnitPrice.HasValue || this.INMasterID == 0)
                {
                    this.CostTotal = null;
                    this.Margin = null;
                    return;
                }

                if (!this.costPrice.HasValue || resetCostPrice)
                {
                    var costData =
                        DataManager.Instance.GetPriceListDetail(NouvemGlobal.CostPriceList.PriceListID, this.INMasterID);
                    this.costPrice = costData.Price;
                    this.costPriceMethod = costData.NouPriceMethodName == Constant.PriceByQty
                        ? PriceMethod.Quantity
                        : PriceMethod.Weight;
                }

                if (useDelivery)
                {
                    if (this.costPriceMethod == PriceMethod.Weight)
                    {
                        this.CostTotal = this.WeightDelivered.ToDecimal() * this.costPrice;
                    }
                    else
                    {
                        this.CostTotal = this.QuantityDelivered.ToDecimal() * this.costPrice;
                    }
                }
                else
                {
                    if (this.costPriceMethod == PriceMethod.Weight)
                    {
                        this.CostTotal = this.WeightOrdered.ToDecimal() * this.costPrice;
                    }
                    else
                    {
                        this.CostTotal = this.QuantityOrdered.ToDecimal() * this.costPrice;
                    }
                }

                this.Margin = Math.Round(((this.TotalIncVAT - this.CostTotal) / this.TotalIncVAT * 100).ToDecimal(), 2);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Gets the current product.
        /// </summary>
        public void SetInventoryItem()
        {
            this.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.inMasterID);
        }

        /// <summary>
        /// Sets the price method.
        /// </summary>
        public void SetPriceMethod()
        {
            if (this.priceListID == 0)
            {
                return;
            }

            var localPriceList =
                    NouvemGlobal.PriceListMasters.FirstOrDefault(
                        x => x.PriceListID == this.priceListID);

            if (localPriceList == null)
            {
                // use base price list
                localPriceList = NouvemGlobal.BasePriceList;
            }

            if (localPriceList != null)
            {
                // now get the price associated with this inventory item, belonging to the selected customer (partner) associated price list.
                var localPriceListDetail = DataManager.Instance.GetPriceListDetail(localPriceList.PriceListID,
                   this.inMasterID);

                if (localPriceListDetail == null)
                {
                    if (localPriceList.BasePriceListID == NouvemGlobal.BasePriceList.PriceListID)
                    {
                        localPriceListDetail = DataManager.Instance.GetPriceListDetail(NouvemGlobal.BasePriceList.PriceListID,
                              this.inMasterID);
                    }
                    else if (localPriceList.BasePriceListID == NouvemGlobal.CostPriceList.PriceListID)
                    {
                        localPriceListDetail = DataManager.Instance.GetPriceListDetail(NouvemGlobal.CostPriceList.PriceListID,
                          this.inMasterID);
                    }
                }

                if (localPriceListDetail != null)
                {
                    if (this.UnitPrice.IsNull())
                    {
                        var specialPrice = this.GetSpecialPrice();
                        if (specialPrice != null)
                        {
                            this.UnitPrice = specialPrice;
                        }
                        else
                        {
                            this.UnitPrice = localPriceListDetail.Price;
                        }
                    }

                    // get the product order method
                    var orderMethod = localPriceListDetail.NouOrderMethodName;
                    this.NouOrderMethod = OrderMethod.QuantityAndWeight;
                    this.CanOrderByQuantity = true;
                    this.CanOrderByWeight = true;

                    if (orderMethod.Equals(OrderMethod.Quantity))
                    {
                        // qty only
                        this.CanOrderByWeight = false;
                        this.NouOrderMethod = OrderMethod.Quantity;
                    }
                    else if (orderMethod.Equals(OrderMethod.Weight))
                    {
                        // wgt only
                        this.CanOrderByQuantity = false;
                        this.NouOrderMethod = OrderMethod.Weight;
                    }
                    else if (orderMethod.Equals(OrderMethod.QuantityOrWeight))
                    {
                        // wgt or qty 
                        this.CanOrderByQuantity = true;
                        this.CanOrderByWeight = true;
                        this.NouOrderMethod = OrderMethod.QuantityOrWeight;
                    }

                    // get the price method
                    this.PriceMethod = PriceMethod.Weight;

                    var localPriceMethod = DataManager.Instance.GetProductPriceMethod(this.inMasterID);
                    if (localPriceMethod != null)
                    {
                        if (localPriceMethod.Name.Equals(Constant.PriceByQty))
                        {
                            this.PriceMethod = PriceMethod.Quantity;
                        }
                        else if (localPriceMethod.Name.Equals(Constant.TypicalWeightXPrice))
                        {
                            this.PriceByTypicalWeight = true;
                        }
                    }
                    else
                    {
                        if (localPriceListDetail.NouPriceMethodName.Equals(Constant.PriceByQty))
                        {
                            this.PriceMethod = PriceMethod.Quantity;
                        }
                        else if (localPriceListDetail.NouPriceMethodName.Equals(Constant.TypicalWeightXPrice))
                        {
                            this.PriceByTypicalWeight = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the price method.
        /// </summary>
        public void SetPriceMethodOnly()
        {
            this.PriceMethod = PriceMethod.Weight;

            if (this.priceListID == 0)
            {
                return;
            }

            var localPriceList =
                    NouvemGlobal.PriceListMasters.FirstOrDefault(
                        x => x.PriceListID == this.priceListID);

            if (localPriceList == null)
            {
                // use base price list
                localPriceList = NouvemGlobal.BasePriceList;
            }

            if (localPriceList != null)
            {
                // now get the price associated with this inventory item, belonging to the selected customer (partner) associated price list.
                var localPriceListDetail = DataManager.Instance.GetPriceListDetail(localPriceList.PriceListID,
                    this.inMasterID);

                if (localPriceListDetail == null)
                {
                    if (localPriceList.BasePriceListID == NouvemGlobal.BasePriceList.PriceListID)
                    {
                        localPriceListDetail = 
                            DataManager.Instance.GetPriceListDetail(NouvemGlobal.BasePriceList.PriceListID,this.inMasterID);
                    }
                    else if (localPriceList.BasePriceListID == NouvemGlobal.CostPriceList.PriceListID)
                    {
                        localPriceListDetail = 
                            DataManager.Instance.GetPriceListDetail(NouvemGlobal.CostPriceList.PriceListID, this.inMasterID);
                    }
                }

                if (localPriceListDetail != null)
                {
                    //this.UnitPrice = localPriceListDetail.Price;

                    // get the product order method
                    var orderMethod = localPriceListDetail.NouOrderMethodName;
                    this.NouOrderMethod = OrderMethod.QuantityAndWeight;
                    this.CanOrderByQuantity = true;
                    this.CanOrderByWeight = true;

                    if (orderMethod.Equals(OrderMethod.Quantity))
                    {
                        // qty only
                        this.CanOrderByWeight = false;
                        this.NouOrderMethod = OrderMethod.Quantity;
                    }
                    else if (orderMethod.Equals(OrderMethod.Weight))
                    {
                        // wgt only
                        this.CanOrderByQuantity = false;
                        this.NouOrderMethod = OrderMethod.Weight;
                    }
                    else if (orderMethod.Equals(OrderMethod.QuantityOrWeight))
                    {
                        // wgt or qty 
                        this.CanOrderByQuantity = true;
                        this.CanOrderByWeight = true;
                        this.NouOrderMethod = OrderMethod.QuantityOrWeight;
                    }

                    // get the price method
                    var localPriceMethod = DataManager.Instance.GetProductPriceMethod(this.inMasterID);
                    if (localPriceMethod != null)
                    {
                        if (localPriceMethod.Name.Equals(Constant.PriceByQty))
                        {
                            this.PriceMethod = PriceMethod.Quantity;
                        }
                        else if (localPriceMethod.Name.Equals(Constant.TypicalWeightXPrice))
                        {
                            this.PriceByTypicalWeight = true;
                        }
                    }
                    else
                    {
                        if (localPriceListDetail.NouPriceMethodName.Equals(Constant.PriceByQty))
                        {
                            this.PriceMethod = PriceMethod.Quantity;
                        }
                        else if (localPriceListDetail.NouPriceMethodName.Equals(Constant.TypicalWeightXPrice))
                        {
                            this.PriceByTypicalWeight = true;
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Sets the price method.
        /// </summary>
        public void SetFirstPriceMethod()
        {
            this.PriceMethod = PriceMethod.Weight;
            if (this.InventoryItem == null)
            {
                return;
            }

            var localPriceMethod = DataManager.Instance.GetProductPriceMethod(this.InventoryItem.Master.INMasterID);
            if (localPriceMethod != null)
            {
                if (localPriceMethod.Name.Equals(Constant.PriceByQty))
                {
                    this.PriceMethod = PriceMethod.Quantity;
                }
                else if (localPriceMethod.Name.Equals(Constant.TypicalWeightXPrice))
                {
                    this.PriceByTypicalWeight = true;
                }
            }
            else
            {
                var localPriceListDetail =
                DataManager.Instance.GetFirstProductPriceListDetail(this.InventoryItem.Master.INMasterID);

                // get the price method
                if (localPriceListDetail != null && localPriceListDetail.NouPriceMethodName.Equals(Constant.PriceByQty))
                {
                    this.PriceMethod = PriceMethod.Quantity;
                }
                else if (localPriceListDetail != null && localPriceListDetail.NouPriceMethodName.Equals(Constant.TypicalWeightXPrice))
                {
                    this.PriceByTypicalWeight = true;
                }
            }
        }

        /// <summary>
        /// Updates the line total.
        /// </summary>
        /// <param name="goodsIn">In goods in mode flag.</param>
        /// <param name="dispatch">Dispatch mode flag.</param>
        public void UpdateTotal(bool goodsIn = false, bool dispatch = false)
        {
            // get the quantity and weight (if it's goods in then we use the received values)
            var localQty = this.quantityOrdered;
            var localWgt = this.weightOrdered;

            if (goodsIn)
            {
                localQty = this.quantityReceived;
                localWgt = this.weightReceived;
            }

            if (dispatch)
            {
                localQty = this.quantityDelivered;
                localWgt = this.weightDelivered;
            }

            // we use the product unit price, unless a discount unit price has been manually entered.
            //var localUnitPrice = this.DiscountPrice != null && this.DiscountPrice > 0
            //    ? this.DiscountPrice.ToDecimal()
            //    : this.unitPrice.ToDecimal();
            var localUnitPrice = this.unitPrice.ToDecimal();
            if (!this.DiscountPercentage.IsNullOrZero() && localUnitPrice > 0)
            {
                localUnitPrice = localUnitPrice - localUnitPrice.ToDecimalApplyPercentage(this.DiscountPercentage.ToDecimal());
            }

            var orderAmount = 0M;
            if (this.PriceMethod == PriceMethod.Weight)
            {
                if (localWgt != null && localWgt > 0)
                {
                    // order by wgt
                    orderAmount = localWgt.ToDecimal();

                    // update the live available wgt.
                    this.AvailableStockWeight = goodsIn ? this.StockWeight + localWgt.ToDecimal() : this.StockWeight - localWgt.ToDecimal();
                }

                //if (this.NominalWeight > 0 && this.PriceByTypicalWeight && orderAmount > 0)
                //{
                //    // product set up to use it's nominal weight for pricing.
                //    orderAmount = this.NominalWeight;
                //}
            }
            else
            {
                if (localQty != null && localQty > 0)
                {
                    // order by qty
                    orderAmount = localQty.ToDecimal();

                    // update the live available qty.
                    this.AvailableStockQuantity = goodsIn ? this.StockQuantity + localQty.ToDecimal() : this.StockQuantity - localQty.ToDecimal();
                }
            }

            if (this.NominalWeight == 0 && this.PriceByTypicalWeight)
            {
                // no nominal weight set up.
                orderAmount = 0;
            }

            if (this.NominalWeight > 0 && this.PriceByTypicalWeight && localQty.ToDouble() > 0)
            {
                // product set up to use it's nominal weight for pricing.
                orderAmount = this.NominalWeight * localQty.ToDecimal();
            }

            #region recipe

            if (this.nouRecipePriceMethodID.HasValue && NouvemGlobal.ReceipePriceList != null)
            {
                if (this.nouRecipePriceMethodID == 1 && this.NouUOMID == 2)
                {
                    if (this.NominalWeight > 0)
                    {
                        orderAmount = (this.QuantityOrdered / this.NominalWeight).ToDecimal();
                    }
                    else
                    {
                        orderAmount = 0;
                    }
                }
                else if (this.nouRecipePriceMethodID == 3 && this.NouUOMID == 1)
                {
                    if (this.NominalWeight > 0)
                    {
                        orderAmount = (this.QuantityOrdered * this.NominalWeight).ToDecimal();
                    }
                    else
                    {
                       orderAmount= 0;
                    }
                }
            }

            #endregion

            var rounding = NouvemGlobal.ReceipePriceList != null ? 5 : 2;
            this.TotalExVAT = Math.Round(orderAmount * localUnitPrice, rounding, MidpointRounding.AwayFromZero);

            // Instruct the vm to update it's sale totals.
            Messenger.Default.Send(Token.Message, Token.UpdateSaleOrder);
        }

        /// <summary>
        /// Updates the line total.(Doesn't send message to associated vm to update)
        /// </summary>
        /// <param name="goodsIn">In goods in mode flag.</param>
        /// <param name="dispatch">Dispatch mode flag.</param>
        public void UpdateTotalLocallyOnly(bool goodsIn = false, bool dispatch = false)
        {
            // get the quantity and weight (if it's goods in then we use the received values)
            var localQty = this.quantityOrdered;
            var localWgt = this.weightOrdered;

            if (goodsIn)
            {
                localQty = this.quantityReceived;
                localWgt = this.weightReceived;
            }

            if (dispatch)
            {
                localQty = this.quantityDelivered;
                localWgt = this.weightDelivered;
            }

            // we use the product unit price, unless a discount unit price has been manually entered.
            var localUnitPrice = this.DiscountPrice != null && this.DiscountPrice > 0
                ? this.DiscountPrice.ToDecimal()
                : this.unitPrice.ToDecimal();

            var orderAmount = 0M;
            if (this.PriceMethod == PriceMethod.Weight)
            {
                if (localWgt != null && localWgt > 0)
                {
                    // order by wgt
                    orderAmount = localWgt.ToDecimal();

                    // update the live available wgt.
                    this.AvailableStockWeight = goodsIn ? this.StockWeight + localWgt.ToDecimal() : this.StockWeight - localWgt.ToDecimal();
                }

                //if (this.NominalWeight > 0 && this.PriceByTypicalWeight && orderAmount > 0)
                //{
                //    // product set up to use it's nominal weight for pricing.
                //    orderAmount = this.NominalWeight;
                //}
            }
            else
            {
                if (localQty != null && localQty > 0)
                {
                    // order by qty
                    orderAmount = localQty.ToDecimal();

                    // update the live available qty.
                    this.AvailableStockQuantity = goodsIn ? this.StockQuantity + localQty.ToDecimal() : this.StockQuantity - localQty.ToDecimal();
                }
            }

            if (this.NominalWeight == 0 && this.PriceByTypicalWeight)
            {
                // no nominal weight set up.
                orderAmount = 0;
            }
            
            if (this.NominalWeight > 0 && this.PriceByTypicalWeight && localQty.ToDouble() > 0)
            {
                // product set up to use it's nominal weight for pricing.
                orderAmount = this.NominalWeight * localQty.ToDecimal();
            }

            this.TotalExVAT = Math.Round(orderAmount * localUnitPrice, 2, MidpointRounding.AwayFromZero);
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether the line is filled or not.
        /// </summary>
        public bool IsFilled
        {
            get
            {
                return this.isFilled;
            }
            set
            {
                this.isFilled = value;
                this.OnPropertyChanged("IsFilled");
            }
        }
        
        /// <summary>
        /// Is the line filled check.
        /// </summary>
        public bool CheckLineStatus()
        {
            if (string.IsNullOrWhiteSpace(this.NouOrderMethod))
            {
                this.SetPriceMethodOnly();
            }

            if (this.NouOrderMethod == OrderMethod.QuantityAndWeight)
            {
                this.IsFilled = (this.QuantityOrdered.ToDecimal() > 0 || this.WeightOrdered.ToDecimal() > 0) && this.QuantityOutstanding <= 0 && this.WeightOutstanding <= 0;
            }
            else if (this.NouOrderMethod == OrderMethod.QuantityOrWeight)
            {
                this.IsFilled = (this.QuantityOrdered.ToDecimal() > 0 && this.QuantityOutstanding <= 0) || (this.WeightOrdered.ToDecimal() > 0 && this.WeightOutstanding <= 0);
            }
            else if (this.NouOrderMethod == OrderMethod.Quantity)
            {
                this.IsFilled = (this.QuantityOrdered.ToDecimal() > 0) && this.QuantityOutstanding <= 0;
            }
            else
            {
                this.IsFilled = (this.WeightOrdered.ToDecimal() > 0) && this.WeightOutstanding <= 0;
            }

            return this.IsFilled;
        }

        /// <summary>
        /// Is the recipe line filled check.
        /// </summary>
        public bool CheckRecipeLineStatus()
        {
            if (this.NouOrderMethod.CompareIgnoringCase(Constant.Weight))
            {
                var start = this.WeightOrdered.ToDecimal() - (this.WeightOrdered.ToDecimal() / 100 * this.MinusTolerance);
                var end = this.WeightOrdered.ToDecimal() + (this.WeightOrdered.ToDecimal() / 100 * this.PlusTolerance);

                if (this.WeightIntoProduction.ToDecimal() >= start)
                {
                    this.IsFilled = true;
                }
            }
            else
            {
                var start = this.QuantityOrdered.ToDecimal() - (this.QuantityOrdered.ToDecimal() / 100 * this.MinusTolerance);
                var end = this.QuantityOrdered.ToDecimal() + (this.QuantityOrdered.ToDecimal() / 100 * this.PlusTolerance);

                if (this.QuantityIntoProduction.ToDecimal() >= start)
                {
                    this.IsFilled = true;
                }
            }

            return this.IsFilled;
        }

        #endregion

        #region event

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region private

        /// <summary>
        /// Gets the special price (if any) set up against the current product/partner data.
        /// </summary>
        /// <returns>A corresponding special price, if found.</returns>
        private decimal? GetSpecialPrice()
        {
            #region validation

            if (NouvemGlobal.SpecialPrices == null || !NouvemGlobal.SpecialPrices.Any())
            {
                return null;
            }

            var focusedSaleViewModel = this.GetActiveDocument();
            if (focusedSaleViewModel == null || !this.IsSaleOrderDocument(focusedSaleViewModel))
            {
                return null;
            }

            if (focusedSaleViewModel.SelectedPartner == null)
            {
                return null;
            }

            #endregion

            var partnerId = focusedSaleViewModel.SelectedPartner.BPMasterID;
            var groupId = focusedSaleViewModel.SelectedPartner.BPGroupID;

            // check product/partner combination first
            var localPrice = NouvemGlobal.SpecialPrices.FirstOrDefault(x => x.INMasterID == this.inMasterID && x.BPMasterID == partnerId);
            if (localPrice == null)
            {
                // ..check product/partner group combination next
                localPrice = NouvemGlobal.SpecialPrices.FirstOrDefault(x => x.INMasterID == this.inMasterID && x.BPGroupID == groupId);

                if (localPrice == null)
                {
                    // ..finally, check product/all partners combination
                    localPrice = NouvemGlobal.SpecialPrices
                        .FirstOrDefault(x => x.INMasterID == this.inMasterID && x.BPGroupID == null && x.BPMasterID == null);

                    if (localPrice == null)
                    {
                        return null;
                    }
                }
            }

            this.LoadingSale = true;
            //this.PriceListID = NouvemGlobal.SpecialPriceList.PriceListID;
            return localPrice.Price;
        }

        /// <summary>
        /// Handles an inventory item selection.
        /// </summary>
        /// <param name="itemId">The inventory item id.</param>
        private void HandleItemSelection(int itemId)
        {
            if (NouvemGlobal.ReceipePriceList != null)
            {
                this.HandleSelectedReceipeItem(itemId);
                return;
            }

            // Get the focused vm.
            var focusedSaleViewModel = this.GetActiveDocument();
            if (focusedSaleViewModel == null)
            {
                return;
            }

            if (focusedSaleViewModel.SelectedPartner != null)
            {
                // get the associated vat rate.
                this.InventoryItem =
                    NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == itemId);

                if (this.InventoryItem != null && this.InventoryItem.VatCode != null)
                {
                    this.VAT = this.InventoryItem.VatCode.Percentage.ToDecimal();
                    this.VatCodeID = this.InventoryItem.VatCode.VATCodeID;
                }

                if (focusedSaleViewModel.SelectedPartner.PriceListID == null)
                {
                    if (this.UseCostPriceBook)
                    {
                        this.PriceListID = NouvemGlobal.CostPriceList.PriceListID;
                    }
                    else
                    {
                        this.PriceListID = NouvemGlobal.BasePriceList.PriceListID;
                    }
                }
                else
                {
                    this.PriceListID = focusedSaleViewModel.SelectedPartner.PriceListID.ToInt();
                }
            }
            else
            {
                this.InventoryItem = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == itemId);
                if (this.InventoryItem != null && this.InventoryItem.VatCode != null)
                {
                    this.VAT = this.InventoryItem.VatCode.Percentage.ToDecimal();
                    this.VatCodeID = this.InventoryItem.VatCode.VATCodeID;
                }
            }
        }

        /// <summary>
        /// Handles an inventory item selection.
        /// </summary>
        /// <param name="itemId">The inventory item id.</param>
        public void HandleSelectedReceipeItem(int itemId)
        {
            // get the associated vat rate.
            this.InventoryItem =
                NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == itemId);

            if (this.InventoryItem != null && this.InventoryItem.VatCode != null)
            {
                this.VAT = this.InventoryItem.VatCode.Percentage.ToDecimal();
                this.VatCodeID = this.InventoryItem.VatCode.VATCodeID;
            }

            if (NouvemGlobal.ReceipePriceList != null && NouvemGlobal.ReceipePriceList.PriceListID > 0)
            {
                this.PriceListID = NouvemGlobal.ReceipePriceList.PriceListID;
            }
        }

        public IList<SaleDetail> InternalSaleDetails { get; set; }
     
        /// <summary>
        /// Handles a line price list change.
        /// </summary>
        /// <param name="pricingListId">The newly selected price list id.</param>
        private void HandleUpdatedPriceList(int pricingListId)
        {
            if (NouvemGlobal.ReceipePriceList != null)
            {
                this.HandleUpdatedReceipePriceList();
                return;
            }

            // Get the focused vm.
            var focusedSaleViewModel = this.GetActiveDocument();
            if (focusedSaleViewModel == null)
            {
                return;
            }

            if (focusedSaleViewModel.SelectedPartner == null || pricingListId == 0)
            {
                return;
            }

            var localPriceList =
                    focusedSaleViewModel.PriceLists.FirstOrDefault(
                        x => x.PriceListID == pricingListId);

            if (localPriceList == null)
            {
                // not on the customer price list so use base/cost price list
                if (this.UseCostPriceBook)
                {
                    localPriceList = NouvemGlobal.CostPriceList;
                }
                else
                {
                    localPriceList = NouvemGlobal.BasePriceList;
                }
            }

            if (localPriceList != null)
            {
                // now get the price associated with this inventory item, belonging to the selected customer (partner) associated price list.
                var localPriceListDetail = DataManager.Instance.GetPriceListDetail(localPriceList.PriceListID,
                    this.inMasterID);

                if (localPriceListDetail == null)
                {
                    var localBasePriceListId = this.UseCostPriceBook ? NouvemGlobal.CostPriceList.PriceListID : NouvemGlobal.BasePriceList.PriceListID;
                    var baseDetail = DataManager.Instance.GetPriceListDetail(localBasePriceListId,
                    this.inMasterID);
                        if (baseDetail == null)
                        {
                            this.PriceListID = 0;
                            return;
                        }
                        else
                        {
                            this.PriceListID = localBasePriceListId;
                            return;
                        }
                   // }
                   // else if (localPriceList.BasePriceListID == NouvemGlobal.CostPriceList.PriceListID)
                   // {
                        //localPriceListDetail = NouvemGlobal.CostPriceList.PriceListDetails
                        //.FirstOrDefault(x => x.INMasterID == this.inMasterID);
                    //    this.PriceListID = NouvemGlobal.CostPriceList.PriceListID;
                    //    return;
                   // }
                }

                if (localPriceListDetail != null)
                {
                    //if (!PriceChangingOnDesktop || this.UnitPrice.IsNull())
                    if (this.UnitPrice.IsNull())
                    {
                        var specialPrice = this.GetSpecialPrice();
                        if (specialPrice != null)
                        {
                            this.UnitPrice = specialPrice;
                        }
                        else
                        {
                            var localPrice = localPriceListDetail.Price;
                            this.UnitPrice = localPrice;
                        }

                        this.UnitPriceOld = this.UnitPrice;
                        this.UnitPriceOnPriceBook = this.UnitPrice;
                    }

                    // get the product order method
                    var orderMethod = localPriceListDetail.NouOrderMethodName;
                    this.NouOrderMethod = OrderMethod.QuantityAndWeight;
                    this.CanOrderByQuantity = true;
                    this.CanOrderByWeight = true;

                    if (orderMethod.Equals(OrderMethod.Quantity))
                    {
                        // qty only
                        this.CanOrderByWeight = false;
                        this.NouOrderMethod = OrderMethod.Quantity;
                    }
                    else if (orderMethod.Equals(OrderMethod.Weight))
                    {
                        // wgt only
                        this.CanOrderByQuantity = false;
                        this.NouOrderMethod = OrderMethod.Weight;
                    }
                    else if (orderMethod.Equals(OrderMethod.QuantityOrWeight))
                    {
                        // wgt or qty 
                        this.CanOrderByQuantity = true;
                        this.CanOrderByWeight = true;
                        this.NouOrderMethod = OrderMethod.QuantityOrWeight;
                    }

                    // get the price method
                    this.PriceMethod = PriceMethod.Weight;

                    var localPriceMethod = DataManager.Instance.GetProductPriceMethod(this.inMasterID);
                    if (localPriceMethod != null)
                    {
                        if (localPriceMethod.Name.Equals(Constant.PriceByQty))
                        {
                            this.PriceMethod = PriceMethod.Quantity;
                        }
                        else if (localPriceMethod.Name.Equals(Constant.TypicalWeightXPrice))
                        {
                            this.PriceByTypicalWeight = true;
                        }
                    }
                    else
                    {
                        if (localPriceListDetail.NouPriceMethodName.Equals(Constant.PriceByQty))
                        {
                            this.PriceMethod = PriceMethod.Quantity;
                        }
                        else if (localPriceListDetail.NouPriceMethodName.Equals(Constant.TypicalWeightXPrice))
                        {
                            this.PriceByTypicalWeight = true;
                        }
                    }
                }
                else
                {
                    //focusedSaleViewModel.HandleProductNotOnPriceList();
                }
            }
        }

        /// <summary>
        /// Handles a line price list change.
        /// </summary>
        private void HandleUpdatedReceipePriceList()
        {
            var localPriceList = NouvemGlobal.ReceipePriceList;
            this.nouRecipePriceMethodID = null;

            if (localPriceList != null)
            {
                // now get the price associated with this inventory item, belonging to the selected customer (partner) associated price list.
                var localPriceListDetail = DataManager.Instance.GetPriceListDetail(this.PriceListID,
                    this.inMasterID);

                // get the price method
                this.PriceMethod = PriceMethod.Quantity;
                if (localPriceListDetail != null)
                {
                    this.nouRecipePriceMethodID = localPriceListDetail.NouRecipePriceMethodID;
                    var localPrice = localPriceListDetail.Price;
                    this.UnitPrice = localPrice;
                    if (localPriceListDetail.NouRecipePriceMethodID.HasValue)
                    {
                        if (localPriceListDetail.NouRecipePriceMethodID == 4)
                        {
                            if (this.NominalWeight > 0)
                            {
                                this.UnitPrice = this.UnitPrice / this.NominalWeight;
                            }
                            else
                            {
                                this.UnitPrice = 0;
                            }
                        }
                    }
                   
                    // get the product order method
                    var orderMethod = localPriceListDetail.NouOrderMethodName;
                    this.NouOrderMethod = OrderMethod.QuantityAndWeight;
                    this.CanOrderByQuantity = true;
                    this.CanOrderByWeight = true;

                    if (orderMethod.Equals(OrderMethod.Quantity))
                    {
                        // qty only
                        this.CanOrderByWeight = false;
                        this.NouOrderMethod = OrderMethod.Quantity;
                    }
                    else if (orderMethod.Equals(OrderMethod.Weight))
                    {
                        // wgt only
                        this.CanOrderByQuantity = false;
                        this.NouOrderMethod = OrderMethod.Weight;
                    }
                    else if (orderMethod.Equals(OrderMethod.QuantityOrWeight))
                    {
                        // wgt or qty 
                        this.CanOrderByQuantity = true;
                        this.CanOrderByWeight = true;
                        this.NouOrderMethod = OrderMethod.QuantityOrWeight;
                    }
                    
                }
                else
                {
                    this.UnitPrice = null;
                }
            }

            this.UpdateTotal();
        }

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        /// <summary>
        /// Gets the focused sale vm.
        /// </summary>
        /// <returns>The focused sale vm.</returns>
        private SalesViewModelBase GetActiveDocument()
        {
            SalesViewModelBase focusedSaleViewModel = null;
            if (!ViewModelLocator.IsQuoteNull() && ViewModelLocator.QuoteStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.QuoteStatic;
            }
            else if (!ViewModelLocator.IsOrderNull() && ViewModelLocator.OrderStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.OrderStatic;
            }
            else if (!ViewModelLocator.IsOrder2Null() && ViewModelLocator.Order2Static.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.Order2Static;
            }
            else if (!ViewModelLocator.IsARDispatchNull() && ViewModelLocator.ARDispatchStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.ARDispatchStatic;
            }
            else if (!ViewModelLocator.IsARDispatch2Null() && ViewModelLocator.ARDispatch2Static.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.ARDispatch2Static;
            }
            else if (!ViewModelLocator.IsInvoiceNull() && ViewModelLocator.InvoiceStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.InvoiceStatic;
            }
            else if (!ViewModelLocator.IsARDispatchTouchscreenNull() && ViewModelLocator.ARDispatchTouchscreenStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.ARDispatchTouchscreenStatic;
            }
            else if (!ViewModelLocator.IsScannerMainDispatchNull() && ViewModelLocator.ScannerMainDispatchStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.ScannerMainDispatchStatic;
            }
            else if (!ViewModelLocator.IsAPQuoteNull() && ViewModelLocator.APQuoteStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.APQuoteStatic;
                this.UseCostPriceBook = true;
            }
            else if (!ViewModelLocator.IsAPOrderNull() && ViewModelLocator.APOrderStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.APOrderStatic;
                this.UseCostPriceBook = true;
            }
            else if (!ViewModelLocator.IsAPReceiptNull() && ViewModelLocator.APReceiptStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.APReceiptStatic;
                this.UseCostPriceBook = true;
            }
            else if (!ViewModelLocator.IsAPReceiptTouchscreenNull() && ViewModelLocator.APReceiptTouchscreenStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.APReceiptTouchscreenStatic;
                this.UseCostPriceBook = true;
            }
            else if (!ViewModelLocator.IsLairageNull() && ViewModelLocator.LairageStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.LairageStatic;
                this.UseCostPriceBook = true;
            }
            else if (!ViewModelLocator.IsAPInvoiceNull() && ViewModelLocator.APInvoiceStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.APInvoiceStatic;
                this.UseCostPriceBook = true;
            }
            else if (!ViewModelLocator.IsARReturnNull() && ViewModelLocator.ARReturnStatic.IsFocusedSaleModule)
            {
                focusedSaleViewModel = ViewModelLocator.ARReturnStatic;
                this.UseCostPriceBook = false;
            }

            return focusedSaleViewModel;
        }

        /// <summary>
        /// Determines if a document is an end document (goods receipt or dispatch).
        /// </summary>
        /// <returns>Flag, as to whether a document is an end document.</returns>
        private bool IsEndDocument()
        {
            var doc = this.GetActiveDocument();
            return
                   (!ViewModelLocator.IsAPReceiptNull() && doc == ViewModelLocator.APReceiptStatic)
                || (!ViewModelLocator.IsAPReceiptTouchscreenNull() && doc == ViewModelLocator.APReceiptTouchscreenStatic)
                || (!ViewModelLocator.IsARDispatchNull() && doc == ViewModelLocator.ARDispatchStatic)
                || (!ViewModelLocator.IsARDispatchTouchscreenNull() && doc == ViewModelLocator.ARDispatchTouchscreenStatic);
        }

        /// <summary>
        /// Determines if a document is a dispatch document.
        /// </summary>
        /// <returns>Flag, as to whether a document is a dispatch document.</returns>
        private bool IsSaleOrderDocument(SalesViewModelBase doc)
        {
            return
                   (!ViewModelLocator.IsARDispatchNull() && doc == ViewModelLocator.ARDispatchStatic)
                || (!ViewModelLocator.IsARDispatchTouchscreenNull() && doc == ViewModelLocator.ARDispatchTouchscreenStatic)
                || (!ViewModelLocator.IsOrderNull() && doc == ViewModelLocator.OrderStatic)
                || (!ViewModelLocator.IsQuoteNull() && doc == ViewModelLocator.QuoteStatic);
        }

        #endregion
    }
}
