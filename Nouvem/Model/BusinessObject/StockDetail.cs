﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Nouvem.Global;
using Nouvem.Model.DataLayer;
using Nouvem.Model.Enum;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;
using Attribute = Nouvem.Model.DataLayer.Attribute;

namespace Nouvem.Model.BusinessObject
{
    public class StockDetail : INotifyPropertyChanged
    {
        public IList<KillPaymentItem> Items { get; set; }

        /// <summary>
        /// The carcass side 1 wgt.
        /// </summary>
        private decimal weightSide1;

        /// <summary>
        /// The carcass side 2 wgt.
        /// </summary>
        private decimal weightSide2;

        /// <summary>
        /// The price.
        /// </summary>
        private decimal? unitPrice;

        /// <summary>
        /// The price.
        /// </summary>
        private decimal? adjustedUnitPrice;

        /// <summary>
        /// The price.
        /// </summary>
        private decimal? priceAdjust;

        /// <summary>
        /// The total price.
        /// </summary>
        private decimal? totalExclVat;

        /// <summary>
        /// The carcass grade.
        /// </summary>
        private string grade;

        /// <summary>
        /// Has the price been entered on the stock item.
        /// </summary>
        private bool usingStockPrice;

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public int WarehouseId { get; set; }

        /// <summary>
        /// The paid weight.
        /// </summary>
        private decimal? paidWeight { get; set; }

        /// <summary>
        /// Holds the current carcass side value.
        /// </summary>
        public enum Side
        {
            /// <summary>
            /// Carcass side 1.
            /// </summary>
            Side1,

            /// <summary>
            /// carcass side 2.
            /// </summary>
            Side2
        }

        public bool LogCategoryChange { get; set; }

        /// <summary>
        /// Gets a value indicating whether the current animal is to be graded whole.
        /// </summary>
        public bool GradeWholeBeefAnimal
        {
            get
            {
                if (ApplicationSettings.GradeVealAsWholeAnimalWithZ)
                {
                    return this.CategoryCAT.CompareIgnoringCase("V") || this.CategoryCAT.CompareIgnoringCase("Z");
                }

                return ApplicationSettings.GradeVealAsWholeAnimal &&
                       this.CategoryCAT.CompareIgnoringCase("V");
            }
        }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public StockTransaction Transaction { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        public DateTime? LotAuthorisedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are querying a herd cert at lairage.
        /// </summary>
        public bool DontQueryHerdCert { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public string ScannedIndicator { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public int? StockTransactionID { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public bool ScanningProductionStock { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public int BoxItemsCount { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public int PalletId { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public bool ReprintLabelAtDispatch { get; set; }

        /// <summary>
        /// Gets or sets the t id.
        /// </summary>
        public int StockDetailID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? SplitID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? BPMasterID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? MasterTableID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? BatchNumberID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? ProductionBatchID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? ProcessID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? BatchNumberBaseID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? IntakeID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? AttributeID { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public bool AutoSplittingStock { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int AutoSplittingStockId { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the killtype.
        /// </summary>
        public string KillType { get; set; }

        /// <summary>
        /// Gets or sets the killtype.
        /// </summary>
        public KillType NouKillType
        {
            get
            {
                try
                {
                    return (KillType) System.Enum.Parse(typeof(KillType), this.KillType);
                }
                catch (Exception)
                {
                    return Enum.KillType.Beef;
                }
            }
        }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int OTM { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int UTM { get; set; }

        /// <summary>
        /// Gets or sets the killed value.
        /// </summary>
        public int Killed { get; set; }

        /// <summary>
        /// Gets or sets the deleted flag.
        /// </summary>
        public DateTime? Deleted { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public int? APGoodsReceiptDetailID { get; set; }

        /// <summary>
        /// Gets or sets the labelid.
        /// </summary>
        public int? LabelID { get; set; }

        /// <summary>
        /// Gets or sets the labelid.
        /// </summary>
        public int? LabelIDSide1 { get; set; }

        /// <summary>
        /// Gets or sets the labelid.
        /// </summary>
        public int? LabelIDSide2 { get; set; }

        /// <summary>
        /// Gets or sets the supplier id.
        /// </summary>
        public int? SupplierID { get; set; }

        /// <summary>
        /// Gets or sets the carcass side.
        /// </summary>
        public Side CarcassSide { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the holding number.
        /// </summary>
        public string HoldingNumber { get; set; }

        private string notes;
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string Notes
        {
            get
            {
                return this.notes;
            }

            set
            {
                this.notes = value;
                this.OnPropertyChanged("Notes");
            }
        }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string LogInfo { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string ExtraNotes { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public bool LoadingStockDetail { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public bool RPA { get; set; }

        /// <summary>
        /// Gets or sets the transaction weight.
        /// </summary>
        public decimal? TransactionWeight { get; set; }

        /// <summary>
        /// Gets or sets the transaction qty.
        /// </summary>
        public decimal? TypicalBatchSize { get; set; }

        /// <summary>
        /// Gets or sets the transaction qty.
        /// </summary>
        public decimal? TransactionQty { get; set; }

        /// <summary>
        /// Gets or sets the transaction weight.
        /// </summary>
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// Gets or sets the attrribute id.
        /// </summary>
        public int? PalletID { get; set; }

        public int PalletNo { get; set; }
        public string PackDate { get; set; }
        public string UseBy { get; set; }
        public string PLU { get; set; }
        public string AnaCode { get; set; }
        public string UOM { get; set; }
        public string CaseUID { get; set; }
        public string PalletSerial { get; set; }
        public string FixedWeight { get; set; }
        public string FactoryOfSlaughter { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        public int? Serial { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        public int? StoredLabelId { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        public int StockTransactionIdToConsume { get; set; }

        /// <summary>
        /// Gets or sets the serial number.
        /// </summary>
        public int StockTransactionIdToUnconsume { get; set; }

        /// <summary>
        /// Gets or sets the side 1 wgt.
        /// </summary>
        public decimal WeightSide1
        {
            get
            {
                return this.weightSide1;
            }

            set
            {
                this.weightSide1 = value;
                this.OnPropertyChanged("WeightSide1");
            }
        }

        /// <summary>
        /// Gets or sets the side 2 wgt.
        /// </summary>
        public decimal WeightSide2
        {
            get
            {
                return this.weightSide2;
            }

            set
            {
                this.weightSide2 = value;
                this.OnPropertyChanged("WeightSide2");
            }
        }

        /// <summary>
        /// Formats to 1 decimal place.
        /// </summary>
        public decimal WeightSide2Formatted
        {
            get
            {
                return Math.Round(this.WeightSide2,1);
            }
        }

        /// <summary>
        /// Formats to 1 decimal place.
        /// </summary>
        public decimal TransactionWeightFormatted
        {
            get
            {
                return Math.Round(this.TransactionWeight.ToDecimal(), 1);
            }
        }

        /// <summary>
        /// Formats to 2 decimal place.
        /// </summary>
        public decimal TransactionWeight2DP
        {
            get
            {
                return Math.Round(this.TransactionWeight.ToDecimal(), 2);
            }
        }

        /// <summary>
        /// Formats to 1 decimal place.
        /// </summary>
        public decimal WeightSide1Formatted
        {
            get
            {
                return Math.Round(this.WeightSide1, 1);
            }
        }

        /// <summary>
        /// Gets or sets the total wgt.
        /// </summary>
        public decimal TotalCarcassWeight
        {
            get
            {
                return this.weightSide1 + this.weightSide2;
            }
        }

        /// <summary>
        /// Gets or sets the total cold wgt.
        /// </summary>
        public decimal TotalCarcassColdWeight
        {
            get
            {
                return this.TotalCarcassWeight * .98M;
            }
        }

        /// <summary>
        /// Gets or sets the fat colour.
        /// </summary>
        public NouFatColour FatColour { get; set; }

        /// <summary>
        /// Gets or sets the fat colour abbreviation.
        /// </summary>
        public string FatColourName
        {
            get
            {
                if (this.FatColour == null)
                {
                    return "White";
                }

                return this.FatColour.Name;
            }
        }

        /// <summary>
        /// Gets or sets the tarewgt.
        /// </summary>
        public bool ManualWeight { get; set; }

        /// <summary>
        /// Gets or sets the tarewgt.
        /// </summary>
        public decimal Tare { get; set; }

        /// <summary>
        /// Gets or sets the tarewgt.
        /// </summary>
        public double ProductTare { get; set; }

        /// <summary>
        /// Gets or sets the tarewgt.
        /// </summary>
        public BatchNumber BatchNumber { get; set; }

        /// <summary>
        /// Gets or sets the tarewgt.
        /// </summary>
        public StockAttribute StockAttribute { get; set; }

        /// <summary>
        /// Gets or sets the grade.
        /// </summary>
        public string Grade
        {
            get
            {
                return this.grade;
            }

            set
            {
                this.grade = value;
                this.OnPropertyChanged("Grade");
            }
        }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Eartag { get; set; }
        public string Conformation { get; set; }
        public string FatScore { get; set; }

        /// <summary>
        /// Gets or sets the herd.
        /// </summary>
        public string PresentingHerdNo { get; set; }

        /// <summary>
        /// Gets or sets the herd.
        /// </summary>
        public string HerdNo { get; set; }

        /// <summary>
        /// Gets or sets the herd.
        /// </summary>
        public string HerdOfOrigin { get; set; }

        /// <summary>
        /// Gets or sets the date of last movement.
        /// </summary>
        public string DateOfLastMovement { get; set; }

        /// <summary>
        /// Gets or sets the herd.
        /// </summary>
        public string DamEvent { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public NouBreed Breed { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string BreedName { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public bool HasOffSpring { get; set; }

        public string LotNo { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string TagOfOffspring { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string DamBreedName { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public int? Label1 { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public int? Label2 { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public int? NouDocStatusID { get; set; }

        /// <summary>
        /// Gets a value indicating if the aniaml is from a completed lot.
        /// </summary>
        public bool CompletedLot
        {
            get
            {
                return this.NouDocStatusID.HasValue && (this.NouDocStatusID == NouvemGlobal.NouDocStatusComplete.NouDocStatusID 
                       || this.NouDocStatusID == NouvemGlobal.NouDocStatusMoved.NouDocStatusID);
            }
        }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public int? Alibi { get; set; }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public int? StockTransactionID_Container { get; set; }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public bool? IsBox { get; set; }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public bool Update { get; set; }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public int? Reference { get; set; }

        public string CarcassBreed { get; set; }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public bool? InLocation { get; set; }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public DateTime? Consumed { get; set; }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public int? ContainerID { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public int? BreedID
        {
            get
            {
                if (this.Breed != null)
                {
                    return this.Breed.NouBreedID;
                }

                return (int?) null;
            }
        }

        /// <summary>
        /// Gets or sets the breed type.
        /// </summary>
        public string BreedType
        {
            get
            {
                if (this.Breed != null)
                {
                    return this.Breed.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public int? CarcassSideNo
        {
            get
            {
                if (this.CarcassSide == Side.Side1)
                {
                    return 1;
                }

                return 2;
            }
        }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal? UnitPrice
        {
            get
            {
                return this.unitPrice;
            }

            set
            {
                this.unitPrice = value;
                //if (!this.LoadingStockDetail)
                //{
                //    this.CalculateTotals();
                //}

                this.OnPropertyChanged("UnitPrice");
                this.SetAdjustedPrice();
            }
        }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal? AdjustedUnitPrice
        {
            get
            {
                return this.adjustedUnitPrice;
            }

            set
            {
                this.adjustedUnitPrice = value;
                if (!this.LoadingStockDetail)
                {
                    this.CalculateTotals();
                }

                this.OnPropertyChanged("AdjustedUnitPrice");
            }
        }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal? PriceAdjust
        {
            get
            {
                return this.priceAdjust;
            }

            set
            {
                this.priceAdjust = value;
                this.OnPropertyChanged("PriceAdjust");
            }
        }

        /// <summary>
        /// Gets or sets the unit price after discount.
        /// </summary>
        public decimal? UnitPriceAfterDiscount { get; set; }

        /// <summary>
        /// Gets or sets the discount price.
        /// </summary>
        public decimal? DiscountPrice { get; set; }

        /// <summary>
        /// Gets or sets the discount percentage.
        /// </summary>
        public decimal? DiscountPercentage { get; set; }

        /// <summary>
        /// Gets or sets the discount amt.
        /// </summary>
        public decimal? DiscountAmount { get; set; }

        /// <summary>
        /// Gets or sets the vat.
        /// </summary>
        public decimal? VAT { get; set; }

        /// <summary>
        /// Gets or sets the vat code id.
        /// </summary>
        public int? VatCodeId { get; set; }

        /// <summary>
        /// Gets or sets the total with vat.
        /// </summary>
        public decimal? TotalIncVat { get; set; }

        /// <summary>
        /// Gets or sets the total ex vat.
        /// </summary>
        public decimal? TotalExclVat
        {
            get
            {
                return this.totalExclVat;
            }

            set
            {
                this.totalExclVat = value;
                this.OnPropertyChanged("TotalExclVat");
            }
        }

        /// <summary>
        /// Gets or sets the line discount percentage.
        /// </summary>
        public decimal? LineDiscountPercentage { get; set; }

        /// <summary>
        /// Gets or sets the line discount amount.
        /// </summary>
        public decimal? LineDiscountAmount { get; set; }

        /// <summary>
        /// Gets or sets the posted value.
        /// </summary>
        public bool? Posted { get; set; }

        /// <summary>
        /// Gets or sets the exported value.
        /// </summary>
        public bool? Exported { get; set; }

        /// <summary>
        /// Gets or sets the exported value.
        /// </summary>
        public DateTime? ExportedDate { get; set; }

        /// <summary>
        /// Gets or sets the kill no.
        /// </summary>
        public int? KillNumber { get; set; }

        /// <summary>
        /// Gets or sets the kill no.
        /// </summary>
        public string ExternalKillNumber { get; set; }

        /// <summary>
        /// Gets or sets the carcass no.
        /// </summary>
        public int? CarcassNumber { get; set; }

        /// <summary>
        /// Gets or sets the sequenced date.
        /// </summary>
        public DateTime? SequencedDate { get; set; }

        /// <summary>
        /// Gets or sets the grading date.
        /// </summary>
        public DateTime? GradingDate { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public DateTime? DOB { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public DateTime? DateOfImport { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public DateTime? DamDOB { get; set; }

        /// <summary>
        /// Gets or sets the age in months.
        /// </summary>
        public int? AgeInMonths { get; set; }

        public bool IsUTM
        {
            get { return this.AgeInMonths.ToInt() <= ApplicationSettings.YoungBullMaxAge; }
        }

        /// <summary>
        /// Gets or sets the age in days.
        /// </summary>
        public int? AgeInDays { get; set; }

        public DateTime? CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the age in days.
        /// </summary>
        public bool? Halal { get; set; }

        public string HalalYesNo
        {
            get
            {
                return this.Halal.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the sale details.
        /// </summary>
        public IList<Model.DataLayer.Attribute> NonStandardResponses { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public NouCategory Category { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public int? NouCategoryID { get; set; }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public string CountryOfOrigin { get; set; }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public string OrderMethod { get; set; }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public bool DispatchProduct { get; set; }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public string RearedIn { get; set; }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public string CatName { get; set; }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public string AIMStatus { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public bool IsAnimalImported
        {
            get
            {
                return this.DateOfImport.HasValue;
            }
        }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public int? CategoryID
        {
            get
            {
                if (this.Category != null)
                {
                    return this.Category.CategoryID;
                }

                return (int?) null;
            }
        }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public int CatID
        {
            get
            {
                if (this.Category != null)
                {
                    return this.Category.CategoryID;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public string CategoryName
        {
            get
            {
                if (this.Category != null)
                {
                    return this.Category.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the record weight flag.
        /// </summary>
        public bool RecordingWeight { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public string CategoryCAT
        {
            get
            {
                if (this.Category != null)
                {
                    return this.Category.CAT;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the country of origin.
        /// </summary>
        public int? MixCount { get; set; }

        /// <summary>
        /// Gets or sets the cleanliness rating.
        /// </summary>
        public NouCleanliness Cleanliness { get; set; }

        /// <summary>
        /// Gets or sets the pieces.
        /// </summary>
        public int? Pieces { get; set; }

        /// <summary>
        /// Gets or sets the product id.
        /// </summary>
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets the group.
        /// </summary>
        public int? INGroupID
        {
            get
            {
                var prod = NouvemGlobal.InventoryItems.FirstOrDefault(x =>
                    x.Master.INMasterID == this.INMasterID);
                if (prod != null)
                {
                    return prod.Master.INGroupID;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the location id.
        /// </summary>
        public int? DeviceMasterID { get; set; }

        /// <summary>
        /// Gets or sets the location id.
        /// </summary>
        public int? UserMasterID { get; set; }

        /// <summary>
        /// Gets or sets the type id.
        /// </summary>
        public int NouTransactionTypeID { get; set; }

        /// <summary>
        /// Gets or sets the location id.
        /// </summary>
        public int WarehouseID { get; set; }

        /// <summary>
        /// Gets or sets the location id.
        /// </summary>
        public string Comments { get; set; }
        public string PartnerName { get; set; }
        public string CustomerName { get; set; }

        public decimal? IntakeWeight { get; set; }

        public decimal? LossWeight { get; set; }

        /// <summary>
        /// Gets or sets the location id.
        /// </summary>
        public int BaseWarehouseID { get; set; }

        /// <summary>
        /// Gets or sets the location id.
        /// </summary>
        public int? BaseMasterTableID { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public int? CleanlinessID
        {
            get
            {
                if (this.Cleanliness != null)
                {
                    return this.Cleanliness.NouCleanlinessID;
                }

                return (int?)null;
            }
        }

        /// <summary>
        /// Gets or sets the cleanliness rating.
        /// </summary>
        public string CleanlinessRating
        {
            get
            {
                return this.Cleanliness != null ? this.Cleanliness.Name : string.Empty;
                
            }
        }

        /// <summary>
        /// Gets or sets the sex.
        /// </summary>
        public string Sex{ get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public int? CustomerID { get; set; }

        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public int? ProductID { get; set; }

        /// <summary>
        /// Gets the product name.
        /// </summary>
        public string ProductName
        {
            get
            {
                var prod = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.ProductID);
                if (prod != null)
                {
                    return prod.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the product name.
        /// </summary>
        public string Code
        {
            get
            {
                var prod = NouvemGlobal.InventoryItems.FirstOrDefault(x => x.Master.INMasterID == this.ProductID);
                if (prod != null)
                {
                    return prod.Code;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the qa rating.
        /// </summary>
        public ViewBusinessPartner Customer
        {
            get
            {
                var localCustomer = NouvemGlobal.CustomerPartners.FirstOrDefault(x => x.Details.BPMasterID == this.CustomerID);
                if (localCustomer != null)
                {
                    return localCustomer.Details;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the herd/presenting herd match.
        /// </summary>
        public bool FactoryHerdMatch { get; set; }

        /// <summary>
        /// Gets or sets the herd qa rating.
        /// </summary>
        public bool HerdFarmAssured { get; set; }

        /// <summary>
        /// Gets or sets the qa rating.
        /// </summary>
        public bool? FarmAssured { get; set; }

        /// <summary>
        /// Gets or sets the qa rating.
        /// </summary>
        public string QA { get; set; }

        /// <summary>
        /// Gets or sets the qa rating.
        /// </summary>
        public string FarmAssuredString { get; set; }

        /// <summary>
        /// Gets or sets the qa rating.
        /// </summary>
        public string FarmAssuredYesNo
        {
            get
            {
                return this.FarmAssured.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the identigen value.
        /// </summary>
        public string Identigen { get; set; }

        /// <summary>
        /// Gets or sets the no of moves.
        /// </summary>
        public int? NumberOfMoves { get; set; }

        /// <summary>
        /// Gets or sets the current residency.
        /// </summary>
        public int? CurrentResidency { get; set; }

        /// <summary>
        /// Gets or sets the current residency.
        /// </summary>
        public int? CurrentFarmResidency { get; set; }

        /// <summary>
        /// Gets or sets the paid outside of weight band flag.
        /// </summary>
        public bool? OutsideWeightBand { get; set; }

        /// <summary>
        /// Gets or sets the current residency.
        /// </summary>
        public int? PreviousResidency { get; set; }

        /// <summary>
        /// Gets or sets the current residency.
        /// </summary>
        public int? TotalResidency { get; set; }

        public int? DispatchNo { get; set; }
        public int? IntakeNo { get; set; }

        /// <summary>
        /// Gets or sets the sex.
        /// </summary>
        public DateTime? DateOfLastMove { get; set; }

        /// <summary>
        /// Gets or sets the clipped value.
        /// </summary>
        public bool? Clipped { get; set; }

        /// <summary>
        /// Gets or sets the clipped value.
        /// </summary>
        public string FreezerType { get; set; }

        /// <summary>
        /// Gets or sets the casualty value.
        /// </summary>
        public bool? Casualty { get; set; }

        /// <summary>
        /// Gets or sets the current detained value.
        /// </summary>
        public bool? Condemned { get; set; }

        /// <summary>
        /// Gets or sets the current detained value.
        /// </summary>
        public bool? Detained { get; set; }

        /// <summary>
        /// Gets or sets the casualty value.
        /// </summary>
        public bool? CondemnedSide1 { get; set; }

        /// <summary>
        /// Gets or sets the casualty value.
        /// </summary>
        public bool? CondemnedSide2 { get; set; }

        /// <summary>
        /// Gets or sets the casualty value.
        /// </summary>
        public bool? DetainedSide1 { get; set; }

        /// <summary>
        /// Gets or sets the casualty value.
        /// </summary>
        public bool? DetainedSide2 { get; set; }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public bool? NotInsured { get; set; }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public int? Number { get; set; }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public int? WeightBandId { get; set; }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public int? PaymentNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the caracss is whole.
        /// </summary>
        public bool? IsFullCarcass { get; set; }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string CondemnedSide1YesNo
        {
            get
            {
                return this.CondemnedSide1.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string CondemnedSide2YesNo
        {
            get
            {
                return this.CondemnedSide2.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string DetainedSide1YesNo
        {
            get
            {
                return this.DetainedSide1.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string DetainedSide2YesNo
        {
            get
            {
                return this.DetainedSide2.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the hot weight.
        /// </summary>
        public decimal HotWeight
        {
            get
            {
                return Math.Round(this.TransactionWeight.ToDecimal(), 1);
            }
        }

        /// <summary>
        /// Gets or sets the cold weight.
        /// </summary>
        public decimal ColdWeight
        {
            get
            {
                return Math.Round(this.TransactionWeight.ToDecimal() * 0.98m,1);
            }
        }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public decimal GrossWeight
        {
            get
            {
                return this.TransactionWeight.ToDecimal() + this.Tare;
            }
        }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public int Quantity
        {
            get
            {
                return this.TransactionQty.ToInt();
            }
        }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public string Classifier { get; set; }

        /// <summary>
        /// Gets or sets the paid weight.
        /// </summary>
        public decimal? PaidWeight
        {
            get
            {
                return this.paidWeight;
            }

            set
            {
                if (!value.HasValue)
                {
                    this.paidWeight = this.ColdWeight;
                    if (!string.IsNullOrEmpty(this.FreezerType))
                    {
                        if (this.FreezerType.CompareIgnoringCase(Strings.Full)
                            || this.FreezerType.CompareIgnoringCase(Strings.ForeQtr)
                            || this.FreezerType.CompareIgnoringCase(Strings.HindQtr))
                        {
                            this.paidWeight = 0;
                        }
                        else if (this.FreezerType.CompareIgnoringCase(Strings.Side))
                        {
                            var leftWgt = this.WeightSide1*.98M;
                            this.paidWeight = Math.Round(this.ColdWeight - leftWgt,1);
                        }
                    }
                }
                else
                {
                    this.paidWeight = value;
                }

                if (!this.LoadingStockDetail)
                {
                    this.CalculateTotals();
                }

                this.OnPropertyChanged("PaidWeight");
            }
        }

        /// <summary>
        /// Gets or sets the supplier id.
        /// </summary>
        public bool IsStockLocation { get; set; }

        /// <summary>
        /// Gets or sets the supplier id.
        /// </summary>
        public bool NotInStock { get; set; }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string NotInsuredYesNo
        {
            get
            {
                return this.NotInsured.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the killtype.
        /// </summary>
        public string PlusMinus
        {
            get { return this.IsStockLocation.BoolToPlusMinus(); }
        }

        /// <summary>
        /// Gets or sets the killtype.
        /// </summary>
        public string IsLive
        {
            get
            {
                return (this.Consumed == null).BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the killtype.
        /// </summary>
        public bool RedLabel
        {
            get { return this.Attribute238.CompareIgnoringCase("Control OTM"); }
        }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public bool? TBYes { get; set; }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string TBYesYesNo
        {
            get
            {
                return this.TBYes.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public bool? BurstBelly { get; set; }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string BurstBellyYesNo
        {
            get
            {
                return this.BurstBelly.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public bool? Abscess { get; set; }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string AbscessYesNo
        {
            get
            {
                return this.Abscess.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public bool? LegMissing { get; set; }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string LegMissingYesNo
        {
            get
            {
                return this.LegMissing.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the paid value.
        /// </summary>
        public int PaidCount { get; set; }

        /// <summary>
        /// Gets or sets the paid value.
        /// </summary>
        public int UnpaidCount { get; set; }

        /// <summary>
        /// Gets or sets the paid value.
        /// </summary>
        public bool Paid { get; set; }

        /// <summary>
        /// Gets or sets the paid y/n.
        /// </summary>
        public string PaidYesNo
        {
            get
            {
                return this.Paid.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public bool? Lame { get; set; }

        /// <summary>
        /// Gets or sets the imported value.
        /// </summary>
        public bool? Imported { get; set; }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string LameYesNo
        {
            get
            {
                return this.Lame.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the supplier.
        /// </summary>
        public string SupplierName
        {
            get
            {
                if (this.SupplierID.HasValue)
                {
                    var localSupplier =
                        NouvemGlobal.SupplierPartners.FirstOrDefault(x => x.Details.BPMasterID == this.SupplierID);
                    if (localSupplier != null)
                    {
                        return localSupplier.Details.Name;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the supplier.
        /// </summary>
        public string SupplierCode
        {
            get
            {
                if (this.SupplierID.HasValue)
                {
                    var localSupplier =
                        NouvemGlobal.SupplierPartners.FirstOrDefault(x => x.Details.BPMasterID == this.SupplierID);
                    if (localSupplier != null)
                    {
                        return localSupplier.Details.Code;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string CondemnedYesNo
        {
            get
            {
                return this.Condemned.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the lame y/n.
        /// </summary>
        public string DetainedYesNo
        {
            get
            {
                return this.Detained.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the aims data.
        /// </summary>
        public AimsData AimsData { get; set; }

        /// <summary>
        /// Gets or sets the lame value.
        /// </summary>
        public string ThirdParty { get; set; }

        /// <summary>
        /// Gets or sets the imported y/n.
        /// </summary>
        public string ImportedYesNo
        {
            get
            {
                return this.Imported.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the clipped y/n.
        /// </summary>
        public string ClippedYesNo
        {
            get
            {
                return this.Clipped.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the casualty y/n.
        /// </summary>
        public string CasualtyYesNo
        {
            get
            {
                return this.Casualty.BoolToYesNo();
            }
        }

        /// <summary>
        /// Gets or sets the supplier.
        /// </summary>
        public string Supplier
        {
            get
            {
                if (this.SupplierID.HasValue)
                {
                    var localSupplier = NouvemGlobal.BusinessPartners.FirstOrDefault(x => x.Details.BPMasterID == this.SupplierID);
                    if (localSupplier == null)
                    {
                        var supplier = BusinessLogic.DataManager.Instance.GetBusinessPartner(this.SupplierID.ToInt());
                        if (supplier != null)
                        {
                            NouvemGlobal.AddNewPartner(supplier);
                            localSupplier = supplier;
                        }
                    }

                    if (localSupplier != null)
                    {
                        return localSupplier.Details != null ? localSupplier.Details.Name : string.Empty;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the supplier.
        /// </summary>
        public DateTime? KillDate { get; set; }
       
        /// <summary>
        /// Gets or sets the eartag.
        /// </summary>
        public string Generic1 { get; set; }

        #region attributes

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute1 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute2 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute3 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute4 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute5 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute6 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute7 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute8 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute9 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute10 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute11 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute12 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute13 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute14 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute15 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute16 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute17 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute18 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute19 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute20 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute21 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute22 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute23 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute24 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute25 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute26 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute27 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute28 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute29 { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public string Attribute30 { get; set; }

        public string Attribute31 { get; set; }
        public string Attribute32 { get; set; }
        public string Attribute33 { get; set; }
        public string Attribute34 { get; set; }
        public string Attribute35 { get; set; }
        public string Attribute36 { get; set; }
        public string Attribute37 { get; set; }
        public string Attribute38 { get; set; }
        public string Attribute39 { get; set; }
        public string Attribute40 { get; set; }
        public string Attribute41 { get; set; }
        public string Attribute42 { get; set; }
        public string Attribute43 { get; set; }
        public string Attribute44 { get; set; }
        public string Attribute45 { get; set; }
        public string Attribute46 { get; set; }
        public string Attribute47 { get; set; }
        public string Attribute48 { get; set; }
        public string Attribute49 { get; set; }
        public string Attribute50 { get; set; }
        public string Attribute51 { get; set; }
        public string Attribute52 { get; set; }
        public string Attribute53 { get; set; }
        public string Attribute54 { get; set; }
        public string Attribute55 { get; set; }
        public string Attribute56 { get; set; }
        public string Attribute57 { get; set; }
        public string Attribute58 { get; set; }
        public string Attribute59 { get; set; }
        public string Attribute60 { get; set; }
        public string Attribute61 { get; set; }
        public string Attribute62 { get; set; }
        public string Attribute63 { get; set; }
        public string Attribute64 { get; set; }
        public string Attribute65 { get; set; }
        public string Attribute66 { get; set; }
        public string Attribute67 { get; set; }
        public string Attribute68 { get; set; }
        public string Attribute69 { get; set; }
        public string Attribute70 { get; set; }
        public string Attribute71 { get; set; }
        public string Attribute72 { get; set; }
        public string Attribute73 { get; set; }
        public string Attribute74 { get; set; }
        public string Attribute75 { get; set; }
        public string Attribute76 { get; set; }
        public string Attribute77 { get; set; }
        public string Attribute78 { get; set; }
        public string Attribute79 { get; set; }
        public string Attribute80 { get; set; }
        public string Attribute81 { get; set; }
        public string Attribute82 { get; set; }
        public string Attribute83 { get; set; }
        public string Attribute84 { get; set; }
        public string Attribute85 { get; set; }
        public string Attribute86 { get; set; }
        public string Attribute87 { get; set; }
        public string Attribute88 { get; set; }
        public string Attribute89 { get; set; }
        public string Attribute90 { get; set; }
        public string Attribute91 { get; set; }
        public string Attribute92 { get; set; }
        public string Attribute93 { get; set; }
        public string Attribute94 { get; set; }
        public string Attribute95 { get; set; }
        public string Attribute96 { get; set; }
        public string Attribute97 { get; set; }
        public string Attribute98 { get; set; }
        public string Attribute99 { get; set; }
        public string Attribute100 { get; set; }

        public string Attribute101 { get; set; }
        public string Attribute102 { get; set; }
        public string Attribute103 { get; set; }
        public string Attribute104 { get; set; }
        public string Attribute105 { get; set; }
        public string Attribute106 { get; set; }
        public string Attribute107 { get; set; }
        public string Attribute108 { get; set; }
        public string Attribute109 { get; set; }
        public string Attribute110 { get; set; }
        public string Attribute111 { get; set; }
        public string Attribute112 { get; set; }
        public string Attribute113 { get; set; }
        public string Attribute114 { get; set; }
        public string Attribute115 { get; set; }
        public string Attribute116 { get; set; }
        public string Attribute117 { get; set; }
        public string Attribute118 { get; set; }
        public string Attribute119 { get; set; }
        public string Attribute120 { get; set; }
        public string Attribute121 { get; set; }
        public string Attribute122 { get; set; }
        public string Attribute123 { get; set; }
        public string Attribute124 { get; set; }
        public string Attribute125 { get; set; }
        public string Attribute126 { get; set; }
        public string Attribute127 { get; set; }
        public string Attribute128 { get; set; }
        public string Attribute129 { get; set; }
        public string Attribute130 { get; set; }
        public string Attribute131 { get; set; }
        public string Attribute132 { get; set; }
        public string Attribute133 { get; set; }
        public string Attribute134 { get; set; }
        public string Attribute135 { get; set; }
        public string Attribute136 { get; set; }
        public string Attribute137 { get; set; }
        public string Attribute138 { get; set; }
        public string Attribute139 { get; set; }
        public string Attribute140 { get; set; }
        public string Attribute141 { get; set; }
        public string Attribute142 { get; set; }
        public string Attribute143 { get; set; }
        public string Attribute144 { get; set; }
        public string Attribute145 { get; set; }
        public string Attribute146 { get; set; }
        public string Attribute147 { get; set; }
        public string Attribute148 { get; set; }
        public string Attribute149 { get; set; }
        public string Attribute150 { get; set; }
        public string Attribute151 { get; set; }
        public string Attribute152 { get; set; }
        public string Attribute153 { get; set; }
        public string Attribute154 { get; set; }
        public string Attribute155 { get; set; }
        public string Attribute156 { get; set; }
        public string Attribute157 { get; set; }
        public string Attribute158 { get; set; }
        public string Attribute159 { get; set; }
        public string Attribute160 { get; set; }
        public string Attribute161 { get; set; }
        public string Attribute162 { get; set; }
        public string Attribute163 { get; set; }
        public string Attribute164 { get; set; }
        public string Attribute165 { get; set; }
        public string Attribute166 { get; set; }
        public string Attribute167 { get; set; }
        public string Attribute168 { get; set; }
        public string Attribute169 { get; set; }
        public string Attribute170 { get; set; }
        public string Attribute171 { get; set; }
        public string Attribute172 { get; set; }
        public string Attribute173 { get; set; }
        public string Attribute174 { get; set; }
        public string Attribute175 { get; set; }
        public string Attribute176 { get; set; }
        public string Attribute177 { get; set; }
        public string Attribute178 { get; set; }
        public string Attribute179 { get; set; }
        public string Attribute180 { get; set; }
        public string Attribute181 { get; set; }
        public string Attribute182 { get; set; }
        public string Attribute183 { get; set; }
        public string Attribute184 { get; set; }
        public string Attribute185 { get; set; }
        public string Attribute186 { get; set; }
        public string Attribute187 { get; set; }
        public string Attribute188 { get; set; }
        public string Attribute189 { get; set; }
        public string Attribute190 { get; set; }
        public string Attribute191 { get; set; }
        public string Attribute192 { get; set; }
        public string Attribute193 { get; set; }
        public string Attribute194 { get; set; }
        public string Attribute195 { get; set; }
        public string Attribute196 { get; set; }
        public string Attribute197 { get; set; }
        public string Attribute198 { get; set; }
        public string Attribute199 { get; set; }
        public string Attribute200 { get; set; }
        public string Attribute201 { get; set; }
        public string Attribute202 { get; set; }
        public string Attribute203 { get; set; }
        public string Attribute204 { get; set; }
        public string Attribute205 { get; set; }
        public string Attribute206 { get; set; }
        public string Attribute207 { get; set; }
        public string Attribute208 { get; set; }
        public string Attribute209 { get; set; }
        public string Attribute210 { get; set; }
        public string Attribute211 { get; set; }
        public string Attribute212 { get; set; }
        public string Attribute213 { get; set; }
        public string Attribute214 { get; set; }
        public string Attribute215 { get; set; }
        public string Attribute216 { get; set; }
        public string Attribute217 { get; set; }
        public string Attribute218 { get; set; }
        public string Attribute219 { get; set; }
        public string Attribute220 { get; set; }
        public string Attribute221 { get; set; }
        public string Attribute222 { get; set; }
        public string Attribute223 { get; set; }
        public string Attribute224 { get; set; }
        public string Attribute225 { get; set; }
        public string Attribute226 { get; set; }
        public string Attribute227 { get; set; }
        public string Attribute228 { get; set; }
        public string Attribute229 { get; set; }
        public string Attribute230 { get; set; }
        public string Attribute231 { get; set; }
        public string Attribute232 { get; set; }
        public string Attribute233 { get; set; }
        public string Attribute234 { get; set; }
        public string Attribute235 { get; set; }
        public string Attribute236 { get; set; }
        public string Attribute237 { get; set; }
        public string Attribute238 { get; set; }
        public string Attribute239 { get; set; }
        public string Attribute240 { get; set; }
        public string Attribute241 { get; set; }
        public string Attribute242 { get; set; }
        public string Attribute243 { get; set; }
        public string Attribute244 { get; set; }
        public string Attribute245 { get; set; }
        public string Attribute246 { get; set; }
        public string Attribute247 { get; set; }
        public string Attribute248 { get; set; }
        public string Attribute249 { get; set; }
        public string Attribute250 { get; set; }
        public string Attribute251 { get; set; }
        public string Attribute252 { get; set; }
        public string Attribute253 { get; set; }
        public string Attribute254 { get; set; }
        public string Attribute255 { get; set; }
        public string Attribute256 { get; set; }
        public string Attribute257 { get; set; }
        public string Attribute258 { get; set; }
        public string Attribute259 { get; set; }
        public string Attribute260 { get; set; }
        public string Attribute261 { get; set; }
        public string Attribute262 { get; set; }
        public string Attribute263 { get; set; }
        public string Attribute264 { get; set; }
        public string Attribute265 { get; set; }
        public string Attribute266 { get; set; }
        public string Attribute267 { get; set; }
        public string Attribute268 { get; set; }
        public string Attribute269 { get; set; }
        public string Attribute270 { get; set; }
        public string Attribute271 { get; set; }
        public string Attribute272 { get; set; }
        public string Attribute273 { get; set; }
        public string Attribute274 { get; set; }
        public string Attribute275 { get; set; }
        public string Attribute276 { get; set; }
        public string Attribute277 { get; set; }
        public string Attribute278 { get; set; }
        public string Attribute279 { get; set; }



        #endregion

        /// <summary>
        /// Gets or sets the movements.
        /// </summary>
        public IList<FarmMovement> Movements { get; set; }

        /// <summary>
        /// Gets or sets the batch attributes.
        /// </summary>
        public StockDetail BatchAttribute { get; set; }

        /// <summary>
        /// Gets or sets the qa rating.
        /// </summary>
        public string Generic2 { get; set; }

        public bool UsingStockPrice
        {
            get
            {
                return this.usingStockPrice;
            }

            set
            {
                this.usingStockPrice = value;
                if (value)
                {
                    var wgt = this.PaidWeight.ToDecimal();
                    if (wgt > 0)
                    {
                        var localPrice = this.TotalExclVat / wgt;
                        this.UnitPrice = Math.Round(localPrice.ToDecimal(), 2);
                        this.TotalIncVat = this.TotalExclVat.ToDecimal() + this.VAT.ToDecimal();
                        Messenger.Default.Send(false, Token.CalculatePayment);
                    }
                }
            }
        }

        private void SetAdjustedPrice()
        {
            if (!this.PriceAdjust.IsNullOrZero())
            {
                var adjust = this.PriceAdjust.ToDecimal();
                this.AdjustedUnitPrice = this.UnitPrice + adjust;
                return;
            }

            this.AdjustedUnitPrice = this.UnitPrice;
        }

        private void CalculateTotals()
        {
            var price = this.AdjustedUnitPrice.ToDecimal();
            this.TotalExclVat = Math.Round(price*this.PaidWeight.ToDecimal(), 2);
            this.TotalIncVat = this.TotalExclVat.ToDecimal() + this.VAT.ToDecimal();
            Messenger.Default.Send(false, Token.CalculatePayment);
        }

        public string DocumentReference { get; set; }
        public string Warehouse { get; set; }
        public string Device { get; set; }
        public string User { get; set; }
        public string Partner { get; set; }
        public string Process { get; set; }
        public string Product { get; set; }
        public string ProductCode { get; set; }
        public bool Manual { get; set; }

        public string ManualYesNo
        {
            get { return this.Manual.BoolToYesNo(); }
        }

        /// <summary>
        /// The work flow notes.
        /// </summary>
        public IList<Note> NotesCollection { get; set; }

        /// <summary>
        /// Gets all the notes.
        /// </summary>
        public string NotesList
        {
            get
            {
                if (this.NotesCollection == null || !this.NotesCollection.Any())
                {
                    return string.Empty;
                }

                if (this.NotesCollection.Count == 1)
                {
                    return this.NotesCollection.First().Notes;
                }

                return string.Join(",", this.NotesCollection.Select(x => x.Notes));
            }
        }

        /// <summary>
        /// Gets all the notes.
        /// </summary>
        public string EditDateList
        {
            get
            {
                if (this.NotesCollection == null || !this.NotesCollection.Any())
                {
                    return string.Empty;
                }

                if (this.NotesCollection.Count == 1)
                {
                    return this.NotesCollection.First().CreationDate.ToString();
                }

                return string.Join(",", this.NotesCollection.Select(x => x.CreationDate));
            }
        }

        /// <summary>
        /// Gets all the notes.
        /// </summary>
        public string EditedByList
        {
            get
            {
                if (this.NotesCollection == null || !this.NotesCollection.Any())
                {
                    return string.Empty;
                }

                var edits = this.NotesCollection.Select(x => x.UserMasterID);
                var editNames = new List<string>();
                foreach (var edit in edits)
                {
                    var name = NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == edit);
                    if (name != null)
                    {
                        editNames.Add(name.UserMaster.FullName);
                    }
                }

                if (editNames.Count == 1)
                {
                    return editNames.First();
                }

                return string.Join(",", editNames.Select(x => x));
            }
        }

        public void SetNotes()
        {
            this.Notes = this.NotesList;
        }

        /// <summary>
        /// Gets all the notes.
        /// </summary>
        public string EditedBy
        {
            get
            {
                if (this.UserMasterID.HasValue)
                {
                    var name = NouvemGlobal.Users.FirstOrDefault(x => x.UserMaster.UserMasterID == this.UserMasterID);
                    if (name != null)
                    {
                        return name.UserMaster.FullName;
                    }
                }

                return string.Empty;
            }
        }

        public IList<StockDetail> StockDetails { get; set; }
        public DateTime? EditDate { get; set; }
        public bool? EditStatusID { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        public string EditStatus
        {
            get
            {
                return this.EditStatusID == true ? Strings.PostEdit : Strings.PreEdit;
            }
        }

        #region INotifyPropertyChanged implementation

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #endregion
    }
}
