﻿// -----------------------------------------------------------------------
// <copyright file="Report.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Model.DataLayer;

namespace Nouvem.Model.BusinessObject
{
    using System.Collections.Generic;
    using Microsoft.Reporting.WinForms;

    public class ReportData
    {
        /// <summary>
        /// The report name.
        /// </summary>
        public int ReportID { get; set; }

        /// <summary>
        /// The report name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The report alias name.
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// The report path.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// The report name.
        /// </summary>
        public int? ReportFolderID { get; set; }

        /// <summary>
        /// The report name.
        /// </summary>
        public bool? IsHistoric { get; set; }

        /// <summary>
        /// The report name.
        /// </summary>
        public string HistoricParameter { get; set; }

        /// <summary>
        /// Gets or sets the report parameters
        /// </summary>
        public IEnumerable<ReportParameter> ReportParameters { get; set; }

        /// <summary>
        /// Gets or sets the report parameters
        /// </summary>
        public IList<ReportParam> ReportParams { get; set; }

        /// <summary>
        /// Gets or sets the report parameters
        /// </summary>
        public IList<ReportDateTypeLookUp> ReportDateLookUps { get; set; }

        /// <summary>
        /// Gets or sets the report parameters
        /// </summary>
        public ReportUserDateType ReportUserDateType { get; set; }

        /// <summary>
        /// Gets or sets the report parameters
        /// </summary>
        public IList<int> ReportDatesTypeIds { get; set; }

        /// <summary>
        /// Gets or sets the report parameters
        /// </summary>
        public IList<UserReport> UserReports { get; set; }

        /// <summary>
        /// Gets or sets the report parameters
        /// </summary>
        public UserReport UserReportSettings { get; set; }

        /// <summary>
        /// The printer to print the report to.
        /// </summary>
        public string PrinterName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show all is selected/displayed.
        /// </summary>
        public bool ShowAll { get; set; }

        /// <summary>
        /// Gets or sets the report sp.
        /// </summary>
        public string ReportMacro { get; set; }

        /// <summary>
        /// Gets or sets the associated search macro.
        /// </summary>
        public string SearchGridMacro{ get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public bool ShowCreateFile { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public string CreateFileProcess { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public bool ShowDates { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public bool ShowDateTypes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether dates are displayed.
        /// </summary>
        public bool ShowSearchID { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether multi select is used.
        /// </summary>
        public bool MultiSelect { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether multi select is used.
        /// </summary>
        public bool ViewMultiple { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether keep visible is selected/displayed.
        /// </summary>
        public bool KeepVisible { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether refresh is selected/displayed.
        /// </summary>
        public bool Refresh { get; set; }

        /// <summary>
        /// Gets or sets the address list.
        /// </summary>
        public IList<string> EmailAddressList { get; set; }
    }
}
