﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.BusinessObject
{
    public class DepartmentLookup
    {
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string Code { get; set; }
        public bool Deleted { get; set; }
        public bool IsSelected { get; set; }
    }
}
