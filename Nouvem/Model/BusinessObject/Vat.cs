﻿// -----------------------------------------------------------------------
// <copyright file="Vat.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    public class Vat
    {
        /// <summary>
        /// Gets or sets the vat code id.
        /// </summary>
        public int VatCodeId { get; set; }

        /// <summary>
        /// Gets or sets the vat code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the vat code description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the vat code dpercentage.
        /// </summary>
        public decimal? Percentage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the vat code is deleted or not.
        /// </summary>
        public bool Deleted { get; set; }
    }
}
