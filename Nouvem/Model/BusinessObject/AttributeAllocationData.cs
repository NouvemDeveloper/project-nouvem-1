﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpf.Controls.Localization;
using Nouvem.Model.DataLayer;
using Nouvem.Shared;
using Nouvem.ViewModel;

namespace Nouvem.Model.BusinessObject
{
    public class AttributeAllocationData
    {
        /// <summary>
        /// Gets or sets the allocation.
        /// </summary>
        public AttributeAllocation AttributionAllocation { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public AttributeMaster AttributeMaster { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public AttributeTabName AttributeTabName { get; set; }

        ///// <summary>
        ///// Gets or sets the attribute.
        ///// </summary>
        public AttributeTemplate AttributeTemplate { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public NouDateType NouDateType { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public NouAttributeReset AttributeReset { get; set; }

        ///// <summary>
        ///// Gets or sets the attribute.
        ///// </summary>
        //public Nouvem.Model.DataLayer.Attribute Attribute { get; set; }

        ///// <summary>
        ///// Gets or sets the non standard response attribute.
        ///// </summary>
        //public Nouvem.Model.DataLayer.Attribute AttributeNonStandard { get; set; }

        /// <summary>
        /// Gets or sets the attribute.
        /// </summary>
        public Nouvem.Model.DataLayer.AttributeWorkflow Attribute { get; set; }

        /// <summary>
        /// Gets or sets the non standard response attribute.
        /// </summary>
        public Nouvem.Model.DataLayer.AttributeWorkflow AttributeNonStandard { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public NouTraceabilityType NouTraceabilityType { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public DateDay DateDay { get; set; }

        /// <summary>
        /// Gets or sets the attribute tab index.
        /// </summary>
        public int TabIndex { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public PropertyInfo PropertyInfo { get; set; }

        /// <summary>
        /// Gets or sets the binding vm.
        /// </summary>
        public NouvemViewModelBase BindingViewModel { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public NouTraceabilityType NouTraceabilityTypeNonStandard { get; set; }

        /// <summary>
        /// Gets or sets the attribute value.
        /// </summary>
        public string TraceabilityValue { get; set; }

        /// <summary>
        /// Gets or sets the non standard attribute value.
        /// </summary>
        public string TraceabilityValueNonStandard { get; set; }

        /// <summary>
        /// Gets or sets the attribute value.
        /// </summary>
        public bool NonStandardResponseGiven { get; set; }

        /// <summary>
        /// Gets or sets the associated product.
        /// </summary>
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the attribute value.
        /// </summary>
        public bool AlertsRecorded { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is visible.
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is editable.
        /// </summary>
        public bool IsEditable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this stage is the current process.
        /// </summary>
        public bool IsCurrentProcess { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public bool? UserCanAdd { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public bool? UserCanEdit { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public IList<AttributeProcess> AttributeEditProcesses { get; set; }

        /// <summary>
        /// Gets or sets the attribute master.
        /// </summary>
        public IList<AttributeProcess> AttributeVisibleProcesses { get; set; }

        /// <summary>
        /// Gets the traceability type.
        /// </summary>
        public string TraceabilityType
        {
            get
            {
                if (this.NouTraceabilityType != null)
                {
                    return this.NouTraceabilityType.TraceabilityType;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the traceability batch value.
        /// </summary>
        public bool IsBatch
        {
            get
            {
                if (this.AttributionAllocation != null)
                {
                    return this.AttributionAllocation.Batch.ToBool();
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the date type.
        /// </summary>
        public string DateType
        {
            get
            {
                if (this.NouDateType != null)
                {
                    return this.NouDateType.DateType;
                }

                return Properties.DateType.EnteredDate;
            }
        }

        /// <summary>
        /// Gets the attribute description.
        /// </summary>
        public string Description
        {
            get
            {
                if (this.AttributeMaster != null)
                {
                    return this.AttributeMaster.Description;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the attribute description.
        /// </summary>
        public string Code
        {
            get
            {
                if (this.AttributeMaster != null)
                {
                    return this.AttributeMaster.Code;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the non standard traceability type.
        /// </summary>
        public string TraceabilityTypeNonStandard
        {
            get
            {
                if (this.NouTraceabilityTypeNonStandard != null)
                {
                    return this.NouTraceabilityTypeNonStandard.TraceabilityType;
                }

                return string.Empty;
            }
        }
    }
}

