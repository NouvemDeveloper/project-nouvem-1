﻿// -----------------------------------------------------------------------
// <copyright file="EposSale.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Model.BusinessObject
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using Nouvem.Model.DataLayer;

    /// <summary>
    /// Models an epos sale.
    /// </summary>
    public class EposSale : INotifyPropertyChanged
    {
        /// <summary>
        /// Flag, as to whether an unpaid account sale is to be marked paid.
        /// </summary>
        private bool paid;

        /// <summary>
        /// Gets or sets the invoice header.
        /// </summary>
        public InvoiceHeader InvoiceHeader { get; set; }

        /// <summary>
        /// Gets or sets the invoice item details.
        /// </summary>
        public IList<ProductData> InvoiceDetails { get; set; }

        /// <summary>
        /// Gets or sets the customer name.
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an unpaid account sale is to be marked paid.
        /// </summary>
        public bool Paid
        {
            get
            {
                return this.paid;
            }

            set
            {
                this.paid = value;
                this.OnPropertyChanged("Paid");
            }
        }

        /// <summary>
        /// The property changed event.
        /// </summary>
        /// <param name="property">The property to raise this event for.</param>
        private void OnPropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        /// <summary>
        /// The property event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
