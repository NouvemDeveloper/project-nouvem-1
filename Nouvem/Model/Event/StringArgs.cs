﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Model.Event
{
    using System;

    /// <summary>
    /// Event arguments class for the touch pad data..
    /// </summary>
    public class StringArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StringArgs"/> class.
        /// </summary>
        /// <param name="Data">The weight value to broadcast.</param>
        public StringArgs(string data)
        {
            this.Data = data;
        }

        /// <summary>
        /// Gets or sets the serial data,
        /// </summary>
        public string Data { get; set; }
    }
}
