﻿// -----------------------------------------------------------------------
// <copyright file="BusinessPartner.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ReportEngine.ObjectCollections
{
    public class BusinessPartner
    {
        /// <summary>
        /// The name field.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The code field.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The tel field.
        /// </summary>
        public string Tel { get; set; }

        /// <summary>
        /// The fax field.
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// The web field.
        /// </summary>
        public string Web { get; set; }

        /// <summary>
        /// The vat field.
        /// </summary>
        public string VATNo { get; set; }

        /// <summary>
        /// The name field.
        /// </summary>
        public string CompanyNumber { get; set; }
    }
}
