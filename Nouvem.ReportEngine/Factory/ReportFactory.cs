﻿// -----------------------------------------------------------------------
// <copyright file="ReportFactory.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using System.Windows.Forms;

namespace Nouvem.ReportEngine.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Windows.Threading;
    using CrystalDecisions.CrystalReports.Engine;
    using Nouvem.ReportEngine.Model.Enum;
    using Nouvem.ReportEngine.ObjectCollections;
    using Nouvem.ReportEngine.Properties;
    using Nouvem.ReportEngine.Reports;

    public class ReportFactory
    {
        #region field

        /// <summary>
        /// The singleton reference.
        /// </summary>
        private static readonly ReportFactory Factory;

        /// <summary>
        /// The crystal report class.
        /// </summary>
        private ReportClass report;

        /// <summary>
        /// The devexpress report class.
        /// </summary>
        private XtraReport devReport;
        
        /// <summary>
        /// The connection string data source.
        /// </summary>
        private string dataSource;

        /// <summary>
        /// The connection string.
        /// </summary>
        private string connectionString;

        /// <summary>
        /// The connection string database.
        /// </summary>
        private string initialCatalog;

        /// <summary>
        /// The connection string user id.
        /// </summary>
        private string userID;

        /// <summary>
        /// The connection string password.
        /// </summary>
        private string password;

        /// <summary>
        /// The connection string integrated security flag.
        /// </summary>
        private bool integratedSecurity;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFactory"/> class.
        /// </summary>
        protected ReportFactory()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFactory"/> class with parameters.
        /// </summary>
        /// <param name="connectionString">The connection string to set.</param>
        protected ReportFactory(string connectionString)
        {
            this.connectionString = connectionString;
            this.ParseConnectionString();
        }

        #endregion

        #region public interface

        #region creation

        /// <summary>
        /// Instantiates and/or returns the the class singleton.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>The singleton reference.</returns>
        public static ReportFactory Create(string connectionString)
        {
            return Factory ?? new ReportFactory(connectionString);
        }

        #endregion

        #region method

        #region crystal

        /// <summary>
        /// Creates a report.
        /// </summary>
        /// <param name="type">The report to generate.</param>
        /// <param name="parameters">The reports parameters.</param>
        /// <param name="mode">The print/preview mode.</param>
        /// <param name="noOfPrints">The number of copies to print.</param>
        /// <param name="data">The data being passed in, if it's a collection based report.</param>
        /// <returns>A string validation. Empty if successful, else what the issue is.</returns>
        public string CreateReport(ReportType type, IList<KeyValuePair<string, object>> parameters, ReportMode mode, int noOfPrints, IEnumerable<object> data = null)
        {
            try
            {
                DataSet reportData = null;
                switch (type)
                {
                    case ReportType.BusinessPartners:

                        if (data == null)
                        {
                            return Constant.NoData;
                        }

                        this.report = new BusinessPartners();
                        var businessPartners = new List<BusinessPartner>();

                        data.ToList().ForEach(x => businessPartners.Add(x as BusinessPartner));
                        this.report.SetDataSource(businessPartners);
                        break;
                }

                if (!this.integratedSecurity)
                {
                    this.report.DataSourceConnections[0].SetConnection(this.dataSource, this.initialCatalog, this.userID, this.password);

                    // We need to set this here, otherwise the user will be prompted at runtime.
                    this.report.SetDatabaseLogon(this.userID, this.password);
                }
                else
                {
                    this.report.DataSourceConnections[0].SetConnection(this.dataSource, this.initialCatalog, true);
                }

                // set the data source
                if (data == null)
                {
                    // we are using a data set
                    // we are using the passed in collection#
                    if (reportData.Tables[0].Rows.Count == 0)
                    {
                        // no data to return.
                        return Constant.NoData;
                    }

                    this.report.SetDataSource(reportData);
                }

                // add the parameters(name and value)
                if (parameters.Any())
                {
                    foreach (var parameter in parameters)
                    {
                        this.report.SetParameterValue(parameter.Key, parameter.Value);
                    }
                }

                // generate the report.
                Dispatcher.CurrentDispatcher.Invoke(() => this.GenerateReport(mode, noOfPrints));
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #region dev

        /// <summary>
        /// Creates a report.
        /// </summary>
        /// <param name="reportPath">The path of the stored report file.</param>
        /// <param name="parameters">The reports parameters.</param>
        /// <param name="mode">The print/preview mode.</param>
        /// <param name="noOfPrints">The number of copies to print.</param>
        /// <returns>A string validation. Empty if successful, else what the issue is.</returns>
        public string CreateReport(string reportPath, IDictionary<string, object> parameters, ReportMode mode, int noOfPrints)
        {
            try
            {
                this.devReport = new XtraReport { RequestParameters = false };
                this.devReport.LoadLayout(reportPath);
              
                // add the data source details
                MsSqlConnectionParameters connectionParameters;
                if (!this.integratedSecurity)
                {
                     connectionParameters = new MsSqlConnectionParameters(this.dataSource, this.initialCatalog, this.userID, this.password, MsSqlAuthorizationType.SqlServer);
                }
                else
                {
                    connectionParameters = new MsSqlConnectionParameters(this.dataSource, this.initialCatalog, string.Empty, string.Empty, MsSqlAuthorizationType.Windows);
                }

                this.devReport.DataSource = new SqlDataSource(connectionParameters);

                // add the parameters(name and value)
                if (parameters.Any())
                {
                    foreach (var parameter in parameters)
                    {
                        this.devReport.Parameters[parameter.Key].Value = parameter.Value;
                    }
                }

                // generate the report.
                Dispatcher.CurrentDispatcher.Invoke(() => this.GenerateReport(mode, noOfPrints, false));
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion

        #endregion

        #endregion

        #region helper

        /// <summary>
        /// Generate the report.
        /// </summary>
        /// <param name="mode">The report mode(print or preview).</param>
        /// <param name="noOfPrints">The number of copies, if printing.</param>
        /// <param name="isCrystal">Flag, as to whether we are printing a crystal or devexpress report.</param>
        private void GenerateReport(ReportMode mode, int noOfPrints, bool isCrystal = true)
        {
            if (isCrystal)
            {
                // crystal report
                switch (mode)
                {
                    case ReportMode.Print:
                        // print the report.
                        this.report.PrintToPrinter(noOfPrints, false, 0, 0);
                        break;

                    case ReportMode.Preview:
                        // display the report preview.
                        var reportView = new ReportView();
                        reportView.SetReport(this.report);
                        reportView.ShowDialog();
                        break;
                }
            }
            else
            {
                // devexpress report
                var tool = new ReportPrintTool(this.devReport);

                switch (mode)
                {
                    case ReportMode.Print:
                        // print the report.
                        tool.Print();
                        break;

                    case ReportMode.Preview:
                        // display the report preview.
                        tool.ShowPreview();
                        break;
                }
            }
        }

        /// <summary>
        /// Breaks up the incoming connection string.
        /// </summary>
        private void ParseConnectionString()
        {
            var sqlBuilder = new SqlConnectionStringBuilder(this.connectionString);
            this.integratedSecurity = sqlBuilder.IntegratedSecurity;
            this.dataSource = sqlBuilder.DataSource;
            this.initialCatalog = sqlBuilder.InitialCatalog;
            this.userID = sqlBuilder.UserID;
            this.password = sqlBuilder.Password;
        }

        /// <summary>
        /// Generates the report data.
        /// </summary>
        /// <typeparam name="T">The generic type.</typeparam>
        /// <param name="storedProcedure">The associated stored procedure name.</param>
        /// <param name="parameters">The report parameters.</param>
        /// <returns>A dataset used to populate the report.</returns>
        private DataSet GenerateData<T>(string storedProcedure, IEnumerable<KeyValuePair<string, T>> parameters)
        {
            var data = new DataSet();

            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                var command = new SqlCommand
                {
                    Connection = connection,
                    CommandText = storedProcedure,
                    CommandType = CommandType.StoredProcedure
                };

                parameters.ToList().ForEach(x => command.Parameters.AddWithValue(x.Key, x.Value));
                command.ExecuteScalar();

                var dataAdapter = new SqlDataAdapter(command);
                dataAdapter.Fill(data, storedProcedure);
                return data;
            }
        }

        #endregion
    }
}
