﻿// -----------------------------------------------------------------------
// <copyright file="ReportView.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.ReportEngine
{
    using System.Windows;
    using CrystalDecisions.CrystalReports.Engine;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ReportView : Window
    {
        public ReportView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Set the crystal report source.
        /// </summary>
        /// <param name="report">The report source.</param>
        public void SetReport(ReportDocument report)
        {
            this.CrystalReportsViewer.ViewerCore.ReportSource = report;
        }
    }
}
