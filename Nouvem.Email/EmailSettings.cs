﻿// -----------------------------------------------------------------------
// <copyright file="EmailSettings.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace Nouvem.Email
{
    using System.Net.Mail;

    public class EmailSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailSettings"/> class.
        /// </summary>
        public EmailSettings()
        {
            this.Attachments = new List<string>();
            this.CCAddresses = new List<MailAddress>();

            // default
            this.EnableSsl = true;
            this.Port = 587;
            this.Host = "smtp.gmail.com";
            this.DeliveryMethod = SmtpDeliveryMethod.Network;
        }

        /// <summary>
        /// Gets or sets the from address.
        /// </summary>
        public string FromAddress { get; set; }

        /// <summary>
        /// Gets or sets the to address.
        /// </summary>
        public string ToAddress { get; set; }

        /// <summary>
        /// Gets or sets the from address display name.
        /// </summary>
        public string FromAddressDisplayName { get; set; }

        /// <summary>
        /// Gets or sets the to address display name.
        /// </summary>
        public string ToAddressDisplayName { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the mail password.
        /// </summary>
        public string FromMailPassword { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we are using the default credentials.
        /// </summary>
        public bool UseDefaultCredentials { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether SSL encrytion is used.
        /// </summary>
        public bool EnableSsl { get; set; }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets the delivery method.
        /// </summary>
        public SmtpDeliveryMethod DeliveryMethod { get; set; }

        /// <summary>
        /// Gets or sets the attachments.
        /// </summary>
        public IList<string> Attachments { get; set; }

        /// <summary>
        /// Gets or sets the cc addresses.
        /// </summary>
        public string Addresses { get; set; }

        /// <summary>
        /// Gets or sets the cc addresses.
        /// </summary>
        public IList<MailAddress> CCAddresses { get; set; }
    }
}
