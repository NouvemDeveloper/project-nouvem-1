﻿// -----------------------------------------------------------------------
// <copyright file="Email.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Email
{
    using System.Net;
    using System.Net.Mail;

    public class Email
    {
        /// <summary>
        /// Sends an email.
        /// </summary>
        /// <param name="settings">The email settings.</param>
        public void SendEmail(EmailSettings settings)
        {
            var fromAddress = new MailAddress(settings.FromAddress, settings.FromAddressDisplayName);
            var toAddress = new MailAddress(settings.ToAddress, settings.ToAddressDisplayName);
            var subject = settings.Subject;
            var body = settings.Body;
            var message = new MailMessage(fromAddress, toAddress);
            message.Subject = subject;
            message.Body = body;

            foreach (var copy in settings.CCAddresses)
            {
               message.CC.Add(copy);
            }

            var smtp = new SmtpClient
            {
                Host = settings.Host,
                Port = settings.Port,
                EnableSsl = settings.EnableSsl,
                DeliveryMethod = settings.DeliveryMethod,
                UseDefaultCredentials = settings.UseDefaultCredentials,
                Credentials = new NetworkCredential(fromAddress.Address, settings.FromMailPassword)
            };

            foreach (var attachmentPath in settings.Attachments)
            {
                message.Attachments.Add(new Attachment(attachmentPath));
            }

            smtp.Send(message);
        }
    }
}
