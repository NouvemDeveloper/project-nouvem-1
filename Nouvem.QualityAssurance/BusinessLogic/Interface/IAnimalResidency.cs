﻿// -----------------------------------------------------------------------
// <copyright file="IAnimalResidency.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.QualityAssurance.BusinessLogic.Interface
{
    using System;
    using System.Collections.Generic;
    using Nouvem.QualityAssurance.Model.BusinessObject;

    /// <summary>
    /// Interface which models the residency of an animal.
    /// </summary>
    public interface IAnimalResidency
    {
        /// <summary>
        /// Find the residency in days on the current farm.
        /// </summary>
        /// <returns>An object representation of the number of days the animal spent on the current farm which have been approved by the quality assurance service.</returns>
        ResidencyResult FindCurrentFarmResidency(string herdNo = "", string presentHerdNo = "");

        /// <summary>
        /// Find the residency in days on the previous farm.
        /// </summary>
        /// <returns>An object representation of the number of days the animal spent on the previous farm which have been approved by the quality assurance service.</returns>
        ResidencyResult FindPreviousFarmResidency();

        /// <summary>
        /// Find the residency in days on all farms.
        /// </summary>
        /// <param name="farmCount">The number of farms to search through when calculating residency. -1 indicates that all farms are checked.</param>
        /// <returns>The number of days the animal spent of all farms which have been approved by the quality assurance service.</returns>
        ResidencyResult FindTotalFarmResidency(int farmCount = -1);

        /// <summary>
        /// Remove the mart and factory movements from the movements of the animal.
        /// </summary>
        /// <returns>The collection of movements with the mart and factory moves removed.</returns>
        IList<Movement> FilterMovements();

        /// <summary>
        /// Find the last date on which the animal moved.
        /// </summary>
        /// <returns>The on which the animal was last moved.</returns>
        DateTime? FindLastMovementDate();

        /// <summary>
        /// Check to see if the animal has been imported through the department of agriculture.
        /// </summary>
        /// <returns>A boolean flag to indicate if the animal has been imported through the department of agriculture.</returns>
        bool IsAnimalImported();

        /// <summary>
        /// Find the date on which the animal is marked as imported by the department of agriculture.
        /// </summary>
        /// <returns>The date, if present, on which the animal was imported as noted by the department of agriculture.</returns>
        DateTime? FindImportDate();
    }
}
