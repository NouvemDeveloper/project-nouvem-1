﻿// -----------------------------------------------------------------------
// <copyright file="ResidencyResult.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.QualityAssurance.Model.BusinessObject
{
    using System.Collections.Generic;

    /// <summary>
    /// Class which models the complete result for the calculation of residency for an animal.
    /// </summary>
    public class ResidencyResult
    {
        #region Fields

        /// <summary>
        /// A collection of individual calculation which provide a breakdown of the residency calculation.
        /// </summary>
        private IList<ResidencyCalculation> residencyCalculations = new List<ResidencyCalculation>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the eartag to which the calculation relates.
        /// </summary>
        public string Eartag { get; set; }

        /// <summary>
        /// Gets or sets the final number of days residency.
        /// </summary>
        public int TotalDaysResidency { get; set; }

        /// <summary>
        /// Gets or sets a collection of individual calculation which provide a breakdown of the residency calculation.
        /// </summary>
        public IList<ResidencyCalculation> ResidencyCalculations
        {
            get
            {
                return this.residencyCalculations;
            }

            set
            {
                this.residencyCalculations = value;
            }
        }


        public string Error { get; set; }

        #endregion

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="ResidencyResult"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="ResidencyResult"/> object.</returns>
        public static ResidencyResult CreateNewResidencyResult()
        {
            return new ResidencyResult();
        }

        /// <summary>
        /// Create a new <see cref="ResidencyResult"/> populated with the parameters passed.
        /// </summary>
        /// <param name="eartag">The eartag to which the calculation relates.</param>
        /// <param name="daysResidency">The final number of days residency.</param>
        /// <param name="residencyCalculations">A collection of individual calculation which provide a breakdown of the residency calculation.</param>
        /// <returns>A new <see cref="ResidencyResult"/> object populated with the parameters passed.</returns>
        public static ResidencyResult CreateResidencyResult(string eartag, int daysResidency, IList<ResidencyCalculation> residencyCalculations, string error = "")
        {
            return new ResidencyResult()
            {
                Eartag = eartag,
                TotalDaysResidency = daysResidency,
                ResidencyCalculations = residencyCalculations,
                Error = error
            };
        }

        #endregion
    }
}

