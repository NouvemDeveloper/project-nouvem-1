﻿// -----------------------------------------------------------------------
// <copyright file="CertificateHistory.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.QualityAssurance.Model.BusinessObject
{
    using System;

    /// <summary>
    /// Class which models the certification history of a herd. 
    /// </summary>
    public class CertificateHistory
    {
        #region Constructor

        /// <summary>
        /// Prevents a default instance of the <see cref="CertificateHistory"/> class from being created.
        /// </summary>
        private CertificateHistory()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the herd number associated with the certificate history.
        /// </summary>
        public string HerdNumber { get; set; }

        /// <summary>
        /// Gets or sets the owner of the herd, according to the quality assurance service.
        /// </summary>
        public string HerdOwner { get; set; }

        /// <summary>
        /// Gets the date from which the herd is quality assured.
        /// </summary>
        public DateTime CertStartDate
        {
            get
            {
                return this.CalculateStartDate();
            }
        }

        /// <summary>
        /// Gets or sets the length of time, in months, for which the certificate is valid.
        /// </summary>
        public int CertDuration { get; set; }

        /// <summary>
        /// Gets or sets the date on which the quality assurance certificate will expire.
        /// </summary>
        public DateTime CertExpiryDate { get; set; }

        #endregion

        #region Creation

        /// <summary>
        /// Create a new empty <see cref="CertificateHistory"/> with default parameters.
        /// </summary>
        /// <returns>A new <see cref="CertificateHistory"/> object.</returns>
        public static CertificateHistory CreateNewCertificateHistory()
        {
            return new CertificateHistory();
        }

        /// <summary>
        /// Create a new <see cref="CertificateHistory"/> with specific parameters.
        /// </summary>
        /// <param name="herdNumber">The herd number for the certificate.</param>
        /// <param name="herdOwner">The owner of the herd.</param>
        /// <param name="certExpiryDate">The expiry date of the certificate.</param>
        /// <param name="certDuration">The number of months for which the certificate is valid.</param>
        /// <returns>A new <see cref="CertificateHistory"/> object with the parameters specified.</returns>
        public static CertificateHistory CreateCertificateHistory(
            string herdNumber,
            string herdOwner,
            DateTime certExpiryDate,
            int certDuration)
        {
            return new CertificateHistory
            {
                HerdNumber = herdNumber,
                HerdOwner = herdOwner,
                CertExpiryDate = certExpiryDate,
                CertDuration = certDuration
            };
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Calculate the start date for a certificate.
        /// </summary>
        /// <returns>The start date for the certificate.</returns>
        /// <remarks>TODO : Attach this to an extension method of calculate days passed and make the parameter an application property.</remarks>
        private DateTime CalculateStartDate()
        {
            return this.CertExpiryDate != DateTime.MinValue ? this.CertExpiryDate.AddMonths(-this.CertDuration) : DateTime.MinValue;
        }

        #endregion
    }
}

