﻿// -----------------------------------------------------------------------
// <copyright file="Movement.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Shared;

namespace Nouvem.QualityAssurance.Model.BusinessObject
{
    using System;

    /// <summary>
    /// Class which models the movement of an animal from one location to another, on a specific date.
    /// </summary>
    public class Movement
    {
        #region Constructor

        /// <summary>
        /// Prevents a default instance of the <see cref="Movement"/> class from being created./>
        /// </summary>
        private Movement()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the location (farm, mart) from which the animal moved.
        /// </summary>
        public string FromId { get; set; }

        /// <summary>
        /// Gets or sets the location (farm, mart) to which the animal moved.
        /// </summary>
        public string ToId { get; set; }

        /// <summary>
        /// Gets or sets the date on which the animal movement took place.
        /// </summary>
        public DateTime MovementDate { get; set; }

        /// <summary>
        /// Gets or sets the date on which the animal movement took place.
        /// </summary>
        public DateTime? MovementOffDate { get; set; }

        public bool IsMart 
        {
            get
            {
                if (!string.IsNullOrEmpty(this.ToId) && this.ToId.Length > 4)
                {
                    var holdingEnd = this.ToId.Substring(this.ToId.Length - 4, 4).ToInt();
                    if (holdingEnd >= 8000 && holdingEnd < 9000)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets or sets the movement index, used when ordering by dates which are identical.
        /// </summary>
        public int Index { get; set; }

        public string SubLocation { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create a new animal movement.
        /// </summary>
        /// <returns>A new instance of an animal movement with default parameters.</returns>
        public static Movement CreateNewMovement()
        {
            return new Movement();
        }

        /// <summary>
        /// Create a new animal movement with the parameters passed.
        /// </summary>
        /// <param name="fromId">The location (farm, mart) from which the animal moved</param>
        /// <param name="toId">The location (farm, mart) to which the animal moved</param>
        /// <param name="moveDate">The date on which the animal movement took place</param>
        /// <returns>A new instance of an animal movement with the parameters passed.</returns>
        public static Movement CreateMovement(string fromId, string toId, DateTime moveDate, int index = 0, DateTime? moveOff = null, string subLocation = "")
        {
            return new Movement
            {
                FromId = fromId,
                ToId = toId,
                MovementDate = moveDate,
                MovementOffDate = moveOff,
                Index = index,
                SubLocation = subLocation
            };
        }

        #endregion
    }
}

