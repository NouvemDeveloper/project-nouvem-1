﻿// -----------------------------------------------------------------------
// <copyright file="IAccountsIntegration.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration
{
    using Nouvem.AccountsIntegration.BusinessObject;
    using System;
    using System.Collections.Generic;

    public interface IAccountsIntegration
    {
        /// <summary>
        /// Create accounts db connection.
        /// </summary>
        /// <param name="path">The db path.</param>
        /// <param name="userName">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string Connect(string path, string userName, string password);

        /// <summary>
        /// Release resource.
        /// </summary>
        void Logout();

        /// <summary>
        /// Posts the sale order invoice.
        /// </summary>
        /// <param name="header">The invoice data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string PostSalesInvoice(InvoiceHeader header);

        /// <summary>
        /// Posts the sale order .
        /// </summary>
        /// <param name="header">The sale order data</param>
        /// <returns>An empty string if successful, otherwise error message.</returns>
        string PostSaleOrder(InvoiceHeader header);

        /// <summary>
        /// Retrieves the partners.
        /// </summary>
        /// <returns>The accounts partners.</returns>
        Tuple<List<BusinessPartner>, string> GetPartners();
    }
}
