﻿// -----------------------------------------------------------------------
// <copyright file="InvoiceDetail.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.AccountsIntegration.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class InvoiceDetail
    {
        /// <summary>
        /// Gets or sets the product code.
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// Gets or sets the product description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the qty.
        /// </summary>
        public float Quantity { get; set; }

        /// <summary>
        /// Gets or sets the unit price.
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets the unit of sale.
        /// </summary>
        public string UnitOfSale { get; set; }

        /// <summary>
        /// Gets or sets the total ex vat.
        /// </summary>
        public decimal TotalExVat { get; set; }

        /// <summary>
        /// Gets or sets the total inc vat.
        /// </summary>
        public decimal TotalIncVat { get; set; }

        /// <summary>
        /// Gets or sets the vat.
        /// </summary>
        public decimal Vat { get; set; }

        /// <summary>
        /// Gets or sets the nominal code.
        /// </summary>
        public string NominalCode { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public double? QuantityDelivered { get; set; }

        /// <summary>
        /// Gets or sets the qty delivered.
        /// </summary>
        public double? WeightDelivered { get; set; }
    }
}
