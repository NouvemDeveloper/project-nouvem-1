﻿

CREATE view [dbo].[ViewTraceabilityMaster]
  as
  SELECT
  t.TraceabilityTemplatesMasterID,
  t.TraceabilityCode,
  t.NouTraceabilityTypeID,
  t.SQL,
  t.GS1AIMasterID,
  t.Collection,
  t.TraceabilityDescription,
  t.Deleted,
  n.TraceabilityType,
  ISNULL(g.AI, '') as Identifier, 
  ISNULL(g.OfficialDescription, '') as OfficialDescription, 
  ISNULL(g.RelatedDescription, '') as RelatedDescription,
  ISNULL(g.DataLength, '') as [DataLength],
  ISNULL(g.AI, '') + '  ' + ISNULL(g.OfficialDescription, '') as VisualDisplay,
  CONVERT(BIT, 0) as Batch,
  CONVERT(BIT, 0) as [Transaction],
  CONVERT(BIT, 0) as [Required],
  CONVERT(BIT, 0) as [Reset]

  FROM TraceabilityTemplateMaster t 
      inner join NouTraceabilityType n on n.NouTraceabilityTypeID = t.NouTraceabilityTypeID 
      left join Gs1AIMaster g on g.GS1AIMasterID = t.GS1AIMasterID 
  WHERE t.Deleted = 0


