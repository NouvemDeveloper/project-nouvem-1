﻿CREATE TABLE [dbo].[BPGroup] (
    [BPGroupID]   INT           IDENTITY (1, 1) NOT NULL,
    [BPGroupName] NVARCHAR (50) NOT NULL,
    [Deleted]     BIT           NOT NULL,
    CONSTRAINT [PK_BPGroup] PRIMARY KEY CLUSTERED ([BPGroupID] ASC)
);

