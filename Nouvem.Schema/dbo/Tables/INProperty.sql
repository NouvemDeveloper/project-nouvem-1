﻿CREATE TABLE [dbo].[INProperty] (
    [INPropertyID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (50) NOT NULL,
    [Deleted]      BIT           NOT NULL,
    CONSTRAINT [PK_INProperty] PRIMARY KEY CLUSTERED ([INPropertyID] ASC)
);

