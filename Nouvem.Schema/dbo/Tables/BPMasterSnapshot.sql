﻿CREATE TABLE [dbo].[BPMasterSnapshot] (
    [BPMasterSnapshotID]   INT            IDENTITY (1, 1) NOT NULL,
    [BPMasterID]           INT            NOT NULL,
    [BPType_Type]          NVARCHAR (50)  NOT NULL,
    [BPMaster_Name]        NVARCHAR (100) NOT NULL,
    [BPMaster_Code]        NVARCHAR (30)  NOT NULL,
    [BPGroup_BPGroupName]  NVARCHAR (50)  NOT NULL,
    [BPMaster_UpLift]      INT            NULL,
    [BPMaster_CreditLimit] INT            NULL,
    [Deleted]              BIT            NOT NULL,
    CONSTRAINT [PK_InvoiceHdrBPMaster] PRIMARY KEY CLUSTERED ([BPMasterSnapshotID] ASC)
);







