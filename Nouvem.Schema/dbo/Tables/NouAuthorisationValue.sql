﻿CREATE TABLE [dbo].[NouAuthorisationValue] (
    [NouAuthorisationValueID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]             NVARCHAR (50) NOT NULL,
    [Deleted]                 BIT           NOT NULL,
    CONSTRAINT [PK_SysAutherisationValueID] PRIMARY KEY CLUSTERED ([NouAuthorisationValueID] ASC)
);

