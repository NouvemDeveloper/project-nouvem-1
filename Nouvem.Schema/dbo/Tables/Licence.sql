﻿CREATE TABLE [dbo].[Licence] (
    [LicenceID]      INT           IDENTITY (1, 1) NOT NULL,
    [LicenceNumber]  NVARCHAR (50) NOT NULL,
    [MessageContent] NVARCHAR (50) NOT NULL,
    [ValidFrom]      DATE          NULL,
    [ValidTo]        DATE          NULL,
    [IssuedOn]       DATE          NULL,
    [ImportedOn]     DATETIME      NULL,
    [Deleted]        BIT           NOT NULL,
    [ValidForDays]   INT           NULL,
    CONSTRAINT [PK_Licence] PRIMARY KEY CLUSTERED ([LicenceID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_LicenceName]
    ON [dbo].[Licence]([LicenceNumber] ASC);

