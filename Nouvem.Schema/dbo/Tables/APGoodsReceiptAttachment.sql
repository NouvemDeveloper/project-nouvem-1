﻿CREATE TABLE [dbo].[APGoodsReceiptAttachment] (
    [APGoodsReceiptAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [APGoodsReceiptID]           INT             NOT NULL,
    [FilePath]                   NVARCHAR (50)   NULL,
    [FileName]                   NVARCHAR (50)   NOT NULL,
    [AttachmentDate]             DATE            NOT NULL,
    [File]                       VARBINARY (MAX) NOT NULL,
    [Deleted]                    DATETIME        NULL,
    CONSTRAINT [PK_APSaleOrderGoodsReceiptAttachment] PRIMARY KEY CLUSTERED ([APGoodsReceiptAttachmentID] ASC),
    CONSTRAINT [FK_APGoodsReceiptAttachment_APGoodsReceipt] FOREIGN KEY ([APGoodsReceiptID]) REFERENCES [dbo].[APGoodsReceipt] ([APGoodsReceiptID])
);

