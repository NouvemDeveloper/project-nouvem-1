﻿




-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the products from the DEMLocal db to the Nouvem inmaster table
-- =============================================
CREATE PROCEDURE [dbo].[DM_Products]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @ProductCode varchar(30),	
	 @ProductDescription varchar(70),
	 @GroupID bigint
	
	

	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT p.ProductCode,
			p.[Description],
			p.GroupID
		
     FROM [SERVER2\sqlexpress].[DEMLocal].[dbo].[Products]  p
	
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO @ProductCode,		
		  @ProductDescription,
		  @GroupID
		
     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 	 

	  DECLARE @GroupDescription varchar(70) = (SELECT TOP 1 GroupDescription FROM [SERVER2\sqlexpress].[DEMLocal].[dbo].[ProductGroups] WHERE id = @GroupID)
	  DECLARE @LocalGroupID int = (SELECT TOP 1 INGroupID FROM INGroup WHERE Name = @GroupDescription)
	  INSERT INTO INMaster (Code, Name, SalesItem, PurchaseItem, ProductionProduct, CreationDate, INGroupID) Values (@ProductCode, @ProductDescription,1,1,1, Getdate(), @LocalGroupID)
		

	  FETCH NEXT FROM PRICE_CURSOR
	 INTO @ProductCode,		
		  @ProductDescription,
		  @GroupID

	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
END