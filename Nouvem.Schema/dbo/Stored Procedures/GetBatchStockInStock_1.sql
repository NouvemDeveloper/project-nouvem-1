﻿

-- =============================================
-- Author:		brian murray
-- Create date: 24/08/2016
-- Description:	Returns the remaining in stock, batch stock.
-- =============================================
CREATE PROCEDURE [dbo].[GetBatchStockInStock]
	-- Add the parameters for the stored procedure here
	@BatchNo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select Serial, Name as 'Location', Product, TransactionQTY, TransactionWeight, Pieces from ViewTransaction where batchno = @BatchNo
	--and StockLocation= 1 and consumed is null 
	--and deleted is null and (TransactionQTY > 0 or TransactionWeight > 0)
	--and InLocation = 1
	--order by Product

	select Serial, Name as 'Location', Product, TransactionQTY, TransactionWeight, Pieces from viewtransaction where mastertableid in(
    select apgoodsreceiptdetailid from APGoodsReceiptDetail where apgoodsreceiptid = 23438 and deleted is null)
     and deleted is null and consumed is  null

END