﻿
-- =============================================
-- Author:		brian murray
-- Create date: 21-06-2016
-- Description:	Migrates the stock/traceability.
-- =============================================
CREATE PROCEDURE [dbo].[DM_Stock]
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @ProductId int,
	 @LocationId int,
	 @ScalesGrossWeight money,
	 @ScalesNettWeight money,
	 @Pieces int,
	 @Quantity int,
	 @ContainerStockId int,
	 @Barcode varchar(100),
	 @CreationDate datetime,
	 @PackDate datetime,
	 @BestBeforeDate datetime,
	 @DisplayUntilDate datetime,
	 @CutDate datetime,
	 @BornIn varchar(100),
	 @ReardedIn varchar(100),
	 @SlaughteredIn varchar(100),
	 @CutIn varchar(100),
	 @BatchNumber nvarchar(100),
	 @LocationName nvarchar(100),
	 @ProductCode nvarchar(100),
	 @ProductDescription nvarchar(100),
	 @StockID int

	 DECLARE PARTNER_CURSOR CURSOR STATIC FOR
     SELECT s.ProductId,
	        s.LocationID,
			s.ScaleGrossWeight,
			s.ScaleNettWeight,
			s.Pieces,
			s.Quantity,
			s.ContainerStockID,
			s.Barcode AS Barcode,
			s.CreationDate,
			s.PackDate,
			s.BestBeforeDate,
			s.DisplayUntilDate,
			s.CutDate,
			c_Born.[Description],
			c_Rear.[Description],
			p_sl.PlantCode,
			p_cut.PlantCode,
			s.BatchCode,
			l.[Description],
			p.ProductCode,
			p.[Description],
			s.ID
			
		
 
     FROM [DEMLocal].[dbo].[Stock] s 
	 INNER JOIN [DEMLocal].[dbo].[Locations] l ON l.ID = s.LocationID
	 INNER JOIN  [DEMLocal].[dbo].[Products] p ON p.ID = s.ProductID
	 INNER JOIN [DEMLocal].[dbo].[Plants] p_cut ON s.CutIn = p_cut.ID
	 INNER JOIN [DEMLocal].[dbo].[Plants] p_sl ON s.SlaughteredIn = p_sl.ID
	 INNER JOIN [DEMLocal].[dbo].[Countries] c_Born ON s.BornIn = c_Born.ID
	 INNER JOIN [DEMLocal].[dbo].[Countries] c_Rear ON s.RearedIn = c_Rear.ID
	 
	 WHERE CONVERT(date,s.CreationDate) > DATEADD(day, -60, getdate())  AND l.InStock = -1 
	 ORDER BY s.ID DESC 

	 

	 DECLARE @ContainerIds TABLE(ID int NULL)   
	 INSERT INTO @ContainerIds 
	 SELECT DISTINCT(s.ContainerStockID)
	 FROM [DEMLocal].[dbo].[Stock] s 
	 INNER JOIN [DEMLocal].[dbo].[Locations] l ON l.ID = s.LocationID
	 INNER JOIN  [DEMLocal].[dbo].[Products] p ON p.ID = s.ProductID	 
	 WHERE CONVERT(date,s.CreationDate) > DATEADD(day, -60, getdate())  AND l.InStock = -1 AND s.ContainerStockID IS NOT NULL

      
 
     OPEN PARTNER_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @ProductId,
	       @LocationId,
		   @ScalesGrossWeight,
	       @ScalesNettWeight,
	       @Pieces,
		   @Quantity,
		   @ContainerStockId,
		   @Barcode,
		   @CreationDate,
		   @PackDate,
		   @BestBeforeDate,
		   @DisplayUntilDate,
		   @CutDate,
		   @BornIn,
		   @ReardedIn,
		   @SlaughteredIn,
		   @CutIn,
		   @BatchNumber,
		   @LocationName,
		   @ProductCode ,
	       @ProductDescription,
		   @StockID


     -- Check Fetch_status to ensure we have a record to work with
       WHILE @@FETCH_STATUS = 0
       BEGIN    


	   BEGIN TRY        
	   
	   DECLARE @LocalBatchId int = (SELECT TOP 1 BatchNumberID FROM BatchNumber WHERE extReference = @BatchNumber)
	   if @LocalBatchId IS NULL
	   BEGIN
	     INSERT INTO BatchNumber Values(1, NULL, GETDATE(),@BatchNumber)
		 SET @LocalBatchId = IDENT_CURRENT('BatchNumber')
	   END

	   DECLARE @WareHouseID INT = (CASE WHEN @LocationName = 'Intake Stock Room' THEN 1002 
	                                    WHEN @LocationName = 'Fresh Product Stock Room' THEN 2004 
										WHEN @LocationName = 'Frozen Stock Room' THEN 3005 
										ELSE 3004 END)

	   DECLARE @INMasterID INT = (SELECT TOP 1 INMasterID FROM INMaster WHERE Code = @ProductCode AND Name = @ProductDescription)
	   	
	   
	   
	   DECLARE @IsBox bit 
	   If @StockID IN (SELECT * FROM @ContainerIds)
	   BEGIN
	   SET @IsBox = 1
	  
	   END

	   DECLARE @StockContainerID int = NULL
	   DECLARE @Consumed datetime = null
	   If (@ContainerStockId IS NOT NULL)
	   BEGIN
	    SET  @StockContainerID = (SELECT TOP 1 StockTransactionID FROM StockTransaction WHERE Reference = @ContainerStockId)
		SET @Consumed = GETDATE()
	   END

	   if @INMasterID IS NOT NULL
	   BEGIN

	   INSERT INTO StockTransaction (BatchNumberID, Serial, TransactionDate, NouTransactionTypeID, TransactionQTY, TransactionWeight, GrossWeight, Tare, Pieces, WarehouseID, INMasterID, ManualWeight, DeviceMasterID, UserMasterID, IsBox, Reference, StockTransactionID_Container, Consumed) 
	        VALUES(@LocalBatchId, @Barcode, @CreationDate, 4, @Quantity, @ScalesNettWeight, @ScalesGrossWeight,0, @Pieces, @WareHouseID, @INMasterID,0, 4019, 8020, @IsBox, @StockID, @StockContainerID, @Consumed)
      
	  
	   DECLARE @StockTransactionID INT = IDENT_CURRENT('StockTransaction')
	   INSERT INTO TransactionTraceability VALUES(@StockTransactionID, 2, @PackDate, NULL,NULL,10,NULL,NULL)
	   INSERT INTO TransactionTraceability VALUES(@StockTransactionID, 2, @BestBeforeDate, NULL,NULL,9,NULL,NULL)
	   INSERT INTO TransactionTraceability VALUES(@StockTransactionID, 1, @BornIn, NULL,9,NULL,NULL,NULL)		
	   INSERT INTO TransactionTraceability VALUES(@StockTransactionID, 1, @ReardedIn, NULL,10,NULL,NULL,NULL)
	   INSERT INTO TransactionTraceability VALUES(@StockTransactionID, 1, @CutIn, NULL,12,NULL,NULL,NULL)
	   INSERT INTO TransactionTraceability VALUES(@StockTransactionID, 1, @SlaughteredIn, NULL,11,NULL,NULL,NULL)
	  END

	  END TRY  
       BEGIN CATCH  
      SELECT   
        ERROR_NUMBER() AS ErrorNumber  
       ,ERROR_MESSAGE() AS ErrorMessage;  
	   PRINT @StockId 
       END CATCH;


	 FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @ProductId,
	       @LocationId,
		   @ScalesGrossWeight,
	       @ScalesNettWeight,
	       @Pieces,
		   @Quantity,
		   @ContainerStockId,
		   @Barcode,
		   @CreationDate,
		   @PackDate,
		   @BestBeforeDate,
		   @DisplayUntilDate,
		   @CutDate,
		   @BornIn,
		   @ReardedIn,
		   @SlaughteredIn,
		   @CutIn,
		   @BatchNumber,
		   @LocationName,
		   @ProductCode,
           @ProductDescription,
		   @StockID


	 END
	 
	 CLOSE PARTNER_CURSOR;
     DEALLOCATE PARTNER_CURSOR;

	 update stocktransaction set consumed = null where isbox = 1
    update stocktransaction set isBox = null, consumed = getdate() where StockTransactionID_Container is not null
    update batchnumber set number = batchnumberid

END

