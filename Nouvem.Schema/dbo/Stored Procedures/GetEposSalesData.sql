﻿



-- =============================================
-- Author: brian murray
-- Create date: 04/08/2015
-- Description: Retrieves sales data for the epos system falling between the selected dates.
-- =============================================
CREATE PROCEDURE [dbo].[GetEposSalesData]  

@StartDate as Datetime,
@EndDate as Datetime
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT inm.Name, inm.INMasterID,

--(SELECT TOP 1 st.ClosingQTY 
--     FROM StockTransaction st WHERE CONVERT(date, TransactionDate) < convert(date, @StartDate)
--      AND st.INMasterID = inm.INMasterID
--ORDER BY st.StockTransactionID DESC) AS OpenQty,

(SELECT SUM(st.TransactionQTY)
     FROM StockTransaction st inner join NouTransactionType n
	    ON st.NouTransactionTypeID = n.NouTransactionTypeID
     WHERE (CONVERT(date, TransactionDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                           AND st.INMasterID = inm.INMasterID
                                           AND n.Description = 'GoodsReceipt')) AS NewStock,

(SELECT SUM(st.TransactionQTY) 
         from StockTransaction st inner join NouTransactionType n
             ON st.NouTransactionTypeID = n.NouTransactionTypeID
         WHERE (CONVERT(date, TransactionDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                               AND st.INMasterID = inm.INMasterID
                                               AND n.Description = 'GoodsReturnToHeadOffice')) AS [Returns],

(SELECT SUM(id.QuantityDelivered) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  LEFT JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType is null)) AS AccSales,

(SELECT SUM(id.QuantityDelivered) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  LEFT JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType is not null)) AS ActualSales,

(SELECT SUM(id.TotalExcVAT) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  LEFT JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType is not null)) AS SalesRevenue,

(SELECT SUM(id.QuantityDelivered) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  LEFT JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType is null)) AS AccSalesUnpaid,

(SELECT SUM(id.QuantityDelivered) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  INNER JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType = 'account')) AS AccSalesPaid,

(SELECT SUM(id.TotalExcVAT) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  LEFT JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType is null)) AS AccSalesUnpaidRevenue,

(SELECT SUM(id.TotalExcVAT) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  INNER JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType = 'account')) AS AccSalesPaidRevenue,

(SELECT SUM(id.QuantityDelivered) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  INNER JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType = 'cash')) AS CashSales,

(SELECT SUM(id.VAT) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  INNER JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType = 'cash')) AS CashSalesVat,

(SELECT SUM(id.QuantityDelivered) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  INNER JOIN NouDeliveryMethod ndm
	  ON ih.NouDeliveryMethodID = ndm.NouDeliveryMethodID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND ndm.Method = 'delivery')) AS Delivery,												

(SELECT SUM(id.QuantityDelivered) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  INNER JOIN NouDeliveryMethod ndm
	  ON ih.NouDeliveryMethodID = ndm.NouDeliveryMethodID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND ndm.Method = 'collection')) AS [Collection],		

(SELECT SUM(id.QuantityDelivered) from InvoiceDetail id inner join InvoiceHeader ih
      ON id.InvoiceHeaderID = ih.InvoiceHeaderID  INNER JOIN NouPaymentType npt
	  ON ih.NouPaymentTypeID = npt.NouPaymentTypeID
      WHERE (CONVERT(date, ih.CreationDate) BETWEEN CONVERT(date, @StartDate) AND CONVERT(date, @EndDate)  
                                            AND id.INMasterID = inm.INMasterID 
											AND ih.Deleted is null
											AND npt.PaymentType = 'credit card')) AS CCSales,

(SELECT TOP 1 Price FROM PriceListDetail pd WHERE pd.INMasterID = inm.INMasterID) AS BasePrice

FROM INMaster inm
ORDER BY inm.Name  

END