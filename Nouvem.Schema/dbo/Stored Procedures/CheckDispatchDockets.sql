﻿




-- =============================================
-- Author:		brian murray
-- Create date: 19/07/16
-- Description:	Checks the dispatch docket totals against the line totals.
-- =============================================
CREATE PROCEDURE [dbo].[CheckDispatchDockets] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   DECLARE 
	 @DispatchId int,
	 @SubTotal decimal(18,2),
	 @Number int


	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT a.ArdispatchId,
			a.SubTotalExVat	,
			a.Number		
     FROM ARDispatch a 
	 WHERE a.Deleted is null and a.noudocstatusid <> 2
 
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO @DispatchId,
		  @SubTotal,
		  @Number

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 	   
	   DECLARE @LinesTotal decimal(18,2) =
	   (select sum(totalexclvat) from ARDispatchDetail
	    where ardispatchid = @DispatchId
		 and Deleted is null) if (@LinesTotal != 
		 @SubTotal AND @Dispatchid<>17047) BEGIN
	    PRINT 'Docket Number ' + CONVERT(varchar(5),@Number)
		END
	 FETCH NEXT FROM PRICE_CURSOR
		INTO @DispatchId,
		  @SubTotal,
		  @Number
	 END	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
	
END