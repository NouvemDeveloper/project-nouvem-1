﻿-- =============================================
-- Author:		brian murray
-- Create date: 15/09/2015
-- Description:	Removes the users/groups
-- =============================================
CREATE PROCEDURE TestRemoveUsers 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from [Audit]
	delete from UserLoggedOn
	delete from WindowSettings
	delete from UserMaster
	delete from UserGroupRule
	delete from [UserGroup ]
END
