﻿
-- =============================================
-- Author:		brian murray
-- Create date: 13-10-16
-- Description:	Retrieves the concession data.
-- =============================================
CREATE PROCEDURE [dbo].[ReportConcessions]
	-- Add the parameters for the stored procedure here
	@StartDate date,
	@EndDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	c.Reason,
	c.ConcessionTime,
	u.FullName as 'User',
	inm.Code as 'Product Code',
	inm.Name as 'Product Name',
	cinms.Code as 'Batch Product Code',
	cinms.Name as 'Batch Product Name',
	ar.Number as 'Docket Number'


	FROM Concession c
	INNER JOIN ARDispatchDetail ard 
	  ON c.ARDispatchDetailID = ard.ARDispatchDetailID
	INNER JOIN ARDispatch ar 
	  ON ard.ARDispatchID = ar.ARDispatchID
	INNER JOIN INMaster inm 
	  ON ard.INMasterID = inm.INMasterID
	INNER JOIN INMaster cinms
	  ON c.INMasterID = cinms.INMasterID
	LEFT JOIN UserMaster u
	  ON c.UserMasterID = u.UserMasterID

	WHERE c.ConcessionTime BETWEEN @StartDate AND @EndDate
END

