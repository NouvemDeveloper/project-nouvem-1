﻿-- =============================================
-- Author:		<James Harrison>
-- Create date: <02/11/16>
-- Description:	<Slicing Production List (Get all sliced products needed for the day>
-- =============================================
CREATE PROCEDURE [dbo].[ReportSlicingProductionList]
	-- Add the parameters for the stored procedure here
 @StartDate date,
 @EndDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  
	bpm.Name,
	RTRIM(LTRIM(inm.code)) as 'Code',
    RTRIM(LTRIM(inm.Name)) as 'Product Name',
	arod.QuantityOrdered,
	aro.DeliveryDate,
   (SELECT arod.QuantityOrdered  -
   (SELECT TOP 1 QuantityDelivered FROM ARDispatchDetail WHERE ARDispatchID = ard.ARDispatchID AND INMasterID = inm.INMasterID  )) AS 'QtyOutstanding'


	      FROM
	  AROrder aro inner join AROrderDetail arod on aro.AROrderID = arod.AROrderID	  
	  inner join INMaster inm on inm.INMasterID = arod.INMasterID
	  left join ARDispatch ard on ard.BaseDocumentReferenceID = arod.AROrderID	
	   left join BPMaster bpm on ard.BPMasterID_Customer = bpm.BPMasterID
	

	
	  WHERE aro.DeliveryDate BETWEEN @StartDate AND @EndDate and aro.RouteID = 3
END