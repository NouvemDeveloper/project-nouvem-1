﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportPriceDetails]
	-- Add the parameters for the stored procedure here
	@PriceListId int,
	@INGroupId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@INGroupId = 0)
	BEGIN
    -- Insert statements for procedure here
	SELECT
	i.Name,
	i.Code,
	i.INGroupID,
	g.Name as 'GroupName',
	pd.Price,
	np.Name as 'PriceMethod',
	o.Name as 'OrderMethod',
	p.CurrentPriceListName
	
	from PriceListDetail pd
	INNER JOIN INMaster i on pd.INMasterID = i.INMasterID
	INNER JOIN INGroup g on i.INGroupID = g.INGroupID
	INNER JOIN NouPriceMethod np on np.NouPriceMethodID = pd.NouPriceMethodID
	INNER JOIN NouOrderMethod o on o.NouOrderMethodID = pd.NouOrderMethodID
	INNER JOIN PriceList p on p.PriceListID = pd.PriceListID
	where pd.pricelistid = @PriceListId
	END
	ELSE
	BEGIN
	SELECT
	i.Name,
	i.Code,
	i.INGroupID,
	g.Name as 'GroupName',
	pd.Price,
	np.Name as 'PriceMethod',
	o.Name as 'OrderMethod',
	p.CurrentPriceListName
	
	from PriceListDetail pd
	INNER JOIN INMaster i on pd.INMasterID = i.INMasterID
	INNER JOIN INGroup g on i.INGroupID = g.INGroupID
	INNER JOIN NouPriceMethod np on np.NouPriceMethodID = pd.NouPriceMethodID
	INNER JOIN NouOrderMethod o on o.NouOrderMethodID = pd.NouOrderMethodID
	INNER JOIN PriceList p on p.PriceListID = pd.PriceListID
	where pd.pricelistid = @PriceListId and i.INGroupID = @INGroupId
	END
	
END