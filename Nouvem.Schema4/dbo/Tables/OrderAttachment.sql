﻿CREATE TABLE [dbo].[OrderAttachment] (
    [OrderAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [AROrderID]         INT             NOT NULL,
    [FilePath]          NVARCHAR (50)   NULL,
    [FileName]          NVARCHAR (50)   NOT NULL,
    [AttachmentDate]    DATE            NOT NULL,
    [File]              VARBINARY (MAX) NOT NULL,
    [Deleted]           DATETIME        NULL,
    CONSTRAINT [PK_OrderAttachment] PRIMARY KEY CLUSTERED ([OrderAttachmentID] ASC),
    CONSTRAINT [FK_OrderAttachment_AROrder] FOREIGN KEY ([AROrderID]) REFERENCES [dbo].[AROrder] ([AROrderID])
);

