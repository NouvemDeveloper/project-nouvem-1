﻿CREATE TABLE [dbo].[QualityAssuranceHistory] (
    [QualityAssuranceHistoryID] INT           IDENTITY (1, 1) NOT NULL,
    [HerdNumber]                NVARCHAR (20) NOT NULL,
    [HerdOwner]                 NVARCHAR (50) NOT NULL,
    [CertExpiryDate]            DATETIME      NOT NULL,
    [Timestamp]                 DATETIME      NOT NULL,
    [Deleted]                   DATETIME      NULL,
    CONSTRAINT [PK_DEMQualityAssuranceHistory] PRIMARY KEY CLUSTERED ([QualityAssuranceHistoryID] ASC)
);

