﻿CREATE TABLE [dbo].[BPContact] (
    [BPContactID]    INT           IDENTITY (1, 1) NOT NULL,
    [BPMasterID]     INT           NOT NULL,
    [Title]          NCHAR (10)    NULL,
    [FirstName]      VARCHAR (30)  NULL,
    [LastName]       VARCHAR (30)  NULL,
    [JobTitle]       VARCHAR (20)  NULL,
    [Phone]          VARCHAR (20)  NOT NULL,
    [Mobile]         VARCHAR (20)  NOT NULL,
    [Email]          NVARCHAR (30) NOT NULL,
    [PrimaryContact] BIT           NULL,
    [Deleted]        BIT           NOT NULL,
    CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED ([BPContactID] ASC),
    CONSTRAINT [FK_Contact_BusinessPartner] FOREIGN KEY ([BPMasterID]) REFERENCES [dbo].[BPMaster] ([BPMasterID])
);

