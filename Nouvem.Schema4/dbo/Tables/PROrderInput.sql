﻿CREATE TABLE [dbo].[PROrderInput] (
    [PROrderInputID] INT      IDENTITY (1, 1) NOT NULL,
    [PROrderID]      INT      NOT NULL,
    [INMasterID]     INT      NOT NULL,
    [Selected]       BIT      NOT NULL,
    [Deleted]        DATETIME NULL,
    CONSTRAINT [PK_PRInputOrder] PRIMARY KEY CLUSTERED ([PROrderInputID] ASC),
    CONSTRAINT [FK_PRInputOrder_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]),
    CONSTRAINT [FK_PRInputOrder_PROrder] FOREIGN KEY ([PROrderID]) REFERENCES [dbo].[PROrder] ([PROrderID])
);

