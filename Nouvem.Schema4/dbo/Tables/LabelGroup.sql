﻿CREATE TABLE [dbo].[LabelGroup] (
    [LabelGroupID] INT        IDENTITY (1, 1) NOT NULL,
    [Name]         NCHAR (30) NULL,
    [Deleted]      BIT        NOT NULL,
    CONSTRAINT [PK_LabelGroup] PRIMARY KEY CLUSTERED ([LabelGroupID] ASC)
);

