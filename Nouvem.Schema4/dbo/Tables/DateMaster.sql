﻿CREATE TABLE [dbo].[DateMaster] (
    [DateMasterID]         INT           IDENTITY (1, 1) NOT NULL,
    [NouDateTypeID]        INT           NOT NULL,
    [DateCode]             NVARCHAR (50) NOT NULL,
    [DateDescription]      NVARCHAR (50) NULL,
    [GS1AIMasterID]        INT           NULL,
    [Deleted]              BIT           NOT NULL,
    [DateMasterID_BasedOn] INT           NULL,
    CONSTRAINT [PK_INDates] PRIMARY KEY CLUSTERED ([DateMasterID] ASC),
    CONSTRAINT [FK_DateMaster_NouDateType] FOREIGN KEY ([NouDateTypeID]) REFERENCES [dbo].[NouDateType] ([NouDateTypeID])
);

