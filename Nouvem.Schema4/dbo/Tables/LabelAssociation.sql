﻿CREATE TABLE [dbo].[LabelAssociation] (
    [LabelAssociationID] INT      IDENTITY (1, 1) NOT NULL,
    [BPGroupID]          INT      NULL,
    [INGroupID]          INT      NULL,
    [BPMasterID]         INT      NULL,
    [INMasterID]         INT      NULL,
    [Deleted]            DATETIME NULL,
    [UsingCustomerGroup] BIT      NULL,
    [UsingProductGroup]  BIT      NULL,
    [ItemLabelQty]       INT      NULL,
    [BoxLabelQty]        INT      NULL,
    [PalletLabelQty]     INT      NULL,
    [ShippingLabelQty]   INT      NULL,
    CONSTRAINT [PK_LabelAssociation] PRIMARY KEY CLUSTERED ([LabelAssociationID] ASC),
    CONSTRAINT [FK_LabelAssociation_BPGroup] FOREIGN KEY ([BPGroupID]) REFERENCES [dbo].[BPGroup] ([BPGroupID]),
    CONSTRAINT [FK_LabelAssociation_BPMaster] FOREIGN KEY ([BPMasterID]) REFERENCES [dbo].[BPMaster] ([BPMasterID]),
    CONSTRAINT [FK_LabelAssociation_INGroup] FOREIGN KEY ([INGroupID]) REFERENCES [dbo].[INGroup] ([INGroupID]),
    CONSTRAINT [FK_LabelAssociation_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID])
);

