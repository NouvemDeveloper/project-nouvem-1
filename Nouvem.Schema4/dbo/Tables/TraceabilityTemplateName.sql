﻿CREATE TABLE [dbo].[TraceabilityTemplateName] (
    [TraceabilityTemplateNameID] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                       NVARCHAR (50) NOT NULL,
    [Deleted]                    BIT           NOT NULL,
    CONSTRAINT [PK_TracebilityTemplateName] PRIMARY KEY CLUSTERED ([TraceabilityTemplateNameID] ASC)
);

