﻿CREATE view [dbo].[ViewGs1Master]
as
SELECT 
g.GS1AIMasterID,
g.AI,
g.OfficialDescription,
g.RelatedDescription,
g.[DataLength],
ISNULL(g.AI, '') + '  ' + ISNULL(g.OfficialDescription, '') as VisualDisplay      

FROM [dbo].[Gs1AIMaster] g