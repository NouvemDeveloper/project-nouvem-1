﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Nouvem.LabelDesign.Dialogue
{
    /// <summary>
    /// Interaction logic for FontDialogueView.xaml
    /// </summary>
    public partial class FontDialogueView : Window
    {
        public FontDialogueView()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }


        public Neodynamic.SDK.Printing.Font Font
        {
            get
            {
                return this.fontUC1.GetFont();
            }
            set
            {
                this.fontUC1.SetFont(value);
            }
        }


    }
}