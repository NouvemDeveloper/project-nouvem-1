﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nouvem.LabelDesign.Utility
{
    /// <summary>
    /// Interaction logic for DataBindingView.xaml
    /// </summary>
    public partial class DataBindingView : UserControl
    {
        public DataBindingView()
        {
            InitializeComponent();
        }


        public string ItemDataField
        {
            get
            {
                return txtItemDataField.Text;
            }
            set
            {
                txtItemDataField.Text = value;
            }
        }

        public string ItemDataFieldFormatString
        {
            get
            {
                return txtItemDataFieldFormatString.Text;
            }
            set
            {
                txtItemDataFieldFormatString.Text = value;
            }
        }

    }

}

