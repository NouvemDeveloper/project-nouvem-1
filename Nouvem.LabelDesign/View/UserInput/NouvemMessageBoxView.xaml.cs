﻿// -----------------------------------------------------------------------
// <copyright file="NouvemMessageBoxView.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.LabelDesign.View.UserInput
{
    using GalaSoft.MvvmLight.Messaging;
    using Nouvem.LabelDesign.Global;

    /// <summary>
    /// Interaction logic for NouvemMessageBoxView.xaml
    /// </summary>
    public partial class NouvemMessageBoxView
    {
        public NouvemMessageBoxView()
        {
            this.InitializeComponent();
            Messenger.Default.Register<string>(this, Token.CloseMessageBoxWindow, x => this.Close());
        }
    }
}