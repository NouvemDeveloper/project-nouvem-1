﻿CREATE TABLE [dbo].[APOrderAttachment] (
    [APOrderAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [APOrderID]           INT             NOT NULL,
    [FilePath]            NVARCHAR (50)   NULL,
    [FileName]            NVARCHAR (50)   NOT NULL,
    [AttachmentDate]      DATE            NOT NULL,
    [File]                VARBINARY (MAX) NOT NULL,
    [Deleted]             DATETIME        NULL,
    CONSTRAINT [PK_APSaleOrderAttachment] PRIMARY KEY CLUSTERED ([APOrderAttachmentID] ASC),
    CONSTRAINT [FK_APOrderAttachment_APOrder] FOREIGN KEY ([APOrderID]) REFERENCES [dbo].[APOrder] ([APOrderID])
);

