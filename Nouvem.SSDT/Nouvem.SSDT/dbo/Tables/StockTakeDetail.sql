﻿CREATE TABLE [dbo].[StockTakeDetail] (
    [StockTakeDetailID] INT             IDENTITY (1, 1) NOT NULL,
    [StockTakeID]       INT             NOT NULL,
    [INMasterID]        INT             NOT NULL,
    [OpeningQty]        DECIMAL (18, 2) NULL,
    [OpeningWgt]        DECIMAL (18, 5) NULL,
    [ClosingQty]        DECIMAL (18, 2) NULL,
    [ClosingWgt]        DECIMAL (18, 5) NULL,
    [Deleted]           DATETIME        NULL,
    CONSTRAINT [PK_StockTakeDetail] PRIMARY KEY CLUSTERED ([StockTakeDetailID] ASC),
    CONSTRAINT [FK_StockTakeDetail_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]),
    CONSTRAINT [FK_StockTakeDetail_StockTake] FOREIGN KEY ([StockTakeID]) REFERENCES [dbo].[StockTake] ([StockTakeID])
);

