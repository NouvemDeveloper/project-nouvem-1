﻿CREATE TABLE [dbo].[PRInputConfirmationOrder] (
    [PRInputConfirmationOrderID] INT      IDENTITY (1, 1) NOT NULL,
    [PROrderID]                  INT      NOT NULL,
    [NouConfirmationID]          INT      NOT NULL,
    [Selected]                   BIT      NOT NULL,
    [Deleted]                    DATETIME NULL,
    CONSTRAINT [FK_PRInputConfirmationOrder_PROrder] FOREIGN KEY ([PROrderID]) REFERENCES [dbo].[PROrder] ([PROrderID])
);

