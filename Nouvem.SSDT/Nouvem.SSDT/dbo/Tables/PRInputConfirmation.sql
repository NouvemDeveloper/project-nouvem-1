﻿CREATE TABLE [dbo].[PRInputConfirmation] (
    [PRInputConfirmationID] INT      IDENTITY (1, 1) NOT NULL,
    [PRSpecID]              INT      NOT NULL,
    [NouConfirmationID]     INT      NOT NULL,
    [Selected]              BIT      NOT NULL,
    [Deleted]               DATETIME NULL,
    CONSTRAINT [PK_PRInputConfirmation] PRIMARY KEY CLUSTERED ([PRInputConfirmationID] ASC),
    CONSTRAINT [FK_PRInputConfirmation_PRSpec] FOREIGN KEY ([PRSpecID]) REFERENCES [dbo].[PRSpec] ([PRSpecID])
);

