﻿



CREATE VIEW [dbo].[ViewBusinessPartner]
AS
SELECT bpm.BPMasterID, bpm.Code, bpm.Name, bpm.Remarks, bpm.Notes, bpm.PopUpNotes, bpm.Tel, bpm.FAX, bpm.Web, bpm.VATNo, bpm.CompanyNo,
bpm.Uplift, bpm.CreditLimit, bpm.PORequired, bpm.RouteMasterID, bpm.OnHold, bpm.Balance, bpm.ActiveFrom, bpm.ActiveTo, bpm.InActiveFrom,bpm.InActiveTo,
bpt.NouBPTypeID, bpt.Type, bpm.PriceListID,
bpg.BPGroupID, bpg.BPGroupName,
bpc.BPCurrencyID, bpc.Name AS CurrencyName, bpc.RateToBase, bpc.Symbol
FROM bpmaster bpm inner join noubptype bpt on bpm.noubptypeid = bpt.noubptypeid
LEFT OUTER JOIN
dbo.BPGroup bpg ON bpm.BPGroupID = bpg.BPGroupID LEFT OUTER JOIN
dbo.BPCurrency bpc ON bpm.BPCurrencyID = bpc.BPCurrencyID
 
WHERE bpt.Deleted = 0




