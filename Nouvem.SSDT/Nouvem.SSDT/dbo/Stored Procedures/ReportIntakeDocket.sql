﻿

-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the intake docket details
-- =============================================
CREATE PROCEDURE [dbo].[ReportIntakeDocket]
	-- Add the parameters for the stored procedure here
	@GoodsInID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT gr.Number AS 'Document No', 
	       gr.CreationDate AS 'Intake Date',
		   gr.APGoodsReceiptID,
		   s.TransactionWeight AS 'Weight',
		   s.TransactionQTY AS 'Quantity',
		   b.Name as 'Supplier',
		   b.Code,
		   m.Name AS 'Product',
		   m.INMasterID,
		   s.StockTransactionID,
		   s.Serial AS 'LabelID',
		   bn.Number,

		   (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'KillNumber') AS 'Kill Number',
           (SELECT Value FROM TransactionTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON TransactionTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'SupplierBatch') AS 'Supplier Batch'   


		   FROM StockTransaction s      		
		          INNER JOIN APGoodsReceiptDetail grd
	                  ON grd.APGoodsReceiptDetailID = s.MasterTableID
			      INNER JOIN APGoodsReceipt gr
				      ON grd.APGoodsReceiptID = gr.APGoodsReceiptID
				  INNER JOIN INMaster m
				      ON m.INMasterID = s.INMasterID
				  LEFT JOIN BPMaster b
				      ON b.BPMasterID = gr.BPMasterID_Supplier	
				  LEFT JOIN BatchNumber bn
				      ON bn.BatchNumberID = s.BatchNumberID		       
	

	WHERE gr.APGoodsReceiptID = @GoodsInID
	         
END


