﻿

-- =============================================
-- Author:		brian murray
-- Create date: 28/03/16
-- Description:	Fields used for the intake label.
-- =============================================
CREATE PROCEDURE [dbo].[LabelOutOfProd]
  @StockTransactionID INT 

AS
BEGIN    

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT b.Number AS 'Batch/Lot Number',
	       s.Serial AS 'Serial Number',	
		   s.Pieces,	   
		   CONVERT(DECIMAL(18,2), s.TransactionWeight) AS 'Product Net Weight in kg',
		   CONVERT(DECIMAL(18,2), s.Tare) AS 'Tare',		
		   CONVERT(DECIMAL(18,2), s.GrossWeight) AS 'Gross Wgt',
		   s.TransactionQTY,
		   m.Code AS 'Product Code',
		   m.Name as 'Product Name',
		    p.MultiLineField1 as '#MultiLine Text1',
		   p.MultiLineField2 as '#MultiLine Text2',
		   p.Field1 as '#Text1',
		   p.Field2 as '#Text2',
		   p.Field3 as '#Text3',
		   p.Field4 as '#Text4',
		   p.Field5 as '#Text5',
		   p.Field6 as '#Text6',
		   p.Field7 as '#Text7',
		   p.Field8 as '#Text8',
		   p.Field9 as '#Text9',
		   p.Field10 as '#Text10',
		   p.Field11 as '#Text11',
		   p.Field12 as '#Text12',
		   p.Field13 as '#Text13',
		   p.Field14 as '#Text14',
		   p.Field15 as '#Text15',
		   p.Field16 as '#Text16',
		   p.Field17 as '#Text17',
		   p.Field18 as '#Text18',
		   p.Field19 as '#Text19',
		   p.Field20 as '#Text20',
		   p.Field21 as '#Text21',
		   p.Field22 as '#Text21',
		   p.Field23 as '#Text23',
		   p.Field24 as '#Text24',
		   p.Field25 as '#Text25',
		   p.Field26 as '#Text26',
		   p.Field27 as '#Text27',
		   p.Field28 as '#Text28',
		   p.Field29 as '#Text29',
		   p.Field30 as '#Text30',		
	       CONVERT(VARCHAR, s.Serial) AS 'Barcode_Serial',
		     (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'KillDate') AS 'Kill Date',
		   (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'BornIn') AS 'Born In',
            (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'RearedIn') AS 'Reared In',
            (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'FattenedIn') AS 'Fattened In',
		    (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'SlaughteredIn') AS 'Slaughtered In',
            (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'CutIn') AS 'Cut In',
            (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
			   								    WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
     											AND TraceabilityTemplateMaster.TraceabilityCode = 'BestBefore') AS 'Best Before Date',
         (SELECT SUBSTRING(Value, 1, 10) FROM BatchTraceability INNER JOIN DateMaster 
		                                        ON BatchTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
     											AND DateMaster.DateCode = 'PackedOnDate') AS 'Packed On Date',
         (SELECT SUBSTRING(Value, 1, 10) FROM TransactionTraceability INNER JOIN DateMaster 
		                                        ON TransactionTraceability.DateMasterID = DateMaster.DateMasterID
			   								    WHERE TransactionTraceability.StockTransactionID = s.StockTransactionID
     											AND DateMaster.DateCode = 'UseBy') AS 'Use By Date',
		   (SELECT Value FROM BatchTraceability INNER JOIN TraceabilityTemplateMaster 
		                                        ON BatchTraceability.TraceabilityTemplateMasterID = TraceabilityTemplateMaster.TraceabilityTemplatesMasterID
												WHERE BatchTraceability.BatchNumberID = s.BatchNumberID
												AND TraceabilityTemplateMaster.TraceabilityCode = 'CountryOfOrigin') AS 'Country Of Origin'

	 FROM StockTransaction s 
	  INNER join INMaster m on s.INMasterID = m.INMasterID
	  INNER JOIN PROrder o ON o.PROrderID = s.MasterTableID
	  INNER JOIN BatchNumber b ON s.BatchNumberID = b.BatchNumberID
	  LEFT JOIN ProductLabelField p on m.ProductLabelFieldID = p.ProductLabelFieldID

	WHERE s.StockTransactionID = @StockTransactionID
END












