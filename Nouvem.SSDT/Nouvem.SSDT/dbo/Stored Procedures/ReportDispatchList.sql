﻿-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the dispatch identifier values for all dockets falling between the input dates.
-- =============================================
CREATE PROCEDURE ReportDispatchList 
	-- Add the parameters for the stored procedure here
	@Start AS Datetime,
	@End AS Datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT d.ARDispatchID, 
	       d.DeliveryDate,
	       d.Number,
		   b.Name,
		   b.Code

	FROM ARDispatch d 
	   INNER JOIN NouDocStatus n
	          ON  n.NouDocStatusID = d.NouDocStatusID
	   LEFT JOIN  BPMaster b 
	          ON  b.BPMasterID = d.BPMasterID_Customer
    
	WHERE n.Value = 'Complete'
	  AND d.DeliveryDate BETWEEN @Start AND @End	    
	                            
END
