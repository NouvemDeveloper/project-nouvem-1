﻿


-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the dispatch docket transaction details
-- =============================================
CREATE PROCEDURE [dbo].[ReportDispatchDocketDetail] 
	-- Add the parameters for the stored procedure here
	@DispatchID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  d.ARDispatchID,       
		d.DeliveryDate,
		d.DocumentDate,
		d.Number,
		d.CustomerPOReference,
        b.Name,
		b.Code,
		a.AddressLine1,
		a.AddressLine2,
		a.AddressLine3,
		a.AddressLine4,
		invoice.AddressLine1 AS 'InvoiceAddressLine1',
		invoice.AddressLine2 AS 'InvoiceAddressLine2',
		invoice.AddressLine3 AS 'InvoiceAddressLine3',
		invoice.AddressLine4 AS 'InvoiceAddressLine4',
		dd.ARDispatchDetailID,
		dd.WeightDelivered,
		dd.QuantityDelivered,
		s.Serial,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		bn.Number AS 'BatchNo'
    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN StockTransaction s
	      ON s.MasterTableID = dd.ARDispatchDetailID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID
       INNER JOIN BPMaster b 
          ON d.BPMasterID_Customer = b.BPMasterID
	   LEFT JOIN BatchNumber bn
	      ON bn.BatchNumberID = s.BatchNumberID
       LEFT JOIN BPAddress a
          ON d.BPAddressID_Delivery = a.BPAddressID
       LEFT JOIN BPAddress invoice
          ON d.BPAddressID_Invoice = invoice.BPAddressID
     WHERE d.ARDispatchID = @DispatchID  AND dd.Deleted IS NULL
     END





