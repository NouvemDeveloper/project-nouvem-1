﻿


-- =============================================
-- Author:		brian murray
-- Create date: 06/04/2016
-- Description:	Gets the dispatch summary data.
-- =============================================
CREATE PROCEDURE [dbo].[ReportDispatchDocketSummary] 
	-- Add the parameters for the stored procedure here
	@Start AS DateTime,
	@End AS DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  d.ARDispatchID,
        d.TotalExVAT,
		d.SubTotalExVAT,
		d.GrandTotalIncVAT,
		d.VAT,	
		d.DeliveryDate,
		d.DocumentDate,
		d.Number,
		d.CustomerPOReference,       
		dd.WeightDelivered,
		dd.QuantityDelivered,
		dd.UnitPriceAfterDiscount,
		dd.TotalExclVAT,
		i.Code AS 'ProductCode',
		i.Name AS 'ProductName',
		i.INGroupID,
		g.Name AS 'GroupName'

    FROM ARDispatch d
       INNER JOIN ARDispatchDetail dd
          ON d.ARDispatchID = dd.ARDispatchID
	   INNER JOIN NouDocStatus nds
	      ON nds.NouDocStatusID = d.NouDocStatusID
       INNER JOIN INMaster i
          ON dd.INMasterID = i.INMasterID	
	   INNER JOIN INGroup g
	      ON g.INGroupID = i.INGroupID
       
     WHERE nds.Value = 'Complete' 
	   AND d.DeliveryDate BETWEEN @Start AND @End
	   AND dd.Deleted IS NULL

     END
