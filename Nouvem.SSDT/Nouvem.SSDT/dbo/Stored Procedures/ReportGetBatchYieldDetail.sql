﻿

-- =============================================
-- Author:		brian murray	
-- Create date: 04/04/2016
-- Description:	Gets the batch yield transaction details.
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetBatchYieldDetail] 
	@PROrderID int,
	@INMasterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT m.Name, s.Serial, s.TransactionWeight, s.TransactionQTY, 1 as 'BoxCount', s.MasterTableID as 'PROrderID', s.INMasterID
	FROM StockTransaction s 
	INNER JOIN INMaster m ON s.INMasterID = m.INMasterID
    WHERE s.MasterTableID = @PROrderID and s.INMasterID = @INMasterID
END


