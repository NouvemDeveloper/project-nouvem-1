﻿// -----------------------------------------------------------------------
// <copyright file="FTraceConnect.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.FTrace
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Xml.Linq;
    using Nouvem.Shared;

    public class FTraceConnect
    {
        /// <summary>
        /// Event used to broadcast the touch pad data.
        /// </summary>
        public event EventHandler DataSent;
        private readonly string filePath;
        private readonly string url;
        private readonly string userName;
        private readonly string password;

        /// <summary>
        /// Connection set up.
        /// </summary>
        /// <param name="path">The ftrace xml files path.</param>
        /// <param name="url">The ftrace endpoint url.</param>
        /// <param name="userName">The ftrace network user name.</param>
        /// <param name="password">The ftrace network password.</param>
        public FTraceConnect(string path, string url, string userName, string password)
        {
            this.filePath = path;
            this.url = url;
            this.userName = userName;
            this.password = password;
        }

        /// <summary>
        /// Generates and sends ftrace data corresponding to a delivery.
        /// </summary>
        /// <param name="dt">The delivery\processing out data.</param>
        /// <param name="proc">THe processing in data.</param>
        /// <param name="slaughter">The slaughter data.</param>
        public void GenerateFTraceData(DataTable dt, DataTable proc, DataTable slaughter)
        {
            if (dt == null)
            {
                this.GenerateSlaughterData(slaughter);
                return;
            }

            var traceData = new List<TraceData>();
            foreach (DataRow row in dt.Rows)
            {
                traceData.Add(new TraceData
                {
                    SenderGLN = row["SenderGLN"].ToString(),
                    RecipientGLN = row["RecipientGLN"].ToString(),
                    EventDate = row["EventDate"].ToString(),
                    EventDateBatch = row["EventDateBatch"].ToString(),
                    EventTimeZoneOffset = row["EventTimeZoneOffset"].ToString(),
                    GTINLot = row["GTINLot"].ToString(),
                    CreationDate = row["CreationDate"].ToString(),
                    Quantity = row["Quantity"].ToString(),
                    BestBefore = row["BestBefore"].ToString(),
                    PONumber = row["PurchaseOrder"].ToString()
                });
            }

            if (slaughter != null)
            {
                this.GenerateSlaughterData(slaughter);
            }

            this.GenerateProcessingData(traceData, proc);
            this.GenerateDeliveryData(traceData);
        }

        private void GenerateSlaughterData(DataTable dt)
        {
            var traceData = new List<TraceData>();
            foreach (DataRow row in dt.Rows)
            {
                traceData.Add(new TraceData
                {
                    SenderGLN = row["SenderGLN"].ToString(),
                    RecipientGLN = row["RecipientGLN"].ToString(),
                    EventDate = row["EventDate"].ToString(),
                    EventTimeZoneOffset = row["EventTimeZoneOffset"].ToString(),
                    GTINLot = row["GTINLot"].ToString(),
                    Quantity = row["Quantity"].ToString(),
                    AddressLine1 = row["AddressLine1"].ToString(),
                    AddressLine2 = row["AddressLine2"].ToString(),
                    AddressLine3 = row["AddressLine3"].ToString(),
                    AddressLine4 = row["AddressLine4"].ToString(),
                    PostCode = row["PostCode"].ToString(),
                    MinKillDate = row["MinKillDate"].ToString(),
                    MaxKillDate = row["MaxKillDate"].ToString(),
                    Supplier = row["Supplier"].ToString(),
                    CountryCode = row["CountryCode"].ToString(),
                    AnimalCount = row["AnimalCount"].ToString(),
                    LotNumber = row["Number"].ToString(),
                    RowNumber = row["RowNumber"].ToString(),
                    CreationDate = row["CreationDate"].ToString(),
                    CountryOfBirth = row["CountryOfBirth"].ToString()
                });
            }

            foreach (var trace in traceData)
            {
                var lotTotal = traceData.Where(x => x.LotNumber == trace.LotNumber).Sum(x => x.AnimalCount.ToInt());
                var lotPercentage = trace.AnimalCount.ToDouble().ToDoubleGetPercentage(lotTotal.ToDouble());

                var doc = new XDocument(new XDeclaration("1.0", "utf-8", null));

                var creationDate = trace.CreationDate;
                var eventTime = trace.EventDate;
                var eventOffset = trace.EventTimeZoneOffset;
                var source = trace.SenderGLN;
                var destination = trace.RecipientGLN;
                XNamespace epcis = "urn:epcglobal:epcis:xsd:1";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace fT = "http://ns.ftrace.com/epcis";
                XNamespace fT_fish = "http://ns.fish.ftrace.com";

                var header =
                    new XElement(epcis + "EPCISDocument",
                        new XAttribute(XNamespace.Xmlns + "epcis", "urn:epcglobal:epcis:xsd:1"),
                        new XAttribute("schemaVersion", "1.1"),
                        new XAttribute("creationDate", creationDate),
                        new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                        new XAttribute(xsi + "schemaLocation", "urn:epcglobal:epcis:xsd:1 EPCglobal-epcis-1_1.xsd"),
                        new XAttribute(XNamespace.Xmlns + "fT", "http://ns.ftrace.com/epcis"),
                        new XAttribute(XNamespace.Xmlns + "fT_fish", "http://ns.fish.ftrace.com")
                    );

                var epcisBody = new XElement("EPCISBody");
                var eventList = new XElement("EventList");

                var objectEvent = new XElement("ObjectEvent",
                    new XElement("eventTime", eventTime),
                    new XElement("eventTimeZoneOffset", eventOffset),
                    new XElement("epcList"),
                    new XElement("action", "ADD"),
                    new XElement("bizStep", "urn:epcglobal:cbv:bizstep:commissioning")
                );

                objectEvent.Add(new XElement("readPoint",
                    new XElement("id", source)
                ));

                objectEvent.Add(new XElement("bizLocation",
                    new XElement("id", destination)
                ));

                var extension = new XElement("extension",
                    new XElement("quantityList",
                        new XElement("quantityElement",
                            new XElement("epcClass", trace.GTINLot),
                            new XElement("quantity", trace.Quantity),
                            new XElement("uom", "KGM"))),

                    new XElement("ilmd",
                        new XElement(fT + "eventPeriodEnd", trace.MaxKillDate),
                        new XElement(fT + "preStageDetails",
                            new XElement(fT + "agricultureDetails",
                                    new XElement(fT + "address",
                                       new XElement(fT + "name", trace.Supplier),
                                       new XElement(fT + "streetAddressOne", trace.AddressLine1),
                                       new XElement(fT + "streetAddressTwo", trace.AddressLine2),
                                       new XElement(fT + "countyCode", trace.AddressLine3),
                                       new XElement(fT + "countryCode", trace.CountryCode.Trim())),
                                    new XElement(fT + "proportionPercentOfLot", lotPercentage),
                                    new XElement(fT + "proportionRankingOfLot", trace.RowNumber),
                                    new XElement(fT + "numberOfAnimals", trace.AnimalCount),
                                    new XElement(fT + "countriesOfBirth", new XElement(fT + "countryCode", trace.CountryOfBirth.Trim()))
                                       )
                                    )

                        )
                    );


                objectEvent.Add(extension);
                eventList.Add(objectEvent);
                epcisBody.Add(eventList);
                header.Add(epcisBody);
                doc.Add(header);

                var fileDelivery = Path.Combine(this.filePath, string.Format("{0}{1}.xml", "Slaughter", DateTime.Now.ToString("ddMMyyyyHHmmssffff")));
                File.WriteAllText(fileDelivery, doc.ToString());
                this.SendFile(fileDelivery);
            }
        }

        /// <summary>
        /// Generates and sends the processing data.
        /// </summary>
        /// <param name="traceData">The batch out data.</param>
        /// <param name="dt">The batch in data.</param>
        private void GenerateProcessingData(IList<TraceData> traceDataAll, DataTable dt)
        {
            var traceProcData = new List<TraceData>();
            foreach (DataRow row in dt.Rows)
            {
                traceProcData.Add(new TraceData
                {
                    GTINLot = row["GTINLot"].ToString(),
                    Quantity = row["Quantity"].ToString()
                });
            }

            var creationDate = traceDataAll.First().CreationDate;
            var eventTime = traceDataAll.First().EventDateBatch;
            var eventOffset = traceDataAll.First().EventTimeZoneOffset;
            var source = traceDataAll.First().SenderGLN;
            var destination = traceDataAll.First().RecipientGLN;

            XNamespace epcis = "urn:epcglobal:epcis:xsd:1";
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            XNamespace fT = "http://ns.ftrace.com/epcis";
            XNamespace fT_fish = "http://ns.fish.ftrace.com";

            var groupedTraceData = traceDataAll.GroupBy(x => x.BestBefore);
            foreach (var traceData in groupedTraceData)
            {
                var doc = new XDocument(new XDeclaration("1.0", "utf-8", null));
                var bestBefore = traceData.First().BestBefore;
                var header =
               new XElement(epcis + "EPCISDocument",
                   new XAttribute(XNamespace.Xmlns + "epcis", "urn:epcglobal:epcis:xsd:1"),
                   new XAttribute("schemaVersion", "1.1"),
                   new XAttribute("creationDate", creationDate),
                   new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                   new XAttribute(xsi + "schemaLocation", "urn:epcglobal:epcis:xsd:1 EPCglobal-epcis-1_1.xsd"),
                   new XAttribute(XNamespace.Xmlns + "fT", "http://ns.ftrace.com/epcis"),
                   new XAttribute(XNamespace.Xmlns + "fT_fish", "http://ns.fish.ftrace.com")
               );

                var epcisBody = new XElement("EPCISBody");
                var eventList = new XElement("EventList");

                var transformationEvent = new XElement("TransformationEvent",
                    new XElement("eventTime", eventTime),
                    new XElement("eventTimeZoneOffset", eventOffset));

                var inputQuantityList = new XElement("inputQuantityList");

                foreach (var data in traceProcData)
                {
                    var quantityElement = new XElement("quantityElement",
                        new XElement("epcClass", data.GTINLot),
                        new XElement("quantity", data.Quantity)
                    );

                    inputQuantityList.Add(quantityElement);
                }

                transformationEvent.Add(inputQuantityList);

                var outputQuantityList = new XElement("outputQuantityList");

                foreach (var data in traceData)
                {
                    var quantityElement = new XElement("quantityElement",
                        new XElement("epcClass", data.GTINLot),
                        new XElement("quantity", data.Quantity)
                    );

                    outputQuantityList.Add(quantityElement);
                }

                transformationEvent.Add(outputQuantityList);

                var bizStep = new XElement("bizStep", "http://epcis.ftrace.com/voc/bizstep/splitting");
                transformationEvent.Add(bizStep);

                transformationEvent.Add(new XElement("readPoint",
                    new XElement("id", source)
                ));

                transformationEvent.Add(new XElement("bizLocation",
                    new XElement("id", destination)
                ));

                transformationEvent.Add(new XElement("ilmd",
                    new XElement(fT + "bestBeforeDate", bestBefore)
                ));

                var extension = new XElement("extension");
                extension.Add(transformationEvent);
                eventList.Add(extension);
                epcisBody.Add(eventList);
                header.Add(epcisBody);
                doc.Add(header);

                var file = Path.Combine(this.filePath, string.Format("{0}{1}.xml", "Processing", DateTime.Now.ToString("ddMMyyyyHHmmssffff")));
                File.WriteAllText(file, doc.ToString());
                this.SendFile(file);
            }
        }

        /// <summary>
        /// Generates and sends the delivery data.
        /// </summary>
        /// <param name="traceData">The delivery data.</param>
        private void GenerateDeliveryData(IList<TraceData> traceData)
        {
            var doc = new XDocument(new XDeclaration("1.0", "utf-8", "Yes"));

            var creationDate = traceData.First().CreationDate;
            var eventTime = traceData.First().EventDate;
            var eventOffset = traceData.First().EventTimeZoneOffset;
            var source = traceData.First().SenderGLN;
            var destination = traceData.First().RecipientGLN;
            var poOrderNo = traceData.First().PONumber;
            XNamespace epcis = "urn:epcglobal:epcis:xsd:1";
            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            XNamespace fT = "http://ns.ftrace.com/epcis";
            XNamespace fT_fish = "http://ns.fish.ftrace.com";

            var header =
                new XElement(epcis + "EPCISDocument",
                    new XAttribute(XNamespace.Xmlns + "epcis", "urn:epcglobal:epcis:xsd:1"),
                    new XAttribute("schemaVersion", "1.1"),
                    new XAttribute("creationDate", creationDate),
                    new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                    new XAttribute(xsi + "schemaLocation", "urn:epcglobal:epcis:xsd:1 EPCglobal-epcis-1_1.xsd"),
                    new XAttribute(XNamespace.Xmlns + "fT", "http://ns.ftrace.com/epcis"),
                    new XAttribute(XNamespace.Xmlns + "fT_fish", "http://ns.fish.ftrace.com")
                );

            var epcisBody = new XElement("EPCISBody");
            var eventList = new XElement("EventList");
            var objectEvent = new XElement("ObjectEvent",
                new XElement("eventTime", eventTime),
                new XElement("eventTimeZoneOffset", eventOffset),
                new XElement("epcList"),
                new XElement("action", "OBSERVE"),
                new XElement("bizStep", "urn:epcglobal:cbv:bizstep:departing")
                );

            var readPoint = new XElement("readPoint",
                new XElement("id", source));

            var bizStep = new XElement("bizTransactionList",
                new XElement("bizTransaction", new XAttribute("type", "urn:epcglobal:cbv:btt:po"), poOrderNo));

            var extension = new XElement("extension");
            var quantityList = new XElement("quantityList");

            foreach (var data in traceData)
            {
                var quantityElement = new XElement("quantityElement",
                    new XElement("epcClass", data.GTINLot),
                    new XElement("quantity", data.Quantity)
                    );

                quantityList.Add(quantityElement);
            }

            extension.Add(quantityList);

            extension.Add(new XElement("sourceList",
                new XElement("source", new XAttribute("type", "urn:epcglobal:cbv:sdt:possessing_party"), source)
            ));

            extension.Add(new XElement("destinationList",
                new XElement("destination", new XAttribute("type", "urn:epcglobal:cbv:sdt:possessing_party"), destination)
            ));

            objectEvent.Add(readPoint);
            objectEvent.Add(bizStep);
            objectEvent.Add(extension);
            eventList.Add(objectEvent);
            epcisBody.Add(eventList);
            header.Add(epcisBody);
            doc.Add(header);

            var file = Path.Combine(this.filePath, string.Format("{0}{1}.xml", "Delivery", DateTime.Now.ToString("ddMMyyyyHHmmssffff")));
            File.WriteAllText(file, doc.ToString());
            this.SendFile(file);
        }

        /// <summary>
        /// Sends an ftrace xml file to the endpoint.
        /// </summary>
        /// <param name="file">Thefile to send.</param>
        public void SendFile(string file)
        {
            try
            {
                using (var client = new WebClient())
                {
                    var localCredentials = $"{this.userName}:{this.password}";
                    var credentials = Convert.ToBase64String(Encoding.UTF8.GetBytes(localCredentials));
                    client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);

                    byte[] result = client.UploadFile(this.url, file);
                    string responseAsString = Encoding.Default.GetString(result);
                    this.OnDataSent(new EventArgs());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Creates a new event, to broadcast the touch pad data.
        /// </summary>
        /// <param name="e">The event argument.</param>
        private void OnDataSent(EventArgs e)
        {
            if (this.DataSent != null)
            {
                this.DataSent(this, e);
            }
        }

        public string PostXMLData(string destinationUrl, string requestXml)
        {
            try
            {
                var fileToSend = @"C:\Nouvem\Transfer\correct_slaughter.xml";
                var url = @"http://access-onb.ftrace.com/ftrace.epcis.converter/CaptureServiceConverter";
                url = @"http://access-onb.ftrace.com/ftrace.epcis.capture/CaptureServiceProxyServlet";


                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(fileToSend);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";

                var toEncodeAsBytes = Encoding.UTF8.GetBytes("brian@dunleavymeats.com");
                var username = Convert.ToBase64String(toEncodeAsBytes);

                var toEncodeAsBytes1 = Encoding.UTF8.GetBytes("?z;Byh$0xI:!t:OV");
                var localPassword = Convert.ToBase64String(toEncodeAsBytes1);

                //request.UseDefaultCredentials = true;
                request.Credentials = new System.Net.NetworkCredential(username, localPassword);
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    return responseStr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            return null;
        }
    }
}




