﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.FTrace
{
    public class TraceData
    {
        public string SenderGLN { get; set; }
        public string RecipientGLN { get; set; }
        public string EventDate { get; set; }
        public string EventDateBatch { get; set; }
        public string EventTimeZoneOffset { get; set; }
        public string GTINLot { get; set; }
        public string CreationDate { get; set; }
        public string Quantity { get; set; }
        public string BestBefore { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string PostCode { get; set; }
        public string MinKillDate { get; set; }
        public string MaxKillDate { get; set; }
        public string Supplier { get; set; }
        public string CountryCode { get; set; }
        public string AnimalCount { get; set; }
        public string LotNumber { get; set; }
        public string RowNumber { get; set; }
        public string CountryOfBirth { get; set; }
        public string PONumber { get; set; }
    }
}

