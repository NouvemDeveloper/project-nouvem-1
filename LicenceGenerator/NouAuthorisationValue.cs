//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LicenceGenerator
{
    using System;
    using System.Collections.Generic;
    
    public partial class NouAuthorisationValue
    {
        public NouAuthorisationValue()
        {
            this.NouLicenceTypeRules = new HashSet<NouLicenceTypeRule>();
        }
    
        public int NouAuthorisationValueID { get; set; }
        public string Description { get; set; }
        public bool Deleted { get; set; }
    
        public virtual ICollection<NouLicenceTypeRule> NouLicenceTypeRules { get; set; }
    }
}
