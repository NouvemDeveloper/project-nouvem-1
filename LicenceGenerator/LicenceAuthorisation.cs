﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenceGenerator
{
    public class LicenceAuthorisation
    {
        public NouAuthorisation Authorisation { get; set; }


        public bool FullAccess { get; set; }

        public bool NoAccess { get; set; }

        public bool ReadOnly { get; set; }
    }
}
