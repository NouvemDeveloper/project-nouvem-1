﻿// -----------------------------------------------------------------------
// <copyright file="SaleItemData.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace Nouvem.Printing.BusinessObject
{
    public class SaleItemData
    {
        /// <summary>
        /// Gets or sets the amount sold.
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Gets or sets the item description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the item price total.
        /// </summary>
        public string Total { get; set; }
    }
}
