﻿// -----------------------------------------------------------------------
// <copyright file="Data.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Runtime.CompilerServices;
using Nouvem.Logging;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Trigger
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Scheduler.Global;
    using Nouvem.Scheduler.Jobs;
    using Nouvem.Scheduler.Jobs.Handler;
    using Nouvem.Scheduler.Model.Repository;
    using Nouvem.Scheduler.Properties;
    using Quartz;
    using Quartz.Impl;

    public class Data
    {
        private ILogger log = new Logger();

        /// <summary>
        /// Handles the service start operations.
        /// </summary>
        public void SchedulerStart()
        {
            this.log.LogInfo(this.GetType(), "SchedulerStarting: Registering start up..Version 23");
            var schedFact = new StdSchedulerFactory();
        
            var sched = schedFact.GetScheduler();
            sched.Start();
   
            var job = JobBuilder.Create<SchedulerStartProcessing>()
                .WithIdentity("SchedulerStart")
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            var trigger = TriggerBuilder.Create()
              .WithIdentity("SchedulerTrigger")
              .StartNow()
              .Build();

            sched.ScheduleJob(job, trigger);
            this.log.LogInfo(this.GetType(), "SchedulerStart(): Registered");
        }

        /// <summary>
        /// Saves the scheduler settings.
        /// </summary>
        public void SaveSettings()
        {
            var settings = new List<DeviceSetting>();
            var deviceId = SchedulerGlobal.Device.DeviceID;

            #region settings

            settings.Add(new DeviceSetting
            {
                Name = "Scheduler_DispatchReport",
                Value1 = Settings.Default.DispatchReportPrinter1,
                Value2 = Settings.Default.DispatchReportPrinter2,
                Value3 = Settings.Default.DispatchReportPrinter3,
                Value4 = Settings.Default.DispatchReportPrinter4,
                DeviceMasterID = deviceId
            });

            #endregion

            var data = new DataRepository();
            data.UpdateDeviceSettings(settings);
        }


        public class SchedulerStartProcessing : HandlerBase, IJob
        {
            public void Execute(IJobExecutionContext context)
            {
                this.log.LogInfo(this.GetType(), "SchedulerStartProcessing(): Starting...");
                this.GetDeviceSettings();
                this.ScheduleJobs();
            }

            /// <summary>
            /// Gets the scheduler settings.
            /// </summary>
            private void GetDeviceSettings()
            {
                var settings = Data.GetDeviceSettings();
                foreach (var setting in settings)
                {
                    #region settings

                    if (setting.Name.CompareIgnoringCase("ReportServerPath"))
                    {
                        Settings.Default.ReportServerPath = setting.Value1;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("SSRSWebServiceURL"))
                    {
                        Settings.Default.SSRSWebServiceURL = setting.Value1;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("SSRSUserName"))
                    {
                        Settings.Default.SSRSUserName = setting.Value1;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("SSRSPassword"))
                    {
                        Settings.Default.SSRSPassword = setting.Value1;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("SSRSDomain"))
                    {
                        Settings.Default.SSRSDomain = setting.Value1;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_Settings"))
                    {
                        ApplicationSettings.PrintPalletLabelData = setting.Value1.ToBool();
                        ApplicationSettings.PrintPalletLabelDataCheck = setting.Value2.ToInt();
                        ApplicationSettings.PrinterPalletIP = setting.Value3 != null ? setting.Value3.Trim() : string.Empty;
                        ApplicationSettings.PrinterPalletIP2 = setting.Value4 != null ? setting.Value4.Trim() : string.Empty;
                        ApplicationSettings.PrinterPallet2Device = setting.Value5.ToInt();
                        ApplicationSettings.ConnectionString = setting.Value6;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_InvoiceReport"))
                    {
                        Settings.Default.InvoiceReportPrinter1 = setting.Value1;
                        Settings.Default.InvoiceReportPrinter2 = setting.Value2;
                        Settings.Default.InvoiceReportPrinter3 = setting.Value3;
                        Settings.Default.InvoiceReportPrinter4 = setting.Value4;
                        Settings.Default.InvoiceReportPrinterCopies = setting.Value5.ToInt();
                        Settings.Default.InvoiceReportConnect = setting.Value6.ToBool();

                        Settings.Default.IntakeReportPrinter1 = setting.Value8;
                        Settings.Default.IntakeReportPrinter2 = setting.Value9;
                        Settings.Default.IntakeReportPrinter3 = setting.Value10;
                        Settings.Default.IntakeReportPrinter4 = setting.Value11;
                        Settings.Default.IntakeReportPrinterCopies = setting.Value12.ToInt();
                        Settings.Default.IntakeDetailsReportConnect = setting.Value13.ToBool();
                        ApplicationSettings.InvoiceName = setting.Value14;
                        ApplicationSettings.CheckForUnifyOrder = setting.Value18.ToBool();
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_Job_ReportProcessing"))
                    {
                        Settings.Default.ReportsProcessingInterval = setting.Value1.ToInt();
                        Settings.Default.RunReportsProcessing = setting.Value2.ToBool();
                        ApplicationSettings.SyncAccountsData = setting.Value3.ToBool();
                        ApplicationSettings.DataIntegrationProcessingInterval = setting.Value4.ToInt();
                        ApplicationSettings.BackUpDatabase = setting.Value5.ToBool();
                        ApplicationSettings.BackUpDatabaseTime = setting.Value6.ToInt();
                        ApplicationSettings.BackUpDatabaseTimeMins = setting.Value7.ToInt();
                        ApplicationSettings.RemoveBatchesTime = setting.Value8.ToInt();
                        ApplicationSettings.RemoveBatchesTimeMins = setting.Value9.ToInt();
                        ApplicationSettings.RemoveBatchesOverMonths = setting.Value10.ToInt();
                        ApplicationSettings.RemoveBatches = setting.Value11.ToBool();
                        ApplicationSettings.CheckSpecialPrices = setting.Value12.ToBool();
                        ApplicationSettings.DateTemplateId = setting.Value13.ToNullableInt();
                        ApplicationSettings.TraceabilityTemplateId = setting.Value14.ToNullableInt();
                        ApplicationSettings.AttributeTemplateId = setting.Value15.ToNullableInt();
                        ApplicationSettings.VatCodeId = setting.Value16.ToNullableInt();
                        ApplicationSettings.ProductGroupId = setting.Value17.ToInt();
                        //ApplicationSettings.CheckForUnifyOrder = setting.Value18.ToBool();
                        ApplicationSettings.CheckForUnifyOrderTime = setting.Value19.ToInt() == 0 ? 10 : setting.Value19.ToInt();
                        ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses = setting.Value20;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_Job_ReportProcessing2"))
                    {
                        ApplicationSettings.PrintPalletLabel = setting.Value1.ToBool();
                        ApplicationSettings.PrintPalletLabelCheck = setting.Value2.ToInt();
                        ApplicationSettings.PrinterIP = setting.Value3 != null ? setting.Value3.Trim() : string.Empty;
                        ApplicationSettings.UpdateProducts = setting.Value4.ToBool();
                        if (setting.Value5.ToInt() == 0)
                        {
                            ApplicationSettings.PrinterDPI = 203;
                        }
                        else
                        {
                            ApplicationSettings.PrinterDPI = setting.Value5.ToInt();
                        }

                        ApplicationSettings.PalletLabelsToPrint = setting.Value6.ToInt() > 0 ? setting.Value6.ToInt() : 1;
                        ApplicationSettings.ReprintMoveLabels = setting.Value7.ToBool();
                        ApplicationSettings.CheckForIntakeImport = setting.Value8.ToBool();
                        ApplicationSettings.CheckForIntakeImportTime = setting.Value9.ToInt();
                        ApplicationSettings.CheckForIntakeImportPath = setting.Value10;
                        ApplicationSettings.CheckForIntakeImportWarehouse = setting.Value11.ToInt();
                        ApplicationSettings.CheckForIntakeImportProduct = setting.Value12.ToInt();
                        ApplicationSettings.CheckForIntakeImportCountryOfOrigin = string.IsNullOrEmpty(setting.Value13) ? "Ireland" : setting.Value13;
                        ApplicationSettings.RunDBJob = setting.Value14.ToBool();
                        ApplicationSettings.RunDBJobRunTime = setting.Value15.ToInt() == 0 ? 1000 : setting.Value15.ToInt();
                        ApplicationSettings.RunDBJob2 = setting.Value16.ToBool();
                        ApplicationSettings.RunDBJobRunTime2 = setting.Value17.ToInt() == 0 ? 1000 : setting.Value17.ToInt();
                        ApplicationSettings.CheckForLairageImport = setting.Value18.ToBool();
                        ApplicationSettings.NouvemEmailAddresses = string.IsNullOrEmpty(setting.Value19) ? "brianmurray@nouvem,olliehayden@nouvem.com,aidanvallely@nouvem.com" : setting.Value19;
                        ApplicationSettings.Customer = string.IsNullOrEmpty(setting.Value20) ? "" : setting.Value20;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_Job_ReportProcessing3"))
                    {
                        ApplicationSettings.NouvemEmailAddress = string.IsNullOrEmpty(setting.Value1) ? "nouvem.email@gmail.com" : setting.Value1;
                        ApplicationSettings.NouvemEmailPassword = string.IsNullOrEmpty(setting.Value2) ? "Nouv1234" : setting.Value2;
                        ApplicationSettings.ProductIntake = setting.Value3.ToInt();
                        ApplicationSettings.CheckForDispatchExport = setting.Value4.ToBool();
                        ApplicationSettings.DispatchExportFilePath = setting.Value5;
                        ApplicationSettings.DispatchExportTimeMins = setting.Value6.ToInt();
                        ApplicationSettings.DispatchExportEmailAddress = setting.Value7;
                        ApplicationSettings.DispatchExportHindID = setting.Value8.ToInt();
                        ApplicationSettings.DispatchExportForeID = setting.Value9.ToInt();

                        ApplicationSettings.ExportCarcassPrices = setting.Value10.ToBool();
                        ApplicationSettings.ExportCarcassPricesTime = setting.Value11.ToInt();
                        ApplicationSettings.ExportCarcassPricesTimeMins = setting.Value12.ToInt();
                        ApplicationSettings.ExportCarcassPricesPath = setting.Value13;
                        ApplicationSettings.ExportCarcassPricesEmail = setting.Value14;

                        ApplicationSettings.ImportCarcassPrices = setting.Value15.ToBool();
                        ApplicationSettings.ImportCarcassPricesTime = setting.Value16.ToInt();
                        ApplicationSettings.ImportCarcassPricesPath = setting.Value17;

                        ApplicationSettings.UpdateAveragePriceList = setting.Value18.ToBool();
                        ApplicationSettings.UpdateAveragePriceListTime = setting.Value19.ToInt();
                        ApplicationSettings.EmailPort = setting.Value20.ToInt() == 0 ? 587 : setting.Value20.ToInt();
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("KillLine"))
                    {
                        ApplicationSettings.ProductIdYoungBull = setting.Value6.ToInt();
                        ApplicationSettings.ProductIdBull = setting.Value7.ToInt();
                        ApplicationSettings.ProductIdSteer = setting.Value8.ToInt();
                        ApplicationSettings.ProductIdCow = setting.Value9.ToInt();
                        ApplicationSettings.ProductIdHeifer = setting.Value10.ToInt();
                        ApplicationSettings.ProductIdVealYoung = setting.Value11.ToInt();
                        ApplicationSettings.ProductIdVealOld = setting.Value12.ToInt();
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_Job_BroadcastDBChanges"))
                    {
                        Settings.Default.BroadcastDBChanges = setting.Value1.ToBool();
                        Settings.Default.BroadcastDBChangesInterval = setting.Value2.ToInt();
                        ApplicationSettings.BackUpDatabase2 = setting.Value3.ToBool();
                        ApplicationSettings.BackUpDatabaseTime2 = setting.Value4.ToInt();
                        ApplicationSettings.BackUpDatabaseTimeMins2 = setting.Value5.ToInt();
                        ApplicationSettings.BackUpDatabase3 = setting.Value6.ToBool();
                        ApplicationSettings.BackUpDatabaseTime3 = setting.Value7.ToInt();
                        ApplicationSettings.BackUpDatabaseTimeMins3 = setting.Value8.ToInt();
                        ApplicationSettings.BackUpDatabase4 = setting.Value9.ToBool();
                        ApplicationSettings.BackUpDatabaseTime4 = setting.Value10.ToInt();
                        ApplicationSettings.BackUpDatabaseTimeMins4 = setting.Value11.ToInt();
                        ApplicationSettings.CheckForLairageExport = setting.Value12.ToBool();
                        ApplicationSettings.CheckForLairageExportPath = setting.Value13;
                        ApplicationSettings.CheckForLairageExportTime = setting.Value14.ToInt();
                        ApplicationSettings.CheckForLairageExportCustomer1 = setting.Value15 ?? string.Empty;
                        ApplicationSettings.CheckForLairageExportCustomer2 = setting.Value16 ?? string.Empty;
                        ApplicationSettings.CheckForLairageExportCustomer3 = setting.Value17 ?? string.Empty;
                        ApplicationSettings.CheckForImportLairage = setting.Value18.ToBool();
                        ApplicationSettings.CheckForImportLairagePath = setting.Value19;
                        ApplicationSettings.CheckForImportLairageTime = setting.Value20.ToInt();
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_DispatchReport"))
                    {
                        Settings.Default.DispatchReportPrinter1 = setting.Value1;
                        Settings.Default.DispatchReportPrinter2 = setting.Value2;
                        Settings.Default.DispatchReportPrinter3 = setting.Value3;
                        Settings.Default.DispatchReportPrinter4 = setting.Value4;
                        Settings.Default.DispatchReportPrinterCopies = setting.Value5.ToInt();
                        Settings.Default.DispatchReportConnect = setting.Value6.ToBool();
                        ApplicationSettings.CheckForLairageExportSleep = setting.Value7.ToInt();
                        ApplicationSettings.LairageExportTime = setting.Value8.ToInt() == 0 ? 16 : setting.Value8.ToInt();
                        ApplicationSettings.LairageExportTimeMins = setting.Value9.ToInt() == 0 ? 30 : setting.Value9.ToInt(); 
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_DispatchSummaryReport"))
                    {
                        Settings.Default.DispatchSummaryReportPrinter1 = setting.Value1;
                        Settings.Default.DispatchSummaryReportPrinter2 = setting.Value2;
                        Settings.Default.DispatchSummaryReportPrinter3 = setting.Value3;
                        Settings.Default.DispatchSummaryReportPrinter4 = setting.Value4;
                        Settings.Default.DispatchSummaryReportPrinterCopies = setting.Value5.ToInt();
                        Settings.Default.DispatchSummaryReportConnect = setting.Value6.ToBool();
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("Scheduler_SaleOrderReport"))
                    {
                        Settings.Default.SaleOrderReportPrinter1 = setting.Value1;
                        Settings.Default.SaleOrderReportPrinter2 = setting.Value2;
                        Settings.Default.SaleOrderReportPrinter3 = setting.Value3;
                        Settings.Default.SaleOrderReportPrinter4 = setting.Value4;
                        Settings.Default.SaleOrderReportPrinterCopies = setting.Value5.ToInt();
                        Settings.Default.SaleOrderReportConnect = setting.Value6.ToBool();
                        ApplicationSettings.SaleOrderName =
                            string.IsNullOrEmpty(setting.Value7) ? "Sale Order" : setting.Value7;
                        ApplicationSettings.SaleOrderParameter =
                            string.IsNullOrEmpty(setting.Value8) ? "OrderID" : setting.Value8;
                        continue;
                    }

                    if (setting.Name.CompareIgnoringCase("AccountsPackage"))
                    {
                        ApplicationSettings.AccountsPackage = setting.Value1;
                        ApplicationSettings.AccountsDirectory = setting.Value9;
                        ApplicationSettings.AccountsConnection = setting.Value10;
                        continue;
                    }

                    #endregion
                }

                this.GetEmailJobData();
            }

            private void GetEmailJobData()
            {
                var jobs = Data.GetEmailReportJobs();
                var count = 1;
                foreach (var job in jobs)
                {
                    if (count == 1)
                    {
                        ApplicationSettings.EmailReportJob1Run = true;
                        ApplicationSettings.EmailReportJob1ReportName = job.ReportName;
                        ApplicationSettings.EmailReportJob1Time = job.StartTimeHour.ToInt();
                        ApplicationSettings.EmailReportJob1TimeMins = job.StartTimeMin.ToInt();
                        ApplicationSettings.EmailReportJob1RunTime = job.LoopTimeInMilliseconds.ToInt();
                        ApplicationSettings.EmailReportJob1Param1= job.Param1;
                        ApplicationSettings.EmailReportJob1Param2 = job.Param2;
                        ApplicationSettings.EmailReportJob1Param3 = job.Param3;
                        ApplicationSettings.EmailReportJob1Param4 = job.Param4;
                        ApplicationSettings.EmailReportJob1Param5 = job.Param5;
                        ApplicationSettings.EmailReportJob1Param6 = job.Param6;
                        ApplicationSettings.EmailReportJob1Path = job.ReportPath;
                    }
                    else
                    {
                        ApplicationSettings.EmailReportJob2Run = true;
                        ApplicationSettings.EmailReportJob2ReportName = job.ReportName;
                        ApplicationSettings.EmailReportJob2Time = job.StartTimeHour.ToInt();
                        ApplicationSettings.EmailReportJob2TimeMins = job.StartTimeMin.ToInt();
                        ApplicationSettings.EmailReportJob2RunTime = job.LoopTimeInMilliseconds.ToInt();
                        ApplicationSettings.EmailReportJob2Param1 = job.Param1;
                        ApplicationSettings.EmailReportJob2Param2 = job.Param2;
                        ApplicationSettings.EmailReportJob2Param3 = job.Param3;
                        ApplicationSettings.EmailReportJob2Param4 = job.Param4;
                        ApplicationSettings.EmailReportJob2Param5 = job.Param5;
                        ApplicationSettings.EmailReportJob2Param6 = job.Param6;
                        ApplicationSettings.EmailReportJob2Path = job.ReportPath;
                    }

                    count++;
                }
            }

            private void ScheduleJobs()            
            {
                //this.job.RunAutoSaleOrderProcessor();
                //this.BackUpApplication();
                //this.CheckForDatabaseChanges();

                //this.SendPaymentJob();

                if (ApplicationSettings.EmailReportJob1Run)
                {
                    this.EmailReportJob1();
                }

                if (ApplicationSettings.EmailReportJob2Run)
                {
                    this.EmailReportJob2();
                }

                if (ApplicationSettings.CheckForUnifyOrder)
                {
                    this.CheckForUnifyOrders();
                }

                if (ApplicationSettings.RunDBJob)
                {
                    this.DBJob();
                }

                if (ApplicationSettings.RunDBJob2)
                {
                    this.DBJob2();
                }

                if (ApplicationSettings.CheckSpecialPrices)
                {
                    this.CheckSpecialPrices();
                }

                if (ApplicationSettings.CheckForLairageImport)
                {
                    this.CheckForLairageImport();
                }

                if (ApplicationSettings.CheckForImportLairage)
                {
                    this.CheckForImportLairage();
                }

                if (ApplicationSettings.CheckForLairageExport)
                {
                    this.CheckForExportLairage();
                }

                if (ApplicationSettings.ImportCarcassPrices)
                {
                    this.CheckForCarcassPricesImport();
                }

                if (ApplicationSettings.ExportCarcassPrices)
                {
                    this.CheckForCarcassPricesExport();
                }

                if (ApplicationSettings.CheckForDispatchExport)
                {
                    this.CheckForExportDispatch();
                }

                if (ApplicationSettings.UpdateAveragePriceList)
                {
                    this.CheckForUpdateAveragePriceList();
                }

                if (ApplicationSettings.CheckForIntakeImport)
                {
                    this.CheckForIntakeImport();
                }

                if (ApplicationSettings.RunReportsProcessing)
                {
                    this.CheckReportsForPrinting();
                }

                if (ApplicationSettings.BroadcastDBChanges)
                {
                    this.CheckForDatabaseChanges();
                }

                if (ApplicationSettings.SyncAccountsData)
                {
                    this.CheckDataIntegration();
                }

                if (ApplicationSettings.BackUpDatabase)
                {
                    this.Backupdatabase();
                }

                if (ApplicationSettings.BackUpDatabase2)
                {
                    this.Backupdatabase2();
                }

                if (ApplicationSettings.BackUpDatabase3)
                {
                    this.Backupdatabase3();
                }

                if (ApplicationSettings.BackUpDatabase4)
                {
                    this.Backupdatabase4();
                }

                if (ApplicationSettings.RemoveBatches)
                {
                    this.RemoveBatches();
                }

                if (ApplicationSettings.PrintPalletLabel)
                {
                    this.PrintPalletLabel();
                }

                if (ApplicationSettings.PrintPalletLabelData)
                {
                    this.PrintPalletLabelData();
                }

                if (ApplicationSettings.ReprintMoveLabels)
                {
                    this.ReprintMoveLabels();
                }

                //job.CheckDispatchDockets();
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckDataIntegration()
            {
                this.log.LogInfo(this.GetType(), "CheckDataIntegration(): checking..");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<DataIntegration>()
                    .WithIdentity("DataIntegration")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("DataIntegrationTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInSeconds(ApplicationSettings.DataIntegrationProcessingInterval)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), 
                    string.Format("CheckDataintegration(): Registered. Set to run every {0} mins. Path:{1}, Updating products:{2}, Sage200:{3}", 
                    ApplicationSettings.DataIntegrationProcessingInterval,
                    ApplicationSettings.AccountsDirectory, 
                    ApplicationSettings.UpdateProducts,
                    ApplicationSettings.AccountsConnection));
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void PrintPalletLabel()
            {
                this.log.LogInfo(this.GetType(),
                    string.Format("PrintPalletLabel(): Start... Set to run every {0} secs. IP:{1}",
                        ApplicationSettings.PrintPalletLabelCheck,
                        ApplicationSettings.PrinterIP));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<PrintLabel>()
                    .WithIdentity("PrintLabel")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("PrintLabelTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInSeconds(ApplicationSettings.PrintPalletLabelCheck)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(),
                    string.Format("PrintPalletLabel(): Registered. Set to run every {0} secs. IP:{1}",
                    ApplicationSettings.PrintPalletLabelCheck,
                    ApplicationSettings.PrinterIP));
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void PrintPalletLabelData()
            {
                this.log.LogInfo(this.GetType(),
                    string.Format("PrintPalletLabelData(): Start... Set to run every {0} secs. IP:{1}, IP2:{2},Printer2Device:{3}",
                        ApplicationSettings.PrintPalletLabelDataCheck,
                        ApplicationSettings.PrinterPalletIP,
                        ApplicationSettings.PrinterPalletIP2,
                        ApplicationSettings.PrinterPallet2Device));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<Jobs.Handler.PrintPalletLabel>()
                    .WithIdentity("PrintPalletLabel")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("PrintPalletLabelTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInSeconds(ApplicationSettings.PrintPalletLabelDataCheck)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "PrintPalletLabelData(): Registered.");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void ReprintMoveLabels()
            {
                this.log.LogInfo(this.GetType(),
                    string.Format("ReprintMoveLabels(): Start... Set to run every {0} secs. IP:{1}",
                        ApplicationSettings.PrintPalletLabelCheck,
                        ApplicationSettings.PrinterIP));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<ReprintLabel>()
                    .WithIdentity("ReprintLabel")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("ReprintLabelTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(ApplicationSettings.PrintPalletLabelCheck)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(),
                    string.Format("ReprintPalletLabel(): Registered. Set to run every {0} secs. IP:{1}",
                        ApplicationSettings.PrintPalletLabelCheck,
                        ApplicationSettings.PrinterIP));
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void SendPaymentJob()
            {
                
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<SendPayment>()
                    .WithIdentity("SendPaymentJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("SendPaymentJobTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(20000)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
            }

            /// <summary>
            /// Emails scheduled report.
            /// </summary>
            public void EmailReportJob1()
            {
                this.log.LogInfo(this.GetType(),
                    string.Format("EmailJob(): Start... Run time:{0}, Start time {1}:{2}.",
                        ApplicationSettings.EmailReportJob1RunTime, ApplicationSettings.EmailReportJob1Time, ApplicationSettings.EmailReportJob1TimeMins));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<EmailJob>()
                    .WithIdentity("EmailJob")
                    .Build();

                ITrigger trigger;

                if (ApplicationSettings.EmailReportJob1RunTime > 0)
                {
                    trigger = TriggerBuilder.Create()
                        .WithIdentity("EmailJobTrigger")
                        .StartNow()
                        .WithSimpleSchedule(x => x
                            .WithIntervalInSeconds(ApplicationSettings.EmailReportJob1RunTime)
                            .RepeatForever())
                        .Build();
                }
                else
                {
                    trigger = TriggerBuilder.Create()
                        .WithIdentity("EmailJobTrigger")
                        .StartNow()
                        .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.EmailReportJob1Time, ApplicationSettings.EmailReportJob1TimeMins))
                        .ForJob("EmailJob")
                        .Build();
                }

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(),
                    "EmailJob(): Registered. Set to run");
            }

            /// <summary>
            /// Emails scheduled report.
            /// </summary>
            public void EmailReportJob2()
            {
                this.log.LogInfo(this.GetType(),
                    string.Format("EmailJob2(): Start... Run time:{0}, Start time {1}:{2}.",
                        ApplicationSettings.EmailReportJob2RunTime, ApplicationSettings.EmailReportJob2Time, ApplicationSettings.EmailReportJob2TimeMins));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<EmailJob2>()
                    .WithIdentity("EmailJob2")
                    .Build();

                ITrigger trigger;

                if (ApplicationSettings.EmailReportJob2RunTime > 0)
                {
                    trigger = TriggerBuilder.Create()
                        .WithIdentity("EmailJob2Trigger")
                        .StartNow()
                        .WithSimpleSchedule(x => x
                            .WithIntervalInSeconds(ApplicationSettings.EmailReportJob2RunTime)
                            .RepeatForever())
                        .Build();
                }
                else
                {
                    trigger = TriggerBuilder.Create()
                        .WithIdentity("EmailJob2Trigger")
                        .StartNow()
                        .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.EmailReportJob2Time, ApplicationSettings.EmailReportJob2TimeMins))
                        .ForJob("EmailJob2")
                        .Build();
                }

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(),
                    "EmailJob2(): Registered. Set to run");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void DBJob()
            {
                this.log.LogInfo(this.GetType(),
                    string.Format("DBJob(): Start... Set to run every {0} secs.",
                        ApplicationSettings.RunDBJobRunTime));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<DBJob>()
                    .WithIdentity("DBJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("DBJobTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(ApplicationSettings.RunDBJobRunTime)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(),
                    string.Format("DBJob(): Registered. Set to run every {0} secs.",
                        ApplicationSettings.RunDBJobRunTime));
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void DBJob2()
            {
                this.log.LogInfo(this.GetType(),
                    string.Format("DBJob2(): Start... Set to run every {0} secs.",
                        ApplicationSettings.RunDBJobRunTime2));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<DBJob2>()
                    .WithIdentity("DBJob2")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("DBJob2Trigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(ApplicationSettings.RunDBJobRunTime2)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(),
                    string.Format("DBJob2(): Registered. Set to run every {0} secs.",
                        ApplicationSettings.RunDBJobRunTime2));
            }

            /// <summary>
            /// Marks old batches as inactive.
            /// </summary>
            public void RemoveBatches()
            {
                this.log.LogInfo(this.GetType(), string.Format("RemoveBatchesing..Start time:{0}:{1}", 
                    ApplicationSettings.RemoveBatchesTime, ApplicationSettings.RemoveBatchesTimeMins));

                var schedFact = new StdSchedulerFactory();
                var sched = schedFact.GetScheduler();
                sched.Start();

                var dbJob = JobBuilder.Create<RemoveOldBatches>()
                    .WithIdentity("RemoveBatchesJob")
                    .Build();

                var dbTrigger = TriggerBuilder.Create()
                  .WithIdentity("RemoveBatchesTrigger")
                  .StartNow()
                  .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.RemoveBatchesTime, ApplicationSettings.RemoveBatchesTimeMins))
                  .ForJob("RemoveBatchesJob")
                  .Build();

                sched.ScheduleJob(dbJob, dbTrigger);
                this.log.LogInfo(this.GetType(), "RemoveBatches(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void Backupdatabase()
            {
                this.log.LogInfo(this.GetType(), string.Format("Backupdatabase(): Registering..Start time:{0}:{1}", ApplicationSettings.BackUpDatabaseTime, ApplicationSettings.BackUpDatabaseTimeMins));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var dbJob = JobBuilder.Create<BackUpDatabase>()
                    .WithIdentity("BackUpDatabaseJob")
                    .Build();

                var dbTrigger = TriggerBuilder.Create()
                    .WithIdentity("BackUpDatabaseTrigger")
                    .StartNow()
                    .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.BackUpDatabaseTime, ApplicationSettings.BackUpDatabaseTimeMins))
                    .ForJob("BackUpDatabaseJob")
                    .Build();

                sched.ScheduleJob(dbJob, dbTrigger);
                this.log.LogInfo(this.GetType(), "Backupdatabase(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void Backupdatabase2()
            {
                this.log.LogInfo(this.GetType(), string.Format("Backupdatabase2(): Registering..Start time:{0}:{1}",ApplicationSettings.BackUpDatabaseTime2, ApplicationSettings.BackUpDatabaseTimeMins2));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var dbJob = JobBuilder.Create<BackUpDatabase>()
                    .WithIdentity("BackUpDatabaseJob2")
                    .Build();

                var dbTrigger = TriggerBuilder.Create()
                  .WithIdentity("BackUpDatabaseTrigger2")
                  .StartNow()
                  .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.BackUpDatabaseTime2, ApplicationSettings.BackUpDatabaseTimeMins2))
                  .ForJob("BackUpDatabaseJob2")
                  .Build();

                sched.ScheduleJob(dbJob, dbTrigger);
                this.log.LogInfo(this.GetType(), "Backupdatabase2(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void Backupdatabase3()
            {
                this.log.LogInfo(this.GetType(), string.Format("Backupdatabase3(): Registering..Start time:{0}:{1}", ApplicationSettings.BackUpDatabaseTime3, ApplicationSettings.BackUpDatabaseTimeMins3));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var dbJob = JobBuilder.Create<BackUpDatabase>()
                    .WithIdentity("BackUpDatabaseJob3")
                    .Build();

                var dbTrigger = TriggerBuilder.Create()
                    .WithIdentity("BackUpDatabaseTrigger3")
                    .StartNow()
                    .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.BackUpDatabaseTime3, ApplicationSettings.BackUpDatabaseTimeMins3))
                    .ForJob("BackUpDatabaseJob3")
                    .Build();

                sched.ScheduleJob(dbJob, dbTrigger);
                this.log.LogInfo(this.GetType(), "Backupdatabase3(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void Backupdatabase4()
            {
                this.log.LogInfo(this.GetType(), string.Format("Backupdatabase4(): Registering..Start time:{0}:{1}", ApplicationSettings.BackUpDatabaseTime4, ApplicationSettings.BackUpDatabaseTimeMins4));
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var dbJob = JobBuilder.Create<BackUpDatabase>()
                    .WithIdentity("BackUpDatabaseJob4")
                    .Build();

                var dbTrigger = TriggerBuilder.Create()
                    .WithIdentity("BackUpDatabaseTrigger4")
                    .StartNow()
                    .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.BackUpDatabaseTime4, ApplicationSettings.BackUpDatabaseTimeMins4))
                    .ForJob("BackUpDatabaseJob4")
                    .Build();

                sched.ScheduleJob(dbJob, dbTrigger);
                this.log.LogInfo(this.GetType(), "Backupdatabase4(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckReportsForPrinting()
            {
                this.log.LogInfo(this.GetType(), $"CheckReportsForPrinting(): Registering..Running every {Settings.Default.ReportsProcessingInterval} seconds, Sale Order Name:{ApplicationSettings.SaleOrderName}, " +
                                                 $"Sale Order Param:{ApplicationSettings.SaleOrderParameter}, printer:{ApplicationSettings.SaleOrderReportPrinter1}");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<ReportProcessing>()
                    .WithIdentity("CheckReportsForPrintingJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("CheckReportsForPrintingTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInSeconds(ApplicationSettings.ReportsProcessingInterval)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckReportsForPrinting(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForExportLairage()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForExportLairage(): Registering..Start time:{0}:{1}", 
                    ApplicationSettings.LairageExportTime, ApplicationSettings.LairageExportTimeMins));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<ExportLairage>()
                    .WithIdentity("CheckForLairageExportJob")
                    .Build();

                ITrigger trigger;

#if DEBUG
                trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForLairageExportTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(ApplicationSettings.CheckForLairageExportTime)
                        .RepeatForever())
                    .Build();
#endif

#if !DEBUG

                    trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForLairageExportTrigger")
                    .StartNow()
                    .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.LairageExportTime, ApplicationSettings.LairageExportTimeMins))
                    .ForJob("CheckForLairageExportJob")
                    .Build();
#endif

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForLairageExport(): Registered");

            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForCarcassPricesExport()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForCarcassPricesExport(): Registering..Start time:{0}:{1}, Path:{2}, Email:{3}",
                    ApplicationSettings.ExportCarcassPricesTime, ApplicationSettings.ExportCarcassPricesTimeMins, 
                    ApplicationSettings.ExportCarcassPricesPath,ApplicationSettings.ExportCarcassPricesEmail));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<ExportCarcassPrices>()
                    .WithIdentity("CheckForCarcassPricesExport")
                    .Build();

                ITrigger trigger;

#if DEBUG
                trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForCarcassPricesExportTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(10)
                        .RepeatForever())
                    .Build();
#endif

#if !DEBUG

                    trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForCarcassPricesExportTrigger")
                    .StartNow()
                    .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(ApplicationSettings.ExportCarcassPricesTime, ApplicationSettings.ExportCarcassPricesTimeMins))
                    .ForJob("CheckForCarcassPricesExportJob")
                    .Build();
#endif

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForCarcassPricesExport(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForCarcassPricesImport()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForCarcassPricesImport(): Registering..Running every:{0} mins, Path:{1}",
                    ApplicationSettings.ImportCarcassPricesTime,ApplicationSettings.ImportCarcassPricesPath));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<ImportCarcassPrices>()
                    .WithIdentity("CheckForCarcassPricesImport")
                    .Build();
        
                var trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForCarcassPricesImportTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(ApplicationSettings.ImportCarcassPricesTime)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForCarcassPricesImport(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForExportDispatch()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForExportDispatch(): Registering..Running every:{0} mins, Path:{1}",
                    ApplicationSettings.DispatchExportTimeMins, ApplicationSettings.DispatchExportFilePath));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<ExportDispatches>()
                    .WithIdentity("CheckForDispatchExportJob")
                    .Build();
         
                var trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForDispatchExportTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(ApplicationSettings.DispatchExportTimeMins)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForDispatchExport(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForUpdateAveragePriceList()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForUpdateAveragePriceList(): Registering..Running every:{0} mins",
                    ApplicationSettings.UpdateAveragePriceListTime));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<UpdateAveragePriceList>()
                    .WithIdentity("CheckForUpdateAveragePriceListJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForUpdateAveragePriceListTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(ApplicationSettings.UpdateAveragePriceListTime)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForUpdateAveragePriceList(): Registered");

            }


            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForIntakeImport()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForIntakeImport(): Registering..Running every:{0} minutes. Path:{1}.  Product:{2}.  Warehouse:{3}",
                    ApplicationSettings.CheckForIntakeImportTime, 
                    ApplicationSettings.CheckForIntakeImportPath,
                    ApplicationSettings.CheckForIntakeImportProduct,
                    ApplicationSettings.CheckForIntakeImportWarehouse
                    ));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<IntakeImport>()
                    .WithIdentity("CheckForIntakeImportJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForIntakeImportTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(ApplicationSettings.CheckForIntakeImportTime)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForIntakeImport(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForUnifyOrders()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForUnifyOrder(): Registering..Running every:{0} minutes. Email issue addresses:{1}.",
                    ApplicationSettings.CheckForUnifyOrderTime,
                    ApplicationSettings.CheckForUnifyOrderIssueEmailAddresses
                ));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<UnifyOrdering>()
                    .WithIdentity("CheckForUnifyOrderingJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForUnifyOrderingTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInMinutes(ApplicationSettings.CheckForUnifyOrderTime)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForUnifyOrders(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForImportLairage()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForImportLairage(): Registering..Running every:{0} seconds. Path:{1}.",
                    ApplicationSettings.CheckForImportLairageTime,
                    ApplicationSettings.CheckForImportLairagePath
                ));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<ImportLairage>()
                    .WithIdentity("CheckForImportLairageJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForImportLairageTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(ApplicationSettings.CheckForImportLairageTime)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForImportLairage(): Registered");
            }


            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForLairageImport()
            {
                this.log.LogInfo(this.GetType(), string.Format("CheckForLairageImport(): Registering..Running every:{0} seconds. Path:{1}.",
                    ApplicationSettings.CheckForIntakeImportTime,
                    ApplicationSettings.CheckForIntakeImportPath
                ));

                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<LairageImport>()
                    .WithIdentity("CheckForLairageImportJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity("CheckForLairageImportTrigger")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(ApplicationSettings.CheckForIntakeImportTime)
                        .RepeatForever())
                    .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForLairageImport(): Registered");
            }

            /// <summary>
            /// Prints any unprinted, complete dispatch dockets..
            /// </summary>
            public void CheckForDatabaseChanges()
            {
                this.log.LogInfo(this.GetType(), "CheckForDatabaseChanges(): Registering..");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<Broadcaster>()
                    .WithIdentity("CheckForDatabaseChanges")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("CheckForDatabaseChangesTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInMinutes(ApplicationSettings.BroadcastDBChangesInterval)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckForDatabaseChanges(): Registered");
            }

            /// <summary>
            /// Runs the auto sale order processor.
            /// </summary>
            public void RunAutoSaleOrderProcessor()
            {
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<AutoSaleOrderProcessor>()
                    .WithIdentity("RunAutoSaleOrderProcessor")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("RunAutoSaleOrderProcessorTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInMinutes(100)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
            }


            /// <summary>
            /// Checks for future prices to be implemented.
            /// </summary>
            public void BackUpApplication()
            {
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var specialPricesJob = JobBuilder.Create<BackUpApplication>()
                    .WithIdentity("BackUpApplicationJob")
                    .Build();

                //var specialPricesTrigger = TriggerBuilder.Create()
                //  .WithIdentity("BackUpApplicationTrigger")
                //  .StartNow()
                //  .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(21, 00))
                //  .ForJob("BackUpApplicationJob")
                //  .Build();

                var specialPricesTrigger = TriggerBuilder.Create()
                  .WithIdentity("BackUpApplicationTrigger")
                  .StartNow()
                  .WithSimpleSchedule(x => x
                      .WithIntervalInMinutes(100)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(specialPricesJob, specialPricesTrigger);
            }

            /// <summary>
            /// Checks for future prices to be implemented.
            /// </summary>
            public void CheckSpecialPrices()
            {
                this.log.LogInfo(this.GetType(), "CheckSpecialPrices()...registering - start time 2.00");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var specialPricesJob = JobBuilder.Create<SpecialPrices>()
                    .WithIdentity("SpecialPricesJob")
                    .Build();

                var specialPricesTrigger = TriggerBuilder.Create()
                  .WithIdentity("SpecialPricesTrigger")
                  .StartNow()
                  .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(2, 00))
                  .ForJob("SpecialPricesJob")
                  .Build();

                //var specialPricesTrigger = TriggerBuilder.Create()
                //  .WithIdentity("SpecialPricesTrigger")
                //  .StartNow()
                //  .WithSimpleSchedule(x => x
                //      .WithIntervalInMinutes(100)
                //      .RepeatForever())
                //  .Build();

                sched.ScheduleJob(specialPricesJob, specialPricesTrigger);
                this.log.LogInfo(this.GetType(), "CheckSpecialPrices(): Registered");
            }

            /// <summary>
            /// Check the dispatch dockets line/header totals, ensuring they tally.
            /// </summary>
            public void CheckDispatchDockets()
            {
                this.log.LogInfo(this.GetType(), "SchedulerStart(): Registering..");
                var schedFact = new StdSchedulerFactory();

                var sched = schedFact.GetScheduler();
                sched.Start();

                var job = JobBuilder.Create<DispatchDocketTally>()
                    .WithIdentity("DispatchDocketJob")
                    .Build();

                var trigger = TriggerBuilder.Create()
                  .WithIdentity("DispatchDocketTrigger")
                  .WithSimpleSchedule(x => x
                      .WithIntervalInMinutes(1)
                      .RepeatForever())
                  .Build();

                sched.ScheduleJob(job, trigger);
                this.log.LogInfo(this.GetType(), "CheckReportsForPrinting(): Registered");
            }
        }
    }
}
