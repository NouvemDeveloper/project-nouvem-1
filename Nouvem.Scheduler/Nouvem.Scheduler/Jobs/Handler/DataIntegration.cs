﻿// -----------------------------------------------------------------------
// <copyright file="Broadcaster.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Win32;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Shared;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using Nouvem.Scheduler.Global;
    using Quartz;
    using Nouvem.AccountsIntegration;

    public class DataIntegration : HandlerBase, IJob
    {
        private IAccountsIntegration accounts;

        private const string REG_PATH = @"Software\Sage\MMS";
        private const string REGKEY_VALUE = "ClientInstallLocation";
        private const string DEFAULT_ASSEMBLY = "Sage.Common.dll";
        private const string ASSEMBLY_RESOLVER = "Sage.Common.Utilities.AssemblyResolver";
        private const string RESOLVER_METHOD = "GetResolver";

        public DataIntegration()
        {
            this.SetUpAccounts();
            if (SchedulerGlobal.BPGroups == null)
            {
                SchedulerGlobal.BPGroups = Data.GetPartnerGroups();
            }
        }

        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), "Checking data integration..");
            this.GetPrices();
            this.GetPartners();
            //this.GetProducts();
        }

        private void SetUpAccounts()
        {
            try
            {
                if (ApplicationSettings.AccountsPackage.CompareIgnoringCase("Sage200"))
                {
                    this.FindSage200Assemblies();
                    this.accounts = new Sage200{ConnectionString = ApplicationSettings.AccountsConnection};
                }
                else
                {
                    this.accounts = new Exchequer();
                }
            }
            catch (Exception e)
            {
                this.log.LogError(this.GetType(), e.Message);
            }
        }

        /// <summary>
        /// Gets the newly added partners, and adds to the db.
        /// </summary>
        private void GetPartners()
        {
            if (SchedulerGlobal.PartnersProcessing)
            {
                return;
            }

            SchedulerGlobal.PartnersProcessing = true;

            try
            {
                //this.accounts.Connect("", "", "");
                var data = this.accounts.GetPartners(ApplicationSettings.AccountsDirectory);
                var partners = data.Item1;
                var error = data.Item2;

                if (error != string.Empty)
                {
                    this.log.LogError(this.GetType(), string.Format("Scheduler: GetPartners(): {0}", error));
                }

                var partnerToAdd = new List<BusinessPartner>();
                this.log.LogInfo(this.GetType(), string.Format("{0} partners found to process", partners.Count));

                foreach (var partner in partners)
                {
                    try
                    {
                        var groupId = 0;
                        if (SchedulerGlobal.BPGroups != null)
                        {
                            groupId = SchedulerGlobal.BPGroups.First().BPGroupID;
                        }

                        var bpPartner = new BusinessPartner();
                        var email = string.Empty;
                        if (partner.Email != null && partner.Email.Length <= 50)
                        {
                            email = partner.Email;
                        }

                        bpPartner.BpMaster = new BPMaster
                        {
                            NouBPTypeID = 1,
                            Code = partner.Code,
                            Name = partner.Name,
                            BPGroupID = groupId,
                            Tel = partner.Phone,
                            FAX = partner.Fax,
                            Email = email,
                            Notes = partner.CustomerNotes,
                            CreditLimit = partner.CreditLimit,
                            Balance = partner.AccountBalance,
                            CreationDate = DateTime.Now,
                            EditDate = DateTime.Now
                        };

                        bpPartner.PriceListName = partner.PriceBand;

                        if (partner.AccountStatus == 1)
                        {
                            bpPartner.BpMaster.OnHold = false;
                            //bpPartner.BpMaster.InActiveFrom = null;
                            //bpPartner.BpMaster.InActiveTo = null;
                        }
                        else if (partner.AccountStatus == 2 || partner.AccountStatus == 0)
                        {
                            bpPartner.BpMaster.OnHold = true;
                        }
                        else if (partner.AccountStatus == 3)
                        {
                            bpPartner.BpMaster.InActiveFrom = DateTime.Today;
                            bpPartner.BpMaster.InActiveTo = DateTime.Today.Date.AddYears(100);
                        }

                        if (!string.IsNullOrEmpty(partner.ContactName))
                        {
                            bpPartner.Contact = new BPContact
                            {
                                FirstName = partner.ContactName,
                                Phone = string.Empty,
                                Mobile = string.Empty,
                                Email = string.Empty,
                                Deleted = false
                            };
                        }

                        if (partner.Currency.StartsWithIgnoringCase("UK") || partner.Currency.StartsWithIgnoringCase("GB") || partner.Currency.StartsWithIgnoringCase("£"))
                        {
                            bpPartner.BpMaster.BPCurrencyID = 3;
                        }
                        else
                        {
                            bpPartner.BpMaster.BPCurrencyID = 2;
                        }

                        var addresses = new List<BPAddress>();
                        var localAddress = partner.AddressLine1.Split(new[] { "<CR>" }, StringSplitOptions.None);
                        if (localAddress.Any())
                        {
                            var address = new BPAddress
                            {
                                AddressLine1 = localAddress.ElementAt(0) ?? string.Empty,
                                AddressLine2 = localAddress.Length > 1 ? localAddress.ElementAt(1) : string.Empty,
                                AddressLine3 = localAddress.Length > 2 ? localAddress?.ElementAt(2) : string.Empty,
                                AddressLine4 = localAddress.Length > 3 ? localAddress?.ElementAt(3) : string.Empty,
                                PostCode = localAddress.Length > 4 ? localAddress?.ElementAt(4) : string.Empty,
                                Billing = true
                            };

                            addresses.Add(address);
                        }

                        var localDelAddress = partner.AddressLine2.Split(new[] { "<CR>" }, StringSplitOptions.None);
                        if (localDelAddress.Any())
                        {
                            var address = new BPAddress
                            {
                                AddressLine1 = localDelAddress.ElementAt(0) ?? string.Empty,
                                AddressLine2 = localDelAddress.Length > 1 ? localDelAddress.ElementAt(1) : string.Empty,
                                AddressLine3 = localDelAddress.Length > 2 ? localDelAddress?.ElementAt(2) : string.Empty,
                                AddressLine4 = localDelAddress.Length > 3 ? localDelAddress?.ElementAt(3) : string.Empty,
                                PostCode = localDelAddress.Length > 4 ? localDelAddress?.ElementAt(4) : string.Empty,
                                Shipping = true
                            };

                            addresses.Add(address);
                        }

                        bpPartner.Addresses = addresses;

                        bpPartner.PriceList = new PriceList
                        {
                            CurrentPriceListName = bpPartner.BpMaster.Name,
                            Factor = 0,
                            NouRoundingOptionsID = 1,
                            NouRoundingRulesID = 3,
                            BPCurrencyID = bpPartner.BpMaster.BPCurrencyID != null ? bpPartner.BpMaster.BPCurrencyID.ToInt() : 3,
                            CreationDate = DateTime.Now,
                            EditDate = DateTime.Now,
                            Deleted = false,
                        };

                        partnerToAdd.Add(bpPartner);
                    }
                    catch (Exception e)
                    {
                        this.log.LogError(this.GetType(), e.Message);
                    }
                }

                if (partnerToAdd.Any())
                {
                    Data.AddOrUpdatePartners(partnerToAdd);
                }
            }
            finally
            {
                this.accounts.Logout();
                SchedulerGlobal.PartnersProcessing = false;
            }
        }

        /// <summary>
        /// Gets the newly added partners, and adds to the db.
        /// </summary>
        private void GetPrices()
        {
            if (SchedulerGlobal.PartnersProcessing)
            {
                return;
            }

            SchedulerGlobal.PartnersProcessing = true;

            try
            {
                //this.accounts.Connect("", "", "");
                var data = this.accounts.GetPrices(ApplicationSettings.AccountsDirectory);
                var prices = data.Item1;
                var error = data.Item2;

                if (error != string.Empty)
                {
                    this.log.LogError(this.GetType(), string.Format("Scheduler: GetPrices(): {0}", error));
                }

                if (prices.Any())
                {
                    Data.AddOrUpdatePrices(prices);
                }
            }
            finally
            {
                this.accounts.Logout();
                SchedulerGlobal.PartnersProcessing = false;
            }
        }

        /// <summary>
        /// Gets the newly added partners, and adds to the db.
        /// </summary>
        private void GetProducts()
        {
            var data = this.accounts.GetProducts(ApplicationSettings.AccountsDirectory);
            var products = data.Item1;
            var error = data.Item2;

            if (error != string.Empty)
            {
                this.log.LogError(this.GetType(), string.Format("Scheduler: GetProducts(): {0}", error));
            }

            var productsToAdd = new List<INMaster>();
            this.log.LogInfo(this.GetType(), string.Format("{0} products found to process", products.Count));
            foreach (var product in products)
            {
                var inMaster = new INMaster
                {
                    Code = product.Code,
                    Name = product.Name,
                    TraceabilityTemplateNameID = ApplicationSettings.TraceabilityTemplateId,
                    DateTemplateNameID = ApplicationSettings.DateTemplateId,
                    AttributeTemplateID = ApplicationSettings.AttributeTemplateId,
                    SalesItem = true,
                    PurchaseItem = true,
                    VATCodeID = ApplicationSettings.VatCodeId,
                    MinWeight = 0,
                    MaxWeight = 0,
                    StockItem = false,
                    FixedAsset = false,
                    NominalWeight = 0,
                    TypicalPieces = 0,
                    ProductionProduct = true,
                    CreationDate = DateTime.Now,
                    EditDate = DateTime.Now,
                    PrintPieceLabels = false
                };

                inMaster.INGroupID = ApplicationSettings.ProductGroupId;
                if (!string.IsNullOrEmpty(product.GroupName))
                {
                    var group = Data.GetGroup(product.GroupName);
                    if (group != null)
                    {
                        inMaster.INGroupID = group.INGroupID;
                    }
                }

                productsToAdd.Add(inMaster);
            }

            if (productsToAdd.Any())
            {
                Data.AddOrUpdateProducts(productsToAdd);
            }
        }

        /// <summary>
        /// Locates and invokes assemblies from the client folder at runtime.
        /// </summary>
        private void FindSage200Assemblies()
        {
            if (ApplicationSettings.Sage200AssembliesLoaded)
            {
                return;
            }

            // get registry info for Sage 200 server path
            var path = string.Empty;
            var root = Registry.CurrentUser;
            var key = root.OpenSubKey(REG_PATH);

            if (key != null)
            {
                object value = key.GetValue(REGKEY_VALUE);
                if (value != null)
                    path = value as string;
            }

            // refer to all installed assemblies based on location of default one
            if (string.IsNullOrEmpty(path) == false)
            {
                string commonDllAssemblyName = System.IO.Path.Combine(path, DEFAULT_ASSEMBLY);

                if (System.IO.File.Exists(commonDllAssemblyName))
                {
                    System.Reflection.Assembly defaultAssembly = System.Reflection.Assembly.LoadFrom(commonDllAssemblyName);
                    Type type = defaultAssembly.GetType(ASSEMBLY_RESOLVER);
                    MethodInfo method = type.GetMethod(RESOLVER_METHOD);
                    method.Invoke(null, null);
                }
            }

            ApplicationSettings.Sage200AssembliesLoaded = true;
        }
    }
}

