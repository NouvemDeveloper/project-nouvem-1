﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Shared;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class ImportLairage : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                string newFilename;
                var files = Directory.EnumerateFiles(ApplicationSettings.CheckForImportLairagePath).Where(x => x.EndsWithIgnoringCase("csv"));
                foreach (var file in files)
                {
                    var error = string.Empty;
                    try
                    {
                        foreach (string line in File.ReadLines(file, Encoding.UTF8))
                        {
                            var lineData = line.Split('|');
                            var apGoodsReceiptID = lineData[0].ToInt();
                            var number = lineData[3].ToInt();
                            var documentDate = lineData[10];
                            var remarks = lineData[14];
                            var killdate = lineData[34];
                            var moveResponse = lineData[35];
                            var reference = lineData[36];
                            var serial = lineData[41].ToInt();
                            var transactionDate = lineData[42];
                            var transactionQty = lineData[44].ToDecimal();
                            var transactionWeight = lineData[45].ToDecimal();
                            var tare = lineData[47].ToDecimal();
                            var inmasterId = lineData[52].ToInt();
                            var isbox = lineData[57].StringTrueOrFalseToBool().ToString();
                            var eartag = lineData[73];
                            var herd = lineData[74];
                            var dob = lineData[76];
                            var category = lineData[77].ToInt();
                            var sex = lineData[78];
                            var farmAssured = lineData[80].StringTrueOrFalseToBool().ToString();
                            var ageInMths = lineData[81].ToInt();
                            var clipped = lineData[82].StringTrueOrFalseToBool().ToString();
                            var casualty = lineData[83].StringTrueOrFalseToBool().ToString();
                            var lame = lineData[84].StringTrueOrFalseToBool().ToString();
                            var cleanliness = lineData[85].StringTrueOrFalseToBool().ToString();
                            var noOfMoves = lineData[86].ToInt();
                            var currentResidency = lineData[87].ToInt();
                            var totalResidency = lineData[88].ToInt();
                            var dateOfLastMove = lineData[90];
                            var generic1 = lineData[91];
                            var generic2 = lineData[92];
                            var killNo = lineData[94].ToInt();
                            var carcassNo = lineData[95].ToInt();
                            var sequencedDate = lineData[96];
                            var gradingDate = lineData[97];
                            var condemned = lineData[98].StringTrueOrFalseToBool().ToString();
                            var detained = lineData[99].StringTrueOrFalseToBool().ToString();
                            var notInsured= lineData[100].StringTrueOrFalseToBool().ToString();
                            var tbYes = lineData[101].StringTrueOrFalseToBool().ToString();
                            var burstBelly = lineData[102].StringTrueOrFalseToBool().ToString();
                            var fatColour = lineData[103];
                            var carcassSide = lineData[104].ToInt();
                            var coldWeight = lineData[105].ToDecimal();
                            var grade = lineData[106];
                            var side1Weight = lineData[107].ToDecimal();
                            var side2Weight = lineData[108].ToDecimal();
                            var countryOfOrigin = lineData[109];
                            var rearedIn = lineData[110];
                            var supplierId = lineData[113].ToInt();
                            var attribute220 = lineData[114];
                            var attribute230 = lineData[115];
                            var attribute231 = lineData[116];
                            var attribute232 = lineData[117];
                            var attribute233 = lineData[118];
                            var attribute234 = lineData[119];
                            var attribute235 = lineData[120];
                            var attribute236 = lineData[121];
                            var attribute237 = lineData[122];
                            var attribute238 = lineData[123];
                            var supplierName = lineData[124];
                            var supplierCode = lineData[125];
                            var address1 = lineData[126];
                            var address2 = lineData[127];
                            var address3 = lineData[128];
                            var address4 = lineData[128];
                      
                            error = Data.AddImportLairage(apGoodsReceiptID, number,documentDate, remarks, killdate, moveResponse,
                                reference, serial, transactionDate, transactionQty, transactionWeight,
                                tare, inmasterId, isbox, eartag, herd, dob, category, sex, farmAssured, ageInMths, clipped,
                                casualty, lame, cleanliness, noOfMoves, currentResidency, totalResidency,
                                dateOfLastMove, generic1, generic2, killNo, carcassNo, sequencedDate, gradingDate, condemned,
                                detained, notInsured, tbYes, burstBelly, fatColour, carcassSide,
                                coldWeight, grade, side1Weight, side2Weight, countryOfOrigin, rearedIn, supplierId, attribute220,
                                attribute230, attribute231, attribute232, attribute233,
                                attribute234, attribute235, attribute236, attribute237, attribute238, supplierName,
                                supplierCode, address1, address2, address3, address4);

                            if (error != string.Empty)
                            {
                                newFilename = string.Format("{0}_Error_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));
                                if (!file.Equals(newFilename))
                                {
                                    // mark the file as having been processed.
                                    File.Move(file, newFilename);
                                }

                                break;
                            }
                        }

                        newFilename = string.Format("{0}_Imported_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));

                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                        }
                    }
                    catch (Exception e)
                    {
                        this.log.LogError(this.GetType(), e.Message);
                        newFilename = string.Format("{0}_Error_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));
                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}
