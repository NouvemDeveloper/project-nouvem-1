﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Shared;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class ImportCarcassPrices : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                string newFilename;
                var files = Directory.EnumerateFiles(ApplicationSettings.ImportCarcassPricesPath).Where(x => x.EndsWithIgnoringCase("csv"));
                foreach (var file in files)
                {
                    try
                    {
                        IList<StockDetail> carcasses = new List<StockDetail>();
                        foreach (string line in File.ReadLines(file, Encoding.UTF8))
                        {
                            var lineData = line.Split('|');
                            var carcassNo = lineData[0];
                            var killDate = lineData[1];
                            var price = lineData[2].ToDecimal();
                            carcasses.Add(new StockDetail{Carcass = carcassNo, KillDate = killDate, Price = price});
                        }

                        Data.ImportCarcassPrice(carcasses);
                        newFilename = string.Format("{0}_Imported_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));

                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                        }
                    }
                    catch (Exception e)
                    {
                        this.log.LogError(this.GetType(), e.Message);
                        newFilename = string.Format("{0}_Error_{1}", file, DateTime.Now.ToString("ddMMyyyyHHmm"));
                        if (!file.Equals(newFilename))
                        {
                            // mark the file as having been processed.
                            File.Move(file, newFilename);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}
