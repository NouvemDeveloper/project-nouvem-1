﻿// -----------------------------------------------------------------------
// <copyright file="BackUpDatabase.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Net;
using Neodynamic.SDK.Printing;
using Nouvem.Scheduler.BusinessLogic;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;

namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using Quartz;

    public class PrintLabel : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var printer = PrintManager.Instance;
                if (printer.Label == null)
                {
                    this.log.LogError(this.GetType(), "PrintLabel: No label found.");
                    return;
                }

                var palletId = Data.GetPalletLabel();
                if (palletId.HasValue)
                {
                    this.log.LogInfo(this.GetType(), string.Format("Attempting to print pallet label {0}", palletId));
                    var data = new List<PalletData>();
                    data.Add(new PalletData {BarcodePallet = palletId.ToString()});
                    printer.Label.DataSource = data;
                    printer.Print(ApplicationSettings.PalletLabelsToPrint);
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }
        }
    }
}