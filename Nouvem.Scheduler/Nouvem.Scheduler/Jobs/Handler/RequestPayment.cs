﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Quartz;

namespace Nouvem.Scheduler.Jobs.Handler
{
    public class RequestPayment : HandlerBase, IJob
    {

        public RequestPayment()
        {
            
        }

        public void Execute(IJobExecutionContext context)
        {
            var rxcui = "198440";
            //var request = HttpWebRequest.Create(string.Format(@"http://rxnav.nlm.nih.gov/REST/RxTerms/rxcui/{0}/allinfo", rxcui));
            var request = HttpWebRequest.Create(@" http://192.168.0.114:64588/api/values");

            request.ContentType = "application/json";
            request.Method = "GET";

            try
            {
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        var content = reader.ReadToEnd();
                        if (string.IsNullOrWhiteSpace(content))
                        {
                            Console.Out.WriteLine("Response contained empty body...");
                        }
                        else
                        {
                            var localContent = content.Replace(@"\", string.Empty);
                            localContent = localContent.Substring(1, localContent.Length - 2);
                            var ages = new List<BPMaster>();
                            ages = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BPMaster>>(localContent);
                            var localData = ages;
                        }


                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }
        }
    }
}
