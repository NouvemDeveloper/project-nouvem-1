﻿// -----------------------------------------------------------------------
// <copyright file="JobScheduler.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.Owin.Hosting;
using Nouvem.Scheduler.Jobs.Trigger;

namespace Nouvem.Scheduler
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading.Tasks;

    public partial class JobScheduler : ServiceBase
    {
        private Data job = new Data();

        public static bool DeviceSettingsRetrieved { get; set; }

        public JobScheduler()
        {
            this.InitializeComponent();
        }

        public void OnDebug()
        {
            this.OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            StartOptions options = new StartOptions();

            options.Urls.Add("http://localhost:8000");
            options.Urls.Add("http://127.0.0.1:8000");
            options.Urls.Add("http://192.168.43.197:8000");
     
            //var _webapp = WebApp.Start<Startup>(options);
            this.job.SchedulerStart();
        }

        protected override void OnStop()
        {
            this.job.SaveSettings();
        }
    }
}
