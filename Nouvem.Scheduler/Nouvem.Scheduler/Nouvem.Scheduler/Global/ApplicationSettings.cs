﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationSettings.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Global
{
    using System;
    using Nouvem.Scheduler.Properties;

    public static class ApplicationSettings
    {
        /// <summary>
        /// Gets the ssrs selected report path.
        /// </summary>
        internal static string ReportServerPath
        {
            get
            {
                return Settings.Default.ReportServerPath;
            }
        }


        /// <summary>
        /// Gets the reporting services web service url.
        /// </summary>
        internal static string SSRSWebServiceURL
        {
            get
            {
                return Settings.Default.SSRSWebServiceURL;
            }
        }

        /// <summary>
        /// Gets the reporting services network credentials user name.
        /// </summary>
        internal static string SSRSUserName
        {
            get
            {
                return Settings.Default.SSRSUserName;
            }
        }

        /// <summary>
        /// Gets the reporting services network credentials password.
        /// </summary>
        internal static string SSRSPassword
        {
            get
            {
                return Settings.Default.SSRSPassword;
            }
        }

        /// <summary>
        /// Gets the reporting services network credentials domain name.
        /// </summary>
        internal static string SSRSDomain
        {
            get
            {
                return Settings.Default.SSRSDomain;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the dispatch report to.
        /// </summary>
        internal static string DispatchReportPrinter1
        {
            get
            {
                return Settings.Default.DispatchReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the dispatch report to.
        /// </summary>
        internal static string DispatchReportPrinter2
        {
            get
            {
                return Settings.Default.DispatchReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the dispatch report to.
        /// </summary>
        internal static string DispatchReportPrinter3
        {
            get
            {
                return Settings.Default.DispatchReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the dispatch report to.
        /// </summary>
        internal static string DispatchReportPrinter4
        {
            get
            {
                return Settings.Default.DispatchReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int DispatchReportPrinterCopies
        {
            get
            {
                return Settings.Default.DispatchReportPrinterCopies == 0 ? 1 : Settings.Default.DispatchReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the dispatch report.
        /// </summary>
        internal static bool DispatchReportConnect
        {
            get
            {
                return Settings.Default.DispatchReportConnect;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the DispatchSummary report to.
        /// </summary>
        internal static string DispatchSummaryReportPrinter1
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the DispatchSummary report to.
        /// </summary>
        internal static string DispatchSummaryReportPrinter2
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the DispatchSummary report to.
        /// </summary>
        internal static string DispatchSummaryReportPrinter3
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the DispatchSummary report to.
        /// </summary>
        internal static string DispatchSummaryReportPrinter4
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static string AccountsDirectory { get; set; }
       
        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int DispatchSummaryReportPrinterCopies
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinterCopies == 0 ? 1 : Settings.Default.DispatchSummaryReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the DispatchSummary report.
        /// </summary>
        internal static bool DispatchSummaryReportConnect
        {
            get
            {
                return Settings.Default.DispatchSummaryReportConnect;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the Intake report to.
        /// </summary>
        internal static string IntakeReportPrinter1
        {
            get
            {
                return Settings.Default.IntakeReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the Intake report to.
        /// </summary>
        internal static string IntakeReportPrinter2
        {
            get
            {
                return Settings.Default.IntakeReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the Intake report to.
        /// </summary>
        internal static string IntakeReportPrinter3
        {
            get
            {
                return Settings.Default.IntakeReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the Intake report to.
        /// </summary>
        internal static string IntakeReportPrinter4
        {
            get
            {
                return Settings.Default.IntakeReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the Invoice report to.
        /// </summary>
        internal static string InvoiceReportPrinter1
        {
            get
            {
                return Settings.Default.InvoiceReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the Invoice report to.
        /// </summary>
        internal static string InvoiceReportPrinter2
        {
            get
            {
                return Settings.Default.InvoiceReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the Invoice report to.
        /// </summary>
        internal static string InvoiceReportPrinter3
        {
            get
            {
                return Settings.Default.InvoiceReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the Invoice report to.
        /// </summary>
        internal static string InvoiceReportPrinter4
        {
            get
            {
                return Settings.Default.InvoiceReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int IntakeReportPrinterCopies
        {
            get
            {
                return Settings.Default.IntakeReportPrinterCopies == 0 ? 1 : Settings.Default.IntakeReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int InvoiceReportPrinterCopies
        {
            get
            {
                return Settings.Default.InvoiceReportPrinterCopies == 0 ? 1 : Settings.Default.InvoiceReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the Invoice report.
        /// </summary>
        internal static bool IntakeDetailsReportConnect
        {
            get
            {
                return Settings.Default.IntakeDetailsReportConnect;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the Invoice report.
        /// </summary>
        internal static bool InvoiceReportConnect
        {
            get
            {
                return Settings.Default.InvoiceReportConnect;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the SaleOrder report to.
        /// </summary>
        internal static string SaleOrderReportPrinter1
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the SaleOrder report to.
        /// </summary>
        internal static string SaleOrderReportPrinter2
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the SaleOrder report to.
        /// </summary>
        internal static string SaleOrderReportPrinter3
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the SaleOrder report to.
        /// </summary>
        internal static string SaleOrderReportPrinter4
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int SaleOrderReportPrinterCopies
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinterCopies == 0 ? 1 : Settings.Default.SaleOrderReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the SaleOrder report.
        /// </summary>
        internal static bool SaleOrderReportConnect
        {
            get
            {
                return Settings.Default.SaleOrderReportConnect;
            }
        }

        /// <summary>
        /// Gets the reports job interval.
        /// </summary>
        internal static int ReportsProcessingInterval
        {
            get
            {
                return Settings.Default.ReportsProcessingInterval == 0 ? 1 : Settings.Default.ReportsProcessingInterval;
            }
        }

        public static int DataIntegrationProcessingInterval { get; set; }

        public static bool BackUpDatabase { get; set; }

        public static int BackUpDatabaseTime { get; set; }
        public static int BackUpDatabaseTimeMins { get; set; }
        public static int RemoveBatchesTime { get; set; }
        public static int RemoveBatchesTimeMins { get; set; }
        public static int RemoveBatchesOverMonths { get; set; }
        public static bool RemoveBatches { get; set; }
        public static bool CheckSpecialPrices { get; set; }
        public static int? DateTemplateId { get; set; }
        public static int? TraceabilityTemplateId { get; set; }
        public static int? AttributeTemplateId { get; set; }
        public static int? VatCodeId { get; set; }
        public static int ProductGroupId { get; set; }

        /// <summary>
        /// Gets a value indicating whether we are running the reports job.
        /// </summary>
        internal static bool RunReportsProcessing
        {
            get
            {
                return Settings.Default.RunReportsProcessing;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are checking/broadcasting db changes.
        /// </summary>
        internal static bool BroadcastDBChanges
        {
            get
            {
                return Settings.Default.BroadcastDBChanges;
            }
        }


        /// <summary>
        /// Gets a value indicating whether we are checking/broadcasting db changes.
        /// </summary>
        internal static bool SyncAccountsData { get; set; }

        /// <summary>
        /// Gets the checking/broadcasting db changes time interval.
        /// </summary>
        internal static int BroadcastDBChangesInterval
        {
            get
            {
                return Settings.Default.BroadcastDBChangesInterval;
            }
        }
    }
}

