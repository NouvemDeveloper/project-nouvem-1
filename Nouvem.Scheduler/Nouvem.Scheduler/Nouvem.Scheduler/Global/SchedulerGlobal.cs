﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nouvem.Scheduler.Model.Repository;

namespace Nouvem.Scheduler.Global
{
    internal static class SchedulerGlobal
    {
        private static DataRepository Data = new DataRepository();

        /// <summary>
        /// Gets or set the cancelled status.
        /// </summary>
        public static NouDocStatu NouStatusCancelled { get; set; }

        /// <summary>
        /// Gets or set the cancelled status.
        /// </summary>
        public static NouDocStatu NouStatusComplete { get; set; }

        /// <summary>
        /// Gets or set the cancelled status.
        /// </summary>
        public static NouDocStatu NouStatusActive { get; set; }

        /// <summary>
        /// Gets or set the scheduler device.
        /// </summary>
        public static DeviceMaster Device { get; set; }

        /// <summary>
        /// Gets or set the last time the db was checked for updates.
        /// </summary>
        public static DateTime LastDatabaseCheckTime { get; set; }

        static SchedulerGlobal()
        {
            LastDatabaseCheckTime = DateTime.Now;
            var statuses = Data.GetNouDocStatuses();
            NouStatusActive = statuses.First(x => x.Value.Equals("Active"));
            NouStatusComplete = statuses.First(x => x.Value.Equals("Complete"));
            NouStatusCancelled = statuses.First(x => x.Value.Equals("Cancelled"));
            Device = Data.GetDevice();
        }
    }
}
