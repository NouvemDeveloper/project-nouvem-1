﻿// -----------------------------------------------------------------------
// <copyright file="SpecialPrices.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Quartz;
    using Nouvem.Scheduler.Properties;
    using Nouvem.Shared;

    public class BackUpApplication : HandlerBase, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(@"C:\Source\Nouvem", @"C:\Users\brian\Dropbox\NouvemBackUp", true);
            }
            catch (Exception ex)
            {
                
           
            }
           
        }
    }
}

