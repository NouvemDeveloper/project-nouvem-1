﻿// -----------------------------------------------------------------------
// <copyright file="OrderItem.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace AutoSaleOrderProcessor.Model.BusinessObject
{
    using System;

    public class OrderItem
    {
        /// <summary>
        /// Gets or sets the sale order id.
        /// </summary>
        public long? SaleOrderId { get; set; }

        /// <summary>
        /// Gets or sets the customer.
        /// </summary>
        public string Customer { get; set; }

        /// <summary>
        /// Gets or sets the product code.
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// Gets or sets the product description.
        /// </summary>
        public string ProductDescription { get; set; }

        /// <summary>
        /// Gets or sets the qty.
        /// </summary>
        public int? Quantity { get; set; }

        /// <summary>
        /// Gets or sets the wgt.
        /// </summary>
        public decimal? Weight { get; set; }

        /// <summary>
        /// Gets or sets the creation time.
        /// </summary>
        public DateTime Creation { get; set; }
    }
}

