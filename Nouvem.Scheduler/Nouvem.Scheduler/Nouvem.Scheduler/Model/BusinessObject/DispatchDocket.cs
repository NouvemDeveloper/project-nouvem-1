﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Scheduler.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;

    public class DispatchDocket
    {
        /// <summary>
        /// Gets or sets the  docket id.
        /// </summary>
        public long DispatchDocketId { get; set; }

        /// <summary>
        /// Gets or sets the  docket id.
        /// </summary>
        public int DispatchDocketNumber { get; set; }

        /// <summary>
        /// Gets or sets the associated sale order id.
        /// </summary>
        public long? SaleOrderId { get; set; }

        /// <summary>
        /// Gets or sets the associated sale order id.
        /// </summary>
        public string CustomerSaleOrderId { get; set; }

        /// <summary>
        /// Gets or sets the dispatch date.
        /// </summary>
        public DateTime? DispatchDocketDate { get; set; }

        /// <summary>
        /// Gets or sets the dispatch docket items.
        /// </summary>
        public IList<DispatchDocketItem> DispatchDocketItems { get; set; }

        /// <summary>
        /// Gets the dispatch items count.
        /// </summary>
        public int GetDispatchItemsCount
        {
            get
            {
                return this.DispatchDocketItems.Count;
            }
        }
    }
}
