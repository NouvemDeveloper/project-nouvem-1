﻿// -----------------------------------------------------------------------
// <copyright file="DispatchDocketItem.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Model.BusinessObject
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class DispatchDocketItem
    {
        /// <summary>
        /// Gets or sets the docket item id.
        /// </summary>
        public int DispatchDocketItemId { get; set; }

        /// <summary>
        /// Gets or sets the docket item id.
        /// </summary>
        public int INMasterID { get; set; }

        /// <summary>
        /// Gets or sets the docket item id.
        /// </summary>
        public int PriceListID { get; set; }

        /// <summary>
        /// Gets or sets the docket id.
        /// </summary>
        public long? DispatchDocketId { get; set; }

        /// <summary>
        /// Gets or sets the item quantity.
        /// </summary>
        public double? Quantity { get; set; }

        /// <summary>
        /// Gets or sets the item weight.
        /// </summary>
        public double? Weight { get; set; }

        /// <summary>
        /// Gets or sets the item price.
        /// </summary>
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets the line total price.
        /// </summary>
        public decimal? TotalPrice { get; set; }

        /// <summary>
        /// Gets or sets the external customer code reference.
        /// </summary>
        public string CustomerSaleOrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the external customer code reference.
        /// </summary>
        public string DeliveryAddress { get; set; }

        /// <summary>
        /// Gets or sets the dispathed item product code.
        /// </summary>
        public string ProductCode { get; set; }
    }
}

