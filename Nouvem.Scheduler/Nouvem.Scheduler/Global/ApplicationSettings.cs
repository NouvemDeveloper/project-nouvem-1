﻿// -----------------------------------------------------------------------
// <copyright file="ApplicationSettings.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.AccountsIntegration;

namespace Nouvem.Scheduler.Global
{
    using System;
    using Nouvem.Scheduler.Properties;

    public static class ApplicationSettings
    {
        /// <summary>
        /// Gets the ssrs selected report path.
        /// </summary>
        internal static string ReportServerPath
        {
            get
            {
                return Settings.Default.ReportServerPath;
            }
        }


        /// <summary>
        /// Gets the reporting services web service url.
        /// </summary>
        internal static string SSRSWebServiceURL
        {
            get
            {
                return Settings.Default.SSRSWebServiceURL;
            }
        }

        /// <summary>
        /// Gets the reporting services network credentials user name.
        /// </summary>
        internal static string SSRSUserName
        {
            get
            {
                return Settings.Default.SSRSUserName;
            }
        }

        /// <summary>
        /// Gets the reporting services network credentials password.
        /// </summary>
        internal static string SSRSPassword
        {
            get
            {
                return Settings.Default.SSRSPassword;
            }
        }

        /// <summary>
        /// Gets the reporting services network credentials domain name.
        /// </summary>
        internal static string SSRSDomain
        {
            get
            {
                return Settings.Default.SSRSDomain;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the dispatch report to.
        /// </summary>
        internal static string DispatchReportPrinter1
        {
            get
            {
                return Settings.Default.DispatchReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the dispatch report to.
        /// </summary>
        internal static string DispatchReportPrinter2
        {
            get
            {
                return Settings.Default.DispatchReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the dispatch report to.
        /// </summary>
        internal static string DispatchReportPrinter3
        {
            get
            {
                return Settings.Default.DispatchReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the dispatch report to.
        /// </summary>
        internal static string DispatchReportPrinter4
        {
            get
            {
                return Settings.Default.DispatchReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int DispatchReportPrinterCopies
        {
            get
            {
                return Settings.Default.DispatchReportPrinterCopies == 0 ? 1 : Settings.Default.DispatchReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the dispatch report.
        /// </summary>
        internal static bool DispatchReportConnect
        {
            get
            {
                return Settings.Default.DispatchReportConnect;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the DispatchSummary report to.
        /// </summary>
        internal static string DispatchSummaryReportPrinter1
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the DispatchSummary report to.
        /// </summary>
        internal static string DispatchSummaryReportPrinter2
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the DispatchSummary report to.
        /// </summary>
        internal static string DispatchSummaryReportPrinter3
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the DispatchSummary report to.
        /// </summary>
        internal static string DispatchSummaryReportPrinter4
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static bool Sage200AssembliesLoaded { get; set; }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static string AccountsDirectory { get; set; }

        internal static string AccountsConnection { get; set; }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static string AccountsPackage { get; set; }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int DispatchSummaryReportPrinterCopies
        {
            get
            {
                return Settings.Default.DispatchSummaryReportPrinterCopies == 0 ? 1 : Settings.Default.DispatchSummaryReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the DispatchSummary report.
        /// </summary>
        internal static bool DispatchSummaryReportConnect
        {
            get
            {
                return Settings.Default.DispatchSummaryReportConnect;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the Intake report to.
        /// </summary>
        internal static string IntakeReportPrinter1
        {
            get
            {
                return Settings.Default.IntakeReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the Intake report to.
        /// </summary>
        internal static string IntakeReportPrinter2
        {
            get
            {
                return Settings.Default.IntakeReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the Intake report to.
        /// </summary>
        internal static string IntakeReportPrinter3
        {
            get
            {
                return Settings.Default.IntakeReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the Intake report to.
        /// </summary>
        internal static string IntakeReportPrinter4
        {
            get
            {
                return Settings.Default.IntakeReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the printer1 to print the Invoice report to.
        /// </summary>
        internal static string InvoiceReportPrinter1
        {
            get
            {
                return Settings.Default.InvoiceReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the Invoice report to.
        /// </summary>
        internal static string InvoiceReportPrinter2
        {
            get
            {
                return Settings.Default.InvoiceReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the Invoice report to.
        /// </summary>
        internal static string InvoiceReportPrinter3
        {
            get
            {
                return Settings.Default.InvoiceReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the Invoice report to.
        /// </summary>
        internal static string InvoiceReportPrinter4
        {
            get
            {
                return Settings.Default.InvoiceReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int IntakeReportPrinterCopies
        {
            get
            {
                return Settings.Default.IntakeReportPrinterCopies == 0 ? 1 : Settings.Default.IntakeReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int InvoiceReportPrinterCopies
        {
            get
            {
                return Settings.Default.InvoiceReportPrinterCopies == 0 ? 1 : Settings.Default.InvoiceReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the Invoice report.
        /// </summary>
        internal static bool IntakeDetailsReportConnect
        {
            get
            {
                return Settings.Default.IntakeDetailsReportConnect;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the Invoice report.
        /// </summary>
        internal static bool InvoiceReportConnect
        {
            get
            {
                return Settings.Default.InvoiceReportConnect;
            }
        }

        internal static string InvoiceName { get; set; }

        internal static string SaleOrderName { get; set; }
        internal static string SaleOrderParameter { get; set; }

        /// <summary>
        /// Gets the printer1 to print the SaleOrder report to.
        /// </summary>
        internal static string SaleOrderReportPrinter1
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinter1;
            }
        }

        /// <summary>
        /// Gets the printer2 to print the SaleOrder report to.
        /// </summary>
        internal static string SaleOrderReportPrinter2
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinter2;
            }
        }

        /// <summary>
        /// Gets the printer3 to print the SaleOrder report to.
        /// </summary>
        internal static string SaleOrderReportPrinter3
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinter3;
            }
        }

        /// <summary>
        /// Gets the printer4 to print the SaleOrder report to.
        /// </summary>
        internal static string SaleOrderReportPrinter4
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinter4;
            }
        }

        /// <summary>
        /// Gets the number of copies required.
        /// </summary>
        internal static int SaleOrderReportPrinterCopies
        {
            get
            {
                return Settings.Default.SaleOrderReportPrinterCopies == 0 ? 1 : Settings.Default.SaleOrderReportPrinterCopies;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are printing the SaleOrder report.
        /// </summary>
        internal static bool SaleOrderReportConnect
        {
            get
            {
                return Settings.Default.SaleOrderReportConnect;
            }
        }

        /// <summary>
        /// Gets the reports job interval.
        /// </summary>
        internal static int ReportsProcessingInterval
        {
            get
            {
                return Settings.Default.ReportsProcessingInterval == 0 ? 1 : Settings.Default.ReportsProcessingInterval;
            }
        }

        public static bool EmailReportJob1Run { get; set; }
        public static string EmailReportJob1ReportName { get; set; }
        public static int EmailReportJob1Time { get; set; }
        public static int EmailReportJob1TimeMins { get; set; }
        public static int EmailReportJob1RunTime { get; set; }
        public static string EmailReportJob1Param1 { get; set; }
        public static string EmailReportJob1Param2 { get; set; }
        public static string EmailReportJob1Param3 { get; set; }
        public static string EmailReportJob1Param4 { get; set; }
        public static string EmailReportJob1Param5 { get; set; }
        public static string EmailReportJob1Param6 { get; set; }
        public static string EmailReportJob1Path { get; set; }

        public static bool EmailReportJob2Run { get; set; }
        public static string EmailReportJob2ReportName { get; set; }
        public static int EmailReportJob2Time { get; set; }
        public static int EmailReportJob2TimeMins { get; set; }
        public static int EmailReportJob2RunTime { get; set; }
        public static string EmailReportJob2Param1 { get; set; }
        public static string EmailReportJob2Param2 { get; set; }
        public static string EmailReportJob2Param3 { get; set; }
        public static string EmailReportJob2Param4 { get; set; }
        public static string EmailReportJob2Param5 { get; set; }
        public static string EmailReportJob2Param6 { get; set; }
        public static string EmailReportJob2Path { get; set; }


        public static int DataIntegrationProcessingInterval { get; set; }
        public static bool UpdateProducts { get; set; }

        public static bool BackUpDatabase { get; set; }
        public static int BackUpDatabaseTime { get; set; }
        public static int BackUpDatabaseTimeMins { get; set; }

        public static bool BackUpDatabase2 { get; set; }
        public static int BackUpDatabaseTime2 { get; set; }
        public static int BackUpDatabaseTimeMins2 { get; set; }

        public static bool BackUpDatabase3 { get; set; }
        public static int BackUpDatabaseTime3 { get; set; }
        public static int BackUpDatabaseTimeMins3 { get; set; }

        public static bool BackUpDatabase4 { get; set; }
        public static int BackUpDatabaseTime4 { get; set; }
        public static int BackUpDatabaseTimeMins4 { get; set; }

        public static bool CheckForDispatchExport { get; set; }
        public static int DispatchExportTime { get; set; }
        public static int DispatchExportTimeMins { get; set; }
        public static string DispatchExportEmailAddress { get; set; }
        public static string DispatchExportFilePath { get; set; }
        public static int DispatchExportHindID { get; set; }
        public static int DispatchExportForeID { get; set; }

        public static int LairageExportTime { get; set; }
        public static int LairageExportTimeMins { get; set; }

        public static int RemoveBatchesTime { get; set; }
        public static int RemoveBatchesTimeMins { get; set; }
        public static int RemoveBatchesOverMonths { get; set; }
        public static bool RemoveBatches { get; set; }
        public static bool CheckSpecialPrices { get; set; }
        public static bool RunDBJob { get; set; }
        public static int RunDBJobRunTime { get; set; }
        public static bool RunDBJob2 { get; set; }
        public static int RunDBJobRunTime2 { get; set; }
        public static int? DateTemplateId { get; set; }
        public static int? TraceabilityTemplateId { get; set; }
        public static int? AttributeTemplateId { get; set; }
        public static int? VatCodeId { get; set; }
        public static int ProductGroupId { get; set; }
        public static int CheckForUnifyOrderTime { get; set; }
        public static bool CheckForUnifyOrder { get; set; }
        public static string CheckForUnifyOrderIssueEmailAddresses { get; set; }
        public static bool PrintPalletLabel { get; set; }
        public static bool PrintPalletLabelData { get; set; }
        public static bool ReprintMoveLabels { get; set; }
        public static int PrintPalletLabelCheck { get; set; }
        public static int PrintPalletLabelDataCheck { get; set; }
        public static string PrinterIP { get; set; }
        public static string PrinterPalletIP { get; set; }
        public static string PrinterPalletIP2 { get; set; }
        public static int PrinterPallet2Device { get; set; }
        public static int PrinterDPI { get; set; }
        public static string ConnectionString { get; set; }

        public static bool ExportCarcassPrices { get; set; }
        public static int ExportCarcassPricesTime { get; set; }
        public static int ExportCarcassPricesTimeMins { get; set; }
        public static string ExportCarcassPricesPath { get; set; }
        public static string ExportCarcassPricesEmail { get; set; }

        public static bool ImportCarcassPrices { get; set; }
        public static int ImportCarcassPricesTime { get; set; }
        public static string ImportCarcassPricesPath { get; set; }

        public static bool UpdateAveragePriceList { get; set; }
        public static int UpdateAveragePriceListTime { get; set; }
        public static int EmailPort { get; set; }

        public static int PalletLabelsToPrint { get; set; }
        public static bool CheckForIntakeImport { get; set; }
        public static bool CheckForLairageImport { get; set; }
        public static bool CheckForImportLairage { get; set; }
        public static int CheckForImportLairageTime { get; set; }
        public static string CheckForImportLairagePath { get; set; }
        public static int CheckForIntakeImportTime { get; set; }
        public static string CheckForIntakeImportPath { get; set; }
        public static bool CheckForLairageExport { get; set; }
        public static int CheckForLairageExportTime { get; set; }
        public static string CheckForLairageExportPath { get; set; }
        public static string CheckForLairageExportCustomer1 { get; set; }
        public static string CheckForLairageExportCustomer2 { get; set; }
        public static string CheckForLairageExportCustomer3 { get; set; }
        public static int CheckForLairageExportSleep { get; set; }
        public static int CheckForIntakeImportWarehouse { get; set; }
        public static string CheckForIntakeImportCountryOfOrigin { get; set; }
        public static int CheckForIntakeImportProduct { get; set; }
        public static int ProductIdYoungBull { get; set; }
        public static int ProductIdBull { get; set; }
        public static int ProductIdSteer { get; set; }
        public static int ProductIdCow { get; set; }
        public static int ProductIdHeifer { get; set; }
        public static int ProductIdVealYoung { get; set; }
        public static int ProductIdVealOld { get; set; }
        public static int ProductIntake { get; set; }
        public static string Customer { get; set; } = "Nouvem";
        public static string NouvemEmailAddress { get; set; } = "nouvem.email@gmail.com";
        public static string NouvemEmailPassword { get; set; } = "Nouv1234";

        public static string NouvemEmailAddresses { get; set; } =
            "brianmurray@nouvem,olliehayden@nouvem.com,aidanvallely@nouvem.com";


        /// <summary>
        /// Gets a value indicating whether we are running the reports job.
        /// </summary>
        internal static bool RunReportsProcessing
        {
            get
            {
                return Settings.Default.RunReportsProcessing;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we are checking/broadcasting db changes.
        /// </summary>
        internal static bool BroadcastDBChanges
        {
            get
            {
                return Settings.Default.BroadcastDBChanges;
            }
        }


        /// <summary>
        /// Gets a value indicating whether we are checking/broadcasting db changes.
        /// </summary>
        internal static bool SyncAccountsData { get; set; }

        /// <summary>
        /// Gets the checking/broadcasting db changes time interval.
        /// </summary>
        internal static int BroadcastDBChangesInterval
        {
            get
            {
                return Settings.Default.BroadcastDBChangesInterval;
            }
        }
    }
}

