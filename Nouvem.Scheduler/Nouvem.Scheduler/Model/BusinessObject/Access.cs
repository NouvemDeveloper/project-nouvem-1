﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nouvem.Scheduler.Model.BusinessObject
{
    public class Access
    {
        public string orders { get; set; }
        public string blocking { get; set; }
        public string products { get; set; }
        public string product_base { get; set; }
        public string pricegroups { get; set; }
        public string customers { get; set; }
        public string company { get; set; }
        public string users { get; set; }
        public string access { get; set; }
        public string statistic { get; set; }
        public string schedules { get; set; }
        public string dashboard { get; set; }
        public string images { get; set; }
        public string group_name { get; set; }
        public string leads { get; set; }
        public string waiting { get; set; }
        public string ready { get; set; }
    }

    public class User
    {
        public string id { get; set; }
        public string organisationid { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string type { get; set; }
        public string mobile { get; set; }
        public Access access { get; set; }
        public string token { get; set; }
        public bool first { get; set; }
    }

    public class Organisation
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string image { get; set; }
        public string orgtype { get; set; }
        public string mobile { get; set; }
        public bool docketTab { get; set; }
        public string noimg_colour { get; set; }
        public int isPinPage { get; set; }
    }

   


}
