﻿// -----------------------------------------------------------------------
// <copyright file="ReportManager.cs" company="Nouvem Limited">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------


using Nouvem.Logging;
using Nouvem.Scheduler.Global;
using Nouvem.Scheduler.Model.BusinessObject;
using Nouvem.Scheduler.Properties;
using Nouvem.Shared;

namespace Nouvem.Scheduler.BusinessLogic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Printing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;

    using Microsoft.Reporting.WinForms;

    using Nouvem.Scheduler.ReportService;


    /// <summary>
    /// Manager class for the application reporting.
    /// </summary>
    public class ReportManager 
    {
        #region field

        private ILogger log = new Logger();

        /// <summary>
        /// The direct print reference.
        /// </summary>
        private ReportPrintDocument directPrint;

        /// <summary>
        /// Gets the singleton reference.
        /// </summary>
        private static readonly ReportManager Manager = new ReportManager();

        /// <summary>
        /// The report service web service helper object reference.
        /// </summary>
        private ReportingService2010 reportingService = new ReportingService2010();

        /// <summary>
        /// The reporting server reports.
        /// </summary>
        private IList<ReportData> reports = new List<ReportData>();

        private int reportCount = 1;

        #endregion

        #region constructor

        private ReportManager()
        {
            this.log.LogInfo(this.GetType(), "ReportManager()..cons");
            this.reportingService.Credentials = CredentialCache.DefaultCredentials;
            this.reportingService.Url = ApplicationSettings.SSRSWebServiceURL;
            this.reportingService.Credentials = new NetworkCredential(ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain);

            this.log.LogInfo(this.GetType(), string.Format("URL:{0}, UserName:{1}, Password:{2}, Domain:{3}, URI Path:{4}", 
                ApplicationSettings.SSRSWebServiceURL, ApplicationSettings.SSRSUserName,
                ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain, ApplicationSettings.ReportServerPath));

            this.GetReports();
        }

        #endregion

        #region property

        /// <summary>
        /// Gets the report manager singleton.
        /// </summary>
        public static ReportManager Instance
        {
            get
            {
                return Manager;
            }
        }

        /// <summary>
        /// Gets the reports.
        /// </summary>
        public IList<ReportData> Reports
        {
            get
            {
                return this.reports;
            }
        }

        #endregion

        #region method

        /// <summary>
        /// Prints a report directly without calling the report viewer.
        /// </summary>
        /// <param name="report">The server report to print.</param>
        /// <param name="isServerReport">Is the report a server report flag.</param>
        public void PrintReport(ReportData report, string printerName, bool isServerReport = true, int numOfCopies = 1)
        {
            this.log.LogInfo(this.GetType(), string.Format("Printing:{0},Report Name:{1}", printerName, report.Name));
            if (!this.reports.Any())
            {
                this.GetReports();
            }

            this.log.LogInfo(this.GetType(), $"Checking for report {report.Name}. Reports count:{this.reports.Count}");
            var localReport = this.reports.FirstOrDefault(x => x.Name.Equals(report.Name));

            if (localReport != null)
            {
                this.log.LogInfo(this.GetType(), $"Report {report.Name}. found. Path:{localReport.Path}. URI:{ApplicationSettings.ReportServerPath}");
                foreach (var para in report.ReportParameters)
                {
                    this.log.LogInfo(this.GetType(), $"Parameter Name:{para.Name}");
                }

                localReport.ReportParameters = report.ReportParameters;
                var reportPath = localReport.Path;
                var reportParameters = localReport.ReportParameters;
                var serverReport = new ServerReport { ReportPath = reportPath };
                serverReport.ReportServerUrl = new Uri(ApplicationSettings.ReportServerPath);

                if (reportParameters != null)
                {
                    this.log.LogInfo(this.GetType(), $"Setting parameters");
                    serverReport.SetParameters(reportParameters);
                }

                this.log.LogInfo(this.GetType(), $"Setting up printing");
                try
                {
                    this.directPrint = new ReportPrintDocument(serverReport);
                }
                catch (Exception e)
                {
                    this.log.LogInfo(this.GetType(), $"Report Print Document Error:{e.Message}, {e.InnerException}");
                }
           
                if (this.directPrint != null)
                {
                    try
                    {
                        this.directPrint.PrinterSettings.PrinterName = printerName;
                        this.log.LogInfo(this.GetType(), $"Printing ready: {this.directPrint.PrinterSettings.PrinterName}, Num Copies:{numOfCopies}");

                        for (int i = 0; i < numOfCopies; i++)
                        {
                            this.log.LogInfo(this.GetType(), $"Attempting to print now...");
                            this.directPrint.Print();
                            this.log.LogInfo(this.GetType(), "Printed");
                        }
                    }
                    catch (Exception e)
                    {
                        this.log.LogInfo(this.GetType(), $"Printing Error:{e.Message}, {e.InnerException}");
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="filePath"></param>
        /// <param name="reportName"></param>
        public void GenerateAndEmailReport(ReportData report, string filePath, string emailAddresses)
        {
            try
            {
                if (!this.reports.Any())
                {
                    this.GetReports();
                }

                var localReport = this.reports.FirstOrDefault(x => x.Name.CompareIgnoringCase(report.Name));

                if (localReport != null)
                {
                    localReport.ReportParameters = report.ReportParameters;
                    var reportPath = localReport.Path;
                    var reportParameters = localReport.ReportParameters;
                    var serverReport = new ServerReport { ReportPath = reportPath };

                    serverReport.ReportServerUrl = new Uri(ApplicationSettings.ReportServerPath);
                    serverReport.ReportServerCredentials.NetworkCredentials
                        = new System.Net.NetworkCredential(ApplicationSettings.SSRSUserName, ApplicationSettings.SSRSPassword, ApplicationSettings.SSRSDomain);

                    if (reportParameters != null)
                    {
                        serverReport.SetParameters(reportParameters);
                    }

                    var fileName = this.ExportReports(serverReport, filePath, report.Name);
                    var attachments = new List<string> {fileName};
                    
                    EmailManager.Instance.SendEmail(localReport.Name,"", emailAddresses, attachments);
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
            }
        }

        /// <summary>
        /// Exports the current report out to a pdf file
        /// </summary>
        /// <returns>Path to the file that was generated</returns>
        private string ExportReports(ServerReport report, string filePath, string reportName = "Report")
        {
            Microsoft.Reporting.WinForms.Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;
            var exportType = "PDF";

            //Render the report to a byte array
            var bytes = report.Render(exportType, null, out mimeType,
                out encoding, out filenameExtension, out streamids, out warnings);

            //Write report out to temporary PDF file
            var reportSuffix = string.Format("{0}{1}.{2}", reportName, this.reportCount, exportType);
            var filename = Path.Combine(filePath, reportSuffix);
            using (var fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }

            this.reportCount++;

            //return path to saved file
            return filename;
        }

        #endregion

        #region private

        /// <summary>
        /// Gets the reporting server reports.
        /// </summary>
        public void GetReports()
        {
            this.log.LogInfo(this.GetType(), "GetReports()..retrieving");
            try
            {
                var items = this.reportingService.ListChildren("/", true);
                
                foreach (var item in items.Where(x => x.TypeName.Equals("Report")))
                {
                    this.reports.Add(new ReportData { Name = item.Name, Path = item.Path });
                    //this.log.LogInfo(this.GetType(), string.Format("{0} report found", item.Name));
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), ex.Message);
            }
        }

        #endregion

        #region nested class

        class ReportPrintDocument : PrintDocument
        {
            #region field

            /// <summary>
            /// The page settings.
            /// </summary>
            private PageSettings pageSettings;

            /// <summary>
            /// The current page we are working on.
            /// </summary>
            private int currentPage;

            /// <summary>
            /// The pages to stream.
            /// </summary>
            private readonly List<Stream> pages = new List<Stream>();

            #endregion

            #region public interface

            /// <summary>
            /// Prints the server report.
            /// </summary>
            /// <param name="serverReport">The report to print.</param>
            public ReportPrintDocument(ServerReport serverReport)
                : this((Report)serverReport)
            {
                this.RenderAllServerReportPages(serverReport);
            }

            /// <summary>
            /// Prints the local report.
            /// </summary>
            /// <param name="localReport">The local report to print.</param>
            public ReportPrintDocument(LocalReport localReport)
                : this((Report)localReport)
            {
                this.RenderAllLocalReportPages(localReport);
            }

            #endregion

            #region protected

            /// <summary>
            /// Dispose, and clean up.
            /// </summary>
            /// <param name="disposing">The disposing flag.</param>
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);

                if (disposing)
                {
                    foreach (Stream s in pages)
                    {
                        s.Dispose();
                    }

                    pages.Clear();
                }
            }

            /// <summary>
            /// Handle the print initialisation.
            /// </summary>
            /// <param name="e">The event args.</param>
            protected override void OnBeginPrint(PrintEventArgs e)
            {
                base.OnBeginPrint(e);
                this.currentPage = 0;
            }

            /// <summary>
            /// Handle the page printing.
            /// </summary>
            /// <param name="e">The event args.</param>
            protected override void OnPrintPage(PrintPageEventArgs e)
            {
                base.OnPrintPage(e);

                var pageToPrint = this.pages[this.currentPage];
                pageToPrint.Position = 0;

                // Load each page into a Metafile to draw it.
                using (Metafile pageMetaFile = new Metafile(pageToPrint))
                {
                    var adjustedRect = new Rectangle(
                            e.PageBounds.Left - (int)e.PageSettings.HardMarginX,
                            e.PageBounds.Top - (int)e.PageSettings.HardMarginY,
                            e.PageBounds.Width,
                            e.PageBounds.Height);

                    // Draw a white background for the report
                    e.Graphics.FillRectangle(Brushes.White, adjustedRect);

                    // Draw the report content
                    e.Graphics.DrawImage(pageMetaFile, adjustedRect);

                    // Prepare for next page.  Make sure we haven't hit the end.
                    this.currentPage++;
                    e.HasMorePages = this.currentPage < this.pages.Count;
                }
            }

            /// <summary>
            /// Clone the page settings.
            /// </summary>
            /// <param name="e">The event args.</param>
            protected override void OnQueryPageSettings(QueryPageSettingsEventArgs e)
            {
                e.PageSettings = (PageSettings)this.pageSettings.Clone();
            }

            #endregion

            #region private

            /// <summary>
            /// Set the dimensions.
            /// </summary>
            /// <param name="report">The report to set dimensions for.</param>
            private ReportPrintDocument(Report report)
            {
                // Set the page settings to the default defined in the report
                var reportPageSettings = report.GetDefaultPageSettings();

                /* The page settings object will use the default printer unless
                 * PageSettings.PrinterSettings is changed.  This assumes there               
                 * is a default printer.*/
                this.pageSettings = new PageSettings();
                this.pageSettings.PaperSize = reportPageSettings.PaperSize;
                this.pageSettings.Margins = reportPageSettings.Margins;
            }

            /// <summary>
            /// Render the report pages.
            /// </summary>
            /// <param name="serverReport">The server report to render.</param>
            private void RenderAllServerReportPages(ServerReport serverReport)
            {
                string deviceInfo = CreateEMFDeviceInfo();

                /* Generating Image renderer pages one at a time can be expensive.  In order
                 * to generate page 2, the server would need to recalculate page 1 and throw it
                 * away.  Using PersistStreams causes the server to generate all the pages in
                 * the background but return as soon as page 1 is complete. */
                var firstPageParameters = new NameValueCollection();
                firstPageParameters.Add("rs:PersistStreams", "True");

                // GetNextStream returns the next page in the sequence from the background process
                // started by PersistStreams.
                var nonFirstPageParameters = new NameValueCollection();
                nonFirstPageParameters.Add("rs:GetNextStream", "True");

                string mimeType;
                string fileExtension;
                var pageStream = serverReport.Render("IMAGE", deviceInfo, firstPageParameters, out mimeType, out fileExtension);

                // The server returns an empty stream when moving beyond the last page.
                while (pageStream.Length > 0)
                {
                    this.pages.Add(pageStream);

                    pageStream = serverReport.Render("IMAGE", deviceInfo, nonFirstPageParameters, out mimeType, out fileExtension);
                }
            }

            /// <summary>
            /// Render the local report pages.
            /// </summary>
            /// <param name="localReport">The local report to render.</param>
            private void RenderAllLocalReportPages(LocalReport localReport)
            {
                var deviceInfo = this.CreateEMFDeviceInfo();

                Microsoft.Reporting.WinForms.Warning[] warnings;
                localReport.Render("IMAGE", deviceInfo, this.LocalReportCreateStreamCallback, out warnings);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="name"></param>
            /// <param name="extension"></param>
            /// <param name="encoding"></param>
            /// <param name="mimeType"></param>
            /// <param name="willSeek"></param>
            /// <returns></returns>
            private Stream LocalReportCreateStreamCallback(
                string name,
                string extension,
                Encoding encoding,
                string mimeType,
                bool willSeek)
            {
                var stream = new MemoryStream();
                this.pages.Add(stream);

                return stream;
            }

            private string CreateEMFDeviceInfo()
            {
                PaperSize paperSize = pageSettings.PaperSize;
                Margins margins = pageSettings.Margins;

                // The device info string defines the page range to print as well as the size of the page.
                // A start and end page of 0 means generate all pages.
                return string.Format(
                    CultureInfo.InvariantCulture,
                    "<DeviceInfo><OutputFormat>emf</OutputFormat><StartPage>0</StartPage><EndPage>0</EndPage><MarginTop>{0}</MarginTop><MarginLeft>{1}</MarginLeft><MarginRight>{2}</MarginRight><MarginBottom>{3}</MarginBottom><PageHeight>{4}</PageHeight><PageWidth>{5}</PageWidth></DeviceInfo>",
                    ToInches(margins.Top),
                    ToInches(margins.Left),
                    ToInches(margins.Right),
                    ToInches(margins.Bottom),
                    ToInches(paperSize.Height),
                    ToInches(paperSize.Width));
            }

            /// <summary>
            /// Calculate amount in 1/100 inchs.
            /// </summary>
            /// <param name="hundrethsOfInch">The value to calculate.</param>
            /// <returns></returns>
            private static string ToInches(int hundrethsOfInch)
            {
                double inches = hundrethsOfInch / 100.0;
                return inches.ToString(CultureInfo.InvariantCulture) + "in";
            }

            #endregion
        }

        #endregion
    }
}
