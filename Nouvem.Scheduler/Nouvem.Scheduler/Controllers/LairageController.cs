﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Nouvem.Scheduler.Model.BusinessObject;


namespace Nouvem.Scheduler.Controllers
{
    public class LairageController : ApiController
    {
        public IHttpActionResult GetAllStudents()
        {
            IList<Test> lairages = null;

            using (var entities = new NouvemEntities())
            {
                lairages = (from t in entities.APGoodsReceipts
                           select new Test {Number = t.Number})
                           .ToList();
            }

            if (lairages.Count == 0)
            {
                return NotFound();
            }

            return Ok(lairages);
        }
    }
}
