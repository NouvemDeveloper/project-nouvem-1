﻿

CREATE View [dbo].[ViewTraceabilityTemplateAllocations]
  AS

  SELECT 
         ttn.Name, 
		 tt.TraceabilityTemplateNameID,
		 tt.TraceabilityTemplateMasterID, 
		 tt.Batch,
		 tt.[Transaction],
		 tt.[Required],
		 tt.[Reset],
		 tm.TraceabilityCode, 
		 tm.TraceabilityDescription,
		 n.TraceabilityType
		
  
  FROM TraceabilityTemplateAllocation tt
      inner join TraceabilityTemplateMaster tm on tt.TraceabilityTemplateMasterID = tm.TraceabilityTemplatesMasterID
	  inner join TraceabilityTemplateName ttn on tt.TraceabilityTemplateNameID = ttn.TraceabilityTemplateNameID
	  Inner join NouTraceabilityType n on tm.NouTraceabilityTypeID = n.NouTraceabilityTypeID

  WHERE tt.Deleted = 0