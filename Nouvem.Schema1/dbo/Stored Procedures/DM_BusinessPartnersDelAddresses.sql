﻿


-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the partner data from the DEMLocal db to the Nouvem partners tables.
-- =============================================
CREATE PROCEDURE [dbo].[DM_BusinessPartnersDelAddresses]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 	 
	 @CustomerCode varchar(50),	
	 @CustomerDelAddress varchar(254)
	
	 DECLARE PARTNER_CURSOR CURSOR STATIC FOR
     SELECT c.Code,
	        c.[Address]			
 
     FROM [192.168.16.140\DEMSQLSERVER].[DEMMigrationDB].[dbo].[CustomerDeliveryAddresses] c
      
 
     OPEN PARTNER_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @CustomerCode,	      
		   @CustomerDelAddress
		

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 	 
	   
		DECLARE @BPMasterID int =  (SELECT TOP 1 BPMasterID FROM BPMaster
								   WHERE RTRIM(LTRIM(Code)) = RTRIM(LTRIM(@CustomerCode)))		
								  
			IF (@BPMasterID > 0)
			  INSERT iNTO BPAddress VALUES (@BPMasterID, @CustomerDelAddress, '', '', '', '', 0, 1, 0, NULL)			
			

	 FETCH NEXT FROM PARTNER_CURSOR
	 INTO  @CustomerCode,	      
		   @CustomerDelAddress
	 END
	 
	 CLOSE PARTNER_CURSOR;
     DEALLOCATE PARTNER_CURSOR;
END



