﻿



-- =============================================
-- Author:		brian murray
-- Create date: 09/01.2016
-- Description:	Testing - Remove transaction data
-- =============================================
CREATE PROCEDURE [dbo].[TestRemoveTransactions]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    delete from BatchTraceability
delete from TransactionTraceability
delete from StockTransaction
delete from BatchNumber
delete from APGoodsReceiptDetail
delete from apgoodsreceipt
update APOrder set NouDocStatusID = 1
update APOrderDetail set QuantityDelivered = 0
update APOrderDetail set WeightDelivered = 0
delete from ARDispatchDetail
delete from ARDispatch
delete from PROrder

select * from APGoodsReceiptDetail
select * from apgoodsreceipt
select * from BatchTraceability
select * from TransactionTraceability
select * from BatchNumber
select * from StockTransaction
select * from ViewTransaction
END




