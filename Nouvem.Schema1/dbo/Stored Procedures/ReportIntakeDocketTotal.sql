﻿
-- =============================================
-- Author:		brian murray
-- Create date: 05/04/2016
-- Description:	Gets the intake docket totals
-- =============================================
CREATE PROCEDURE [dbo].[ReportIntakeDocketTotal]
	-- Add the parameters for the stored procedure here
	@GoodsInID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT gr.Number AS 'Document No', 
	       gr.CreationDate AS 'Intake Date',
		   gr.APGoodsReceiptID,
		   SUM(gd.WeightReceived) AS 'Weight',
		   SUM(gd.QuantityReceived) AS 'Quantity',
		   b.Name as 'Supplier',
		   b.Code,
		   m.Name AS 'Product',
		   m.INMasterID

		   FROM APGoodsReceipt gr 
		      INNER JOIN APGoodsReceiptDetail gd
	                  ON gr.APGoodsReceiptID = gd.APGoodsReceiptID
			  INNER JOIN INMaster m
			          ON m.INMasterID = gd.INMasterID			          
			  LEFT JOIN BPMaster b
		              ON gr.BPMasterID_Supplier = b.BPMasterID

	WHERE gr.APGoodsReceiptID = @GoodsInID

	GROUP BY gr.Number, gr.CreationDate, b.Name, b.Code, m.Name, m.INMasterID, gr.APGoodsReceiptID
	         
END

