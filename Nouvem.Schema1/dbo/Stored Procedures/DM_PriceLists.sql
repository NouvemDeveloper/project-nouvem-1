﻿-- =============================================
-- Author:		brian murray
-- Create date: 16-05-2016
-- Description:	Migrates the pricing data from the DEMLocal db to the Nouvem price lists.
-- =============================================
CREATE PROCEDURE [dbo].[DM_PriceLists]	
AS
BEGIN
	SET NOCOUNT ON;
	 DECLARE 
	 @ProductID int,
	 @CustomerID int,
	 @PricePerUnit decimal(18,2),
	 @PriceMethod int,
	 @ProductCode varchar(30),
	 @ProductDescription varchar(70),
	 @CustomerCode varchar(50),
	 @CustomerName varchar(254)


	 DECLARE PRICE_CURSOR CURSOR STATIC FOR
     SELECT pp.ProductID,
			pp.CustomerID,
			pp.PricePerUnit,
			pp.PriceMethod,
			p.ProductCode,
			p.[Description],
			c.CustomerCode,
			c.CompanyName
 
     FROM [192.168.16.140\DEMSQLSERVER].[DEMMigrationDB].[dbo].[ProductPrice]  pp
      INNER JOIN [192.168.16.140\DEMSQLSERVER].[DEMMigrationDB].[dbo].[Products]  p
	     ON p.ID = pp.ProductID
	  INNER JOIN [192.168.16.140\DEMSQLSERVER].[DEMMigrationDB].[dbo].[Customers]  c
	     ON c.ID = pp.CustomerID
 
     OPEN PRICE_CURSOR;

  
    -- Fetch first Batch row (if available) from cursor
     FETCH NEXT FROM PRICE_CURSOR
	 INTO @ProductID,
		  @CustomerID,
		  @PricePerUnit,
		  @PriceMethod,
		  @ProductCode,
		  @ProductDescription,
		  @CustomerCode,
		  @CustomerName

     -- Check Fetch_status to ensure we have a record to work with
     WHILE @@FETCH_STATUS = 0
     BEGIN 
	 
		DECLARE @BPMasterID int =  (SELECT TOP 1 BPMasterID FROM BPMaster
								  WHERE RTRIM(LTRIM(Code)) = RTRIM(LTRIM(@CustomerCode)) AND RTRIM(LTRIM(Name)) = RTRIM(LTRIM(@CustomerName)))
		DECLARE @INMasterID INT = (SELECT TOP 1 INMasterID FROM INMaster
								  WHERE RTRIM(LTRIM(Code)) = RTRIM(LTRIM(@ProductCode)) AND RTRIM(LTRIM(Name)) = RTRIM(LTRIM(@ProductDescription)))
								  
			IF (@INMasterID > 0)
		    BEGIN
			    DECLARE @PriceListID INT = 
			    (SELECT TOP 1 PriceLIstID 
			    FROM PriceList 
			    WHERE [CurrentPriceListName] = RTRIM(LTRIM(@CustomerName)))

				DECLARE @PriceMethodID INT = (CASE WHEN @PriceMethod = 0 THEN 1 ELSE 3 END)

				IF @PriceListID > 0
			    BEGIN
				    
				    INSERT INTO [dbo].[PriceListDetail] VALUES (@PriceListID, @INMasterID, @PricePerUnit, 5,@PriceMethodID, NULL, 0)
			    END
			    ELSE
			    BEGIN
				    INSERT INTO [dbo].[PriceList] VALUES (@CustomerName, NULL, 0, 1, 3, 3, 0,NULL, GETDATE(),NULL,NULL)

				    --now we need to get the id of this inserted record
	        	    SET @PriceListID = IDENT_CURRENT('dbo.PriceList')
			        INSERT INTO [dbo].[PriceListDetail] VALUES (@PriceListID, @INMasterID, @PricePerUnit, 5,@PriceMethodID, NULL, 0)

					  --update the partner with the price list id
					UPDATE BPMaster SET PriceListID = @PriceListID WHERE BPMasterID = @BPMasterID
			    END				
		    END		
	

	 FETCH NEXT FROM PRICE_CURSOR
		 INTO @ProductID,
			  @CustomerID,
			  @PricePerUnit,
			  @PriceMethod,
			  @ProductCode,
			  @ProductDescription,
			  @CustomerCode,
			  @CustomerName
	 END
	 
	 CLOSE PRICE_CURSOR;
     DEALLOCATE PRICE_CURSOR;
END
