﻿





-- =============================================
-- Author:		brian murray
-- Create date: 06/05/2016
-- Description:	Gets the invoice data.
-- =============================================
CREATE PROCEDURE [dbo].[ReportInvoiceDocket] 
	-- Add the parameters for the stored procedure here
	@InvoiceID AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  i.ARInvoiceID,
        i.TotalExVAT,
		i.SubTotalExVAT,
		i.GrandTotalIncVAT,
		i.VAT,	
		i.DeliveryDate,
		i.DocumentDate,
		i.Number,
		i.CustomerPOReference,
        b.Name,
		b.Code,
		case when m.NominalWeight > 0 then dd.QuantityDelivered * m.NominalWeight else dd.WeightDelivered end as 'WeightDelivered',
		a.AddressLine1,
		a.AddressLine2,
		a.AddressLine3,
		a.AddressLine4,
		a.PostCode,
		invoice.AddressLine1 AS 'InvoiceAddressLine1',
		invoice.AddressLine2 AS 'InvoiceAddressLine2',
		invoice.AddressLine3 AS 'InvoiceAddressLine3',
		invoice.AddressLine4 AS 'InvoiceAddressLine4',
		invoice.PostCode AS 'InvoicePostCode',
		--dd.WeightDelivered,
		dd.QuantityDelivered,
		dd.UnitPriceAfterDiscount,
		dd.TotalIncVAT,
		m.Code AS 'ProductCode',
		m.Name AS 'ProductName',
		m.INGroupID,
		g.Name AS 'GroupName',
	   (SELECT TOP 1 Number FROM ARDispatch WHERE i.BaseDocumentReferenceId = ARDispatchID) as 'Dispatch No',
	   (SELECT TOP 1 Number FROM AROrder WHERE AROrderID =
	   (SELECT TOP 1 BaseDocumentReferenceID FROM ARDispatch WHERE i.BaseDocumentReferenceId = ARDispatchID)) as 'Order No'
    FROM ARInvoice i
       INNER JOIN ARInvoiceDetail dd
          ON i.ARInvoiceID = dd.ARInvoiceID
       INNER JOIN INMaster m
          ON dd.INMasterID = m.INMasterID	
	   INNER JOIN INGroup g
	      ON g.INGroupID = m.INGroupID
       INNER JOIN BPMaster b 
          ON i.BPMasterID_Customer = b.BPMasterID	 
       LEFT JOIN BPAddress a
          ON i.BPAddressID_Delivery = a.BPAddressID
       LEFT JOIN BPAddress invoice
          ON i.BPAddressID_Invoice = invoice.BPAddressID
     WHERE i.ARInvoiceID = @InvoiceID  AND dd.Deleted IS NULL AND (dd.WeightDelivered > 0 or dd.QuantityDelivered >0) 
     END