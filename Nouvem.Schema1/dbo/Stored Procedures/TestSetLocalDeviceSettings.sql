﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE TestSetLocalDeviceSettings
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update devicesettings set Value1 = 'http://brian-pc:80/ReportServer/ReportService2010.asmx' where name = 'SSRSWebServiceURL'
	update devicesettings set Value1 = 'brian' where name = 'SSRSUserName'
	update devicesettings set Value1 = '123' where name = 'SSRSPassword'
	update devicesettings set Value1 = 'brian-pc' where name = 'SSRSDomain'
	update devicesettings set Value1 = 'http://brian-pc:80/reportserver' where name = 'ReportServerPath'
	update devicesettings set Value1 = 'True' where name = 'ConnectToSSRS'
	end