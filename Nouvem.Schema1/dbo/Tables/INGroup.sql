﻿CREATE TABLE [dbo].[INGroup] (
    [INGroupID]                  INT          IDENTITY (1, 1) NOT NULL,
    [Name]                       VARCHAR (50) NOT NULL,
    [ParentInGroupID]            INT          NULL,
    [TraceabilityTemplateNameID] INT          NULL,
    [DateTemplateNameID]         INT          NULL,
    [QualityTemplateNameID]      INT          NULL,
    [Deleted]                    BIT          NOT NULL,
    CONSTRAINT [PK_INGroup] PRIMARY KEY CLUSTERED ([INGroupID] ASC),
    CONSTRAINT [FK_INGroup_DateTemplateName] FOREIGN KEY ([DateTemplateNameID]) REFERENCES [dbo].[DateTemplateName] ([DateTemplateNameID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_INGroup_INGroup] FOREIGN KEY ([ParentInGroupID]) REFERENCES [dbo].[INGroup] ([INGroupID]),
    CONSTRAINT [FK_INGroup_TraceabilityTemplateName] FOREIGN KEY ([TraceabilityTemplateNameID]) REFERENCES [dbo].[TraceabilityTemplateName] ([TraceabilityTemplateNameID]) ON DELETE CASCADE ON UPDATE CASCADE
);

