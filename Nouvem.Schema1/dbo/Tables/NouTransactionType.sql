﻿CREATE TABLE [dbo].[NouTransactionType] (
    [NouTransactionTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [Description]          NVARCHAR (50) NOT NULL,
    [AddToStock]           BIT           NULL,
    [SubtractFromStock]    BIT           NULL,
    [Deleted]              BIT           NOT NULL,
    CONSTRAINT [PK_NouTrasactionType] PRIMARY KEY CLUSTERED ([NouTransactionTypeID] ASC)
);

