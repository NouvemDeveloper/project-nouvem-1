﻿CREATE TABLE [dbo].[NouLicenceTypeRule] (
    [NouLicenceTypeRuleID]    INT IDENTITY (1, 1) NOT NULL,
    [LicenceDetailID]         INT NOT NULL,
    [NouAuthorisationID]      INT NOT NULL,
    [NouAuthorisationValueID] INT NOT NULL,
    [Deleted]                 BIT NOT NULL,
    CONSTRAINT [PK_SysLicenceTypeRules] PRIMARY KEY CLUSTERED ([NouLicenceTypeRuleID] ASC),
    CONSTRAINT [FK_NouLicenceTypeRule_LicenceDetail] FOREIGN KEY ([LicenceDetailID]) REFERENCES [dbo].[LicenceDetail] ([LicenceDetailID]),
    CONSTRAINT [FK_NouLicenceTypeRule_NouAuthorisation] FOREIGN KEY ([NouAuthorisationID]) REFERENCES [dbo].[NouAuthorisation] ([NouAuthorisationID]),
    CONSTRAINT [FK_NouLicenceTypeRule_NouAuthorisationValue] FOREIGN KEY ([NouAuthorisationValueID]) REFERENCES [dbo].[NouAuthorisationValue] ([NouAuthorisationValueID])
);

