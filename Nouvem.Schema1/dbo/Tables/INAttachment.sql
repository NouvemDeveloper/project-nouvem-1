﻿CREATE TABLE [dbo].[INAttachment] (
    [INAttachmentID] INT             IDENTITY (1, 1) NOT NULL,
    [INMasterID]     INT             NOT NULL,
    [FilePath]       NVARCHAR (50)   NULL,
    [FileName]       NVARCHAR (50)   NOT NULL,
    [AttachmentDate] DATE            NOT NULL,
    [Deleted]        BIT             NOT NULL,
    [File]           VARBINARY (MAX) CONSTRAINT [DF__INAttachme__File__05F8DC4F] DEFAULT (0x01) NOT NULL,
    CONSTRAINT [PK_INAttachments] PRIMARY KEY CLUSTERED ([INAttachmentID] ASC),
    CONSTRAINT [FK_INAttachments_INMaster] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID])
);

