﻿CREATE TABLE [dbo].[LabelDeletion] (
    [LabelDeletionID]    INT            IDENTITY (1, 1) NOT NULL,
    [StockTransactionID] INT            NOT NULL,
    [DeletionTime]       DATETIME       NOT NULL,
    [UserMasterID]       INT            NULL,
    [DeviceMasterID]     INT            NULL,
    [Reason]             NVARCHAR (500) NULL,
    [Deleted]            DATETIME       NULL,
    CONSTRAINT [PK_LabelDeletion] PRIMARY KEY CLUSTERED ([LabelDeletionID] ASC),
    CONSTRAINT [FK_LabelDeletion_DateMaster] FOREIGN KEY ([DeviceMasterID]) REFERENCES [dbo].[DeviceMaster] ([DeviceID]),
    CONSTRAINT [FK_LabelDeletion_StockTransaction] FOREIGN KEY ([StockTransactionID]) REFERENCES [dbo].[StockTransaction] ([StockTransactionID]),
    CONSTRAINT [FK_LabelDeletion_UserMaster] FOREIGN KEY ([UserMasterID]) REFERENCES [dbo].[UserMaster] ([UserMasterID])
);

