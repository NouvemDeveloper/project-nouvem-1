﻿CREATE TABLE [dbo].[PriceListDetail] (
    [PriceListDetailID]       INT             IDENTITY (1, 1) NOT NULL,
    [PriceListID]             INT             NOT NULL,
    [INMasterID]              INT             NOT NULL,
    [Price]                   DECIMAL (18, 2) NOT NULL,
    [NouOrderMethodID]        INT             NOT NULL,
    [NouPriceMethodID]        INT             NOT NULL,
    [ManualPriceAndOrMethods] BIT             NULL,
    [Deleted]                 BIT             NOT NULL,
    [DeviceMasterID]          INT             NULL,
    [UserMasterID]            INT             NULL,
    [EditDate]                DATETIME        NULL,
    CONSTRAINT [PK_PriceListDetail] PRIMARY KEY CLUSTERED ([PriceListDetailID] ASC),
    CONSTRAINT [FK_INMaster_PriceListDetail] FOREIGN KEY ([INMasterID]) REFERENCES [dbo].[INMaster] ([INMasterID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_NouOrderMethod_PriceListDetail] FOREIGN KEY ([NouOrderMethodID]) REFERENCES [dbo].[NouOrderMethod] ([NouOrderMethodID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_NouPriceMethod_PriceListDetail] FOREIGN KEY ([NouPriceMethodID]) REFERENCES [dbo].[NouPriceMethod] ([NouPriceMethodID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_PriceList_PriceListDetail] FOREIGN KEY ([PriceListID]) REFERENCES [dbo].[PriceList] ([PriceListID]) ON DELETE CASCADE ON UPDATE CASCADE
);

