﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Nouvem.Indicator;
using Nouvem.Shared;
using Nouvem.Shared.Localisation;

namespace Nouvem.SerialTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Indicator.Indicator indicator;
        private Indicator.Indicator indicatorReceive;
        private DispatcherTimer timer = new DispatcherTimer();
        private string motion;
        public MainWindow()
        {
            InitializeComponent();
            this.TextBlockSettings.Text = "COM1,9600,None,One,8";
            this.TextBlockSettingsIndicator.Text = "COM1,9600,None,One,8,GENERIC";
            this.TextBoxInterval.Text = "100";
            this.TextBoxMotion.Text = "M";
            this.TextBoxLength.Text = "10";
            this.TextBoxTerm.Text = "13";
            this.ButtonBuffer.Content = "Buffer Off";
        }

        
        #region loop back

        private void OpenPort()
        {
            try
            {
                var settings = this.TextBlockSettings.Text.Split(',');
                var com = settings.ElementAt(0);
                var baud = settings.ElementAt(1);
                var parity = settings.ElementAt(2);
                var stopBits = settings.ElementAt(3);
                var dataBits = settings.ElementAt(4);
                //var indicatorType = (Indicator.IndicatorModel)Enum.Parse(typeof(Indicator.IndicatorModel), ApplicationSettings.IndicatorModel);
                //var filePath = ApplicationSettings.IndicatorAlibiPath;

                this.indicator = new Indicator.Indicator(
                    com,
                    baud,
                    parity,
                    stopBits,
                    dataBits,
                    Indicator.Indicator.IndicatorModel.GENERIC,
                    "");

                this.indicator.OpenPort();

                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = $"Port Open:{this.indicator.IsPortOpen.BoolToYesNo()}";
                });
            }
            catch (Exception e)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = e.Message;
                });
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                this.timer.Stop();
                this.indicator.ClosePort();
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = $"Port Open:{this.indicator.IsPortOpen.BoolToYesNo()}";
                });
            }
            catch (Exception ex)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = ex.Message;
                });
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.OpenPort();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                this.TextBoxMessage.Text = "Sending Data...";
                this.timer.Interval = TimeSpan.FromMilliseconds(this.TextBoxInterval.Text.ToInt());
                this.timer.Tick += (s, args) =>
                {
                    try
                    {
                        var stable = this.LabelMotion.Content == "Motion" ? "M" : "S";
                        var dataToSend = $"{this.TextBlockWeight.Text.PadLeft(6, '0')}{stable}";
                        this.indicator.WriteToPort(dataToSend);
                    }
                    catch (Exception exception)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            this.TextBoxMessage.Text = exception.Message;
                        });
                    }
                };

                this.timer.Start();
            }
            catch (Exception ex)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = ex.Message;
                });
            }
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.LabelMotion.Content = "Motion";
        }

        private void Slider_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            this.LabelMotion.Content = string.Empty;
        }


        #endregion

        #region indicator test

        private void OpenPortIndicator()
        {
            try
            {
                var settings = this.TextBlockSettingsIndicator.Text.Split(',');
                var com = settings.ElementAt(0);
                var baud = settings.ElementAt(1);
                var parity = settings.ElementAt(2);
                var stopBits = settings.ElementAt(3);
                var dataBits = settings.ElementAt(4);
                var localIndicator = settings.ElementAt(5);
                var indicatorType = (Indicator.Indicator.IndicatorModel)Enum.Parse(typeof(Indicator.Indicator.IndicatorModel), localIndicator);
                //var filePath = ApplicationSettings.IndicatorAlibiPath;

                this.indicatorReceive = new Indicator.Indicator(
                    com,
                    baud,
                    parity,
                    stopBits,
                    dataBits,
                    indicatorType,
                    "",
                    testMode:true);

                this.indicatorReceive.SetPortDataVariables(
                    this.TextBoxLength.Text.ToInt(), 
                    0, 
                    0, 
                    0, 
                    0, 
                    this.TextBoxMotion.Text, 
                    this.TextBoxTerm.Text.ToInt().ToChar());

                this.indicatorReceive.WeightDataReceived += this.IndicatorReceiveOnWeightDataReceived;
                //this.indicatorReceive.BufferDataReceived += this.IndicatorReceiveOnBufferDataReceived;
                this.indicatorReceive.OpenPort();

                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = $"Port Open:{this.indicatorReceive.IsPortOpen.BoolToYesNo()}";
                });
            }
            catch (Exception e)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = e.Message;
                });
            }
        }

        private void IndicatorReceiveOnBufferDataReceived(object sender, BufferDataReceivedArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                try
                {
                    //this.RichTextBoxBuffer.Text = this.RichTextBoxBuffer.Text.Insert(0,e.BufferData);
                    this.RichTextBoxBuffer.AppendText(e.BufferData);
                    this.RichTextBoxBuffer.ScrollToCaret();
                    //this.RichTextBoxBuffer.Document.Blocks.Add(new Paragraph(new Run(e.BufferData)));
                }
                catch (Exception)
                {
                }
            }));
        }

        private void IndicatorReceiveOnWeightDataReceived(object sender, WeightDataReceivedArgs args)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                try
                {
                    this.TextBlockWeightReceive.Text = args.SerialData;
                    this.LabelMotionReceive.Content = args.InMotion ? "Motion" : string.Empty;
                }
                catch (Exception)
                {
                }
            }));
        }

        #endregion

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            this.OpenPortIndicator();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            try
            {
                this.indicatorReceive.WeightDataReceived -= this.IndicatorReceiveOnWeightDataReceived;
                this.indicatorReceive.BufferDataReceived -= this.IndicatorReceiveOnBufferDataReceived;
                this.indicatorReceive.ClosePort();
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = $"Port Open:{this.indicatorReceive.IsPortOpen.BoolToYesNo()}";
                });
            }
            catch (Exception ex)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.TextBoxMessage.Text = ex.Message;
                });
            }
        }

        private void ButtonBuffer_Click(object sender, RoutedEventArgs e)
        {
            if (this.indicatorReceive == null)
            {
                return;
            }

            if (this.ButtonBuffer.Content.ToString() == "Buffer Off")
            {
                this.ButtonBuffer.Content = "Buffer On";
                this.indicatorReceive.BufferDataReceived += this.IndicatorReceiveOnBufferDataReceived;
            }
            else
            {
                this.ButtonBuffer.Content = "Buffer Off";
                this.indicatorReceive.BufferDataReceived -= this.IndicatorReceiveOnBufferDataReceived;
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {

        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.TextBoxMessage.Text = string.Empty;
        }
    }
}
