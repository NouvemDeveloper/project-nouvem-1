﻿// -----------------------------------------------------------------------
// <copyright file="SpecialPrices.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Scheduler.Jobs.Handler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Quartz;
    using Nouvem.Scheduler.Properties;
    using Nouvem.Shared;

    public class SpecialPrices : HandlerBase, IJob
    {
        /// <summary>
        /// gets or sets the price lists.
        /// </summary>
        public IList<PriceList> PriceListMasters { get; set; }

        /// <summary>
        /// Gets or sets the business partners.
        /// </summary>
        public IList<BPMaster> BusinessPartners { get; set; }

        public void Execute(IJobExecutionContext context)
        {
            this.log.LogInfo(this.GetType(), "Checking future prices");
            this.PriceListMasters = Data.GetPriceLists();
            this.BusinessPartners = Data.GetBusinessPartners();
            this.CheckFuturePriceBooks();
        }

        /// <summary>
        /// Checks for future price books to be implemented.
        /// </summary>
        private void CheckFuturePriceBooks()
        {
            var partnersToUpdate = new List<BPMaster>();
            var basePriceLists = new List<PriceList>();
            var today = DateTime.Today;
            foreach (var priceList in this.PriceListMasters)
            {
                if (priceList.StartDate.HasValue && priceList.StartDate.ToDate().Date == today)
                {
                    this.log.LogInfo(this.GetType(), string.Format("Price list found to update:{0}. Base price list:{1}", priceList.PriceListID, priceList.BasePriceListID));
                    var basePriceList = priceList.BasePriceListID;
                    if (basePriceList.HasValue)
                    {
                        var localBaseList = this.PriceListMasters.FirstOrDefault(x => x.PriceListID == basePriceList);
                        if (localBaseList != null)
                        {
                            localBaseList.EndDate = DateTime.Now;
                            basePriceLists.Add(localBaseList);
                        }

                        foreach (var partner in this.BusinessPartners)
                        {
                            if (partner.PriceListID == basePriceList)
                            {
                                partner.PriceListID = priceList.PriceListID;
                                partnersToUpdate.Add(partner);
                            }
                        }
                    }
                }
            }

            this.log.LogInfo(this.GetType(), string.Format("{0} partners found to update", partnersToUpdate.Count));
            if (partnersToUpdate.Any())
            {
                Data.UpdateBusinessPartnerPricing(partnersToUpdate);
                Data.UpdatePriceMasters(basePriceLists);
                Data.HandlePriceChanges(partnersToUpdate);
            }
        }
    }
}
