﻿// -----------------------------------------------------------------------
// <copyright file="DataRepository.cs" company="Nouvem Technology">
// Copyright (c) Nouvem Technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

using Nouvem.Logging;
using Nouvem.Scheduler.Model.BusinessObject;

namespace Nouvem.Scheduler.Model.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Nouvem.Scheduler.Global;
    using Nouvem.Scheduler.Properties;
    using Nouvem.Shared;

    public class DataRepository
    {
        private ILogger log = new Logger();

        #region broadcast

        /// <summary>
        /// Checks the dispatch dockets, updating them if they dont tally.
        /// </summary>
        /// <returns>A flag, indicating a business partner change or not.</returns>
        public bool CheckBusinessPartners(DateTime lastCheckTime)
        {
            this.log.LogInfo(this.GetType(), "CheckBusinessPartners. Checking for partner changes");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.BPMasters.Any(x => x.EditDate != null && x.EditDate >= lastCheckTime);
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        #endregion

        #region auto sale order processor

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="docNumberId">The document number id of the current document.</param>
        /// <returns>The updated doc number.</returns>
        public int GetDeliveryAddress(string address)
        {
           var localAddress = address.ToLower().Trim();
           try
            {
                using (var entities = new NouvemEntities())
                {
                    // update
                    var dbAddress =
                        entities.BPAddresses.FirstOrDefault(x => x.AddressLine1.ToLower().Trim().Equals(localAddress));

                    if (dbAddress == null)
                    {
                        var add = new BPAddress
                        {
                            BPMasterID = 411,
                            AddressLine1 = address,
                            AddressLine2 = string.Empty,
                            AddressLine3 = string.Empty,
                            AddressLine4 = string.Empty,
                            Shipping = true,
                            PostCode = string.Empty,
                            Deleted = false
                        };

                        entities.BPAddresses.Add(add);
                        entities.SaveChanges();
                        return add.BPAddressID;
                    }

                    return dbAddress.BPAddressID;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return 0;
        }

        /// <summary>
        /// Add or updates the document numbers.
        /// </summary>
        /// <param name="docNumberId">The document number id of the current document.</param>
        /// <returns>The updated doc number.</returns>
        public int SetDocumentNumber(int docNumberId)
        {
            this.log.LogInfo(this.GetType(), string.Format("SetDocumentNumber(): Updating doc number ID:{0}", docNumberId));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    // update
                    var dbNumber =
                        entities.DocumentNumberings.FirstOrDefault(
                            number => number.DocumentNumberingID == docNumberId);

                    if (dbNumber != null)
                    {
                        var currentNo = dbNumber.NextNumber;
                        var nextNo = dbNumber.NextNumber + 1;
                        if (nextNo > dbNumber.LastNumber)
                        {
                            // Last number reached, so reset back to first number.
                            nextNo = dbNumber.FirstNumber;
                        }

                        dbNumber.NextNumber = nextNo;
                        entities.SaveChanges();
                        this.log.LogInfo(this.GetType(), "Document number updated");

                        return dbNumber.NextNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
                return 0;
            }

            return 0;
        }

        /// <summary>
        /// Gets the product associated with the product code.
        /// </summary>
        /// <param name="code">The code to serach for.</param>
        /// <returns>An associated product.</returns>
        public bool ImportOrders(IList<ARDispatch> orders)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var order in orders)
                    {
                        var details = order.ARDispatchDetails;
                        order.ARDispatchDetails = null;
                        entities.ARDispatches.Add(order);
                        entities.SaveChanges();

                        foreach (var arDispatchDetail in details)
                        {
                            // Add to the master snapshot table first
                            var snapshot = new INMasterSnapshot
                            {
                                INMasterID = arDispatchDetail.INMasterID,
                                Code = string.Empty,
                                Name = string.Empty
                            };

                            entities.INMasterSnapshots.Add(snapshot);

                            arDispatchDetail.INMasterSnapshotID = snapshot.INMasterSnapshotID;
                            arDispatchDetail.ARDispatchID = order.ARDispatchID;
                            entities.ARDispatchDetails.Add(arDispatchDetail);
                            entities.SaveChanges();
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Gets the product associated with the product code.
        /// </summary>
        /// <param name="code">The code to serach for.</param>
        /// <returns>An associated product.</returns>
        public IList<PriceListDetail> GetPrices()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var prices = new List<PriceListDetail>();
                    var ndPrices = entities.PriceListDetails.Where(x => x.PriceListID == 5254 && !x.Deleted).ToList();
                    var basePrices = entities.PriceListDetails.Where(x => x.PriceListID == 5086 && !x.Deleted).ToList();

                    var ndProductIds = ndPrices.Select(x => x.INMasterID).ToList();
                    foreach (var price in basePrices)
                    {
                        if (!ndProductIds.Contains(price.INMasterID))
                        {
                            prices.Add(price);
                        }
                    }

                    return prices.Concat(ndPrices).ToList();
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the product associated with the product code.
        /// </summary>
        /// <param name="code">The code to serach for.</param>
        /// <returns>An associated product.</returns>
        public INMaster GetProduct(string code)
        {
            this.log.LogInfo(this.GetType(), string.Format("Attempting to get the product for product code:{0}", code));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    var product = entities.INMasters.FirstOrDefault(x => x.Code.Trim().Equals(code.Trim()));

                    if (product != null)
                    {
                        this.log.LogInfo(this.GetType(), string.Format("Product Id:{0} found", product.INMasterID));
                        return product;
                    }

                    this.log.LogError(this.GetType(), "Product not found");
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Marks the processed dispatch dockets as exported.
        /// </summary>
        /// <param name="dockets">The dockets to mark as exported.</param>
        /// <returns>A flag, indicating a successful group export.</returns>
        public bool MarkAsExported(IList<DispatchDocket> dockets)
        {
            this.log.LogInfo(this.GetType(), string.Format("Attempting to mark {0} dispatch dockets as exported", dockets.Count));
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var dispatchDocket in dockets)
                    {
                        var dbDocket =
                            entities.ARDispatches.FirstOrDefault(x => x.ARDispatchID == dispatchDocket.DispatchDocketId);

                        if (dbDocket != null)
                        {
                            dbDocket.Exported = true;
                            this.log.LogInfo(this.GetType(), string.Format("Dispatch docket id:{0} found and marked as exported", dbDocket.ARDispatchID));
                        }
                        else
                        {
                            this.log.LogError(this.GetType(), string.Format("Dispatch docket id:{0} could not be found", dispatchDocket.DispatchDocketId));
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return false;
        }

        /// <summary>
        /// Retrieves the non exported dispatch dockets for the selected customer.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>A collection of non exported dispatch dockets for the selected customer.</returns>
        public IList<DispatchDocket> GetDispatchDockets(int customerId)
        {
            //this.log.LogInfo(this.GetType(), string.Format("Attempting to retrieve the non exported dispatch dockets for customer id:{0}", customerId));
            var dispatchDockets = new List<DispatchDocket>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    dispatchDockets = (from docket in entities.ARDispatches
                                       where docket.BPMasterID_Customer == customerId
                                       && (docket.Exported == null || docket.Exported == false)
                                       && docket.Deleted == null
                                       && docket.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID
                                       select new DispatchDocket
                                       {
                                           DispatchDocketId = docket.ARDispatchID,
                                           SaleOrderId = docket.BaseDocumentReferenceID,
                                           DispatchDocketDate = docket.DeliveryDate,
                                           CustomerSaleOrderId = docket.CustomerPOReference,
                                           DispatchDocketNumber = docket.Number,
                                           DispatchDocketItems = (from item in docket.ARDispatchDetails.Where(x => x.Deleted == null)
                                                                  select new DispatchDocketItem
                                                                  {
                                                                      DispatchDocketItemId = item.ARDispatchDetailID,
                                                                      DispatchDocketId = item.ARDispatchID,
                                                                      Quantity = item.QuantityDelivered,
                                                                      Weight = item.WeightDelivered,
                                                                      UnitPrice = item.UnitPrice,
                                                                      TotalPrice = item.TotalExclVAT,
                                                                      //ExternalRef = item.SaleOrderItem != null ? item.SaleOrderItem.ExternalRef : string.Empty,
                                                                      ProductCode = item.INMaster.Code
                                                                  }).ToList()
                                       }).ToList();

                    //this.log.LogInfo(this.GetType(), string.Format("{0} non exported dispatch dockets retrieved", dispatchDockets.Count));
                }
            }
            catch (Exception ex)
            {
                //this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return dispatchDockets;
        }

        /// <summary>
        /// Retrieves a default user id.
        /// </summary>
        /// <returns>A default user id.</returns>
        public int GetUserId()
        {
            //this.log.LogInfo(this.GetType(), "Attempting to get a user id");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var user = entities.UserMasters.FirstOrDefault();

                    if (user != null)
                    {
                        //this.log.LogInfo(this.GetType(), string.Format("User id :{0} returned", user.ID));
                        return user.UserMasterID;
                    }
                }
            }
            catch (Exception ex)
            {
                //this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            //this.log.LogError(this.GetType(), "No users found");
            return 0;
        }

        /// <summary>
        /// Retrieves a default user id.
        /// </summary>
        /// <returns>A default user id.</returns>
        public int GetDeviceId()
        {
            //this.log.LogInfo(this.GetType(), "Attempting to get a user id");
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var device = entities.DeviceMasters.FirstOrDefault();

                    if (device != null)
                    {
                        //this.log.LogInfo(this.GetType(), string.Format("User id :{0} returned", user.ID));
                        return device.DeviceID;
                    }
                }
            }
            catch (Exception ex)
            {
                //this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            //this.log.LogError(this.GetType(), "No users found");
            return 0;
        }

        #endregion

        /// <summary>
        /// Checks the dispatch dockets, updating them if they dont tally.
        /// </summary>
        /// <returns></returns>
        public string CheckDispatchDockets()
        {

            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.CheckDispatchDockets().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                //this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public List<int> CheckForUnprintedDispatchDockets()
        {
            this.log.LogInfo(this.GetType(), "CheckForUnprintedDispatchDockets(): Retrieving dispatches..");
            var unprintedDocketIds = new List<int>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var unprintedDockets = entities.ARDispatches
                        .Where(
                            x =>
                                x.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID &&
                                x.Printed != true && x.Deleted == null);

                    foreach (var arDispatch in unprintedDockets)
                    {
                        unprintedDocketIds.Add(arDispatch.ARDispatchID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return unprintedDocketIds;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public List<int> CheckForUnprintedInvoiceDockets()
        {
            this.log.LogInfo(this.GetType(), "CheckForUnprintedInvoiceDockets(): Retrieving invoices..");
            var unprintedDocketIds = new List<int>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var unprintedDockets = entities.ARInvoices
                        .Where(
                            x =>
                                x.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID &&
                                x.Printed != true && x.Deleted == null);

                    foreach (var invoice in unprintedDockets)
                    {
                        unprintedDocketIds.Add(invoice.ARInvoiceID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return unprintedDocketIds;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public List<int> CheckForUnprintedSaleOrderDockets()
        {
            this.log.LogInfo(this.GetType(), "CheckForUnprintedSaleOrderDockets(): Retrieving invoices..");
            var unprintedDocketIds = new List<int>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var unprintedDockets = entities.AROrders
                        .Where(
                            x =>
                                x.NouDocStatusID == SchedulerGlobal.NouStatusComplete.NouDocStatusID &&
                                x.Printed != true && x.Deleted == null);

                    foreach (var order in unprintedDockets)
                    {
                        unprintedDocketIds.Add(order.AROrderID);
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return unprintedDocketIds;
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public void UpdateInvoiceDockets(List<int> docketIds)
        {
            if (!docketIds.Any())
            {
                return;
            }

            this.log.LogInfo(this.GetType(), "UpdatingInvoiceDockets(): Updating invoices..");
           
            using (var entities = new NouvemEntities())
            {
                var unprintedDockets = entities.ARInvoices
                    .Where(x => docketIds.Contains(x.ARInvoiceID));

                foreach (var invoice in unprintedDockets)
                {
                    invoice.Printed = true;
                }

                entities.SaveChanges();
            }
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public void UpdateSaleOrderDockets(List<int> docketIds)
        {
            if (!docketIds.Any())
            {
                return;
            }

            this.log.LogInfo(this.GetType(), "UpdatingSaleOrderDockets(): Updating sale orders..");

            using (var entities = new NouvemEntities())
            {
                var unprintedDockets = entities.AROrders
                    .Where(x => docketIds.Contains(x.AROrderID));

                foreach (var docket in unprintedDockets)
                {
                    docket.Printed = true;
                }

                entities.SaveChanges();
            }
        }

        /// <summary>
        /// Retrieves the unprinted dockets.
        /// </summary>
        /// <returns>The unprinted, complete dockets.</returns>
        public void UpdateDispatchDockets(List<int> docketIds)
        {
            if (!docketIds.Any())
            {
                return;
            }

            this.log.LogInfo(this.GetType(), "UpdatingDispatchDockets(): Updating sdispatch dockets..");

            using (var entities = new NouvemEntities())
            {
                var unprintedDockets = entities.ARDispatches
                    .Where(x => docketIds.Contains(x.ARDispatchID));

                foreach (var docket in unprintedDockets)
                {
                    docket.Printed = true;
                }

                entities.SaveChanges();
            }
        }

        /// <summary>
        /// Gets the local device.
        /// </summary>
        /// <returns>The local device.</returns>
        public DeviceMaster GetDevice()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localDevice =
                        entities.DeviceMasters.FirstOrDefault(x => x.DeviceName.Equals(Constant.SchedulerPC));

                    return localDevice;
                }
            }
            catch (Exception ex)
            {
                //this.Log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return null;
        }

        /// <summary>
        /// Gets the local device settings.
        /// </summary>
        /// <returns>The local device settings.</returns>
        public IList<DeviceSetting> GetDeviceSettings()
        {
            this.log.LogInfo(this.GetType(), "GetDeviceSettings(). Retrieving device settings");
            var deviceSettings = new List<DeviceSetting>();
            try
            {
                using (var entities = new NouvemEntities())
                {
                    var localDevice =
                        entities.DeviceMasters.FirstOrDefault(x => x.DeviceName.Equals(Constant.SchedulerPC));

                    if (localDevice != null)
                    {
                        deviceSettings =
                            entities.DeviceSettings.Where(
                                x => x.DeviceMasterID == localDevice.DeviceID && x.Deleted == null).ToList();
                        this.log.LogInfo(this.GetType(), "GetDeviceSettings(). Device settings retrieved");
                    }
                }
            }
            catch (Exception ex)
            {
                this.log.LogError(this.GetType(), string.Format("Exception:{0} Inner:{1}", ex.Message, ex.InnerException));
            }

            return deviceSettings;
        }


        /// <summary>
        /// Updates the current device settings.
        /// </summary>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateDeviceSettings(IList<DeviceSetting> settings)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var setting in settings)
                    {
                        var dbSettings =
                                entities.DeviceSettings.FirstOrDefault(
                                    x => x.Name.Trim() == setting.Name.Trim() && x.DeviceMasterID == SchedulerGlobal.Device.DeviceID);

                        if (dbSettings == null)
                        {
                            entities.DeviceSettings.Add(setting);
                        }
                        else
                        {
                            dbSettings.Name = setting.Name;
                            dbSettings.Value1 = setting.Value1;
                            dbSettings.Value2 = setting.Value2;
                            dbSettings.Value3 = setting.Value3;
                            dbSettings.Value4 = setting.Value4;
                            dbSettings.Value5 = setting.Value5;
                            dbSettings.Value6 = setting.Value6;
                            dbSettings.Value7 = setting.Value7;
                            dbSettings.Value8 = setting.Value8;
                            dbSettings.Deleted = setting.Deleted;
                        }
                    }

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }

            return true;
        }

        /// <summary>
        /// Gets the price lists.
        /// </summary>
        /// <returns>The price lists.</returns>
        public IList<PriceList> GetPriceLists()
        {
            try
            {
                var entities = new NouvemEntities();
                return entities.PriceLists.Where(x => !x.Deleted).ToList();

            }
            catch (Exception ex)
            {
            }

            return null;
        }

        /// <summary>
        /// Gets the price list details.
        /// </summary>
        /// <returns>The price list details.</returns>
        public IList<PriceListDetail> GetPriceListDetails()
        {
            try
            {
                var entities = new NouvemEntities();
                return entities.PriceListDetails.Where(x => !x.Deleted).ToList();

            }
            catch (Exception ex)
            {
            }

            return null;
        }

        /// <summary>
        /// Gets the partners.
        /// </summary>
        /// <returns>The partners.</returns>
        public IList<BPMaster> GetBusinessPartners()
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    return entities.BPMasters.Where(x => x.Deleted == null).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        /// <summary>
        /// Method that updates a business partner.
        /// </summary>
        /// <param name="partnersToUpdate">The partner to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdateBusinessPartnerPricing(IList<BPMaster> partnersToUpdate)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var partnerToUpdate in partnersToUpdate)
                    {
                        var dbPartner =
                           entities.BPMasters.FirstOrDefault(
                             partner => partner.BPMasterID == partnerToUpdate.BPMasterID);

                        if (dbPartner != null)
                        {
                            dbPartner.PriceListID = partnerToUpdate.PriceListID;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }

            return false;
        }

        /// <summary>
        /// Method that updates a price list.
        /// </summary>
        /// <param name="priceListsToUpdate">The price list to update.</param>
        /// <returns>A flag, indicating a successful update or not.</returns>
        public bool UpdatePriceMasters(IList<PriceList> priceListsToUpdate)
        {
            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var priceList in priceListsToUpdate)
                    {
                        var dbList =
                           entities.PriceLists.FirstOrDefault(
                             list => list.PriceListID == priceList.PriceListID);

                        if (dbList != null)
                        {
                            dbList.EndDate = DateTime.Today;
                        }
                    }

                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
            }

            return false;
        }

        /// <summary>
        /// Retrieve all the price methods.
        /// </summary>
        /// <returns>A collection of price methods.</returns>
        private IList<NouPriceMethod> GetPriceMethods()
        {
            var priceMethods = new List<NouPriceMethod>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    priceMethods = entities.NouPriceMethods.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return priceMethods;
        }

        /// <summary>
        /// Retrieve all the price methods.
        /// </summary>
        /// <returns>A collection of price methods.</returns>
        public IList<NouDocStatu> GetNouDocStatuses()
        {
            var statuses = new List<NouDocStatu>();

            try
            {
                using (var entities = new NouvemEntities())
                {
                    statuses = entities.NouDocStatus.Where(x => !x.Deleted).ToList();
                }
            }
            catch (Exception ex)
            {
            }

            return statuses;
        }

        /// <summary>
        /// Handles the price change to a future list.
        /// </summary>
        /// <param name="partners">The affected partners.</param>
        public void HandlePriceChanges(IList<BPMaster> partners)
        {
            var docketsInvoiced = new List<int>();
            var saleOrdersOnDockets = new List<int>();
            var priceMethods = this.GetPriceMethods();
            var priceByQty = priceMethods.First(x => x.Name == Constant.PriceByQty);
            var priceByWgt = priceMethods.First(x => x.Name == Constant.PriceByWgt);
            var priceListDetails = this.GetPriceListDetails();
            var statuses = this.GetNouDocStatuses();
            var statusActive = statuses.First(x => x.Value.Equals("Active"));
            var statusComplete = statuses.First(x => x.Value.Equals("Complete"));
            var statusCancelled = statuses.First(x => x.Value.Equals("Cancelled"));

            try
            {
                using (var entities = new NouvemEntities())
                {
                    foreach (var partner in partners)
                    {
                        #region invoice

                        var newPriceListId = partner.PriceListID;
                        var newPriceListDetails =
                            priceListDetails.Where(x => x.PriceListID == newPriceListId).ToList();
                        var partnerId = partner.BPMasterID;
                        var invoices = entities.ARInvoices
                            .Where(x => x.NouDocStatusID == statusActive.NouDocStatusID
                                   || x.NouDocStatusID == statusComplete.NouDocStatusID
                                        && x.BPMasterID_Customer == partnerId).ToList();

                        foreach (var invoice in invoices)
                        {
                            var docketChange = false;
                            var invoiceDetails = invoice.ARInvoiceDetails.Where(x => x.Deleted == null).ToList();
                            foreach (var invoiceDetail in invoiceDetails)
                            {
                                var currentpriceId = invoiceDetail.PriceListID;
                                var currentPriceDetail = priceListDetails
                                    .FirstOrDefault(x => x.PriceListID == currentpriceId && x.INMasterID == invoiceDetail.INMasterID);
                                if (currentPriceDetail != null)
                                {
                                    var currentPrice = currentPriceDetail.Price;
                                    var newPriceDetail =
                                        newPriceListDetails.FirstOrDefault(x => x.INMasterID == invoiceDetail.INMasterID);
                                    if (newPriceDetail != null)
                                    {
                                        if (newPriceDetail.Price != currentPrice)
                                        {
                                            docketChange = true;
                                            invoiceDetail.PriceListID = newPriceListId;
                                            var priceMethodID = newPriceDetail.NouPriceMethodID;
                                            invoiceDetail.UnitPrice = newPriceDetail.Price;
                                            invoiceDetail.UnitPriceAfterDiscount = newPriceDetail.Price;
                                            if (priceMethodID == priceByQty.NouPriceMethodID)
                                            {
                                                invoiceDetail.TotalExclVAT =
                                                    invoiceDetail.QuantityDelivered.ToDecimal() * newPriceDetail.Price;
                                                invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //if (invoiceDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //}
                                            }
                                            else if (priceMethodID == priceByWgt.NouPriceMethodID)
                                            {
                                                invoiceDetail.TotalExclVAT = invoiceDetail.WeightDelivered.ToDecimal() * newPriceDetail.Price;
                                                invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //if (invoiceDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //}
                                            }
                                            else
                                            {
                                                var typicalWgt = invoiceDetail.INMaster.NominalWeight;
                                                var qtyDelivered = invoiceDetail.QuantityDelivered;
                                                invoiceDetail.TotalExclVAT = (typicalWgt.ToDecimal() *
                                                                             qtyDelivered.ToDecimal()) *
                                                                             newPriceDetail.Price;
                                                invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;

                                                //if (invoiceDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    invoiceDetail.TotalIncVAT = invoiceDetail.TotalExclVAT;
                                                //}
                                            }
                                        }
                                    }
                                }
                            }

                            if (docketChange)
                            {
                                entities.SaveChanges();

                                var totalExVat = invoiceDetails.Sum(x => x.TotalExclVAT);
                                var totalIncVat = invoiceDetails.Sum(x => x.TotalIncVAT);

                                invoice.SubTotalExVAT = totalExVat;
                                invoice.GrandTotalIncVAT = totalIncVat;
                                entities.SaveChanges();

                                docketsInvoiced.Add(invoice.BaseDocumentReferenceID.ToInt());
                            }
                        }

                        #endregion

                        #region dispatch

                        var dockets = entities.ARDispatches
                            .Where(x => ((x.NouDocStatusID != statusComplete.NouDocStatusID
                                   && x.NouDocStatusID != statusCancelled.NouDocStatusID
                                   && x.Deleted == null) || docketsInvoiced.Contains(x.ARDispatchID))
                                        && x.BPMasterID_Customer == partnerId).ToList();

                        foreach (var docket in dockets)
                        {
                            var docketChange = false;
                            var docketDetails = docket.ARDispatchDetails.Where(x => x.Deleted == null).ToList();
                            foreach (var docketDetail in docketDetails)
                            {
                                var currentpriceId = docketDetail.PriceListID;
                                var currentPriceDetail = priceListDetails
                                    .FirstOrDefault(x => x.PriceListID == currentpriceId && x.INMasterID == docketDetail.INMasterID);
                                if (currentPriceDetail != null)
                                {
                                    var currentPrice = currentPriceDetail.Price;
                                    var newPriceDetail =
                                        newPriceListDetails.FirstOrDefault(x => x.INMasterID == docketDetail.INMasterID);
                                    if (newPriceDetail != null)
                                    {
                                        if (newPriceDetail.Price != currentPrice)
                                        {
                                            docketChange = true;
                                            docketDetail.PriceListID = newPriceListId;
                                            docketDetail.UnitPrice = newPriceDetail.Price;
                                            docketDetail.UnitPriceAfterDiscount = newPriceDetail.Price;

                                            var priceMethodID = newPriceDetail.NouPriceMethodID;
                                            if (priceMethodID == priceByQty.NouPriceMethodID)
                                            {
                                                docketDetail.TotalExclVAT =
                                                    docketDetail.QuantityDelivered.ToDecimal() * newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                            else if (priceMethodID == priceByWgt.NouPriceMethodID)
                                            {
                                                docketDetail.TotalExclVAT = docketDetail.WeightDelivered.ToDecimal() * newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                            else
                                            {
                                                var typicalWgt = docketDetail.INMaster.NominalWeight;
                                                var qtyDelivered = docketDetail.QuantityDelivered;
                                                docketDetail.TotalExclVAT = (typicalWgt.ToDecimal() *
                                                                             qtyDelivered.ToDecimal()) *
                                                                             newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;

                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                        }
                                    }
                                }
                            }

                            if (docketChange)
                            {
                                entities.SaveChanges();

                                var totalExVat = docketDetails.Sum(x => x.TotalExclVAT);
                                var totalIncVat = docketDetails.Sum(x => x.TotalIncVAT);

                                docket.SubTotalExVAT = totalExVat;
                                docket.GrandTotalIncVAT = totalIncVat;
                                entities.SaveChanges();

                                saleOrdersOnDockets.Add(docket.BaseDocumentReferenceID.ToInt());
                            }
                        }

                        #endregion

                        #region Order

                        var saleOrders = entities.AROrders
                            .Where(x => ((x.NouDocStatusID != statusComplete.NouDocStatusID
                                   && x.NouDocStatusID != statusCancelled.NouDocStatusID
                                   && x.Deleted == null) || saleOrdersOnDockets.Contains(x.AROrderID))
                                        && x.BPMasterID_Customer == partnerId).ToList();

                        foreach (var docket in saleOrders)
                        {
                            var docketChange = false;
                            var docketDetails = docket.AROrderDetails.Where(x => x.Deleted == null).ToList();
                            foreach (var docketDetail in docketDetails)
                            {
                                var currentpriceId = docketDetail.PriceListID;
                                var currentPriceDetail = priceListDetails
                                    .FirstOrDefault(x => x.PriceListID == currentpriceId && x.INMasterID == docketDetail.INMasterID);
                                if (currentPriceDetail != null)
                                {
                                    var currentPrice = currentPriceDetail.Price;
                                    var newPriceDetail =
                                        newPriceListDetails.FirstOrDefault(x => x.INMasterID == docketDetail.INMasterID);
                                    if (newPriceDetail != null)
                                    {
                                        if (newPriceDetail.Price != currentPrice)
                                        {
                                            docketChange = true;
                                            docketDetail.PriceListID = newPriceListId;
                                            docketDetail.UnitPrice = newPriceDetail.Price;
                                            docketDetail.UnitPriceAfterDiscount = newPriceDetail.Price;

                                            var priceMethodID = newPriceDetail.NouPriceMethodID;
                                            if (priceMethodID == priceByQty.NouPriceMethodID)
                                            {
                                                docketDetail.TotalExclVAT =
                                                    docketDetail.QuantityOrdered.ToDecimal() * newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                            else if (priceMethodID == priceByWgt.NouPriceMethodID)
                                            {
                                                docketDetail.TotalExclVAT = docketDetail.WeightOrdered.ToDecimal() * newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                            else
                                            {
                                                var typicalWgt = docketDetail.INMaster.NominalWeight;
                                                var qtyOrdered = docketDetail.QuantityOrdered;
                                                docketDetail.TotalExclVAT = (typicalWgt.ToDecimal() *
                                                                             qtyOrdered.ToDecimal()) *
                                                                             newPriceDetail.Price;
                                                docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;

                                                //if (docketDetail.VAT != null)
                                                //{

                                                //}
                                                //else
                                                //{
                                                //    docketDetail.TotalIncVAT = docketDetail.TotalExclVAT;
                                                //}
                                            }
                                        }
                                    }
                                }
                            }

                            if (docketChange)
                            {
                                entities.SaveChanges();

                                var totalExVat = docketDetails.Sum(x => x.TotalExclVAT);
                                var totalIncVat = docketDetails.Sum(x => x.TotalIncVAT);

                                docket.SubTotalExVAT = totalExVat;
                                docket.GrandTotalIncVAT = totalIncVat;
                                entities.SaveChanges();
                            }
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
