﻿// -----------------------------------------------------------------------
// <copyright file="LicenseGenerator.cs" company="Nouvem Technology">
// Copyright (c) Nouvem technology. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------
namespace Nouvem.Licencing
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Nouvem.Licencing.Properties;
    using Portable.Licensing;

    /// <summary>
    /// Class which handles the Nouvem licensing generation.
    /// </summary>
    public class LicenseGenerator
    {
        #region field

        /// <summary>
        /// The generated pass phrase GUID used as part of the key generation.
        /// </summary>
        private string passPhrase;

        /// <summary>
        /// The private key.
        /// </summary>
        private string privateKey;

        /// <summary>
        /// The public key.
        /// </summary>
        private string publicKey;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseGenerator"/> class.
        /// </summary>
        protected LicenseGenerator()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LicenseGenerator"/> class.
        /// </summary>
        /// <param name="licensePath">The directory path to the license.</param>
        /// <param name="useDefault">Flag, to indicate whether the default license name is used, or the input licenseName.</param>
        /// <param name="licenseName">The name given to the license.</param>
        protected LicenseGenerator(string licensePath,  bool useDefault, string licenseName = "")
        {
            Settings.Default.LicenseName = useDefault ?  Constant.DefaultLicenseName : licenseName;
            Settings.Default.LicensePath = licensePath;
            Settings.Default.Save();
        }

        #endregion

        #region public interface

        #region creation

        /// <summary>
        /// Method that creates a new license generator.
        /// </summary>
        /// <returns>A new license generator.</returns>
        /// <remarks>Used on the client side (Set the public key, validate)</remarks>
        public static LicenseGenerator CreateNew()
        {
            return new LicenseGenerator();
        }

        /// <summary>
        /// Method that creates a new license generator, with parameters.
        /// </summary>
        /// <param name="licensePath">The directory path to the license.</param>
        /// <param name="useDefault">Flag, to indicate whether the default license name is used, or the input licenseName.</param>
        /// <param name="licenseName">The name given to the license.</param>
        /// <returns>A new license generator.</returns>
        /// <remarks>Used by DEM (Generate the license file and public key)</remarks>
        public static LicenseGenerator Create(string licensePath, bool useDefault, string licenseName = "")
        {
            return new LicenseGenerator(licensePath, useDefault, licenseName);
        }

        #endregion

        #region generation

        /// <summary>
        /// Method that generates the license.
        /// </summary>
        /// <param name="expiryDate">The license expiry date.</param>
        /// <param name="numberOfUsers">The number of users allowed.</param>
        /// <param name="modules">The modules that can be used.</param>
        /// <param name="licenseeName">The licensee name.</param>
        /// <param name="email">The unique, user supplied, client mac address..</param>
        /// <param name="standardLicense">The type of license(Trial or standard).</param>
        /// <returns>A licenceDetail, that returns the generated licence data.</returns>
        public LicenseDetail GenerateLicense(
            DateTime expiryDate,
            int numberOfUsers,
            IDictionary<string, string> modules,
            string licenseeName,
            string email,
            bool standardLicense = true)
        {
            this.GenerateKeys();

            var licenseType = standardLicense ? LicenseType.Standard : LicenseType.Trial;

            // add the public key to the licence
            modules.Add(new KeyValuePair<string, string>("PublicKey", this.publicKey));
           
            // create the licence key
            var key = Guid.NewGuid();

            // create the licence
            var license = License.New()
                .WithUniqueIdentifier(key)
                .As(licenseType)
                .ExpiresAt(expiryDate)
                .WithMaximumUtilization(numberOfUsers) 
                .WithProductFeatures(modules)
                                  .LicensedTo(licenseeName, email)
                                  .CreateAndSignWithPrivateKey(privateKey, passPhrase);

            var fullLicensePath = Path.Combine(Settings.Default.LicensePath, Settings.Default.LicenseName);
            var fullKeyPath = Path.Combine(Settings.Default.LicensePath, Constant.PublicKeyFileName);

            // Write the license, and public key to file.
            File.WriteAllText(fullLicensePath, license.ToString(), Encoding.UTF8);
            File.WriteAllText(fullKeyPath, this.publicKey);

            return new LicenseDetail
            {
                Customer = licenseeName,
                Email = email,
                LicenseKey = key.ToString(),
                PublicKey = this.publicKey,
                PrivateKey = this.privateKey
            };
        }

        #endregion

        #endregion

        #region private 

         /// <summary>
        /// Method that generates the private/public license kets.
        /// </summary>
        private void GenerateKeys()
        {
            this.passPhrase = Guid.NewGuid().ToString(); 
             
            var keyGenerator = Portable.Licensing.Security.Cryptography.KeyGenerator.Create();
            var keyPair = keyGenerator.GenerateKeyPair();

            this.privateKey = keyPair.ToEncryptedPrivateKeyString(this.passPhrase);
            this.publicKey = keyPair.ToPublicKeyString();
        }

        #endregion
    }
}
